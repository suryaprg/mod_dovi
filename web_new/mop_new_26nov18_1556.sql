-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2018 at 09:55 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mop_new`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin` (`id_lv` VARCHAR(2), `email` VARCHAR(20), `pass` VARCHAR(32), `nama` VARCHAR(64), `nip` VARCHAR(20), `jabatan` VARCHAR(50), `id_dinas` INT(3)) RETURNS VARCHAR(16) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(16);
  
  select count(*) into count_row_user from admin;
        
  select id_admin into last_key_user from admin	order by id_admin desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"000001");
  else
    set fix_key_user = concat("AD",SUBSTR(last_key_user,3,13)+1);  
  END IF;
  
  insert into admin values(fix_key_user, id_lv, email, pass, "1", nama, nip, jabatan, id_dinas, "0");
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kecamatan` (`nama` VARCHAR(50), `latlng` TEXT, `luas` DOUBLE) RETURNS VARCHAR(7) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from main_kecamatan;
        
  select id_kec into last_key_user from main_kecamatan order by id_kec desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("KEC1","001");
  else
    set fix_key_user = concat("KEC",SUBSTR(last_key_user,4,4)+1);  
  END IF;
  
  INSERT INTO main_kecamatan VALUES(fix_key_user, nama, latlng, luas, "0");
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kelurahan` (`nama` VARCHAR(64), `luas` DOUBLE, `latlng` TEXT, `id_kec` VARCHAR(7)) RETURNS VARCHAR(7) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from main_kelurahan;
        
  select id_kel into last_key_user from main_kelurahan order by id_kel desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("KEL1","001");
  else
    set fix_key_user = concat("KEL",SUBSTR(last_key_user,4,4)+1);  
  END IF;
  
  INSERT INTO main_kelurahan VALUES(fix_key_user, id_kec, nama, latlng, luas, "0");
  
  return fix_key_user;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(16) NOT NULL,
  `id_lv` int(2) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_dinas` int(3) NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `id_lv`, `email`, `password`, `status_active`, `nama`, `nip`, `jabatan`, `id_dinas`, `is_delete`) VALUES
('AD201811000001', 1, 'dsfsd', 'aff8fbcbf1363cd7edc85a1e11391173', '1', 'surya', '1234', 'surya', 3, '0'),
('AD201811000002', 3, 'surya', 'surya', '1', 'surya', '1234', 'surya', 20, '1'),
('AD201811000003', 1, 'kominfo', 'f970e2767d0cfe75876ea857f92e319b', '1', 'doms rian', '21312312', 'kepala', 2, '0'),
('AD201811000004', 2, 'surya', 'admin', '1', 'surya hanggara', '123343', 'makijan', 4, '1'),
('AD201811000005', 1, 'surya', 'aff8fbcbf1363cd7edc85a1e11391173', '1', 'surya', '3573011903940006', 'pengolah data', 3, '0');

-- --------------------------------------------------------

--
-- Table structure for table `admin_lv`
--

CREATE TABLE `admin_lv` (
  `id_lv` int(2) NOT NULL,
  `ket` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_lv`
--

INSERT INTO `admin_lv` (`id_lv`, `ket`) VALUES
(1, 'Admin Super'),
(2, 'Admin STATISTIK'),
(3, 'Admin OPD');

-- --------------------------------------------------------

--
-- Table structure for table `id_kesehatan_sub_jenis`
--

CREATE TABLE `id_kesehatan_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iklim_hujan`
--

CREATE TABLE `iklim_hujan` (
  `id_hujan` int(6) NOT NULL,
  `id_st` int(6) NOT NULL,
  `th` int(4) NOT NULL,
  `periode` int(2) NOT NULL,
  `jml_cura` double NOT NULL,
  `jml_hari` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iklim_hujan`
--

INSERT INTO `iklim_hujan` (`id_hujan`, `id_st`, `th`, `periode`, `jml_cura`, `jml_hari`) VALUES
(1, 3, 2018, 1, 122, 12),
(3, 3, 2018, 2, 142, 12),
(4, 4, 2018, 1, 135, 13),
(5, 3, 2017, 1, 292, 16),
(6, 4, 2017, 1, 62, 10),
(7, 5, 2017, 1, 142, 10),
(8, 3, 2017, 2, 588, 25),
(9, 4, 2017, 2, 477, 30),
(10, 5, 2017, 2, 685, 24),
(11, 3, 2017, 3, 237, 24),
(12, 4, 2017, 3, 405, 18),
(13, 5, 2017, 3, 388, 18),
(14, 3, 2017, 4, 148, 12),
(15, 4, 2017, 4, 103, 10),
(16, 5, 2017, 4, 196, 11),
(17, 3, 2017, 5, 177, 18),
(18, 4, 2017, 5, 146, 12),
(19, 5, 2017, 5, 247, 12),
(20, 3, 2017, 6, 210, 13),
(21, 4, 2017, 6, 183, 10),
(22, 5, 2017, 6, 280, 12),
(23, 3, 2017, 7, 70, 10),
(24, 4, 2017, 7, 34, 6),
(25, 5, 2017, 7, 67, 11),
(26, 3, 2017, 8, 125, 7),
(27, 4, 2017, 8, 3, 3),
(28, 5, 2017, 8, 78, 5),
(29, 3, 2017, 9, 35, 6),
(30, 4, 2017, 9, 3, 3),
(31, 5, 2017, 9, 70, 7),
(32, 3, 2017, 10, 208, 14),
(33, 4, 2017, 10, 4, 4),
(34, 5, 2017, 10, 196, 14),
(35, 3, 2017, 11, 425, 20),
(36, 4, 2017, 11, 8, 8),
(37, 5, 2017, 11, 677, 25),
(38, 3, 2017, 12, 144, 14),
(39, 4, 2017, 12, 171, 9),
(40, 5, 2017, 12, 295, 18),
(41, 3, 2016, 1, 291, 15),
(42, 4, 2016, 1, 61, 8),
(43, 5, 2016, 1, 140, 9),
(44, 3, 2016, 2, 586, 23),
(45, 4, 2016, 2, 476, 29),
(46, 5, 2016, 2, 683, 22),
(47, 3, 2016, 3, 235, 21),
(48, 4, 2016, 3, 402, 16),
(49, 5, 2016, 3, 387, 16),
(50, 3, 2016, 4, 147, 11),
(51, 4, 2016, 4, 101, 8),
(52, 5, 2016, 4, 194, 9),
(53, 3, 2016, 5, 176, 16),
(54, 4, 2016, 5, 145, 11),
(55, 5, 2016, 5, 246, 11),
(56, 3, 2016, 6, 208, 12),
(57, 4, 2016, 6, 181, 8),
(58, 5, 2016, 6, 279, 10),
(59, 5, 2016, 6, 279, 10),
(60, 3, 2016, 7, 69, 8),
(61, 4, 2016, 7, 32, 4),
(62, 5, 2016, 8, 124, 6),
(63, 4, 2016, 8, 0, 0),
(64, 5, 2016, 8, 77, 3),
(65, 3, 2016, 9, 33, 5),
(66, 4, 2016, 9, 0, 0),
(67, 5, 2016, 9, 69, 5),
(68, 3, 2016, 10, 207, 13),
(69, 4, 2016, 10, 0, 0),
(70, 5, 2016, 10, 195, 12),
(71, 3, 2016, 11, 424, 19),
(72, 4, 2016, 11, 0, 0),
(73, 5, 2016, 11, 675, 22),
(74, 3, 2016, 12, 143, 12),
(75, 4, 2016, 12, 170, 8),
(76, 5, 2016, 12, 294, 17);

-- --------------------------------------------------------

--
-- Table structure for table `iklim_station`
--

CREATE TABLE `iklim_station` (
  `id_st` int(6) NOT NULL,
  `ket_st` varchar(50) NOT NULL,
  `alamat_st` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iklim_station`
--

INSERT INTO `iklim_station` (`id_st`, `ket_st`, `alamat_st`) VALUES
(3, 'Ciliwung', 'malang'),
(4, 'Kedung Kandang', 'malang'),
(5, 'Sukun', 'malang'),
(6, 'Kedawung', 'malang');

-- --------------------------------------------------------

--
-- Table structure for table `index_kes_daya_pend`
--

CREATE TABLE `index_kes_daya_pend` (
  `id_index` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `in_pend` varchar(10) NOT NULL,
  `in_kes` varchar(10) NOT NULL,
  `in_daya` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `index_kes_daya_pend`
--

INSERT INTO `index_kes_daya_pend` (`id_index`, `th`, `in_pend`, `in_kes`, `in_daya`) VALUES
(3, 2014, '73.43', '80.46', '83.33'),
(4, 2015, '76.05', '80.92', '83.37'),
(5, 2016, '76.52', '81.05', '83.98');

-- --------------------------------------------------------

--
-- Table structure for table `index_pem_manusia`
--

CREATE TABLE `index_pem_manusia` (
  `id_ipm` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `ipm` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `index_pem_manusia`
--

INSERT INTO `index_pem_manusia` (`id_ipm`, `th`, `ipm`) VALUES
(3, 2014, '78.96'),
(4, 2015, '80.05'),
(5, 2016, '80.46');

-- --------------------------------------------------------

--
-- Table structure for table `kepandudukan_jk`
--

CREATE TABLE `kepandudukan_jk` (
  `id_kepend_jk` int(11) NOT NULL,
  `th` int(11) NOT NULL,
  `rasio_jk` varchar(10) NOT NULL,
  `t_cowo` varchar(20) NOT NULL,
  `t_cewe` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kepandudukan_jk`
--

INSERT INTO `kepandudukan_jk` (`id_kepend_jk`, `th`, `rasio_jk`, `t_cowo`, `t_cewe`) VALUES
(3, 2014, '97.20', '416982', '428991'),
(4, 2015, '97.25', '419713', '431585'),
(5, 2016, '97.27', '422276', '434134');

-- --------------------------------------------------------

--
-- Table structure for table `kependudukan_kel_umur`
--

CREATE TABLE `kependudukan_kel_umur` (
  `id_kel_umur` int(11) NOT NULL,
  `kelompok` varchar(50) NOT NULL,
  `periode` int(11) NOT NULL,
  `jml_penduduk` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kependudukan_kel_umur`
--

INSERT INTO `kependudukan_kel_umur` (`id_kel_umur`, `kelompok`, `periode`, `jml_penduduk`) VALUES
(4, '0 - 14', 2014, '183388'),
(5, '0 - 14', 2015, '180358'),
(6, '0 - 14', 2016, '179299'),
(7, '15 - 64', 2014, '614628'),
(8, '15 - 64', 2015, '620802'),
(9, '15 - 64', 2016, '625473'),
(10, '> 65', 2014, '47957'),
(11, '> 65', 2015, '50138'),
(12, '> 65', 2016, '51638'),
(13, '0 - 14', 2019, '23423425324');

-- --------------------------------------------------------

--
-- Table structure for table `kependudukan_rasio_ketergantungan`
--

CREATE TABLE `kependudukan_rasio_ketergantungan` (
  `id_rasio` int(11) NOT NULL,
  `th_rasio` int(4) NOT NULL,
  `rasio_keter` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kependudukan_rasio_ketergantungan`
--

INSERT INTO `kependudukan_rasio_ketergantungan` (`id_rasio`, `th_rasio`, `rasio_keter`) VALUES
(3, 2014, '37.64'),
(4, 2015, '37.13'),
(5, 2016, '36.92');

-- --------------------------------------------------------

--
-- Table structure for table `kerja_angkatan`
--

CREATE TABLE `kerja_angkatan` (
  `id_angkatan` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_kerja` varchar(20) NOT NULL,
  `jml_no_kerja` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kerja_angkatan`
--

INSERT INTO `kerja_angkatan` (`id_angkatan`, `th`, `jml_kerja`, `jml_no_kerja`) VALUES
(3, 2014, '393050', '30581'),
(4, 2015, '377329', '29606'),
(5, 2016, '377329', '29606');

-- --------------------------------------------------------

--
-- Table structure for table `kerja_pengangguran`
--

CREATE TABLE `kerja_pengangguran` (
  `id_pengangguran` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kerja_pengangguran`
--

INSERT INTO `kerja_pengangguran` (`id_pengangguran`, `th`, `jml`) VALUES
(3, 2014, '6920'),
(4, 2015, '6257'),
(5, 2017, '6194');

-- --------------------------------------------------------

--
-- Table structure for table `kerja_ump`
--

CREATE TABLE `kerja_ump` (
  `id_ump` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kerja_ump`
--

INSERT INTO `kerja_ump` (`id_ump`, `th`, `jml`) VALUES
(4, 2014, '1587000'),
(5, 2015, '1882500'),
(6, 2016, '2099000');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_gizi_balita`
--

CREATE TABLE `kesehatan_gizi_balita` (
  `id_gizi_balita` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_gizi_buruk` varchar(10) NOT NULL,
  `jmh_kurang_gizi` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_gizi_balita`
--

INSERT INTO `kesehatan_gizi_balita` (`id_gizi_balita`, `th`, `jml_gizi_buruk`, `jmh_kurang_gizi`) VALUES
(3, 2014, '119', '1611'),
(4, 2015, '100', '1627'),
(5, 2016, '66', '1868');

-- --------------------------------------------------------

--
-- Table structure for table `keu_jenis`
--

CREATE TABLE `keu_jenis` (
  `id_jenis` int(11) NOT NULL,
  `ket` varchar(100) NOT NULL,
  `kategori` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keu_jenis`
--

INSERT INTO `keu_jenis` (`id_jenis`, `ket`, `kategori`) VALUES
(6, 'Bagi Hasil Pajak', '0'),
(7, 'Bagi Hasil Bukan Pajak / Sumber Daya Alam', '0'),
(8, 'Dana Alokasi Umum', '0'),
(9, 'Dana Alokasi Khusus', '0'),
(10, 'Belanja Pegawai', '1'),
(11, 'Belanja Modal', '1'),
(12, 'Belanja Lainnya', '1'),
(13, 'Pendapatan Asli Daerah', '0'),
(15, 'Lain-lain Pendapatan yang Sah', '0');

-- --------------------------------------------------------

--
-- Table structure for table `keu_jml`
--

CREATE TABLE `keu_jml` (
  `id_jml_keu` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keu_jml`
--

INSERT INTO `keu_jml` (`id_jml_keu`, `id_jenis`, `th`, `jml`) VALUES
(3, 6, 2014, '66740371.27'),
(4, 7, 2014, '50203519.87'),
(5, 8, 2014, '808447825'),
(6, 9, 2014, '31304060'),
(7, 10, 2014, '877246394.71'),
(8, 11, 2014, '318462052.42'),
(9, 12, 2014, '407291403.01'),
(10, 13, 2014, '372545396.29'),
(11, 14, 2014, '956695776.14'),
(12, 15, 2014, '435623517.23'),
(13, 16, 2014, '1764864689.66'),
(14, 17, 2014, '1602999850.14'),
(15, 6, 2015, '33850624'),
(16, 7, 2015, '53164497.78'),
(17, 8, 2015, '818758893'),
(18, 9, 2015, '20590560'),
(19, 10, 2015, '931090867.80'),
(20, 11, 2015, '337647558.97'),
(21, 12, 2015, '534682114.15'),
(22, 13, 2015, '424938755.52'),
(23, 14, 2015, '926364574.78'),
(24, 15, 2015, '477769359.41'),
(25, 16, 2015, '1829072689.71'),
(26, 17, 2015, '1803420540.92'),
(27, 6, 2016, '69368351.04'),
(28, 7, 2016, '45506060.94'),
(29, 8, 2016, '859678208'),
(30, 9, 2016, '94813827'),
(31, 10, 2016, '993592920'),
(32, 11, 2016, '193646732.35'),
(33, 12, 2016, '522678430.69'),
(34, 13, 2016, '447332655.83'),
(35, 14, 2016, '1069366446.98'),
(36, 15, 2016, '194486247.27'),
(37, 16, 2016, '1711185350.08'),
(38, 17, 2016, '1709918083.05');

-- --------------------------------------------------------

--
-- Table structure for table `lp_aparat`
--

CREATE TABLE `lp_aparat` (
  `id` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_aparat`
--

INSERT INTO `lp_aparat` (`id`, `id_jenis`, `tahun`, `jumlah`) VALUES
(11, 6, 2014, 1),
(12, 7, 2016, 8),
(23, 1, 2014, 959),
(27, 2, 2014, 134),
(28, 4, 2015, 27),
(29, 1, 2015, 968),
(30, 1, 2016, 959),
(31, 2, 2015, 120),
(32, 2, 2016, 116),
(33, 3, 2014, 1995),
(34, 3, 2015, 1995),
(35, 3, 2016, 2166),
(36, 4, 2014, 27),
(37, 4, 2016, 27),
(38, 5, 2014, 1267),
(39, 5, 2015, 1690),
(40, 5, 2016, 1690),
(41, 6, 2015, 1),
(42, 6, 2016, 1),
(43, 7, 2014, 8),
(44, 7, 2015, 8);

-- --------------------------------------------------------

--
-- Table structure for table `lp_aparat_jenis`
--

CREATE TABLE `lp_aparat_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_aparat_jenis`
--

INSERT INTO `lp_aparat_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Aparat keamanan (Polisi) (orang)'),
(2, 'Aparat Pamong Praja (Satpol PP) (orang)'),
(3, 'Aparat Perlindungan Masyarakat (Linmas)\r\n(orang)'),
(4, 'Pos Keamanan (Polisi)\r\n(unit)'),
(5, 'Pos Siskamling\r\n(unit)'),
(6, 'Pos Pemadam Kebakaran (unit)'),
(7, 'Mobil Pemadam Kebakaran (unit)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_bencana`
--

CREATE TABLE `lp_bencana` (
  `id_bencana` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_bencana_jenis`
--

CREATE TABLE `lp_bencana_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_kajian_penelitian`
--

CREATE TABLE `lp_kajian_penelitian` (
  `id_kajian` int(11) NOT NULL,
  `nama_kajian` text NOT NULL,
  `th` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kajian_penelitian`
--

INSERT INTO `lp_kajian_penelitian` (`id_kajian`, `nama_kajian`, `th`) VALUES
(1, 'Analisis Angka Tingkat Pengangguran', 2014),
(2, 'Kajian Fasilitas dan Insentif Pendukung Kepala Investor di Kota Malang', 2014),
(3, 'Studi Kebutuhan Teknologi Untuk Pengembangan Usaha Kecil dan Menengah di Kota Malang', 2014),
(4, 'Profil Perlindungan Sosial dan Pemberdayaan Masyarakat di Kota Malang', 2014),
(5, 'Analisa Kesesuaian Toko Modern Terhadap Perijinan Kota Malang', 2014),
(6, 'Analisa Angka Pertumbuhan Ekonomi di Kota Malang', 2014),
(7, 'Kajian Strategi Pemberdayaan Usaha Kecil Menengah Dalam Menyongsong Ekonomi Global ASEAN 2015', 2014),
(8, 'Penyusunan Pemetaan Potensi dan Pengembangan Ekonomi Sektor Informal Kota Malang', 2014),
(9, 'Rencana Aksi Daerah Pangan dan Gizi Kota Malang', 2014),
(10, 'Rencana Umum Penanaman Modal Kota Malang', 2014),
(11, 'Analisa Indeks Pembangunan Manusia Kota Malang Tahun 2014', 2014),
(12, 'Penyusunan Naskah Akademik dan Ranperda Tentang Sistem Kesehatan Daerah Kota Malang', 2014),
(13, 'Penyusunan Studi Kelayakan Akses Jalan Tunggulwulung -Sudimoro - Karanglo Pada Bagian Wilayah Perkotaan Malang Utara', 2014),
(14, 'Naskah Akademis Rencana Induk Sistem Penyediaan Air Minum (RISPAM) dan Penyusunan Rencana Peraturan Walikota Tentang RISPAM', 2014),
(15, 'Penyusunan Study Kelayakan Sarana Angkutan Umum Masal (SAUM) Kota Malang', 2014),
(16, 'Penyusunan Study Kelayakan dan Pra Detail Engineering Design Jembatan - Tlogomas - Saksofon Pada Bagian Wilayah Perkotaan Malang Utara', 2014),
(17, 'Penyusunan Naskah Akademis dan Rancangan Peraturan Walikota Tentang Tata Cara Pemberian Insentif dan Disinsentif Pemanfaatan Ruang Kota', 2014),
(18, 'Rencana Program Investasi Jangka Menengah (RPIJM), Dokumen Etrategi Sanitasi Kota (SSK) dan Memorandum Program Sanitasi (MPS) Berdasarkan', 2014),
(19, 'Penyusunan Naskah Akademis dan Rancangan Peraturan Walikota Tentang Rencana Tata Bangunan dan Lingkungan Sub BWP Prioritas Pada BWP Malang Tengah', 2014),
(20, 'Penyusunan Naskah Akademis dan Rancangan Peraturan Walikota Tentang Rencana Tata Bangunan dan Lingkungan Sub BWP Prioritas Pada BWP Malang Utara', 2014),
(21, 'Penyusunan Studi Kelayakan Pelebaran Jalan Ki Ageng Gribig Pada Bagian Wilayah Perkantoran Malang Timur', 2014),
(22, 'Penyusunan Studi Kelayakan Pelebaran Jalan Mayjend Sungkono Pada Bagian Wilayah Perkotaan Malang Tenggara', 2014),
(23, 'Penyusunan Naskah Akademis dan Rancangan Peraturan Walikota Tentang Tata Cara Partisipasi Masyarakat Dalam Penataan Ruang', 2014),
(24, 'Pembuatan Sistem Informasi Penataan Ruang Kota Malang', 2014),
(25, 'Analisa Kebutuhan Kantong Perkir di Bagian Wilayah Perncanaan Malang Tengah', 2015),
(26, 'Kajian Pemanfaatan Pelayanan Pemerintahan Berbasis Elektronik', 2015),
(27, 'Kajian Peningkatan Kelembagaan Usaha Ekonomi Perempuan', 2015),
(28, 'Pengembangan Modal Subsidi Pendidikan Bagi Warga Miskin', 2015),
(29, 'Kajian Peningkatan Fasilitas Sarana dan Prasarana Penunjang Destinasi Periwisata', 2015),
(30, 'Analisis Penguatan Kemitraan di Sektor Pariwisata , Hotel, Restoran dan Transportasi Untuk Peningkatan Jumlah Kunjungan dan Lama Hari Berkunjung', 2015),
(31, 'Penyusunan Naskah Akademis dan RANPERDA Penataan UMKM di Kota Malang', 2015),
(32, 'Penyusunan Naskah Akademis dan RANPERWAL Fasilitas dan Insentif Pendukung Kepada Investor di Kota Malang', 2015),
(33, 'Kajian Inovasi dan Peningkatan Standarisasi Produk', 2015),
(34, 'Analisa Pengembangan Semangat Kewirausahaan Bagi Wirausaha Pemula Kota Malang', 2015),
(35, 'Kajian Peran Koperasi Dalam Ekonomi Kerakyatan', 2015),
(36, 'Kajian Minat Baca Masyarakat dan Akses ke Perpustakaan Umum Kota Malang', 2015),
(37, 'Analisa Sarana dan Prasarana Olah Raga di Kota Malang', 2015),
(38, 'Pemetaan Kesenian', 2015),
(39, 'Kajian dan Investarisasi Berbasis Masyarakat', 2015),
(40, 'Evaluasi Rencana Aksi Daerah Pangan dan Gizi Kota Malang', 2015),
(41, 'Analisa dan Perhitungan PDRB Kota Malang', 2015),
(42, 'Rencana Induk Pengembangan Ekonomi Kreatif Kota Malang', 2015),
(43, 'Analisis Situasi Ibu dan Anak Kota Malang', 2015),
(44, 'Analisis Indeks Pembangunan Manusia Kota Malang', 2015),
(45, 'Laporan Pencapaian MDG’s Kota Malang', 2015),
(46, 'Naskah Akademis dan Ranperda Perlindungan dan Pemenuhan Hak Anak', 2015),
(47, 'Penyusunan Database Infrastuktur Daerah', 2015),
(48, 'Penyusunan Database Hukum dan Keamanan Serta Insidensial', 2015),
(49, 'Penyusunan Datebase Sosial Budaya dan Ekonomi Daerah', 2015),
(50, 'Penyusunan Database Geografi Pemerintahan Demografi dan SDA', 2015),
(51, 'Penyusunan Grand Design Peruntukan DBHCHT', 2015),
(52, 'Penyusunan Rancangan Awal RKPD Tahun 2017', 2015),
(53, 'Evaluasi Hasil RKPD Tahun 2015', 2015),
(54, 'Penyusunan NA dan Ranperda Perubahan RPJMD 2013-2018', 2015),
(55, 'Penyusunan Profil Kota Malang Tahun 2014', 2015),
(56, 'Pendampingan Aplikasi Sistem Perencanaan Pembangunan', 2015),
(57, 'Penyusunan Data SIPD', 2015),
(58, 'Penyusunan Rencana Aksi Malang Sustainable Urban Development (SUD)', 2015),
(59, 'Penyusunan Studi Kelayakan Pembangunan Jalan Tembus Tidar- Genting', 2015),
(60, 'Penyusunan Studi Kelayakan Underpass Taman Trunojoyo – Taman Sriwijaya - Stasiun', 2015),
(61, 'Penyusunan Rencana Aksi Malang Kota Pusaka (Heritage City)', 2015),
(62, 'Penyusunan Rencana Aksi Malang Kota Hijau (Green City)', 2015),
(63, 'Penyusunan Rencana Aksi Malang Tanpa Kumuh (Slum Free City)', 2015),
(64, 'Penyusunan Materi Teknis dan Rancangan Peraturan Walikota Tentang Rencana Induk Sistem Proteksi Kebakaran', 2015),
(65, 'Pelaksanaan Enironmental Helth Risk Assesment (EHRA)', 2015),
(66, 'Penyusunan Database Pembangunan', 2015),
(67, 'Penyusunan Materi Teknis dan Rancangan Peraturan Walikota Tentang Tata Cara Pengenaan Sanksi Atas Pelanggaran Pemanfaatan Ruang', 2015),
(68, 'Penyusunan Materi Teknis dan Rancangan Peraturan Walikota Tentang Mekanisme Pengendalian Pemanfaat Ruang Kota Malang', 2015),
(69, 'Profil Kuliner Kota Malang', 2016),
(70, 'Analisa Dampak Sosial Ekonomi Budaya Urban Mahasiswa Terhadap Masyarakat Kota Malang', 2016),
(71, 'Kajian Peran Lembaga Keuangan Dalam Pembangunan UMKM', 2016),
(72, 'Kajian Penambahan Prasarana Sekolah', 2016),
(73, 'Analisa Derajat Kesehatan', 2016),
(74, 'Analisa Potensi Peredaran Uang di Pasar Tradisional', 2016),
(75, 'Analisa Dampak Ekonomi Bank Sampah Masyarakat Terhadap Peningkatan Kualitas hidup Masyarakat di Kota Malang', 2016),
(76, 'Analisa Keamanan Pangan Guna Meningkatkan Daya Saing Usaha Mikro, Kecil dan Menengah Dalam Rangka', 2016),
(77, 'Database Pelaku Ekonomi Kota Malang', 2016),
(78, 'Pengembangan Pola Kemitraan UMKM dan IKM', 2016),
(79, 'Profil Pangan dan Gizi Kota Malang', 2016),
(80, 'Analisis PDRB Kota Malang', 2016),
(81, 'Rencana Aksi Penanganan Penyandang Masalah Kesejahteraan Sosial (PMKS)', 2016),
(82, 'Analisis Indeks Pembangunan Manusia Kota Malang Tahun 2016', 2016),
(83, 'Profil Kemiskinan', 2016),
(84, 'Profil Kota Layak Anak', 2016),
(85, 'Profil Kota Sehat', 2016),
(86, 'Kegiatan Integerasi Pembangunan Aplikasi Perencanaan Pembangunan dan Penganggaran', 2016),
(87, 'Kegiatan Pengembangan Aplikasi Perencanaan Pembangunan dan Penganggaran Kota Malang', 2016),
(88, 'Penyusunan Indentifikasi dan Analisa Isu Strategis Pembangunan Daerah', 2016),
(89, 'Kegiatan Penyusunan Prpofil Kota Malang Tahun 2015', 2016),
(90, 'Kajian Kebijakan Publik Dalam Perencanaan Pembangunan Kota Malang', 2016),
(91, 'Penyusunan Updating Rencana Program Investasi Infrastruktur Jangka Menengah (RP12JM)', 2016),
(92, 'Penyusunan Updating Dokumen Strategis Sanitasi Kota (SSK) dan Memorandum Program Sanitasi (MPS)', 2016),
(93, 'Penyusunan Roadmap Land Banking Kota Malang', 2016),
(94, 'Penyusunan Roadmap Malang Kota Berketahanan Bencana dan Perubahan Iklim', 2016),
(95, 'Penyusunan Naskah Akademis dan Rancangan Peraturan Walikota Tentang Rencana Induk Pengelolaan Sampah Rumah Tangga dan Sampah Sejenis Rumah Tangga', 2016),
(96, 'Penyusunan Review Naskah Akademis dan Rancangan Peraturan Walikota Tentang Rencana Induk Jaringan Jalan Terintegerasi Malang Raya', 2016),
(97, 'Penyusunan Studi Kelayakan dan Konsep Desain Malang Art And Spatial Gallery', 2016),
(98, 'Pengembangan Integerasi Aplikasi Sisten Informasi Penataan Ruang', 2016),
(99, 'Review Rencana Tata Ruang Wilayah Kota Malang Tahun 2010-2030', 2016),
(100, 'Review Penyusunan Rencana Kawasan Stategis', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `lp_kel_umur`
--

CREATE TABLE `lp_kel_umur` (
  `id_kel_umur` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_l` varchar(10) NOT NULL,
  `jml_p` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_kel_umur_jenis`
--

CREATE TABLE `lp_kel_umur_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_kendaran_kec`
--

CREATE TABLE `lp_kendaran_kec` (
  `id_kendaraan` int(11) NOT NULL,
  `id_jenis_kendaraan` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kendaran_kec`
--

INSERT INTO `lp_kendaran_kec` (`id_kendaraan`, `id_jenis_kendaraan`, `id_kec`, `th`, `jml`) VALUES
(1, 1, 'KEC1001', 2015, '20044'),
(2, 1, 'KEC1001', 2016, '15470'),
(3, 2, 'KEC1001', 2015, '121'),
(4, 2, 'KEC1001', 2016, '113'),
(5, 3, 'KEC1001', 2015, '4158'),
(6, 3, 'KEC1001', 2016, '3529'),
(7, 4, 'KEC1001', 2015, '98035'),
(8, 4, 'KEC1001', 2016, '97595'),
(9, 1, 'KEC1002', 2015, '14607'),
(10, 1, 'KEC1002', 2016, '17170'),
(11, 2, 'KEC1002', 2015, '112'),
(12, 2, 'KEC1002', 2016, '163'),
(13, 3, 'KEC1002', 2015, '3398'),
(14, 3, 'KEC1002', 2016, '4410'),
(15, 4, 'KEC1002', 2015, '93795'),
(16, 4, 'KEC1002', 2016, '107218'),
(17, 1, 'KEC1003', 2015, '13068'),
(18, 1, 'KEC1003', 2016, '13306'),
(19, 2, 'KEC1003', 2015, '252'),
(20, 2, 'KEC1003', 2016, '265'),
(21, 3, 'KEC1003', 2015, '4092'),
(22, 3, 'KEC1003', 2016, '4060'),
(23, 4, 'KEC1003', 2015, '54967'),
(24, 4, 'KEC1003', 2016, '56385'),
(25, 1, 'KEC1004', 2015, '21985'),
(26, 1, 'KEC1004', 2016, '21098'),
(27, 2, 'KEC1004', 2015, '315'),
(28, 2, 'KEC1004', 2016, '137'),
(29, 3, 'KEC1004', 2015, '3463'),
(30, 3, 'KEC1004', 2016, '4445'),
(31, 4, 'KEC1004', 2015, '90455'),
(32, 4, 'KEC1004', 2016, '101541'),
(33, 1, 'KEC1005', 2015, '16387'),
(34, 1, 'KEC1005', 2016, '23014'),
(35, 2, 'KEC1005', 2015, '134'),
(36, 2, 'KEC1005', 2016, '288'),
(37, 3, 'KEC1005', 2015, '4356'),
(38, 3, 'KEC1005', 2016, '3558'),
(39, 4, 'KEC1005', 2015, '103871'),
(40, 4, 'KEC1005', 2016, '93954');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kendaran_plat`
--

CREATE TABLE `lp_kendaran_plat` (
  `id_kendaraan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_merah` varchar(10) NOT NULL,
  `jml_kuning` varchar(10) NOT NULL,
  `jml_hitam` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kendaran_plat`
--

INSERT INTO `lp_kendaran_plat` (`id_kendaraan`, `id_jenis`, `th`, `jml_merah`, `jml_kuning`, `jml_hitam`) VALUES
(1, 1, 2015, '13923', '165', '77'),
(2, 1, 2016, '13585', '174', '85'),
(3, 2, 2015, '8337', '-', '38'),
(4, 2, 2016, '8699', '-', '35'),
(5, 3, 2015, '60460', '2442', '649'),
(6, 3, 2016, '64417', '2401', '662'),
(7, 4, 2015, '379', '502', '53'),
(8, 4, 2016, '418', '481', '67'),
(9, 5, 2015, '15956', '3312', '199'),
(10, 5, 2016, '16473', '3317', '212'),
(11, 6, 2015, '439648', '-', '1475'),
(12, 6, 2016, '455073', '-', '1620');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kendaran_plat_jenis`
--

CREATE TABLE `lp_kendaran_plat_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kendaran_plat_jenis`
--

INSERT INTO `lp_kendaran_plat_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Sedan dan Sejenisnya'),
(2, 'Jeep dan Sejenisnya'),
(3, 'Station Wagon dan Sejenisnya'),
(4, 'Bus dan Sejenisnya'),
(5, 'Truck dan Sejenisnya'),
(6, 'Sepeda Motor');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kendaran_umum`
--

CREATE TABLE `lp_kendaran_umum` (
  `id_kendaraan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_jenis_kendaraan` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kendaran_umum`
--

INSERT INTO `lp_kendaran_umum` (`id_kendaraan`, `id_jenis`, `id_jenis_kendaraan`, `th`, `jml`) VALUES
(1, 1, 1, 2015, '2606'),
(2, 1, 1, 2016, '90058'),
(3, 2, 1, 2015, '77682'),
(4, 2, 1, 2016, '86701'),
(5, 3, 1, 2015, '700'),
(6, 3, 1, 2016, '782'),
(7, 1, 2, 2015, '543'),
(8, 1, 2, 2016, '481'),
(9, 2, 2, 2015, '327'),
(10, 2, 2, 2016, '418'),
(11, 3, 2, 2015, '48'),
(12, 3, 2, 2016, '67'),
(13, 1, 3, 2015, '3378'),
(14, 1, 3, 2016, '3317'),
(15, 2, 3, 2015, '15042'),
(16, 2, 3, 2016, '16473'),
(17, 3, 3, 2015, '178'),
(18, 3, 3, 2016, '212'),
(19, 1, 4, 2015, '-'),
(20, 1, 4, 2016, '-'),
(21, 2, 4, 2015, '410177'),
(22, 2, 4, 2016, '455073'),
(23, 3, 4, 2015, '1391'),
(24, 3, 4, 2016, '1620');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kendaran_umum_jenis`
--

CREATE TABLE `lp_kendaran_umum_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kendaran_umum_jenis`
--

INSERT INTO `lp_kendaran_umum_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Umum'),
(2, 'Non Umum (Pribadi)'),
(3, 'Dinas');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kesehatan`
--

CREATE TABLE `lp_kesehatan` (
  `id_kesehatan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kesehatan`
--

INSERT INTO `lp_kesehatan` (`id_kesehatan`, `id_jenis`, `id_sub_jenis`, `th`, `jml`) VALUES
(3, 1, 1, 2014, '655'),
(4, 1, 1, 2015, '653'),
(5, 1, 1, 2016, '647'),
(6, 27, 0, 2014, '57'),
(7, 27, 0, 2015, '57'),
(8, 27, 0, 2016, '57'),
(9, 5, 4, 2014, '15'),
(10, 5, 4, 2015, '15'),
(11, 5, 4, 2016, '15'),
(12, 5, 5, 2014, '34'),
(13, 5, 5, 2015, '34'),
(14, 5, 5, 2016, '34'),
(15, 5, 6, 2014, '15'),
(16, 5, 6, 2015, '15'),
(17, 5, 6, 2016, '15'),
(18, 6, 8, 2014, '1'),
(19, 6, 8, 2015, '1'),
(20, 6, 8, 2016, '1'),
(21, 6, 11, 2016, '1'),
(22, 7, 12, 2015, '2'),
(23, 7, 12, 2016, '4'),
(24, 7, 13, 2014, '5'),
(25, 7, 13, 2015, '4'),
(26, 7, 13, 2016, '5'),
(27, 7, 14, 2014, '3'),
(28, 7, 14, 2015, '3'),
(29, 7, 14, 2016, '1'),
(30, 8, 16, 2014, '13'),
(31, 8, 16, 2015, '13'),
(32, 8, 16, 2016, '13'),
(33, 28, 0, 2014, '589'),
(34, 28, 0, 2015, '570'),
(35, 28, 0, 2016, '600'),
(36, 9, 26, 2014, '2'),
(37, 9, 26, 2015, '2'),
(38, 9, 26, 2016, '2');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kesehatan_jenis`
--

CREATE TABLE `lp_kesehatan_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL,
  `kategori_jenis` enum('0','1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kesehatan_jenis`
--

INSERT INTO `lp_kesehatan_jenis` (`id_jenis`, `nama_jenis`, `kategori_jenis`) VALUES
(1, 'Posyandu', '0'),
(5, 'Puskesmas', '0'),
(6, 'Rumah Sakit Umum Daerah', '0'),
(7, 'Rumah Sakit Umum Swasta', '0'),
(8, 'Rumah Sakit Khusus', '0'),
(9, 'Rumah Sakit Tentara', '0'),
(10, 'Jumlah Rumah Tangga Yang Menggunakan Air Bersih', '0'),
(11, 'Jumlah Penduduk Yang Mendapatkan Akses Air Minum', '0'),
(12, 'Industri Farmasi Narkotika', '1'),
(13, 'Industri Farmasi Produksi Obat Tradisional / Industri Obat Tradisional', '1'),
(14, 'Gudang Farmasi\r\n', '1'),
(15, 'PBF (Pedagang Besar Farmasi)', '1'),
(16, 'Produk Alat Kesehatan', '1'),
(17, 'Penyalur Obat Kesehatan', '1'),
(18, 'Cabang Penyalur Obat Kesehatan', '1'),
(19, 'Penyalur Alat Kesehatan', '1'),
(20, 'Jumlah Orang Sakit Jiwa\r\n', '2'),
(21, 'Jumlah Penderita Narkoba\r\n', '2'),
(22, 'Jumlah Balita', '2'),
(23, 'Jumlah Penduduk meninggal Menurut Wabah (Total)', '2'),
(24, 'Jumlah ibu Hamil Gizi buruk', '2'),
(25, ' Jumlah peserta Program KB Aktif', '2'),
(26, 'Rata-rata Jumlah Penduduk Yang Sakit', '2'),
(27, 'Poskesdes', '0'),
(28, 'Klinik/Praktek', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kesehatan_sub3_jenis`
--

CREATE TABLE `lp_kesehatan_sub3_jenis` (
  `id_3sub_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `nama_3sub_jenis` text NOT NULL,
  `satuan` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_kesehatan_sub_jenis`
--

CREATE TABLE `lp_kesehatan_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL,
  `satuan` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kesehatan_sub_jenis`
--

INSERT INTO `lp_kesehatan_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`, `satuan`) VALUES
(1, 1, 'Posyandu Terdaftar', 'unit'),
(2, 1, 'Posyandu Tidak Aktif', 'unit'),
(4, 5, 'Induk\r\n', 'unit'),
(5, 5, 'Pembantu\r\n', 'unit'),
(6, 5, 'Keliling', 'unit'),
(7, 5, 'Poliklinik', 'unit'),
(8, 6, 'Tipe A', 'unit'),
(9, 6, 'Tipe B', 'unit'),
(10, 6, 'Tipe C\r\n', 'unit'),
(11, 6, 'Tipe D\r\n', 'unit'),
(12, 7, 'Tipe A', 'unit'),
(13, 7, 'Tipe B', 'unit'),
(14, 7, 'Tipe C', 'unit'),
(15, 7, 'Tipe D', 'unit'),
(16, 8, 'Rumah Sakit Bersalin', 'unit'),
(17, 8, 'Rumah Sakit Jiwa', 'unit'),
(18, 8, 'Rumah Sakit Ketergantungan Obat', 'unit'),
(19, 8, 'Rumah Sakit Kusta\r\n', 'unit'),
(20, 8, 'Rumah Sakit Mata', 'unit'),
(21, 8, 'Rumah Sakit Bedah', 'unit'),
(22, 8, 'Rumah Sakit Jantung', 'unit'),
(23, 8, 'Rumah Sakit Paru', 'unit'),
(25, 8, 'Rumah Sakit Umum Pusat (RSUP)', 'unit'),
(26, 9, 'Rumah Sakit Angkatan Darat', 'unit'),
(27, 9, 'Rumah Sakit Angkatan Udara', 'unit'),
(28, 9, 'Rumah Sakit Angkatan Laut', 'unit'),
(29, 9, 'Rumah Sakit POLRI', 'unit'),
(30, 10, 'Leding (Perpipaan)', 'RT'),
(31, 10, 'Sumur Lindung', 'RT'),
(32, 10, 'Sumur Tidak Terlindungi\r\n', 'RT'),
(33, 10, 'Mata Air Terlindungi', 'RT'),
(34, 10, 'Mata Air Tidak Terlindungi', 'RT'),
(35, 10, 'Danau/Waduk', 'RT'),
(36, 10, 'Air Hujan', 'RT'),
(37, 10, 'Air Kemasan', 'RT'),
(38, 22, 'Jumlah Seluruh Balita Kurang Gizi', 'Balita'),
(39, 22, 'Jumlah Anak Balita 0-3 Tahun', 'Balita'),
(40, 22, 'Jumlah Balita Kurang Gizi Buruk', 'Balita'),
(41, 22, 'Jumlah Seluruh Balita Gizi Buruk', 'Balita'),
(42, 22, 'Jumlah balita Gizi Buruk Yang Mendapat Perawatan', 'Balita'),
(43, 23, 'Menurut Wabah Demam Berdarah', 'Jiwa'),
(44, 23, 'Menurut Wabah Muntaber', 'Jiwa'),
(45, 23, 'Menurut Wabah Infeksi Saluran Pernafasan', 'Jiwa'),
(46, 23, 'Menurut wabah Campak\r\n', 'Jiwa'),
(47, 23, 'Menurut Wabah Malaria', 'Jiwa'),
(48, 23, 'Menurut Wabah Lainnya', 'Jiwa'),
(49, 25, 'Laki-laki', 'Jiwa'),
(50, 25, 'Perempuan', 'Jiwa');

-- --------------------------------------------------------

--
-- Table structure for table `lp_keu`
--

CREATE TABLE `lp_keu` (
  `id_keu` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_keu`
--

INSERT INTO `lp_keu` (`id_keu`, `id_jenis`, `id_sub_jenis`, `th`, `jml`) VALUES
(2, 1, 2, 2014, '278885189.55'),
(3, 1, 2, 2015, '316682891.17'),
(4, 1, 2, 2016, '374641673.42'),
(5, 1, 3, 2014, '45557675.30'),
(6, 1, 3, 2015, '35281817.93'),
(7, 1, 3, 2016, '42782439.06'),
(8, 1, 4, 2014, '13385924.50'),
(9, 1, 4, 2015, '14649144.87'),
(10, 1, 4, 2016, '15785980.80'),
(11, 1, 5, 2014, '34716606.94'),
(12, 1, 5, 2015, '58324901.55'),
(13, 1, 5, 2016, '44122562.56'),
(14, 2, 6, 2014, '66740371.27'),
(15, 2, 6, 2015, '33850624.00'),
(16, 2, 6, 2016, '69368351.04'),
(17, 2, 7, 2014, '50203519.87'),
(18, 2, 7, 2015, '53164497.78'),
(19, 2, 7, 2016, '45506060.94'),
(20, 2, 8, 2014, '808447825.00'),
(21, 2, 8, 2015, '818758893.00'),
(22, 2, 8, 2016, '859678208.00'),
(23, 2, 9, 2014, '31304060.00'),
(24, 2, 9, 2015, '20590560.00'),
(25, 2, 9, 2016, '94813827.00'),
(26, 3, 10, 2014, '19023000.00'),
(27, 3, 10, 2015, '14781000.00'),
(28, 3, 10, 2016, '12606000.00'),
(29, 3, 12, 2014, '154505481.23'),
(30, 3, 12, 2015, '152348658.41'),
(31, 3, 12, 2016, '171619907.27'),
(32, 3, 13, 2014, '217906979.00'),
(33, 3, 13, 2015, '224102748.00'),
(34, 3, 13, 2016, '5000000.00'),
(35, 3, 14, 2014, '44188057.00'),
(36, 3, 14, 2015, '86536953.00'),
(37, 3, 14, 2016, '5260340.00'),
(38, 4, 16, 2014, '798826075.31'),
(39, 4, 16, 2015, '825456394.83'),
(40, 4, 16, 2016, '862559055.66'),
(41, 4, 19, 2014, '44148137.50'),
(42, 4, 19, 2015, '66492877.50'),
(43, 4, 19, 2016, '57425620.00'),
(44, 4, 20, 2014, '1402739.63'),
(45, 4, 20, 2015, '397500.00'),
(46, 4, 21, 2014, '78330.60'),
(47, 4, 21, 2015, '75036.60'),
(48, 4, 21, 2016, '78766.20'),
(49, 4, 22, 2014, '647332.84'),
(50, 4, 22, 2015, '635264.96'),
(51, 4, 22, 2016, '635264396.00'),
(52, 4, 23, 2014, '13261.35'),
(53, 4, 23, 2015, '657238.60'),
(54, 4, 23, 2016, '331951.13'),
(55, 5, 24, 2014, '78420319.40'),
(56, 5, 24, 2015, '105634472.97'),
(57, 5, 24, 2016, '131033864.34'),
(58, 5, 25, 2014, '361001601.09'),
(59, 5, 25, 2015, '466424196.49'),
(60, 5, 25, 2016, '464206828.40'),
(61, 5, 26, 2014, '318462052.42 '),
(62, 5, 26, 2015, '337647558.97'),
(63, 5, 26, 2016, '193646732.35');

-- --------------------------------------------------------

--
-- Table structure for table `lp_keu_jenis`
--

CREATE TABLE `lp_keu_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL,
  `kategori` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_keu_jenis`
--

INSERT INTO `lp_keu_jenis` (`id_jenis`, `nama_jenis`, `kategori`) VALUES
(1, 'pendapatan asli daerah', '0'),
(2, 'dana perimbangan', '0'),
(3, 'lain-lain pendapatan yang sah', '0'),
(4, 'Belanja Tidak Langsung', '1'),
(5, 'Belanja Langsung', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lp_keu_sub_jenis`
--

CREATE TABLE `lp_keu_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_keu_sub_jenis`
--

INSERT INTO `lp_keu_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`) VALUES
(2, 1, 'pajak daerah'),
(3, 1, 'Retribusi Daerah'),
(4, 1, 'Hasil Perusahaan Milik Daerah dan Pengelolaan Kekayaan Daerah yang di Pisahkan'),
(5, 1, 'Lain-lain PAD yang sah'),
(6, 2, 'Bagi Hasil Pajak'),
(7, 2, 'Bagi Hasil Bukan Pajak/ Sumber Daya Alam'),
(8, 2, 'Dana Alokasi Umum'),
(9, 2, 'Dana Alokasi Khusus'),
(10, 3, 'Pendapatan hibah'),
(11, 3, 'Dana Darurat'),
(12, 3, 'Dana Bagi Hasil Pajak dari Provinsi dan Pemerintah Daerah Lainnya'),
(13, 3, 'Dana Penyesuaian dan Otonomi Daerah'),
(14, 3, 'Bantuan Keuangan dari Provinsi atau Pemerintah Daerah Lainnya'),
(15, 3, 'Lainnya'),
(16, 4, 'Belanja Pegawai '),
(17, 4, 'Belanja Bunga'),
(18, 4, 'Belanja Subsidi'),
(19, 4, 'Belanja Hibah'),
(20, 4, 'Belanja Bantuan Sosial'),
(21, 4, 'Belanja Bagi Hasil Kepada Provinsi/Kabupaten/ Kota'),
(22, 4, 'Belanja Bantuan Keuangan '),
(23, 4, 'Belanja Tidak Terduga'),
(24, 5, 'Belanja Pegawai'),
(25, 5, 'Belanja Barang dan Jasa'),
(26, 5, 'Belanja Modal');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kooperasi_jenis`
--

CREATE TABLE `lp_kooperasi_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kooperasi_jenis`
--

INSERT INTO `lp_kooperasi_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Koperasi'),
(2, 'Jumlah Pengusaha yang Bergabung Dalam Koperasi'),
(3, 'Mengadakan Rapat Tahunan (unit)'),
(4, 'Jumlah Koperasi yang Tidak Mengadakan Rapat Tahunan (unit)\r\n'),
(5, 'Jumlah Koperasi yang Dinyatakan Kolaps (unit)'),
(6, 'Jumlah Uang Yang Beredar pada Koperasi di Kota Malang(rupiah)'),
(7, 'Jumlah Koperasi Yang Mendapat Penghargaan (unit)\r\n'),
(8, 'Jumlah Anggota (orang)'),
(9, 'Jumlah Aset (rupiah)'),
(10, 'Sisa Hasil Usaha (rupiah)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kooperasi_sub_jenis`
--

CREATE TABLE `lp_kooperasi_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kooperasi_sub_jenis`
--

INSERT INTO `lp_kooperasi_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`) VALUES
(1, 1, 'Jumlah Koperasi Aktif (unit)'),
(2, 1, 'Jumlah Koperasi Tidak Aktif (unit)'),
(3, 1, 'Jumlah Induk Koperasi (unit)'),
(4, 1, 'Jumlah Koperasi Primer (unit)'),
(5, 1, 'Jumlah KUD (unit)'),
(6, 1, 'Jumlah Non KUD (unit)\r\n'),
(7, 2, 'Pengusaha Kecil (orang)'),
(8, 2, 'Pengusaha Menengah (orang)'),
(9, 2, 'Pengusaha Besar (orang)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_koperasi`
--

CREATE TABLE `lp_koperasi` (
  `id_koperasi` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_koperasi`
--

INSERT INTO `lp_koperasi` (`id_koperasi`, `id_jenis`, `id_sub_jenis`, `th`, `jml`) VALUES
(1, 1, 1, 2014, '395'),
(2, 1, 1, 2015, '407'),
(3, 1, 1, 2016, '382'),
(4, 1, 2, 2014, '366'),
(5, 1, 2, 2015, '360'),
(6, 1, 2, 2016, '336'),
(7, 1, 3, 2014, '7'),
(8, 1, 3, 2015, '7'),
(9, 1, 3, 2016, '7'),
(10, 1, 4, 2014, '759'),
(11, 1, 4, 2015, '767'),
(12, 1, 4, 2016, '718'),
(13, 1, 5, 2014, '4'),
(14, 1, 5, 2015, '4'),
(15, 1, 5, 2016, '4'),
(16, 1, 6, 2014, '755'),
(17, 1, 6, 2015, '763'),
(18, 1, 6, 2016, '714'),
(19, 2, 7, 2014, '415'),
(20, 2, 7, 2015, '409'),
(21, 2, 7, 2016, '403'),
(22, 3, 0, 2014, '305'),
(23, 3, 0, 2015, '321'),
(24, 3, 0, 2016, '301'),
(25, 4, 0, 2014, '456'),
(26, 4, 0, 2015, '446'),
(27, 4, 0, 2016, '417'),
(30, 5, 0, 2014, '99'),
(31, 5, 0, 2015, '99'),
(32, 5, 0, 2016, '99'),
(33, 6, 0, 2014, '850920000'),
(34, 6, 0, 2015, '892621300000'),
(35, 6, 0, 2016, '528050976048'),
(36, 7, 0, 2014, '3'),
(37, 7, 0, 2015, '9'),
(38, 7, 0, 2016, '1'),
(39, 8, 0, 2014, '72593'),
(40, 8, 0, 2015, '77966'),
(41, 8, 0, 2016, '92666'),
(42, 9, 0, 2014, '851513370000'),
(43, 9, 0, 2015, '857600232000'),
(44, 9, 0, 2016, '1008552777531'),
(45, 10, 0, 2014, '32960420000'),
(46, 10, 0, 2015, '33016020000'),
(47, 10, 0, 2016, '50686769443');

-- --------------------------------------------------------

--
-- Table structure for table `lp_lahan`
--

CREATE TABLE `lp_lahan` (
  `id_lahan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kec` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_lahan_jenis`
--

CREATE TABLE `lp_lahan_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_miskin`
--

CREATE TABLE `lp_miskin` (
  `id_miskin` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_miskin`
--

INSERT INTO `lp_miskin` (`id_miskin`, `id_jenis`, `th`, `jml`) VALUES
(1, 1, 2014, '40,64'),
(2, 1, 2015, '39,1'),
(3, 1, 2016, '37,03'),
(4, 2, 2014, '4,80'),
(5, 2, 2015, '4,6'),
(6, 2, 2016, '4,33'),
(7, 3, 2014, '0,61 '),
(8, 3, 2015, '0,53'),
(9, 3, 2016, '0,54'),
(10, 4, 2014, '0,12'),
(11, 4, 2015, '0,11'),
(12, 4, 2016, '0,09'),
(13, 5, 2014, '381 400'),
(14, 5, 2015, '411 709'),
(15, 5, 2016, '426 527');

-- --------------------------------------------------------

--
-- Table structure for table `lp_miskin_jenis`
--

CREATE TABLE `lp_miskin_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_miskin_jenis`
--

INSERT INTO `lp_miskin_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Jumlah Penduduk Miskin (000)'),
(2, 'Persentase Penduduk Miskin (Po)'),
(3, 'Indeks Kedalaman Kemiskinan (P1)'),
(4, 'Indeks Keparahan Kemiskinan (P2)'),
(5, 'Garis Kemiskinan (Rp/Kab/Bulan)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_or`
--

CREATE TABLE `lp_or` (
  `id_or` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_or`
--

INSERT INTO `lp_or` (`id_or`, `id_jenis`, `th`, `jml`) VALUES
(1, 1, 2014, 43),
(2, 1, 2015, 43),
(3, 1, 2016, 44),
(4, 2, 2014, 201),
(5, 2, 2015, 201),
(6, 2, 2016, 201),
(7, 3, 2014, 74),
(8, 3, 2015, 74),
(9, 3, 2016, 74),
(10, 4, 2014, 55),
(11, 4, 2015, 153),
(12, 4, 2016, 220),
(13, 5, 2014, 22),
(14, 5, 2015, 104),
(15, 5, 2016, 62),
(16, 6, 2014, 23),
(17, 6, 2015, 106),
(18, 6, 2016, 101),
(19, 7, 2014, 20),
(20, 7, 2015, 78),
(21, 7, 2016, 102),
(22, 8, 2014, 12),
(23, 8, 2015, 89),
(24, 8, 2016, 118),
(25, 9, 2014, 23),
(26, 9, 2015, 106),
(27, 9, 2016, 101),
(28, 10, 2014, 20),
(29, 10, 2015, 78),
(30, 10, 2016, 102),
(31, 11, 2014, 12),
(32, 11, 2015, 89),
(33, 11, 2016, 118),
(34, 12, 2014, 174),
(35, 12, 2015, 174),
(36, 12, 2016, 174),
(37, 13, 2014, 52),
(38, 13, 2015, 53),
(39, 13, 2016, 53),
(40, 14, 2014, 164),
(41, 14, 2015, 164),
(42, 14, 2016, 164),
(43, 15, 2014, 6),
(44, 15, 2015, 6),
(45, 15, 2016, 6),
(46, 16, 2014, 42),
(47, 16, 2015, 42),
(48, 16, 2016, 42),
(49, 17, 2014, 3),
(50, 17, 2015, 3),
(51, 17, 2016, 3),
(52, 18, 2014, 1),
(53, 18, 2015, 1),
(54, 18, 2016, 1);

-- --------------------------------------------------------

--
-- Table structure for table `lp_or_jenis`
--

CREATE TABLE `lp_or_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL,
  `kategori` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_or_jenis`
--

INSERT INTO `lp_or_jenis` (`id_jenis`, `nama_jenis`, `kategori`) VALUES
(1, 'Jumlah cabang Olahraga yang mendapat pembinaan Koni', '0'),
(2, 'Jumlah Club Olahraga di bawah Pembinaan Koni', '0'),
(3, 'Jumlah Sarana dan Prasarana Olahraga\r\n', '0'),
(4, 'Jumlah Prestasi yang didapat\r\ndari ajang olahraga tingkat Jawa\r\nTimur', '0'),
(5, 'Jumlah Prestasi yang Didapat dari ajang olahraga tingkat Jawa Nasional\r\n', '0'),
(6, 'Jumlah atlet yang mendapatkan medali emas\r\n', '0'),
(7, 'Jumlah atlit yang mendapatkan medali perak\r\n', '0'),
(8, 'Jumlah atlit yang mendapatkan medali perunggu\r\n', '0'),
(9, 'Jumlah klub olahraga yang menerima medali emas\r\n', '0'),
(10, 'Jumlah klub olahraga yang menerima medali perak ', '0'),
(11, 'Jumlah klub olahraga yang menerima medali perunggu', '0'),
(12, 'Jumlah Klub Olahraga (Klub)\r\n', '1'),
(13, 'Jumlah Sarana dan Prasarana Olahraga (Unit) ', '1'),
(14, 'Organisasi Kemasyarakatan Pemuda (Buah) ', '1'),
(15, 'Kegiatan Kepemudaan (Kegiatan)', '1'),
(16, 'Organisasi Olahraga (Buah) ', '1'),
(17, 'Kegiatan Olahraga Tradisional (Kegiatan)', '1'),
(18, 'Kegiatan Olahraga Prestasi (Kegiatan) ', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pajak`
--

CREATE TABLE `lp_pajak` (
  `id_pajak` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(11) NOT NULL,
  `jml` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pajak`
--

INSERT INTO `lp_pajak` (`id_pajak`, `id_jenis`, `th`, `jml`) VALUES
(1, 1, 2014, '17169937868.68 '),
(2, 1, 2015, '22131094351.27'),
(3, 1, 2016, '27680570200.00'),
(4, 2, 2014, '28476534584.04'),
(5, 2, 2015, '28476534584.04'),
(6, 2, 2016, '34976534500.00'),
(7, 3, 2014, '4542595922.35'),
(8, 3, 2015, '4943004 465.24'),
(9, 3, 2016, '5543000 000.00'),
(10, 4, 2014, '156401433942.58'),
(11, 4, 2015, '18676522723.99'),
(12, 4, 2016, '18670522800.00'),
(13, 5, 2014, '37602101561.92'),
(14, 5, 2015, '40602101561.92'),
(15, 5, 2016, '44602106500.00'),
(16, 6, 2014, '-'),
(17, 6, 2015, '-'),
(18, 6, 2016, '-'),
(19, 7, 2014, '1947997758.99'),
(20, 7, 2015, '2501998407.60'),
(21, 7, 2016, '3501998000.00'),
(22, 8, 2014, '701130420.40'),
(23, 8, 2015, '749475964.90'),
(24, 8, 2016, '600000000.00'),
(25, 9, 2014, '-'),
(26, 9, 2015, '-'),
(27, 9, 2016, '-'),
(28, 10, 2014, '53869267940.29'),
(29, 10, 2015, '53869267940.29'),
(30, 10, 2016, '56869258000.00'),
(31, 11, 2014, '100050000000.75'),
(32, 11, 2015, '100050000000.75'),
(33, 11, 2016, '108550000000.00'),
(34, 12, 2014, '-'),
(35, 12, 2015, '-'),
(36, 12, 2016, '-'),
(37, 13, 2014, '-'),
(38, 13, 2015, '-'),
(39, 13, 2016, '-');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pajak_jenis`
--

CREATE TABLE `lp_pajak_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pajak_jenis`
--

INSERT INTO `lp_pajak_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Pajak Hotel'),
(2, 'Pajak Restoran'),
(3, 'Pajak Hiburan'),
(4, 'Pajak Reklame'),
(5, 'Pajak Penerangan Jalan '),
(6, 'Pajak Mineral Bukan Logam dan Batuan'),
(7, 'Pajak Parkir'),
(8, 'Pajak Air Tanah'),
(9, 'Pajak Sarang Burung Walet'),
(10, 'Pajak Bumi dan Bangunan Perdesaan dan Perkotaan (PBB-P2)'),
(11, 'Bea Perolehan Hak atas Tanah dan Bangunan (BPHTP)'),
(12, 'Jumlah Pajak yang Dikeluarkan'),
(13, 'Jumlah Insentif Pajak yang Mendukung Iklim Investasi');

-- --------------------------------------------------------

--
-- Table structure for table `lp_panen`
--

CREATE TABLE `lp_panen` (
  `id_panen` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `luas_panen` varchar(20) NOT NULL,
  `jml_panen` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_panen`
--

INSERT INTO `lp_panen` (`id_panen`, `id_jenis`, `id_kec`, `th`, `luas_panen`, `jml_panen`) VALUES
(5, 1, 'KEC1001', 2014, '666', '4065'),
(6, 1, 'KEC1001', 2015, '606', '4139'),
(7, 1, 'KEC1002', 2014, '556', '3671'),
(8, 1, 'KEC1002', 2015, '645', '4658'),
(9, 1, 'KEC1003', 2014, '0', '0'),
(10, 1, 'KEC1003', 2015, '0', '0'),
(11, 1, 'KEC1001', 2016, '628', '4442'),
(12, 1, 'KEC1002', 2016, '636', '4676'),
(13, 1, 'KEC1003', 2016, '0', '0'),
(14, 1, 'KEC1004', 2014, '253', '1689'),
(15, 1, 'KEC1004', 2015, '201', '1460'),
(16, 1, 'KEC1004', 2016, '191', '1312'),
(17, 1, 'KEC1005', 2014, '509', '3754'),
(18, 1, 'KEC1005', 2015, '525', '4008'),
(19, 1, 'KEC1005', 2016, '545', '4043'),
(20, 2, 'KEC1001', 2014, '145', '691'),
(21, 2, 'KEC1001', 2015, '126', '566'),
(22, 2, 'KEC1001', 2016, '79', '519'),
(23, 2, 'KEC1002', 2014, '10', '44'),
(24, 2, 'KEC1002', 2015, '10', '45'),
(25, 2, 'KEC1002', 2016, '10', '45'),
(26, 2, 'KEC1003', 2014, '0', '0'),
(27, 2, 'KEC1003', 2015, '0', '0'),
(28, 2, 'KEC1003', 2016, '0', '0'),
(29, 2, 'KEC1004', 2014, '0', '0'),
(30, 2, 'KEC1004', 2015, '1', '4'),
(31, 2, 'KEC1004', 2016, '0', '0'),
(32, 2, 'KEC1005', 2014, '8', '33'),
(33, 2, 'KEC1005', 2015, '14', '63'),
(34, 2, 'KEC1005', 2016, '7', '41'),
(35, 3, 'KEC1001', 2014, '10', '36'),
(36, 3, 'KEC1001', 2015, '3', '11'),
(37, 3, 'KEC1001', 2016, '0', '0'),
(38, 3, 'KEC1002', 2014, '4', '14'),
(39, 3, 'KEC1002', 2015, '4', '14'),
(42, 3, 'KEC1002', 2016, '3', '11'),
(43, 3, 'KEC1003', 2014, '0', '0'),
(44, 3, 'KEC1003', 2015, '0', '0'),
(45, 3, 'KEC1003', 2016, '0', '0'),
(46, 3, 'KEC1004', 2014, '0', '0'),
(47, 3, 'KEC1004', 2015, '0', '0'),
(48, 3, 'KEC1004', 2016, '0', '0'),
(49, 3, 'KEC1005', 2014, '0', '0'),
(50, 3, 'KEC1005', 2015, '2', '7'),
(51, 3, 'KEC1005', 2016, '1', '4'),
(52, 4, 'KEC1001', 2014, '119', '2420'),
(53, 4, 'KEC1001', 2015, '78', '2128'),
(54, 4, 'KEC1001', 2016, '73', '2978'),
(55, 4, 'KEC1002', 2014, '6', '122'),
(56, 4, 'KEC1002', 2015, '11', '308'),
(57, 4, 'KEC1002', 2016, '4', '202'),
(58, 4, 'KEC1003', 2014, '0', '0'),
(59, 4, 'KEC1003', 2015, '0', '0'),
(60, 4, 'KEC1003', 2016, '0', '0'),
(61, 4, 'KEC1004', 2014, '0', '0'),
(62, 4, 'KEC1004', 2015, '0', '0'),
(63, 4, 'KEC1004', 2016, '0', '0'),
(64, 4, 'KEC1005', 2014, '5', '102'),
(65, 4, 'KEC1005', 2015, '2', '40'),
(66, 4, 'KEC1005', 2016, '2', '101'),
(67, 5, 'KEC1001', 2014, '1', '22'),
(68, 5, 'KEC1001', 2015, '0', '0'),
(69, 5, 'KEC1001', 2016, '0', '0'),
(70, 5, 'KEC1002', 2014, '0', '0'),
(71, 5, 'KEC1002', 2015, '0', '0'),
(72, 5, 'KEC1002', 2016, '0', '0'),
(73, 5, 'KEC1003', 2014, '0', '0'),
(74, 5, 'KEC1003', 2015, '0', '0'),
(75, 3, 'KEC1003', 2016, '0', '0'),
(76, 5, 'KEC1004', 2014, '0', '0'),
(77, 5, 'KEC1004', 2015, '0', '0'),
(78, 5, 'KEC1004', 2016, '0', '0'),
(79, 5, 'KEC1005', 2014, '0', '0'),
(80, 5, 'KEC1005', 2015, '0', '0'),
(81, 5, 'KEC1005', 2016, '1', '19');

-- --------------------------------------------------------

--
-- Table structure for table `lp_panen_jenis`
--

CREATE TABLE `lp_panen_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_panen_jenis`
--

INSERT INTO `lp_panen_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'padisawah'),
(2, 'jagung'),
(3, 'kacangtanah'),
(4, 'ubikayu'),
(5, 'ubijalar');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pdb`
--

CREATE TABLE `lp_pdb` (
  `id_pdb` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_pdb_jenis`
--

CREATE TABLE `lp_pdb_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL,
  `satuan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_pendidikan`
--

CREATE TABLE `lp_pendidikan` (
  `id_pendidikan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_negeri` varchar(10) NOT NULL,
  `jml_swasta` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pendidikan`
--

INSERT INTO `lp_pendidikan` (`id_pendidikan`, `id_jenis`, `id_kategori`, `id_kec`, `th`, `jml_negeri`, `jml_swasta`) VALUES
(2, 1, 1, 'KEC1001', 2014, '2', '55'),
(3, 1, 1, 'KEC1001', 2015, '2', '61'),
(4, 1, 1, 'KEC1001', 2016, '2', '63'),
(5, 1, 1, 'KEC1002', 2014, '1', '67'),
(6, 1, 1, 'KEC1002', 2015, '1', '68'),
(7, 1, 1, 'KEC1002', 2016, '1', '68'),
(8, 1, 1, 'KEC1003', 2014, '1', '69'),
(9, 1, 1, 'KEC1003', 2015, '1', '72'),
(10, 1, 1, 'KEC1003', 2016, '1', '64'),
(11, 1, 1, 'KEC1004', 2014, '1', '64'),
(12, 1, 1, 'KEC1004', 2015, '1', '65'),
(13, 1, 1, 'KEC1004', 2016, '1', '66'),
(14, 1, 1, 'KEC1005', 2014, '0', '73'),
(15, 1, 1, 'KEC1005', 2015, '0', '73'),
(16, 1, 1, 'KEC1005', 2016, '0', '75'),
(17, 2, 1, 'KEC1001', 2014, '98', '4111'),
(18, 2, 1, 'KEC1001', 2015, '51', '3853'),
(19, 2, 1, 'KEC1001', 2016, '176', '4103'),
(20, 2, 1, 'KEC1002', 2014, '17', '3454'),
(21, 2, 1, 'KEC1002', 2015, '100', '4313'),
(22, 2, 1, 'KEC1002', 2016, '85', '4283'),
(23, 2, 1, 'KEC1003', 2014, '98', '3798'),
(24, 2, 1, 'KEC1003', 2015, '180', '4199'),
(25, 2, 1, 'KEC1003', 2016, '165', '4059'),
(26, 2, 1, 'KEC1004', 2014, '47', '3770'),
(27, 2, 1, 'KEC1004', 2015, '131', '3895'),
(28, 2, 1, 'KEC1004', 2016, '148', '3974'),
(29, 2, 1, 'KEC1005', 2014, '0', '4015'),
(30, 2, 1, 'KEC1005', 2015, '0', '3831'),
(31, 2, 1, 'KEC1005', 2016, '0', '3658'),
(32, 3, 1, 'KEC1001', 2014, '14', '261'),
(33, 3, 1, 'KEC1001', 2015, '4', '283'),
(34, 3, 1, 'KEC1001', 2016, '15', '536'),
(35, 3, 1, 'KEC1002', 2014, '5', '295'),
(36, 3, 1, 'KEC1002', 2015, '7', '307'),
(37, 3, 1, 'KEC1002', 2016, '6', '574'),
(38, 3, 1, 'KEC1003', 2014, '10', '301'),
(39, 3, 1, 'KEC1003', 2015, '14', '299'),
(40, 3, 1, 'KEC1003', 2016, '12', '523'),
(41, 3, 1, 'KEC1004', 2014, '5', '313'),
(42, 3, 1, 'KEC1004', 2015, '8', '327'),
(43, 3, 1, 'KEC1004', 2016, '9', '574'),
(44, 3, 1, 'KEC1005', 2014, '0', '319'),
(45, 3, 1, 'KEC1005', 2015, '0', '309'),
(46, 3, 1, 'KEC1005', 2016, '0', '578'),
(47, 4, 1, 'KEC1001', 2014, '7.00', '15.75'),
(48, 4, 1, 'KEC1001', 2015, '12.75', '13.61'),
(49, 4, 1, 'KEC1001', 2016, '11.73', '7.65'),
(50, 4, 1, 'KEC1002', 2014, '3.40', '11.71'),
(51, 4, 1, 'KEC1002', 2015, '14.29', '14.05'),
(52, 4, 1, 'KEC1002', 2016, '14.17', '7.46'),
(53, 4, 1, 'KEC1003', 2014, '9.80', '12.62'),
(54, 4, 1, 'KEC1003', 2015, '12.86', '14.04'),
(55, 4, 1, 'KEC1003', 2016, '13.75', '7.76'),
(56, 4, 1, 'KEC1004', 2014, '9.40', '16.44'),
(57, 4, 1, 'KEC1004', 2015, '16.38', '11.91'),
(58, 4, 1, 'KEC1004', 2016, '16.44', '6.92'),
(59, 4, 1, 'KEC1005', 2014, '0', '12.59'),
(60, 4, 1, 'KEC1005', 2015, '0', '12.40'),
(61, 4, 1, 'KEC1005', 2016, '0.00', '6.33'),
(62, 1, 2, 'KEC1001', 2014, '0', '1'),
(63, 1, 2, 'KEC1001', 2015, '0', '0'),
(64, 1, 2, 'KEC1001', 2016, '0', '1'),
(65, 1, 2, 'KEC1002', 2014, '0', '1'),
(66, 1, 2, 'KEC1002', 2015, '0', '1'),
(67, 1, 2, 'KEC1002', 2016, '0', '1'),
(68, 1, 2, 'KEC1003', 2014, '0', '1'),
(69, 1, 2, 'KEC1003', 2015, '0', '1'),
(70, 1, 2, 'KEC1003', 2016, '0', '1'),
(71, 1, 2, 'KEC1004', 2014, '0', '2'),
(72, 1, 2, 'KEC1004', 2015, '0', '2'),
(73, 1, 2, 'KEC1004', 2016, '0', '2'),
(74, 1, 2, 'KEC1005', 2014, '0', '2'),
(75, 1, 2, 'KEC1005', 2015, '0', '2'),
(76, 1, 2, 'KEC1005', 2016, '0', '2'),
(77, 2, 2, 'KEC1001', 2014, '0', '21'),
(78, 2, 2, 'KEC1001', 2015, '0', '0'),
(79, 2, 2, 'KEC1001', 2016, '0', '18'),
(80, 2, 2, 'KEC1002', 2014, '0', '13'),
(81, 2, 2, 'KEC1002', 2015, '0', '2'),
(82, 2, 2, 'KEC1002', 2016, '0', '0'),
(83, 2, 2, 'KEC1003', 2014, '0', '21'),
(84, 2, 2, 'KEC1003', 2015, '0', '13'),
(85, 2, 2, 'KEC1003', 2016, '0', '14'),
(86, 2, 2, 'KEC1004', 2014, '0', '16'),
(87, 2, 2, 'KEC1004', 2015, '0', '20'),
(88, 2, 2, 'KEC1004', 2016, '0', '9'),
(89, 2, 2, 'KEC1005', 2014, '0', '40'),
(90, 2, 2, 'KEC1005', 2015, '0', '27'),
(91, 2, 2, 'KEC1005', 2016, '0', '0'),
(92, 3, 2, 'KEC1001', 2014, '0', '23'),
(93, 3, 2, 'KEC1001', 2015, '0', '0'),
(94, 3, 2, 'KEC1001', 2016, '0', '4'),
(95, 3, 2, 'KEC1002', 2014, '0', '7'),
(96, 3, 2, 'KEC1002', 2015, '0', '1'),
(97, 3, 2, 'KEC1002', 2016, '0', '0'),
(98, 3, 2, 'KEC1003', 2014, '0', '8'),
(99, 3, 2, 'KEC1003', 2015, '0', '4'),
(100, 3, 2, 'KEC1003', 2016, '0', '1'),
(101, 3, 2, 'KEC1004', 2014, '0', '23'),
(102, 3, 2, 'KEC1004', 2015, '0', '5'),
(103, 3, 2, 'KEC1004', 2016, '0', '3'),
(104, 3, 2, 'KEC1005', 2014, '0', '26'),
(105, 3, 2, 'KEC1005', 2015, '0', '14'),
(106, 3, 2, 'KEC1005', 2016, '0', '11'),
(107, 4, 2, 'KEC1001', 2014, '0', '0.91'),
(108, 4, 2, 'KEC1001', 2015, '0', '0'),
(109, 4, 2, 'KEC1001', 2016, '0', '4.50'),
(110, 4, 2, 'KEC1002', 2014, '0', '1.86'),
(111, 4, 2, 'KEC1002', 2015, '0', '2.00'),
(112, 4, 2, 'KEC1002', 2016, '0', '0'),
(113, 4, 2, 'KEC1003', 2014, '0', '2.63'),
(114, 4, 2, 'KEC1003', 2015, '0', '3.25'),
(115, 4, 2, 'KEC1003', 2016, '0', '14.00'),
(116, 4, 2, 'KEC1004', 2014, '0', '0.70'),
(117, 4, 2, 'KEC1004', 2015, '0', '4.00'),
(118, 4, 2, 'KEC1004', 2016, '0', '3.00'),
(119, 4, 2, 'KEC1005', 2014, '0', '1.54'),
(120, 4, 2, 'KEC1005', 2015, '0', '1.93'),
(121, 4, 2, 'KEC1005', 2016, '0', '0'),
(122, 1, 3, 'KEC1001', 2014, '0', '32'),
(123, 1, 3, 'KEC1001', 2015, '0', '37'),
(124, 1, 3, 'KEC1001', 2016, '0', '36'),
(125, 1, 3, 'KEC1002', 2014, '0', '18'),
(126, 1, 3, 'KEC1002', 2015, '0', '18'),
(127, 1, 3, 'KEC1002', 2016, '0', '18'),
(128, 1, 3, 'KEC1003', 2014, '0', '5'),
(129, 1, 3, 'KEC1003', 2015, '0', '5'),
(130, 1, 3, 'KEC1003', 2016, '0', '6'),
(131, 1, 3, 'KEC1004', 2014, '0', '12'),
(132, 1, 3, 'KEC1004', 2015, '0', '15'),
(133, 1, 3, 'KEC1004', 2016, '0', '15'),
(134, 1, 3, 'KEC1005', 2014, '0', '25'),
(135, 1, 3, 'KEC1005', 2015, '0', '25'),
(136, 1, 3, 'KEC1005', 2016, '0', '26'),
(137, 2, 3, 'KEC1001', 2014, '0', '1939'),
(138, 2, 3, 'KEC1001', 2015, '0', '2000'),
(139, 2, 3, 'KEC1001', 2016, '0', '2000'),
(140, 2, 3, 'KEC1002', 2014, '0', '1437'),
(141, 2, 3, 'KEC1002', 2015, '0', '1409'),
(142, 2, 3, 'KEC1002', 2016, '0', '1396'),
(143, 2, 3, 'KEC1003', 2014, '0', '413'),
(144, 2, 3, 'KEC1003', 2015, '0', '466'),
(145, 2, 3, 'KEC1003', 2016, '0', '310'),
(146, 2, 3, 'KEC1004', 2014, '0', '996'),
(147, 2, 3, 'KEC1004', 2015, '0', '1255'),
(148, 2, 3, 'KEC1004', 2016, '0', '1289'),
(149, 2, 3, 'KEC1005', 2014, '0', '1290'),
(150, 2, 3, 'KEC1005', 2015, '0', '1334'),
(151, 2, 3, 'KEC1005', 2016, '0', '1634'),
(152, 3, 3, 'KEC1001', 2014, '0', '113'),
(153, 3, 3, 'KEC1001', 2015, '0', '138'),
(154, 3, 3, 'KEC1001', 2016, '0', '131'),
(155, 3, 3, 'KEC1002', 2014, '0', '83'),
(156, 3, 3, 'KEC1002', 2015, '0', '77'),
(157, 3, 3, 'KEC1002', 2016, '0', '75'),
(158, 3, 3, 'KEC1003', 2014, '0', '38'),
(159, 3, 3, 'KEC1003', 2015, '0', '30'),
(160, 3, 3, 'KEC1003', 2016, '0', '20'),
(161, 3, 3, 'KEC1004', 2014, '0', '86'),
(162, 3, 3, 'KEC1004', 2015, '0', '98'),
(163, 3, 3, 'KEC1004', 2016, '0', '103'),
(164, 3, 3, 'KEC1005', 2014, '0', '116'),
(165, 3, 3, 'KEC1005', 2015, '0', '99'),
(166, 3, 3, 'KEC1005', 2016, '0', '108'),
(167, 4, 3, 'KEC1001', 2014, '0', '17.16'),
(168, 4, 3, 'KEC1001', 2015, '0', '14.49'),
(169, 4, 3, 'KEC1001', 2016, '0', '15.80'),
(170, 4, 3, 'KEC1002', 2014, '0', '17.31'),
(171, 4, 3, 'KEC1002', 2015, '0', '18.30'),
(172, 4, 3, 'KEC1002', 2016, '0', '18.61'),
(173, 4, 3, 'KEC1003', 2014, '0', '10.87'),
(174, 4, 3, 'KEC1003', 2015, '0', '15.53'),
(175, 4, 3, 'KEC1003', 2016, '0', '15.50'),
(176, 4, 3, 'KEC1004', 2014, '0', '11.58'),
(177, 4, 3, 'KEC1004', 2015, '0', '12.81'),
(178, 4, 3, 'KEC1004', 2016, '0', '12.51'),
(179, 4, 3, 'KEC1005', 2014, '0', '11.12'),
(180, 4, 3, 'KEC1005', 2015, '0', '13.47'),
(181, 4, 3, 'KEC1005', 2016, '0', '15.13'),
(182, 1, 4, 'KEC1001', 2014, '45', '10'),
(183, 1, 4, 'KEC1001', 2015, '45', '10'),
(184, 1, 4, 'KEC1001', 2016, '45', '11'),
(185, 1, 4, 'KEC1002', 2014, '42', '16'),
(186, 1, 4, 'KEC1002', 2015, '42', '16'),
(187, 1, 4, 'KEC1002', 2016, '42', '16'),
(188, 1, 4, 'KEC1003', 2014, '19', '23'),
(189, 1, 4, 'KEC1003', 2015, '19', '23'),
(190, 1, 4, 'KEC1003', 2016, '19', '24'),
(191, 1, 4, 'KEC1004', 2014, '44', '13'),
(192, 1, 4, 'KEC1004', 2015, '44', '14'),
(193, 1, 4, 'KEC1004', 2016, '44', '13'),
(194, 1, 4, 'KEC1005', 2014, '45', '13'),
(195, 1, 4, 'KEC1005', 2015, '45', '14'),
(196, 1, 4, 'KEC1005', 2016, '45', '15'),
(197, 2, 4, 'KEC1001', 2014, '13920', '1339'),
(198, 2, 4, 'KEC1001', 2015, '13587', '1537'),
(199, 2, 4, 'KEC1001', 2016, '13738', '1738'),
(200, 2, 4, 'KEC1002', 2014, '14090', '2166'),
(201, 2, 4, 'KEC1002', 2015, '13786', '2581'),
(202, 2, 4, 'KEC1002', 2016, '13674', '2761'),
(203, 2, 4, 'KEC1003', 2014, '6519', '5784'),
(204, 2, 4, 'KEC1003', 2015, '6303', '5778'),
(205, 2, 4, 'KEC1003', 2016, '6057', '5847'),
(206, 2, 4, 'KEC1004', 2014, '13623', '3293'),
(207, 2, 4, 'KEC1004', 2015, '13235', '3279'),
(208, 2, 4, 'KEC1004', 2016, '12979', '3222'),
(209, 2, 4, 'KEC1005', 2014, '11726', '4339'),
(210, 2, 4, 'KEC1005', 2015, '11425', '4868'),
(211, 2, 4, 'KEC1005', 2016, '11190', '5612'),
(212, 3, 4, 'KEC1001', 2014, '612', '119'),
(213, 3, 4, 'KEC1001', 2015, '611', '128'),
(214, 3, 4, 'KEC1001', 2016, '617', '129'),
(215, 3, 4, 'KEC1002', 2014, '601', '205'),
(216, 3, 4, 'KEC1002', 2015, '583', '229'),
(217, 3, 4, 'KEC1002', 2016, '589', '235'),
(218, 3, 4, 'KEC1003', 2014, '305', '357'),
(219, 3, 4, 'KEC1003', 2015, '315', '363'),
(220, 3, 4, 'KEC1003', 2016, '303', '355'),
(221, 3, 4, 'KEC1004', 2014, '664', '258'),
(222, 3, 4, 'KEC1004', 2015, '605', '249'),
(223, 3, 4, 'KEC1004', 2016, '604', '235'),
(224, 3, 4, 'KEC1005', 2014, '584', '300'),
(225, 3, 4, 'KEC1005', 2015, '573', '314'),
(226, 3, 4, 'KEC1005', 2016, '593', '345'),
(227, 4, 4, 'KEC1001', 2014, '22.75', '11.25'),
(228, 4, 4, 'KEC1001', 2015, '22.24', '12.0'),
(229, 4, 4, 'KEC1001', 2016, '22.27', '13.47'),
(230, 4, 4, 'KEC1002', 2014, '23.44', '210.57'),
(231, 4, 4, 'KEC1002', 2015, '23.65', '1.27'),
(232, 4, 4, 'KEC1002', 2016, '23.22', '11.75'),
(233, 4, 4, 'KEC1003', 2014, '21.37', '16.20'),
(234, 4, 4, 'KEC1003', 2015, '20.01', '15.92'),
(235, 4, 4, 'KEC1003', 2016, '19.99', '16.47'),
(236, 4, 4, 'KEC1004', 2014, '20.52', '12.76'),
(237, 4, 4, 'KEC1004', 2015, '21.88', '13.17'),
(238, 4, 4, 'KEC1004', 2016, '21.49', '13.71'),
(239, 4, 4, 'KEC1005', 2014, '20.08', '14.46'),
(240, 4, 4, 'KEC1005', 2015, '19.94', '15.50'),
(241, 4, 4, 'KEC1005', 2016, '18.87', '16.27'),
(242, 1, 5, 'KEC1001', 2014, '1', '0'),
(243, 1, 5, 'KEC1001', 2015, '1', '0'),
(244, 1, 5, 'KEC1001', 2016, '1', '0'),
(245, 1, 5, 'KEC1002', 2014, '0', '1'),
(246, 1, 5, 'KEC1002', 2015, '0', '1'),
(247, 1, 5, 'KEC1002', 2016, '0', '1'),
(248, 1, 5, 'KEC1003', 2014, '0', '1'),
(249, 1, 5, 'KEC1003', 2015, '0', '1'),
(250, 1, 5, 'KEC1003', 2016, '0', '1'),
(251, 1, 5, 'KEC1004', 2014, '0', '3'),
(252, 1, 5, 'KEC1004', 2015, '0', '3'),
(253, 1, 5, 'KEC1004', 2016, '0', '3'),
(254, 1, 5, 'KEC1005', 2014, '0', '3'),
(255, 1, 5, 'KEC1005', 2015, '0', '2'),
(256, 1, 5, 'KEC1005', 2016, '0', '2'),
(257, 2, 5, 'KEC1001', 2014, '114', '0'),
(258, 2, 5, 'KEC1001', 2015, '130', '0'),
(259, 2, 5, 'KEC1001', 2016, '141', '0'),
(260, 2, 5, 'KEC1002', 2014, '0', '263'),
(261, 2, 5, 'KEC1002', 2015, '0', '241'),
(262, 2, 5, 'KEC1002', 2016, '0', '217'),
(263, 2, 5, 'KEC1003', 2014, '0', '39'),
(264, 2, 5, 'KEC1003', 2015, '0', '40'),
(265, 2, 5, 'KEC1003', 2016, '0', '40'),
(266, 2, 5, 'KEC1004', 2014, '0', '68'),
(267, 2, 5, 'KEC1004', 2015, '0', '82'),
(268, 2, 5, 'KEC1004', 2016, '0', '76'),
(269, 2, 5, 'KEC1005', 2014, '0', '93'),
(270, 2, 5, 'KEC1005', 2015, '0', '61'),
(271, 2, 5, 'KEC1005', 2016, '0', '68'),
(272, 3, 5, 'KEC1001', 2014, '40', '0'),
(273, 3, 5, 'KEC1001', 2015, '7', '0'),
(274, 3, 5, 'KEC1001', 2016, '9', '0'),
(275, 3, 5, 'KEC1002', 2014, '0', '63'),
(276, 3, 5, 'KEC1002', 2015, '0', '38'),
(277, 3, 5, 'KEC1002', 2016, '0', '32'),
(278, 3, 5, 'KEC1003', 2014, '0', '24'),
(279, 3, 5, 'KEC1003', 2015, '0', '7'),
(280, 3, 5, 'KEC1003', 2016, '0', '6'),
(281, 3, 5, 'KEC1004', 2014, '0', '61'),
(282, 3, 5, 'KEC1004', 2015, '0', '22'),
(283, 3, 5, 'KEC1004', 2016, '0', '23'),
(284, 3, 5, 'KEC1005', 2014, '0', '63'),
(285, 3, 5, 'KEC1005', 2015, '0', '17'),
(286, 3, 5, 'KEC1005', 2016, '0', '19'),
(287, 4, 5, 'KEC1001', 2014, '2.85', '0'),
(288, 4, 5, 'KEC1001', 2015, '18.57', '0'),
(289, 4, 5, 'KEC1001', 2016, '15.67', '0'),
(290, 4, 5, 'KEC1002', 2014, '0', '4.17'),
(291, 4, 5, 'KEC1002', 2015, '0', '6.34'),
(292, 4, 5, 'KEC1002', 2016, '0', '6.78'),
(293, 4, 5, 'KEC1003', 2014, '0', '1.63'),
(294, 4, 5, 'KEC1003', 2015, '0', '5.71'),
(295, 4, 5, 'KEC1003', 2016, '0', '6.67'),
(296, 4, 5, 'KEC1004', 2014, '0', '1.11'),
(297, 4, 5, 'KEC1004', 2015, '0', '3.73'),
(298, 4, 5, 'KEC1004', 2016, '0', '3.30'),
(299, 4, 5, 'KEC1005', 2014, '0', '1.48'),
(300, 4, 5, 'KEC1005', 2015, '0', '3.59'),
(301, 4, 5, 'KEC1005', 2016, '0', '3.58'),
(302, 1, 6, 'KEC1001', 2014, '0', '22'),
(303, 1, 6, 'KEC1001', 2015, '0', '24'),
(304, 1, 6, 'KEC1001', 2016, '0', '21'),
(305, 1, 6, 'KEC1002', 2014, '1', '11'),
(306, 1, 6, 'KEC1002', 2015, '1', '11'),
(307, 1, 6, 'KEC1002', 2016, '1', '10'),
(308, 1, 6, 'KEC1003', 2014, '1', '5'),
(309, 1, 6, 'KEC1003', 2015, '1', '5'),
(310, 1, 6, 'KEC1003', 2016, '1', '4'),
(311, 1, 6, 'KEC1004', 2014, '0', '5'),
(312, 1, 6, 'KEC1004', 2015, '0', '5'),
(313, 1, 6, 'KEC1004', 2016, '0', '4'),
(314, 1, 6, 'KEC1005', 2014, '0', '4'),
(315, 1, 6, 'KEC1005', 2015, '0', '5'),
(316, 1, 6, 'KEC1005', 2016, '0', '2'),
(317, 2, 6, 'KEC1001', 2014, '0', '3497'),
(318, 2, 6, 'KEC1001', 2015, '0', '3639'),
(319, 2, 6, 'KEC1001', 2016, '0', '3164'),
(320, 2, 6, 'KEC1002', 2014, '529', '2000'),
(321, 2, 6, 'KEC1002', 2015, '569', '2063'),
(322, 2, 6, 'KEC1002', 2016, '730', '1898'),
(323, 2, 6, 'KEC1003', 2014, '1336', '1384'),
(324, 2, 6, 'KEC1003', 2015, '1457', '1438'),
(325, 2, 6, 'KEC1003', 2016, '1594', '1432'),
(326, 2, 6, 'KEC1004', 2014, '0', '1115'),
(327, 2, 6, 'KEC1004', 2015, '0', '1217'),
(328, 2, 6, 'KEC1004', 2016, '0', '1086'),
(329, 2, 6, 'KEC1005', 2014, '0', '461'),
(330, 2, 6, 'KEC1005', 2015, '0', '749'),
(331, 2, 6, 'KEC1005', 2016, '0', '102'),
(332, 3, 6, 'KEC1001', 2014, '0', '239'),
(333, 3, 6, 'KEC1001', 2015, '0', '246'),
(334, 3, 6, 'KEC1001', 2016, '0', '194'),
(335, 3, 6, 'KEC1002', 2014, '29', '122'),
(336, 3, 6, 'KEC1002', 2015, '32', '122'),
(337, 3, 6, 'KEC1002', 2016, '37', '107'),
(338, 3, 6, 'KEC1003', 2014, '63', '74'),
(339, 3, 6, 'KEC1003', 2015, '65', '69'),
(340, 3, 6, 'KEC1003', 2016, '67', '61'),
(341, 3, 6, 'KEC1004', 2014, '0', '52'),
(342, 3, 6, 'KEC1004', 2015, '0', '56'),
(343, 3, 6, 'KEC1004', 2016, '0', '46'),
(344, 3, 6, 'KEC1005', 2014, '0', '34'),
(345, 3, 6, 'KEC1005', 2015, '0', '49'),
(346, 3, 6, 'KEC1005', 2016, '0', '11'),
(347, 4, 6, 'KEC1001', 2014, '0', '14.63'),
(348, 4, 6, 'KEC1001', 2015, '0', '14.79'),
(349, 4, 6, 'KEC1001', 2016, '0', '16.31'),
(350, 4, 6, 'KEC1002', 2014, '18.24', '16.39'),
(351, 4, 6, 'KEC1002', 2015, '17.78', '16.91'),
(352, 4, 6, 'KEC1002', 2016, '19.73', '17.74'),
(353, 4, 6, 'KEC1003', 2014, '21.21', '18.70'),
(354, 4, 6, 'KEC1003', 2015, '22.42', '20.84'),
(355, 4, 6, 'KEC1003', 2016, '23.79', '23.48'),
(356, 4, 6, 'KEC1004', 2014, '0', '21.44'),
(357, 4, 6, 'KEC1004', 2015, '0', '21.73'),
(358, 4, 6, 'KEC1004', 2016, '0', '23.61'),
(359, 4, 6, 'KEC1005', 2014, '0', '13.56'),
(360, 4, 6, 'KEC1005', 2015, '0', '15.29'),
(361, 4, 6, 'KEC1005', 2016, '0', '9.27'),
(362, 1, 7, 'KEC1001', 2014, '6', '9'),
(363, 1, 7, 'KEC1001', 2015, '6', '10'),
(364, 1, 7, 'KEC1001', 2016, '6', '11'),
(365, 1, 7, 'KEC1002', 2014, '4', '12'),
(366, 1, 7, 'KEC1002', 2015, '4', '12'),
(367, 1, 7, 'KEC1002', 2016, '4', '12'),
(368, 1, 7, 'KEC1003', 2014, '9', '16'),
(369, 1, 7, 'KEC1003', 2015, '9', '17'),
(370, 1, 7, 'KEC1003', 2016, '9', '16'),
(371, 1, 7, 'KEC1004', 2014, '6', '14'),
(372, 1, 7, 'KEC1004', 2015, '6', '16'),
(373, 1, 7, 'KEC1004', 2016, '6', '15'),
(374, 1, 7, 'KEC1005', 2014, '7', '14'),
(375, 1, 7, 'KEC1005', 2015, '7', '14'),
(376, 1, 7, 'KEC1005', 2016, '7', '14'),
(377, 2, 7, 'KEC1001', 2014, '4164', '1183'),
(378, 2, 7, 'KEC1001', 2015, '4298', '1449'),
(379, 2, 7, 'KEC1001', 2016, '4408', '1425'),
(380, 2, 7, 'KEC1002', 2014, '2745', '2735'),
(381, 2, 7, 'KEC1002', 2015, '2484', '2692'),
(382, 2, 7, 'KEC1002', 2016, '2534', '2622'),
(383, 2, 7, 'KEC1003', 2014, '7390', '4029'),
(384, 2, 7, 'KEC1003', 2015, '7226', '4241'),
(385, 2, 7, 'KEC1003', 2016, '7262', '3827'),
(386, 2, 7, 'KEC1004', 2014, '3667', '3202'),
(387, 2, 7, 'KEC1004', 2015, '3515', '3577'),
(388, 2, 7, 'KEC1004', 2016, '3592', '3381'),
(389, 2, 7, 'KEC1005', 2014, '4368', '2981'),
(390, 2, 7, 'KEC1005', 2015, '4004', '3292'),
(391, 2, 7, 'KEC1005', 2016, '4673', '3173'),
(392, 3, 7, 'KEC1001', 2014, '242', '138'),
(393, 3, 7, 'KEC1001', 2015, '248', '133'),
(394, 3, 7, 'KEC1001', 2016, '259', '148'),
(395, 3, 7, 'KEC1002', 2014, '144', '229'),
(396, 3, 7, 'KEC1002', 2015, '158', '228'),
(397, 3, 7, 'KEC1002', 2016, '159', '222'),
(398, 3, 7, 'KEC1003', 2014, '423', '288'),
(399, 3, 7, 'KEC1003', 2015, '438', '315'),
(400, 3, 7, 'KEC1003', 2016, '438', '300'),
(401, 3, 7, 'KEC1004', 2014, '221', '225'),
(402, 3, 7, 'KEC1004', 2015, '240', '267'),
(403, 3, 7, 'KEC1004', 2016, '246', '245'),
(404, 3, 7, 'KEC1005', 2014, '265', '269'),
(405, 3, 7, 'KEC1005', 2015, '238', '264'),
(406, 3, 7, 'KEC1005', 2016, '290', '254'),
(407, 4, 7, 'KEC1001', 2014, '17.21', '8.57'),
(408, 4, 7, 'KEC1001', 2015, '17.33', '10.89'),
(409, 4, 7, 'KEC1001', 2016, '17.02', '9.63'),
(410, 4, 7, 'KEC1002', 2014, '19.06', '11.94'),
(411, 4, 7, 'KEC1002', 2015, '15.72', '11.81'),
(412, 4, 7, 'KEC1002', 2016, '15.94', '11.81'),
(413, 4, 7, 'KEC1003', 2014, '17.47', '13.99'),
(414, 4, 7, 'KEC1003', 2015, '16.50', '13.46'),
(415, 4, 7, 'KEC1003', 2016, '16.58', '12.76'),
(416, 4, 7, 'KEC1004', 2014, '16.59', '14.23'),
(417, 4, 7, 'KEC1004', 2015, '14.65', '13.40'),
(418, 4, 7, 'KEC1004', 2016, '14.60', '13.80'),
(419, 4, 7, 'KEC1005', 2014, '16.48', '11.08'),
(420, 4, 7, 'KEC1005', 2015, '16.82', '12.47'),
(421, 4, 7, 'KEC1005', 2016, '16.11', '12.49'),
(422, 1, 8, 'KEC1001', 2014, '1', '0'),
(423, 1, 8, 'KEC1001', 2015, '1', '0'),
(424, 1, 8, 'KEC1001', 2016, '1', '0'),
(425, 1, 8, 'KEC1002', 2014, '0', '1'),
(426, 1, 8, 'KEC1002', 2015, '0', '1'),
(427, 1, 8, 'KEC1002', 2016, '0', '1'),
(428, 1, 8, 'KEC1003', 2014, '0', '1'),
(429, 1, 8, 'KEC1003', 2015, '0', '1'),
(430, 1, 8, 'KEC1003', 2016, '0', '1'),
(431, 1, 8, 'KEC1004', 2014, '0', '3'),
(432, 1, 8, 'KEC1004', 2015, '0', '3'),
(433, 1, 8, 'KEC1004', 2016, '0', '3'),
(434, 1, 8, 'KEC1005', 2014, '0', '2'),
(435, 1, 8, 'KEC1005', 2015, '0', '2'),
(436, 1, 8, 'KEC1005', 2016, '0', '2'),
(437, 2, 8, 'KEC1001', 2014, '48', '0'),
(438, 2, 8, 'KEC1001', 2015, '57', '0'),
(439, 2, 8, 'KEC1001', 2016, '57', '0'),
(440, 2, 8, 'KEC1002', 2014, '0', '102'),
(441, 2, 8, 'KEC1002', 2015, '0', '94'),
(442, 2, 8, 'KEC1002', 2016, '0', '101'),
(443, 2, 8, 'KEC1003', 2014, '0', '16'),
(444, 2, 8, 'KEC1003', 2015, '0', '17'),
(445, 2, 8, 'KEC1003', 2016, '0', '22'),
(446, 2, 8, 'KEC1004', 2014, '0', '49'),
(447, 2, 8, 'KEC1004', 2015, '0', '30'),
(448, 2, 8, 'KEC1004', 2016, '0', '25'),
(449, 2, 8, 'KEC1005', 2014, '0', '34'),
(450, 2, 8, 'KEC1005', 2015, '0', '32'),
(451, 2, 8, 'KEC1005', 2016, '0', '23'),
(452, 3, 8, 'KEC1001', 2014, '14', '0'),
(453, 3, 8, 'KEC1001', 2015, '14', '0'),
(454, 3, 8, 'KEC1001', 2016, '14', '0'),
(455, 3, 8, 'KEC1002', 2014, '0', '59'),
(456, 3, 8, 'KEC1002', 2015, '0', '14'),
(457, 3, 8, 'KEC1002', 2016, '0', '14'),
(458, 3, 8, 'KEC1003', 2014, '0', '14'),
(459, 3, 8, 'KEC1003', 2015, '0', '6'),
(460, 3, 8, 'KEC1003', 2016, '0', '5'),
(461, 3, 8, 'KEC1004', 2014, '0', '32'),
(462, 3, 8, 'KEC1004', 2015, '0', '14'),
(463, 3, 8, 'KEC1004', 2016, '0', '12'),
(464, 3, 8, 'KEC1005', 2014, '0', '28'),
(465, 3, 8, 'KEC1005', 2015, '0', '9'),
(466, 3, 8, 'KEC1005', 2016, '0', '8'),
(467, 4, 8, 'KEC1001', 2014, '3.43', '0'),
(468, 4, 8, 'KEC1001', 2015, '4.07', '0'),
(469, 4, 8, 'KEC1001', 2016, '4.07', '0'),
(470, 4, 8, 'KEC1002', 2014, '0', '1.73'),
(471, 4, 8, 'KEC1002', 2015, '0', '6.71'),
(472, 4, 8, 'KEC1002', 2016, '0', '7.21'),
(473, 4, 8, 'KEC1003', 2014, '0', '1.14'),
(474, 4, 8, 'KEC1003', 2015, '0', '2.83'),
(475, 4, 8, 'KEC1003', 2016, '0', '4.40'),
(476, 4, 8, 'KEC1004', 2014, '0', '1.53'),
(477, 4, 8, 'KEC1004', 2015, '0', '2.14'),
(478, 4, 8, 'KEC1004', 2016, '0', '2.08'),
(479, 4, 8, 'KEC1005', 2014, '0', '1.21'),
(480, 4, 8, 'KEC1005', 2015, '0', '3.56'),
(481, 4, 8, 'KEC1005', 2016, '0', '2.88'),
(482, 1, 9, 'KEC1001', 2014, '1', '11'),
(483, 1, 9, 'KEC1001', 2015, '1', '13'),
(484, 1, 9, 'KEC1001', 2016, '1', '13'),
(485, 1, 9, 'KEC1002', 2014, '0', '2'),
(486, 1, 9, 'KEC1002', 2015, '0', '3'),
(487, 1, 9, 'KEC1002', 2016, '0', '4'),
(488, 1, 9, 'KEC1003', 2014, '1', '4'),
(489, 1, 9, 'KEC1003', 2015, '1', '4'),
(490, 1, 9, 'KEC1003', 2016, '1', '5'),
(491, 1, 9, 'KEC1004', 2014, '0', '2'),
(492, 1, 9, 'KEC1004', 2015, '0', '2'),
(493, 1, 9, 'KEC1004', 2016, '0', '2'),
(494, 1, 9, 'KEC1005', 2014, '0', '5'),
(495, 1, 9, 'KEC1005', 2015, '0', '6'),
(496, 1, 9, 'KEC1005', 2016, '0', '8'),
(497, 2, 9, 'KEC1001', 2014, '535', '1004'),
(498, 2, 9, 'KEC1001', 2015, '539', '1138'),
(499, 2, 9, 'KEC1001', 2016, '468', '1130'),
(500, 2, 9, 'KEC1002', 2014, '0', '577'),
(501, 2, 9, 'KEC1002', 2015, '0', '738'),
(502, 2, 9, 'KEC1002', 2016, '0', '832'),
(503, 2, 9, 'KEC1003', 2014, '835', '804'),
(504, 2, 9, 'KEC1003', 2015, '893', '1134'),
(505, 2, 9, 'KEC1003', 2016, '955', '1431'),
(506, 2, 9, 'KEC1004', 2014, '0', '383'),
(507, 2, 9, 'KEC1004', 2015, '0', '542'),
(508, 2, 9, 'KEC1004', 2016, '0', '542'),
(509, 2, 9, 'KEC1005', 2014, '0', '1072'),
(510, 2, 9, 'KEC1005', 2015, '0', '1210'),
(511, 2, 9, 'KEC1005', 2016, '0', '1296'),
(512, 3, 9, 'KEC1001', 2014, '28', '158'),
(513, 3, 9, 'KEC1001', 2015, '34', '171'),
(514, 3, 9, 'KEC1001', 2016, '35', '165'),
(515, 3, 9, 'KEC1002', 2014, '0', '51'),
(516, 3, 9, 'KEC1002', 2015, '0', '57'),
(517, 3, 9, 'KEC1002', 2016, '0', '70'),
(518, 3, 9, 'KEC1003', 2014, '57', '78'),
(519, 3, 9, 'KEC1003', 2015, '59', '100'),
(520, 3, 9, 'KEC1003', 2016, '65', '105'),
(521, 3, 9, 'KEC1004', 2014, '0', '29'),
(522, 3, 9, 'KEC1004', 2015, '0', '32'),
(523, 3, 9, 'KEC1004', 2016, '0', '32'),
(524, 3, 9, 'KEC1005', 2014, '0', '88'),
(525, 3, 9, 'KEC1005', 2015, '0', '108'),
(526, 3, 9, 'KEC1005', 2016, '0', '134'),
(527, 4, 9, 'KEC1001', 2014, '19.11', '6.35'),
(528, 4, 9, 'KEC1001', 2015, '15.85', '6.65'),
(529, 4, 9, 'KEC1001', 2016, '13.37', '6.85'),
(530, 4, 9, 'KEC1002', 2014, '0', '11.31'),
(531, 4, 9, 'KEC1002', 2015, '0', '12.95'),
(532, 4, 9, 'KEC1002', 2016, '0', '11.89'),
(533, 4, 9, 'KEC1003', 2014, '14.65', '10.31'),
(534, 4, 9, 'KEC1003', 2015, '15.14', '11.34'),
(535, 4, 9, 'KEC1003', 2016, '14.69', '13.63'),
(536, 4, 9, 'KEC1004', 2014, '0', '13.21'),
(537, 4, 9, 'KEC1004', 2015, '0', '16.94'),
(538, 4, 9, 'KEC1004', 2016, '0', '16.94'),
(539, 4, 9, 'KEC1005', 2014, '0', '12.18'),
(540, 4, 9, 'KEC1005', 2015, '0', '11.20'),
(541, 4, 9, 'KEC1005', 2016, '0', '9.67'),
(542, 1, 10, 'KEC1001', 2014, '2', '3'),
(543, 1, 10, 'KEC1001', 2015, '2', '4'),
(544, 1, 10, 'KEC1001', 2016, '2', '4'),
(545, 1, 10, 'KEC1002', 2014, '0', '6'),
(546, 1, 10, 'KEC1002', 2015, '0', '5'),
(547, 1, 10, 'KEC1002', 2016, '0', '5'),
(548, 1, 10, 'KEC1003', 2014, '5', '7'),
(549, 1, 10, 'KEC1003', 2015, '5', '13'),
(550, 1, 10, 'KEC1003', 2016, '5', '13'),
(551, 1, 10, 'KEC1004', 2014, '0', '5'),
(552, 1, 10, 'KEC1004', 2015, '0', '6'),
(553, 1, 10, 'KEC1004', 2016, '0', '5'),
(554, 1, 10, 'KEC1005', 2014, '3', '7'),
(555, 1, 10, 'KEC1005', 2015, '3', '9'),
(556, 1, 10, 'KEC1005', 2016, '3', '8'),
(557, 2, 10, 'KEC1001', 2014, '1762', '144'),
(558, 2, 10, 'KEC1001', 2015, '1765', '291'),
(559, 2, 10, 'KEC1001', 2016, '1944', '274'),
(560, 2, 10, 'KEC1002', 2014, '0', '449'),
(561, 2, 10, 'KEC1002', 2015, '0', '544'),
(562, 2, 10, 'KEC1002', 2016, '0', '693'),
(563, 2, 10, 'KEC1003', 2014, '4418', '5255'),
(564, 2, 10, 'KEC1003', 2015, '4574', '5660'),
(565, 2, 10, 'KEC1003', 2016, '4679', '5728'),
(566, 2, 10, 'KEC1004', 2014, '0', '342'),
(567, 2, 10, 'KEC1004', 2015, '0', '319'),
(568, 2, 10, 'KEC1004', 2016, '0', '316'),
(569, 2, 10, 'KEC1005', 2014, '2909', '2016'),
(570, 2, 10, 'KEC1005', 2015, '2941', '2626'),
(571, 2, 10, 'KEC1005', 2016, '2995', '2749'),
(572, 3, 10, 'KEC1001', 2014, '131', '44'),
(573, 3, 10, 'KEC1001', 2015, '126', '58'),
(574, 3, 10, 'KEC1001', 2016, '125', '53'),
(575, 3, 10, 'KEC1002', 2014, '0', '104'),
(576, 3, 10, 'KEC1002', 2015, '0', '99'),
(577, 3, 10, 'KEC1002', 2016, '0', '95'),
(578, 3, 10, 'KEC1003', 2014, '338', '403'),
(579, 3, 10, 'KEC1003', 2015, '337', '432'),
(580, 3, 10, 'KEC1003', 2016, '341', '420'),
(581, 3, 10, 'KEC1004', 2014, '0', '86'),
(582, 3, 10, 'KEC1004', 2015, '0', '101'),
(583, 3, 10, 'KEC1004', 2016, '0', '81'),
(584, 3, 10, 'KEC1005', 2014, '206', '187'),
(585, 3, 10, 'KEC1005', 2015, '213', '258'),
(586, 3, 10, 'KEC1005', 2016, '209', '251'),
(587, 4, 10, 'KEC1001', 2014, '13.45', '3.27'),
(588, 4, 10, 'KEC1001', 2015, '14.01', '5.02'),
(589, 4, 10, 'KEC1001', 2016, '15.55', '5.17'),
(590, 4, 10, 'KEC1002', 2014, '0', '4.32'),
(591, 4, 10, 'KEC1002', 2015, '0', '5.49'),
(592, 4, 10, 'KEC1002', 2016, '0', '7.29'),
(593, 4, 10, 'KEC1003', 2014, '13.07', '13.04'),
(594, 4, 10, 'KEC1003', 2015, '13.57', '13.10'),
(595, 4, 10, 'KEC1003', 2016, '13.72', '13.64'),
(596, 4, 10, 'KEC1004', 2014, '0', '3.98'),
(597, 4, 10, 'KEC1004', 2015, '0', '3.16'),
(598, 4, 10, 'KEC1004', 2016, '0', '3.90'),
(599, 4, 10, 'KEC1005', 2014, '14.12', '10.78'),
(600, 4, 10, 'KEC1005', 2015, '13.81', '10.18'),
(601, 4, 10, 'KEC1005', 2016, '14.33', '10.95'),
(602, 1, 11, 'KEC1001', 2014, '0', '1'),
(603, 1, 11, 'KEC1001', 2015, '0', '1'),
(604, 1, 11, 'KEC1001', 2016, '0', '1'),
(605, 1, 11, 'KEC1002', 2014, '0', '1'),
(606, 1, 11, 'KEC1002', 2015, '0', '1'),
(607, 1, 11, 'KEC1002', 2016, '0', '1'),
(608, 1, 11, 'KEC1003', 2014, '0', '1'),
(609, 1, 11, 'KEC1003', 2015, '0', '1'),
(610, 1, 11, 'KEC1003', 2016, '0', '1'),
(611, 1, 11, 'KEC1004', 2014, '0', '2'),
(612, 1, 11, 'KEC1004', 2015, '0', '2'),
(613, 1, 11, 'KEC1004', 2016, '0', '1'),
(614, 1, 11, 'KEC1005', 2014, '0', '0'),
(615, 1, 11, 'KEC1005', 2015, '0', '1'),
(616, 1, 11, 'KEC1005', 2016, '0', '1'),
(617, 2, 11, 'KEC1001', 2014, '0', '30'),
(618, 2, 11, 'KEC1001', 2015, '0', '40'),
(619, 2, 11, 'KEC1001', 2016, '0', '41'),
(620, 2, 11, 'KEC1002', 2014, '0', '59'),
(621, 2, 11, 'KEC1002', 2015, '0', '63'),
(622, 2, 11, 'KEC1002', 2016, '0', '71'),
(623, 2, 11, 'KEC1003', 2014, '0', '19'),
(624, 2, 11, 'KEC1003', 2015, '0', '16'),
(625, 2, 11, 'KEC1003', 2016, '0', '14'),
(626, 2, 11, 'KEC1004', 2014, '0', '21'),
(627, 2, 11, 'KEC1004', 2015, '0', '18'),
(628, 2, 11, 'KEC1004', 2016, '0', '11'),
(629, 2, 11, 'KEC1005', 2014, '0', '11'),
(630, 2, 11, 'KEC1005', 2015, '0', '10'),
(631, 2, 11, 'KEC1005', 2016, '0', '12'),
(632, 3, 11, 'KEC1001', 2014, '0', '12'),
(633, 3, 11, 'KEC1001', 2015, '0', '8'),
(634, 3, 11, 'KEC1001', 2016, '0', '9'),
(635, 3, 11, 'KEC1002', 2014, '0', '19'),
(636, 3, 11, 'KEC1002', 2015, '0', '9'),
(637, 3, 11, 'KEC1002', 2016, '0', '9'),
(638, 3, 11, 'KEC1003', 2014, '0', '8'),
(639, 3, 11, 'KEC1003', 2015, '0', '4'),
(640, 3, 11, 'KEC1003', 2016, '0', '5'),
(641, 3, 11, 'KEC1004', 2014, '0', '11'),
(642, 3, 11, 'KEC1004', 2015, '0', '6'),
(643, 3, 11, 'KEC1004', 2016, '0', '3'),
(644, 3, 11, 'KEC1005', 2014, '0', '7'),
(645, 3, 11, 'KEC1005', 2015, '0', '4'),
(646, 3, 11, 'KEC1005', 2016, '0', '4'),
(647, 4, 11, 'KEC1001', 2014, '0', '2.50'),
(648, 4, 11, 'KEC1001', 2015, '0', '5.00'),
(649, 4, 11, 'KEC1001', 2016, '0', '4.56'),
(650, 4, 11, 'KEC1002', 2014, '0', '3.11'),
(651, 4, 11, 'KEC1002', 2015, '0', '7.00'),
(652, 4, 11, 'KEC1002', 2016, '0', '7.89'),
(653, 4, 11, 'KEC1003', 2014, '0', '2.38'),
(654, 4, 11, 'KEC1003', 2015, '0', '4.00'),
(655, 4, 11, 'KEC1003', 2016, '0', '2.80'),
(656, 4, 11, 'KEC1004', 2014, '0', '1.91'),
(657, 4, 11, 'KEC1004', 2015, '0', '3.00'),
(658, 4, 11, 'KEC1004', 2016, '0', '3.67'),
(659, 4, 11, 'KEC1005', 2014, '0', '1.57'),
(660, 4, 11, 'KEC1005', 2015, '0', '2.50'),
(661, 4, 11, 'KEC1005', 2016, '0', '3.00'),
(662, 1, 12, 'KEC1001', 2014, '0', '5'),
(663, 1, 12, 'KEC1001', 2015, '0', '5'),
(664, 1, 12, 'KEC1001', 2016, '0', '5'),
(665, 1, 12, 'KEC1002', 2014, '0', '1'),
(666, 1, 12, 'KEC1002', 2015, '0', '1'),
(667, 1, 12, 'KEC1002', 2016, '0', '1'),
(668, 1, 12, 'KEC1003', 2014, '1', '3'),
(669, 1, 12, 'KEC1003', 2015, '1', '3'),
(670, 1, 12, 'KEC1003', 2016, '1', '3'),
(671, 1, 12, 'KEC1004', 2014, '0', '1'),
(672, 1, 12, 'KEC1004', 2015, '0', '1'),
(673, 1, 12, 'KEC1004', 2016, '0', '1'),
(674, 1, 12, 'KEC1005', 2014, '1', '3'),
(675, 1, 12, 'KEC1005', 2015, '1', '3'),
(676, 1, 12, 'KEC1005', 2016, '1', '3'),
(677, 2, 12, 'KEC1001', 2014, '0', '341'),
(678, 2, 12, 'KEC1001', 2015, '0', '366'),
(679, 2, 12, 'KEC1001', 2016, '0', '329'),
(680, 2, 12, 'KEC1002', 2014, '0', '300'),
(681, 2, 12, 'KEC1002', 2015, '0', '318'),
(682, 2, 12, 'KEC1002', 2016, '0', '286'),
(683, 2, 12, 'KEC1003', 2014, '720', '180'),
(684, 2, 12, 'KEC1003', 2015, '748', '209'),
(685, 2, 12, 'KEC1003', 2016, '748', '227'),
(686, 2, 12, 'KEC1004', 2014, '0', '24'),
(687, 2, 12, 'KEC1004', 2015, '0', '23'),
(688, 2, 12, 'KEC1004', 2016, '0', '21'),
(689, 2, 12, 'KEC1005', 2014, '840', '350'),
(690, 2, 12, 'KEC1005', 2015, '886', '340'),
(691, 2, 12, 'KEC1005', 2016, '886', '344'),
(692, 3, 12, 'KEC1001', 2014, '0', '89'),
(693, 3, 12, 'KEC1001', 2015, '0', '73'),
(694, 3, 12, 'KEC1001', 2016, '0', '75'),
(695, 3, 12, 'KEC1002', 2014, '0', '21'),
(696, 3, 12, 'KEC1002', 2015, '0', '16'),
(697, 3, 12, 'KEC1002', 2016, '0', '16'),
(698, 3, 12, 'KEC1003', 2014, '68', '49'),
(699, 3, 12, 'KEC1003', 2015, '68', '63'),
(700, 3, 12, 'KEC1003', 2016, '67', '63'),
(701, 3, 12, 'KEC1004', 2014, '0', '13'),
(702, 3, 12, 'KEC1004', 2015, '0', '15'),
(703, 3, 12, 'KEC1004', 2016, '0', '4'),
(704, 3, 12, 'KEC1005', 2014, '63', '55'),
(705, 3, 12, 'KEC1005', 2015, '64', '57'),
(706, 3, 12, 'KEC1005', 2016, '64', '57'),
(707, 4, 12, 'KEC1001', 2014, '0', '3.83'),
(708, 4, 12, 'KEC1001', 2015, '0', '5.01'),
(709, 4, 12, 'KEC1001', 2016, '0', '4.39'),
(710, 4, 12, 'KEC1002', 2014, '0', '14.29'),
(711, 4, 12, 'KEC1002', 2015, '0', '19.88'),
(712, 4, 12, 'KEC1002', 2016, '0', '17.88'),
(713, 4, 12, 'KEC1003', 2014, '10.59', '3.67'),
(714, 4, 12, 'KEC1003', 2015, '11.00', '3.32'),
(715, 4, 12, 'KEC1003', 2016, '11.16', '3.60'),
(716, 4, 12, 'KEC1004', 2014, '0', '1.85'),
(717, 4, 12, 'KEC1004', 2015, '0', '1.53'),
(718, 4, 12, 'KEC1004', 2016, '0', '5.25'),
(719, 4, 12, 'KEC1005', 2014, '13.33', '6.36'),
(720, 4, 12, 'KEC1005', 2015, '13.84', '5.96'),
(721, 4, 12, 'KEC1005', 2016, '13.84', '6.04'),
(722, 1, 13, 'KEC1001', 2014, '3', '5'),
(723, 1, 13, 'KEC1001', 2015, '3', '5'),
(724, 1, 13, 'KEC1001', 2016, '3', '5'),
(725, 1, 13, 'KEC1002', 2014, '3', '7'),
(726, 1, 13, 'KEC1002', 2015, '3', '8'),
(727, 1, 13, 'KEC1002', 2016, '3', '9'),
(728, 1, 13, 'KEC1003', 2014, '2', '9'),
(729, 1, 13, 'KEC1003', 2015, '2', '9'),
(730, 1, 13, 'KEC1003', 2016, '2', '9'),
(731, 1, 13, 'KEC1004', 2014, '2', '7'),
(732, 1, 13, 'KEC1004', 2015, '2', '8'),
(733, 1, 13, 'KEC1004', 2016, '2', '9'),
(734, 1, 13, 'KEC1005', 2014, '3', '10'),
(735, 1, 13, 'KEC1005', 2015, '3', '10'),
(736, 1, 13, 'KEC1005', 2016, '3', '10'),
(737, 2, 13, 'KEC1001', 2014, '4996', '1253'),
(738, 2, 13, 'KEC1001', 2015, '4697', '1356'),
(739, 2, 13, 'KEC1001', 2016, '4820', '1440'),
(740, 2, 13, 'KEC1002', 2014, '3743', '1986'),
(741, 2, 13, 'KEC1002', 2015, '3735', '2045'),
(742, 2, 13, 'KEC1002', 2016, '4252', '2029'),
(743, 2, 13, 'KEC1003', 2014, '5736', '2133'),
(744, 2, 13, 'KEC1003', 2015, '3894', '1910'),
(745, 2, 13, 'KEC1003', 2016, '4220', '1841'),
(746, 2, 13, 'KEC1004', 2014, '1800', '1739'),
(747, 2, 13, 'KEC1004', 2015, '2206', '1785'),
(748, 2, 13, 'KEC1004', 2016, '2610', '2283'),
(749, 2, 13, 'KEC1005', 2014, '4039', '4098'),
(750, 2, 13, 'KEC1005', 2015, '4082', '3726'),
(751, 2, 13, 'KEC1005', 2016, '4244', '3666'),
(752, 3, 13, 'KEC1001', 2014, '299', '118'),
(753, 3, 13, 'KEC1001', 2015, '229', '129'),
(754, 3, 13, 'KEC1001', 2016, '299', '128'),
(755, 3, 13, 'KEC1002', 2014, '227', '241'),
(756, 3, 13, 'KEC1002', 2015, '229', '255'),
(757, 3, 13, 'KEC1002', 2016, '233', '257'),
(758, 3, 13, 'KEC1003', 2014, '246', '244'),
(759, 3, 13, 'KEC1003', 2015, '236', '230'),
(760, 3, 13, 'KEC1003', 2016, '239', '223'),
(761, 3, 13, 'KEC1004', 2014, '126', '190'),
(762, 3, 13, 'KEC1004', 2015, '132', '214'),
(763, 3, 13, 'KEC1004', 2016, '127', '280'),
(764, 3, 13, 'KEC1005', 2014, '251', '308'),
(765, 3, 13, 'KEC1005', 2015, '264', '289'),
(766, 3, 13, 'KEC1005', 2016, '266', '277'),
(767, 4, 13, 'KEC1001', 2014, '16.71', '10.62'),
(768, 4, 13, 'KEC1001', 2015, '20.51', '10.51'),
(769, 4, 13, 'KEC1001', 2016, '16.12', '11.25'),
(770, 4, 13, 'KEC1002', 2014, '16.49', '8.24'),
(771, 4, 13, 'KEC1002', 2015, '16.31', '8.02'),
(772, 4, 13, 'KEC1002', 2016, '18.25', '7.89'),
(773, 4, 13, 'KEC1003', 2014, '23.32', '8.74'),
(774, 4, 13, 'KEC1003', 2015, '16.50', '8.30'),
(775, 4, 13, 'KEC1003', 2016, '17.66', '8.26'),
(776, 4, 13, 'KEC1004', 2014, '14.29', '9.15'),
(777, 4, 13, 'KEC1004', 2015, '16.71', '8.34'),
(778, 4, 13, 'KEC1004', 2016, '20.55', '8.15'),
(779, 4, 13, 'KEC1005', 2014, '16.09', '13.31'),
(780, 4, 13, 'KEC1005', 2015, '15.46', '12.89'),
(781, 4, 13, 'KEC1005', 2016, '15.95', '13.23');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pendidikan_jenis`
--

CREATE TABLE `lp_pendidikan_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pendidikan_jenis`
--

INSERT INTO `lp_pendidikan_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Jumlah Sekolah'),
(2, 'Jumlah Murid'),
(3, 'Jumlah Guru'),
(4, 'Rasio Guru-Murid');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pendidikan_kategori`
--

CREATE TABLE `lp_pendidikan_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pendidikan_kategori`
--

INSERT INTO `lp_pendidikan_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Taman Kanak-Kanak'),
(2, 'Taman Kanak-Kanak Luar Biasa'),
(3, 'Roudlotul Athfal'),
(4, 'Sekolah Dasar'),
(5, 'Sekolah Dasar Luar Biasa'),
(6, 'Madrasah Ibtidaiyah'),
(7, 'Sekolah Menengah Pertama'),
(8, 'Sekolah Menengah Pertama Luar Biasa'),
(9, 'Madrasah Tsanawiyah'),
(10, 'Sekolah Menengah Atas'),
(11, 'Sekolah Menengah Atas Luar Biasa'),
(12, 'Madrasah Aliyah'),
(13, 'Sekolah Menengah Kejuruan');

-- --------------------------------------------------------

--
-- Table structure for table `lp_penduduk_jk`
--

CREATE TABLE `lp_penduduk_jk` (
  `id_penduduk` int(11) NOT NULL,
  `id_kec` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_l` varchar(20) NOT NULL,
  `jml_p` varchar(20) NOT NULL,
  `rasio` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_pend_industri`
--

CREATE TABLE `lp_pend_industri` (
  `id_pend` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pend_industri`
--

INSERT INTO `lp_pend_industri` (`id_pend`, `id_jenis`, `id_kategori`, `th`, `jml`) VALUES
(1, 1, 1, 2014, '5'),
(2, 1, 2, 2014, '3788'),
(3, 1, 3, 2014, '80777671000'),
(4, 2, 1, 2014, '667'),
(5, 2, 2, 2014, '20639'),
(6, 2, 3, 2014, '1994378637'),
(7, 3, 1, 2014, '594'),
(8, 3, 2, 2014, '2398'),
(9, 3, 3, 2014, '65613010333'),
(10, 4, 1, 2014, '1746'),
(11, 4, 2, 2014, '5908'),
(12, 4, 3, 2014, '5386354000'),
(13, 1, 1, 2015, '5'),
(14, 1, 2, 2015, '3788'),
(15, 1, 3, 2015, '80777671000'),
(16, 2, 1, 2015, '696'),
(17, 2, 2, 2015, '21318'),
(18, 2, 3, 2015, '3648152072'),
(19, 3, 1, 2015, '594'),
(20, 3, 2, 2015, '2398'),
(21, 3, 3, 2015, '65613010333'),
(22, 4, 1, 2015, '1746'),
(23, 4, 2, 2015, '5908'),
(24, 4, 3, 2015, '5386354000'),
(25, 1, 1, 2016, '16'),
(26, 1, 2, 2016, '8015'),
(27, 1, 3, 2016, '1277678638000'),
(28, 2, 1, 2016, '646'),
(29, 2, 2, 2016, '13302'),
(30, 2, 3, 2016, '255759234000'),
(31, 3, 1, 2016, '2413'),
(32, 3, 2, 2016, '5696'),
(33, 2, 3, 2016, '121005432900'),
(34, 4, 1, 2016, '1746'),
(35, 4, 2, 2016, '5908'),
(36, 4, 3, 2016, '5386354000');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pend_industri_jenis`
--

CREATE TABLE `lp_pend_industri_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pend_industri_jenis`
--

INSERT INTO `lp_pend_industri_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'industri besar (>10 Miliar)'),
(2, 'Industri Kecil Dan Menengah (750 Jt s/d 10 Miliar)'),
(3, 'Sentra Industri'),
(4, 'Industri Non Formal');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pend_industri_kategori`
--

CREATE TABLE `lp_pend_industri_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pend_industri_kategori`
--

INSERT INTO `lp_pend_industri_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'jumlah unit usaha (unit)'),
(2, 'Jumlah Tenaga Kerja (Orang)'),
(3, 'Jumlah Nilai Investasi (Rp.)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_perpustakaan`
--

CREATE TABLE `lp_perpustakaan` (
  `id_perpustakaan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_perpustakaan`
--

INSERT INTO `lp_perpustakaan` (`id_perpustakaan`, `id_jenis`, `th`, `jml`) VALUES
(1, 1, 2014, '254946'),
(2, 1, 2015, '227132'),
(3, 1, 2016, '226073'),
(4, 2, 2014, '156273'),
(5, 2, 2015, '164568'),
(6, 2, 2016, '175699'),
(7, 3, 2014, '70'),
(8, 3, 2015, '370'),
(9, 3, 2016, '370'),
(10, 4, 2014, '100'),
(11, 4, 2015, '100'),
(12, 4, 2016, '100'),
(13, 5, 2014, '4'),
(14, 5, 2015, '6'),
(15, 5, 2016, '6'),
(16, 6, 2014, '1'),
(17, 6, 2015, '1'),
(18, 6, 2016, '1');

-- --------------------------------------------------------

--
-- Table structure for table `lp_perpustakaan_jenis`
--

CREATE TABLE `lp_perpustakaan_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_perpustakaan_jenis`
--

INSERT INTO `lp_perpustakaan_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Jumlah Pengunjung Perpustakaan (Orang)'),
(2, 'jumlah penyediaan bahan perpustakaan (eksemplar)'),
(3, 'jumlah pembinaan masyarakat dan lembaga pendidikan binaan (perpustakaan)'),
(4, 'jumlah pelayanan perpustakaan keliling (binaan)'),
(5, 'jumlah sarana ruang baca perpustakaan (jenis)'),
(6, 'jumlah pengendalian hama/fumigasi (kali)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pertanian`
--

CREATE TABLE `lp_pertanian` (
  `id_pertanian` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `luas` varchar(20) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pertanian`
--

INSERT INTO `lp_pertanian` (`id_pertanian`, `id_sub_jenis`, `th`, `luas`, `jml`) VALUES
(1, 1, 2014, '0', '0'),
(2, 1, 2015, '0', '0'),
(3, 1, 2016, '0', '0'),
(4, 2, 2014, '0', '0'),
(5, 2, 2015, '1', '62'),
(6, 2, 2016, '0', '0'),
(7, 3, 2014, '0', '0'),
(8, 3, 2015, '0', '0'),
(9, 3, 2016, '0', '0'),
(10, 4, 2014, '1', '18'),
(11, 4, 2015, '0', '0'),
(12, 4, 2016, '0', '0'),
(13, 5, 2014, '0', '0'),
(14, 5, 2015, '0', '0'),
(15, 5, 2016, '0', '0'),
(16, 6, 2014, '1', '30'),
(17, 6, 2015, '0', '0'),
(18, 6, 2016, '0', '0'),
(19, 7, 2014, '10', '589'),
(20, 7, 2015, '34', '1596'),
(21, 7, 2016, '27', '278'),
(22, 8, 2014, '25', '646'),
(23, 8, 2015, '18', '1125'),
(24, 8, 2016, '11', '44'),
(25, 9, 2014, '10280', '417920'),
(26, 9, 2015, '85700', '1896600'),
(27, 9, 2016, '29600', '3547120'),
(28, 10, 2014, '0', '0'),
(29, 10, 2015, '0', '0'),
(30, 10, 2016, '0', '0'),
(31, 11, 2014, '7', '295'),
(32, 11, 2015, '2', '40'),
(33, 11, 2016, '1', '50'),
(34, 12, 2014, '4', '60'),
(35, 12, 2015, '0', '0'),
(36, 12, 2016, '0', '0'),
(37, 13, 2014, '0', '0'),
(38, 13, 2015, '0', '0'),
(39, 13, 2016, '4', '8'),
(40, 14, 2014, '0', '0'),
(41, 14, 2015, '0', '0'),
(42, 14, 2016, '0', '0'),
(43, 15, 2014, '4', '240'),
(44, 15, 2015, '1', '60'),
(45, 15, 2016, '1', '2'),
(46, 16, 2014, '0', '0'),
(47, 16, 2015, '0', '0'),
(48, 16, 2016, '0', '0'),
(49, 17, 2014, '1', '72'),
(50, 17, 2015, '0', '0'),
(51, 17, 2016, '0', '0'),
(52, 18, 2014, '0', '0'),
(53, 18, 2015, '0', '0'),
(54, 18, 2016, '0', '0'),
(55, 19, 2014, '0', '0'),
(56, 19, 2015, '0', '0'),
(57, 19, 2016, '0', '0'),
(58, 20, 2014, '150', '1875'),
(59, 20, 2015, '0', '0'),
(60, 20, 2016, '0', '0'),
(61, 21, 2014, '7', '155'),
(62, 21, 2015, '7', '320'),
(63, 21, 2016, '2', '42'),
(64, 22, 2014, '0', '0'),
(65, 22, 2015, '0', '0'),
(66, 22, 2016, '0', '0'),
(67, 23, 2014, '0', '0'),
(68, 23, 2015, '0', '0'),
(69, 23, 2016, '0', '0'),
(70, 24, 2014, '1', '46'),
(71, 24, 2015, '4', '140'),
(72, 24, 2016, '6', '20'),
(73, 25, 2014, '4', '352'),
(74, 25, 2015, '7', '227'),
(75, 25, 2016, '12', '82'),
(76, 26, 2014, '0', '0'),
(77, 26, 2015, '0', '0'),
(78, 26, 2016, '0', '0'),
(79, 27, 2014, '280', '251'),
(80, 27, 2015, '425', '247'),
(81, 27, 2016, '350', '200'),
(82, 28, 2014, '93', '66'),
(83, 28, 2015, '112', '77'),
(84, 28, 2016, '80', '55'),
(85, 29, 2014, '0', '0'),
(86, 29, 2015, '0', '0'),
(87, 29, 2016, '0', '0'),
(88, 30, 2014, '1470', '981'),
(89, 30, 2015, '1335', '821'),
(90, 30, 2016, '1665', '836'),
(91, 31, 2014, '53', '53'),
(92, 31, 2015, '60', '52'),
(93, 31, 2016, '65', '50'),
(94, 32, 2014, '370', '464'),
(95, 32, 2015, '385', '466'),
(96, 32, 2016, '510', '467'),
(97, 33, 2014, '609', '405'),
(98, 33, 2015, '619', '412'),
(99, 33, 2016, '714', '323'),
(100, 34, 2014, '1765', '920'),
(101, 34, 2015, '1933', '915'),
(102, 34, 2016, '1480', '815'),
(103, 35, 2014, '0', '0'),
(104, 35, 2015, '0', '0'),
(105, 35, 2016, '0', '0'),
(106, 36, 2014, '120', '221'),
(107, 36, 2015, '160', '200'),
(108, 36, 2016, '200', '200'),
(109, 37, 2014, '5085', '5888'),
(110, 37, 2015, '5060', '5875'),
(111, 37, 2016, '7520', '5429'),
(112, 38, 2014, '10442', '11315'),
(113, 38, 2015, '10241', '11459'),
(114, 38, 2016, '9871', '10137'),
(115, 39, 2014, '0', '0'),
(116, 39, 2015, '0', '0'),
(117, 39, 2016, '0', '0'),
(118, 40, 2014, '50', '5'),
(119, 40, 2015, '140', '49'),
(120, 40, 2016, '160', '65'),
(121, 41, 2014, '1256', '488'),
(122, 41, 2015, '1248', '487'),
(123, 41, 2016, '1300', '475'),
(124, 42, 2014, '1230', '1530'),
(125, 42, 2015, '1255', '1523'),
(126, 42, 2016, '1105', '1397'),
(127, 43, 2014, '0', '0'),
(128, 43, 2015, '0', '0'),
(129, 43, 2016, '0', '0'),
(130, 44, 2014, '1890', '1560'),
(131, 44, 2015, '1720', '1454'),
(132, 44, 2016, '1770', '1853'),
(133, 45, 2014, '788', '358'),
(134, 45, 2015, '1035', '411'),
(135, 45, 2016, '1165', '536'),
(136, 46, 2014, '5307', '2133'),
(137, 46, 2015, '5526', '2133'),
(138, 46, 2016, '5728', '2121'),
(139, 47, 2014, '1776', '860'),
(140, 47, 2015, '1775', '872'),
(141, 47, 2016, '2170', '877'),
(142, 48, 2014, '8290', '2651'),
(143, 48, 2015, '8085', '2466'),
(144, 48, 2016, '8088', '2310'),
(145, 49, 2014, '226', '135'),
(146, 49, 2015, '245', '145'),
(147, 49, 2016, '225', '139'),
(148, 50, 2014, '895', '459'),
(149, 50, 2015, '995', '434'),
(150, 50, 2016, '710', '161'),
(151, 51, 2014, '895', '677'),
(152, 51, 2015, '1306', '707'),
(153, 51, 2016, '1350', '649'),
(154, 52, 2014, '0', '0'),
(155, 52, 2015, '60', '120'),
(156, 52, 2016, '30', '60'),
(157, 53, 2014, '12112', '30324'),
(158, 53, 2015, '16612', '40412'),
(159, 53, 2016, '10263', '30326'),
(160, 54, 2014, '0', '0'),
(161, 54, 2015, '30', '15'),
(162, 54, 2016, '20', '10'),
(163, 55, 2014, '0', '0'),
(164, 55, 2015, '50', '200'),
(165, 55, 2016, '50', '200'),
(166, 56, 2014, '4', '11'),
(167, 56, 2015, '230', '235'),
(168, 56, 2016, '80', '200'),
(169, 57, 2014, '50106', '100199'),
(170, 57, 2015, '90906', '181512'),
(171, 57, 2016, '45005', '90015'),
(172, 58, 2014, '6105', '26870'),
(173, 58, 2015, '9250', '23020'),
(174, 58, 2016, '4100', '12750'),
(175, 59, 2014, '50', '140'),
(176, 59, 2015, '300', '365'),
(177, 59, 2016, '0', '0'),
(178, 60, 2014, '50', '200'),
(179, 60, 2015, '104', '416'),
(180, 60, 2016, '312', '1248'),
(181, 61, 2014, '8', '792'),
(182, 61, 2015, '75', '1818'),
(183, 61, 2016, '67', '4812'),
(184, 62, 2014, '17', '849'),
(185, 62, 2015, '108', '458'),
(186, 62, 2016, '83', '731'),
(187, 63, 2014, '100', '200'),
(188, 63, 2015, '307', '202'),
(189, 63, 2016, '257', '127'),
(190, 64, 2014, '100', '214'),
(191, 64, 2015, '350', '416'),
(192, 64, 2016, '50', '110'),
(193, 65, 2014, '0', '55'),
(194, 65, 2015, '250', '192'),
(195, 65, 2016, '0', '0'),
(196, 66, 2014, '107', '188'),
(197, 66, 2015, '400', '455'),
(198, 66, 2016, '50', '150'),
(199, 67, 2014, '86', '357'),
(200, 67, 2015, '11', '46'),
(201, 67, 2016, '140', '560'),
(202, 68, 2014, '232', '2370'),
(203, 68, 2015, '86', '121'),
(204, 68, 2016, '143', '413'),
(205, 69, 2014, '4269', '132039'),
(206, 69, 2015, '2422', '62473'),
(207, 69, 2016, '4952', '25484'),
(208, 70, 2014, '587', '6953'),
(209, 70, 2015, '190', '3450'),
(210, 70, 2016, '175', '665'),
(211, 71, 2014, '220', '2345'),
(212, 71, 2015, '5', '10'),
(213, 71, 2016, '340', '1160'),
(214, 72, 2014, '0', '0'),
(215, 72, 2015, '0', '0'),
(216, 72, 2016, '0', '0'),
(217, 73, 2014, '220', '3195'),
(218, 73, 2015, '0', '0'),
(219, 73, 2016, '100', '500'),
(220, 74, 2014, '55', '520'),
(221, 74, 2015, '80', '96'),
(222, 74, 2016, '75', '86'),
(223, 75, 2014, '164', '4350'),
(224, 75, 2015, '70', '116'),
(225, 75, 2016, '200', '346'),
(226, 76, 2014, '15', '30'),
(227, 76, 2015, '0', '0'),
(228, 76, 2016, '0', '0'),
(229, 77, 2014, '72', '332'),
(230, 77, 2015, '10', '10'),
(231, 77, 2016, '83', '332'),
(232, 78, 2014, '0', '0'),
(233, 78, 2015, '0', '0'),
(234, 78, 2016, '0', '0'),
(235, 79, 2014, '2', '40'),
(236, 79, 2015, '0', '0'),
(237, 79, 2016, '0', '0'),
(238, 80, 2014, '2', '4'),
(239, 80, 2015, '0', '0'),
(240, 80, 2016, '0', '0'),
(241, 81, 2014, '50', '100'),
(242, 81, 2015, '20', '46'),
(243, 81, 2016, '0', '0'),
(244, 82, 2014, '0', '0'),
(245, 82, 2015, '0', '0'),
(246, 82, 2016, '0', '0'),
(247, 83, 2014, '318', '13550'),
(248, 83, 2015, '75', '8275'),
(249, 83, 2016, '412', '1648'),
(250, 84, 2014, '32', '91'),
(251, 84, 2015, '105', '29'),
(252, 84, 2016, '214', '212'),
(253, 85, 2014, '7', '10'),
(254, 85, 2015, '165', '183'),
(255, 85, 2016, '83', '89'),
(256, 86, 2014, '92', '624'),
(257, 86, 2015, '150', '168'),
(258, 86, 2016, '89', '96'),
(259, 87, 2014, '230', '230'),
(260, 87, 2015, '520', '710'),
(261, 87, 2016, '1641', '1641'),
(262, 88, 2014, '90', '660'),
(263, 88, 2015, '150', '168'),
(264, 88, 2016, '135', '292'),
(265, 89, 2014, '1705', '12525'),
(266, 89, 2015, '25', '50'),
(267, 89, 2016, '305', '1020'),
(268, 90, 2014, '0', '0'),
(269, 90, 2015, '0', '0'),
(270, 90, 2016, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pertanian_jenis`
--

CREATE TABLE `lp_pertanian_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pertanian_jenis`
--

INSERT INTO `lp_pertanian_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Tanaman Sayuran dan Buah-Buahan Semusim'),
(2, 'Tanaman Sayuran dan Buah Buahan Komoditas'),
(3, 'Tanaman Biofarmaka Komoditas'),
(4, 'Tanaman Hias Komoditas');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pertanian_sub_jenis`
--

CREATE TABLE `lp_pertanian_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` varchar(86) NOT NULL,
  `satuan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pertanian_sub_jenis`
--

INSERT INTO `lp_pertanian_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`, `satuan`) VALUES
(1, 1, 'Bawang Daun', 'kuintal'),
(2, 1, 'Bawang Merah', 'Kuintal'),
(3, 1, 'Bawang Putih', 'Kuintal'),
(4, 1, 'Bayam', 'Kuintal'),
(5, 1, 'Blewah', 'Kuintal'),
(6, 1, 'Buncis', 'Kuintal'),
(7, 1, 'Cabai Besar', 'Kuintal'),
(8, 1, 'Cabai Rawit', 'Kuintal'),
(9, 1, 'Jamur', 'Kuintal'),
(10, 1, 'Kacang Merah', 'Kuintal'),
(11, 1, 'Kacang Panjang', 'Kuintal'),
(12, 1, 'Kangkung', 'Kuintal'),
(13, 1, 'Kembang Kol', 'Kuintal'),
(14, 1, 'Kentang', 'Kuintal'),
(15, 1, 'Ketimun', 'Kuintal'),
(16, 1, 'Kubis', 'Kuintal'),
(17, 1, 'Labu Siam', 'Kuintal'),
(18, 1, 'Lobak', 'Kuintal'),
(19, 1, 'Melon', 'Kuintal'),
(20, 1, 'Paprika', 'Kuintal'),
(21, 1, 'Petai/Sawi', 'Kuintal'),
(22, 1, 'Semangka', 'Kuintal'),
(23, 1, 'Stroberi', 'Kuintal'),
(24, 1, 'Terong', 'Kuintal'),
(25, 1, 'Tomat', 'Kuintal'),
(26, 1, 'Wortel', 'Kuintal'),
(27, 2, 'Alpukat', 'Kuintal'),
(28, 2, 'Anggur', 'Kuintal'),
(29, 2, 'Apel', 'Kuintal'),
(30, 2, 'Belimbing', 'Kuintal'),
(31, 2, 'Duku / Langsat', 'Kuintal'),
(32, 2, 'Durian', 'Kuintal'),
(33, 2, 'Jambu Air', 'Kuintal'),
(34, 2, 'Jambu Biji', 'Kuintal'),
(35, 2, 'Jengkol', 'Kuintal'),
(36, 2, 'Jeruk Besar', 'Kuintal'),
(37, 2, 'Jeruk Siam / Keprok', 'Kuintal'),
(38, 2, 'Mangga', 'Kuintal'),
(39, 2, 'Manggis', 'Kuintal'),
(40, 2, 'Markisa', 'Kuintal'),
(41, 2, 'Melinjo', 'Kuintal'),
(42, 2, 'Nangka / Cikampek', 'Kuintal'),
(43, 2, 'Nanas', 'Kuintal'),
(44, 2, 'Pepaya', 'Kuintal'),
(45, 2, 'Petai', 'Kuintal'),
(46, 2, 'Pisang', 'Kuintal'),
(47, 2, 'Rambutan', 'Kuintal'),
(48, 2, 'Salak', 'Kuintal'),
(49, 2, 'Sawo', 'Kuintal'),
(50, 2, 'Sirsak', 'Kuintal'),
(51, 2, 'Sukun', 'Kuintal'),
(52, 3, 'Dlingo', 'Kg'),
(53, 3, 'Jahe', 'Kg'),
(54, 3, 'Kapulaga', 'Kg'),
(55, 3, 'Keji Beling', 'Kg'),
(56, 3, 'Kencur', 'Kg'),
(57, 3, 'Kunyit', 'Kg'),
(58, 3, 'Laos / Lengkuas', 'Kg'),
(59, 3, 'Lempuyang', 'Kg'),
(60, 3, 'Lidah Buaya', 'Kg'),
(61, 3, 'Mahkota Dewa', 'Kg'),
(62, 3, 'Mengkudu', 'Kg'),
(63, 3, 'Sambiloto', 'Kg'),
(64, 3, 'Temuireng', 'Kg'),
(65, 3, 'Temukunci', 'Kg'),
(66, 3, 'Temulawak', 'Kg'),
(67, 4, 'Adenium (Kamboja Jepang)', 'Pohon'),
(68, 4, 'Aglonema', 'Pohon'),
(69, 4, 'Anggrek', 'Tangkai'),
(70, 4, 'Anthurium Bunga', 'Tangkai'),
(71, 4, 'Anthurium Daun', 'Pohon'),
(72, 4, 'Anyelir', 'Tangkai'),
(73, 4, 'Caladium', 'Pohon'),
(74, 4, 'Cordyline', 'Pohon'),
(75, 4, 'Diffenbachia', 'Pohon'),
(76, 4, 'Dracaena', 'Pohon'),
(77, 4, 'Euphorbia', 'Pohon'),
(78, 4, 'Gerbera (Herbras)', 'Tangkai'),
(79, 4, 'Gladiol', 'Tangkai'),
(80, 4, 'Heliconia (Pisang-Pisangan)', 'Tangkai'),
(81, 4, 'Ixora (Soka)', 'Pohon'),
(82, 4, 'Krisan', 'Tangkai'),
(83, 4, 'Mawar', 'Tangkai'),
(84, 4, 'Melati', 'Kg'),
(85, 4, 'Monstera', 'Pohon'),
(86, 4, 'Pakis', 'Pohon'),
(87, 4, 'Palem', 'Pohon'),
(88, 4, 'Phylodendron', 'Pohon'),
(89, 4, 'Sansevieria', 'Rumpun'),
(90, 4, 'Sedap Malam', 'Tangkai');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pidana`
--

CREATE TABLE `lp_pidana` (
  `id_pidana` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `lapor` varchar(10) NOT NULL,
  `selesai` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pidana`
--

INSERT INTO `lp_pidana` (`id_pidana`, `id_jenis`, `th`, `lapor`, `selesai`) VALUES
(1, 1, 2015, '1', '0'),
(2, 1, 2016, '0', '2'),
(3, 2, 2014, ' 3', ' 2'),
(4, 2, 2015, ' 47', ' 29'),
(5, 2, 2016, ' 17', ' 6'),
(6, 4, 2014, ' 96', '81'),
(7, 4, 2015, '40', '37'),
(8, 4, 2016, '57', '24'),
(9, 5, 2014, ' 19', ' 16'),
(10, 5, 2015, ' 12', ' 6'),
(11, 5, 2016, ' 29', ' 13'),
(12, 6, 2014, '66', '57'),
(13, 6, 2015, ' 64', '61'),
(14, 6, 2016, ' 86', ' 61'),
(15, 7, 2014, ' 27', ' 16'),
(16, 7, 2015, ' 136', ' 87'),
(17, 7, 2016, ' 125', '75'),
(18, 8, 2015, '3', '0'),
(19, 8, 2016, ' 1', '0'),
(20, 9, 2014, '82', '46'),
(21, 9, 2015, '86', '20'),
(22, 9, 2016, ' 85', ' 27'),
(23, 11, 2014, '10', '16'),
(24, 11, 2015, '4', ' 7'),
(25, 11, 2016, ' 16', ' 19'),
(26, 12, 2014, '2', '0'),
(27, 12, 2015, '1', '1'),
(28, 13, 2014, '0', '1'),
(29, 13, 2015, '1', '4'),
(30, 14, 2014, '16', '15'),
(31, 14, 2015, '20', '22'),
(32, 14, 2016, ' 72', ' 40'),
(33, 15, 2014, '116', '103'),
(34, 15, 2015, '335', ' 148'),
(35, 15, 2016, ' 450', ' 179'),
(36, 16, 2014, '379', '158'),
(37, 16, 2015, '871', ' 176'),
(38, 16, 2016, '1187', ' 151'),
(39, 17, 2014, '0', '24'),
(40, 17, 2015, '2', ' 31'),
(41, 17, 2016, ' 79', ' 48'),
(42, 18, 2014, '140', '112'),
(43, 18, 2015, '231', '152'),
(44, 18, 2016, ' 424', ' 150'),
(45, 19, 2014, '3', '3'),
(46, 19, 2015, '7', '2'),
(47, 19, 2016, ' 14', ' 3'),
(48, 20, 2014, '2', '3'),
(49, 20, 2015, '1', '1'),
(50, 20, 2016, '3', '2'),
(51, 21, 2014, '4', '6'),
(52, 21, 2015, '10', '5'),
(53, 21, 2016, '6', '2'),
(54, 22, 2015, '0', '1'),
(55, 22, 2016, '4', '7'),
(56, 23, 2014, '53', '53'),
(57, 23, 2015, '45', '101'),
(58, 23, 2016, ' 46', '63'),
(59, 24, 2014, '11', '9'),
(60, 24, 2015, '39', ' 14'),
(61, 24, 2016, '16', ' 4'),
(62, 25, 2014, '1', '0'),
(63, 25, 0, '1', ''),
(64, 25, 2016, '1', '0'),
(65, 26, 2014, '92', '92'),
(66, 26, 2015, '170', ' 171'),
(67, 26, 2016, ' 136', '136'),
(68, 27, 2014, '61', '61'),
(69, 27, 2015, '10', '15'),
(70, 27, 2016, '1', '2'),
(71, 27, 2015, '2', '2'),
(72, 29, 2014, '20', '16'),
(73, 29, 2015, '9', '3'),
(74, 29, 2016, '9', '8'),
(75, 30, 2014, '180', '89'),
(76, 30, 2015, '223', ' 54'),
(77, 30, 2016, ' 280', '76'),
(78, 31, 2014, '103', '87'),
(79, 31, 2015, '146', ' 53'),
(80, 31, 2016, '166', ' 67'),
(81, 32, 2014, '1', '3'),
(82, 32, 2015, '6', '1'),
(83, 32, 2016, '8', '8'),
(84, 33, 2014, '2', '1'),
(85, 33, 2015, '2', '0'),
(86, 33, 2016, '1', '0'),
(87, 37, 2014, '3', '3'),
(88, 37, 2015, '2', '1'),
(89, 37, 2016, '3', '1'),
(90, 38, 2014, '14', '11'),
(91, 38, 2015, '15', '6'),
(92, 38, 2016, '17', '4'),
(93, 39, 2015, '1', '0'),
(94, 40, 2014, '7', '9'),
(95, 40, 2015, '6', '8'),
(96, 41, 2014, '1', '0'),
(97, 41, 2016, '3', '1'),
(98, 42, 2014, '15', '6'),
(99, 42, 2015, '19', '2'),
(100, 42, 2016, '25', '7'),
(101, 43, 2015, '2', '3'),
(102, 43, 2016, '1', '1'),
(103, 44, 2014, '32', '32'),
(104, 44, 2015, '38', '25'),
(105, 44, 2016, '42', '41'),
(106, 45, 2014, '132', '110'),
(107, 45, 2015, '184', ' 200'),
(108, 45, 2016, '365', '315'),
(109, 46, 2014, '217', '13'),
(110, 46, 2015, '198', '2'),
(111, 46, 2016, ' 229', '1'),
(112, 47, 2014, '23', '24'),
(113, 47, 2015, '52', ' 7'),
(114, 47, 2016, '19', '4'),
(115, 48, 2014, '41', '23'),
(116, 48, 2015, '62', '41'),
(117, 48, 2016, '54', ' 22'),
(118, 49, 2014, '33', '25'),
(119, 49, 2015, '93', '16'),
(120, 49, 2016, '69', '31'),
(121, 50, 2014, '2', '2'),
(122, 50, 2015, '1', '0'),
(123, 50, 2016, '1', '0'),
(124, 51, 2014, '1', '0'),
(125, 51, 2015, '0', '1'),
(126, 51, 2016, '1', '0'),
(127, 52, 2014, '4', '1'),
(128, 52, 2015, '8', '0'),
(129, 52, 2016, '1', '0'),
(130, 53, 2014, '71', '125'),
(131, 53, 2015, '35', ' 124'),
(132, 53, 2016, '92', '115');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pidana_jenis`
--

CREATE TABLE `lp_pidana_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pidana_jenis`
--

INSERT INTO `lp_pidana_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Pembunuhan'),
(2, 'Penemuan Mayat'),
(3, 'Akibat Orang Mati'),
(4, 'Penganiayaan Berat (Anirat)'),
(5, 'Pengrusakan'),
(6, 'Pengroyokan'),
(7, 'Penganiayaan Ringan (Aniring)'),
(8, 'Mengakibatkan Orang Luka'),
(9, 'KDRT'),
(10, 'Penganiayaan Dalam Keluarga'),
(11, 'Senjata Tajam (Sajam)'),
(12, 'Penculikan'),
(13, 'Bawa Lari Gadis'),
(14, 'Pencurian Dengan Kekerasan\r\n(Curas)'),
(15, 'Pencurian Dengan Pemberatan\r\n(Curat)'),
(16, 'Curanmor'),
(17, 'Penadahan'),
(18, 'Curi Biasa'),
(19, 'Percobaan Pencurian'),
(20, 'Perkosaan'),
(21, 'Perzinahan'),
(22, 'Pornografi'),
(23, 'Perjudian'),
(24, 'Kebakaran'),
(25, 'Pembakaran'),
(26, 'Narkoba'),
(27, 'Minuman Keras (Miras)'),
(28, 'Jual Obat Keras\r\n'),
(29, 'Pemerasan'),
(30, 'Penipuan'),
(31, 'Penggelapan'),
(32, 'Perampasan'),
(33, 'Korupsi'),
(34, 'Pemberian Suap'),
(35, 'Penerimaan Suap'),
(36, 'Penyelundupan'),
(37, 'Penghinaan'),
(38, 'Perbuatan Tidak Enak (PTE)'),
(39, 'Martabat Presiden'),
(40, 'Kejahatan Asusila'),
(41, 'Pengancaman'),
(42, 'Pemalsuan Surat'),
(43, 'Uang Palsu'),
(44, 'Kecelakaan Meninggal Dunia'),
(45, 'Kecelakaan Mengakibatkan Luka'),
(46, 'Informasi Transaksi Elektronik (ITE)'),
(47, 'Fiducia'),
(48, 'Perlindungan Anak'),
(49, 'Pergi Tanpa Pamit'),
(50, 'Serobot Tanah'),
(51, 'Perbankan'),
(52, 'Ketertiban Umum'),
(53, 'Lain-Lain');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pidana_kec`
--

CREATE TABLE `lp_pidana_kec` (
  `id_pidana` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_lapor` varchar(10) NOT NULL,
  `jml_selesai` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pidana_kec`
--

INSERT INTO `lp_pidana_kec` (`id_pidana`, `id_kec`, `th`, `jml_lapor`, `jml_selesai`) VALUES
(1, 'KEC1001', 2015, '216', '165'),
(2, 'KEC1001', 2016, '203', '112'),
(3, 'KEC1002', 2015, '196', '106'),
(4, 'KEC1002', 2016, '265', '98'),
(5, 'KEC1003', 2015, '188', '154'),
(6, 'KEC1003', 2016, '244', '148'),
(7, 'KEC1004', 2015, '234', '99'),
(8, 'KEC1004', 2016, '232', '85'),
(9, 'KEC1005', 2015, '523 ', '253'),
(10, 'KEC1005', 2016, '548', '167'),
(11, '', 2015, '1984', '864'),
(12, '', 2016, '2749', '1106');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pmks`
--

CREATE TABLE `lp_pmks` (
  `id_pmks` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_l` varchar(10) NOT NULL,
  `jml_p` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_pmks_jenis`
--

CREATE TABLE `lp_pmks_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_tehnologi`
--

CREATE TABLE `lp_tehnologi` (
  `id_tehnologi` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tehnologi`
--

INSERT INTO `lp_tehnologi` (`id_tehnologi`, `id_sub_jenis`, `th`, `jml`) VALUES
(1, 1, 2014, '57'),
(2, 1, 2015, '57'),
(3, 1, 2016, '57'),
(4, 2, 2014, '317'),
(5, 2, 2015, '342'),
(6, 2, 2016, '385'),
(7, 3, 2014, '9'),
(8, 3, 2015, '7'),
(9, 3, 2016, '5'),
(10, 4, 2014, '57'),
(11, 4, 2016, '57'),
(12, 5, 2014, '1'),
(13, 5, 2015, '1'),
(14, 5, 2016, '1'),
(15, 6, 2014, '18'),
(16, 4, 2015, '57'),
(17, 6, 2015, '17'),
(18, 6, 2016, '16'),
(19, 7, 2014, '1'),
(20, 7, 2015, '1'),
(21, 7, 2016, '1'),
(22, 8, 2014, '8'),
(23, 8, 2015, '8'),
(24, 8, 2016, '10'),
(25, 9, 2014, '92'),
(26, 9, 2015, '96'),
(27, 9, 2016, '97'),
(28, 10, 2014, '57'),
(29, 10, 2015, '57'),
(30, 10, 2016, '57'),
(31, 11, 2014, '317'),
(32, 11, 2015, '342'),
(33, 11, 2016, '385'),
(34, 12, 2014, '9'),
(35, 12, 2015, '7'),
(36, 12, 2016, '5'),
(37, 13, 2014, '57'),
(38, 13, 2015, '57'),
(39, 13, 2016, '57'),
(40, 14, 2014, '1'),
(41, 14, 2015, '1'),
(42, 14, 2016, '1'),
(43, 15, 2014, '18'),
(44, 15, 2015, '17'),
(45, 15, 2016, '16');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tehnologi_jenis`
--

CREATE TABLE `lp_tehnologi_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tehnologi_jenis`
--

INSERT INTO `lp_tehnologi_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Jumlah Sarana Pendukung Komunikasi dan Informasi'),
(2, 'Data Sistem Informasi ');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tehnologi_sub_jenis`
--

CREATE TABLE `lp_tehnologi_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL,
  `satuan` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tehnologi_sub_jenis`
--

INSERT INTO `lp_tehnologi_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`, `satuan`) VALUES
(1, 1, 'Layanan Telepon Seluler', 'Kelurahan'),
(2, 1, 'Jumlah BTS', 'BTS'),
(3, 1, 'Jumlah Provider', 'Provider '),
(4, 1, 'Layanan Internet', 'Kelurahan'),
(5, 1, 'Radio Pemerintah', 'Radio'),
(6, 1, 'Radio Swasta', 'Radio'),
(7, 1, 'Televisi Pemerintah', 'TV'),
(8, 1, 'Televisi Swasta', 'TV'),
(9, 1, 'Warnet', 'Warnet'),
(10, 2, 'Domain Pemkot Malang', 'Domain'),
(11, 2, 'Website SKPD Pemkot Malang', 'Website'),
(12, 2, 'Aplikasi Layanan Pemkot Malang', 'Aplikasi'),
(13, 2, 'Jumlah Pelayanan Internet di Area Publik Pemkot Malang', 'Lokasi'),
(14, 2, 'Jumlah Fasilitasi Jaringan Internet di Lingkup Kota Malang', 'Lokasi '),
(15, 2, 'Kelompok Informasi Masyarakat (KIM)', 'KIM');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tenaga`
--

CREATE TABLE `lp_tenaga` (
  `id_tenaga` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tenaga`
--

INSERT INTO `lp_tenaga` (`id_tenaga`, `id_jenis`, `id_kategori`, `th`, `jml`) VALUES
(1, 1, 1, 2015, '45'),
(2, 1, 1, 2016, '20'),
(3, 1, 2, 2015, '23'),
(4, 1, 2, 2016, '131'),
(5, 1, 3, 2015, '58 115 200 310'),
(6, 1, 3, 2016, '62 297 965 035'),
(7, 2, 1, 2015, '15'),
(8, 2, 1, 2016, '7'),
(9, 2, 2, 2015, '32'),
(10, 2, 2, 2016, '19'),
(11, 2, 3, 2015, '7 631 000 000'),
(12, 2, 3, 2016, '15 762 500 000'),
(13, 3, 1, 2015, '1'),
(14, 3, 1, 2016, '-'),
(15, 3, 2, 2015, '-'),
(16, 3, 2, 2016, '-'),
(17, 3, 3, 2015, '202 000 000'),
(18, 3, 3, 2016, '-'),
(19, 4, 1, 2015, '32'),
(20, 4, 1, 2016, '3'),
(21, 4, 2, 2015, '26'),
(22, 4, 2, 2016, '27'),
(23, 4, 3, 2015, '31 802 450 000'),
(24, 4, 3, 2016, '1 440 000 000'),
(25, 5, 1, 2015, '6'),
(26, 5, 1, 2016, '3'),
(27, 5, 2, 2015, '25'),
(28, 5, 2, 2016, '18'),
(29, 5, 3, 2015, '7 110 000 000'),
(30, 5, 3, 2016, '11 500 000 000'),
(31, 6, 1, 2015, '9'),
(32, 6, 1, 2016, '15'),
(33, 6, 2, 2015, '381'),
(34, 6, 2, 2016, '298'),
(35, 6, 3, 2015, ' 148 695 330 000'),
(36, 6, 3, 2016, '33 123 136 891'),
(37, 7, 1, 2015, '304'),
(38, 7, 1, 2016, '391'),
(39, 7, 2, 2015, '449'),
(40, 7, 2, 2016, '1420'),
(41, 7, 3, 2015, '138 829 196 884'),
(42, 7, 3, 2016, '156 525 527 137'),
(43, 8, 1, 2015, '-'),
(44, 8, 1, 2016, '1'),
(45, 8, 2, 2015, '-'),
(46, 8, 2, 2016, '124'),
(47, 8, 3, 2015, '-'),
(48, 8, 3, 2016, '25 000 000 000'),
(49, 9, 1, 2015, '-'),
(50, 9, 1, 2016, '25'),
(51, 9, 2, 2015, '-'),
(52, 9, 2, 2016, '568'),
(53, 9, 3, 2015, '-'),
(54, 9, 3, 2016, '50 835 039 094'),
(55, 10, 1, 2015, '29'),
(56, 10, 1, 2016, '25'),
(57, 10, 2, 2015, '52'),
(58, 10, 2, 2016, '228'),
(59, 10, 3, 2015, '103 435 924 000'),
(60, 10, 3, 2016, '148 572 325 000'),
(61, 11, 1, 2015, '-'),
(62, 11, 1, 2016, '-'),
(63, 11, 2, 2015, '-'),
(64, 11, 2, 2016, '-'),
(65, 11, 3, 2015, '-'),
(66, 11, 3, 2016, '-'),
(67, 12, 1, 2015, '142'),
(68, 12, 1, 2016, '120'),
(69, 12, 2, 2015, '254'),
(70, 12, 2, 2016, '567'),
(71, 12, 3, 2015, '181 221 614 177 '),
(72, 12, 3, 2016, '227 123 468 310'),
(73, 13, 1, 2015, '-'),
(74, 13, 1, 2016, '-'),
(75, 13, 2, 2015, '-'),
(76, 13, 2, 2016, '-'),
(77, 14, 1, 2015, '40'),
(78, 14, 1, 2016, '37'),
(79, 14, 2, 2015, '81'),
(80, 14, 2, 2016, '144'),
(81, 14, 3, 2015, '27 783 274 130'),
(82, 14, 3, 2016, '109 346 000 000'),
(83, 15, 1, 2015, '22'),
(84, 15, 1, 2016, '22'),
(85, 15, 2, 2015, '29'),
(86, 15, 2, 2016, '109'),
(87, 15, 3, 2015, '18 030 000 000'),
(88, 15, 3, 2016, '29 828 611 852'),
(89, 16, 1, 2015, '10'),
(90, 16, 1, 2016, '23'),
(91, 16, 2, 2015, '39'),
(92, 16, 2, 2016, '373'),
(93, 16, 3, 2015, '46 244 984 679'),
(94, 16, 3, 2016, '219 740 032 558'),
(95, 17, 1, 2015, '8'),
(96, 17, 1, 2016, '12'),
(97, 17, 2, 2015, '60'),
(98, 17, 2, 2016, '89'),
(99, 17, 3, 2015, '3 210 159 136'),
(100, 17, 3, 2016, '14 661 125 118'),
(101, 18, 1, 2015, '113'),
(102, 18, 1, 2016, '127'),
(103, 18, 2, 2015, '145'),
(104, 18, 2, 2016, '545'),
(105, 18, 3, 2015, '66 096 182 783'),
(106, 18, 3, 2016, '992 499 787 832');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tenaga_jenis`
--

CREATE TABLE `lp_tenaga_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tenaga_jenis`
--

INSERT INTO `lp_tenaga_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Pertanian (Antara lain : Penggilingan Padi Dll.)'),
(2, 'Peternakan'),
(3, 'Perikanan'),
(4, 'Perkebunan / Kehutanan'),
(5, 'Pertambangan dan Galian Gol C'),
(6, 'Perindustrian'),
(7, 'Perdagangan'),
(8, 'Perhotelan'),
(9, 'Restoran / Rumah Makan Cafe'),
(10, 'Perumahan dan Ruko'),
(11, 'Perkantoran, Supermarket, dan Super Mall'),
(12, 'Jasa Konstruksi'),
(13, 'Pergudangan'),
(14, 'Transportasi Darat / Laut'),
(15, 'Kesehatan'),
(16, 'Koperasi'),
(17, 'Jasa Hiburan / Rekreasi'),
(18, 'Lain Lain');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tenaga_kategori`
--

CREATE TABLE `lp_tenaga_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tenaga_kategori`
--

INSERT INTO `lp_tenaga_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Jumlah Unit Usaha'),
(2, 'Jumlah Tenaga Kerja (Orang)'),
(3, 'Modal / Investasi');

-- --------------------------------------------------------

--
-- Table structure for table `lp_terminal`
--

CREATE TABLE `lp_terminal` (
  `id_terminal` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_terminal`
--

INSERT INTO `lp_terminal` (`id_terminal`, `id_jenis`, `id_sub_jenis`, `th`, `jml`) VALUES
(1, 1, 1, 2014, '1'),
(2, 1, 1, 2015, '1'),
(3, 1, 1, 2016, '1'),
(4, 1, 2, 2014, '2'),
(5, 1, 2, 2015, '2'),
(6, 1, 2, 2016, '2'),
(7, 1, 3, 2014, '2'),
(8, 1, 3, 2015, '2'),
(9, 1, 3, 2016, '2'),
(10, 2, 4, 2014, '2659'),
(11, 2, 4, 2015, '2490'),
(12, 2, 4, 2016, '2900'),
(13, 2, 5, 2014, '2347'),
(14, 2, 5, 2015, '2297'),
(15, 2, 5, 2016, '2374'),
(16, 2, 6, 2014, '7465'),
(17, 2, 6, 2015, '7850'),
(18, 2, 6, 2016, '7923'),
(19, 2, 7, 2014, '134'),
(20, 2, 7, 2015, '113'),
(21, 2, 7, 2016, '127'),
(22, 2, 8, 2014, '49'),
(23, 2, 8, 2015, '70'),
(24, 2, 8, 2016, '83'),
(25, 3, 9, 2014, '6'),
(26, 3, 9, 2015, '6'),
(27, 3, 9, 2016, '6'),
(28, 3, 10, 2014, '6'),
(29, 3, 10, 2015, '6'),
(30, 3, 10, 2016, '6'),
(31, 3, 11, 2014, '6'),
(32, 3, 11, 2015, '6'),
(33, 3, 11, 2016, '6'),
(34, 3, 12, 2014, '6'),
(35, 3, 12, 2015, '6'),
(36, 3, 12, 2016, '6'),
(37, 3, 13, 2014, '6'),
(38, 3, 13, 2015, '6'),
(39, 3, 13, 2016, '6'),
(40, 3, 14, 2014, '6'),
(41, 3, 14, 2015, '6'),
(42, 3, 14, 2016, '6'),
(43, 4, 17, 2014, '179'),
(44, 4, 17, 2015, '179'),
(45, 4, 17, 2016, '179'),
(46, 4, 18, 2014, '1,30'),
(47, 4, 18, 2015, '1,30'),
(48, 4, 18, 2016, '1,30'),
(49, 4, 19, 2014, '4'),
(50, 4, 19, 2015, '4'),
(51, 4, 19, 2016, '4'),
(52, 4, 20, 2014, '37'),
(53, 4, 20, 2015, '37'),
(54, 4, 20, 2016, '37'),
(55, 4, 21, 2014, '25'),
(56, 4, 21, 2015, '25'),
(57, 4, 21, 2016, '25');

-- --------------------------------------------------------

--
-- Table structure for table `lp_terminal_jenis`
--

CREATE TABLE `lp_terminal_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_terminal_jenis`
--

INSERT INTO `lp_terminal_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Jumlah Terminal (unit)'),
(2, 'Jumlah Uji KIR (unit)'),
(3, 'Lama Pengujian KIR (bulan)'),
(4, 'Fasilitas Perlengkapan Jalan'),
(5, 'Jumlah Trayek (unit)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_terminal_sub_jenis`
--

CREATE TABLE `lp_terminal_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_terminal_sub_jenis`
--

INSERT INTO `lp_terminal_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`) VALUES
(1, 1, 'Kelas A '),
(2, 1, 'Kelas B'),
(3, 1, 'Kelas C'),
(4, 2, 'Mobil Penumpang Umum'),
(5, 2, 'Mobil Bus'),
(6, 2, 'Mobil Barang'),
(7, 2, 'Kereta Gandengan'),
(8, 2, 'Kereta Tempelan'),
(9, 3, 'Mobil Penumpang Umum'),
(10, 3, 'Mobil Bus'),
(11, 3, 'Mobil Barang'),
(12, 3, 'Mobil Barang'),
(13, 3, 'Kereta Gandengan'),
(14, 3, 'Kereta Tempelan'),
(17, 4, 'Trotoar (Km)'),
(18, 4, 'Jalur Sepeda (Km)'),
(19, 4, 'Tempat Penyebrangan Pejalan Kaki'),
(20, 4, 'Halte (Unit)'),
(21, 4, 'Fasilitas Khusus Penyandang Cacat (Km)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tpa`
--

CREATE TABLE `lp_tpa` (
  `id_tpa` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tpa`
--

INSERT INTO `lp_tpa` (`id_tpa`, `id_kec`, `th`, `jml`) VALUES
(5, 'KEC1001', 2015, '13'),
(6, 'KEC1001', 2016, '11'),
(7, 'KEC1002', 2015, '15'),
(8, 'KEC1002', 2016, '15'),
(9, 'KEC1003', 2015, '12'),
(10, 'KEC1003', 2016, '9'),
(11, 'KEC1004', 2015, '17'),
(12, 'KEC1004', 2016, '15'),
(13, 'KEC1005', 2015, '11'),
(14, 'KEC1005', 2016, '14');

-- --------------------------------------------------------

--
-- Table structure for table `main_dinas`
--

CREATE TABLE `main_dinas` (
  `id_dinas` int(11) NOT NULL,
  `nama_dinas` varchar(500) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_dinas`
--

INSERT INTO `main_dinas` (`id_dinas`, `nama_dinas`, `alamat`) VALUES
(2, 'Dinas Perdagangan', 'Malang'),
(3, 'Dinas Komunikasi dan Informatika', 'Malang'),
(4, 'Dinas Kebudayaan dan Pariwisata', 'Malang'),
(6, 'Dinas Perindustrian', 'Malang'),
(7, 'Dinas Pendidikan', 'Malang'),
(8, 'Dinas Kesehatan', 'Malang'),
(9, 'Dinas Perhubungan', 'Malang'),
(10, 'Dinas Pertanian dan Ketahanan Pangan', 'Malang'),
(11, 'Dinas Perumahan dan Kawasan Pemukiman', 'Malang'),
(14, 'Dinas Koperasi dan Usaha Mikro', 'Malang'),
(15, 'Dinas Kepemudaan dan Olahraga', ''),
(16, 'Dinas Kepedudukan dan Pencatatan Sipil', ''),
(17, 'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana', ''),
(18, 'Dinas Lingkungan Hidup', ''),
(19, 'Dinas Perpustakaan Umum dan Arsip Daerah', ''),
(20, 'Badan Kesatuan Bangsa dan Politik', ''),
(25, 'Dinas Pertanian', 'Malang');

-- --------------------------------------------------------

--
-- Table structure for table `main_jenis_kendaraan`
--

CREATE TABLE `main_jenis_kendaraan` (
  `id_jenis_kendaraan` int(11) NOT NULL,
  `keterangan` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_jenis_kendaraan`
--

INSERT INTO `main_jenis_kendaraan` (`id_jenis_kendaraan`, `keterangan`) VALUES
(1, 'Mobil Penumpang'),
(2, 'Bus'),
(3, 'Truk'),
(4, 'Sepeda Motor');

-- --------------------------------------------------------

--
-- Table structure for table `main_jenis_penyakit`
--

CREATE TABLE `main_jenis_penyakit` (
  `id_jenis_penyakit` int(11) NOT NULL,
  `keterangan` varchar(64) NOT NULL,
  `jml` varchar(10) NOT NULL,
  `th` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_jenis_penyakit`
--

INSERT INTO `main_jenis_penyakit` (`id_jenis_penyakit`, `keterangan`, `jml`, `th`) VALUES
(3, 'Headache', '7966', 2016),
(4, 'Dermatitis Kontak Alergi', '8718', 2016),
(5, 'Penyakit Pulpa Dan Jar Perapikal', '8819', 2016),
(6, 'Myalgia / Nyeri Otot', '9025', 2016),
(7, 'Obs Febris', '10773', 2016),
(8, 'Influensa, Virus Tidak Diidentifikasi', '12743', 2016),
(9, 'DM', '13815', 2016),
(10, 'Gastritis', '13840', 2016),
(11, 'Hipertensi Primer', '32109', 2016),
(12, 'Infeksi Saluran Pernapasan', '55351', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `main_jenjang_pend`
--

CREATE TABLE `main_jenjang_pend` (
  `id_jenjang` int(11) NOT NULL,
  `ket` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `main_kecamatan`
--

CREATE TABLE `main_kecamatan` (
  `id_kec` varchar(7) NOT NULL,
  `nama_kec` varchar(50) NOT NULL,
  `latlng` text NOT NULL,
  `luas` double NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_kecamatan`
--

INSERT INTO `main_kecamatan` (`id_kec`, `nama_kec`, `latlng`, `luas`, `is_delete`) VALUES
('KEC1001', 'Kedungkandang', 'null', 39.87, '0'),
('KEC1002', 'Sukun', 'null', 20.97, '0'),
('KEC1003', 'Klojen', 'null', 8.83, '0'),
('KEC1004', 'Blimbing', 'null', 17.77, '0'),
('KEC1005', 'Lowokwaru', 'null', 22.6, '0');

-- --------------------------------------------------------

--
-- Table structure for table `main_kelurahan`
--

CREATE TABLE `main_kelurahan` (
  `id_kel` varchar(7) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `nama_kel` varchar(64) NOT NULL,
  `latlng_kel` text NOT NULL,
  `luas_kel` double NOT NULL,
  `is_del` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_kelurahan`
--

INSERT INTO `main_kelurahan` (`id_kel`, `id_kec`, `nama_kel`, `latlng_kel`, `luas_kel`, `is_del`) VALUES
('KEL1001', 'KEC1001', 'Bumiayu', '0', 12, '0'),
('KEL1002', 'KEC1001', 'Mergosono', '12', 12, '0'),
('KEL1003', 'KEC1001', 'Kotalama', '12', 12, '0'),
('KEL1004', 'KEC1001', 'Wonokoyo', '12', 12, '0'),
('KEL1005', 'KEC1001', 'Buring', '12', 12, '0'),
('KEL1006', 'KEC1001', 'Kedungkandang', '12', 12, '0'),
('KEL1007', 'KEC1001', 'Lesanpuro', '12', 12, '0'),
('KEL1008', 'KEC1001', 'Sawojajar', '12', 12, '0'),
('KEL1009', 'KEC1001', 'Madyopuro', '12', 12, '0'),
('KEL1010', 'KEC1001', 'Cemorokandang', '12', 12, '0'),
('KEL1011', 'KEC1001', 'Arjowinangun', '12', 12, '0'),
('KEL1012', 'KEC1001', 'Tlogowaru', '12', 12, '0'),
('KEL1013', 'KEC1002', 'Ciptomulyo', '12', 12, '0'),
('KEL1014', 'KEC1002', 'Gadang', '12', 12, '0'),
('KEL1015', 'KEC1002', 'Kebonsari', '12', 12, '0'),
('KEL1016', 'KEC1002', 'Bandungrejosari', '12', 12, '0'),
('KEL1017', 'KEC1002', 'Sukun', '12', 12, '0'),
('KEL1018', 'KEC1005', 'Tunggulwulung', '1', 12, '0'),
('KEL1019', 'KEC1005', 'Merjosari', '1', 1, '0'),
('KEL1020', 'KEC1005', 'Tlogomas', '1', 1, '0'),
('KEL1021', 'KEC1003', 'Klojen', '1', 1, '0'),
('KEL1022', 'KEC1003', 'Samaan', '1', 1, '0'),
('KEL1023', 'KEC1003', 'Rampalcelaket', '1', 1, '0'),
('KEL1024', 'KEC1004', 'Balearjosari', '1', 1, '0'),
('KEL1025', 'KEC1004', 'Arjosari', '1', 1, '0'),
('KEL1026', 'KEC1004', 'Polowijen', '1', 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `main_partai`
--

CREATE TABLE `main_partai` (
  `id_partai` int(2) NOT NULL,
  `nama_partai` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_partai`
--

INSERT INTO `main_partai` (`id_partai`, `nama_partai`, `is_delete`) VALUES
(1, 'Partai Kebangkitan Bangsa (PKB)', '0'),
(2, 'Partai Gerakan Indonesia Raya (Gerindra)', '0'),
(3, 'Partai Demokrasi Indonesia Perjuangan (PDIP)', '0'),
(4, 'Partai Golongan Karya (Golkar)', '0'),
(5, 'Partai Demokrat', '0'),
(6, 'Partai Keadilan Sejahtera (PKS)', '0'),
(7, 'Partai Amanat Nasional (PAN)', '0'),
(8, 'Partai Gerakan Indonesia Raya (Gerindra)', '0'),
(9, 'Partai Persatuan Pembangunan (PPP)', '0'),
(10, 'Partai Hati Nurani Rakyat (Hanura)', '0'),
(11, 'Partai Nasional Demokrat (Nasdem)', '0'),
(12, 'Partai Bulan Bintang', '0'),
(13, 'Partai Keadilan dan Persatuan Indonesia (PKPI)', '0'),
(14, 'rwergfd ', '1');

-- --------------------------------------------------------

--
-- Table structure for table `main_pendidikan`
--

CREATE TABLE `main_pendidikan` (
  `id_pendidikan` int(11) NOT NULL,
  `jenjang` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `miskin_jml_pend`
--

CREATE TABLE `miskin_jml_pend` (
  `id_pend_miskin` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL,
  `agregat` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `miskin_jml_pend`
--

INSERT INTO `miskin_jml_pend` (`id_pend_miskin`, `th`, `prosentase`, `agregat`) VALUES
(4, 2014, '4.80', '40.64'),
(5, 2015, '4.60', '39.10'),
(6, 2016, '4.33', '37.03');

-- --------------------------------------------------------

--
-- Table structure for table `miskin_pengeluaran_perkapita`
--

CREATE TABLE `miskin_pengeluaran_perkapita` (
  `id_perkapita` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `pengeluaran_perkapita` varchar(20) NOT NULL,
  `garis_kemiskinan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `miskin_pengeluaran_perkapita`
--

INSERT INTO `miskin_pengeluaran_perkapita` (`id_perkapita`, `th`, `pengeluaran_perkapita`, `garis_kemiskinan`) VALUES
(3, 2014, '1215502', '381400'),
(4, 2015, '1260186', '411709'),
(5, 2016, '1355476', '426527');

-- --------------------------------------------------------

--
-- Table structure for table `pem_dpr`
--

CREATE TABLE `pem_dpr` (
  `id_pem_dpr` int(11) NOT NULL,
  `id_partai` int(2) NOT NULL,
  `th` varchar(12) NOT NULL,
  `jk` enum('0','1') NOT NULL,
  `jml` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pem_dpr`
--

INSERT INTO `pem_dpr` (`id_pem_dpr`, `id_partai`, `th`, `jk`, `jml`) VALUES
(3, 2, '2014-2016', '0', 3),
(7, 2, '2014-2016', '1', 1),
(8, 11, '2014-2016', '0', 1),
(9, 10, '2014-2016', '0', 2),
(10, 10, '2014-2016', '1', 1),
(11, 9, '2014-2016', '0', 1),
(12, 9, '2014-2016', '1', 2),
(13, 7, '2014-2016', '0', 4),
(14, 6, '2014-2016', '0', 3),
(15, 1, '2014-2016', '0', 6),
(16, 5, '2014-2016', '0', 3),
(17, 5, '2014-2016', '1', 2),
(18, 4, '2014-2016', '0', 4),
(19, 4, '2014-2016', '1', 1),
(20, 3, '2014-2016', '0', 7),
(21, 3, '2014-2016', '1', 4),
(22, 11, '2014-2016', '1', 0),
(23, 7, '2014-2016', '1', 0),
(24, 1, '2014-2016', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pem_jml_asn_jenjang`
--

CREATE TABLE `pem_jml_asn_jenjang` (
  `id_jml_asn` varchar(6) NOT NULL,
  `id_jenjang` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pem_jml_rt_rw`
--

CREATE TABLE `pem_jml_rt_rw` (
  `id_rt_rw` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_rt` int(11) NOT NULL,
  `jml_rw` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pem_jml_rt_rw`
--

INSERT INTO `pem_jml_rt_rw` (`id_rt_rw`, `id_kec`, `th`, `jml_rt`, `jml_rw`) VALUES
(1, 'KEC1001', 2018, 892, 116),
(2, 'KEC1002', 2018, 882, 94),
(4, 'KEC1004', 2018, 923, 127),
(5, 'KEC1003', 2018, 675, 89),
(6, 'KEC1005', 2018, 785, 120),
(7, 'KEC1001', 2017, 891, 115),
(8, 'KEC1002', 2017, 881, 92),
(9, 'KEC1003', 2017, 673, 88),
(10, 'KEC1003', 2016, 670, 85),
(11, 'KEC1004', 2017, 922, 125),
(12, 'KEC1005', 2017, 783, 118),
(13, 'KEC1001', 2016, 890, 114),
(14, 'KEC1002', 2016, 880, 90),
(15, 'KEC1004', 2016, 920, 123),
(16, 'KEC1005', 2016, 781, 116);

-- --------------------------------------------------------

--
-- Table structure for table `pendapatan_lap_usaha`
--

CREATE TABLE `pendapatan_lap_usaha` (
  `id_lap_usaha` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendapatan_lap_usaha`
--

INSERT INTO `pendapatan_lap_usaha` (`id_lap_usaha`, `id_jenis`, `th`, `jml`) VALUES
(2, 5, 2016, '1231'),
(3, 7, 2014, '28.47'),
(4, 7, 2015, '28.9'),
(5, 7, 2016, '29.54'),
(6, 8, 2014, '27.14'),
(7, 8, 2015, '26.51'),
(8, 8, 2016, '25.4'),
(9, 9, 2014, '12.56'),
(10, 9, 2015, '12.52'),
(11, 9, 2016, '12.92');

-- --------------------------------------------------------

--
-- Table structure for table `pendapatan_lap_usaha_jenis`
--

CREATE TABLE `pendapatan_lap_usaha_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendapatan_lap_usaha_jenis`
--

INSERT INTO `pendapatan_lap_usaha_jenis` (`id_jenis`, `nama_jenis`) VALUES
(7, 'Perdagangan Besar dan Eceran; Reparasi Mobil dan Sepeda Motor'),
(8, 'Industri Pengolahan'),
(9, 'Konstruksi');

-- --------------------------------------------------------

--
-- Table structure for table `pendapatan_regional`
--

CREATE TABLE `pendapatan_regional` (
  `id_pendapatan` int(11) NOT NULL,
  `th_pendapatan` int(4) NOT NULL,
  `jml_pendapatan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendapatan_regional`
--

INSERT INTO `pendapatan_regional` (`id_pendapatan`, `th_pendapatan`, `jml_pendapatan`) VALUES
(4, 2014, '46563213.3'),
(5, 2015, '51824393.8'),
(6, 2016, '57171601.6');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_baca_tulis`
--

CREATE TABLE `pendidikan_baca_tulis` (
  `id_baca_tulis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_baca_tulis`
--

INSERT INTO `pendidikan_baca_tulis` (`id_baca_tulis`, `id_jenis`, `th`, `prosentase`) VALUES
(3, 1, 2014, '97.45'),
(4, 1, 2015, '98.16'),
(5, 1, 2016, '98.17'),
(6, 2, 2014, '2.55'),
(7, 2, 2015, '1.84'),
(8, 2, 2016, '1.83');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_baca_tulis_jenis`
--

CREATE TABLE `pendidikan_baca_tulis_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_baca_tulis_jenis`
--

INSERT INTO `pendidikan_baca_tulis_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Huruf Latin dan  atau Lainnya'),
(2, 'Buta Huruf');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_jml_sklh`
--

CREATE TABLE `pendidikan_jml_sklh` (
  `id_jml_sklh` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_jml_sklh`
--

INSERT INTO `pendidikan_jml_sklh` (`id_jml_sklh`, `id_jenis`, `th`, `prosentase`) VALUES
(2, 3, 2015, '2314'),
(5, 4, 2014, '333'),
(6, 4, 2015, '344'),
(7, 4, 2016, '341'),
(9, 5, 2014, '7'),
(10, 5, 2015, '6'),
(11, 5, 2016, '7'),
(12, 6, 2014, '92'),
(13, 6, 2015, '100'),
(14, 6, 2016, '101'),
(15, 7, 2014, '270'),
(16, 7, 2015, '272'),
(17, 7, 2016, '274'),
(18, 8, 2014, '9'),
(19, 8, 2015, '8'),
(20, 8, 2016, '8'),
(21, 9, 2014, '4'),
(22, 9, 2015, '52'),
(23, 9, 2016, '43'),
(24, 10, 2014, '97'),
(25, 10, 2015, '101'),
(26, 10, 2016, '100'),
(27, 11, 2014, '8'),
(28, 11, 2015, '8'),
(29, 11, 2016, '8'),
(30, 12, 2014, '26'),
(31, 12, 2015, '30'),
(32, 12, 2016, '34'),
(33, 13, 2014, '38'),
(34, 13, 2015, '47'),
(35, 13, 2016, '45'),
(36, 14, 2014, '5'),
(37, 14, 2015, '6'),
(38, 14, 2016, '5'),
(39, 15, 2014, '38'),
(40, 15, 2015, '47'),
(41, 15, 2016, '45'),
(42, 16, 2014, '15'),
(43, 16, 2015, '15'),
(44, 16, 2016, '15');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_jml_sklh_jenis`
--

CREATE TABLE `pendidikan_jml_sklh_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_jml_sklh_jenis`
--

INSERT INTO `pendidikan_jml_sklh_jenis` (`id_jenis`, `nama_jenis`) VALUES
(4, 'TK'),
(5, 'TK LB'),
(6, 'RA'),
(7, 'SD'),
(8, 'SD LB'),
(9, 'MI'),
(10, 'SMP'),
(11, 'SMP LB'),
(12, 'MTS'),
(13, 'SMA'),
(14, 'SMA LB'),
(15, 'SMK'),
(16, 'MA');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_partisipasi_sklh`
--

CREATE TABLE `pendidikan_partisipasi_sklh` (
  `id_partisipasi_sklh` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_partisipasi_sklh`
--

INSERT INTO `pendidikan_partisipasi_sklh` (`id_partisipasi_sklh`, `id_jenis`, `th`, `prosentase`) VALUES
(1, 1, 2014, '100'),
(2, 1, 2015, '100'),
(3, 1, 2016, '100'),
(4, 2, 2014, '99.08'),
(5, 2, 2015, '98.95'),
(6, 2, 2016, '95.75'),
(7, 3, 2014, '71.59'),
(8, 3, 2015, '78.91'),
(9, 3, 2016, '78.32');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_partisipasi_sklh_jenis`
--

CREATE TABLE `pendidikan_partisipasi_sklh_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_partisipasi_sklh_jenis`
--

INSERT INTO `pendidikan_partisipasi_sklh_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'APS SD (7-12 tahun)'),
(2, 'APS SMP (13-15 tahun)'),
(3, 'APS SMA (16-18 tahun)');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_penduduk`
--

CREATE TABLE `pendidikan_penduduk` (
  `id_pendidikan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_penduduk`
--

INSERT INTO `pendidikan_penduduk` (`id_pendidikan`, `id_jenis`, `th`, `prosentase`) VALUES
(1, 3, 2014, '13.79'),
(2, 3, 2015, '8.39'),
(3, 3, 2016, '8.76'),
(4, 4, 2014, '16.16'),
(5, 4, 2015, '18.90'),
(6, 4, 2016, '26.35'),
(7, 5, 2014, '17.53'),
(8, 5, 2015, '19.83'),
(9, 5, 2016, '11.93'),
(10, 6, 2014, '35.67'),
(11, 6, 2015, '36.09'),
(12, 6, 2016, '35.94'),
(13, 7, 2014, '16.85'),
(14, 7, 2015, '16.79'),
(15, 7, 2016, '17.03');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_penduduk_jenis`
--

CREATE TABLE `pendidikan_penduduk_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_penduduk_jenis`
--

INSERT INTO `pendidikan_penduduk_jenis` (`id_jenis`, `nama_jenis`) VALUES
(3, 'Tidak Punya Ijazah SD'),
(4, 'SD / Sederajat'),
(5, 'SMP / Sederajat'),
(6, 'SMA / Sederajat'),
(7, 'PT / Universitas');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_rasio_guru_murid`
--

CREATE TABLE `pendidikan_rasio_guru_murid` (
  `id_rasio` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_rasio_guru_murid`
--

INSERT INTO `pendidikan_rasio_guru_murid` (`id_rasio`, `id_jenis`, `th`, `prosentase`) VALUES
(1, 2, 2015, '1222'),
(2, 1, 2016, '120'),
(5, 6, 2014, '13'),
(6, 6, 2015, '13'),
(7, 6, 2016, '7'),
(8, 7, 2014, '19'),
(9, 7, 2015, '19'),
(10, 7, 2016, '19'),
(11, 8, 2014, '15'),
(12, 8, 2015, '15'),
(13, 8, 2016, '14'),
(14, 9, 2014, '12'),
(15, 9, 2015, '12'),
(16, 9, 2016, '12'),
(17, 10, 2014, '14'),
(18, 10, 2015, '13'),
(19, 10, 2016, '13');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_rasio_guru_murid_jenis`
--

CREATE TABLE `pendidikan_rasio_guru_murid_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_rasio_guru_murid_jenis`
--

INSERT INTO `pendidikan_rasio_guru_murid_jenis` (`id_jenis`, `nama_jenis`) VALUES
(6, 'TK'),
(7, 'SD'),
(8, 'SMP'),
(9, 'SMA'),
(10, 'SMK');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_penduduk`
--

CREATE TABLE `pengeluaran_penduduk` (
  `id_pengeluaran` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran_penduduk`
--

INSERT INTO `pengeluaran_penduduk` (`id_pengeluaran`, `id_jenis`, `th`, `jml`) VALUES
(2, 6, 2014, '40.93'),
(3, 6, 2015, '44.97'),
(4, 6, 2016, '50.38'),
(5, 7, 2014, '18.03'),
(6, 7, 2015, '14.19'),
(7, 7, 2016, '13.12'),
(8, 8, 2014, '23.32'),
(9, 8, 2015, '22.33'),
(10, 8, 2016, '14.66'),
(11, 9, 2014, '15.88'),
(12, 9, 2015, '16.11'),
(13, 9, 2016, '19.16'),
(14, 10, 2014, '1.84'),
(15, 10, 2015, '2.40'),
(16, 10, 2016, '2.69');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_penduduk_jenis`
--

CREATE TABLE `pengeluaran_penduduk_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran_penduduk_jenis`
--

INSERT INTO `pengeluaran_penduduk_jenis` (`id_jenis`, `nama_jenis`) VALUES
(6, '>= 1000000'),
(7, '750000 s/d 999999'),
(8, '500000 s/d 749999'),
(9, '300000 s/d 499999'),
(10, '<= 299999');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_rmt_tgg`
--

CREATE TABLE `pengeluaran_rmt_tgg` (
  `id_rmt_tgg` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_mkn` varchar(20) NOT NULL,
  `jml_non` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran_rmt_tgg`
--

INSERT INTO `pengeluaran_rmt_tgg` (`id_rmt_tgg`, `th`, `jml_mkn`, `jml_non`) VALUES
(5, 2014, '38.09', '61.91'),
(6, 2015, '39.21', '60.79'),
(7, 2016, '38.63', '61.37');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_holti_jenis`
--

CREATE TABLE `pertanian_holti_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL,
  `kategori` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_holti_jenis`
--

INSERT INTO `pertanian_holti_jenis` (`id_jenis`, `nama_jenis`, `kategori`) VALUES
(9, 'Sukun', '0'),
(10, 'Sirsak', '0'),
(11, 'Sawo', '0'),
(12, 'Salak', '0'),
(13, 'Rambutan', '0'),
(14, 'Pisang', '0'),
(15, 'Petai', '1'),
(16, 'Pepaya', '0'),
(17, 'Nanas', '0'),
(18, 'Nangka/Cempedak', '0'),
(19, 'Melinjo', '0'),
(20, 'Markisa/Konyal', '0'),
(21, 'Manggis', '0'),
(22, 'Mangga', '0'),
(23, 'Jeruk Siam/Keprok', '0'),
(24, 'Jeruk Besar', '0'),
(25, 'Jengkol', '1'),
(26, 'Jambu Biji', '0'),
(27, 'Jambu Air', '0'),
(28, 'Durian', '0'),
(29, 'Duku/Langsat', '0'),
(30, 'Belimbing', '0'),
(31, 'Apel', '0'),
(32, 'Anggur', '0'),
(33, 'Alpukat', '0'),
(34, 'Bawang Daun', '1'),
(35, 'Bawang Merah', '1'),
(36, 'Bawang Putih', '1'),
(37, 'Bayam', '1'),
(38, 'Blewah', '0'),
(39, 'Buncis', '0'),
(40, 'Cabai Besar', '0'),
(41, 'Cabai Rawit', '0'),
(42, 'Jamur', '1'),
(43, 'Kacang Merah', '1'),
(44, 'Kacang Panjang', '1'),
(45, 'Kangkung', '1'),
(46, 'Kembang Kol', '1'),
(47, 'Kentang ', '1'),
(48, 'Ketimun', '0'),
(49, 'Kubis', '1'),
(50, 'Labu Siam', '1'),
(51, 'Lobak', '1'),
(52, 'Melon', '0'),
(53, 'Paprika', '0'),
(54, 'Petsai/Sawi', '1'),
(55, 'Semangka', '0'),
(56, 'Stroberi', '0'),
(57, 'Terung', '1'),
(58, 'Tomat', '0'),
(59, 'Wortel', '1');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_holti_jml`
--

CREATE TABLE `pertanian_holti_jml` (
  `id_holti` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_holti_jml`
--

INSERT INTO `pertanian_holti_jml` (`id_holti`, `id_jenis`, `th`, `jml`) VALUES
(3, 5, 2019, '200'),
(5, 9, 2016, '649'),
(6, 10, 2016, '161'),
(7, 11, 2016, '139'),
(8, 12, 2016, '2310'),
(9, 13, 2016, '777'),
(10, 14, 2016, '2121'),
(11, 15, 2016, '536'),
(12, 16, 2016, '1853'),
(13, 17, 2016, '0'),
(14, 18, 2016, '1397'),
(15, 19, 2016, '475'),
(16, 20, 2016, '65'),
(17, 21, 2016, '0'),
(18, 22, 2016, '10137'),
(19, 23, 2016, '5429'),
(20, 24, 2016, '200'),
(21, 25, 2016, '0'),
(22, 26, 2016, '815'),
(23, 27, 2016, '323'),
(24, 28, 2016, '467'),
(25, 29, 2016, '50'),
(26, 30, 2016, '836'),
(27, 31, 2016, '0'),
(28, 32, 2016, '55'),
(29, 33, 2016, '200'),
(30, 34, 2016, '0'),
(31, 35, 2016, '0'),
(32, 36, 2016, '0'),
(33, 37, 2016, '0'),
(34, 38, 2016, '0'),
(35, 39, 2016, '0'),
(36, 40, 2016, '278'),
(37, 41, 2016, '44'),
(38, 42, 2016, '35471.2'),
(39, 43, 2016, '0'),
(40, 44, 2016, '50'),
(41, 45, 2016, '0'),
(42, 46, 2016, '8'),
(43, 47, 2016, '0'),
(44, 48, 2016, '2'),
(45, 49, 2016, '0'),
(46, 50, 2016, '0'),
(47, 51, 2016, '0'),
(48, 52, 2016, '0'),
(49, 53, 2016, '0'),
(50, 54, 2016, '42'),
(51, 55, 2016, '0'),
(52, 56, 2016, '0'),
(53, 57, 2016, '20'),
(54, 58, 2016, '82'),
(55, 59, 2016, '0');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_ikan_jenis`
--

CREATE TABLE `pertanian_ikan_jenis` (
  `id_jenis` int(11) NOT NULL,
  `kategori` enum('0','1') NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_ikan_jenis`
--

INSERT INTO `pertanian_ikan_jenis` (`id_jenis`, `kategori`, `nama_jenis`) VALUES
(11, '0', 'Ikan Nila'),
(12, '0', 'Ikan Tombro'),
(13, '0', 'Ikan Gurame'),
(14, '0', 'Ikan Lele'),
(15, '1', 'Ikan Nila'),
(16, '1', 'Ikan Tombro'),
(17, '1', 'Ikan Gurame'),
(18, '1', 'Ikan Lele');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_ikan_jml`
--

CREATE TABLE `pertanian_ikan_jml` (
  `id_ikan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_ikan_jml`
--

INSERT INTO `pertanian_ikan_jml` (`id_ikan`, `id_jenis`, `th`, `jml`) VALUES
(4, 11, 2014, '3499'),
(5, 11, 2015, '2735'),
(6, 11, 2016, '6258'),
(7, 12, 2014, '20'),
(8, 12, 2015, '-'),
(9, 12, 2016, '-'),
(10, 17, 2014, '15'),
(11, 13, 2015, '-'),
(12, 13, 2016, '40'),
(13, 14, 2014, '33830'),
(17, 14, 2015, '53771'),
(18, 14, 2016, '101231'),
(19, 15, 2014, '1495'),
(20, 15, 2015, '203'),
(21, 15, 2016, '1220'),
(22, 16, 2014, '1528'),
(23, 16, 2015, '550'),
(24, 16, 2016, '2176'),
(25, 17, 2014, '-'),
(26, 17, 2015, '-'),
(27, 17, 2016, '-'),
(28, 18, 2014, '-'),
(29, 18, 2015, '-'),
(30, 18, 2016, '-');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_kelapa_tebu`
--

CREATE TABLE `pertanian_kelapa_tebu` (
  `id_pertanian` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_kelapa` varchar(20) NOT NULL,
  `jml_tebu` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_kelapa_tebu`
--

INSERT INTO `pertanian_kelapa_tebu` (`id_pertanian`, `th`, `jml_kelapa`, `jml_tebu`) VALUES
(3, 2016, '925', '53142'),
(4, 2015, '807', '60013.28'),
(5, 2014, '790', '62725');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_komoditas_jenis`
--

CREATE TABLE `pertanian_komoditas_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_komoditas_jenis`
--

INSERT INTO `pertanian_komoditas_jenis` (`id_jenis`, `nama_jenis`) VALUES
(6, 'Padi Sawah'),
(7, 'Padi Ladang'),
(8, 'Jagung'),
(9, 'Kedelai'),
(10, 'Kacang Tanah'),
(11, 'Kacang Hijau'),
(12, 'Ubi Kayu'),
(13, 'Ubi Jalar');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_komoditas_jml`
--

CREATE TABLE `pertanian_komoditas_jml` (
  `id_komoditas` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_komoditas_jml`
--

INSERT INTO `pertanian_komoditas_jml` (`id_komoditas`, `id_jenis`, `th`, `jml`) VALUES
(3, 6, 2016, '142850'),
(4, 7, 2016, '0'),
(5, 8, 2016, '4590'),
(6, 9, 2016, '0'),
(7, 10, 2016, '150'),
(8, 11, 2016, '0'),
(9, 12, 2016, '38580'),
(10, 13, 2016, '190');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_lahan`
--

CREATE TABLE `pertanian_lahan` (
  `id_lahan` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jenis_lahan` varchar(64) NOT NULL,
  `luas_lahan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_lahan`
--

INSERT INTO `pertanian_lahan` (`id_lahan`, `th`, `jenis_lahan`, `luas_lahan`) VALUES
(1, 2017, 'Lahan Pertanian Sawah', '20.90'),
(3, 2016, 'Lahan Pertanian Sawah', '10.38'),
(4, 2016, 'Lahan Pertanian Bukan Sawah', '18.85'),
(5, 2016, 'Lahan Bukan Pertanian', '70.77');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_lahan_penggunaan`
--

CREATE TABLE `pertanian_lahan_penggunaan` (
  `id_peng` int(11) NOT NULL,
  `jenis_peng` text NOT NULL,
  `th` int(4) NOT NULL,
  `luas_pend` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_lahan_penggunaan`
--

INSERT INTO `pertanian_lahan_penggunaan` (`id_peng`, `jenis_peng`, `th`, `luas_pend`) VALUES
(1, 'Lahan Ditanami Padi (Sawah)', 2018, '2000.98'),
(3, 'Lahan Ditanami Padi (Sawah)', 2016, '844'),
(4, 'Lahan Ditanami Selain Padi (Bukan Sawah)', 2016, '292'),
(5, 'Lahan Tidak Ditanami', 2016, '6');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_ternak_jenis`
--

CREATE TABLE `pertanian_ternak_jenis` (
  `id_jenis` int(11) NOT NULL,
  `kategori` enum('0','1','2') NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_ternak_jenis`
--

INSERT INTO `pertanian_ternak_jenis` (`id_jenis`, `kategori`, `nama_jenis`) VALUES
(9, '0', 'Daging sapi'),
(10, '0', 'Daging Kambing/Domba'),
(11, '0', 'Daging Babi'),
(12, '1', 'Ayam Buras'),
(13, '1', 'Ayam Pedaging'),
(14, '1', 'Itik'),
(15, '1', 'Itik Manila'),
(16, '0', 'Susu'),
(17, '2', 'Ayam Buras'),
(18, '2', 'Ayam Petelur'),
(19, '2', 'Itik'),
(20, '2', 'Itik Manila'),
(21, '1', 'Ayam Petelur');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_ternak_jml`
--

CREATE TABLE `pertanian_ternak_jml` (
  `id_ternak` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_ternak_jml`
--

INSERT INTO `pertanian_ternak_jml` (`id_ternak`, `id_jenis`, `th`, `jml`) VALUES
(3, 9, 2014, '3465'),
(4, 9, 2015, '3614.84'),
(5, 9, 2016, '3246.32'),
(6, 10, 2014, '320'),
(7, 10, 2015, '430'),
(8, 10, 2015, '490.22'),
(9, 11, 2014, '511.80'),
(10, 11, 2015, '525.05'),
(11, 11, 2016, '508.39'),
(12, 12, 2014, '642.70'),
(13, 12, 2015, '551.74'),
(14, 12, 2016, '381.06'),
(19, 21, 2014, '14.90'),
(20, 21, 2015, '18.21'),
(21, 21, 2016, '21.61'),
(22, 14, 2014, '4.30'),
(23, 19, 2015, '23.24'),
(24, 13, 2014, '17673.30'),
(25, 13, 2015, '17513.17'),
(26, 13, 2016, '17877.11'),
(27, 14, 2014, '4.30'),
(28, 14, 2015, '23.24'),
(29, 14, 2016, '25.25'),
(30, 20, 2014, '2.10'),
(31, 20, 2015, '2.34'),
(32, 20, 2016, '2.28'),
(33, 16, 2014, '335085.60'),
(34, 16, 2015, '286496.99'),
(35, 16, 2016, '214417.74'),
(36, 17, 2014, '30.50'),
(37, 17, 2015, '27.45'),
(38, 17, 2016, '27.51'),
(39, 18, 2014, '1332.60'),
(40, 18, 2015, '1747.36'),
(41, 18, 2016, '1945.20'),
(42, 19, 2014, '4.70'),
(43, 19, 2015, '101.50'),
(44, 19, 2016, '77.34'),
(45, 20, 2014, '1.60'),
(46, 20, 2015, '0.89'),
(47, 20, 2016, '0.93');

-- --------------------------------------------------------

--
-- Table structure for table `tehnologi_jml_bts`
--

CREATE TABLE `tehnologi_jml_bts` (
  `id_jml_bts` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tehnologi_jml_bts`
--

INSERT INTO `tehnologi_jml_bts` (`id_jml_bts`, `th`, `jml`) VALUES
(5, 2014, 317),
(6, 2015, 342),
(7, 2016, 385);

-- --------------------------------------------------------

--
-- Table structure for table `tehnologi_jml_warnet`
--

CREATE TABLE `tehnologi_jml_warnet` (
  `id_jml_warnet` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tehnologi_jml_warnet`
--

INSERT INTO `tehnologi_jml_warnet` (`id_jml_warnet`, `th`, `jml`) VALUES
(4, 2014, 92),
(5, 2015, 96),
(6, 2016, 97);

-- --------------------------------------------------------

--
-- Table structure for table `tehnologi_jml_web_opd`
--

CREATE TABLE `tehnologi_jml_web_opd` (
  `id_web_opd` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tehnologi_jml_web_opd`
--

INSERT INTO `tehnologi_jml_web_opd` (`id_web_opd`, `th`, `jml`) VALUES
(3, 2014, 100),
(4, 2015, 105),
(5, 2016, 107);

-- --------------------------------------------------------

--
-- Table structure for table `trans_jln`
--

CREATE TABLE `trans_jln` (
  `id_jln` int(11) NOT NULL,
  `kategori_jln` varchar(64) NOT NULL,
  `jln_negara` varchar(20) NOT NULL,
  `jln_prov` varchar(20) NOT NULL,
  `jln_kota` varchar(20) NOT NULL,
  `th` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_jln`
--

INSERT INTO `trans_jln` (`id_jln`, `kategori_jln`, `jln_negara`, `jln_prov`, `jln_kota`, `th`) VALUES
(4, 'Kondisi Jalan Baik', '1.45', '48.45', '135.19', 2015),
(5, 'Kondisi Jalan Baik', '12.64', '10.44', '993.64', 2016),
(6, 'Kondisi Jalan Sedang', '0', '0', '0', 2015),
(7, 'Kondisi Jalan Sedang', '0', '0', '0', 2016),
(8, 'Kondisi Jalan Rusak', '0', '0.5', '5.59', 2015),
(9, 'Kondisi Jalan Rusak', '0', '0.5', '33.47', 2016),
(10, 'Kondisi Jalan Rusak Berat', '0', '0', '0', 2015),
(11, 'Kondisi Jalan Rusak Berat', '0', '0', '0', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `trans_jml_kendaraan`
--

CREATE TABLE `trans_jml_kendaraan` (
  `id_jml_kendaraan` int(11) NOT NULL,
  `id_jenis_kendaraan` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_jml_kendaraan`
--

INSERT INTO `trans_jml_kendaraan` (`id_jml_kendaraan`, `id_jenis_kendaraan`, `th`, `jml`) VALUES
(1, 1, 2014, '75818'),
(2, 1, 2015, '86091'),
(3, 1, 2016, '90058'),
(4, 2, 2014, '861'),
(5, 2, 2015, '934'),
(6, 2, 2016, '966'),
(7, 3, 2014, '17949'),
(8, 3, 2015, '19457'),
(9, 3, 2016, '20002'),
(10, 4, 2014, '392559'),
(11, 4, 2015, '441123'),
(12, 4, 2016, '456693');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `admin_lv`
--
ALTER TABLE `admin_lv`
  ADD PRIMARY KEY (`id_lv`);

--
-- Indexes for table `id_kesehatan_sub_jenis`
--
ALTER TABLE `id_kesehatan_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `iklim_hujan`
--
ALTER TABLE `iklim_hujan`
  ADD PRIMARY KEY (`id_hujan`);

--
-- Indexes for table `iklim_station`
--
ALTER TABLE `iklim_station`
  ADD PRIMARY KEY (`id_st`);

--
-- Indexes for table `index_kes_daya_pend`
--
ALTER TABLE `index_kes_daya_pend`
  ADD PRIMARY KEY (`id_index`);

--
-- Indexes for table `index_pem_manusia`
--
ALTER TABLE `index_pem_manusia`
  ADD PRIMARY KEY (`id_ipm`);

--
-- Indexes for table `kepandudukan_jk`
--
ALTER TABLE `kepandudukan_jk`
  ADD PRIMARY KEY (`id_kepend_jk`);

--
-- Indexes for table `kependudukan_kel_umur`
--
ALTER TABLE `kependudukan_kel_umur`
  ADD PRIMARY KEY (`id_kel_umur`);

--
-- Indexes for table `kependudukan_rasio_ketergantungan`
--
ALTER TABLE `kependudukan_rasio_ketergantungan`
  ADD PRIMARY KEY (`id_rasio`);

--
-- Indexes for table `kerja_angkatan`
--
ALTER TABLE `kerja_angkatan`
  ADD PRIMARY KEY (`id_angkatan`);

--
-- Indexes for table `kerja_pengangguran`
--
ALTER TABLE `kerja_pengangguran`
  ADD PRIMARY KEY (`id_pengangguran`);

--
-- Indexes for table `kerja_ump`
--
ALTER TABLE `kerja_ump`
  ADD PRIMARY KEY (`id_ump`);

--
-- Indexes for table `kesehatan_gizi_balita`
--
ALTER TABLE `kesehatan_gizi_balita`
  ADD PRIMARY KEY (`id_gizi_balita`);

--
-- Indexes for table `keu_jenis`
--
ALTER TABLE `keu_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `keu_jml`
--
ALTER TABLE `keu_jml`
  ADD PRIMARY KEY (`id_jml_keu`);

--
-- Indexes for table `lp_aparat`
--
ALTER TABLE `lp_aparat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lp_aparat_jenis`
--
ALTER TABLE `lp_aparat_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_bencana`
--
ALTER TABLE `lp_bencana`
  ADD PRIMARY KEY (`id_bencana`);

--
-- Indexes for table `lp_bencana_jenis`
--
ALTER TABLE `lp_bencana_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_kajian_penelitian`
--
ALTER TABLE `lp_kajian_penelitian`
  ADD PRIMARY KEY (`id_kajian`);

--
-- Indexes for table `lp_kel_umur`
--
ALTER TABLE `lp_kel_umur`
  ADD PRIMARY KEY (`id_kel_umur`);

--
-- Indexes for table `lp_kel_umur_jenis`
--
ALTER TABLE `lp_kel_umur_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_kendaran_kec`
--
ALTER TABLE `lp_kendaran_kec`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indexes for table `lp_kendaran_plat`
--
ALTER TABLE `lp_kendaran_plat`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indexes for table `lp_kendaran_plat_jenis`
--
ALTER TABLE `lp_kendaran_plat_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_kendaran_umum`
--
ALTER TABLE `lp_kendaran_umum`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indexes for table `lp_kendaran_umum_jenis`
--
ALTER TABLE `lp_kendaran_umum_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_kesehatan`
--
ALTER TABLE `lp_kesehatan`
  ADD PRIMARY KEY (`id_kesehatan`);

--
-- Indexes for table `lp_kesehatan_jenis`
--
ALTER TABLE `lp_kesehatan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_kesehatan_sub3_jenis`
--
ALTER TABLE `lp_kesehatan_sub3_jenis`
  ADD PRIMARY KEY (`id_3sub_jenis`);

--
-- Indexes for table `lp_kesehatan_sub_jenis`
--
ALTER TABLE `lp_kesehatan_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `lp_keu`
--
ALTER TABLE `lp_keu`
  ADD PRIMARY KEY (`id_keu`);

--
-- Indexes for table `lp_keu_jenis`
--
ALTER TABLE `lp_keu_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_keu_sub_jenis`
--
ALTER TABLE `lp_keu_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `lp_kooperasi_jenis`
--
ALTER TABLE `lp_kooperasi_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_kooperasi_sub_jenis`
--
ALTER TABLE `lp_kooperasi_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `lp_koperasi`
--
ALTER TABLE `lp_koperasi`
  ADD PRIMARY KEY (`id_koperasi`);

--
-- Indexes for table `lp_lahan`
--
ALTER TABLE `lp_lahan`
  ADD PRIMARY KEY (`id_lahan`);

--
-- Indexes for table `lp_lahan_jenis`
--
ALTER TABLE `lp_lahan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_miskin`
--
ALTER TABLE `lp_miskin`
  ADD PRIMARY KEY (`id_miskin`);

--
-- Indexes for table `lp_miskin_jenis`
--
ALTER TABLE `lp_miskin_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_or`
--
ALTER TABLE `lp_or`
  ADD PRIMARY KEY (`id_or`);

--
-- Indexes for table `lp_or_jenis`
--
ALTER TABLE `lp_or_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pajak`
--
ALTER TABLE `lp_pajak`
  ADD PRIMARY KEY (`id_pajak`);

--
-- Indexes for table `lp_pajak_jenis`
--
ALTER TABLE `lp_pajak_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_panen`
--
ALTER TABLE `lp_panen`
  ADD PRIMARY KEY (`id_panen`);

--
-- Indexes for table `lp_panen_jenis`
--
ALTER TABLE `lp_panen_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pdb`
--
ALTER TABLE `lp_pdb`
  ADD PRIMARY KEY (`id_pdb`);

--
-- Indexes for table `lp_pdb_jenis`
--
ALTER TABLE `lp_pdb_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pendidikan`
--
ALTER TABLE `lp_pendidikan`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `lp_pendidikan_jenis`
--
ALTER TABLE `lp_pendidikan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pendidikan_kategori`
--
ALTER TABLE `lp_pendidikan_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `lp_penduduk_jk`
--
ALTER TABLE `lp_penduduk_jk`
  ADD PRIMARY KEY (`id_penduduk`);

--
-- Indexes for table `lp_pend_industri`
--
ALTER TABLE `lp_pend_industri`
  ADD PRIMARY KEY (`id_pend`);

--
-- Indexes for table `lp_pend_industri_jenis`
--
ALTER TABLE `lp_pend_industri_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pend_industri_kategori`
--
ALTER TABLE `lp_pend_industri_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `lp_perpustakaan`
--
ALTER TABLE `lp_perpustakaan`
  ADD PRIMARY KEY (`id_perpustakaan`);

--
-- Indexes for table `lp_perpustakaan_jenis`
--
ALTER TABLE `lp_perpustakaan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pertanian`
--
ALTER TABLE `lp_pertanian`
  ADD PRIMARY KEY (`id_pertanian`);

--
-- Indexes for table `lp_pertanian_jenis`
--
ALTER TABLE `lp_pertanian_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pertanian_sub_jenis`
--
ALTER TABLE `lp_pertanian_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `lp_pidana`
--
ALTER TABLE `lp_pidana`
  ADD PRIMARY KEY (`id_pidana`);

--
-- Indexes for table `lp_pidana_jenis`
--
ALTER TABLE `lp_pidana_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pidana_kec`
--
ALTER TABLE `lp_pidana_kec`
  ADD PRIMARY KEY (`id_pidana`);

--
-- Indexes for table `lp_pmks`
--
ALTER TABLE `lp_pmks`
  ADD PRIMARY KEY (`id_pmks`);

--
-- Indexes for table `lp_pmks_jenis`
--
ALTER TABLE `lp_pmks_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_tehnologi`
--
ALTER TABLE `lp_tehnologi`
  ADD PRIMARY KEY (`id_tehnologi`);

--
-- Indexes for table `lp_tehnologi_jenis`
--
ALTER TABLE `lp_tehnologi_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_tehnologi_sub_jenis`
--
ALTER TABLE `lp_tehnologi_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `lp_tenaga`
--
ALTER TABLE `lp_tenaga`
  ADD PRIMARY KEY (`id_tenaga`);

--
-- Indexes for table `lp_tenaga_jenis`
--
ALTER TABLE `lp_tenaga_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_tenaga_kategori`
--
ALTER TABLE `lp_tenaga_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `lp_terminal`
--
ALTER TABLE `lp_terminal`
  ADD PRIMARY KEY (`id_terminal`);

--
-- Indexes for table `lp_terminal_jenis`
--
ALTER TABLE `lp_terminal_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_terminal_sub_jenis`
--
ALTER TABLE `lp_terminal_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `lp_tpa`
--
ALTER TABLE `lp_tpa`
  ADD PRIMARY KEY (`id_tpa`);

--
-- Indexes for table `main_dinas`
--
ALTER TABLE `main_dinas`
  ADD PRIMARY KEY (`id_dinas`);

--
-- Indexes for table `main_jenis_kendaraan`
--
ALTER TABLE `main_jenis_kendaraan`
  ADD PRIMARY KEY (`id_jenis_kendaraan`);

--
-- Indexes for table `main_jenis_penyakit`
--
ALTER TABLE `main_jenis_penyakit`
  ADD PRIMARY KEY (`id_jenis_penyakit`);

--
-- Indexes for table `main_jenjang_pend`
--
ALTER TABLE `main_jenjang_pend`
  ADD PRIMARY KEY (`id_jenjang`);

--
-- Indexes for table `main_kecamatan`
--
ALTER TABLE `main_kecamatan`
  ADD PRIMARY KEY (`id_kec`);

--
-- Indexes for table `main_kelurahan`
--
ALTER TABLE `main_kelurahan`
  ADD PRIMARY KEY (`id_kel`);

--
-- Indexes for table `main_partai`
--
ALTER TABLE `main_partai`
  ADD PRIMARY KEY (`id_partai`);

--
-- Indexes for table `main_pendidikan`
--
ALTER TABLE `main_pendidikan`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `miskin_jml_pend`
--
ALTER TABLE `miskin_jml_pend`
  ADD PRIMARY KEY (`id_pend_miskin`);

--
-- Indexes for table `miskin_pengeluaran_perkapita`
--
ALTER TABLE `miskin_pengeluaran_perkapita`
  ADD PRIMARY KEY (`id_perkapita`);

--
-- Indexes for table `pem_dpr`
--
ALTER TABLE `pem_dpr`
  ADD PRIMARY KEY (`id_pem_dpr`);

--
-- Indexes for table `pem_jml_asn_jenjang`
--
ALTER TABLE `pem_jml_asn_jenjang`
  ADD PRIMARY KEY (`id_jml_asn`);

--
-- Indexes for table `pem_jml_rt_rw`
--
ALTER TABLE `pem_jml_rt_rw`
  ADD PRIMARY KEY (`id_rt_rw`);

--
-- Indexes for table `pendapatan_lap_usaha`
--
ALTER TABLE `pendapatan_lap_usaha`
  ADD PRIMARY KEY (`id_lap_usaha`);

--
-- Indexes for table `pendapatan_lap_usaha_jenis`
--
ALTER TABLE `pendapatan_lap_usaha_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendapatan_regional`
--
ALTER TABLE `pendapatan_regional`
  ADD PRIMARY KEY (`id_pendapatan`);

--
-- Indexes for table `pendidikan_baca_tulis`
--
ALTER TABLE `pendidikan_baca_tulis`
  ADD PRIMARY KEY (`id_baca_tulis`);

--
-- Indexes for table `pendidikan_baca_tulis_jenis`
--
ALTER TABLE `pendidikan_baca_tulis_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_jml_sklh`
--
ALTER TABLE `pendidikan_jml_sklh`
  ADD PRIMARY KEY (`id_jml_sklh`);

--
-- Indexes for table `pendidikan_jml_sklh_jenis`
--
ALTER TABLE `pendidikan_jml_sklh_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_partisipasi_sklh`
--
ALTER TABLE `pendidikan_partisipasi_sklh`
  ADD PRIMARY KEY (`id_partisipasi_sklh`);

--
-- Indexes for table `pendidikan_partisipasi_sklh_jenis`
--
ALTER TABLE `pendidikan_partisipasi_sklh_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_penduduk`
--
ALTER TABLE `pendidikan_penduduk`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `pendidikan_penduduk_jenis`
--
ALTER TABLE `pendidikan_penduduk_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_rasio_guru_murid`
--
ALTER TABLE `pendidikan_rasio_guru_murid`
  ADD PRIMARY KEY (`id_rasio`);

--
-- Indexes for table `pendidikan_rasio_guru_murid_jenis`
--
ALTER TABLE `pendidikan_rasio_guru_murid_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pengeluaran_penduduk`
--
ALTER TABLE `pengeluaran_penduduk`
  ADD PRIMARY KEY (`id_pengeluaran`);

--
-- Indexes for table `pengeluaran_penduduk_jenis`
--
ALTER TABLE `pengeluaran_penduduk_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pengeluaran_rmt_tgg`
--
ALTER TABLE `pengeluaran_rmt_tgg`
  ADD PRIMARY KEY (`id_rmt_tgg`);

--
-- Indexes for table `pertanian_holti_jenis`
--
ALTER TABLE `pertanian_holti_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pertanian_holti_jml`
--
ALTER TABLE `pertanian_holti_jml`
  ADD PRIMARY KEY (`id_holti`);

--
-- Indexes for table `pertanian_ikan_jenis`
--
ALTER TABLE `pertanian_ikan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pertanian_ikan_jml`
--
ALTER TABLE `pertanian_ikan_jml`
  ADD PRIMARY KEY (`id_ikan`);

--
-- Indexes for table `pertanian_kelapa_tebu`
--
ALTER TABLE `pertanian_kelapa_tebu`
  ADD PRIMARY KEY (`id_pertanian`);

--
-- Indexes for table `pertanian_komoditas_jenis`
--
ALTER TABLE `pertanian_komoditas_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pertanian_komoditas_jml`
--
ALTER TABLE `pertanian_komoditas_jml`
  ADD PRIMARY KEY (`id_komoditas`);

--
-- Indexes for table `pertanian_lahan`
--
ALTER TABLE `pertanian_lahan`
  ADD PRIMARY KEY (`id_lahan`);

--
-- Indexes for table `pertanian_lahan_penggunaan`
--
ALTER TABLE `pertanian_lahan_penggunaan`
  ADD PRIMARY KEY (`id_peng`);

--
-- Indexes for table `pertanian_ternak_jenis`
--
ALTER TABLE `pertanian_ternak_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pertanian_ternak_jml`
--
ALTER TABLE `pertanian_ternak_jml`
  ADD PRIMARY KEY (`id_ternak`);

--
-- Indexes for table `tehnologi_jml_bts`
--
ALTER TABLE `tehnologi_jml_bts`
  ADD PRIMARY KEY (`id_jml_bts`);

--
-- Indexes for table `tehnologi_jml_warnet`
--
ALTER TABLE `tehnologi_jml_warnet`
  ADD PRIMARY KEY (`id_jml_warnet`);

--
-- Indexes for table `tehnologi_jml_web_opd`
--
ALTER TABLE `tehnologi_jml_web_opd`
  ADD PRIMARY KEY (`id_web_opd`);

--
-- Indexes for table `trans_jln`
--
ALTER TABLE `trans_jln`
  ADD PRIMARY KEY (`id_jln`);

--
-- Indexes for table `trans_jml_kendaraan`
--
ALTER TABLE `trans_jml_kendaraan`
  ADD PRIMARY KEY (`id_jml_kendaraan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_lv`
--
ALTER TABLE `admin_lv`
  MODIFY `id_lv` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `id_kesehatan_sub_jenis`
--
ALTER TABLE `id_kesehatan_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `iklim_hujan`
--
ALTER TABLE `iklim_hujan`
  MODIFY `id_hujan` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `iklim_station`
--
ALTER TABLE `iklim_station`
  MODIFY `id_st` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `index_kes_daya_pend`
--
ALTER TABLE `index_kes_daya_pend`
  MODIFY `id_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `index_pem_manusia`
--
ALTER TABLE `index_pem_manusia`
  MODIFY `id_ipm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kepandudukan_jk`
--
ALTER TABLE `kepandudukan_jk`
  MODIFY `id_kepend_jk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kependudukan_kel_umur`
--
ALTER TABLE `kependudukan_kel_umur`
  MODIFY `id_kel_umur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `kependudukan_rasio_ketergantungan`
--
ALTER TABLE `kependudukan_rasio_ketergantungan`
  MODIFY `id_rasio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kerja_angkatan`
--
ALTER TABLE `kerja_angkatan`
  MODIFY `id_angkatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kerja_pengangguran`
--
ALTER TABLE `kerja_pengangguran`
  MODIFY `id_pengangguran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kerja_ump`
--
ALTER TABLE `kerja_ump`
  MODIFY `id_ump` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kesehatan_gizi_balita`
--
ALTER TABLE `kesehatan_gizi_balita`
  MODIFY `id_gizi_balita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `keu_jenis`
--
ALTER TABLE `keu_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `keu_jml`
--
ALTER TABLE `keu_jml`
  MODIFY `id_jml_keu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `lp_aparat`
--
ALTER TABLE `lp_aparat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `lp_aparat_jenis`
--
ALTER TABLE `lp_aparat_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `lp_bencana`
--
ALTER TABLE `lp_bencana`
  MODIFY `id_bencana` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_bencana_jenis`
--
ALTER TABLE `lp_bencana_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_kajian_penelitian`
--
ALTER TABLE `lp_kajian_penelitian`
  MODIFY `id_kajian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `lp_kel_umur`
--
ALTER TABLE `lp_kel_umur`
  MODIFY `id_kel_umur` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_kel_umur_jenis`
--
ALTER TABLE `lp_kel_umur_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_kendaran_kec`
--
ALTER TABLE `lp_kendaran_kec`
  MODIFY `id_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `lp_kendaran_plat`
--
ALTER TABLE `lp_kendaran_plat`
  MODIFY `id_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `lp_kendaran_plat_jenis`
--
ALTER TABLE `lp_kendaran_plat_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `lp_kendaran_umum`
--
ALTER TABLE `lp_kendaran_umum`
  MODIFY `id_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `lp_kendaran_umum_jenis`
--
ALTER TABLE `lp_kendaran_umum_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lp_kesehatan`
--
ALTER TABLE `lp_kesehatan`
  MODIFY `id_kesehatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `lp_kesehatan_jenis`
--
ALTER TABLE `lp_kesehatan_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `lp_kesehatan_sub3_jenis`
--
ALTER TABLE `lp_kesehatan_sub3_jenis`
  MODIFY `id_3sub_jenis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_kesehatan_sub_jenis`
--
ALTER TABLE `lp_kesehatan_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `lp_keu`
--
ALTER TABLE `lp_keu`
  MODIFY `id_keu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `lp_keu_jenis`
--
ALTER TABLE `lp_keu_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lp_keu_sub_jenis`
--
ALTER TABLE `lp_keu_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `lp_kooperasi_jenis`
--
ALTER TABLE `lp_kooperasi_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `lp_kooperasi_sub_jenis`
--
ALTER TABLE `lp_kooperasi_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `lp_koperasi`
--
ALTER TABLE `lp_koperasi`
  MODIFY `id_koperasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `lp_lahan`
--
ALTER TABLE `lp_lahan`
  MODIFY `id_lahan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_lahan_jenis`
--
ALTER TABLE `lp_lahan_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_miskin`
--
ALTER TABLE `lp_miskin`
  MODIFY `id_miskin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `lp_miskin_jenis`
--
ALTER TABLE `lp_miskin_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lp_or`
--
ALTER TABLE `lp_or`
  MODIFY `id_or` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `lp_or_jenis`
--
ALTER TABLE `lp_or_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `lp_pajak`
--
ALTER TABLE `lp_pajak`
  MODIFY `id_pajak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `lp_pajak_jenis`
--
ALTER TABLE `lp_pajak_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `lp_panen`
--
ALTER TABLE `lp_panen`
  MODIFY `id_panen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `lp_panen_jenis`
--
ALTER TABLE `lp_panen_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lp_pdb`
--
ALTER TABLE `lp_pdb`
  MODIFY `id_pdb` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_pdb_jenis`
--
ALTER TABLE `lp_pdb_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_pendidikan`
--
ALTER TABLE `lp_pendidikan`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=782;

--
-- AUTO_INCREMENT for table `lp_pendidikan_jenis`
--
ALTER TABLE `lp_pendidikan_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lp_pendidikan_kategori`
--
ALTER TABLE `lp_pendidikan_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `lp_penduduk_jk`
--
ALTER TABLE `lp_penduduk_jk`
  MODIFY `id_penduduk` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_pend_industri`
--
ALTER TABLE `lp_pend_industri`
  MODIFY `id_pend` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `lp_pend_industri_jenis`
--
ALTER TABLE `lp_pend_industri_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lp_pend_industri_kategori`
--
ALTER TABLE `lp_pend_industri_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lp_perpustakaan`
--
ALTER TABLE `lp_perpustakaan`
  MODIFY `id_perpustakaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `lp_perpustakaan_jenis`
--
ALTER TABLE `lp_perpustakaan_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `lp_pertanian`
--
ALTER TABLE `lp_pertanian`
  MODIFY `id_pertanian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;

--
-- AUTO_INCREMENT for table `lp_pertanian_jenis`
--
ALTER TABLE `lp_pertanian_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lp_pertanian_sub_jenis`
--
ALTER TABLE `lp_pertanian_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `lp_pidana`
--
ALTER TABLE `lp_pidana`
  MODIFY `id_pidana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `lp_pidana_jenis`
--
ALTER TABLE `lp_pidana_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `lp_pidana_kec`
--
ALTER TABLE `lp_pidana_kec`
  MODIFY `id_pidana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `lp_pmks`
--
ALTER TABLE `lp_pmks`
  MODIFY `id_pmks` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_pmks_jenis`
--
ALTER TABLE `lp_pmks_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_tehnologi`
--
ALTER TABLE `lp_tehnologi`
  MODIFY `id_tehnologi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `lp_tehnologi_jenis`
--
ALTER TABLE `lp_tehnologi_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lp_tehnologi_sub_jenis`
--
ALTER TABLE `lp_tehnologi_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `lp_tenaga`
--
ALTER TABLE `lp_tenaga`
  MODIFY `id_tenaga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `lp_tenaga_jenis`
--
ALTER TABLE `lp_tenaga_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `lp_tenaga_kategori`
--
ALTER TABLE `lp_tenaga_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lp_terminal`
--
ALTER TABLE `lp_terminal`
  MODIFY `id_terminal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `lp_terminal_jenis`
--
ALTER TABLE `lp_terminal_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lp_terminal_sub_jenis`
--
ALTER TABLE `lp_terminal_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `lp_tpa`
--
ALTER TABLE `lp_tpa`
  MODIFY `id_tpa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `main_dinas`
--
ALTER TABLE `main_dinas`
  MODIFY `id_dinas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `main_jenis_kendaraan`
--
ALTER TABLE `main_jenis_kendaraan`
  MODIFY `id_jenis_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `main_jenis_penyakit`
--
ALTER TABLE `main_jenis_penyakit`
  MODIFY `id_jenis_penyakit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `main_jenjang_pend`
--
ALTER TABLE `main_jenjang_pend`
  MODIFY `id_jenjang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_partai`
--
ALTER TABLE `main_partai`
  MODIFY `id_partai` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `main_pendidikan`
--
ALTER TABLE `main_pendidikan`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `miskin_jml_pend`
--
ALTER TABLE `miskin_jml_pend`
  MODIFY `id_pend_miskin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `miskin_pengeluaran_perkapita`
--
ALTER TABLE `miskin_pengeluaran_perkapita`
  MODIFY `id_perkapita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pem_dpr`
--
ALTER TABLE `pem_dpr`
  MODIFY `id_pem_dpr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pem_jml_rt_rw`
--
ALTER TABLE `pem_jml_rt_rw`
  MODIFY `id_rt_rw` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pendapatan_lap_usaha`
--
ALTER TABLE `pendapatan_lap_usaha`
  MODIFY `id_lap_usaha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pendapatan_lap_usaha_jenis`
--
ALTER TABLE `pendapatan_lap_usaha_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pendapatan_regional`
--
ALTER TABLE `pendapatan_regional`
  MODIFY `id_pendapatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pendidikan_baca_tulis`
--
ALTER TABLE `pendidikan_baca_tulis`
  MODIFY `id_baca_tulis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pendidikan_baca_tulis_jenis`
--
ALTER TABLE `pendidikan_baca_tulis_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pendidikan_jml_sklh`
--
ALTER TABLE `pendidikan_jml_sklh`
  MODIFY `id_jml_sklh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `pendidikan_jml_sklh_jenis`
--
ALTER TABLE `pendidikan_jml_sklh_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pendidikan_partisipasi_sklh`
--
ALTER TABLE `pendidikan_partisipasi_sklh`
  MODIFY `id_partisipasi_sklh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pendidikan_partisipasi_sklh_jenis`
--
ALTER TABLE `pendidikan_partisipasi_sklh_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pendidikan_penduduk`
--
ALTER TABLE `pendidikan_penduduk`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pendidikan_penduduk_jenis`
--
ALTER TABLE `pendidikan_penduduk_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pendidikan_rasio_guru_murid`
--
ALTER TABLE `pendidikan_rasio_guru_murid`
  MODIFY `id_rasio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `pendidikan_rasio_guru_murid_jenis`
--
ALTER TABLE `pendidikan_rasio_guru_murid_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pengeluaran_penduduk`
--
ALTER TABLE `pengeluaran_penduduk`
  MODIFY `id_pengeluaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pengeluaran_penduduk_jenis`
--
ALTER TABLE `pengeluaran_penduduk_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pengeluaran_rmt_tgg`
--
ALTER TABLE `pengeluaran_rmt_tgg`
  MODIFY `id_rmt_tgg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pertanian_holti_jenis`
--
ALTER TABLE `pertanian_holti_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `pertanian_holti_jml`
--
ALTER TABLE `pertanian_holti_jml`
  MODIFY `id_holti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `pertanian_ikan_jenis`
--
ALTER TABLE `pertanian_ikan_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pertanian_ikan_jml`
--
ALTER TABLE `pertanian_ikan_jml`
  MODIFY `id_ikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pertanian_kelapa_tebu`
--
ALTER TABLE `pertanian_kelapa_tebu`
  MODIFY `id_pertanian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pertanian_komoditas_jenis`
--
ALTER TABLE `pertanian_komoditas_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pertanian_komoditas_jml`
--
ALTER TABLE `pertanian_komoditas_jml`
  MODIFY `id_komoditas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pertanian_lahan`
--
ALTER TABLE `pertanian_lahan`
  MODIFY `id_lahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pertanian_lahan_penggunaan`
--
ALTER TABLE `pertanian_lahan_penggunaan`
  MODIFY `id_peng` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pertanian_ternak_jenis`
--
ALTER TABLE `pertanian_ternak_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `pertanian_ternak_jml`
--
ALTER TABLE `pertanian_ternak_jml`
  MODIFY `id_ternak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `tehnologi_jml_bts`
--
ALTER TABLE `tehnologi_jml_bts`
  MODIFY `id_jml_bts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tehnologi_jml_warnet`
--
ALTER TABLE `tehnologi_jml_warnet`
  MODIFY `id_jml_warnet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tehnologi_jml_web_opd`
--
ALTER TABLE `tehnologi_jml_web_opd`
  MODIFY `id_web_opd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `trans_jln`
--
ALTER TABLE `trans_jln`
  MODIFY `id_jln` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `trans_jml_kendaraan`
--
ALTER TABLE `trans_jml_kendaraan`
  MODIFY `id_jml_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2018 at 10:00 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mop_new`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin` (`id_lv` VARCHAR(2), `email` VARCHAR(20), `pass` VARCHAR(32), `nama` VARCHAR(64), `nip` VARCHAR(20), `jabatan` VARCHAR(50), `id_dinas` INT(3)) RETURNS VARCHAR(16) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(16);
  
  select count(*) into count_row_user from admin;
        
  select id_admin into last_key_user from admin	order by id_admin desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"000001");
  else
    set fix_key_user = concat("AD",SUBSTR(last_key_user,3,13)+1);  
  END IF;
  
  insert into admin values(fix_key_user, id_lv, email, pass, "1", nama, nip, jabatan, id_dinas, "0");
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kecamatan` (`nama` VARCHAR(50), `latlng` TEXT, `luas` DOUBLE) RETURNS VARCHAR(7) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from main_kecamatan;
        
  select id_kec into last_key_user from main_kecamatan order by id_kec desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("KEC1","001");
  else
    set fix_key_user = concat("KEC",SUBSTR(last_key_user,4,4)+1);  
  END IF;
  
  INSERT INTO main_kecamatan VALUES(fix_key_user, nama, latlng, luas, "0");
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kelurahan` (`nama` VARCHAR(64), `luas` DOUBLE, `latlng` TEXT, `id_kec` VARCHAR(7)) RETURNS VARCHAR(7) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from main_kelurahan;
        
  select id_kel into last_key_user from main_kelurahan order by id_kel desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("KEL1","001");
  else
    set fix_key_user = concat("KEL",SUBSTR(last_key_user,4,4)+1);  
  END IF;
  
  INSERT INTO main_kelurahan VALUES(fix_key_user, id_kec, nama, latlng, luas, "0");
  
  return fix_key_user;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(16) NOT NULL,
  `id_lv` int(2) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_dinas` int(3) NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `id_lv`, `email`, `password`, `status_active`, `nama`, `nip`, `jabatan`, `id_dinas`, `is_delete`) VALUES
('AD201811000001', 1, 'dsfsd', 'aff8fbcbf1363cd7edc85a1e11391173', '1', 'surya', '1234', 'surya', 3, '0'),
('AD201811000002', 3, 'surya', 'surya', '1', 'surya', '1234', 'surya', 20, '1'),
('AD201811000003', 1, 'kominfo', 'f970e2767d0cfe75876ea857f92e319b', '1', 'doms rian', '21312312', 'kepala', 2, '0'),
('AD201811000004', 2, 'surya', 'admin', '1', 'surya hanggara', '123343', 'makijan', 4, '1'),
('AD201811000005', 1, 'surya', 'aff8fbcbf1363cd7edc85a1e11391173', '1', 'surya', '3573011903940006', 'pengolah data', 3, '0');

-- --------------------------------------------------------

--
-- Table structure for table `admin_lv`
--

CREATE TABLE `admin_lv` (
  `id_lv` int(2) NOT NULL,
  `ket` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_lv`
--

INSERT INTO `admin_lv` (`id_lv`, `ket`) VALUES
(1, 'Admin Super'),
(2, 'Admin STATISTIK'),
(3, 'Admin OPD');

-- --------------------------------------------------------

--
-- Table structure for table `iklim_hujan`
--

CREATE TABLE `iklim_hujan` (
  `id_hujan` int(6) NOT NULL,
  `id_st` int(6) NOT NULL,
  `th` int(4) NOT NULL,
  `periode` int(2) NOT NULL,
  `jml_cura` double NOT NULL,
  `jml_hari` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iklim_hujan`
--

INSERT INTO `iklim_hujan` (`id_hujan`, `id_st`, `th`, `periode`, `jml_cura`, `jml_hari`) VALUES
(1, 3, 2018, 1, 122, 12),
(3, 3, 2018, 2, 142, 12),
(4, 4, 2018, 1, 135, 13),
(5, 3, 2017, 1, 292, 16),
(6, 4, 2017, 1, 62, 10),
(7, 5, 2017, 1, 142, 10),
(8, 3, 2017, 2, 588, 25),
(9, 4, 2017, 2, 477, 30),
(10, 5, 2017, 2, 685, 24),
(11, 3, 2017, 3, 237, 24),
(12, 4, 2017, 3, 405, 18),
(13, 5, 2017, 3, 388, 18),
(14, 3, 2017, 4, 148, 12),
(15, 4, 2017, 4, 103, 10),
(16, 5, 2017, 4, 196, 11),
(17, 3, 2017, 5, 177, 18),
(18, 4, 2017, 5, 146, 12),
(19, 5, 2017, 5, 247, 12),
(20, 3, 2017, 6, 210, 13),
(21, 4, 2017, 6, 183, 10),
(22, 5, 2017, 6, 280, 12),
(23, 3, 2017, 7, 70, 10),
(24, 4, 2017, 7, 34, 6),
(25, 5, 2017, 7, 67, 11),
(26, 3, 2017, 8, 125, 7),
(27, 4, 2017, 8, 3, 3),
(28, 5, 2017, 8, 78, 5),
(29, 3, 2017, 9, 35, 6),
(30, 4, 2017, 9, 3, 3),
(31, 5, 2017, 9, 70, 7),
(32, 3, 2017, 10, 208, 14),
(33, 4, 2017, 10, 4, 4),
(34, 5, 2017, 10, 196, 14),
(35, 3, 2017, 11, 425, 20),
(36, 4, 2017, 11, 8, 8),
(37, 5, 2017, 11, 677, 25),
(38, 3, 2017, 12, 144, 14),
(39, 4, 2017, 12, 171, 9),
(40, 5, 2017, 12, 295, 18),
(41, 3, 2016, 1, 291, 15),
(42, 4, 2016, 1, 61, 8),
(43, 5, 2016, 1, 140, 9),
(44, 3, 2016, 2, 586, 23),
(45, 4, 2016, 2, 476, 29),
(46, 5, 2016, 2, 683, 22),
(47, 3, 2016, 3, 235, 21),
(48, 4, 2016, 3, 402, 16),
(49, 5, 2016, 3, 387, 16),
(50, 3, 2016, 4, 147, 11),
(51, 4, 2016, 4, 101, 8),
(52, 5, 2016, 4, 194, 9),
(53, 3, 2016, 5, 176, 16),
(54, 4, 2016, 5, 145, 11),
(55, 5, 2016, 5, 246, 11),
(56, 3, 2016, 6, 208, 12),
(57, 4, 2016, 6, 181, 8),
(58, 5, 2016, 6, 279, 10),
(59, 5, 2016, 6, 279, 10),
(60, 3, 2016, 7, 69, 8),
(61, 4, 2016, 7, 32, 4),
(62, 5, 2016, 8, 124, 6),
(63, 4, 2016, 8, 0, 0),
(64, 5, 2016, 8, 77, 3),
(65, 3, 2016, 9, 33, 5),
(66, 4, 2016, 9, 0, 0),
(67, 5, 2016, 9, 69, 5),
(68, 3, 2016, 10, 207, 13),
(69, 4, 2016, 10, 0, 0),
(70, 5, 2016, 10, 195, 12),
(71, 3, 2016, 11, 424, 19),
(72, 4, 2016, 11, 0, 0),
(73, 5, 2016, 11, 675, 22),
(74, 3, 2016, 12, 143, 12),
(75, 4, 2016, 12, 170, 8),
(76, 5, 2016, 12, 294, 17);

-- --------------------------------------------------------

--
-- Table structure for table `iklim_station`
--

CREATE TABLE `iklim_station` (
  `id_st` int(6) NOT NULL,
  `ket_st` varchar(50) NOT NULL,
  `alamat_st` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iklim_station`
--

INSERT INTO `iklim_station` (`id_st`, `ket_st`, `alamat_st`) VALUES
(3, 'Ciliwung', 'malang'),
(4, 'Kedung Kandang', 'malang'),
(5, 'Sukun', 'malang');

-- --------------------------------------------------------

--
-- Table structure for table `index_kes_daya_pend`
--

CREATE TABLE `index_kes_daya_pend` (
  `id_index` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `in_pend` varchar(10) NOT NULL,
  `in_kes` varchar(10) NOT NULL,
  `in_daya` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `index_kes_daya_pend`
--

INSERT INTO `index_kes_daya_pend` (`id_index`, `th`, `in_pend`, `in_kes`, `in_daya`) VALUES
(3, 2014, '73.43', '80.46', '83.33'),
(4, 2015, '76.05', '80.92', '83.37'),
(5, 2016, '76.52', '81.05', '83.98');

-- --------------------------------------------------------

--
-- Table structure for table `index_pem_manusia`
--

CREATE TABLE `index_pem_manusia` (
  `id_ipm` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `ipm` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `index_pem_manusia`
--

INSERT INTO `index_pem_manusia` (`id_ipm`, `th`, `ipm`) VALUES
(3, 2014, '78.96'),
(4, 2015, '80.05'),
(5, 2016, '80.46');

-- --------------------------------------------------------

--
-- Table structure for table `kepandudukan_jk`
--

CREATE TABLE `kepandudukan_jk` (
  `id_kepend_jk` int(11) NOT NULL,
  `th` int(11) NOT NULL,
  `rasio_jk` varchar(10) NOT NULL,
  `t_cowo` varchar(20) NOT NULL,
  `t_cewe` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kepandudukan_jk`
--

INSERT INTO `kepandudukan_jk` (`id_kepend_jk`, `th`, `rasio_jk`, `t_cowo`, `t_cewe`) VALUES
(3, 2014, '97.20', '416982', '428991'),
(4, 2015, '97.25', '419713', '431585'),
(5, 2016, '97.27', '422276', '434134');

-- --------------------------------------------------------

--
-- Table structure for table `kependudukan_kel_umur`
--

CREATE TABLE `kependudukan_kel_umur` (
  `id_kel_umur` int(11) NOT NULL,
  `kelompok` varchar(50) NOT NULL,
  `periode` int(11) NOT NULL,
  `jml_penduduk` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kependudukan_kel_umur`
--

INSERT INTO `kependudukan_kel_umur` (`id_kel_umur`, `kelompok`, `periode`, `jml_penduduk`) VALUES
(4, '0 - 14', 2014, '183388'),
(5, '0 - 14', 2015, '180358'),
(6, '0 - 14', 2016, '179299'),
(7, '15 - 64', 2014, '614628'),
(8, '15 - 64', 2015, '620802'),
(9, '15 - 64', 2016, '625473'),
(10, '> 65', 2014, '47957'),
(11, '> 65', 2015, '50138'),
(12, '> 65', 2016, '51638');

-- --------------------------------------------------------

--
-- Table structure for table `kependudukan_rasio_ketergantungan`
--

CREATE TABLE `kependudukan_rasio_ketergantungan` (
  `id_rasio` int(11) NOT NULL,
  `th_rasio` int(4) NOT NULL,
  `rasio_keter` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kependudukan_rasio_ketergantungan`
--

INSERT INTO `kependudukan_rasio_ketergantungan` (`id_rasio`, `th_rasio`, `rasio_keter`) VALUES
(3, 2014, '37.64'),
(4, 2015, '37.13'),
(5, 2016, '36.92');

-- --------------------------------------------------------

--
-- Table structure for table `kerja_angkatan`
--

CREATE TABLE `kerja_angkatan` (
  `id_angkatan` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_kerja` varchar(20) NOT NULL,
  `jml_no_kerja` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kerja_angkatan`
--

INSERT INTO `kerja_angkatan` (`id_angkatan`, `th`, `jml_kerja`, `jml_no_kerja`) VALUES
(3, 2014, '393050', '30581'),
(4, 2015, '377329', '29606'),
(5, 2016, '377329', '29606');

-- --------------------------------------------------------

--
-- Table structure for table `kerja_pengangguran`
--

CREATE TABLE `kerja_pengangguran` (
  `id_pengangguran` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kerja_pengangguran`
--

INSERT INTO `kerja_pengangguran` (`id_pengangguran`, `th`, `jml`) VALUES
(3, 2014, '6920'),
(4, 2015, '6257'),
(5, 2017, '6194');

-- --------------------------------------------------------

--
-- Table structure for table `kerja_ump`
--

CREATE TABLE `kerja_ump` (
  `id_ump` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kerja_ump`
--

INSERT INTO `kerja_ump` (`id_ump`, `th`, `jml`) VALUES
(4, 2014, '1587000'),
(5, 2015, '1882500'),
(6, 2016, '2099000');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_gizi_balita`
--

CREATE TABLE `kesehatan_gizi_balita` (
  `id_gizi_balita` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_gizi_buruk` varchar(10) NOT NULL,
  `jmh_kurang_gizi` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_gizi_balita`
--

INSERT INTO `kesehatan_gizi_balita` (`id_gizi_balita`, `th`, `jml_gizi_buruk`, `jmh_kurang_gizi`) VALUES
(3, 2014, '119', '1611'),
(4, 2015, '100', '1627'),
(5, 2016, '66', '1868');

-- --------------------------------------------------------

--
-- Table structure for table `keu_jenis`
--

CREATE TABLE `keu_jenis` (
  `id_jenis` int(11) NOT NULL,
  `ket` varchar(100) NOT NULL,
  `kategori` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keu_jenis`
--

INSERT INTO `keu_jenis` (`id_jenis`, `ket`, `kategori`) VALUES
(6, 'Bagi Hasil Pajak', '0'),
(7, 'Bagi Hasil Bukan Pajak / Sumber Daya Alam', '0'),
(8, 'Dana Alokasi Umum', '0'),
(9, 'Dana Alokasi Khusus', '0'),
(10, 'Belanja Pegawai', '1'),
(11, 'Belanja Modal', '1'),
(12, 'Belanja Lainnya', '1'),
(13, 'Pendapatan Asli Daerah', '0'),
(15, 'Lain-lain Pendapatan yang Sah', '0');

-- --------------------------------------------------------

--
-- Table structure for table `keu_jml`
--

CREATE TABLE `keu_jml` (
  `id_jml_keu` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keu_jml`
--

INSERT INTO `keu_jml` (`id_jml_keu`, `id_jenis`, `th`, `jml`) VALUES
(3, 6, 2014, '66740371.27'),
(4, 7, 2014, '50203519.87'),
(5, 8, 2014, '808447825'),
(6, 9, 2014, '31304060'),
(7, 10, 2014, '877246394.71'),
(8, 11, 2014, '318462052.42'),
(9, 12, 2014, '407291403.01'),
(10, 13, 2014, '372545396.29'),
(11, 14, 2014, '956695776.14'),
(12, 15, 2014, '435623517.23'),
(13, 16, 2014, '1764864689.66'),
(14, 17, 2014, '1602999850.14'),
(15, 6, 2015, '33850624'),
(16, 7, 2015, '53164497.78'),
(17, 8, 2015, '818758893'),
(18, 9, 2015, '20590560'),
(19, 10, 2015, '931090867.80'),
(20, 11, 2015, '337647558.97'),
(21, 12, 2015, '534682114.15'),
(22, 13, 2015, '424938755.52'),
(23, 14, 2015, '926364574.78'),
(24, 15, 2015, '477769359.41'),
(25, 16, 2015, '1829072689.71'),
(26, 17, 2015, '1803420540.92'),
(27, 6, 2016, '69368351.04'),
(28, 7, 2016, '45506060.94'),
(29, 8, 2016, '859678208'),
(30, 9, 2016, '94813827'),
(31, 10, 2016, '993592920'),
(32, 11, 2016, '193646732.35'),
(33, 12, 2016, '522678430.69'),
(34, 13, 2016, '447332655.83'),
(35, 14, 2016, '1069366446.98'),
(36, 15, 2016, '194486247.27'),
(37, 16, 2016, '1711185350.08'),
(38, 17, 2016, '1709918083.05');

-- --------------------------------------------------------

--
-- Table structure for table `main_dinas`
--

CREATE TABLE `main_dinas` (
  `id_dinas` int(11) NOT NULL,
  `nama_dinas` varchar(500) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_dinas`
--

INSERT INTO `main_dinas` (`id_dinas`, `nama_dinas`, `alamat`) VALUES
(2, 'Dinas Perdagangan', 'Malang'),
(3, 'Dinas Komunikasi dan Informatika', 'Malang'),
(4, 'Dinas Kebudayaan dan Pariwisata', 'Malang'),
(6, 'Dinas Perindustrian', 'Malang'),
(7, 'Dinas Pendidikan', 'Malang'),
(8, 'Dinas Kesehatan', 'Malang'),
(9, 'Dinas Perhubungan', 'Malang'),
(10, 'Dinas Pertanian dan Ketahanan Pangan', 'Malang'),
(11, 'Dinas Perumahan dan Kawasan Pemukiman', 'Malang'),
(14, 'Dinas Koperasi dan Usaha Mikro', 'Malang'),
(15, 'Dinas Kepemudaan dan Olahraga', ''),
(16, 'Dinas Kepedudukan dan Pencatatan Sipil', ''),
(17, 'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana', ''),
(18, 'Dinas Lingkungan Hidup', ''),
(19, 'Dinas Perpustakaan Umum dan Arsip Daerah', ''),
(20, 'Badan Kesatuan Bangsa dan Politik', ''),
(25, 'Dinas Pertanian', 'Malang');

-- --------------------------------------------------------

--
-- Table structure for table `main_jenis_kendaraan`
--

CREATE TABLE `main_jenis_kendaraan` (
  `id_jenis_kendaraan` int(11) NOT NULL,
  `keterangan` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_jenis_kendaraan`
--

INSERT INTO `main_jenis_kendaraan` (`id_jenis_kendaraan`, `keterangan`) VALUES
(1, 'Mobil Penumpang'),
(2, 'Bus'),
(3, 'Truk'),
(4, 'Sepeda Motor');

-- --------------------------------------------------------

--
-- Table structure for table `main_jenis_penyakit`
--

CREATE TABLE `main_jenis_penyakit` (
  `id_jenis_penyakit` int(11) NOT NULL,
  `keterangan` varchar(64) NOT NULL,
  `jml` varchar(10) NOT NULL,
  `th` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_jenis_penyakit`
--

INSERT INTO `main_jenis_penyakit` (`id_jenis_penyakit`, `keterangan`, `jml`, `th`) VALUES
(3, 'Headache', '7966', 2016),
(4, 'Dermatitis Kontak Alergi', '8718', 2016),
(5, 'Penyakit Pulpa Dan Jar Perapikal', '8819', 2016),
(6, 'Myalgia / Nyeri Otot', '9025', 2016),
(7, 'Obs Febris', '10773', 2016),
(8, 'Influensa, Virus Tidak Diidentifikasi', '12743', 2016),
(9, 'DM', '13815', 2016),
(10, 'Gastritis', '13840', 2016),
(11, 'Hipertensi Primer', '32109', 2016),
(12, 'Infeksi Saluran Pernapasan', '55351', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `main_jenjang_pend`
--

CREATE TABLE `main_jenjang_pend` (
  `id_jenjang` int(11) NOT NULL,
  `ket` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `main_kecamatan`
--

CREATE TABLE `main_kecamatan` (
  `id_kec` varchar(7) NOT NULL,
  `nama_kec` varchar(50) NOT NULL,
  `latlng` text NOT NULL,
  `luas` double NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_kecamatan`
--

INSERT INTO `main_kecamatan` (`id_kec`, `nama_kec`, `latlng`, `luas`, `is_delete`) VALUES
('KEC1001', 'Kedungkandang', 'null', 39.87, '0'),
('KEC1002', 'Sukun', 'null', 20.97, '0'),
('KEC1003', 'Klojen', 'null', 8.83, '0'),
('KEC1004', 'Blimbing', 'null', 17.77, '0'),
('KEC1005', 'Lowokwaru', 'null', 22.6, '0');

-- --------------------------------------------------------

--
-- Table structure for table `main_kelurahan`
--

CREATE TABLE `main_kelurahan` (
  `id_kel` varchar(7) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `nama_kel` varchar(64) NOT NULL,
  `latlng_kel` text NOT NULL,
  `luas_kel` double NOT NULL,
  `is_del` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_kelurahan`
--

INSERT INTO `main_kelurahan` (`id_kel`, `id_kec`, `nama_kel`, `latlng_kel`, `luas_kel`, `is_del`) VALUES
('KEL1001', 'KEC1001', 'Bumiayu', '0', 12, '0'),
('KEL1002', 'KEC1001', 'Mergosono', '12', 12, '0'),
('KEL1003', 'KEC1001', 'Kotalama', '12', 12, '0'),
('KEL1004', 'KEC1001', 'Wonokoyo', '12', 12, '0'),
('KEL1005', 'KEC1001', 'Buring', '12', 12, '0'),
('KEL1006', 'KEC1001', 'Kedungkandang', '12', 12, '0'),
('KEL1007', 'KEC1001', 'Lesanpuro', '12', 12, '0'),
('KEL1008', 'KEC1001', 'Sawojajar', '12', 12, '0'),
('KEL1009', 'KEC1001', 'Madyopuro', '12', 12, '0'),
('KEL1010', 'KEC1001', 'Cemorokandang', '12', 12, '0'),
('KEL1011', 'KEC1001', 'Arjowinangun', '12', 12, '0'),
('KEL1012', 'KEC1001', 'Tlogowaru', '12', 12, '0'),
('KEL1013', 'KEC1002', 'Ciptomulyo', '12', 12, '0'),
('KEL1014', 'KEC1002', 'Gadang', '12', 12, '0'),
('KEL1015', 'KEC1002', 'Kebonsari', '12', 12, '0'),
('KEL1016', 'KEC1002', 'Bandungrejosari', '12', 12, '0'),
('KEL1017', 'KEC1002', 'Sukun', '12', 12, '0'),
('KEL1018', 'KEC1005', 'Tunggulwulung', '1', 12, '0'),
('KEL1019', 'KEC1005', 'Merjosari', '1', 1, '0'),
('KEL1020', 'KEC1005', 'Tlogomas', '1', 1, '0'),
('KEL1021', 'KEC1003', 'Klojen', '1', 1, '0'),
('KEL1022', 'KEC1003', 'Samaan', '1', 1, '0'),
('KEL1023', 'KEC1003', 'Rampalcelaket', '1', 1, '0'),
('KEL1024', 'KEC1004', 'Balearjosari', '1', 1, '0'),
('KEL1025', 'KEC1004', 'Arjosari', '1', 1, '0'),
('KEL1026', 'KEC1004', 'Polowijen', '1', 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `main_partai`
--

CREATE TABLE `main_partai` (
  `id_partai` int(2) NOT NULL,
  `nama_partai` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_partai`
--

INSERT INTO `main_partai` (`id_partai`, `nama_partai`, `is_delete`) VALUES
(1, 'Partai Kebangkitan Bangsa (PKB)', '0'),
(2, 'Partai Gerakan Indonesia Raya (Gerindra)', '0'),
(3, 'Partai Demokrasi Indonesia Perjuangan (PDIP)', '0'),
(4, 'Partai Golongan Karya (Golkar)', '0'),
(5, 'Partai Demokrat', '0'),
(6, 'Partai Keadilan Sejahtera (PKS)', '0'),
(7, 'Partai Amanat Nasional (PAN)', '0'),
(8, 'Partai Gerakan Indonesia Raya (Gerindra)', '0'),
(9, 'Partai Persatuan Pembangunan (PPP)', '0'),
(10, 'Partai Hati Nurani Rakyat (Hanura)', '0'),
(11, 'Partai Nasional Demokrat (Nasdem)', '0'),
(12, 'Partai Bulan Bintang', '0'),
(13, 'Partai Keadilan dan Persatuan Indonesia (PKPI)', '0');

-- --------------------------------------------------------

--
-- Table structure for table `main_pendidikan`
--

CREATE TABLE `main_pendidikan` (
  `id_pendidikan` int(11) NOT NULL,
  `jenjang` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `miskin_jml_pend`
--

CREATE TABLE `miskin_jml_pend` (
  `id_pend_miskin` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL,
  `agregat` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `miskin_jml_pend`
--

INSERT INTO `miskin_jml_pend` (`id_pend_miskin`, `th`, `prosentase`, `agregat`) VALUES
(4, 2014, '4.80', '40.64'),
(5, 2015, '4.60', '39.10'),
(6, 2016, '4.33', '37.03');

-- --------------------------------------------------------

--
-- Table structure for table `miskin_pengeluaran_perkapita`
--

CREATE TABLE `miskin_pengeluaran_perkapita` (
  `id_perkapita` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `pengeluaran_perkapita` varchar(20) NOT NULL,
  `garis_kemiskinan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `miskin_pengeluaran_perkapita`
--

INSERT INTO `miskin_pengeluaran_perkapita` (`id_perkapita`, `th`, `pengeluaran_perkapita`, `garis_kemiskinan`) VALUES
(3, 2014, '1215502', '381400'),
(4, 2015, '1260186', '411709'),
(5, 2016, '1355476', '426527');

-- --------------------------------------------------------

--
-- Table structure for table `pem_dpr`
--

CREATE TABLE `pem_dpr` (
  `id_pem_dpr` int(11) NOT NULL,
  `id_partai` int(2) NOT NULL,
  `th` varchar(12) NOT NULL,
  `jk` enum('0','1') NOT NULL,
  `jml` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pem_dpr`
--

INSERT INTO `pem_dpr` (`id_pem_dpr`, `id_partai`, `th`, `jk`, `jml`) VALUES
(3, 2, '2014-2016', '0', 3),
(7, 2, '2014-2016', '1', 1),
(8, 11, '2014-2016', '0', 1),
(9, 10, '2014-2016', '0', 2),
(10, 10, '2014-2016', '1', 1),
(11, 9, '2014-2016', '0', 1),
(12, 9, '2014-2016', '1', 2),
(13, 7, '2014-2016', '0', 4),
(14, 6, '2014-2016', '0', 3),
(15, 1, '2014-2016', '0', 6),
(16, 5, '2014-2016', '0', 3),
(17, 5, '2014-2016', '1', 2),
(18, 4, '2014-2016', '0', 4),
(19, 4, '2014-2016', '1', 1),
(20, 3, '2014-2016', '0', 7),
(21, 3, '2014-2016', '1', 4),
(22, 11, '2014-2016', '1', 0),
(23, 7, '2014-2016', '1', 0),
(24, 1, '2014-2016', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pem_jml_asn_jenjang`
--

CREATE TABLE `pem_jml_asn_jenjang` (
  `id_jml_asn` varchar(6) NOT NULL,
  `id_jenjang` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pem_jml_rt_rw`
--

CREATE TABLE `pem_jml_rt_rw` (
  `id_rt_rw` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_rt` int(11) NOT NULL,
  `jml_rw` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pem_jml_rt_rw`
--

INSERT INTO `pem_jml_rt_rw` (`id_rt_rw`, `id_kec`, `th`, `jml_rt`, `jml_rw`) VALUES
(1, 'KEC1001', 2018, 892, 116),
(2, 'KEC1002', 2018, 882, 94),
(4, 'KEC1004', 2018, 923, 127),
(5, 'KEC1003', 2018, 675, 89),
(6, 'KEC1005', 2018, 785, 120),
(7, 'KEC1001', 2017, 891, 115),
(8, 'KEC1002', 2017, 881, 92),
(9, 'KEC1003', 2017, 673, 88),
(10, 'KEC1003', 2016, 670, 85),
(11, 'KEC1004', 2017, 922, 125),
(12, 'KEC1005', 2017, 783, 118),
(13, 'KEC1001', 2016, 890, 114),
(14, 'KEC1002', 2016, 880, 90),
(15, 'KEC1004', 2016, 920, 123),
(16, 'KEC1005', 2016, 781, 116);

-- --------------------------------------------------------

--
-- Table structure for table `pendapatan_lap_usaha`
--

CREATE TABLE `pendapatan_lap_usaha` (
  `id_lap_usaha` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendapatan_lap_usaha`
--

INSERT INTO `pendapatan_lap_usaha` (`id_lap_usaha`, `id_jenis`, `th`, `jml`) VALUES
(2, 5, 2016, '1231'),
(3, 7, 2014, '28.47'),
(4, 7, 2015, '28.9'),
(5, 7, 2016, '29.54'),
(6, 8, 2014, '27.14'),
(7, 8, 2015, '26.51'),
(8, 8, 2016, '25.4'),
(9, 9, 2014, '12.56'),
(10, 9, 2015, '12.52'),
(11, 9, 2016, '12.92');

-- --------------------------------------------------------

--
-- Table structure for table `pendapatan_lap_usaha_jenis`
--

CREATE TABLE `pendapatan_lap_usaha_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendapatan_lap_usaha_jenis`
--

INSERT INTO `pendapatan_lap_usaha_jenis` (`id_jenis`, `nama_jenis`) VALUES
(7, 'Perdagangan Besar dan Eceran; Reparasi Mobil dan Sepeda Motor'),
(8, 'Industri Pengolahan'),
(9, 'Konstruksi');

-- --------------------------------------------------------

--
-- Table structure for table `pendapatan_regional`
--

CREATE TABLE `pendapatan_regional` (
  `id_pendapatan` int(11) NOT NULL,
  `th_pendapatan` int(4) NOT NULL,
  `jml_pendapatan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendapatan_regional`
--

INSERT INTO `pendapatan_regional` (`id_pendapatan`, `th_pendapatan`, `jml_pendapatan`) VALUES
(4, 2014, '46563213.3'),
(5, 2015, '51824393.8'),
(6, 2016, '57171601.6');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_baca_tulis`
--

CREATE TABLE `pendidikan_baca_tulis` (
  `id_baca_tulis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_baca_tulis`
--

INSERT INTO `pendidikan_baca_tulis` (`id_baca_tulis`, `id_jenis`, `th`, `prosentase`) VALUES
(3, 1, 2014, '97.45'),
(4, 1, 2015, '98.16'),
(5, 1, 2016, '98.17'),
(6, 2, 2014, '2.55'),
(7, 2, 2015, '1.84'),
(8, 2, 2016, '1.83');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_baca_tulis_jenis`
--

CREATE TABLE `pendidikan_baca_tulis_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_baca_tulis_jenis`
--

INSERT INTO `pendidikan_baca_tulis_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Huruf Latin dan  atau Lainnya'),
(2, 'Buta Huruf');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_jml_sklh`
--

CREATE TABLE `pendidikan_jml_sklh` (
  `id_jml_sklh` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_jml_sklh`
--

INSERT INTO `pendidikan_jml_sklh` (`id_jml_sklh`, `id_jenis`, `th`, `prosentase`) VALUES
(2, 3, 2015, '2314'),
(5, 4, 2014, '333'),
(6, 4, 2015, '344'),
(7, 4, 2016, '341'),
(9, 5, 2014, '7'),
(10, 5, 2015, '6'),
(11, 5, 2016, '7'),
(12, 6, 2014, '92'),
(13, 6, 2015, '100'),
(14, 6, 2016, '101'),
(15, 7, 2014, '270'),
(16, 7, 2015, '272'),
(17, 7, 2016, '274'),
(18, 8, 2014, '9'),
(19, 8, 2015, '8'),
(20, 8, 2016, '8'),
(21, 9, 2014, '4'),
(22, 9, 2015, '52'),
(23, 9, 2016, '43'),
(24, 10, 2014, '97'),
(25, 10, 2015, '101'),
(26, 10, 2016, '100'),
(27, 11, 2014, '8'),
(28, 11, 2015, '8'),
(29, 11, 2016, '8'),
(30, 12, 2014, '26'),
(31, 12, 2015, '30'),
(32, 12, 2016, '34'),
(33, 13, 2014, '38'),
(34, 13, 2015, '47'),
(35, 13, 2016, '45'),
(36, 14, 2014, '5'),
(37, 14, 2015, '6'),
(38, 14, 2016, '5'),
(39, 15, 2014, '38'),
(40, 15, 2015, '47'),
(41, 15, 2016, '45'),
(42, 16, 2014, '15'),
(43, 16, 2015, '15'),
(44, 16, 2016, '15');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_jml_sklh_jenis`
--

CREATE TABLE `pendidikan_jml_sklh_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_jml_sklh_jenis`
--

INSERT INTO `pendidikan_jml_sklh_jenis` (`id_jenis`, `nama_jenis`) VALUES
(4, 'TK'),
(5, 'TK LB'),
(6, 'RA'),
(7, 'SD'),
(8, 'SD LB'),
(9, 'MI'),
(10, 'SMP'),
(11, 'SMP LB'),
(12, 'MTS'),
(13, 'SMA'),
(14, 'SMA LB'),
(15, 'SMK'),
(16, 'MA');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_partisipasi_sklh`
--

CREATE TABLE `pendidikan_partisipasi_sklh` (
  `id_partisipasi_sklh` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_partisipasi_sklh`
--

INSERT INTO `pendidikan_partisipasi_sklh` (`id_partisipasi_sklh`, `id_jenis`, `th`, `prosentase`) VALUES
(1, 1, 2014, '100'),
(2, 1, 2015, '100'),
(3, 1, 2016, '100'),
(4, 2, 2014, '99.08'),
(5, 2, 2015, '98.95'),
(6, 2, 2016, '95.75'),
(7, 3, 2014, '71.59'),
(8, 3, 2015, '78.91'),
(9, 3, 2016, '78.32');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_partisipasi_sklh_jenis`
--

CREATE TABLE `pendidikan_partisipasi_sklh_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_partisipasi_sklh_jenis`
--

INSERT INTO `pendidikan_partisipasi_sklh_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'APS SD (7-12 tahun)'),
(2, 'APS SMP (13-15 tahun)'),
(3, 'APS SMA (16-18 tahun)');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_penduduk`
--

CREATE TABLE `pendidikan_penduduk` (
  `id_pendidikan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_penduduk`
--

INSERT INTO `pendidikan_penduduk` (`id_pendidikan`, `id_jenis`, `th`, `prosentase`) VALUES
(1, 3, 2014, '13.79'),
(2, 3, 2015, '8.39'),
(3, 3, 2016, '8.76'),
(4, 4, 2014, '16.16'),
(5, 4, 2015, '18.90'),
(6, 4, 2016, '26.35'),
(7, 5, 2014, '17.53'),
(8, 5, 2015, '19.83'),
(9, 5, 2016, '11.93'),
(10, 6, 2014, '35.67'),
(11, 6, 2015, '36.09'),
(12, 6, 2016, '35.94'),
(13, 7, 2014, '16.85'),
(14, 7, 2015, '16.79'),
(15, 7, 2016, '17.03');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_penduduk_jenis`
--

CREATE TABLE `pendidikan_penduduk_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_penduduk_jenis`
--

INSERT INTO `pendidikan_penduduk_jenis` (`id_jenis`, `nama_jenis`) VALUES
(3, 'Tidak Punya Ijazah SD'),
(4, 'SD / Sederajat'),
(5, 'SMP / Sederajat'),
(6, 'SMA / Sederajat'),
(7, 'PT / Universitas');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_rasio_guru_murid`
--

CREATE TABLE `pendidikan_rasio_guru_murid` (
  `id_rasio` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_rasio_guru_murid`
--

INSERT INTO `pendidikan_rasio_guru_murid` (`id_rasio`, `id_jenis`, `th`, `prosentase`) VALUES
(1, 2, 2015, '1222'),
(2, 1, 2016, '120'),
(5, 6, 2014, '13'),
(6, 6, 2015, '13'),
(7, 6, 2016, '7'),
(8, 7, 2014, '19'),
(9, 7, 2015, '19'),
(10, 7, 2016, '19'),
(11, 8, 2014, '15'),
(12, 8, 2015, '15'),
(13, 8, 2016, '14'),
(14, 9, 2014, '12'),
(15, 9, 2015, '12'),
(16, 9, 2016, '12'),
(17, 10, 2014, '14'),
(18, 10, 2015, '13'),
(19, 10, 2016, '13');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_rasio_guru_murid_jenis`
--

CREATE TABLE `pendidikan_rasio_guru_murid_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_rasio_guru_murid_jenis`
--

INSERT INTO `pendidikan_rasio_guru_murid_jenis` (`id_jenis`, `nama_jenis`) VALUES
(6, 'TK'),
(7, 'SD'),
(8, 'SMP'),
(9, 'SMA'),
(10, 'SMK');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_penduduk`
--

CREATE TABLE `pengeluaran_penduduk` (
  `id_pengeluaran` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran_penduduk`
--

INSERT INTO `pengeluaran_penduduk` (`id_pengeluaran`, `id_jenis`, `th`, `jml`) VALUES
(2, 6, 2014, '40.93'),
(3, 6, 2015, '44.97'),
(4, 6, 2016, '50.38'),
(5, 7, 2014, '18.03'),
(6, 7, 2015, '14.19'),
(7, 7, 2016, '13.12'),
(8, 8, 2014, '23.32'),
(9, 8, 2015, '22.33'),
(10, 8, 2016, '14.66'),
(11, 9, 2014, '15.88'),
(12, 9, 2015, '16.11'),
(13, 9, 2016, '19.16'),
(14, 10, 2014, '1.84'),
(15, 10, 2015, '2.40'),
(16, 10, 2016, '2.69');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_penduduk_jenis`
--

CREATE TABLE `pengeluaran_penduduk_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran_penduduk_jenis`
--

INSERT INTO `pengeluaran_penduduk_jenis` (`id_jenis`, `nama_jenis`) VALUES
(6, '>= 1000000'),
(7, '750000 s/d 999999'),
(8, '500000 s/d 749999'),
(9, '300000 s/d 499999'),
(10, '<= 299999');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_rmt_tgg`
--

CREATE TABLE `pengeluaran_rmt_tgg` (
  `id_rmt_tgg` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_mkn` varchar(20) NOT NULL,
  `jml_non` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran_rmt_tgg`
--

INSERT INTO `pengeluaran_rmt_tgg` (`id_rmt_tgg`, `th`, `jml_mkn`, `jml_non`) VALUES
(5, 2014, '38.09', '61.91'),
(6, 2015, '39.21', '60.79'),
(7, 2016, '38.63', '61.37');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_holti_jenis`
--

CREATE TABLE `pertanian_holti_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL,
  `kategori` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_holti_jenis`
--

INSERT INTO `pertanian_holti_jenis` (`id_jenis`, `nama_jenis`, `kategori`) VALUES
(9, 'Sukun', '0'),
(10, 'Sirsak', '0'),
(11, 'Sawo', '0'),
(12, 'Salak', '0'),
(13, 'Rambutan', '0'),
(14, 'Pisang', '0'),
(15, 'Petai', '1'),
(16, 'Pepaya', '0'),
(17, 'Nanas', '0'),
(18, 'Nangka/Cempedak', '0'),
(19, 'Melinjo', '0'),
(20, 'Markisa/Konyal', '0'),
(21, 'Manggis', '0'),
(22, 'Mangga', '0'),
(23, 'Jeruk Siam/Keprok', '0'),
(24, 'Jeruk Besar', '0'),
(25, 'Jengkol', '1'),
(26, 'Jambu Biji', '0'),
(27, 'Jambu Air', '0'),
(28, 'Durian', '0'),
(29, 'Duku/Langsat', '0'),
(30, 'Belimbing', '0'),
(31, 'Apel', '0'),
(32, 'Anggur', '0'),
(33, 'Alpukat', '0'),
(34, 'Bawang Daun', '1'),
(35, 'Bawang Merah', '1'),
(36, 'Bawang Putih', '1'),
(37, 'Bayam', '1'),
(38, 'Blewah', '0'),
(39, 'Buncis', '0'),
(40, 'Cabai Besar', '0'),
(41, 'Cabai Rawit', '0'),
(42, 'Jamur', '1'),
(43, 'Kacang Merah', '1'),
(44, 'Kacang Panjang', '1'),
(45, 'Kangkung', '1'),
(46, 'Kembang Kol', '1'),
(47, 'Kentang ', '1'),
(48, 'Ketimun', '0'),
(49, 'Kubis', '1'),
(50, 'Labu Siam', '1'),
(51, 'Lobak', '1'),
(52, 'Melon', '0'),
(53, 'Paprika', '0'),
(54, 'Petsai/Sawi', '1'),
(55, 'Semangka', '0'),
(56, 'Stroberi', '0'),
(57, 'Terung', '1'),
(58, 'Tomat', '0'),
(59, 'Wortel', '1');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_holti_jml`
--

CREATE TABLE `pertanian_holti_jml` (
  `id_holti` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_holti_jml`
--

INSERT INTO `pertanian_holti_jml` (`id_holti`, `id_jenis`, `th`, `jml`) VALUES
(3, 5, 2019, '200'),
(5, 9, 2016, '649'),
(6, 10, 2016, '161'),
(7, 11, 2016, '139'),
(8, 12, 2016, '2310'),
(9, 13, 2016, '777'),
(10, 14, 2016, '2121'),
(11, 15, 2016, '536'),
(12, 16, 2016, '1853'),
(13, 17, 2016, '0'),
(14, 18, 2016, '1397'),
(15, 19, 2016, '475'),
(16, 20, 2016, '65'),
(17, 21, 2016, '0'),
(18, 22, 2016, '10137'),
(19, 23, 2016, '5429'),
(20, 24, 2016, '200'),
(21, 25, 2016, '0'),
(22, 26, 2016, '815'),
(23, 27, 2016, '323'),
(24, 28, 2016, '467'),
(25, 29, 2016, '50'),
(26, 30, 2016, '836'),
(27, 31, 2016, '0'),
(28, 32, 2016, '55'),
(29, 33, 2016, '200'),
(30, 34, 2016, '0'),
(31, 35, 2016, '0'),
(32, 36, 2016, '0'),
(33, 37, 2016, '0'),
(34, 38, 2016, '0'),
(35, 39, 2016, '0'),
(36, 40, 2016, '278'),
(37, 41, 2016, '44'),
(38, 42, 2016, '35471.2'),
(39, 43, 2016, '0'),
(40, 44, 2016, '50'),
(41, 45, 2016, '0'),
(42, 46, 2016, '8'),
(43, 47, 2016, '0'),
(44, 48, 2016, '2'),
(45, 49, 2016, '0'),
(46, 50, 2016, '0'),
(47, 51, 2016, '0'),
(48, 52, 2016, '0'),
(49, 53, 2016, '0'),
(50, 54, 2016, '42'),
(51, 55, 2016, '0'),
(52, 56, 2016, '0'),
(53, 57, 2016, '20'),
(54, 58, 2016, '82'),
(55, 59, 2016, '0');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_ikan_jenis`
--

CREATE TABLE `pertanian_ikan_jenis` (
  `id_jenis` int(11) NOT NULL,
  `kategori` enum('0','1') NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_ikan_jenis`
--

INSERT INTO `pertanian_ikan_jenis` (`id_jenis`, `kategori`, `nama_jenis`) VALUES
(11, '0', 'Ikan Nila'),
(12, '0', 'Ikan Tombro'),
(13, '0', 'Ikan Gurame'),
(14, '0', 'Ikan Lele'),
(15, '1', 'Ikan Nila'),
(16, '1', 'Ikan Tombro'),
(17, '1', 'Ikan Gurame'),
(18, '1', 'Ikan Lele');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_ikan_jml`
--

CREATE TABLE `pertanian_ikan_jml` (
  `id_ikan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_ikan_jml`
--

INSERT INTO `pertanian_ikan_jml` (`id_ikan`, `id_jenis`, `th`, `jml`) VALUES
(4, 11, 2014, '3499'),
(5, 11, 2015, '2735'),
(6, 11, 2016, '6258'),
(7, 12, 2014, '20'),
(8, 12, 2015, '-'),
(9, 12, 2016, '-'),
(10, 17, 2014, '15'),
(11, 13, 2015, '-'),
(12, 13, 2016, '40'),
(13, 14, 2014, '33830'),
(17, 14, 2015, '53771'),
(18, 14, 2016, '101231'),
(19, 15, 2014, '1495'),
(20, 15, 2015, '203'),
(21, 15, 2016, '1220'),
(22, 16, 2014, '1528'),
(23, 16, 2015, '550'),
(24, 16, 2016, '2176'),
(25, 17, 2014, '-'),
(26, 17, 2015, '-'),
(27, 17, 2016, '-'),
(28, 18, 2014, '-'),
(29, 18, 2015, '-'),
(30, 18, 2016, '-');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_kelapa_tebu`
--

CREATE TABLE `pertanian_kelapa_tebu` (
  `id_pertanian` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_kelapa` varchar(20) NOT NULL,
  `jml_tebu` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_kelapa_tebu`
--

INSERT INTO `pertanian_kelapa_tebu` (`id_pertanian`, `th`, `jml_kelapa`, `jml_tebu`) VALUES
(3, 2016, '925', '53142'),
(4, 2015, '807', '60013.28'),
(5, 2014, '790', '62725');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_komoditas_jenis`
--

CREATE TABLE `pertanian_komoditas_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_komoditas_jenis`
--

INSERT INTO `pertanian_komoditas_jenis` (`id_jenis`, `nama_jenis`) VALUES
(6, 'Padi Sawah'),
(7, 'Padi Ladang'),
(8, 'Jagung'),
(9, 'Kedelai'),
(10, 'Kacang Tanah'),
(11, 'Kacang Hijau'),
(12, 'Ubi Kayu'),
(13, 'Ubi Jalar');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_komoditas_jml`
--

CREATE TABLE `pertanian_komoditas_jml` (
  `id_komoditas` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_komoditas_jml`
--

INSERT INTO `pertanian_komoditas_jml` (`id_komoditas`, `id_jenis`, `th`, `jml`) VALUES
(3, 6, 2016, '142850'),
(4, 7, 2016, '0'),
(5, 8, 2016, '4590'),
(6, 9, 2016, '0'),
(7, 10, 2016, '150'),
(8, 11, 2016, '0'),
(9, 12, 2016, '38580'),
(10, 13, 2016, '190');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_lahan`
--

CREATE TABLE `pertanian_lahan` (
  `id_lahan` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jenis_lahan` varchar(64) NOT NULL,
  `luas_lahan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_lahan`
--

INSERT INTO `pertanian_lahan` (`id_lahan`, `th`, `jenis_lahan`, `luas_lahan`) VALUES
(1, 2017, 'Lahan Pertanian Sawah', '20.90'),
(3, 2016, 'Lahan Pertanian Sawah', '10.38'),
(4, 2016, 'Lahan Pertanian Bukan Sawah', '18.85'),
(5, 2016, 'Lahan Bukan Pertanian', '70.77');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_lahan_penggunaan`
--

CREATE TABLE `pertanian_lahan_penggunaan` (
  `id_peng` int(11) NOT NULL,
  `jenis_peng` text NOT NULL,
  `th` int(4) NOT NULL,
  `luas_pend` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_lahan_penggunaan`
--

INSERT INTO `pertanian_lahan_penggunaan` (`id_peng`, `jenis_peng`, `th`, `luas_pend`) VALUES
(1, 'Lahan Ditanami Padi (Sawah)', 2018, '2000.98'),
(3, 'Lahan Ditanami Padi (Sawah)', 2016, '844'),
(4, 'Lahan Ditanami Selain Padi (Bukan Sawah)', 2016, '292'),
(5, 'Lahan Tidak Ditanami', 2016, '6');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_ternak_jenis`
--

CREATE TABLE `pertanian_ternak_jenis` (
  `id_jenis` int(11) NOT NULL,
  `kategori` enum('0','1','2') NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_ternak_jenis`
--

INSERT INTO `pertanian_ternak_jenis` (`id_jenis`, `kategori`, `nama_jenis`) VALUES
(9, '0', 'Daging sapi'),
(10, '0', 'Daging Kambing/Domba'),
(11, '0', 'Daging Babi'),
(12, '1', 'Ayam Buras'),
(13, '1', 'Ayam Pedaging'),
(14, '1', 'Itik'),
(15, '1', 'Itik Manila'),
(16, '0', 'Susu'),
(17, '2', 'Ayam Buras'),
(18, '2', 'Ayam Petelur'),
(19, '2', 'Itik'),
(20, '2', 'Itik Manila'),
(21, '1', 'Ayam Petelur');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_ternak_jml`
--

CREATE TABLE `pertanian_ternak_jml` (
  `id_ternak` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_ternak_jml`
--

INSERT INTO `pertanian_ternak_jml` (`id_ternak`, `id_jenis`, `th`, `jml`) VALUES
(3, 9, 2014, '3465'),
(4, 9, 2015, '3614.84'),
(5, 9, 2016, '3246.32'),
(6, 10, 2014, '320'),
(7, 10, 2015, '430'),
(8, 10, 2015, '490.22'),
(9, 11, 2014, '511.80'),
(10, 11, 2015, '525.05'),
(11, 11, 2016, '508.39'),
(12, 12, 2014, '642.70'),
(13, 12, 2015, '551.74'),
(14, 12, 2016, '381.06'),
(19, 21, 2014, '14.90'),
(20, 21, 2015, '18.21'),
(21, 21, 2016, '21.61'),
(22, 14, 2014, '4.30'),
(23, 19, 2015, '23.24'),
(24, 13, 2014, '17673.30'),
(25, 13, 2015, '17513.17'),
(26, 13, 2016, '17877.11'),
(27, 14, 2014, '4.30'),
(28, 14, 2015, '23.24'),
(29, 14, 2016, '25.25'),
(30, 20, 2014, '2.10'),
(31, 20, 2015, '2.34'),
(32, 20, 2016, '2.28'),
(33, 16, 2014, '335085.60'),
(34, 16, 2015, '286496.99'),
(35, 16, 2016, '214417.74'),
(36, 17, 2014, '30.50'),
(37, 17, 2015, '27.45'),
(38, 17, 2016, '27.51'),
(39, 18, 2014, '1332.60'),
(40, 18, 2015, '1747.36'),
(41, 18, 2016, '1945.20'),
(42, 19, 2014, '4.70'),
(43, 19, 2015, '101.50'),
(44, 19, 2016, '77.34'),
(45, 20, 2014, '1.60'),
(46, 20, 2015, '0.89'),
(47, 20, 2016, '0.93');

-- --------------------------------------------------------

--
-- Table structure for table `tehnologi_jml_bts`
--

CREATE TABLE `tehnologi_jml_bts` (
  `id_jml_bts` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tehnologi_jml_bts`
--

INSERT INTO `tehnologi_jml_bts` (`id_jml_bts`, `th`, `jml`) VALUES
(5, 2014, 317),
(6, 2015, 342),
(7, 2016, 385);

-- --------------------------------------------------------

--
-- Table structure for table `tehnologi_jml_warnet`
--

CREATE TABLE `tehnologi_jml_warnet` (
  `id_jml_warnet` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tehnologi_jml_warnet`
--

INSERT INTO `tehnologi_jml_warnet` (`id_jml_warnet`, `th`, `jml`) VALUES
(4, 2014, 92),
(5, 2015, 96),
(6, 2016, 97);

-- --------------------------------------------------------

--
-- Table structure for table `tehnologi_jml_web_opd`
--

CREATE TABLE `tehnologi_jml_web_opd` (
  `id_web_opd` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tehnologi_jml_web_opd`
--

INSERT INTO `tehnologi_jml_web_opd` (`id_web_opd`, `th`, `jml`) VALUES
(3, 2014, 100),
(4, 2015, 105),
(5, 2016, 107);

-- --------------------------------------------------------

--
-- Table structure for table `trans_jln`
--

CREATE TABLE `trans_jln` (
  `id_jln` int(11) NOT NULL,
  `kategori_jln` varchar(64) NOT NULL,
  `jln_negara` varchar(20) NOT NULL,
  `jln_prov` varchar(20) NOT NULL,
  `jln_kota` varchar(20) NOT NULL,
  `th` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_jln`
--

INSERT INTO `trans_jln` (`id_jln`, `kategori_jln`, `jln_negara`, `jln_prov`, `jln_kota`, `th`) VALUES
(4, 'Kondisi Jalan Baik', '1.45', '48.45', '135.19', 2015),
(5, 'Kondisi Jalan Baik', '12.64', '10.44', '993.64', 2016),
(6, 'Kondisi Jalan Sedang', '0', '0', '0', 2015),
(7, 'Kondisi Jalan Sedang', '0', '0', '0', 2016),
(8, 'Kondisi Jalan Rusak', '0', '0.5', '5.59', 2015),
(9, 'Kondisi Jalan Rusak', '0', '0.5', '33.47', 2016),
(10, 'Kondisi Jalan Rusak Berat', '0', '0', '0', 2015),
(11, 'Kondisi Jalan Rusak Berat', '0', '0', '0', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `trans_jml_kendaraan`
--

CREATE TABLE `trans_jml_kendaraan` (
  `id_jml_kendaraan` int(11) NOT NULL,
  `id_jenis_kendaraan` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_jml_kendaraan`
--

INSERT INTO `trans_jml_kendaraan` (`id_jml_kendaraan`, `id_jenis_kendaraan`, `th`, `jml`) VALUES
(1, 1, 2014, '75818'),
(2, 1, 2015, '86091'),
(3, 1, 2016, '90058'),
(4, 2, 2014, '861'),
(5, 2, 2015, '934'),
(6, 2, 2016, '966'),
(7, 3, 2014, '17949'),
(8, 3, 2015, '19457'),
(9, 3, 2016, '20002'),
(10, 4, 2014, '392559'),
(11, 4, 2015, '441123'),
(12, 4, 2016, '456693');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `admin_lv`
--
ALTER TABLE `admin_lv`
  ADD PRIMARY KEY (`id_lv`);

--
-- Indexes for table `iklim_hujan`
--
ALTER TABLE `iklim_hujan`
  ADD PRIMARY KEY (`id_hujan`);

--
-- Indexes for table `iklim_station`
--
ALTER TABLE `iklim_station`
  ADD PRIMARY KEY (`id_st`);

--
-- Indexes for table `index_kes_daya_pend`
--
ALTER TABLE `index_kes_daya_pend`
  ADD PRIMARY KEY (`id_index`);

--
-- Indexes for table `index_pem_manusia`
--
ALTER TABLE `index_pem_manusia`
  ADD PRIMARY KEY (`id_ipm`);

--
-- Indexes for table `kepandudukan_jk`
--
ALTER TABLE `kepandudukan_jk`
  ADD PRIMARY KEY (`id_kepend_jk`);

--
-- Indexes for table `kependudukan_kel_umur`
--
ALTER TABLE `kependudukan_kel_umur`
  ADD PRIMARY KEY (`id_kel_umur`);

--
-- Indexes for table `kependudukan_rasio_ketergantungan`
--
ALTER TABLE `kependudukan_rasio_ketergantungan`
  ADD PRIMARY KEY (`id_rasio`);

--
-- Indexes for table `kerja_angkatan`
--
ALTER TABLE `kerja_angkatan`
  ADD PRIMARY KEY (`id_angkatan`);

--
-- Indexes for table `kerja_pengangguran`
--
ALTER TABLE `kerja_pengangguran`
  ADD PRIMARY KEY (`id_pengangguran`);

--
-- Indexes for table `kerja_ump`
--
ALTER TABLE `kerja_ump`
  ADD PRIMARY KEY (`id_ump`);

--
-- Indexes for table `kesehatan_gizi_balita`
--
ALTER TABLE `kesehatan_gizi_balita`
  ADD PRIMARY KEY (`id_gizi_balita`);

--
-- Indexes for table `keu_jenis`
--
ALTER TABLE `keu_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `keu_jml`
--
ALTER TABLE `keu_jml`
  ADD PRIMARY KEY (`id_jml_keu`);

--
-- Indexes for table `main_dinas`
--
ALTER TABLE `main_dinas`
  ADD PRIMARY KEY (`id_dinas`);

--
-- Indexes for table `main_jenis_kendaraan`
--
ALTER TABLE `main_jenis_kendaraan`
  ADD PRIMARY KEY (`id_jenis_kendaraan`);

--
-- Indexes for table `main_jenis_penyakit`
--
ALTER TABLE `main_jenis_penyakit`
  ADD PRIMARY KEY (`id_jenis_penyakit`);

--
-- Indexes for table `main_jenjang_pend`
--
ALTER TABLE `main_jenjang_pend`
  ADD PRIMARY KEY (`id_jenjang`);

--
-- Indexes for table `main_kecamatan`
--
ALTER TABLE `main_kecamatan`
  ADD PRIMARY KEY (`id_kec`);

--
-- Indexes for table `main_kelurahan`
--
ALTER TABLE `main_kelurahan`
  ADD PRIMARY KEY (`id_kel`);

--
-- Indexes for table `main_partai`
--
ALTER TABLE `main_partai`
  ADD PRIMARY KEY (`id_partai`);

--
-- Indexes for table `main_pendidikan`
--
ALTER TABLE `main_pendidikan`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `miskin_jml_pend`
--
ALTER TABLE `miskin_jml_pend`
  ADD PRIMARY KEY (`id_pend_miskin`);

--
-- Indexes for table `miskin_pengeluaran_perkapita`
--
ALTER TABLE `miskin_pengeluaran_perkapita`
  ADD PRIMARY KEY (`id_perkapita`);

--
-- Indexes for table `pem_dpr`
--
ALTER TABLE `pem_dpr`
  ADD PRIMARY KEY (`id_pem_dpr`);

--
-- Indexes for table `pem_jml_asn_jenjang`
--
ALTER TABLE `pem_jml_asn_jenjang`
  ADD PRIMARY KEY (`id_jml_asn`);

--
-- Indexes for table `pem_jml_rt_rw`
--
ALTER TABLE `pem_jml_rt_rw`
  ADD PRIMARY KEY (`id_rt_rw`);

--
-- Indexes for table `pendapatan_lap_usaha`
--
ALTER TABLE `pendapatan_lap_usaha`
  ADD PRIMARY KEY (`id_lap_usaha`);

--
-- Indexes for table `pendapatan_lap_usaha_jenis`
--
ALTER TABLE `pendapatan_lap_usaha_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendapatan_regional`
--
ALTER TABLE `pendapatan_regional`
  ADD PRIMARY KEY (`id_pendapatan`);

--
-- Indexes for table `pendidikan_baca_tulis`
--
ALTER TABLE `pendidikan_baca_tulis`
  ADD PRIMARY KEY (`id_baca_tulis`);

--
-- Indexes for table `pendidikan_baca_tulis_jenis`
--
ALTER TABLE `pendidikan_baca_tulis_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_jml_sklh`
--
ALTER TABLE `pendidikan_jml_sklh`
  ADD PRIMARY KEY (`id_jml_sklh`);

--
-- Indexes for table `pendidikan_jml_sklh_jenis`
--
ALTER TABLE `pendidikan_jml_sklh_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_partisipasi_sklh`
--
ALTER TABLE `pendidikan_partisipasi_sklh`
  ADD PRIMARY KEY (`id_partisipasi_sklh`);

--
-- Indexes for table `pendidikan_partisipasi_sklh_jenis`
--
ALTER TABLE `pendidikan_partisipasi_sklh_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_penduduk`
--
ALTER TABLE `pendidikan_penduduk`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `pendidikan_penduduk_jenis`
--
ALTER TABLE `pendidikan_penduduk_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_rasio_guru_murid`
--
ALTER TABLE `pendidikan_rasio_guru_murid`
  ADD PRIMARY KEY (`id_rasio`);

--
-- Indexes for table `pendidikan_rasio_guru_murid_jenis`
--
ALTER TABLE `pendidikan_rasio_guru_murid_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pengeluaran_penduduk`
--
ALTER TABLE `pengeluaran_penduduk`
  ADD PRIMARY KEY (`id_pengeluaran`);

--
-- Indexes for table `pengeluaran_penduduk_jenis`
--
ALTER TABLE `pengeluaran_penduduk_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pengeluaran_rmt_tgg`
--
ALTER TABLE `pengeluaran_rmt_tgg`
  ADD PRIMARY KEY (`id_rmt_tgg`);

--
-- Indexes for table `pertanian_holti_jenis`
--
ALTER TABLE `pertanian_holti_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pertanian_holti_jml`
--
ALTER TABLE `pertanian_holti_jml`
  ADD PRIMARY KEY (`id_holti`);

--
-- Indexes for table `pertanian_ikan_jenis`
--
ALTER TABLE `pertanian_ikan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pertanian_ikan_jml`
--
ALTER TABLE `pertanian_ikan_jml`
  ADD PRIMARY KEY (`id_ikan`);

--
-- Indexes for table `pertanian_kelapa_tebu`
--
ALTER TABLE `pertanian_kelapa_tebu`
  ADD PRIMARY KEY (`id_pertanian`);

--
-- Indexes for table `pertanian_komoditas_jenis`
--
ALTER TABLE `pertanian_komoditas_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pertanian_komoditas_jml`
--
ALTER TABLE `pertanian_komoditas_jml`
  ADD PRIMARY KEY (`id_komoditas`);

--
-- Indexes for table `pertanian_lahan`
--
ALTER TABLE `pertanian_lahan`
  ADD PRIMARY KEY (`id_lahan`);

--
-- Indexes for table `pertanian_lahan_penggunaan`
--
ALTER TABLE `pertanian_lahan_penggunaan`
  ADD PRIMARY KEY (`id_peng`);

--
-- Indexes for table `pertanian_ternak_jenis`
--
ALTER TABLE `pertanian_ternak_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pertanian_ternak_jml`
--
ALTER TABLE `pertanian_ternak_jml`
  ADD PRIMARY KEY (`id_ternak`);

--
-- Indexes for table `tehnologi_jml_bts`
--
ALTER TABLE `tehnologi_jml_bts`
  ADD PRIMARY KEY (`id_jml_bts`);

--
-- Indexes for table `tehnologi_jml_warnet`
--
ALTER TABLE `tehnologi_jml_warnet`
  ADD PRIMARY KEY (`id_jml_warnet`);

--
-- Indexes for table `tehnologi_jml_web_opd`
--
ALTER TABLE `tehnologi_jml_web_opd`
  ADD PRIMARY KEY (`id_web_opd`);

--
-- Indexes for table `trans_jln`
--
ALTER TABLE `trans_jln`
  ADD PRIMARY KEY (`id_jln`);

--
-- Indexes for table `trans_jml_kendaraan`
--
ALTER TABLE `trans_jml_kendaraan`
  ADD PRIMARY KEY (`id_jml_kendaraan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_lv`
--
ALTER TABLE `admin_lv`
  MODIFY `id_lv` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `iklim_hujan`
--
ALTER TABLE `iklim_hujan`
  MODIFY `id_hujan` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `iklim_station`
--
ALTER TABLE `iklim_station`
  MODIFY `id_st` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `index_kes_daya_pend`
--
ALTER TABLE `index_kes_daya_pend`
  MODIFY `id_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `index_pem_manusia`
--
ALTER TABLE `index_pem_manusia`
  MODIFY `id_ipm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kepandudukan_jk`
--
ALTER TABLE `kepandudukan_jk`
  MODIFY `id_kepend_jk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kependudukan_kel_umur`
--
ALTER TABLE `kependudukan_kel_umur`
  MODIFY `id_kel_umur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `kependudukan_rasio_ketergantungan`
--
ALTER TABLE `kependudukan_rasio_ketergantungan`
  MODIFY `id_rasio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kerja_angkatan`
--
ALTER TABLE `kerja_angkatan`
  MODIFY `id_angkatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kerja_pengangguran`
--
ALTER TABLE `kerja_pengangguran`
  MODIFY `id_pengangguran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kerja_ump`
--
ALTER TABLE `kerja_ump`
  MODIFY `id_ump` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kesehatan_gizi_balita`
--
ALTER TABLE `kesehatan_gizi_balita`
  MODIFY `id_gizi_balita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `keu_jenis`
--
ALTER TABLE `keu_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `keu_jml`
--
ALTER TABLE `keu_jml`
  MODIFY `id_jml_keu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `main_dinas`
--
ALTER TABLE `main_dinas`
  MODIFY `id_dinas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `main_jenis_kendaraan`
--
ALTER TABLE `main_jenis_kendaraan`
  MODIFY `id_jenis_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `main_jenis_penyakit`
--
ALTER TABLE `main_jenis_penyakit`
  MODIFY `id_jenis_penyakit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `main_jenjang_pend`
--
ALTER TABLE `main_jenjang_pend`
  MODIFY `id_jenjang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_partai`
--
ALTER TABLE `main_partai`
  MODIFY `id_partai` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `main_pendidikan`
--
ALTER TABLE `main_pendidikan`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `miskin_jml_pend`
--
ALTER TABLE `miskin_jml_pend`
  MODIFY `id_pend_miskin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `miskin_pengeluaran_perkapita`
--
ALTER TABLE `miskin_pengeluaran_perkapita`
  MODIFY `id_perkapita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pem_dpr`
--
ALTER TABLE `pem_dpr`
  MODIFY `id_pem_dpr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pem_jml_rt_rw`
--
ALTER TABLE `pem_jml_rt_rw`
  MODIFY `id_rt_rw` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pendapatan_lap_usaha`
--
ALTER TABLE `pendapatan_lap_usaha`
  MODIFY `id_lap_usaha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pendapatan_lap_usaha_jenis`
--
ALTER TABLE `pendapatan_lap_usaha_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pendapatan_regional`
--
ALTER TABLE `pendapatan_regional`
  MODIFY `id_pendapatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pendidikan_baca_tulis`
--
ALTER TABLE `pendidikan_baca_tulis`
  MODIFY `id_baca_tulis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pendidikan_baca_tulis_jenis`
--
ALTER TABLE `pendidikan_baca_tulis_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pendidikan_jml_sklh`
--
ALTER TABLE `pendidikan_jml_sklh`
  MODIFY `id_jml_sklh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `pendidikan_jml_sklh_jenis`
--
ALTER TABLE `pendidikan_jml_sklh_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pendidikan_partisipasi_sklh`
--
ALTER TABLE `pendidikan_partisipasi_sklh`
  MODIFY `id_partisipasi_sklh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pendidikan_partisipasi_sklh_jenis`
--
ALTER TABLE `pendidikan_partisipasi_sklh_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pendidikan_penduduk`
--
ALTER TABLE `pendidikan_penduduk`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pendidikan_penduduk_jenis`
--
ALTER TABLE `pendidikan_penduduk_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pendidikan_rasio_guru_murid`
--
ALTER TABLE `pendidikan_rasio_guru_murid`
  MODIFY `id_rasio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `pendidikan_rasio_guru_murid_jenis`
--
ALTER TABLE `pendidikan_rasio_guru_murid_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pengeluaran_penduduk`
--
ALTER TABLE `pengeluaran_penduduk`
  MODIFY `id_pengeluaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pengeluaran_penduduk_jenis`
--
ALTER TABLE `pengeluaran_penduduk_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pengeluaran_rmt_tgg`
--
ALTER TABLE `pengeluaran_rmt_tgg`
  MODIFY `id_rmt_tgg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pertanian_holti_jenis`
--
ALTER TABLE `pertanian_holti_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `pertanian_holti_jml`
--
ALTER TABLE `pertanian_holti_jml`
  MODIFY `id_holti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `pertanian_ikan_jenis`
--
ALTER TABLE `pertanian_ikan_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pertanian_ikan_jml`
--
ALTER TABLE `pertanian_ikan_jml`
  MODIFY `id_ikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pertanian_kelapa_tebu`
--
ALTER TABLE `pertanian_kelapa_tebu`
  MODIFY `id_pertanian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pertanian_komoditas_jenis`
--
ALTER TABLE `pertanian_komoditas_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pertanian_komoditas_jml`
--
ALTER TABLE `pertanian_komoditas_jml`
  MODIFY `id_komoditas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pertanian_lahan`
--
ALTER TABLE `pertanian_lahan`
  MODIFY `id_lahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pertanian_lahan_penggunaan`
--
ALTER TABLE `pertanian_lahan_penggunaan`
  MODIFY `id_peng` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pertanian_ternak_jenis`
--
ALTER TABLE `pertanian_ternak_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `pertanian_ternak_jml`
--
ALTER TABLE `pertanian_ternak_jml`
  MODIFY `id_ternak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `tehnologi_jml_bts`
--
ALTER TABLE `tehnologi_jml_bts`
  MODIFY `id_jml_bts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tehnologi_jml_warnet`
--
ALTER TABLE `tehnologi_jml_warnet`
  MODIFY `id_jml_warnet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tehnologi_jml_web_opd`
--
ALTER TABLE `tehnologi_jml_web_opd`
  MODIFY `id_web_opd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `trans_jln`
--
ALTER TABLE `trans_jln`
  MODIFY `id_jln` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `trans_jml_kendaraan`
--
ALTER TABLE `trans_jml_kendaraan`
  MODIFY `id_jml_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

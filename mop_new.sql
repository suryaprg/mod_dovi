-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2019 at 08:35 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mop_new`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin` (`id_lv` VARCHAR(2), `email` VARCHAR(20), `pass` VARCHAR(32), `nama` VARCHAR(64), `nip` VARCHAR(20), `jabatan` VARCHAR(50), `id_dinas` INT(3)) RETURNS VARCHAR(16) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(16);
  
  select count(*) into count_row_user from admin;
        
  select id_admin into last_key_user from admin	order by id_admin desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"000001");
  else
    set fix_key_user = concat("AD",SUBSTR(last_key_user,3,13)+1);  
  END IF;
  
  insert into admin values(fix_key_user, id_lv, email, pass, "1", nama, nip, jabatan, id_dinas, "0");
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kecamatan` (`nama` VARCHAR(50), `latlng` TEXT, `luas` DOUBLE) RETURNS VARCHAR(7) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from main_kecamatan;
        
  select id_kec into last_key_user from main_kecamatan order by id_kec desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("KEC1","001");
  else
    set fix_key_user = concat("KEC",SUBSTR(last_key_user,4,4)+1);  
  END IF;
  
  INSERT INTO main_kecamatan VALUES(fix_key_user, nama, latlng, luas, "0");
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kelurahan` (`nama` VARCHAR(64), `luas` DOUBLE, `latlng` TEXT, `id_kec` VARCHAR(7)) RETURNS VARCHAR(7) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from main_kelurahan;
        
  select id_kel into last_key_user from main_kelurahan order by id_kel desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("KEL1","001");
  else
    set fix_key_user = concat("KEL",SUBSTR(last_key_user,4,4)+1);  
  END IF;
  
  INSERT INTO main_kelurahan VALUES(fix_key_user, id_kec, nama, latlng, luas, "0");
  
  return fix_key_user;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(16) NOT NULL,
  `id_lv` int(2) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_dinas` int(3) NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `id_lv`, `email`, `password`, `status_active`, `nama`, `nip`, `jabatan`, `id_dinas`, `is_delete`) VALUES
('AD201811000001', 1, 'dsfsd', 'aff8fbcbf1363cd7edc85a1e11391173', '1', 'surya', '1234', 'surya', 3, '0'),
('AD201811000002', 3, 'surya', 'surya', '1', 'surya', '1234', 'surya', 20, '1'),
('AD201811000003', 1, 'kominfo', 'f970e2767d0cfe75876ea857f92e319b', '1', 'doms rian', '21312312', 'kepala', 2, '0'),
('AD201811000004', 2, 'surya', 'admin', '1', 'surya hanggara', '123343', 'makijan', 4, '1'),
('AD201811000005', 1, 'surya', 'aff8fbcbf1363cd7edc85a1e11391173', '1', 'surya', '3573011903940006', 'pengolah data', 3, '0');

-- --------------------------------------------------------

--
-- Table structure for table `admin_lv`
--

CREATE TABLE `admin_lv` (
  `id_lv` int(2) NOT NULL,
  `ket` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_lv`
--

INSERT INTO `admin_lv` (`id_lv`, `ket`) VALUES
(1, 'Admin Super'),
(2, 'Admin STATISTIK'),
(3, 'Admin OPD');

-- --------------------------------------------------------

--
-- Table structure for table `dinas_perdagangan`
--

CREATE TABLE `dinas_perdagangan` (
  `id_perdagangan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) DEFAULT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dinas_perdagangan`
--

INSERT INTO `dinas_perdagangan` (`id_perdagangan`, `id_jenis`, `id_sub_jenis`, `th`, `jml`) VALUES
(1, 1, 1, 2015, '27'),
(2, 1, 1, 2016, '27'),
(3, 1, 1, 2017, '26'),
(4, 1, 2, 2015, '11'),
(5, 1, 2, 2016, '11'),
(6, 1, 2, 2017, '13'),
(7, 1, 3, 2015, '1'),
(8, 1, 3, 2016, '1'),
(9, 1, 3, 2017, '1'),
(10, 1, 4, 2015, '1'),
(11, 1, 4, 2016, '1'),
(12, 1, 4, 2017, '1'),
(13, 1, 5, 2015, '9'),
(14, 1, 5, 2016, '10'),
(15, 1, 5, 2017, '11'),
(16, 1, 6, 2015, '0'),
(17, 1, 6, 2016, '0'),
(18, 1, 6, 2017, '0'),
(19, 2, 7, 2015, '27'),
(20, 2, 7, 2016, '27'),
(21, 2, 7, 2017, '26'),
(22, 2, 8, 2015, '0'),
(23, 2, 8, 2016, '0'),
(24, 2, 8, 2017, '0'),
(25, 2, 9, 2015, '7'),
(26, 2, 9, 2016, '7'),
(27, 2, 9, 2017, '7'),
(28, 3, 0, 2015, '0'),
(29, 3, 0, 2016, '0'),
(30, 3, 0, 2017, '0'),
(31, 4, 0, 2015, '1'),
(32, 4, 0, 2016, '1'),
(33, 4, 0, 2017, '1'),
(34, 5, 10, 2015, '391516952832'),
(35, 5, 10, 2016, '12660054977147'),
(36, 5, 10, 2017, '1359443700883'),
(37, 5, 11, 2015, '83067693237'),
(38, 5, 11, 2016, '130237389406'),
(39, 5, 11, 2017, '1023820482203'),
(40, 6, 12, 2015, '0'),
(41, 6, 12, 2016, '0'),
(42, 6, 12, 2017, '0'),
(43, 6, 13, 2015, '0'),
(44, 6, 13, 2016, '0'),
(45, 6, 13, 2017, '0'),
(46, 7, 0, 2015, '13669'),
(47, 7, 0, 2016, '13669'),
(48, 7, 0, 2017, '12940'),
(49, 8, 0, 2015, '4223'),
(50, 8, 0, 2016, '4223'),
(51, 8, 0, 2017, '4223'),
(52, 9, 0, 2015, '0'),
(53, 9, 0, 2016, '0'),
(54, 9, 0, 2017, '0'),
(55, 10, 0, 2015, '0'),
(56, 10, 0, 2016, '39'),
(57, 10, 0, 2017, '39'),
(58, 11, 0, 2015, '11'),
(59, 11, 0, 2016, '10'),
(60, 11, 0, 2017, '10'),
(61, 12, 0, 2015, '11'),
(62, 12, 0, 2016, '10'),
(63, 12, 0, 2017, '10');

-- --------------------------------------------------------

--
-- Table structure for table `dinas_perdagangan_jenis`
--

CREATE TABLE `dinas_perdagangan_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL,
  `unit` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dinas_perdagangan_jenis`
--

INSERT INTO `dinas_perdagangan_jenis` (`id_jenis`, `nama_jenis`, `unit`) VALUES
(1, 'Sarana Perdagangan', 'Buah'),
(2, 'Jenis Bangunan Pasar ', 'Buah'),
(3, 'Kontribusi sektor Perdagangan terhadap PDRB', '%'),
(4, 'Rumah Potong Hewan dan Unggas', 'Unit'),
(5, 'Nilai Total bersih Ekspor dan Impor Perdagangan', 'Rp. Juta'),
(6, 'Nilai Ekspor dam Impor Non Migas', 'Rp. Juta'),
(7, 'Jumlah Kelompok Pedagang / Usaha formal', 'Kelompok'),
(8, 'Jumlah Kelompok Pedagang / Usaha informal', 'Kelompok'),
(9, 'Jumlah Kelompok Pedagang / Usaha formal yang mendapatkan Bantuan Binaan dari Pemda', 'Kelompok'),
(10, 'Jumlah Kelompok Pedagang / Usaha informal yang mendapatkan Bantuan Binaan dari Pemda', 'Kelompok'),
(11, 'Jumlah Pameran / expo yang dilaksanakan ', 'Kegiatan / pameran'),
(12, 'Jumlah Pameran/ expo yang diikuti', 'Kegiatan / pameran');

-- --------------------------------------------------------

--
-- Table structure for table `dinas_perdagangan_sub_jenis`
--

CREATE TABLE `dinas_perdagangan_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL,
  `unit` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dinas_perdagangan_sub_jenis`
--

INSERT INTO `dinas_perdagangan_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`, `unit`) VALUES
(1, 1, 'Pasar Tradisional', 'Buah'),
(2, 1, 'Pasar  Swalayan/Supermarket/Toserba', 'Buah'),
(3, 1, 'Hipermarket', 'Buah'),
(4, 1, 'Pasar Grosir / Toko Grosir', 'Buah'),
(5, 1, 'Mall / Plaza', 'Buah'),
(6, 1, 'Pertokoan / Warung / Kios', 'Buah'),
(7, 2, 'Pasar Bangunan Permanen / Semi Permanen', 'Buah'),
(8, 2, 'Pasar Tanpa Bangunan Permanen  / Semi Permanen', 'Buah'),
(9, 2, 'Pusat Perdagangan', 'Unit'),
(10, 5, 'ekspor', 'Rp. Juta'),
(11, 5, 'impor', 'Rp. Juta'),
(12, 6, 'ekspor', 'Rp. Juta'),
(13, 6, 'impor', 'Rp. Juta');

-- --------------------------------------------------------

--
-- Table structure for table `disbudpar_dts`
--

CREATE TABLE `disbudpar_dts` (
  `id_main` int(11) NOT NULL,
  `nama` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disbudpar_dts`
--

INSERT INTO `disbudpar_dts` (`id_main`, `nama`) VALUES
(1, 'Balai Kota Malang'),
(2, 'Perpustakaan Kota Malang'),
(3, 'Musium Brawijaya'),
(4, 'Musium Mpu Purwa'),
(5, 'Pasar Burung'),
(6, 'Pasar Bunga Splendid'),
(7, 'Pasar Ikan'),
(8, 'Pasar Besar'),
(9, 'Masjid Jami'),
(10, 'Gereja Katedral Ijen'),
(11, 'Pura Luhur Dwijawarsa'),
(12, 'Klenteng Eng Ang Kiong'),
(13, 'Vihara Dharma Mitra'),
(14, 'Gereja Hati Kudus Yesus'),
(15, 'Sekolah Menengah Atas Katholik Cor Jesu'),
(16, 'Taman Rekreasi Kota'),
(17, 'Sekolah Model Terpadu'),
(18, 'Gedung Perusahaan Listrik Negara'),
(19, 'Taman Rekreasi Tlogomas'),
(20, 'Sentra Mebel Kemirahan'),
(21, 'Kampung Keramik Dinoyo'),
(22, 'Sentra Rotan Arjosari'),
(23, 'Sentra Sanitair Karang Besuki'),
(24, 'Kampung Kripik Sanan'),
(25, 'Alun-alun Tugu'),
(26, 'Alun-alun merdeka malang'),
(27, 'Sumur Windu Kendedes'),
(28, 'Universitas Muhamadiah Malang'),
(29, 'Universitas Brawijaya'),
(30, 'Universitas Negeri Malang'),
(31, 'Musium Bentoel'),
(32, 'Musium Malang Tempo Doloe'),
(33, 'Toko OEN'),
(34, 'Taman Senaputra'),
(35, 'Sentra Keramik Dinoyo'),
(36, 'Taman Merjosari'),
(37, 'Taman Trunojoyo'),
(38, 'Taman Merbabu'),
(39, 'Taman Kendedes'),
(40, 'Taman Slamet'),
(41, 'Taman Kota Kediri'),
(42, 'Taman Hutan Kota Malabar'),
(43, 'Taman TPA Supit Urang'),
(44, 'Taman Rektor Universitas Brawijaya'),
(45, 'Taman Kunang-Kunang'),
(46, 'Taman Riverside'),
(47, 'Play Ground'),
(48, 'Taman Permata Jingga'),
(49, 'Makam Ki Ageng Gribik'),
(50, 'Kawasan Pecinan'),
(51, 'Kampung Batik Claket'),
(52, 'Kampung Sinau'),
(53, 'Pasar Wisata Tugu'),
(54, 'Pasar Wisata Velodrom');

-- --------------------------------------------------------

--
-- Table structure for table `disbudpar_dtw_jenis`
--

CREATE TABLE `disbudpar_dtw_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disbudpar_dtw_jenis`
--

INSERT INTO `disbudpar_dtw_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Museum Brawijaya'),
(2, 'Museum Malang Tempo Doeloe'),
(3, 'Museum Bentoel'),
(4, 'Museum Mpu Purwa'),
(5, 'Taman Rekreasi Tlogomas'),
(6, 'Taman Rekreasi Senaputra'),
(7, 'Taman Rekreasi Tareko'),
(8, 'Taman Rekreasi Playground'),
(9, 'Lapangan Golf Araya'),
(10, 'Wisata Belanja Tugu'),
(11, 'Kampung Sanan Tempe'),
(12, 'Kampung Keramik Dinoyo'),
(13, 'Kampung 1000 Topeng'),
(14, 'Taman Merjosari'),
(15, 'Taman Cerdas Trunojoyo'),
(16, 'Taman Merbabu'),
(17, 'Taman Slamet'),
(18, 'Perpustakaan Kota Malang'),
(19, 'Kampung Tridi'),
(20, 'MACITO (Malang City Tour)'),
(21, 'Alun Alun Kota Malang'),
(22, 'Alun Alun Tugu Balaikota'),
(23, 'Kampung Warna Warni'),
(24, 'Kampung Glintung Go Green (3G)'),
(25, 'Car Free Day (Taman Jalan Ijen)'),
(26, 'Kampung  Religius Gribig'),
(27, 'Kampung Budaya Polowijen'),
(28, 'Kampung Buring'),
(29, 'Taman Kunang Kunang'),
(30, 'Kampung Putih');

-- --------------------------------------------------------

--
-- Table structure for table `disbudpar_dtw_main`
--

CREATE TABLE `disbudpar_dtw_main` (
  `id_main` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `wisman` varchar(20) NOT NULL,
  `wisnus` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disbudpar_dtw_main`
--

INSERT INTO `disbudpar_dtw_main` (`id_main`, `id_jenis`, `th`, `wisman`, `wisnus`) VALUES
(1, 1, 2013, '456', '32124'),
(2, 2, 2013, '338', '5712'),
(3, 3, 2013, '10', '75'),
(4, 4, 2013, '58', '4678'),
(5, 5, 2013, '4', '8790'),
(6, 6, 2013, '', '46036'),
(7, 7, 2013, '38', '93732'),
(8, 8, 2013, '', '111275'),
(9, 9, 2013, '387', '4562'),
(10, 10, 2013, '12', '14808'),
(11, 11, 2013, '1487', '1121000'),
(12, 12, 2013, '2708', '507000'),
(13, 13, 2013, '', ''),
(14, 14, 2013, '', ''),
(15, 15, 2013, '', ''),
(16, 16, 2013, '', ''),
(17, 17, 2013, '', ''),
(18, 18, 2013, '', ''),
(19, 19, 2013, '', ''),
(20, 20, 2013, '', ''),
(21, 21, 2013, '', ''),
(22, 22, 2013, '', ''),
(23, 23, 2013, '', ''),
(24, 24, 2013, '', ''),
(25, 25, 2013, '', ''),
(26, 26, 2013, '', ''),
(27, 27, 2013, '', ''),
(28, 28, 2013, '', ''),
(29, 29, 2013, '', ''),
(30, 30, 2013, '', ''),
(31, 1, 2014, '462', '67148'),
(32, 2, 2014, '183', '7208'),
(33, 3, 2014, '', ''),
(34, 4, 2014, '39', '2134'),
(35, 5, 2014, '4', '15658'),
(36, 6, 2014, '', ''),
(37, 7, 2014, '10', '102764'),
(38, 8, 2014, '85', '140064'),
(39, 9, 2014, '315', '6441'),
(40, 10, 2014, '329', '39104'),
(41, 11, 2014, '1775', '1227916'),
(42, 12, 2014, '2219', '573140'),
(43, 13, 2014, '', ''),
(44, 14, 2014, '220', '63294'),
(45, 15, 2014, '203', '50880'),
(46, 16, 2014, '59', '57735'),
(47, 17, 2014, '', ''),
(48, 18, 2014, '122', '69590'),
(49, 19, 2014, '', ''),
(50, 20, 2014, '', ''),
(51, 21, 2014, '', ''),
(52, 22, 2014, '', ''),
(53, 23, 2014, '', ''),
(54, 24, 2014, '', ''),
(55, 25, 2014, '', ''),
(56, 26, 2014, '', ''),
(57, 27, 2014, '', ''),
(58, 28, 2014, '', ''),
(59, 29, 2014, '', ''),
(60, 30, 2014, '', ''),
(61, 1, 2015, '597', '73122'),
(62, 2, 2015, '262', '7888'),
(63, 3, 2015, '', ''),
(64, 4, 2015, '', ''),
(65, 5, 2015, '', '28621'),
(66, 6, 2015, '', ''),
(67, 7, 2015, '10', '139274'),
(68, 8, 2015, '', '170488'),
(69, 9, 2015, '291', '7166'),
(70, 10, 2015, '136', '46272'),
(71, 11, 2015, '1581', '1593707'),
(72, 12, 2015, '2767', '724723'),
(73, 13, 2015, '', ''),
(74, 14, 2015, '101', '75172'),
(75, 15, 2015, '205', '111279'),
(76, 16, 2015, '283', '72025'),
(77, 17, 2015, '', ''),
(78, 18, 2015, '', '89137'),
(79, 19, 2015, '', ''),
(80, 20, 2015, '1217', '19594'),
(81, 21, 2015, '1155', '100528'),
(82, 22, 2015, '', ''),
(83, 23, 2015, '', ''),
(84, 24, 2015, '', ''),
(85, 25, 2015, '', ''),
(86, 26, 2015, '', ''),
(87, 27, 2015, '', ''),
(88, 28, 2015, '', ''),
(89, 29, 2015, '23', '31075'),
(90, 30, 2015, '', ''),
(91, 1, 2016, '645', '134021'),
(92, 2, 2016, '139', '3029'),
(93, 3, 2016, '', ''),
(94, 4, 2016, '', ''),
(95, 5, 2016, '', '34245'),
(96, 6, 2016, '', ''),
(97, 7, 2016, '49', '214410'),
(98, 8, 2016, '', '30242'),
(99, 9, 2016, '269', '5707'),
(100, 10, 2016, '609', '166418'),
(101, 11, 2016, '1030', '815583'),
(102, 12, 2016, '865', '606449'),
(103, 13, 2016, '', ''),
(104, 14, 2016, '100', '80903'),
(105, 15, 2016, '430', '138805'),
(106, 16, 2016, '239', '72492'),
(107, 17, 2016, '54', '78550'),
(108, 18, 2016, '', '100307'),
(109, 19, 2016, '', ''),
(110, 20, 2016, '979', '35977'),
(111, 21, 2016, '1034', '621249'),
(112, 22, 2016, '1535', '78160'),
(113, 23, 2016, '211', '411074'),
(114, 24, 2016, '83', '41674'),
(115, 25, 2016, '402', '190283'),
(116, 26, 2016, '', ''),
(117, 27, 2016, '', ''),
(118, 28, 2016, '', ''),
(119, 29, 2016, '862', '127496'),
(120, 30, 2016, '', ''),
(121, 1, 2017, '333', '38634'),
(122, 2, 2017, '', ''),
(123, 3, 2017, '', ''),
(124, 4, 2017, '', ''),
(125, 5, 2017, '', '43108'),
(126, 6, 2017, '', ''),
(127, 7, 2017, '', ''),
(128, 8, 2017, '', '36262'),
(129, 9, 2017, '498', '7188.96'),
(130, 10, 2017, '686', '186117'),
(131, 11, 2017, '1222', '1236903'),
(132, 12, 2017, '1291', '829910'),
(133, 13, 2017, '', '23588'),
(134, 14, 2017, '115', '91911'),
(135, 15, 2017, '747', '132566'),
(136, 16, 2017, '339', '89854'),
(137, 17, 2017, '25', '99019'),
(138, 18, 2017, '', '121071'),
(139, 19, 2017, '', ''),
(140, 20, 2017, '1157', '52427'),
(141, 21, 2017, '1938', '698816'),
(142, 22, 2017, '1676', '87810'),
(143, 23, 2017, '178', '610376'),
(144, 24, 2017, '194', '57696'),
(145, 25, 2017, '559', '273012'),
(146, 26, 2017, '', ''),
(147, 27, 2017, '', ''),
(148, 28, 2017, '', '13602'),
(149, 29, 2017, '975', '160969'),
(150, 30, 2017, '', '13602'),
(151, 1, 2018, '377', '42800'),
(152, 2, 2018, '', ''),
(153, 3, 2018, '', ''),
(154, 4, 2018, '', ''),
(155, 5, 2018, '', '18490'),
(156, 6, 2018, '', ''),
(157, 7, 2018, '', ''),
(158, 8, 2018, '', '24277'),
(159, 9, 2018, '225', '2839'),
(160, 10, 2018, '311', '75216'),
(161, 11, 2018, '520', '490328'),
(162, 12, 2018, '550', '328990'),
(163, 13, 2018, '', '3311'),
(164, 14, 2018, '48', '37935'),
(165, 15, 2018, '415', '52416'),
(166, 16, 2018, '171', '35215'),
(167, 17, 2018, '16', '39173'),
(168, 18, 2018, '', '47581'),
(169, 19, 2018, '243', '40253'),
(170, 20, 2018, '546', '20641'),
(171, 21, 2018, '929', '272895'),
(172, 22, 2018, '845', '34320'),
(173, 23, 2018, '71', '191763'),
(174, 24, 2018, '106', '22578'),
(175, 25, 2018, '275', '118928'),
(176, 26, 2018, '', '800'),
(177, 27, 2018, '', ''),
(178, 28, 2018, '', '5244'),
(179, 29, 2018, '', ''),
(180, 30, 2018, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `disbudpar_kunjungan_hotel_jenis`
--

CREATE TABLE `disbudpar_kunjungan_hotel_jenis` (
  `id_jenis` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `jml_kmr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disbudpar_kunjungan_hotel_jenis`
--

INSERT INTO `disbudpar_kunjungan_hotel_jenis` (`id_jenis`, `id_kategori`, `nama_jenis`, `alamat`, `jml_kmr`) VALUES
(1, 1, 'Hotel Malang', 'Jl. Zainal Arifin No. 85 Malang', 15),
(2, 1, 'Hotel Megah Mansion', 'Jl. Laks. Martadinata No. 9', 29),
(3, 2, 'Pondok Wisata Gress Home Stay', 'Jl. Kahayan No. 6 Malang', 11),
(4, 1, 'Hotel Mutiara', 'Jl. Jaksa Agung Suprapto 30-32 Malang', 32),
(5, 1, 'Hotel Trio Indah I', 'Jl. Jaksa Agung Suprapto No. 18-20 Malang', 53),
(6, 1, 'Hotel Trio Indah II', 'Jl. Brigjen Slamet Riyadi No. 1 - 3 Malang', 40),
(7, 1, 'Hotel Sampurna Asri', 'Jl. Kolonel Sugiono 166 Malang\r\n', 27),
(8, 3, 'Hotel Santika', 'Jl. Letjen Sutoyo No. 79 Malang\r\n', 112),
(9, 2, 'Pondok Wisata Armyn\'s Guest House', 'Jl. Telomoyo No. 22 Malang\r\n', 12),
(10, 3, 'Hotel Gajahmada Graha', 'Jl. Dr. Cipto No. 17 Malang\r\n', 44),
(11, 1, 'Hotel Graha Dewata Agung ', 'Jl. R. Panji Suroso No. 10 Malang\r\n', 34),
(12, 1, 'Hotel Arjosari', 'Jl. Raden Intan No. 49 Malang\r\n', 20),
(13, 1, 'Hotel Serayu', 'Jl. Serayu\r\n', 40),
(14, 1, 'Hotel Armi', 'Jl. Kaliurang No. 63 Malang\r\n', 46),
(15, 1, 'Hotel Malinda', 'Jl. Zainul Arifin No. 37-39 Malang\r\n', 26),
(16, 1, 'Hotel Menara', 'Jl. Pajajaran No. 5 Malang\r\n', 19),
(17, 1, 'Hotel Tirto', 'Jl. Simp. Panji Suroso Kav. 133 Malang\r\n', 19),
(18, 1, 'Hotel Palem I', 'Jl. Hassanudin No. 10 Malang\r\n', 15),
(19, 1, 'Hotel Palem II', 'Jl. Thamrin No. 15 Malang\r\n', 33),
(20, 1, 'Hotel Pinus', 'Jl. Sunandar Priyo Sudarmo No. 32 Malang\r\n', 45),
(21, 1, 'Hotel Kalpataru', 'Jl. Kalpataru No. 43 Malang\r\n', 30),
(22, 1, 'Hotel Royall Inn', 'Jl. Tenaga Baru No. I /15 malang\r\n', 50),
(23, 3, 'Hotel Sahid Montana', 'Jl. Kahuripan  No. 9 Malang\r\n', 70),
(24, 1, 'Hotel Griyadi Montana', 'Jl. Candi Panggung No. 2 Malang\r\n', 70),
(25, 1, 'Hotel Megawati', 'Jl. Panglima Sudirman No. 99 Malang\r\n', 26),
(26, 1, 'Hotel Pajajaran', 'Jl. Letjen Sutoyo No. 178 Malang\r\n', 50),
(27, 1, 'Hotel Helios ', 'Jl. Pattimura No. 37 Malang\r\n', 19),
(28, 3, 'Hotel Pelangi I', 'Jl. Merdeka Selatan No. 3 Malang\r\n', 75),
(29, 1, 'Hotel Pelangi II', 'Jl. Simpang Gajayana No. 575 B Malang\r\n', 47),
(30, 3, 'Hotel Grand Palace', 'Jl. Ade Irma Suryani No. 23 Malang\r\n', 50),
(31, 1, 'Hotel Wilis Indah', 'Jl. Dr. Wahidin No. 40 Malang\r\n', 20),
(32, 1, 'Hotel Aloha', 'Jl. Gajah Mada No. 7 Malang\r\n', 25),
(33, 1, 'Hotel Margosuko', 'Jl. K.H Ahamad Dahlan No. 40-42 Malang\r\n', 28),
(34, 1, 'Hotel Arjuno', 'Jl. Brigjen Slamet Riyadi 122 Malang\r\n', 12),
(35, 1, 'Hotel Emma', 'Jl. Trunojoyo No. 21 Malang\r\n', 10),
(36, 1, 'Hotel Nugroho', 'Jl. Panji Suroso No. 16 Malang\r\n', 59),
(37, 1, 'Hotel Setia Budi', 'Jl. Pattimura No. 71 A Malang\r\n', 20),
(38, 1, 'Hotel Kahuripan', 'Jl. Kahuripan  No. 11 D Malang\r\n', 10),
(39, 1, 'Hotel Riche ', 'Jl. Jendral Basuki Rahmad No. 1 Malang\r\n', 52),
(40, 1, 'Hotel Mandala Puri', 'Jl. Panglima Sudirman No.81 Malang\r\n', 19),
(41, 1, 'Hotel Tosari', 'Jl. K.H Ahmad Dahlan No. 31 Malang\r\n', 30),
(42, 1, 'Hotel Santoso', 'Jl. K.H Agus Salim No. 24 Malang\r\n', 50),
(43, 2, 'Pondok Wisata PT. Dermaga Adi Kencana', 'JL. Mgr. Sugio Pranoto 3 C-P Malang \r\n', 5),
(44, 1, 'Hotel Kartika Kusuma', 'Jl. Kahuripan No.12 Malang\r\n', 31),
(45, 2, 'Pondok Wisata Graha Asri', 'Jl. Welirang no. 6 Malang\r\n', 10),
(46, 1, 'Hotel Camelia', 'Jl. Dr. Cipto No. 24 Malang\r\n', 28),
(47, 1, 'Hotel Griya Bromo', 'Jl. Bromo No. 7 Malang\r\n', 11),
(48, 1, 'Hotel Pusposari', 'Jl. Kolonel Sugiyono I/16 Malang\r\n', 20),
(49, 1, 'Hotel Windu Kencono', 'Jl. Kolonel Sugiyono 46 Malang\r\n', 10),
(50, 2, 'Pondok Wisata Kosabra', 'Jl. Gresik No. 4 Malang\r\n', 9),
(51, 2, 'Penginapan KPRI Mitra Sejahtera', 'Jl. Panglima Sudirman No. 93 Malang\r\n', 15),
(52, 3, 'The Shalimaar', 'Jl. Cerme No. 165 Malang\r\n', 0),
(53, 3, 'Hotel Kartika Graha', 'Jl. Jaksa Agung Suprapto No. 17 Malang\r\n', 79),
(54, 2, 'Pondok Wisata Fendy\'s Guest House', 'Jl. Kawi No. 48 Malang\r\n', 14),
(55, 1, 'Hotel UB', 'Jl. MT. Haryono No. 169 Malang\r\n', 39),
(56, 1, 'Hotel Bintang', 'Jl. Hamid Rusdi No. 87 Malang\r\n', 21),
(57, 2, 'Pondok Wisata  Peye Guest House', 'Jl. Simpang Dieng No. 1 Malang\r\n\r\n', 12),
(58, 2, 'Pondok Wisata Guest House Oscar', 'Jl. Raya Kebonsari No. 9 A Malang\r\n', 7),
(59, 2, 'Pondok Wisata Jona\'s Homestay', 'Jl. Dr. Sutomo No. 4 Malang\r\n', 8),
(60, 2, 'Pondok Wisata Guest House Singo Nade Inn', 'Jl. Bend. Sigura-gura Barat III No. 20 \r\n', 50),
(61, 3, 'Hotel Regent \'s Park', 'Jl. Jaksa Agung Suprapto No. 12-16 Malang\r\n', 23),
(62, 3, 'Hotel Splendid Inn', 'Jl. Mojopahir No. 4 Malang\r\n', 27),
(63, 3, 'Hotel Tugu', 'Jl. Tugu No. 1 Malang\r\n', 49),
(64, 3, 'Hotel Citi Hub', 'Jl. Pasar Besar No. 58 Malang\r\n', 19),
(65, 1, 'Hotel Olino Garden', 'Jl. Aris Munandar No. 41-45 Malang\r\n', 80),
(66, 2, 'Guest House Wisma Jasa Tirta', 'Jl. Besar Ijen No. 52 Malang\r\n', 10),
(67, 2, 'Hotel dan Villa Ubud', 'Jl. Bend. Sigura-gura Barat I No. 8 Malang\r\n', 30),
(68, 2, 'Guest House Cozy', 'Jl. Ringgit No. 8 Malang (Jl. TGP)\r\n', 14),
(69, 1, 'Hotel Edotel Senior Malang', 'Jl. Veteran No. 17 Malang\r\n', 16),
(70, 2, 'Guest House CV. Merbabu Graha Makmur', 'Jl. Merbabu No. 26 Malang\r\n', 11),
(71, 3, 'Hotel Aria Gajayana', 'Jl. Kawi No. 24 Malang\r\n', 158),
(72, 2, 'Enny\'s Guest House', 'Jl. Taman Wilis 1 A-B\r\n', 31),
(73, 4, 'Guset House Jero Sading', 'Jl. Serang No. 2 Malang\r\n', 7),
(74, 4, 'Guest House Shafira', 'Jl. Janti Barat Raya No. 6 Malang\r\n', 9),
(75, 4, 'Guest House D\'Fresh', 'Jl. Candi Trowulan No. 12 Malang\r\n', 30),
(76, 3, 'De Warna I', 'Jl Zainul Arifin 55 Malang\r\n', 47),
(77, 3, 'De Warna II', 'Jl. Letjen Sutoyo 22 Malang\r\n', 60),
(78, 3, 'Hotel Haris', 'Jl. A. Yani Utara, Riverside C-1\r\n', 229),
(79, 3, 'Hotel Amaris', 'Jl. Letjen Sutoyo 39 Malang\r\n', 111),
(80, 4, 'Guest House Kertanegara ', 'Jl. Semeru 59 Malang\r\n', 23),
(81, 3, 'Hotel Violet', 'Jl KH Wahid Hasyim\r\n', 11),
(82, 5, 'Hotel Morina', 'Jl. Dr. Cipto No. 5 Malang\r\n', 0),
(83, 4, 'Guest House \"Amalia\"', 'Jl. Merbabu No. 18 Malang\r\n', 20),
(84, 4, 'Guest House \"D Pavilion\"', 'Jl. Buring No. 37 Malang\r\n', 20),
(85, 3, 'Hotel Savana', 'Jl. Letjen Sutoyo 32-34 Malang\r\n', 122),
(86, 3, 'Hotel Best Western ', 'Jl. Dr. Cipto no.11 Malang\r\n', 129),
(87, 3, 'Hotel Ibis Styles Malang', 'Jl. Letjen S. Parman No. 45 Malang\r\n', 150),
(88, 3, 'Hotel Atria & Conference Malang', 'Jl. Jend. S. Parman\r\n\r\n', 175),
(89, 3, 'Hotel Everyday Smart', 'Jl. Soekarno Hatta No. 2\r\n', 130),
(90, 3, 'Hotel Swiss Bel Inn Malang', 'Jl. Veteran No. 2 Malang\r\n', 203),
(91, 3, 'Hotel Horison Malang', 'Jl. Blimbing Indah Tengah I No. 1 Malang\r\n', 124),
(92, 4, 'Guest House \"Ciliwung\"', 'Jl. Ciliwung No. 8 malang\r\n', 14),
(93, 4, 'Guest House \"Pisang Kipas\"', 'Jl. Pisang Kipas No. 2 B Malang\r\n', 12),
(94, 3, 'Hotel Ijen Padjajaran Suites', 'Jl. Ijen Nirwana Raya Blok A-16 Malang', 140),
(95, 4, 'Guest House \"Lovender\"', 'Jl. Lamongan No. 12 Malang\r\n', 12),
(96, 2, 'Pondok Wisata \"Aventree\"', 'Jl. Soekarno Hatta B4 A RT 03 RW 10 Malang\r\n', 10),
(97, 6, 'Penginapan \"Griya Raggea', 'Jl. Taman Borobudur No. 8 Malang\r\n', 29),
(98, 4, 'Guest House \"Nat Nat\"', 'Jl. Raung No. 2 Malang\r\n', 22);

-- --------------------------------------------------------

--
-- Table structure for table `disbudpar_kunjungan_hotel_kate`
--

CREATE TABLE `disbudpar_kunjungan_hotel_kate` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disbudpar_kunjungan_hotel_kate`
--

INSERT INTO `disbudpar_kunjungan_hotel_kate` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Melati'),
(2, 'Pondok Wisata'),
(3, 'Bintang'),
(4, 'Guest House'),
(5, 'Non Bintang'),
(6, 'Penginapan');

-- --------------------------------------------------------

--
-- Table structure for table `disbudpar_kunjungan_hotel_main`
--

CREATE TABLE `disbudpar_kunjungan_hotel_main` (
  `id_main` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `wisman` int(11) NOT NULL,
  `wisnus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disbudpar_kunjungan_hotel_main`
--

INSERT INTO `disbudpar_kunjungan_hotel_main` (`id_main`, `id_jenis`, `th`, `wisman`, `wisnus`) VALUES
(3, 1, 2015, 0, 5481),
(4, 2, 2015, 0, 10594),
(5, 3, 2015, 0, 4037),
(6, 4, 205, 256, 11697),
(7, 5, 2015, 0, 19356),
(8, 6, 2015, 387, 14609),
(9, 7, 2015, 131, 9862),
(10, 8, 2015, 0, 40890),
(11, 9, 2015, 308, 4379),
(12, 10, 2015, 33, 9502),
(23, 11, 2015, 0, 12429),
(24, 12, 2015, 529, 7292),
(25, 13, 2015, 0, 14609),
(26, 14, 2015, 0, 16786),
(27, 15, 2015, 256, 9493),
(28, 16, 2015, 149, 4261),
(29, 17, 2015, 0, 5189),
(30, 18, 2015, 387, 5481),
(31, 19, 2015, 283, 12064),
(32, 20, 2015, 0, 16419),
(33, 21, 2015, 0, 10963),
(34, 22, 2015, 0, 18254),
(35, 23, 2015, 387, 25570),
(36, 24, 2015, 0, 25570),
(37, 25, 2015, 0, 8108),
(53, 26, 2015, 47, 4364),
(54, 27, 2015, 3078, 6924),
(55, 28, 2015, 1970, 27380),
(56, 29, 2015, 0, 17152),
(57, 30, 2015, 270, 18254),
(58, 31, 2015, 529, 7292),
(59, 32, 2015, 658, 9127),
(60, 33, 2015, 334, 10227),
(61, 34, 2015, 0, 4379),
(62, 35, 2015, 992, 3668),
(63, 36, 2015, 504, 21533),
(64, 37, 2015, 142, 7292),
(65, 38, 2015, 0, 3668),
(66, 39, 2015, 1365, 18987),
(67, 40, 2015, 504, 6924),
(83, 41, 2015, 785, 10963),
(84, 42, 2015, 436, 18254),
(85, 43, 2015, 412, 1836),
(86, 44, 2015, 0, 11329),
(87, 45, 2015, 0, 3668),
(88, 46, 2015, 0, 10227),
(89, 47, 2015, 283, 4037),
(90, 48, 2015, 0, 7292),
(91, 49, 2015, 0, 3668),
(92, 50, 2015, 0, 3303),
(93, 51, 2015, 0, 5481),
(94, 52, 2015, 0, 0),
(95, 53, 2015, 121, 10658),
(96, 54, 2015, 361, 5115),
(97, 55, 2015, 1016, 14243),
(98, 56, 2015, 0, 7658),
(99, 57, 2015, 308, 4379),
(100, 58, 2015, 181, 2569),
(101, 59, 2015, 207, 2935),
(102, 60, 2015, 283, 18254),
(103, 61, 2015, 605, 8394),
(104, 62, 2015, 824, 1739),
(105, 63, 2015, 3277, 4347),
(106, 64, 2015, 504, 6924),
(107, 65, 2015, 424, 18254),
(108, 66, 2015, 0, 3668),
(109, 67, 2015, 295, 10963),
(110, 68, 2015, 361, 5115),
(111, 69, 2015, 424, 5823),
(112, 70, 2015, 0, 4037),
(113, 71, 2015, 992, 57697),
(114, 72, 2015, 811, 11329),
(115, 73, 2015, 0, 2569),
(116, 74, 2015, 0, 3303),
(117, 75, 2015, 0, 10963),
(119, 76, 2015, 346, 17152),
(120, 77, 2015, 308, 21903),
(121, 78, 2015, 3373, 83610),
(122, 79, 2015, 463, 40521),
(123, 80, 2015, 605, 8394),
(124, 81, 2015, 283, 4037),
(125, 82, 2015, 0, 0),
(126, 83, 2015, 529, 7292),
(127, 84, 2015, 529, 7292),
(128, 85, 2015, 758, 31489),
(129, 86, 2015, 2034, 47102),
(130, 87, 2015, 1016, 54762),
(131, 88, 2015, 1970, 63888),
(132, 89, 2015, 567, 47470),
(133, 90, 2015, 1083, 74117),
(134, 91, 2015, 863, 45293),
(135, 92, 2015, 0, 5115),
(136, 93, 2015, 0, 4379),
(137, 94, 2015, 2717, 51116),
(138, 95, 2015, 308, 4379),
(139, 96, 2015, 256, 3668),
(140, 97, 2015, 115, 10594),
(141, 98, 2015, 0, 8024),
(142, 1, 2016, 0, 5790),
(143, 2, 2016, 0, 11778),
(144, 3, 2016, 0, 4680),
(145, 4, 2016, 261, 13097),
(146, 5, 2016, 0, 19762),
(147, 6, 2016, 401, 15277),
(148, 7, 2016, 133, 9459),
(149, 8, 2016, 0, 44607),
(150, 9, 2016, 317, 5136),
(151, 10, 2016, 0, 13326),
(152, 11, 2016, 0, 14107),
(153, 12, 2016, 544, 7656),
(154, 13, 2016, 0, 17812),
(155, 14, 2016, 0, 18724),
(156, 15, 2016, 261, 10496),
(157, 16, 2016, 517, 7965),
(158, 41, 2016, 0, 11716),
(159, 18, 2016, 401, 5886),
(160, 19, 2016, 290, 13066),
(161, 20, 2016, 0, 17827),
(162, 21, 2016, 0, 11946),
(163, 22, 2016, 0, 20751),
(164, 23, 2016, 401, 28052),
(165, 24, 2016, 0, 26895),
(166, 25, 2016, 0, 10986),
(167, 26, 2016, 28, 9442),
(168, 27, 2016, 3163, 6307),
(169, 28, 2016, 2028, 27890),
(170, 29, 2016, 0, 15277),
(171, 30, 2016, 275, 19859),
(172, 31, 2016, 544, 7132),
(173, 32, 2016, 675, 11716),
(174, 33, 2016, 345, 8565),
(175, 34, 2016, 0, 3512),
(176, 35, 2016, 1018, 2809),
(177, 36, 2016, 517, 20963),
(178, 37, 2016, 144, 2895),
(179, 38, 2016, 0, 3479),
(180, 39, 2016, 1399, 24408),
(181, 40, 2016, 517, 6029),
(182, 41, 2016, 806, 12224),
(183, 42, 2016, 448, 17992),
(184, 43, 2016, 420, 2274),
(185, 44, 2016, 0, 12187),
(186, 45, 2016, 0, 4048),
(187, 46, 2016, 0, 11406),
(188, 47, 2016, 290, 4904),
(189, 48, 2016, 0, 7409),
(190, 49, 2016, 0, 4307),
(191, 50, 2016, 0, 3948),
(192, 51, 2016, 0, 7069),
(193, 52, 2016, 290, 11243),
(194, 53, 2016, 103, 17681),
(195, 54, 2016, 374, 5593),
(196, 55, 2016, 1048, 15602),
(197, 56, 2016, 0, 7739),
(198, 57, 2016, 317, 4517),
(199, 58, 2016, 185, 2809),
(200, 59, 2016, 214, 3072),
(201, 60, 2016, 290, 18430),
(202, 61, 2016, 620, 8271),
(203, 62, 2016, 911, 8860),
(204, 63, 2016, 1612, 8712),
(205, 64, 2016, 517, 7295),
(206, 65, 2016, 433, 16498),
(207, 66, 2016, 0, 4633),
(208, 67, 2016, 303, 12156),
(209, 68, 2016, 374, 5637),
(210, 69, 2016, 433, 6906),
(211, 70, 2016, 0, 4649),
(212, 71, 2016, 1018, 61331),
(213, 72, 2016, 834, 11716),
(214, 73, 2016, 0, 2617),
(215, 74, 2016, 0, 3448),
(216, 75, 2016, 0, 12612),
(217, 76, 2016, 358, 19178),
(218, 77, 2016, 317, 25400),
(219, 78, 2016, 3469, 94827),
(220, 79, 2016, 476, 43096),
(221, 80, 2016, 620, 9374),
(222, 81, 2016, 290, 3933),
(223, 82, 2016, 0, 0),
(224, 83, 2016, 544, 5265),
(225, 84, 2016, 544, 8951),
(226, 85, 2016, 1165, 55430),
(227, 86, 2016, 2088, 58373),
(228, 87, 2016, 1048, 67739),
(229, 88, 2016, 2028, 79129),
(230, 89, 2016, 580, 58558),
(231, 90, 2016, 1108, 91132),
(232, 91, 2016, 891, 56391),
(233, 92, 2016, 0, 6291),
(234, 93, 2016, 0, 5470),
(235, 94, 2016, 2791, 63150),
(236, 95, 2016, 317, 5405),
(237, 96, 2016, 261, 4531),
(238, 97, 2016, 116, 13119),
(239, 98, 2016, 0, 9983),
(240, 1, 2017, 0, 6370),
(241, 2, 2017, 0, 13144),
(242, 3, 2017, 0, 5220),
(243, 4, 2017, 315, 14597),
(244, 5, 2017, 0, 22028),
(245, 6, 2017, 474, 17036),
(246, 7, 2017, 159, 10549),
(247, 8, 2017, 0, 49740),
(248, 9, 2017, 377, 5729),
(249, 10, 2017, 0, 14862),
(250, 11, 2017, 0, 15731),
(251, 12, 2017, 650, 8532),
(252, 13, 2017, 0, 19866),
(253, 14, 2017, 0, 20877),
(254, 15, 2017, 315, 11694),
(255, 16, 2017, 601, 8890),
(256, 17, 2017, 0, 13060),
(257, 18, 2017, 474, 6573),
(258, 19, 2017, 347, 14571),
(259, 20, 2017, 0, 19878),
(260, 21, 2017, 0, 13312),
(261, 22, 2017, 0, 23136),
(262, 23, 2017, 474, 31283),
(263, 24, 2017, 0, 29978),
(264, 25, 2017, 0, 12252),
(265, 26, 2017, 24, 10537),
(266, 27, 2017, 3715, 7033),
(267, 28, 2017, 2378, 31100),
(268, 29, 2017, 0, 17036),
(269, 30, 2017, 328, 22150),
(270, 31, 2017, 650, 7950),
(271, 32, 2017, 790, 13060),
(272, 33, 2017, 402, 9545),
(273, 34, 2017, 0, 3915),
(274, 35, 2017, 1203, 3130),
(275, 36, 2017, 601, 23365),
(276, 37, 2017, 176, 3239),
(277, 38, 2017, 0, 3879),
(278, 39, 2017, 1649, 27223),
(279, 40, 2017, 601, 6726),
(280, 41, 2017, 949, 13629),
(281, 42, 2017, 529, 20068),
(282, 43, 2017, 499, 2538),
(283, 44, 2017, 0, 13580),
(284, 45, 2017, 0, 4516),
(285, 46, 2017, 0, 12711),
(286, 47, 2017, 347, 5463),
(287, 48, 2017, 0, 8275),
(288, 49, 2017, 0, 4807),
(289, 50, 2017, 0, 4409),
(290, 51, 2017, 0, 7891),
(291, 52, 2017, 0, 12529),
(292, 53, 2017, 0, 19707),
(293, 54, 2017, 0, 6235),
(294, 55, 2017, 1234, 17399),
(295, 56, 2017, 0, 8627),
(296, 57, 2017, 377, 5035),
(297, 58, 2017, 214, 3130),
(298, 59, 2017, 250, 3442),
(299, 60, 2017, 347, 20552),
(300, 61, 2017, 737, 9208),
(301, 62, 2017, 1075, 9886),
(302, 63, 2017, 1891, 9715),
(303, 64, 2017, 601, 8134),
(304, 65, 2017, 516, 18401),
(305, 66, 2017, 0, 5170),
(306, 67, 2017, 359, 13556),
(307, 68, 2017, 426, 6279),
(308, 69, 2017, 516, 7706),
(309, 70, 2017, 0, 5187),
(310, 71, 2017, 1203, 68387),
(311, 72, 2017, 973, 13060),
(312, 73, 2017, 0, 2915),
(313, 74, 2017, 0, 3855),
(314, 75, 2017, 0, 14064),
(315, 76, 2017, 414, 21385),
(316, 77, 2017, 377, 28319),
(317, 78, 2017, 4074, 105740),
(318, 79, 2017, 560, 48050),
(319, 80, 2017, 737, 10450),
(320, 81, 2017, 347, 4385),
(321, 82, 2017, 0, 12480),
(322, 83, 2017, 650, 5862),
(323, 84, 2017, 650, 8134),
(324, 85, 2017, 1373, 62017),
(325, 86, 2017, 2467, 62052),
(326, 87, 2017, 1234, 69800),
(327, 88, 2017, 2378, 83198),
(328, 89, 2017, 687, 58029),
(329, 90, 2017, 1301, 85348),
(330, 91, 2017, 1050, 63878),
(331, 92, 2017, 0, 6016),
(332, 93, 2017, 0, 6463),
(333, 94, 2017, 3289, 63781),
(334, 95, 2017, 377, 5388),
(335, 96, 2017, 315, 4544),
(336, 97, 2017, 139, 13691),
(337, 98, 2017, 0, 11211);

-- --------------------------------------------------------

--
-- Table structure for table `disbudpar_obj_pemajuan_bud_kate`
--

CREATE TABLE `disbudpar_obj_pemajuan_bud_kate` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disbudpar_obj_pemajuan_bud_kate`
--

INSERT INTO `disbudpar_obj_pemajuan_bud_kate` (`id_kategori`, `nama_kategori`) VALUES
(3, 'Manuskrip'),
(4, 'Tradisi Lisan'),
(5, 'Adat Istiadat'),
(6, 'Ritus'),
(7, 'Pengetahuan Tradisiona'),
(8, 'Teknologi Tradisional'),
(9, 'Seni'),
(10, 'Bahasa'),
(11, 'Permainan Rakyat'),
(12, 'Olahraga Tradisional'),
(13, 'Cagar Budaya');

-- --------------------------------------------------------

--
-- Table structure for table `disbudpar_obj_pemajuan_bud_main`
--

CREATE TABLE `disbudpar_obj_pemajuan_bud_main` (
  `id_main` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_obj` text NOT NULL,
  `th` int(4) NOT NULL,
  `lokasi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disbudpar_obj_pemajuan_bud_main`
--

INSERT INTO `disbudpar_obj_pemajuan_bud_main` (`id_main`, `id_kategori`, `nama_obj`, `th`, `lokasi`) VALUES
(5, 3, 'Al-Quran terjemahan/tafsir bahasa Jawa yang tersimpan di koleksi pribadi Ki Jati Kusumo', 2018, '0'),
(6, 3, 'Atlas berbahasa Belanda yang tersimpan di koleksi pribadi Ki Jati Kusumo\r\n\r\n', 2018, '0'),
(7, 3, 'Bahjah Al-‘Ulum fi Syarhi fi Bayani Aqidah Al-Ushul', 2018, '0'),
(8, 3, 'Kitab Al-Aqidah', 2018, '0'),
(9, 3, 'Kitab Al-Miftah fi Syarhi Ma’rifatil Islam wal Iman', 2018, '0'),
(10, 3, 'Kitab Babad Tanah Jawi versi keratin', 2018, '0'),
(11, 3, 'Kitab Hukmul ‘Aqliy', 2018, '0'),
(12, 3, 'Kitab Keluarga Sunan Kalijaga', 2018, '0'),
(13, 3, 'Manuskrip Kuno (OM.Am).', 2018, '0'),
(14, 3, 'Prasasti Perunggu yang berisi Aksara Pali kuno yang tersimpan di koleksi pribadi Ki Jati Kusumo', 2018, '0'),
(15, 3, 'Sittiin Mas’alah', 2018, '0'),
(16, 4, 'Asal Mula Kota Malang', 2018, '0'),
(17, 4, 'Asal Mula Pisang Candi', 2018, '0'),
(18, 4, 'Asal-Usul Bandulan', 2018, '0'),
(19, 4, 'Asal-Usul Bunulrejo', 2018, '0'),
(20, 4, 'Asal-Usul Dusun Pilang', 2018, '0'),
(21, 4, 'Asal-Usul Kali Mewek', 2018, '0'),
(22, 4, 'Bahasa Walikan', 2018, '0'),
(23, 4, 'Cerita Kendedes', 2018, '0'),
(24, 4, 'Cerita Panji', 2018, '0'),
(25, 4, 'Cerita Suling', 2018, '0'),
(26, 4, 'Dongeng Pak On', 2018, '0'),
(27, 4, 'Legenda Gunung Arjuna Malang', 2018, '0'),
(28, 4, 'Legenda Mbah Purwo Di Kelurahan Polowijen', 2018, '0'),
(29, 4, 'Legenda Watu Genong Di Kelurahan Polowijen', 2018, '0'),
(30, 4, 'Sejarah Dusun Pilanf', 2018, '0'),
(31, 4, 'Sejarah Dusun Tawangsari', 2018, '0'),
(32, 5, 'Adat Bubak Kawah', 2018, '0'),
(33, 5, 'Adat Temanten Malang Keprabon', 2018, '0'),
(34, 5, 'Begandring', 2018, '0'),
(35, 5, 'Bersih desa', 2018, '0'),
(36, 5, 'Bersih Desa Kelurahan Arjosari', 2018, '0'),
(37, 5, 'Bersih Desa Kelurahan Bakalan Krajan', 2018, '0'),
(38, 5, 'Bersih Desa Kelurahan Balearjosari', 2018, '0'),
(39, 5, 'Bersih Desa Kelurahan Ciptomulyo', 2018, '0'),
(40, 5, 'Bersih Desa Kelurahan Dinoyo', 2018, '0'),
(41, 5, 'Bersih Desa Kelurahan Mojolangu', 2018, '0'),
(42, 5, 'Bersih Desa Kelurahan Purwodadi', 2018, '0'),
(43, 5, 'Bersih Desa Kelurahan Tanjungrejo', 2018, '0'),
(44, 5, 'Bersih Desa Kelurahan Tunggulwulung', 2018, '0'),
(45, 5, 'Bersih desa Kelurahan Tunjung Sekar', 2018, '0'),
(46, 5, 'Bersih Desa Polowijen', 2018, '0'),
(47, 5, 'Bubak Manten', 2018, '0'),
(48, 5, 'Ciblungan atau ciblon', 2018, '0'),
(49, 5, 'Galak gampil', 2018, '0'),
(50, 5, 'Gogoh', 2018, '0'),
(51, 5, 'Grebeg Mulud', 2018, '0'),
(52, 5, 'Gumbeng', 2018, '0'),
(53, 5, 'Jagong Bayi dan Sepasaran', 2018, '0'),
(54, 5, 'Kearifan Lokal Petani Tanaman Hias di Batu Malang', 2018, '0'),
(55, 5, 'Kemit', 2018, '0'),
(56, 5, 'Loro Pangkon', 2018, '0'),
(57, 5, 'Madik', 2018, '0'),
(58, 5, 'Marah', 2018, '0'),
(59, 5, 'Mecak', 2018, '0'),
(60, 5, 'Mekeh', 2018, '0'),
(61, 5, 'Nakokne', 2018, '0'),
(62, 5, 'Ngebug', 2018, '0'),
(63, 5, 'Nglomproh', 2018, '0'),
(64, 5, 'Nyeser', 2018, '0'),
(65, 5, 'Nyundangi', 2018, '0'),
(66, 5, 'Oyog', 2018, '0'),
(67, 5, 'Pancing sandal', 2018, '0'),
(68, 5, 'Pasang cok bakal', 2018, '0'),
(69, 5, 'Pawai Kebudayaan Kelurahan Purwantoro', 2018, '0'),
(70, 5, 'Pemakaman Jenazah', 2018, '0'),
(71, 5, 'Peningsetan', 2018, '0'),
(72, 5, 'Peron', 2018, '0'),
(73, 5, 'Petugan dino', 2018, '0'),
(74, 5, 'Rampal Celaket Bersyukur (RCB)', 2018, '0'),
(75, 5, 'Ritual di Punden Gasek, Klaseman, Badut dan Desan', 2018, '0'),
(76, 5, 'Ronda Ngawe', 2018, '0'),
(77, 5, 'Selamatan Desa Warga Kelurahan Kota Lama', 2018, '0'),
(78, 5, 'Selamatan Kelahiran Anak mulai masih dalam kandungan sampai pasca melahirkan', 2018, '0'),
(79, 5, 'Selamatan Tetakan (Supitan/Sunatan)', 2018, '0'),
(80, 5, 'Selametan Geblak Pati', 2018, '0'),
(81, 5, 'Selametan Nyatusi', 2018, '0'),
(82, 5, 'Selametan Nyewuni', 2018, '0'),
(83, 5, 'Selametan Patang Puluh Dinane', 2018, '0'),
(84, 5, 'Selametan Pendhak Pindo', 2018, '0'),
(85, 5, 'Selametan Pendhak Pisan', 2018, '0'),
(86, 5, 'Selametan Pitung Dinane', 2018, '0'),
(87, 5, 'Selametan Pitung Dinane', 2018, '0'),
(88, 5, 'Selametan Telung Dinane', 2018, '0'),
(89, 5, 'Slametan 10 asuro', 2018, '0'),
(90, 5, 'Sonjo', 2018, '0'),
(91, 5, 'Sunggo', 2018, '0'),
(92, 5, 'Tata Rias Manten Malang Keputren', 2018, '0'),
(93, 5, 'Tebusan pitik jago', 2018, '0'),
(94, 5, 'Tradisi 1 suro', 2018, '0'),
(95, 5, 'Tradisi Grebeg Suro', 2018, '0'),
(96, 5, 'Tradisi Mendhem Ari-Ari', 2018, '0'),
(97, 5, 'Tradisi Petri Bayi', 2018, '0'),
(98, 5, 'Tradisi Sungkeman', 2018, '0'),
(99, 5, 'Tradisi Suroan masyarakat Madyopuro', 2018, '0'),
(100, 5, 'Tradisi Syukuran Kamardikan', 2018, '0'),
(101, 5, 'Tradisi Uncalan Untu', 2018, '0'),
(102, 5, 'Tubo', 2018, '0'),
(103, 5, 'Tumpengan', 2018, '0'),
(104, 5, 'Upocoro Brojolan', 2018, '0'),
(105, 5, 'Upocoro Brokohan', 2018, '0'),
(106, 5, 'Upocoro Mitoni/Tingkepan/Pitung Sasen', 2018, '0'),
(107, 5, 'Upocoro Neloni', 2018, '0'),
(108, 5, 'Upocoro Ngupati/Patang Sasen', 2018, '0'),
(109, 5, 'Upocoro Procotan', 2018, '0'),
(110, 5, 'Upocoro Selapanan Dino', 2018, '0'),
(111, 5, 'Upocoro Tedhak Siten', 2018, '0'),
(112, 5, 'Urup', 2018, '0'),
(113, 5, 'Bersih Desa Polowijen', 2018, '0'),
(114, 5, 'Wuwung', 2018, '0'),
(115, 6, 'Atur Sesaji di Punden Makam Bendo Balearjosari', 2018, '0'),
(116, 6, 'Atur Sesaji Punden Bale Arjosari', 2018, '0'),
(117, 6, 'Barikan Kampung', 2018, '0'),
(118, 6, 'Barikan yang diadakan di Kelurahan Mulyorejo', 2018, '0'),
(119, 6, 'Bersih Desa Polowijen', 2018, '0'),
(120, 6, 'Doa bersama warga Kelurahan Dinoyo di Punden Aji Singo Menggolo pada setiap hari Senin pahing antara bulan Agustus atau bulan September', 2018, '0'),
(121, 6, 'Gulali, terdapat di Kelurahan Pisang Candi', 2018, '0'),
(122, 6, 'Hatur Agung Tunggulwulung', 2018, '0'),
(123, 6, 'Larung Sesaji di Sungai Brantas Malang', 2018, '0'),
(124, 6, 'Malem Suroan', 2018, '0'),
(125, 6, 'Mantu Kucing – Malang', 2018, '0'),
(126, 6, 'Metren', 2018, '0'),
(127, 6, 'Ngijabi (Ngadipuro Jaman Biyen)', 2018, '0'),
(128, 6, 'Nyekar Makam Mbah Reni Empu Topeng Malang Polowijen', 2018, '0'),
(129, 6, 'Oikomeni, perkumpulan keagamaan tiap bulan di Kelurahan Sukun.', 2018, '0'),
(130, 6, 'Padhang Bulanan', 2018, '0'),
(131, 6, 'Pencak silat', 2018, '0'),
(132, 6, 'Punden sumbersari', 2018, '0'),
(133, 6, 'Resik-Resik Punden Tasikmadu', 2018, '0'),
(134, 6, 'Ritual di goa buatan tepi kali Brantas yang terletak di Kelurahan Samaan (RW 08)', 2018, '0'),
(135, 6, 'Ritual di punden Eyang Sukma, Mbah Putri dan Mbah Wiryo', 2018, '0'),
(136, 6, 'Ritual di Punden Gasek, Klaseman, Badut dan Desan', 2018, '0'),
(137, 6, 'Ritual di Punden Tasikmadu', 2018, '0'),
(138, 6, 'Ritual di Sumur Windu Kendedes Polowijen', 2018, '0'),
(139, 6, 'Ritual doa bersama warga di punden Kelurahan Tunjung Sekar yang berlokasi di RW 3 setelah acara bersih desa', 2018, '0'),
(140, 6, 'Ritual doa di Belik Pitu Kelurahan Mojolangu', 2018, '0'),
(141, 6, 'Ritual Doa di makam Ki Ageng Gribig, Madyopuro', 2018, '0'),
(142, 6, 'Ritual Doa di Makam Lesanpuro', 2018, '0'),
(143, 6, 'Ritual Doa di makam Mbah Ponco Driyo (Makam Pejuang) yang diadakan rutin pada setiap malam 1 Suro', 2018, '0'),
(144, 6, 'Ritual Doa Punden Gribig', 2018, '0'),
(145, 6, 'Ritual Doa Sesaji', 2018, '0'),
(146, 6, 'Ritual Doa Tunjungsekar', 2018, '0'),
(147, 6, 'Ritual Kirim Dungo Ngaluhur', 2018, '0'),
(148, 6, 'Ritual malam Jum’at Legi', 2018, '0'),
(149, 6, 'Ritual Malam Purnomosidi', 2018, '0'),
(150, 6, 'Ritual Nyadran Senen Legi/ Kemis Legi', 2018, '0'),
(151, 6, 'Ritual Nyekar Malam Jum’at Legian', 2018, '0'),
(152, 6, 'Ritual Panguntapan', 2018, '0'),
(153, 6, 'Ritual Punden Tasikmadu', 2018, '0'),
(154, 6, 'Ritual Ruwahan', 2018, '0'),
(155, 6, 'Ritual Selamatan Punden Ki Ageng Masrangi Sawojajar', 2018, '0'),
(156, 6, 'Ritual Selamatan Punden Mbah Onggo / Raden Ronggo Talun Pasar', 2018, '0'),
(157, 6, 'Ritual Selamatan Punden Mbah Precet Bunulrejo', 2018, '0'),
(158, 6, 'Ritual Selamatan Punden Mbah Sentono Tembalangan', 2018, '0'),
(159, 6, 'Ritual Selamatan Punden Mbah Singo Mulangjoyo Kendalkerep', 2018, '0'),
(160, 6, 'Ritual Selamatan Punden Mbah Tugu Celaket', 2018, '0'),
(161, 6, 'Ritual Selamatan Punden Mbah Wareng Kedungkandang', 2018, '0'),
(162, 6, 'Ritual Selamatan Punden Rondo Kuning Arjosari', 2018, '0'),
(163, 6, 'Ritual Selamatan Punden Rondo Ngawe', 2018, '0'),
(164, 6, 'Ritual Sesaji Samaan', 2018, '0'),
(165, 6, 'Ritual syukuran berupa korban binatang ternak, makanan dan sesaji lainnya di Kelurahan Polowijen', 2018, '0'),
(166, 6, 'Ritual Tabur Bunga di Perempatan Jalan', 2018, '0'),
(167, 6, 'Ritual Tapel Adaman', 2018, '0'),
(168, 6, 'Ritual Unggahan Poso', 2018, '0'),
(169, 6, 'Sajen Bucalan', 2018, '0'),
(170, 6, 'Sajen Kamar Manten', 2018, '0'),
(171, 6, 'Sajen Manggulan', 2018, '0'),
(172, 6, 'Sajen Sepasaran', 2018, '0'),
(173, 6, 'Sajen Siraman', 2018, '0'),
(174, 6, 'Selamatan dengan dilengkapi sesajian di Patren', 2018, '0'),
(175, 6, 'Selamatan Kupat Lepet Lontong Janur', 2018, '0'),
(176, 6, 'Selamatan Punden Bandulan', 2018, '0'),
(177, 6, 'Selamatan Punden Karangbesuki', 2018, '0'),
(178, 6, 'Selamatan Punden Mojolangu', 2018, '0'),
(179, 6, 'Selamatan Satu Suro', 2018, '0'),
(180, 6, 'Selametan di Petren Joko Lolo Polowijen', 2018, '0'),
(181, 6, 'Slametan peringatan 1 Suro oleh warga Rampal Celaket', 2018, '0'),
(182, 6, 'Slametan rutin memperingati maulud nabi Muhammad SAW di Kelurahan Oro-Oro Dowo', 2018, '0'),
(183, 6, 'Suguh sandingan', 2018, '0'),
(184, 6, 'Sumur Windu di Kelurahan Polowijen', 2018, '0'),
(185, 6, 'Suroan warga Sawojajar', 2018, '0'),
(186, 6, 'Syukuran rutin memperingati 1 Suro di Kelurahan Oro-Oro Dowo', 2018, '0'),
(187, 6, 'Tahlil Rutin', 2018, '0'),
(188, 6, 'Tradisi Ruwatan Malang', 2018, '0'),
(189, 6, 'Adat Pengantin Malang', 2018, '0'),
(190, 6, 'Ritual di Sumur Windu Kendeds Polowijen', 2018, '0'),
(191, 6, 'Slametan di Petren Joko Lolo Polowijen', 2018, '0'),
(192, 6, 'Nyekar Makam Mbah Reni Mpu Topeng Polowijen', 2018, '0'),
(193, 7, 'Bakso Malang', 2018, '0'),
(194, 7, 'Bakwan Malang', 2018, '0'),
(195, 7, 'Batik', 2018, '0'),
(196, 7, 'Brownies Tempe', 2018, '0'),
(197, 7, 'Dodol Apel', 2018, '0'),
(198, 7, 'Gulali', 2018, '0'),
(199, 7, 'Jajanan Cenil, Terdapat Di Kelurahan Karang Besuki', 2018, '0'),
(200, 7, 'Jamu Tradisional', 2018, '0'),
(201, 7, 'Kerajinan Anyaman Bambu Malang', 2018, '0'),
(202, 7, 'Kerajinan Kompor Di Malang', 2018, '0'),
(203, 7, 'Kerajinan Monte Bumiayu Malang', 2018, '0'),
(204, 7, 'Kerajinan Monte Kedungkandang', 2018, '0'),
(205, 7, 'Kerajinan Tangan Gerabah Pisang Malang', 2018, '0'),
(206, 7, 'Keramik Dinoyo Malang', 2018, '0'),
(207, 7, 'Kripik Buah', 2018, '0'),
(208, 7, 'Kripik Tempe Malang', 2018, '0'),
(209, 7, 'Makanan Gethuk Dan Cenil Di Kelurahan Karangbesuki, Pisang Candi', 2018, '0'),
(210, 7, 'Pengobatan Tradisional', 2018, '0'),
(211, 7, 'Petungan Dino', 2018, '0'),
(212, 7, 'Pijit Tradisional', 2018, '0'),
(213, 7, 'Puthu, Terdapat Di Kelurahan Pisang Candi Dan Bunulrejo', 2018, '0'),
(214, 7, 'Rujak Gobet', 2018, '0'),
(215, 7, 'Sari Apel', 2018, '0'),
(216, 7, 'Tahu Lontong', 2018, '0'),
(217, 7, 'Tahu Telur', 2018, '0'),
(218, 7, 'Tukang Pijat Tradisional', 2018, '0'),
(219, 8, 'Cobek Malang', 2018, '0'),
(220, 8, 'Dokar – Malang', 2018, '0'),
(221, 8, 'Kriya Batik Tulis Kampung Budaya Polowijen (Motif Kendedes dan Topeng)', 2018, '0'),
(222, 8, 'Kriya Topeng Kayu Kampung Budaya Polowijen', 2018, '0'),
(223, 8, 'Pompa Sumur Tradisional', 2018, '0'),
(224, 8, 'Teknologi Pembuatan Gerabah', 2018, '0'),
(225, 8, 'Tosan Aji Pusaka', 2018, '0'),
(226, 8, 'Kriya Batik Tulis Kampung Budaya Polowijen', 2018, '0'),
(227, 8, 'Kriya Topeng Kayu Kampung Budaya Polowijen', 2018, '0'),
(228, 10, 'Bahasa Jawa', 2018, '0'),
(229, 10, 'Bahasa Madura', 2018, '0'),
(230, 10, 'Bahasa Walikan', 2018, '0'),
(231, 10, 'Geguritan (Sajak Jawa)', 2018, '0'),
(232, 10, 'Parikan', 2018, '0'),
(233, 10, 'Sastra', 2018, ''),
(234, 11, 'Beklu Tong-Tong', 2018, '0'),
(235, 11, 'Bakiak', 2018, '0'),
(236, 11, 'Bal Tembok', 2018, '0'),
(237, 11, 'Ban Banan', 2018, '0'),
(238, 11, 'Bandempo', 2018, '0'),
(239, 11, 'Bekelan', 2018, '0'),
(240, 11, 'Bekndan (Bekthor)', 2018, '0'),
(241, 11, 'Bentengan', 2018, '0'),
(242, 11, 'Biktor', 2018, '0'),
(243, 11, 'Boi- Boian', 2018, '0'),
(244, 11, 'Bola Api', 2018, '0'),
(245, 11, 'Ciblon', 2018, '0'),
(246, 11, 'Dadu Otok', 2018, '0'),
(247, 11, 'Dakon', 2018, '0'),
(248, 11, 'Dam Daman', 2018, '0'),
(249, 11, 'Dlemenan', 2018, '0'),
(250, 11, 'Engklar', 2018, '0'),
(251, 11, 'Engklek', 2018, '0'),
(252, 11, 'Gejrik', 2018, '0'),
(253, 11, 'Gobak Sodor/Bak Sodor/Grobak Sodor', 2018, '0'),
(254, 11, 'Halma', 2018, '0'),
(255, 11, 'Ik Ok', 2018, '0'),
(256, 11, 'Jailangkung', 2018, '0'),
(257, 11, 'Jamuran', 2018, '0'),
(258, 11, 'Jepretan (Karet)', 2018, '0'),
(259, 11, 'Jumpritan', 2018, '0'),
(260, 11, 'Karetan (Uncalan)', 2018, '0'),
(261, 11, 'Kastok', 2018, '0'),
(262, 11, 'Kecikan', 2018, '0'),
(263, 11, 'Kekehan (Gasing)', 2018, '0'),
(264, 11, 'Klek Klek Tembago', 2018, '0'),
(265, 11, 'Klompen Panjang', 2018, '0'),
(266, 11, 'Krempyeng An', 2018, '0'),
(267, 11, 'Krupukan', 2018, '0'),
(268, 11, 'Kucing – Kucing An', 2018, '0'),
(269, 11, 'Layangan', 2018, '0'),
(270, 11, 'Lomba Klompen Panjang', 2018, '0'),
(271, 11, 'Macanan', 2018, '0'),
(272, 11, 'Mercon Bumbung', 2018, '0'),
(273, 11, 'Nekeran', 2018, '0'),
(274, 11, 'Nyai/Nenek Putut', 2018, '0'),
(275, 11, 'Nyai Putut', 2018, '0'),
(276, 11, 'Nyi Puthut', 2018, '0'),
(277, 11, 'Panjat Pinang', 2018, '0'),
(278, 11, 'Permainan Ujung Malang', 2018, '0'),
(279, 11, 'Pring Edan', 2018, '0'),
(280, 11, 'Rojoan/ Rujian', 2018, '0'),
(281, 11, 'Saputangan', 2018, '0'),
(282, 11, 'Sethokan', 2018, '0'),
(283, 11, 'Sri Gandem', 2018, '0'),
(284, 11, 'Tarik Tambang dari Bambu', 2018, '0'),
(285, 11, 'Tekongan/Petak Umpet/Jumper Singit', 2018, '0'),
(286, 11, 'Tri Legetri', 2018, '0'),
(287, 11, 'Tulupan', 2018, '0'),
(288, 11, 'Ular-Ularan (Gagak-Gagakan)', 2018, '0'),
(294, 12, 'Bagiak Panjang', 2018, '0'),
(295, 12, 'Balap Karung', 2018, '0'),
(296, 12, 'Bambu Geprak', 2018, '0'),
(297, 12, 'Bambu Gila', 2018, '0'),
(298, 12, 'Bentengan', 2018, '0'),
(299, 12, 'Debus', 2018, '0'),
(300, 12, 'Egrang', 2018, '0'),
(301, 12, 'Egrang Bathok', 2018, '0'),
(302, 12, 'Engklek', 2018, '0'),
(303, 12, 'Gobak Sodor', 2018, '0'),
(304, 12, 'Jemparingan', 2018, '0'),
(305, 12, 'Jentik', 2018, '0'),
(306, 12, 'Kasti', 2018, '0'),
(307, 12, 'Lompat Karung', 2018, '0'),
(308, 12, 'Lompat Tali (Karetan)', 2018, '0'),
(309, 12, 'Pencak Dor', 2018, '0'),
(310, 12, 'Pencak Silat', 2018, '0'),
(311, 12, 'Perang-Perangan', 2018, '0'),
(312, 12, 'Sepak Bola Air', 2018, '0'),
(313, 12, 'Sepak Bola Api', 2018, '0'),
(314, 12, 'Sepak Bola Kampung', 2018, '0'),
(315, 12, 'Sepak Tekong (Boi-Boian)', 2018, '0'),
(316, 12, 'Tarik Tambang', 2018, '0'),
(317, 13, 'Kemuncak', 2018, 'Jl. Supriadi'),
(318, 13, 'Patirthan', 2018, 'Jl. Muharto Gg. 5 Utara (Bawah) Kutho Bedah'),
(319, 13, 'Pemakaman Kutho Bedah', 2018, 'Kutho Bedah'),
(320, 13, 'Situs Balaekambang', 2018, 'RT. 01 RW. 07 Kel. Telogomas (depan UNITRI) berada di area persawahan warga'),
(321, 13, 'Unsur bangunan', 2018, 'Gasek'),
(322, 13, 'Yoni', 2018, 'Gasek'),
(323, 13, 'Punden Mbah Wiro', 2018, 'Jl. Wiro Margo'),
(324, 13, 'Rs Lavallatte', 2018, 'Rampal Celaket'),
(325, 13, 'Brahma', 2018, 'Kasin'),
(326, 13, 'Arca Ganesa', 2018, 'Hotel Tugu Park Kasin'),
(327, 13, 'Hotel Pelangi', 2018, 'Jl. Merdeka Selatan No. 3 - kauman – klojen'),
(328, 13, 'Surat Yusuf', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(329, 13, 'SMP Negeri 1 Malang', 2018, 'Jl. Lawu 12 - Oro-Oro Dowo – Klojen'),
(330, 13, 'Situs Punden Bejisari', 2018, 'Jl. Vinolia (depan pemakaman Tunggul Wulung)'),
(331, 13, 'Dwarapala', 2018, 'Kasin'),
(332, 13, 'Lapik', 2018, 'Gasek'),
(333, 13, 'Punden Mbah Tugu', 2018, 'Jl. Letjen Sutoyo Gg. 2 (Celaket)'),
(334, 13, 'Situs Punden Mbah Sentono', 2018, 'Jl. Muharto Gg. 5 Selatan (Atas) Kutho Bedah'),
(335, 13, 'Unsur Bangunan', 2018, 'Gasek Karangbesuki'),
(336, 13, 'Nadiswara', 2018, 'Kasin'),
(337, 13, 'SMAN III', 2018, 'Jl. Sultan Agung Utara No. 7 - klojen - klojen'),
(338, 13, 'Gereja Santa Theresia (Katedral Ijen)', 2018, 'Jl. Buring No. 60 - klojen – klojen'),
(339, 13, 'Arca Dewi', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(340, 13, 'Toko Oen', 2018, 'Jl. Basuki Rahmad No. 5 - kauman - klojen'),
(341, 13, 'Lapik', 2018, 'gasek karangbesuki'),
(342, 13, 'Fr Arca Agastiya', 2018, 'Karangbesuki'),
(343, 13, 'Agastya', 2018, 'Kasin'),
(344, 13, 'Wisnu', 2018, 'Kasin'),
(345, 13, 'Apotek Boldy', 2018, 'Jl Gatot Subroto 31 Jodipan'),
(346, 13, 'Kantor TOPDAM V Brawijaya', 2018, 'Jl. Suropati No.15 - klojen – klojen'),
(347, 13, 'Coffe Kong Kow', 2018, 'Jl. Pattimura 1 - klojen – klojen'),
(348, 13, 'Prasasti Tija', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(349, 13, 'Stasiun Baru Kota Malang', 2018, 'Jl Trunojoyo Kidul Dalem'),
(350, 13, 'Prasasti Muncang', 2018, 'Museum Mpu Purwa'),
(351, 13, 'Fr Arca ganesa', 2018, 'Gasek Karangbesuki'),
(352, 13, 'Situs Karangbesuki', 2018, 'Karangbesuki'),
(353, 13, 'Agastya', 2018, 'Jalan Tugu No3 Kasin'),
(354, 13, 'SMAN IV', 2018, 'Jl. Tugu Utara No. 1 - klojen – klojen'),
(355, 13, 'Factory Outlet Cargo', 2018, 'Jl. Suropati No.10 - klojen – klojen'),
(356, 13, 'Asrama Mahasiswa Bali Gunung Agung', 2018, 'Jl. RA.Kartini 30 - klojen – klojen'),
(357, 13, 'Rumah Dinas Wali Kota Malang', 2018, 'Jl. Ijen No. 2 - gadingsari – klojen'),
(358, 13, 'SDN Kauman 1', 2018, 'Jl. Kauman 1 - kauman – klojen'),
(359, 13, 'Mahakala', 2018, 'Kasin'),
(360, 13, 'Nandiswara', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(361, 13, 'Kantor Resimen Induk Kodam V Brawijaya (RINDAM)', 2018, 'Jl. Suropati No.1 - klojen – klojen'),
(362, 13, 'Kantor Bintal (Pembinaan Mental) Kodam V/Brawijaya', 2018, 'Jl. Suropati No. 11 - klojen – klojen'),
(363, 13, 'Arca Dewa', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(364, 13, 'Rumah Tinggal', 2018, 'Jl. Ijen No. 57 - oro-oro dowo – klojen'),
(365, 13, 'Yoni', 2018, 'Jalan Tugu No3 Kasin'),
(366, 13, 'Gardu Listrik Patimura', 2018, 'Jl. Patimura - klojen – klojen'),
(367, 13, 'Agastya', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(368, 13, 'Rumah Tinggal (AIA Finansial)', 2018, 'jl. Diponegoro 8 - klojen – klojen'),
(369, 13, 'Al Quran', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(370, 13, 'Arca Dewa', 2018, 'Kasin'),
(371, 13, 'Alun-alun Bunder', 2018, 'Jl. Tugu No 1 - klojen – klojen'),
(372, 13, 'Prasasti Cunggrang II(Gunung Kawi)', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(373, 13, 'Rajah', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(374, 13, 'Bus Surat', 2018, 'Jl. Merdeka Selatan No. 5 - kauman - klojen'),
(375, 13, 'Rumah Dinas Bank Mandiri', 2018, 'Jl. Ijen No. 34 - Oro-oro dowo – klojen'),
(376, 13, 'Nandiswara', 2018, 'Kasin'),
(377, 13, 'Arca Pancuran Dewi Laksmi', 2018, 'Kasin'),
(378, 13, 'GPIB Immanuel', 2018, 'Jl. Tugu - klojen - klojen'),
(379, 13, 'Gereja Khati Kudus Yesus', 2018, 'Jl Sugiyopranoto No 2 Kidul Dalem'),
(380, 13, 'Rumah Dinas Bank Mandiri', 2018, 'Jl. Ijen No. 33 - Oro-oro dowo – klojen'),
(381, 13, 'Hutan Kota Malabar', 2018, 'Jl. Malabar - Oro-oro dowo – Klojen'),
(383, 13, 'Prasasti', 2018, 'Jalan Tugu No3 Kasin'),
(384, 13, 'Lontar', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(385, 13, 'Kantor Pelayanan Pajak Pratama Malang Selatan', 2018, 'Jl. Merdeka Utara No. 3 - kauman - klojen'),
(386, 13, 'Rumah Dinas Panglima KODAM V, Brawijaya', 2018, 'Jl. Tugu No. 2 - kauman – klojen'),
(387, 13, 'Alun-alun Kota', 2018, 'Jl. Merdeka - kauman – klojen'),
(388, 13, 'Kepala Arca', 2018, 'Hotel Tugu Park Kasin'),
(389, 13, 'Durga', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(390, 13, 'Rumah Makan Inggil', 2018, 'Jl Gajah Mada Kidul dalem'),
(391, 13, 'Wisma Tumapel (D/H Hotel Splendid)', 2018, 'Jl. Tumapel No. 1 - kauman - klojen'),
(392, 13, 'Rumah Tinggal', 2018, 'Jl. Anjasmoro 25 - Oro-Oro Dowo – Klojen'),
(393, 13, 'Ganesa', 2018, 'Jalan Tugu No3 Kasin'),
(394, 13, 'RS Panti Waluyo', 2018, 'Jl Nusa Kambangan 56 Kasin'),
(395, 13, 'SMAN I', 2018, 'Jl. Tugu Utara No. 1 - klojen – klojen'),
(396, 13, 'Sony Sugema Collegs', 2018, 'Jl. Pajajaran No. 2 - klojen – klojen'),
(397, 13, 'Splendid Inn', 2018, 'Jl. Majapahit 2-4 - kauman – klojen'),
(398, 13, 'Aula SKODAM V Brawijaya', 2018, 'Jl. Tugu No. 3 - klojen – klojen'),
(399, 13, 'Kolose Santo Yusup', 2018, 'Jl. DR. Soetomo 35 - klojen – klojen'),
(400, 13, 'Kemuncak', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(401, 13, 'Hotel Richa', 2018, 'Jl. Basuki Rachmad No. 1 - kauman - klojen'),
(402, 13, 'Gedung Bank Indonesia', 2018, 'Jl Merdeka Utara No 4 Kidul Dalem'),
(403, 13, 'Timbangan Paket dan Surat', 2018, 'Jl. Merdeka Selatan No. 5 - kauman - klojen'),
(404, 13, 'Balai kota', 2018, 'Jl. Tugu No 1 - klojen – klojen'),
(405, 13, 'Perkantoran AKLINDO', 2018, 'Jl. Sriwiaya No.1-9 - samaan - klojen'),
(406, 13, 'Kitab', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(407, 13, 'Boulevard Ijen', 2018, 'Jl. Ijen dan Ijen Besar - -'),
(408, 13, 'Lemari Besi', 2018, 'Jl. Merdeka Selatan No. 5 - kauman - klojen'),
(409, 13, 'Bank Commonwealth', 2018, 'Jl. Basuki Rahmat 81 - kauman - klojen'),
(410, 13, 'Biara Ursulin (SMPK Cor Jesu)', 2018, 'Jl. Jaksa Agung Suprapto No. 55 - samaan - klojen'),
(411, 13, 'Buk Gluduk', 2018, 'Jl Trunojoyo Kidul Dalem'),
(412, 13, 'Kantor PelayananPerbendaharaan Negara (KPPN)', 2018, 'Jl. Merdeka Selatan No. 1-2 - kauman - klojen'),
(413, 13, 'KCP Mandiri Malang Merdeka', 2018, 'Jl. Merdeka Barat - kauman - klojen'),
(414, 13, 'Fr. Lingga', 2018, 'Jl. Raya Tlogomas Gg. 8, RT. 04 RW. 05 - Tlogomas'),
(415, 13, 'Yoni', 2018, 'Jl. Kecubung'),
(416, 13, 'Arca Dewa', 2018, 'Di hal. Kntr. Seksi Keb.Dikbud Kodia Malang - Kasin – Klojen'),
(417, 13, 'Watu Gong', 2018, 'Jl. Raya Tlogomas Gg. 8, RT. 04 RW. 05 - Tlogomas'),
(418, 13, 'Watu Gong', 2018, 'Tlogomas. RT. 04 RW. 03 – Lowokwaru'),
(419, 13, 'Fr. Arca ganesa', 2018, 'Tlogomas RT. 04 / RW. 03, - Lowokwaru'),
(420, 13, 'Jaladwara', 2018, 'Tlogomas RT. 04 RW. 03 – Lowokwaru'),
(421, 13, 'Fr. Kaki arca', 2018, 'Tlogomas RT. 04 / RW. 03, - Lowokwaru'),
(422, 13, 'Bakalan Arca', 2018, 'Di hal. Kntr. Seksi Keb.Dikbud Kodia Malang - Kasin – Klojen'),
(423, 13, 'Fr. Arca', 2018, 'Tlogomas RT. 04 / RW. 03, - Lowokwaru'),
(424, 13, 'Arca Dewa', 2018, 'Tlogomas RT. 04 / RW. 03, - Lowokwaru'),
(425, 13, 'Fr. Arca ganesa', 2018, 'Di hal. Kntr. Seksi Keb.Dikbud Kodia Malang - Kasin – Klojen'),
(426, 13, 'Umpak', 2018, 'Di hal.belakang Mushola An Nur di Komplek Kampus Univ. Gajayana'),
(427, 13, 'Jaladwara', 2018, 'Di hal. Kntr. Seksi Keb.Dikbud Kodia Malang - Kasin – Klojen'),
(428, 13, 'Bak Air', 2018, 'Tlogomas RT. 04 / RW. 03, - Lowokwaru'),
(429, 13, 'Lumpang batu', 2018, 'Tlogomas RT. 04 / RW. 03, - Lowokwaru'),
(430, 13, 'Makara', 2018, 'Di hal.depan Gedung Bhayangkara, Jl. Pahlawan Trip No. 4'),
(431, 13, 'Yoni', 2018, 'Ketawang Gede – Lowokwaru'),
(432, 13, 'Arca Budha', 2018, 'Di hal.depan Gedung Bhayangkara, Jl. Pahlawan Trip No. 4'),
(433, 13, 'Patirthan', 2018, 'Jl. Muharto Gg. 5 Utara (Bawah) Kutho Bedah'),
(434, 13, 'Situs Punden Bejisari', 2018, '0'),
(435, 13, 'Situs Punden Mbah Wiro', 2018, '0'),
(436, 13, 'Pemakaman Kutho Bedah', 2018, '0'),
(437, 13, 'Situs Balaekambang', 2018, '0'),
(438, 13, 'Situs Punden Mbah Tugu', 2018, '0'),
(439, 13, 'Situs Punden Mbah Sentono', 2018, '0'),
(440, 13, 'Siwa', 2018, 'museum mpu purwa'),
(441, 13, 'Mahakala', 2018, 'museum mpu purwa'),
(442, 13, 'Tokoh Nenek Moyang', 2018, 'museum mpu purwa'),
(443, 13, 'Tokoh Dewa', 2018, 'museum mpu purwa'),
(444, 13, 'Durgamahisasuramardini', 2018, 'museum mpu purwa'),
(445, 13, 'Nandi?wara', 2018, 'museum mpu purwa'),
(446, 13, 'Kuncup Teratai', 2018, 'museum mpu purwa'),
(447, 13, 'Siwa Mahaguru', 2018, 'museum mpu purwa'),
(448, 13, 'Brahma', 2018, 'museum mpu purwa'),
(449, 13, 'Dewi Budhis', 2018, 'museum mpu purwa'),
(450, 13, 'Resi', 2018, 'museum mpu purwa'),
(451, 13, 'Arca Tokoh', 2018, 'museum mpu purwa'),
(452, 13, 'Ganesya', 2018, 'museum mpu purwa'),
(453, 13, 'Jaladwara', 2018, 'museum mpu purwa'),
(454, 13, 'Sangkhara', 2018, 'museum mpu purwa'),
(455, 13, 'Lontar', 2018, 'Gereja Kayutangan'),
(456, 13, 'Arca Megalithik Muda (tokoh nenek moyang)', 2018, 'museum mpu purwa'),
(457, 13, 'Trimurti', 2018, 'museum mpu purwa'),
(458, 13, 'Dwarapala', 2018, 'museum mpu purwa'),
(459, 13, 'Lembu Nadi', 2018, 'museum mpu purwa'),
(460, 13, 'Simbar', 2018, 'Kertosentono Ketawanggede kec. Lowokwaru Malang'),
(461, 13, 'Asrama Mahasiswa Bali Gunung', 2018, 'Jl. Kartini 30 Malang'),
(462, 13, 'Ganesa', 2018, 'museum mpu purwa'),
(463, 13, 'Batu Pelor', 2018, 'museum mpu purwa'),
(464, 13, 'Bata Merah', 2018, 'museum mpu purwa'),
(465, 13, 'Wisnu', 2018, 'museum mpu purwa'),
(466, 13, 'GanesyaBunulrejo', 2018, 'museum mpu purwa'),
(467, 13, 'Dewi Kesuburan', 2018, 'museum mpu purwa'),
(468, 13, 'Arca Dewi', 2018, 'museum mpu purwa'),
(469, 13, 'Tokoh Pertapa', 2018, 'museum mpu purwa'),
(470, 13, 'Ganesya Tikus', 2018, 'museum mpu purwa'),
(471, 13, 'Bodhisatwa', 2018, 'museum mpu purwa'),
(472, 13, 'Trimurti / Trisirah', 2018, 'museum mpu purwa'),
(473, 13, 'Makara', 2018, 'museum mpu purwa'),
(474, 13, 'Batu Umpak', 2018, 'Kertosentono Ketawanggede kec. Lowokwaru Malang'),
(475, 13, 'RS. Panti Waluyo (RKZ)', 2018, 'Jl. Tanimbar Malang'),
(476, 13, 'Fragmen Garuda', 2018, 'museum mpu purwa'),
(477, 13, 'FragmenKemuncak Candi', 2018, 'Ker'),
(478, 13, 'Yoni', 2018, 'Tlogomas kec. Lowokwaru'),
(479, 13, 'Batu Giling Pipisan', 2018, 'museum mpu purwa'),
(480, 13, 'Gerbang makam sukun', 2018, 'Sukun'),
(481, 13, 'Tandon Air Betek', 2018, 'Jl. Mayjen Pandjaitan - klojen – klojen'),
(482, 13, 'Durga', 2018, 'Kasin'),
(483, 13, 'Kapak Batu', 2018, 'Jalan Tugu No3 Kasin'),
(484, 13, 'Prasasti Muncang', 2018, 'Bandung Josari'),
(485, 13, 'Stasiun Kota Lama', 2018, 'Ciptomulyo'),
(486, 13, 'Nandi', 2018, 'Kasin'),
(487, 13, 'Jaladwara', 2018, 'Kasin'),
(488, 13, 'Ganesa', 2018, 'Kasin'),
(489, 13, 'Ganesa', 2018, 'Jl. Halmahera - klojen – klojen'),
(490, 13, 'Arca', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(491, 13, 'Timbangan Surat', 2018, 'Jl. Merdeka Selatan No. 5 - kauman - klojen'),
(492, 13, 'Majelis Agung Gereja Kristen Jawi Wetan', 2018, 'Sukun'),
(493, 13, 'Kemuncak', 2018, 'Bandung Josari'),
(494, 13, 'Dewa', 2018, 'Kasin'),
(495, 13, 'Jaladwara', 2018, 'Hotel Tugu Park Kasin'),
(496, 13, 'Hariti', 2018, 'Jalan Tugu No3'),
(497, 13, 'Gardu Listrik Ijen', 2018, 'Jl. Ijen - klojen - klojen'),
(498, 13, 'Brahma', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(499, 13, 'Yoni', 2018, 'Bakalan Kerajan'),
(500, 13, 'Siwa', 2018, 'Kasin'),
(501, 13, 'Lontar', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan - Kiduldalem – klojen'),
(502, 13, 'Yoni', 2018, 'Hotel Tugu Park Kasin'),
(503, 13, 'Rumah Tinggal/Agen Rachmalia Indah', 2018, 'Jl. HOS. Cokroaminoto 20 - klojen – klojen'),
(504, 13, 'Nandi', 2018, 'Gereja Paroki Yesus Kudus Kayu Tangan Kiduldalem'),
(505, 13, 'Frateran Bunda Hati Kudus', 2018, 'Jl. Jaksa Agung Suprapto No. 21 - samaan - klojen'),
(506, 13, 'SMK Bina Cendika YPK Malang', 2018, 'Jl. Semeru 42 - Oro-Oro Dowo – Klojen'),
(507, 13, 'Sekolah Teologi Reformed Injil Malang', 2018, 'Jl. Semeru 42 - Oro-Oro Dowo – Klojen'),
(508, 13, 'Yoni', 2018, 'Di Kantor PKK, Jl. Tambakjoyo No. 1- Merjosari- Lowokwaru'),
(509, 13, 'Arca Binatang', 2018, 'Merjosari - lowokwaru'),
(510, 13, 'Yoni', 2018, 'Merjosari - lowokwaru'),
(511, 13, 'Unsur bangunan', 2018, 'Merjosari - lowokwaru'),
(512, 13, 'Yoni', 2018, 'Jl. M.T. Haryono No. 213 – lowokwaru'),
(513, 13, 'Yoni', 2018, 'Jl. Raya Tlogomas Gg. 8, RT. 04 RW. 05 - Tlogomas'),
(514, 13, 'Lingga', 2018, 'Jl. Raya Tlogomas Gg. 8, RT. 04 RW. 05 - Tlogomas'),
(515, 13, 'Nandi', 2018, 'Jl. Raya Tlogomas Gg. 8, RT. 04 RW. 05 - Tlogomas'),
(516, 13, 'Unsur bangunan', 2018, 'Jl. Raya Tlogomas Gg. 8, RT. 04 RW. 05 - Tlogomas'),
(517, 13, 'Pancuran (Jaladwara)', 2018, 'Jl. Raya Tlogomas Gg. 8, RT. 04 RW. 05 - Tlogomas'),
(518, 13, 'Arca Agastya', 2018, 'Kasin'),
(519, 13, 'SMK Bina Cendika YPK', 2018, 'Jl. Semeru 42 Malang'),
(520, 13, 'STRIM', 2018, 'Jl. Semeru 40 Malang'),
(521, 13, 'ArcaTokoh Pertapa', 2018, 'museum mpu purwa'),
(522, 13, 'Kolose Santo Yusup', 2018, 'Jl. Dr. Soetomo Malang'),
(523, 13, 'Rumah Tinggal', 2018, 'Jl. Anjasmoro 25 Malang'),
(524, 13, 'Prasasti Dinoyo 2', 2018, 'museum mpu purwa'),
(525, 13, 'AIA Finansial', 2018, 'Jl. Diponegoro 8 Malang'),
(526, 13, 'Yoni', 2018, 'Hotel Tugu Jl. Tugu kel. Kauman'),
(527, 13, 'Batu Umpak Gong', 2018, 'Punden Watu Gong: Jl. Kanjuruhan Gg.IV RT 04 RW 03 kel. Tlogomas'),
(528, 13, 'Yoni', 2018, 'sekitar wilayah tempat tinggal warga'),
(529, 13, 'Rajah', 2018, 'Gereja Kayutangan'),
(530, 13, 'Menhir', 2018, 'Punden Mbah Tugu: Jl. JA. Suprapto I E (Celaket) kel. Samaan'),
(531, 13, 'Waruga', 2018, 'Punden Mbah Tugu: Jl. JA. Suprapto I E (Celaket) kel. Samaan'),
(532, 13, 'Batu Umpak', 2018, 'Universitas Gajayana Malang'),
(533, 13, 'Lingga Semu / Lingga Patok', 2018, 'museum mpu purwa'),
(534, 13, 'Tokoh Dewi Ibu', 2018, 'museum mpu purwa'),
(535, 13, 'N.V. Apotek Boldy', 2018, 'Jl. Gatot Subroto 31 Malang'),
(536, 13, 'Coffe Kong Kow', 2018, 'Jl. Pattimura Malang'),
(537, 13, 'Agen Rachmalia Indah', 2018, 'Jl. Cokroaminoto 20 Malang'),
(538, 13, 'Kitab', 2018, 'Gereja Kayutangan'),
(539, 13, 'Prasasti Widodaren', 2018, 'Hotel Tugu Jl. Tugu kel. Kauman'),
(540, 13, 'Siwa', 2018, 'Punden Karuman: Jl. Tlogomas Gg.VIII RT 04 RW 05 kel. Tlogomas'),
(541, 13, 'Ganesya', 2018, 'Punden Watu Gong: Jl. Kanjuruhan Gg.IV RT 04 RW 03 kel. Tlogomas'),
(542, 13, 'Yoni', 2018, 'Merjosari kec. Lowokwaru Malang'),
(543, 13, 'Bank Commonwealth', 2018, 'Jl. Basuki Rahmad 81 Malang'),
(544, 13, 'SDN Kauman 1 Malang', 2018, 'Jl. Kauman 1 Malang'),
(545, 13, 'Adhikaranandin/Vresamastaka', 2018, 'Hotel Tugu Jl. Tugu kel. Kauman'),
(546, 13, 'Arca Dewa', 2018, 'Punden Watu Gong: Jl. Kanjuruhan Gg.IV RT 04 RW 03 kel. Tlogomas'),
(547, 13, 'Ganesya', 2018, 'Gasek-Karangbesuki kec. Sukun Malang'),
(548, 13, 'Majelis Agung Gereja Kristen Jawi Wetan (GKJW)', 2018, 'Jl. S. Supriadi Malang'),
(549, 13, 'Batu Gores', 2018, 'museum mpu purwa'),
(550, 13, 'Budha Aksobhya', 2018, 'museum mpu purwa'),
(551, 13, 'Ganesya', 2018, 'Kertosentono Ketawanggede kec. Lowokwaru Malang'),
(552, 13, 'Yoni', 2018, 'Dinoyo kec. Lowokwaru Malang'),
(553, 13, 'Prasasti Cunggrang', 2018, 'Gereja Kayutangan'),
(554, 13, 'Serat Yusuf', 2018, 'Gereja Kayutangan'),
(555, 13, 'Dolmen', 2018, 'Punden Mbah Tugu: Jl. JA. Suprapto I E (Celaket) kel. Samaan'),
(556, 13, 'Yoni', 2018, 'Punden Karuman: Jl. Tlogomas Gg.VIII RT 04 RW 05 kel. Tlogomas'),
(557, 13, 'Lingga', 2018, 'sekitar wilayah tempat tinggal warga'),
(558, 13, 'Fragmen Pipi Tangga', 2018, 'Kertosentono Ketawanggede kec. Lowokwaru Malang'),
(559, 13, 'Batu Pipisan', 2018, 'Hibah dari Bapak Suradi Jl. Muharto Malang'),
(560, 13, 'SMP Negeri 1 Malang', 2018, 'Jl. Lawu 12 Malang'),
(561, 13, 'Prasasti Tija Haru-Haru', 2018, 'Gereja Kayutangan'),
(562, 13, 'Lingga', 2018, 'Punden Karuman: Jl. Tlogomas Gg.VIII RT 04 RW 05 kel. Tlogomas'),
(563, 13, 'Batu Kenong', 2018, 'sekitar wilayah tempat tinggal warga'),
(564, 13, 'Batu Pipisan', 2018, 'sekitar wilayah tempat tinggal warga'),
(565, 13, 'Buk Gluduk', 2018, 'Jl. Trunojoyo Malang'),
(566, 13, 'Tugu Pal', 2018, 'museum mpu purwa'),
(567, 13, 'Lembu Nadi', 2018, 'Punden Karuman: Jl. Tlogomas Gg.VIII RT 04 RW 05 kel. Tlogomas'),
(568, 13, 'Singa Stambha', 2018, 'sekitar wilayah tempat tinggal warga'),
(569, 13, 'Batu Lumpang', 2018, 'museum mpu purwa'),
(570, 13, 'Batu Lumpang', 2018, 'Punden Watu Gong: Jl. Kanjuruhan Gg.IV RT 04 RW 03 kel. Tlogomas'),
(571, 13, 'Pipisan', 2018, 'Punden Watu Gong: Jl. Kanjuruhan Gg.IV RT 04 RW 03 kel. Tlogomas'),
(572, 13, 'Batu Bundar', 2018, 'sekitar wilayah tempat tinggal warga'),
(573, 13, 'Batu Lumpang', 2018, 'sekitar wilayah tempat tinggal warga'),
(574, 13, 'Al Qur an', 2018, 'Gereja Kayutangan'),
(575, 13, 'Durgamahisasuramardini', 2018, 'Punden Karuman: Jl. Tlogomas Gg.VIII RT 04 RW 05 kel. Tlogomas'),
(576, 13, 'Batu Patok', 2018, 'Punden Karuman: Jl. Tlogomas Gg.VIII RT 04 RW 05 kel. Tlogomas'),
(577, 13, 'Bejana Batu', 2018, 'Punden Watu Gong: Jl. Kanjuruhan Gg.IV RT 04 RW 03 kel. Tlogomas'),
(578, 13, 'Dwarapala', 2018, 'sekitar wilayah tempat tinggal warga'),
(579, 13, 'Batu Fragmen', 2018, 'sekitar wilayah tempat tinggal warga'),
(580, 13, 'Arca Tokoh', 2018, 'Punden Watu Gong: Jl. Kanjuruhan Gg.IV RT 04 RW 03 kel. Tlogomas'),
(581, 13, 'Arca Budha Amoghasiddhi', 2018, 'Universitas Gajayana Malang'),
(582, 13, 'Batu Dakon / Watu Loso', 2018, 'sekitar wilayah tempat tinggal warga'),
(583, 13, 'Keris', 2018, 'Kelurahan dinoyo'),
(584, 13, 'Punden Jati Gembol', 2018, 'Kelayatan gang 1'),
(585, 13, 'Watu Genong', 2018, 'kelurahan polowijen'),
(586, 13, 'makam mbah raden', 2018, 'kelurahan samaan'),
(587, 13, 'candi badut', 2018, 'kelurahan karang besuki'),
(588, 13, 'Pondasi Kerajan', 2018, 'kelurahan karang besuki'),
(589, 13, 'punden makam bendo', 2018, 'kelurahan balerejosari'),
(590, 13, 'Makam Mbah Ashuro', 2018, 'kelurahan polowijen'),
(591, 13, 'suling peninggalan belanda', 2018, 'kelurahan samaan'),
(592, 13, 'Kris dan Tombak', 2018, 'kelurahan dinoyo'),
(593, 13, 'Rumah belanda', 2018, 'kelurahan samaan'),
(594, 13, 'Sumur Windu', 2018, 'kelurahan polowijen'),
(595, 13, 'Punden mbah tugu', 2018, 'kelurahan samaan'),
(596, 13, 'Watu Gong', 2018, 'kelurahan ketawang gede'),
(597, 13, 'Sungat Mewek', 2018, 'kelurahan polowijen'),
(598, 13, 'Gorong-gorong(Gua)', 2018, 'kelurahan samaan'),
(599, 13, 'punden ajisingo menggolo', 2018, 'kelurahan dinoyo'),
(600, 13, 'makam mbak kyai', 2018, 'kelurahan samaan'),
(601, 13, 'Tedung singo', 2018, 'kelurahan samaan'),
(602, 13, 'Bendungan Rola', 2018, 'Kelurahan Kedungkandang'),
(603, 13, 'Punden Mbah Bunul', 2018, 'Kelurahan Bunulrejo'),
(604, 13, 'lontar, aneka pecok perunggu, sabuk raja dari bahan logam koleksi pribadi Ki Jati Kusumo', 2018, '0'),
(605, 13, 'Koleksi rintisan Museum Kampung Tawangsari (Re-Enactor Ngalam)', 2018, 'Dusun Tawangsari Kelurahan Sumbersari'),
(606, 13, 'Gedung KPAN', 2018, 'Kelurahan Kauman'),
(607, 13, 'Rumah-rumah kuno penduduk', 2018, 'Kelurahan Kauman'),
(608, 13, 'Makam Mbah Raden', 2018, 'Kelurahan Sukun'),
(609, 13, 'Suling', 2018, 'Kelurahan Samaan'),
(610, 13, 'Makam Mbah Kyai', 2018, 'Kelurahan Samaan'),
(611, 13, 'Figur Wisnu, Siwa, Sarasvati dan Kendedes ukuran sedang', 2018, 'Kediaman Ki Jati Kusumo'),
(612, 13, 'Kantor Pos Kota Malang', 2018, 'Kelurahan Kauman'),
(613, 13, 'Gereja Immanuel', 2018, 'Kelurahan Kauman'),
(614, 13, 'Hotel Pelangi', 2018, 'Kelurahan Kauman'),
(615, 13, 'Masjid Jami’', 2018, 'Kelurahan Kauman'),
(616, 13, 'Koleksi di Rumah Makan Inggil milik Bapak Dwi Cahyono', 2018, '0'),
(617, 13, 'Situs Watu Gong', 2018, '0'),
(618, 13, 'Makam Mbah Tomo', 2018, 'Kelurahan Ciptomulyo'),
(619, 13, 'Makam Mbah Wareng', 2018, 'Kelurahan Kedungkandang'),
(620, 13, 'Fosil daging (dilihat dari jenis seratnya, diduga fosil daging gajah) dan tulang belakang hewan purba ', 2018, ' Koleksi pribadi Ki Jati Kusumo'),
(621, 13, 'Pondasi Kerajaan', 2018, 'Kelurahan Karang Besuki'),
(622, 13, 'Sekolahan Cor Jesu', 2018, 'Kelurahan Samaan'),
(623, 13, 'Gorong-gorong/Gua', 2018, 'Kelurahan Samaan'),
(624, 13, 'Makam Mbah Bango', 2018, 'Kelurahan Kedungkandang'),
(625, 13, 'Beberapa Topeng Tertua di Kota Malang yang terdapat di kediaman Ki Jati Kusumo', 2018, '0'),
(626, 13, 'Watu Rumpak', 2018, 'Dusun Pilang, Kelurahan Sumbersari.'),
(627, 13, 'Museum Mpu Purwa', 2018, 'Kelurahan Mojolangu'),
(628, 13, 'Situs Mbah Tugu', 2018, 'Kelurahan Samaan'),
(629, 13, 'Punden Mbah Raden ', 2018, 'RW 06 Kelurahan Sukun'),
(630, 13, 'Sekolahan Frateran', 2018, 'Kelurahan Samaan'),
(631, 13, 'Patung Kepala Siwa Budha ', 2018, 'Kediaman Ki Jati Kusumo'),
(632, 13, 'Situs Makam Mbah Reni Empu Topeng Malang Polowijen', 2018, '0'),
(633, 13, 'Situs Sumur Windu Kendedes Polowijen', 2018, '0'),
(634, 13, 'Situs Joko Lolo Polowijen', 2018, '0'),
(635, 13, 'Situs Sumur Windu Kendedes Polowijen', 2018, '0'),
(636, 13, 'Situs Joko Lolo Polowijen', 2018, '0');

-- --------------------------------------------------------

--
-- Table structure for table `disbudpar_zona_kreatif`
--

CREATE TABLE `disbudpar_zona_kreatif` (
  `id_zona` int(11) NOT NULL,
  `nama_zona` text NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disbudpar_zona_kreatif`
--

INSERT INTO `disbudpar_zona_kreatif` (`id_zona`, `nama_zona`, `alamat`) VALUES
(127, 'Gedung kesenian Gajayana', 'Jl. Nusakambangan, Kasin, Klojen, Kota Malang, Jawa Timur 65117'),
(128, 'Taman Krida Budaya', 'Jl. Soekarno Hatta No.7, Jatimulyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142'),
(129, 'Gedung Baiduri Sepah', 'Jl. Raya Tlogomas No. 1 Malang 65144'),
(130, 'Aula Skodam Gedung Bela Negara', 'Jl. Belanegara Kodam V Brawijaya, Kesatrian, Blimbing, Kota Malang, Jawa Timur'),
(131, 'Sasana Krida Universitas Negeri Malang', 'Universitas Negeri Malang, Jl. Veteran, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144'),
(132, 'Gedung Dewan Kesenian Kota Malang', 'Jl. Majapahit No.3, Kauman, Klojen, Kota Malang, Jawa Timur 65119'),
(133, 'Graha Cakrawala Universitas Negeri Malang', 'Pusat Bisnis Universitas Negeri Malang, Jalan Semarang No. 5, Lowokwaru, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145'),
(134, 'Dome Universitas Muhamadyah Malang', 'Jl. Karyawiguna No.90, Babatan, Tegalgondo, Karangploso, Malang, Jawa Timur 65152'),
(135, 'Gedung Yayasan Pendidikan Anak Cacat', 'Jl. Raden Tumenggung Suryo No. 39 Kelurahan Bunulrejo, Kecamatan Blimbing, Purwantoro, Blimbing, Kota Malang, Jawa Timur 65122'),
(136, 'Gedung Serbaguna Vocational Education Development Center', 'Jl. Teluk Mandar Tromol Pos No.5, Arjosari, Blimbing, Kota Malang, Jawa Timur 65126'),
(137, 'Universitas Macung', 'Villa Puncak Tidar Blok N No. 1, Karangwidoro, Dau, Doro, Karangwidoro, Dau, Malang, Jawa Timur 65151'),
(138, 'Balaikota Malang', 'Jl. Tugu No.1, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65119'),
(139, 'Taman Rekreasi Kota Malang', 'Jl. Mojopahit No.1, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65111'),
(140, 'Samantha Krida Universitas Brawijaya', 'Universitas Brawijaya, Jl. Veteran, Sumbersari, Kec. Lowokwaru, Kota Malang'),
(141, 'Sasana Budaya Universitas Negeri Malang', 'Jl. Semarang No.5, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145'),
(142, 'Aula Pertamina Politeknik Negeri Malang', 'Jl. Soekarno Hatta no. 9 Malang'),
(143, 'Aula Sekolah Tinggi Bahasa Asing Malang', 'Jalan Terusan Danau Sentani 97 Malang'),
(144, 'Auditorium Poltekkes Kemenkes Malang', 'Jl. Besar Ijen No.77C, Oro-oro Dowo, Klojen, Kota Malang, Jawa Timur 65119'),
(145, 'Aula Balekambang Sekolah Tinggi Ilmu Ekonomi Malangkucecwara', 'Jl. Terusan Candi Kalasan Malang'),
(146, 'Widya Graha Universitas Widyagama', 'Jl. Borobudur No. 35 Malang'),
(147, 'Aula Universitas Gajayana', 'Jl. Mertojoyo Blk. L, Merjosari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144'),
(148, 'Aula Institut Teknologi Nasional', 'Jalan Bendungan Sigura-Gura No.2, Sumbersari, Kecamatan Lowokwaru, Kota Malang, Jawa Timur 65145'),
(149, 'Padi Resto Galeri', 'Jl. Pahlawan Trip no. 19, Malang 65112, Indonesia'),
(150, 'Bakorwil III Malang', 'Jl. Simpang Ijen No.2, Oro-oro Dowo, Klojen, Kota Malang, Jawa Timur 65119'),
(151, 'Kartini Imperial Building', 'Jl. Tangkuban Perahu No.1B, Kauman, Klojen, Kota Malang, Jawa Timur 65119'),
(152, 'Malang City Point', 'Jl. Terusan Dieng No.32, Pisang Candi, Sukun, Kota Malang, Jawa Timur 65115'),
(153, 'Dieng Club House', 'Jl. Istana Dieng Utara I No.22, Bandulan, Sukun, Kota Malang, Jawa Timur 65146'),
(154, 'Mall Dinoyo', 'Jl. M.T. Haryono No.195 - 197, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144'),
(155, 'Araya Golf', 'Jl. Araya Boulevard No. 7, The Araya Malang, Tirtomoyo, Pakis, Malang, Jawa Timur 65154'),
(156, 'Gor Ken Arok', 'Jl. Mayjen Sungkono, Buring, Kedungkandang, Kota Malang, Jawa Timur 65135'),
(157, 'Sekolah Model Terpadu', 'Jl. Raya Tlogowaru No.3, Tlogowaru, Kedungkandang, Kota Malang, Jawa Timur 65132'),
(158, 'Balai Merdeka Universitas Merdeka', 'Jalan Terusan Dieng No. 62-64 Klojen, Pisang Candi, Sukun, Kota Malang, Jawa Timur 65146'),
(159, 'Bioskop Sarinah', 'Sarinah Plaza Lt. 03, Jalan Basuki Rachmat No. 2A, Klojen, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65119'),
(160, 'Bioskop Malang Plaza', 'Plaza Malang, Jl. Agus Salim No.28, Sukoharjo, Klojen, Kota Malang, Jawa Timur 65118'),
(161, 'Bioskop Dieng', 'Dieng Plaza, Jl. Raya Langsep No.2, Pisang Candi, Sukun, Kota Malang, Jawa Timur 65115'),
(162, 'Bioskop Dinoyo', 'Jl. M.T. Haryono No.193, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144'),
(163, 'Lamorfu - Hotel Tugu', 'Jl. Tugu No.3, Kauman, Klojen, Kota Malang, Jawa Timur 65119'),
(164, 'Skyroom – OJ Best Western', 'Jl. Dr. Cipto No.11, Rampal Celaket, Klojen, Kota Malang, Jawa Timur 65111'),
(165, 'Toengkoe – Balava Hotel', 'Jl. Kolonel Sugiono No.6, Ciptomulyo, Kedungkandang, Kota Malang, Jawa Timur 65134'),
(166, 'Rumah Makan Inggil', 'Jl. Gajahmada No.4, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65119'),
(167, 'Taman Merjosari', 'Jl. Mertojoyo Selatan Blk. B No.20, Merjosari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144'),
(168, 'Taman Trunojoyo', 'Jl. Trunojoyo, Klojen, Kota Malang, Jawa Timur 65111'),
(169, 'Taman Merbabu', 'Jl. Merbabu, Oro-oro Dowo, Klojen, Kota Malang, Jawa Timur 65119'),
(170, 'Taman Kendedes', 'Jl. Jend. Ahmad Yani Utara No.4, Balearjosari, Blimbing, Kota Malang, Jawa Timur 65126'),
(171, 'Taman Slamet', 'Jl. Taman Slamet No.8, Gading Kasri, Klojen, Kota Malang, Jawa Timur 65115'),
(172, 'Taman Kota Kediri', 'Jl Kediri Kelurahan Gadingkasri Kecamatan Klojen, Kota  Malang, Jawa Timur'),
(173, 'Taman Hutan Kota', 'Jl. Malabar, Oro-oro Dowo, Klojen, Kota Malang, Jawa Timur 65119'),
(174, 'Taman TPA Supit Urang', ' Jl. Terusan Rawi Sari Atas No. 1 Mulyorejo Sukun, Pandan Selatan, Pandanlandung, Wagir, Kota Malang, Jawa Timur 65158'),
(175, 'Taman Rektor Universitas Brawijaya', 'Jalan Veteran, Ketawanggede, Lowokwaru, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145'),
(176, 'Taman Kunang Kunang', 'Jalan Jakarta, Oro-oro Dowo, Klojen, Penanggungan, Klojen, Kota Malang, Jawa Timur 65113'),
(177, 'Semeru Art Galery', 'Jl. Semeru No.14, Oro-oro Dowo, Klojen, Kota Malang, Jawa Timur 65119'),
(178, 'Area Veledrome', 'Jl. Danau Jonge No.1, Sawojajar, Kedungkandang, Kota Malang, Jawa Timur 65139'),
(179, 'Permata Jingga', 'Jl. Permata Jingga, Tunggulwulung, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142'),
(180, 'Istana Dieng Club House', 'Jl. Istana Dieng Utara I No.22, Bandulan, Sukun, Kota Malang, Jawa Timur 65146'),
(181, 'Aula Dodik Belanegara Rindam V/Brawijaya', 'Jl. P. Sudirman, No. 1, Kesatrian, Blimbing, Kota Malang, Jawa Timur'),
(182, 'Aula Sekolah Menengah Kejuruan Telkom Sandhy Putra', 'Jl. Danau Ranau, Sawojajar, Kedungkandang, Kota Malang, Jawa Timur 65139'),
(183, 'Aula Sekolah Menengah Atas Negeri 9 Malang', 'Jl. Puncak Borobudur No.1, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142'),
(184, 'Auditorium Madrasah Aliyah Negeri 3 Malang', 'Jl. Bandung No.7, Penanggungan, Klojen, Kota Malang, Jawa Timur 65113'),
(185, 'Aula Edhotel Sekolah Menengah Kejuruan Negeri 3 Malang', 'Jl. Surabaya No.1, Gading Kasri, Klojen, Kota Malang, Jawa Timur 65115'),
(186, 'Auditorium KH Masjkur Masjid Sabilillah', 'Masjid Sabilillah Malang, Jl. A. Yani No.15, Blimbing, Kota Malang, Jawa Timur 65126'),
(187, 'Gedung Serbaguna Graha Tirta', 'Jl. Bend. Sengguruh No.32, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145'),
(188, 'Rumah Makan Kaliurang', 'Jl. Kaliurang No.44, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65111'),
(189, 'Oen Corner', 'Jl. Jenderal Basuki Rahmat No.5, Kauman, Klojen, Kota Malang, Jawa Timur 65119');

-- --------------------------------------------------------

--
-- Table structure for table `disnaker_jenis`
--

CREATE TABLE `disnaker_jenis` (
  `id_jenis` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disnaker_jenis`
--

INSERT INTO `disnaker_jenis` (`id_jenis`, `id_kategori`, `nama_jenis`) VALUES
(1, 1, 'Menurut Jenis Kegiatan'),
(2, 1, 'Menurut Lapangan Pekerjaan Utama'),
(3, 1, 'Menurut Status Pekerjaan Utama'),
(4, 2, 'Bekerja'),
(5, 2, 'Mencari Pekerjaan/Pengangguran Terbuka'),
(6, 6, 'Pengangguran Terselubung'),
(7, 6, 'Setengah Menganggur'),
(8, 6, 'Pengangguran Terbuka'),
(9, 7, 'Tenaga Kerja Wanita'),
(10, 7, 'Tenaga Kerja Pria'),
(11, 7, 'Tenaga Kerja yang Bekerja pada PMDN'),
(12, 8, 'Tenaga Kerja Wanita'),
(13, 8, 'Tenaga Kerja Pria'),
(14, 9, 'Kasus PHK'),
(15, 9, 'Orang Terkena PHK'),
(16, 3, 'Kesempatan Bekerja'),
(17, 4, 'Jumlah Pencari Kerja yang Mendaftarkan'),
(18, 5, 'Jumlah Pencari Kerja yang Ditempatkan'),
(19, 11, 'Pertanian, Kehutanan, Perkebunan dan Perikanan'),
(20, 11, 'Pertambangan dan Penggalian'),
(21, 11, 'Industri Pengolahan'),
(22, 11, 'Listrik, Gas dan Air'),
(23, 11, 'Bangunan'),
(24, 11, 'Perdagangan Besar, Eceran, Rumah Makan dan Hotel'),
(25, 11, 'Angkutan, Penggudangan dan Komunikasi'),
(26, 11, 'Keuangan, Asuransi, Usaha dan Sewa Bangunan, Tanah dan Jasa Perusahaan'),
(27, 11, 'Jasa Kemasyarakatan'),
(28, 12, 'Jumlah Balai Latihan Kerja di Kota Malang'),
(29, 12, 'Jumlah Perizinan Ketenagakerjaan'),
(30, 12, 'Pusat Informasi Ketenagakerjaan');

-- --------------------------------------------------------

--
-- Table structure for table `disnaker_kategori`
--

CREATE TABLE `disnaker_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disnaker_kategori`
--

INSERT INTO `disnaker_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Penduduk Usia 15 Tahun ke Atas '),
(2, 'Angkatan Kerja'),
(3, 'Kesempatan Bekerja'),
(4, 'Jumlah Pencari Kerja yang Mendaftarkan'),
(5, 'Jumlah Pencari Kerja yang Ditempatkan'),
(6, 'Jumlah Pengangguran'),
(7, 'Tenaga Kerja Dalam Negeri'),
(8, 'TKI di Luar Negeri'),
(9, 'PHK'),
(10, 'Upah Minimum Kota (UMK)'),
(11, 'Jumlah Penduduk Bekerja Menurut Lapangan Usaha'),
(12, 'Palayanan Ketenagakerjaan');

-- --------------------------------------------------------

--
-- Table structure for table `disnaker_main`
--

CREATE TABLE `disnaker_main` (
  `id_main` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disnaker_main`
--

INSERT INTO `disnaker_main` (`id_main`, `id_jenis`, `id_sub_jenis`, `th`, `jml`) VALUES
(1, 1, 0, 2015, '671.937'),
(2, 1, 0, 2016, '671.937'),
(3, 1, 0, 2017, '684.015'),
(4, 2, 0, 2015, '377.329'),
(5, 2, 0, 2016, '377.239'),
(6, 2, 0, 2017, '411.042'),
(7, 4, 1, 2015, '157.538'),
(8, 4, 1, 2016, '157.538'),
(9, 4, 1, 2017, '170.138'),
(10, 4, 2, 2015, '219.971'),
(11, 4, 2, 2016, '219.971'),
(12, 4, 2, 2017, '240.904'),
(13, 5, 3, 2015, '10.163'),
(14, 5, 3, 2016, '10.163'),
(15, 5, 3, 2017, '10.884'),
(16, 5, 4, 2015, '19.443'),
(17, 5, 4, 2016, '19.443'),
(18, 5, 4, 2017, '21.109'),
(19, 6, 0, 2015, '0'),
(20, 6, 0, 2016, '0'),
(21, 6, 0, 2017, '0'),
(22, 7, 0, 2015, '15.086'),
(23, 7, 0, 2016, '15.086'),
(24, 7, 0, 2017, '21.175'),
(25, 8, 0, 2015, '29.606'),
(26, 8, 0, 2016, '29.606'),
(27, 8, 0, 2017, '31.993'),
(28, 9, 0, 2015, '28.983'),
(29, 9, 0, 2016, '29.42'),
(30, 9, 0, 2017, '29.773'),
(31, 10, 0, 2015, '28.825'),
(32, 10, 0, 2016, '29.761'),
(33, 10, 0, 2017, '30.368'),
(34, 11, 0, 2015, '1.352'),
(35, 11, 0, 2016, '1.352'),
(36, 11, 0, 2017, '0'),
(37, 12, 0, 2015, '45'),
(38, 12, 0, 2016, '96'),
(39, 12, 0, 2017, '133'),
(40, 13, 0, 2015, '2'),
(41, 13, 0, 2016, '3'),
(42, 13, 0, 2017, '19'),
(43, 14, 0, 2015, '23'),
(44, 14, 0, 2016, '28'),
(45, 14, 0, 2017, '19'),
(46, 15, 0, 2015, '52'),
(47, 15, 0, 2016, '47'),
(48, 15, 0, 2017, '29'),
(49, 16, 0, 2015, '377.239'),
(50, 16, 0, 2016, '377.239'),
(51, 16, 0, 2017, '411.042'),
(52, 17, 0, 2015, '2.875'),
(53, 17, 0, 2016, '1.445'),
(54, 17, 0, 2017, '1.722'),
(55, 18, 0, 2015, '1.192'),
(56, 18, 0, 2016, '971'),
(57, 18, 0, 2017, '1.148'),
(58, 19, 0, 2015, '7.99'),
(59, 19, 0, 2016, '7.99'),
(60, 19, 0, 2017, '8.519'),
(61, 20, 0, 2015, '0'),
(62, 20, 0, 2016, '0'),
(63, 20, 0, 2017, '0'),
(64, 21, 0, 2015, '53.922'),
(65, 21, 0, 2016, '53.922'),
(66, 21, 0, 2017, '70.091'),
(67, 22, 0, 2015, '1.082'),
(68, 22, 0, 2016, '1.082'),
(69, 22, 0, 2017, '2.141'),
(70, 23, 0, 2015, '27.375'),
(71, 23, 0, 2016, '27.375'),
(72, 23, 0, 2017, '28.377'),
(73, 24, 0, 2015, '129.256'),
(74, 24, 0, 2016, '129.256'),
(75, 24, 0, 2017, '129.304'),
(76, 25, 0, 2015, '21.744'),
(77, 25, 0, 2016, '21.744'),
(78, 25, 0, 2017, '24.424'),
(79, 26, 0, 2015, '23.638'),
(80, 26, 0, 2016, '23.638'),
(81, 26, 0, 2017, '148.186'),
(82, 27, 0, 2015, '112.322'),
(83, 27, 0, 2016, '112.322'),
(84, 27, 0, 2017, '0'),
(85, 28, 0, 2015, '0'),
(86, 28, 0, 2016, '0'),
(87, 28, 0, 2017, '0'),
(88, 29, 0, 2015, '4'),
(89, 29, 0, 2016, '4'),
(90, 29, 0, 2017, '4'),
(91, 30, 0, 2015, '4'),
(92, 30, 0, 2016, '5'),
(93, 30, 0, 2017, '0');

-- --------------------------------------------------------

--
-- Table structure for table `disnaker_sub_jenis`
--

CREATE TABLE `disnaker_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disnaker_sub_jenis`
--

INSERT INTO `disnaker_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`) VALUES
(1, 4, 'Perempuan Bekerja'),
(2, 4, 'Laki Laki Bekerja'),
(3, 5, 'Perempuan'),
(4, 5, 'Laki Laki');

-- --------------------------------------------------------

--
-- Table structure for table `disperkim_all`
--

CREATE TABLE `disperkim_all` (
  `id_disperkim` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `satuan` varchar(32) NOT NULL,
  `jml` varchar(32) NOT NULL,
  `th` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disperkim_all`
--

INSERT INTO `disperkim_all` (`id_disperkim`, `id_jenis`, `id_sub_jenis`, `satuan`, `jml`, `th`) VALUES
(1, 1, 0, 'Unit', '0', 2015),
(2, 1, 0, 'Unit', '0', 2016),
(3, 1, 0, 'Unit', '0', 2017),
(4, 2, 1, 'Unit', '156411', 2015),
(5, 2, 1, 'Unit', '156411', 2016),
(6, 2, 1, 'Unit', '156411', 2017),
(7, 2, 2, 'Unit', '15444', 2015),
(8, 2, 2, 'Unit', '15444', 2016),
(9, 2, 2, 'Unit', '15444', 2017),
(10, 3, 0, 'Unit', '0', 2015),
(11, 3, 0, 'Unit', '0', 2016),
(12, 3, 0, 'Unit', '0', 2017),
(13, 4, 0, 'Unit', '0', 2015),
(14, 4, 0, 'Unit', '0', 2016),
(15, 4, 0, 'Unit', '0', 2017),
(16, 5, 0, 'Unit', '0', 2015),
(17, 5, 0, 'Unit', '0', 2016),
(18, 5, 0, 'Unit', '0', 2017),
(19, 6, 0, 'Unit', '0', 2015),
(20, 6, 0, 'Unit', '0', 2016),
(21, 6, 0, 'Unit', '0', 2017),
(22, 7, 0, 'Unit', '1', 2015),
(23, 7, 0, 'Unit', '1', 2016),
(24, 7, 0, 'Unit', '2', 2017),
(25, 8, 0, 'Unit', '0', 2015),
(26, 8, 0, 'Unit', '0', 2016),
(27, 8, 0, 'Unit', '0', 2017),
(28, 9, 0, 'Unit', '0', 2015),
(29, 9, 0, 'Unit', '0', 2016),
(30, 9, 0, 'Unit', '0', 2017),
(31, 10, 3, 'Unit', '0', 2015),
(32, 10, 3, 'Unit', '0', 2016),
(33, 10, 3, 'Unit', '0', 2017),
(34, 10, 4, 'Unit', '0', 2015),
(35, 10, 4, 'Unit', '0', 2016),
(36, 10, 4, 'Unit', '0', 2017),
(37, 11, 5, 'Unit', '0', 2015),
(38, 11, 5, 'Unit', '0', 2016),
(39, 11, 5, 'Unit', '0', 2017),
(40, 11, 6, 'Unit', '0', 2015),
(41, 11, 6, 'Unit', '0', 2016),
(42, 11, 6, 'Unit', '0', 2017),
(43, 11, 7, 'Unit', '0', 2015),
(44, 11, 7, 'Unit', '0', 2016),
(45, 11, 7, 'Unit', '0', 2017),
(46, 12, 8, 'm2', '0', 2015),
(47, 12, 8, 'm2', '0', 2016),
(48, 12, 8, 'm2', '0', 2017),
(49, 12, 9, 'm2', '0', 2015),
(50, 12, 9, 'm2', '0', 2016),
(51, 12, 9, 'm2', '0', 2017),
(52, 13, 10, 'm2', '0', 2015),
(53, 13, 10, 'm2', '0', 2016),
(54, 13, 10, 'm2', '0', 2017),
(55, 13, 11, 'm2', '803', 2015),
(56, 13, 11, 'm2', '803', 2016),
(57, 13, 11, 'm2', '803', 2017),
(58, 14, 12, 'm2', '0', 2015),
(59, 14, 12, 'm2', '0', 2016),
(60, 14, 12, 'm2', '0', 2017),
(61, 14, 13, 'm2', '0', 2015),
(62, 14, 13, 'm2', '0', 2016),
(63, 14, 13, 'm2', '0', 2017),
(64, 15, 14, 'm2', '0', 2015),
(65, 15, 14, 'm2', '0', 2016),
(66, 15, 14, 'm2', '0', 2017),
(67, 15, 15, 'm2', '0', 2015),
(68, 15, 15, 'm2', '0', 2016),
(69, 15, 15, 'm2', '0', 2017),
(70, 16, 16, 'm2', '0', 2015),
(71, 16, 16, 'm2', '0', 2016),
(72, 16, 16, 'm2', '0', 2017),
(73, 17, 17, 'm2', '0', 2015),
(74, 17, 17, 'm2', '0', 2016),
(75, 17, 17, 'm2', '0', 2017),
(76, 18, 0, 'Unit', '0', 2015),
(77, 18, 0, 'Unit', '0', 2016),
(78, 18, 0, 'Unit', '0', 2017),
(79, 19, 0, 'Unit', '0', 2015),
(80, 19, 0, 'Unit', '0', 2016),
(81, 19, 0, 'Unit', '0', 2017),
(82, 20, 19, 'Ha', '557723', 2015),
(83, 20, 19, 'Ha', '516958', 2016),
(84, 20, 19, 'Ha', '42744', 2017),
(85, 20, 20, 'Jiwa', '0', 2015),
(86, 20, 20, 'Jiwa', '0', 2016),
(87, 20, 20, 'Jiwa', '0', 2017),
(88, 20, 21, 'KK', '0', 2015),
(89, 20, 21, 'KK', '0', 2016),
(90, 20, 21, 'KK', '0', 2017),
(91, 21, 22, 'KK', '0', 2015),
(92, 21, 22, 'KK', '0', 2016),
(93, 21, 22, 'KK', '0', 2017),
(94, 22, 23, 'Ha', '0', 2015),
(95, 22, 23, 'Ha', '0', 2016),
(96, 22, 23, 'Ha', '0', 2017),
(97, 22, 24, 'Jiwa', '0', 2015),
(98, 22, 24, 'Jiwa', '0', 2016),
(99, 22, 24, 'Jiwa', '0', 2017),
(100, 22, 25, 'KK', '0', 2015),
(101, 22, 25, 'KK', '0', 2016),
(102, 22, 25, 'KK', '0', 2017),
(103, 23, 26, 'Ha', '0', 2015),
(104, 23, 26, 'Ha', '0', 2016),
(105, 23, 26, 'Ha', '0', 2017),
(106, 23, 27, 'Jiwa', '0', 2015),
(107, 23, 27, 'Jiwa', '0', 2016),
(108, 23, 27, 'Jiwa', '0', 2017),
(109, 23, 28, 'KK', '0', 2015),
(110, 23, 28, 'KK', '0', 2016),
(111, 23, 28, 'KK', '0', 2017),
(112, 24, 29, 'Ha', '0', 2015),
(113, 24, 29, 'Ha', '0', 2016),
(114, 24, 29, 'Ha', '0', 2017),
(115, 24, 30, 'Jiwa', '0', 2015),
(116, 24, 30, 'Jiwa', '0', 2016),
(117, 24, 30, 'Jiwa', '0', 2017),
(118, 24, 31, 'KK', '0', 2015),
(119, 24, 31, 'KK', '0', 2016),
(120, 24, 31, 'KK', '0', 2017),
(121, 25, 0, 'Ha', '0', 2015),
(122, 25, 0, 'Ha', '0', 2016),
(123, 25, 0, 'Ha', '0', 2017),
(124, 26, 0, 'Jiwa', '0', 2015),
(125, 26, 0, 'Jiwa', '0', 2016),
(126, 26, 0, 'Jiwa', '0', 2017),
(127, 27, 0, 'KK', '0', 2015),
(128, 27, 0, 'KK', '0', 2016),
(129, 27, 0, 'KK', '0', 2017),
(130, 28, 0, 'Ha', '0', 2015),
(131, 28, 0, 'Ha', '0', 2016),
(132, 28, 0, 'Ha', '0', 2017),
(133, 29, 0, 'Jiwa', '0', 2015),
(134, 29, 0, 'Jiwa', '0', 2016),
(135, 29, 0, 'Jiwa', '0', 2017),
(136, 30, 0, 'KK', '0', 2015),
(137, 30, 0, 'KK', '0', 2016),
(138, 30, 0, 'KK', '0', 2017),
(139, 31, 0, 'Unit', '0', 2015),
(140, 31, 0, 'Unit', '0', 2016),
(141, 31, 0, 'Unit', '0', 2017),
(142, 32, 0, 'Unit', '0', 2015),
(143, 32, 0, 'Unit', '0', 2016),
(144, 32, 0, 'Unit', '0', 2017),
(145, 33, 0, 'Unit', '0', 2015),
(146, 33, 0, 'Unit', '0', 2016),
(147, 33, 0, 'Unit', '0', 2017),
(148, 34, 0, 'Unit', '0', 2015),
(149, 34, 0, 'Unit', '0', 2016),
(150, 34, 0, 'Unit', '0', 2017),
(151, 35, 0, 'Unit', '8', 2015),
(152, 35, 0, 'Unit', '2', 2016),
(153, 35, 0, 'Unit', '1', 2017),
(154, 36, 0, 'Unit', '0', 2015),
(155, 36, 0, 'Unit', '0', 2016),
(156, 36, 0, 'Unit', '0', 2017),
(157, 37, 0, 'Unit', '0', 2015),
(158, 37, 0, 'Unit', '0', 2016),
(159, 37, 0, 'Unit', '0', 2017),
(160, 38, 0, 'Unit', '0', 2015),
(161, 38, 0, 'Unit', '0', 2016),
(162, 38, 0, 'Unit', '0', 2017),
(163, 39, 0, 'Unit', '0', 2015),
(164, 39, 0, 'Unit', '0', 2016),
(165, 39, 0, 'Unit', '0', 2017),
(166, 40, 0, 'Unit', '9', 2015),
(167, 40, 0, 'Unit', '9', 2016),
(168, 40, 0, 'Unit', '9', 2017),
(169, 41, 0, 'Unit', '0', 2015),
(170, 41, 0, 'Unit', '0', 2016),
(171, 41, 0, 'Unit', '0', 2017),
(172, 42, 0, 'Unit', '0', 2015),
(173, 42, 0, 'Unit', '0', 2016),
(174, 42, 0, 'Unit', '0', 2017),
(175, 43, 0, 'Unit', '0', 2015),
(176, 43, 0, 'Unit', '0', 2016),
(177, 43, 0, 'Unit', '0', 2017);

-- --------------------------------------------------------

--
-- Table structure for table `disperkim_all_jenis`
--

CREATE TABLE `disperkim_all_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL,
  `id_kategori` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disperkim_all_jenis`
--

INSERT INTO `disperkim_all_jenis` (`id_jenis`, `nama_jenis`, `id_kategori`) VALUES
(1, 'Perumnas', 1),
(2, 'Status Kepemilikan Rumah', 1),
(3, 'Penyediaan Perumahan', 1),
(4, 'KPR/BTN', 1),
(5, 'Real Estate', 1),
(6, 'Kekurangan Rumah', 1),
(7, 'Rumah Susun', 1),
(8, 'Perorangan', 1),
(9, 'Kebutuhan Rumah', 1),
(10, 'Penyediaan Jaringan /  Instalasi', 1),
(11, 'Jumlah Rumah Berdasarkan Kondisi Fisik Bangunan', 1),
(12, 'Layak Huni', 2),
(13, 'Tidak Layak Huni', 2),
(14, 'Layak Huni', 3),
(15, 'Tidak Layak Huni', 3),
(16, 'Layak Huni', 4),
(17, 'Tidak Layak Huni', 4),
(18, 'Ber IMB', 5),
(19, 'NON IMB', 5),
(20, 'Tingkat Kekumuhan Pemukiman', 6),
(21, 'Pemukiman Lahan Kritis', 6),
(22, 'Pemukiman Ilegal di atas Tanah Milik Negara', 7),
(23, 'Pemukiman Ilegal di atas Kawasan Hijau', 7),
(24, 'Pemukiman Ilegak di atas Tanah Perorangan', 7),
(25, 'Luas Areal', 8),
(26, 'Jumlah Penduduk', 8),
(27, 'Jumlah Keluarga', 8),
(28, 'Luas Areal', 9),
(29, 'Jumlah Penduduk', 9),
(30, 'Jumlah Keluarga', 9),
(31, 'Balai Pertemuan', 10),
(32, 'Gedung Kesenian', 10),
(33, 'Gelanggang Olahraga', 10),
(34, 'Stadion', 10),
(35, 'Tempat Rekreasi', 10),
(36, 'Memiliki Fasilitas Air Bersih', 12),
(37, 'Memiliki Pembuangan Tinja', 12),
(38, 'Memiliki Pembuangan Air Limbah', 12),
(39, 'Memiliki Pembuangan Sampah', 12),
(40, 'Pemakaman Umum', 13),
(41, 'Pemakaman Bukan Umum', 13),
(42, 'Pemakaman Khusus', 13),
(43, 'Taman Makam Pahlawan', 13);

-- --------------------------------------------------------

--
-- Table structure for table `disperkim_all_kategori`
--

CREATE TABLE `disperkim_all_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disperkim_all_kategori`
--

INSERT INTO `disperkim_all_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Perumahan'),
(2, 'Jumlah Rumah'),
(3, 'Luas Rumah per Kapita'),
(4, 'Luas Areal Pemukiman'),
(5, 'Jumlah Bangunan'),
(6, 'Tingkat Kekumuhan dan Keterisolasian Serta Lahan Kritis'),
(7, 'Pemukiman Ilegal'),
(8, 'Pemukiman di Bantaran Sungai'),
(9, 'Pemukiman di Bawah SUTET'),
(10, 'Jumlah Ruang Publik'),
(11, 'Jumlah Ruang Publik yang Berubah Fungsi'),
(12, 'Rumah Tinggal Berakses Sanitasi'),
(13, 'Daya Tampung Tempat Pemakaman');

-- --------------------------------------------------------

--
-- Table structure for table `disperkim_all_sub_jenis`
--

CREATE TABLE `disperkim_all_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disperkim_all_sub_jenis`
--

INSERT INTO `disperkim_all_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`) VALUES
(1, 2, 'Rumah Milik Sendiri'),
(2, 2, 'Rumah Sewa'),
(3, 10, 'Jumlah Jaringan / Instalasi PLN'),
(4, 10, 'Jumlah Jaringan / Instalasi PDAM'),
(5, 11, 'Jumlah Rumah Bangunan Permanen'),
(6, 11, 'Jumlah Rumah Semi Permanen'),
(7, 11, 'Jumlah Rumah Non Permanen'),
(8, 12, 'Pedesaan '),
(9, 12, 'Perkotaan'),
(10, 13, 'Pedesaan '),
(11, 13, 'Perkotaan'),
(12, 14, 'Pedesaan '),
(13, 14, 'Perkotaan'),
(14, 15, 'Pedesaan '),
(15, 15, 'Perkotaan'),
(16, 16, 'Pedesaan '),
(17, 16, 'Perkotaan'),
(18, 17, 'Perkotaan'),
(19, 20, 'Luas Areal Pemukiman Kumuh'),
(20, 20, 'Jumlah Penduduk yang Tinggal di Pemukiman Kumuh'),
(21, 20, 'Jumlah Keluarga yang Tinggal di Pemukiman Kumuh'),
(22, 21, 'Jumlah Keluarga yang Tinggal di Lahan Kritis'),
(23, 22, 'Luas Areal'),
(24, 22, 'Jumlah Penduduk'),
(25, 22, 'Jumlah Keluarga'),
(26, 23, 'Luas Areal'),
(27, 23, 'Jumlah Penduduk'),
(28, 23, 'Jumlah Keluarga'),
(29, 24, 'Luas Areal'),
(30, 24, 'Jumlah Penduduk'),
(31, 24, 'Jumlah Keluarga');

-- --------------------------------------------------------

--
-- Table structure for table `disperkim_csr`
--

CREATE TABLE `disperkim_csr` (
  `id_csr` int(11) NOT NULL,
  `pemberi_csr` text NOT NULL,
  `tgl_nphd` date NOT NULL,
  `no_nphd` text NOT NULL,
  `tgl_bash` date NOT NULL,
  `no_bash` text NOT NULL,
  `obj_pembangunan` text NOT NULL,
  `rab` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disperkim_csr`
--

INSERT INTO `disperkim_csr` (`id_csr`, `pemberi_csr`, `tgl_nphd`, `no_nphd`, `tgl_bash`, `no_bash`, `obj_pembangunan`, `rab`) VALUES
(65, 'PT. Bentoel Internasional Investama TBK', '2013-12-13', '050/73/35.73.123/2013', '2014-11-17', '510.12/3037.1/35.73.303/2014', 'Pembangunan  dan Perawatan Barang Milik Daerah Berupa Taman Kota Jalan Trunojoyo Depan Stasiun Malang Kota Baru', 'Belum Terlampir'),
(66, 'PT. Beiersdorf Indonesia', '2013-12-06', '050/70/35.73.123/2013', '2014-06-14', '660/1384/35.73.303/2014', 'Tanggung Jawab Sosial Perusahaan/ CSR dari PT. Beiersdorf Indonesia Kepada Pemerintah Kota Malang Berupa Pembangunan Kontruksi Taman Merbabu', 'Belum Terlampir'),
(67, 'PT. BRI (PERSERO) Tbk', '2014-11-07', '050/98/35.73.123/2014', '2015-06-17', '', 'Pemberian Dana Tanggung Jawab Sosial dan Lingkungan dalam Rangka Revitalisasi Alun-Alun Kota Malang', 'Terlampir'),
(68, 'PT Amerta Indah Otsuka', '2015-11-02', '050/51/35.73.123/2015', '2016-07-01', '660/2281/35.73.303/2016', 'Pelaksanaan Tanggung Jawab Sosial Perusahaan/ CSR dari PT. Amerta Indah Otsuka Kepada Pemerintah Kota Malang Berupa  Penataan Hutan Kota Malabar', 'Terlampir'),
(69, 'PT. Bentoel Internasional Investama TBK', '2015-01-12', '050/7/35.73.123/2015', '2015-04-17', '660/1444/35.73/2015', 'Revitalisasi Barang Milik Daerah Berupa RTH Terletak di Jalan Jakarta Kota Malang', 'Belum Terlampir'),
(70, 'PT. Bentoel Internasional Investama TBK', '2015-01-11', '050/8/35.73.123/2015', '2015-04-16', '660/1438/35.73.303/2015', 'Revitalisasi Barang Milik Daerah Berupa RTH Terletak di Jalan Taman Slamet Kota Malang', 'Belum Terlampir'),
(71, 'PT. Alam Lestari Unggul', '2016-01-28', '660/175/35.73.303/2016-DKP KOTA MALANG', '2016-02-27', '660/610/35.73.303/2016', 'Penyediaan Alat Olahraga Outdoor di Taman Merjosari serta Playground di Alun-Alun Malang', 'Terlampir'),
(72, 'Henry Soetio', '2016-09-05', '660/2825.1/35.73.303/2016-DKP KOTA MALANG', '2016-12-05', '660/4557/35.73.303/2016', 'Penataan Kawasan Pedestrian Jalan Ijen', 'Terlampir'),
(73, 'PT. Alam Lestari Unggul', '2016-12-15', '660/4862/35.73.303/2016', '2016-12-15', '660/4863/35.73.303/2016', 'Pembangunan Sky Bike  Di Taman Merjosari Kota Malang', 'Terlampir'),
(74, 'PT. Aneka Cipta Mulia Indah', '2016-09-26', '660/3068.1/35.73.303/2016-DKP Kota Malang', '2016-12-26', '660/5144/35.73.303/2016', 'Penataan Taman Jalur Hijau JL. Raya Sawojajar di Kelurahan Sawojajar Kota Malang', 'Terlampir'),
(75, 'Bentoel Group', '2016-12-30', '660/5309/35.73.303/2016-DKP Kota Malang', '2017-03-30', '660/1144/35.73.304/2017', 'Renovasi Pedestrian Taman Dieng', 'Belum Terlampir'),
(76, 'Yayasan Perguruan Tinggi Merdeka Malang', '2017-01-27', '660/254/35.73.304/2017', '2017-07-31', '660/2512/35.73.304/2017', 'Revitalisasi Barang Milik Daerah Berupa Taman Kota Jalan Terusan Raya Dieng Malang', 'Terlampir'),
(77, 'PT. Aneka Cipta Mulia Indah', '2017-05-09', '660/1813/35.73.304/2017-DISPERKIM KOTA MALANG', '2017-07-10', '660/2259/35.73.304/2017', 'Penataan Taman Jalur Hijau JL. Raya Sawojajar di Kelurahan Sawojajar Kota Malang', 'Terlampir'),
(78, 'Telkomsel', '2017-06-14', '660/2100/35.73.304/2017- DISPERKIM KOTA MALANG', '2017-12-09', '', 'Pembangunan Loop Arena di Taman Merjosari Kota Malang', 'Terlampir'),
(79, 'Henry Soetio', '2017-08-01', '660/2530/35.73.304/2017- DISPERKIM KOTA MALANG', '2017-11-28', '660/3930/35.73.304/2017', 'Revitalisasi Taman Dempo Kota Malang', 'Terlampir'),
(80, 'Bentoel Group', '2017-11-01', '660/3381/35.73.304/2017', '2018-01-30', '660/389/35.73.304/2018', 'Revitalisasi Taman Jalan Ijen', 'Belum Terlampir');

-- --------------------------------------------------------

--
-- Table structure for table `disperkim_dsu`
--

CREATE TABLE `disperkim_dsu` (
  `id_dsu` int(11) NOT NULL,
  `nama_kawasan` varchar(64) NOT NULL,
  `lokasi` text NOT NULL,
  `pengembang` text NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disperkim_dsu`
--

INSERT INTO `disperkim_dsu` (`id_dsu`, `nama_kawasan`, `lokasi`, `pengembang`, `tgl`) VALUES
(22, 'Perumahan Tidar Permai', 'Kelurahan Pisang Candi, Kecamatan Sukun', 'PT. SaranaTidar Indah', '1991-07-31'),
(23, 'Perumahan Sawojajar (Tahap I)', 'Kelurahan Madyopuro, Sawojajar,dan Lesanpuro, Kecamatan Kedungkandang', 'Perum. Perumnas Unit Malang Tahap I', '1992-12-24'),
(24, 'Perumahan Pondok Blimbing Indah', 'Kelurahan Polowijen, Kecamatan Blimbing', 'PT Araya Bumi Megah', '1994-12-29'),
(25, 'Perumahan Tidar Permai dan Badut Permai', 'Kelurahan Pisang Candi dan Karangbesuki, Kecamatan Sukun', 'PT. Sarana Tidar Indah', '1995-11-24'),
(26, 'Perumahan Joyo Asri', 'Kelurahan Merjosari, Kecamatan Lowokwaru', '-', '0000-00-00'),
(27, 'Perumahan Pondok Blimbing Indah', 'Kelurahan Polowijen, Kecamatan Blimbing', 'PT. Araya Bumi Megah', '1996-04-15'),
(28, 'Perumahan Dirgantara', 'Kelurahan Lesanpuro, Kecamatan Kedungkandang', 'PT. Karya Makmur', '1996-11-01'),
(29, 'Perumahan Sawojajar (Tahap 2)', 'Kelurahan Madyopuro, Sawojajar,dan Lesanpuro, Kecamatan Kedungkandang', 'Perum. Perumnas Unit Malang Tahap II', '1996-12-26'),
(30, 'Perumahan Griyashanta', 'Kelurahan Tulusrejo dan Mojolangu, Kecamatan Lowokwaru', 'PT. Waskita Karya', '1997-02-24'),
(31, 'Perumahan Borobudur Agung', 'Kelurahan Mojolangu, Kecamatan Lowokwaru', 'PT. Sinar Buana Megah', '1997-04-14'),
(32, 'Perumahan Bhumi Purwantoro Agung', 'Kelurahan Purwantoro dan Bunulrejo, Kecamatan Blimbing', 'PT. Bromonindo Permai Adi Kencana', '1998-05-28'),
(33, 'Perumahan Sawojajar Tahap I', 'Kelurahan Madyopuro, Sawojajar,dan Lesanpuro, Kecamatan Kedungkandang', 'Perum Perumnas Unit Malang Tahap I', '1999-01-12'),
(34, 'Perumahan Graha Kota Asri Malang Indah', 'Kelurahan Kutobedah, Kecamatan Kedungkandang', 'PT Bumi Graha Malang Indah', '2002-03-30'),
(35, 'Perumahan Villa Gunung Buring', 'Kelurahan Cemorokandang, Kecamatan Kedungkandang ', 'PT. Bukit Barisan Permai', '2002-08-21'),
(36, 'Perumahan Puri Kartika Asri', 'Kelurahan Purwantoro, Kecamatan Blimbing', 'PT. Karya Kartika', '2002-09-14'),
(37, 'Perumahan Sukun Pondok Indah', 'Kelurahan Bandungrejosari, Kecamatan Sukun', 'PT. Gatra Kanaka Harum', '2005-04-27'),
(38, 'Perumahan Kebonsari Indah (± 8.657,50 m2)', 'Kelurahan Kebonsari, Kecamatan Sukun', 'Hermanto', '2005-12-05'),
(39, 'Perumahan Gadang Cahaya Raya, Gadang Regency , Cahaya Mutiara Re', 'Kelurahan Gadang, Kecamatan Sukun', 'PT. Kharisma Banjarharum', '2010-09-30'),
(40, 'Perumahan Gadang Mandiri', 'Kelurahan Kebonsari, Kecamatan Sukun', 'Raharjo Sri Utomo', '2011-01-26'),
(41, 'Istana Dieng', 'Kelurahan Bandulan , Kecamatan Sukun', '-', '0000-00-00'),
(42, 'Kota Araya', 'Kelurahan Pandanwangi dan Purwodadi, Kecamatan Blimbing', 'PT. Araya Bumi Megah', '2014-02-26');

-- --------------------------------------------------------

--
-- Table structure for table `disperkim_penanaman_pohon`
--

CREATE TABLE `disperkim_penanaman_pohon` (
  `id_pohon` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `jml` int(11) NOT NULL,
  `lokasi` text NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disperkim_penanaman_pohon`
--

INSERT INTO `disperkim_penanaman_pohon` (`id_pohon`, `tgl`, `id_jenis`, `jml`, `lokasi`, `keterangan`) VALUES
(1, '2018-01-04', 1, 20, 'Jl. Merpati', 'Penanaman baru'),
(2, '2018-01-12', 1, 5, 'Jl. Merpati Utara', 'Penanaman baru'),
(3, '2018-01-12', 2, 5, 'Jl. Walet', 'Penanaman baru'),
(4, '2018-01-12', 3, 5, 'Jl. S.Supriyadi', 'Penanaman baru'),
(5, '2018-01-12', 4, 5, 'Jl. S.Supriyadi', 'Penanaman baru'),
(6, '2018-01-15', 5, 10, 'Jl. Borobudur', 'Penanaman baru'),
(7, '2018-01-15', 5, 10, 'Jl. Mayjen Panjaitan', 'Penanaman baru'),
(8, '2018-01-18', 1, 10, 'Jl. Satsuit Tubun', 'Penanaman baru'),
(9, '2018-01-18', 2, 5, 'Jl. Satsuit Tubun', 'Penanaman baru'),
(10, '2018-01-18', 4, 5, 'Jl. Kol. Sugiono', 'Penanaman baru'),
(11, '2018-01-18', 5, 20, 'Jl. Borobudur', 'Penanaman baru'),
(12, '2018-01-24', 1, 20, 'Jl. Danau Kerinci', 'Penanaman baru'),
(13, '2018-01-25', 1, 20, 'Jl. Danau Kerinci', 'Penanaman baru'),
(14, '2018-01-31', 2, 20, 'Jl. Bogor Atas', 'Penanaman baru'),
(15, '2018-02-01', 2, 10, 'Jl. Bogor Atas', 'Penanaman baru'),
(16, '2018-02-01', 5, 13, 'Jl. Simpang Bogor Atas', 'Penanaman baru'),
(17, '2018-02-03', 2, 20, 'Jl. Bogor Atas', 'Penanaman baru'),
(18, '2018-02-07', 4, 20, 'Jl. Kol. Sugiono', 'Penanaman baru'),
(19, '2018-02-08', 6, 20, 'Jl. Kyai Paseh', 'Penanaman baru'),
(20, '2018-02-08', 6, 3, 'Jl. Rajasa', 'Penyulaman'),
(21, '2018-02-14', 6, 20, 'Jl. Kalimosodo', 'Penanaman baru'),
(22, '2018-02-15', 6, 20, 'Jl. Kalimosodo', 'Penanaman baru'),
(23, '2018-02-16', 6, 20, 'Jl. Mayjen Wiyono', 'Penanaman baru'),
(24, '2018-02-17', 7, 10, 'Jl. Mayjen Wiyono', 'Penanaman baru'),
(25, '2018-02-20', 6, 15, 'Jl. Mayjen Wiyono', 'Penanaman baru'),
(26, '2018-02-21', 6, 15, 'Jl. Mayjen Wiyono', 'Penanaman baru'),
(27, '2018-02-22', 6, 15, 'Jl. Tawangmangu', 'Penanaman baru'),
(28, '2018-02-23', 6, 15, 'Jl. Cengger ayam', 'Penanaman baru'),
(29, '2018-02-24', 6, 15, 'Jl. Bukirsari dan Jl. Cengger Ayam', 'Penanaman baru'),
(30, '2018-02-26', 4, 15, 'Jl. S. Supriyadi dan Jl. Janti', 'Penanaman baru'),
(31, '2018-02-28', 6, 15, 'Jl. Karya Timur', 'Penanaman baru'),
(32, '2018-03-01', 0, 20, 'Jl. Karya Timur', 'Penanaman baru'),
(33, '2018-03-03', 0, 10, 'Jl. MT. Haryono', 'Penanaman baru'),
(34, '2018-03-12', 0, 15, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(35, '0000-00-00', 0, 20, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(36, '2018-03-13', 0, 20, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(37, '2018-03-13', 0, 15, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(38, '2018-03-13', 0, 10, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(39, '2018-03-14', 0, 20, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(40, '2018-03-16', 0, 20, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(41, '2018-03-19', 0, 10, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(42, '2018-03-19', 0, 15, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(43, '2018-03-21', 0, 23, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(44, '2018-03-23', 0, 21, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(45, '2018-03-23', 0, 9, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(46, '2018-03-24', 0, 20, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(47, '2018-03-26', 0, 30, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(48, '2018-03-27', 0, 20, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(49, '2018-03-28', 0, 18, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(50, '2018-03-28', 0, 7, 'Kebun Bibit Mojolangu', 'Penanaman baru'),
(51, '2018-04-02', 0, 21, 'Jl. Sudimoro Utara', 'Penanaman baru'),
(52, '2018-04-04', 0, 20, 'Jl. Sudimoro Utara', 'Penanaman baru'),
(53, '2018-04-11', 0, 20, 'Jl. Karya Timur', 'Penanaman baru'),
(54, '2018-04-13', 0, 20, 'Jl. Karya Timur', 'Penanaman baru'),
(55, '2018-04-17', 0, 25, 'Jl. J.A Suprapto', 'Penanaman baru'),
(56, '2018-04-18', 0, 20, 'Jl. Sudimoro Utara', 'Penanaman baru'),
(57, '2018-04-20', 0, 20, 'Jl. Sudimoro Utara', 'Penanaman baru'),
(58, '2018-04-23', 0, 20, 'Jl. Sudimoro Utara', 'Penanaman baru'),
(59, '2018-04-27', 0, 20, 'Rusunawa I', 'Penanaman baru'),
(60, '2018-05-01', 0, 20, 'Rusun I', 'Penanaman baru'),
(61, '2018-05-02', 0, 20, 'Rusun I', 'Penanaman baru'),
(62, '2018-05-07', 0, 20, 'Rusun I', 'Penanaman baru'),
(63, '2018-05-08', 0, 20, 'Rusun I', 'Penanaman baru'),
(64, '2018-05-11', 0, 20, 'Jl. Merapi, Bromo Buring', 'Penanaman baru'),
(65, '2018-05-12', 0, 20, 'Jl. Mayjen Panjaitan', 'Penanaman baru'),
(66, '2018-05-17', 0, 20, 'Rusun II', 'Penanaman baru'),
(67, '2018-05-19', 0, 20, 'Rusun II', 'Penanaman baru'),
(68, '2018-05-21', 0, 20, 'Rusun II', 'Penanaman baru'),
(69, '2018-05-22', 0, 40, 'Jl. Suprapto', 'Penanaman baru'),
(70, '2018-05-23', 0, 40, 'Baski Rahmad', 'Penanaman baru'),
(71, '2018-05-25', 0, 20, 'Ki Ageng Gribig', 'Penanaman baru'),
(72, '2018-05-29', 0, 20, 'Ki Ageng Gribig', 'Penanaman baru'),
(73, '2018-06-01', 0, 20, 'Ketepeng Kencana', 'Penanaman baru'),
(74, '2018-06-05', 0, 20, 'Ketepeng Kencana', 'Penanaman baru'),
(75, '2018-06-13', 0, 20, 'Spatudea', 'Penanaman baru'),
(76, '2018-06-20', 0, 20, 'Saman', 'Penanaman baru'),
(77, '2018-06-21', 0, 40, 'Pucuk merah', 'Penanaman baru'),
(78, '2018-06-26', 0, 20, 'Spatudea', 'Penanaman baru'),
(79, '2018-06-27', 0, 20, 'Spatudea', 'Penanaman baru'),
(80, '2018-06-29', 0, 20, 'Spatudea', 'Penanaman baru'),
(81, '2018-07-02', 0, 20, 'Bintaro', 'Penanaman baru'),
(82, '0000-00-00', 0, 20, 'Bintaro', 'Penanaman baru'),
(83, '0000-00-00', 0, 20, 'Bintaro', 'Penanaman baru'),
(84, '2018-07-03', 0, 15, 'Buah-buahan', 'Penanaman baru'),
(85, '2018-07-05', 0, 1, 'Palm Putri', 'Penanaman baru'),
(86, '2018-07-09', 0, 0, 'Tanjung', 'Penanaman baru'),
(87, '0000-00-00', 0, 0, 'Tabebuya daun lebar', 'Penanaman baru'),
(88, '2018-07-14', 0, 10, 'Spatudea', 'Penanaman baru'),
(89, '2018-07-16', 0, 10, 'Kenari', 'Penanaman baru'),
(90, '2018-07-19', 0, 16, 'Mahoni', 'Penanaman baru'),
(91, '0000-00-00', 0, 4, 'Mahoni', 'Penanaman baru'),
(92, '2018-08-01', 0, 10, 'Tabebuya daun lebar', 'Penanaman baru'),
(93, '2018-08-02', 0, 10, 'Tabebuya daun lebar', 'Penanaman baru'),
(94, '2018-08-06', 0, 10, 'Spatudea', 'Penanaman baru'),
(95, '0000-00-00', 0, 10, 'Tabebuya daun lebar', 'Penanaman baru'),
(96, '2018-08-07', 0, 25, 'Tabebuya bunga kuning', 'Penanaman baru'),
(97, '2018-08-09', 0, 20, 'Tabebuya daun lebar', 'Penanaman baru');

-- --------------------------------------------------------

--
-- Table structure for table `disperkim_penanaman_pohon_jenis`
--

CREATE TABLE `disperkim_penanaman_pohon_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disperkim_penanaman_pohon_jenis`
--

INSERT INTO `disperkim_penanaman_pohon_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Glodokan Lokal'),
(2, 'Tanjung'),
(3, 'Bintaro'),
(4, 'Spatudea'),
(5, 'Tabebuya'),
(6, 'Tabebuya daun lebar'),
(7, 'Kenari'),
(8, 'Ketepeng Kencana'),
(9, 'Keben'),
(10, 'Saman'),
(11, 'Mahoni'),
(12, 'Pagoda '),
(13, 'Pucuk merah'),
(14, 'Buah-buahan'),
(15, 'Palm Putri'),
(16, 'Tabebuya bunga kuning');

-- --------------------------------------------------------

--
-- Table structure for table `disperkim_taman`
--

CREATE TABLE `disperkim_taman` (
  `id_taman` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disperkim_taman`
--

INSERT INTO `disperkim_taman` (`id_taman`, `id_jenis`, `th`, `jml`) VALUES
(1, 1, 2017, '110'),
(2, 2, 2017, '11'),
(3, 3, 2017, '7'),
(4, 4, 2017, '14'),
(5, 5, 2017, '10'),
(6, 6, 2017, '12'),
(7, 7, 2017, '32'),
(8, 8, 2017, '13'),
(9, 9, 2017, '2'),
(10, 10, 2017, '3'),
(11, 11, 2017, '6'),
(12, 12, 2017, '2'),
(13, 13, 2017, '1'),
(14, 1, 2018, '130'),
(15, 2, 2018, '9'),
(16, 3, 2018, '8'),
(17, 4, 2018, '9'),
(18, 5, 2018, '4'),
(19, 6, 2018, '3'),
(20, 7, 2018, '43'),
(21, 8, 2018, '9'),
(22, 9, 2018, '3'),
(23, 10, 2018, '5'),
(24, 11, 2018, '3'),
(25, 12, 2018, '9'),
(26, 13, 2018, '1'),
(27, 14, 2018, '3'),
(28, 15, 2018, '1');

-- --------------------------------------------------------

--
-- Table structure for table `disperkim_taman_jenis`
--

CREATE TABLE `disperkim_taman_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disperkim_taman_jenis`
--

INSERT INTO `disperkim_taman_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Alun Alun Kota Malang'),
(2, 'Alun Alun Tugu'),
(3, 'Hutan Kota Hamid Rusdi'),
(4, 'Hutan Kota Malabar'),
(5, 'Kebun Bibit Mojolangu'),
(6, 'Pedestrian Jl. Ijen'),
(7, 'Taman Singha Merjosari '),
(8, 'Taman Slamet'),
(9, 'Hutan Kota Jl. Jakarta'),
(10, 'Taman Merbabu'),
(11, 'Taman Trunojoyo'),
(12, 'Taman Cerme'),
(13, 'Taman Median Jl. Terusan Dieng'),
(14, 'Hutan Kota Velodrome'),
(15, 'Taman Kendedes');

-- --------------------------------------------------------

--
-- Table structure for table `dpmptsp_jenis`
--

CREATE TABLE `dpmptsp_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dpmptsp_jenis`
--

INSERT INTO `dpmptsp_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Pertanian'),
(2, 'Peternakan'),
(3, 'Perikanan'),
(4, 'Perkebunan /  Kehutanan'),
(5, 'Pertambangan dan Galian Gol C'),
(6, 'Perindustrian'),
(7, 'Perdagangan'),
(8, 'Perhotelan'),
(9, 'Restoran / Rumah Makan / Cafe'),
(10, 'Perumahan dan Ruko'),
(11, 'Perkantoran, Supermarket, dan Supermall'),
(12, 'Jasa Konstruksi'),
(13, 'Pergudangan'),
(14, 'Transportasi Darat / Laut'),
(15, 'Kesehatan'),
(16, 'Koperasi'),
(17, 'Jasa Hiburan / Rekreasi'),
(18, 'Lain Lain');

-- --------------------------------------------------------

--
-- Table structure for table `dpmptsp_main`
--

CREATE TABLE `dpmptsp_main` (
  `id_main` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(6) NOT NULL,
  `terbit` varchar(20) NOT NULL,
  `investasi` varchar(20) NOT NULL,
  `tk` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dpmptsp_main`
--

INSERT INTO `dpmptsp_main` (`id_main`, `id_jenis`, `th`, `terbit`, `investasi`, `tk`) VALUES
(1, 1, 201701, '1', '1000000000', '3'),
(2, 1, 201702, '1', '200000000', '3'),
(3, 1, 201703, '9', '2607000000', '19'),
(4, 1, 201704, '13', '8672650613', '44'),
(5, 1, 201705, '6', '1923000000', '61'),
(6, 1, 201706, '5', '9750000000', '13'),
(7, 1, 201707, '3', '580000000', '7'),
(8, 1, 201708, '3', '933745000', '5'),
(9, 1, 201709, '0', '0', '0'),
(10, 1, 201710, '2', '200000000', '2'),
(11, 1, 201711, '10', '9438000000', '226'),
(12, 1, 201712, '2', '30400000000', '61'),
(13, 2, 201701, '0', '0', '0'),
(14, 2, 201702, '0', '0', '0'),
(15, 2, 201703, '1', '30000000000', '39'),
(16, 2, 201704, '2', '650000000', ''),
(17, 2, 201705, '0', '0', '0'),
(18, 2, 201706, '0', '0', '0'),
(19, 2, 201707, '0', '0', '0'),
(20, 2, 201708, '0', '0', '0'),
(21, 2, 201709, '1', '500000000', '3'),
(22, 2, 201710, '0', '0', '0'),
(23, 2, 201711, '0', '0', '0'),
(24, 2, 201712, '0', '0', '0'),
(25, 3, 201701, '0', '0', '0'),
(26, 3, 201702, '0', '0', '0'),
(27, 3, 201703, '0', '0', '0'),
(28, 3, 201704, '0', '0', '0'),
(29, 3, 201705, '0', '0', '0'),
(30, 3, 201706, '0', '0', '0'),
(31, 3, 201707, '0', '0', '0'),
(32, 3, 201708, '0', '0', '0'),
(33, 3, 201709, '0', '0', '0'),
(34, 3, 201710, '0', '0', '0'),
(35, 3, 201711, '0', '0', '0'),
(36, 3, 201712, '0', '0', '0'),
(37, 4, 201701, '0', '0', '0'),
(38, 4, 201702, '0', '0', '0'),
(39, 4, 201703, '1', '200000000', '2'),
(40, 4, 201704, '0', '0', '0'),
(41, 4, 201705, '0', '0', '0'),
(42, 4, 201706, '0', '0', '0'),
(43, 4, 201707, '0', '0', '0'),
(44, 4, 201708, '0', '0', '0'),
(45, 4, 201709, '0', '0', '0'),
(46, 4, 201710, '0', '0', '0'),
(47, 4, 201711, '0', '0', '0'),
(48, 4, 201712, '0', '0', '0'),
(49, 5, 201701, '0', '0', '0'),
(50, 5, 201702, '0', '0', '0'),
(51, 5, 201703, '0', '0', '0'),
(52, 5, 201704, '1', '100000000', '2'),
(53, 5, 201705, '0', '0', '0'),
(54, 5, 201706, '0', '0', '0'),
(55, 5, 201707, '0', '0', '0'),
(56, 5, 201708, '0', '0', '0'),
(57, 5, 201709, '0', '0', '0'),
(58, 5, 201710, '0', '0', '0'),
(59, 5, 201711, '0', '0', '0'),
(60, 5, 201712, '0', '0', '0'),
(61, 6, 201701, '0', '0', '0'),
(62, 6, 201702, '0', '0', '0'),
(63, 6, 201703, '0', '0', '0'),
(64, 6, 201704, '0', '0', '0'),
(65, 6, 201705, '1', '400000000', '2'),
(66, 6, 201706, '0', '0', '0'),
(67, 6, 201707, '0', '0', '0'),
(68, 6, 201708, '0', '0', '0'),
(69, 6, 201709, '0', '0', '0'),
(70, 6, 201710, '1', '7899605588', '27'),
(71, 6, 201711, '0', '0', '0'),
(72, 6, 201712, '0', '0', '0'),
(73, 7, 201701, '', '', ''),
(74, 7, 201702, '1', '2000000000', '3'),
(75, 7, 201703, '15', '15677849853', '31'),
(76, 7, 201704, '47', '58950100000', '136'),
(77, 7, 201705, '56', '110451000000', '128'),
(78, 7, 201706, '49', '35179184232', '135'),
(79, 7, 201707, '48', '2199376850620', '309'),
(80, 7, 201708, '51', '1227812015135', '322'),
(81, 7, 201709, '24', '35787512200', '58'),
(82, 7, 201710, '78', '4055961521650', '607'),
(83, 7, 201711, '53', '209987348000', '177'),
(84, 7, 201712, '56', '53202218188', '363'),
(85, 8, 201701, '0', '0', '0'),
(86, 8, 201702, '0', '0', '0'),
(87, 8, 201703, '0', '0', '0'),
(88, 8, 201704, '0', '0', '0'),
(89, 8, 201705, '0', '0', '0'),
(90, 8, 201706, '0', '0', '0'),
(91, 8, 201707, '0', '0', '0'),
(92, 8, 201708, '0', '0', '0'),
(93, 8, 201709, '0', '0', '0'),
(94, 8, 201710, '0', '0', '0'),
(95, 8, 201711, '0', '0', '0'),
(96, 8, 201712, '0', '0', '0'),
(97, 9, 201701, '0', '0', '0'),
(98, 9, 201702, '0', '0', '0'),
(99, 9, 201703, '0', '0', '0'),
(100, 9, 201704, '0', '0', '0'),
(101, 9, 201705, '0', '0', '0'),
(102, 9, 201706, '0', '0', '0'),
(103, 9, 201707, '0', '0', '0'),
(104, 9, 201708, '0', '0', '0'),
(105, 9, 201709, '0', '0', '0'),
(106, 9, 201710, '0', '0', '0'),
(107, 9, 201711, '0', '0', '0'),
(108, 9, 201712, '0', '0', '0'),
(109, 10, 201701, '0', '0', '0'),
(110, 10, 201702, '0', '0', '0'),
(111, 10, 201703, '0', '0', '0'),
(112, 10, 201704, '0', '0', '0'),
(113, 10, 201705, '0', '0', '0'),
(114, 10, 201706, '0', '0', '0'),
(115, 10, 201707, '0', '0', '0'),
(116, 10, 201708, '0', '0', '0'),
(117, 10, 201709, '0', '0', '0'),
(118, 10, 201710, '0', '0', '0'),
(119, 10, 201711, '0', '0', '0'),
(120, 10, 201712, '0', '0', '0'),
(121, 11, 201701, '0', '0', '0'),
(122, 11, 201702, '0', '0', '0'),
(123, 11, 201703, '0', '0', '0'),
(124, 11, 201704, '0', '0', '0'),
(125, 11, 201705, '0', '0', '0'),
(126, 11, 201706, '0', '0', '0'),
(127, 11, 201707, '0', '0', '0'),
(128, 11, 201708, '0', '0', '0'),
(129, 11, 201709, '0', '0', '0'),
(130, 11, 201710, '0', '0', '0'),
(131, 11, 201711, '0', '0', '0'),
(132, 11, 201712, '0', '0', '0'),
(133, 12, 201701, '0', '0', '0'),
(134, 12, 201702, '0', '0', '0'),
(135, 12, 201703, '6', '1000000000', '10'),
(136, 12, 201704, '5', '3200000000', '11'),
(137, 12, 201705, '0', '0', '0'),
(138, 12, 201706, '1', '60000000', '2'),
(139, 12, 201707, '0', '0', '0'),
(140, 12, 201708, '0', '0', '0'),
(141, 12, 201709, '0', '0', '0'),
(142, 12, 201710, '0', '0', '0'),
(143, 12, 201711, '0', '0', '0'),
(144, 12, 201712, '0', '0', '0'),
(145, 13, 201701, '0', '0', '0'),
(146, 13, 201702, '0', '0', '0'),
(147, 13, 201703, '0', '0', '0'),
(148, 13, 201704, '2', '2100000000', '5'),
(149, 13, 201705, '0', '0', '0'),
(150, 13, 201706, '0', '0', '0'),
(151, 13, 201707, '0', '0', '0'),
(152, 13, 201708, '0', '0', '0'),
(153, 13, 201709, '0', '0', '0'),
(154, 13, 201710, '0', '0', '0'),
(155, 13, 201711, '0', '0', '0'),
(156, 13, 201712, '0', '0', '0'),
(157, 14, 201701, '0', '0', '0'),
(158, 14, 201702, '1', '1000000000', '2'),
(159, 14, 201703, '0', '12751000000', '16'),
(160, 14, 201704, '3', '950000000', '7'),
(161, 14, 201705, '2', '1900000000', '59'),
(162, 14, 201706, '3', '4550000000', '11'),
(163, 14, 201707, '0', '0', '0'),
(164, 14, 201708, '1', '80700000', '2'),
(165, 14, 201709, '1', '5000000000', '5'),
(166, 14, 201710, '0', '0', '0'),
(167, 14, 201711, '0', '0', '0'),
(168, 14, 201712, '0', '0', '0'),
(169, 15, 201701, '0', '0', '0'),
(170, 15, 201702, '0', '0', '0'),
(171, 15, 201703, '1', '500000000', '3'),
(172, 15, 201704, '0', '0', '0'),
(173, 15, 201705, '0', '0', '0'),
(174, 15, 201706, '0', '0', '0'),
(175, 15, 201707, '0', '0', '0'),
(176, 15, 201708, '0', '0', '0'),
(177, 15, 201709, '0', '0', '0'),
(178, 15, 201710, '0', '0', '0'),
(179, 15, 201711, '0', '0', '0'),
(180, 15, 201712, '0', '0', '0'),
(181, 16, 201701, '0', '0', '0'),
(182, 16, 201702, '0', '0', '0'),
(183, 16, 201703, '0', '0', '0'),
(184, 16, 201704, '0', '0', '0'),
(185, 16, 201705, '1', '305399715', '5'),
(186, 16, 201706, '0', '0', '0'),
(187, 16, 201707, '0', '0', '0'),
(188, 16, 201708, '0', '0', '0'),
(189, 16, 201709, '0', '0', '0'),
(190, 16, 201710, '0', '0', '0'),
(191, 16, 201711, '0', '0', '0'),
(192, 16, 201712, '0', '0', '0'),
(193, 17, 201701, '0', '0', '0'),
(194, 17, 201702, '0', '0', '0'),
(195, 17, 201703, '0', '0', '0'),
(196, 17, 201704, '0', '0', '0'),
(197, 17, 201705, '0', '0', '0'),
(198, 17, 201706, '0', '0', '0'),
(199, 17, 201707, '0', '0', '0'),
(200, 17, 201708, '0', '0', '0'),
(201, 17, 201709, '0', '0', '0'),
(202, 17, 201710, '0', '0', '0'),
(203, 17, 201711, '0', '0', '0'),
(204, 17, 201712, '0', '0', '0'),
(205, 18, 201701, '0', '0', '0'),
(206, 18, 201702, '0', '0', '0'),
(207, 18, 201703, '0', '0', '0'),
(208, 18, 201704, '13', '380200000', '34'),
(209, 18, 201705, '17', '55921381901', '195'),
(210, 18, 201706, '16', '26051000000', '197'),
(211, 18, 201707, '11', '2641897000', '28'),
(212, 18, 201708, '8', '1371550000', '18'),
(213, 18, 201709, '2', '2055000000', '4'),
(214, 18, 201710, '66', '3350000000', '13'),
(215, 18, 201711, '2', '400000000', '7'),
(216, 18, 201712, '3', '1675000000', '10');

-- --------------------------------------------------------

--
-- Table structure for table `id_kesehatan_sub_jenis`
--

CREATE TABLE `id_kesehatan_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iklim_hujan`
--

CREATE TABLE `iklim_hujan` (
  `id_hujan` int(6) NOT NULL,
  `id_st` int(6) NOT NULL,
  `th` int(4) NOT NULL,
  `periode` int(2) NOT NULL,
  `jml_cura` double NOT NULL,
  `jml_hari` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iklim_hujan`
--

INSERT INTO `iklim_hujan` (`id_hujan`, `id_st`, `th`, `periode`, `jml_cura`, `jml_hari`) VALUES
(1, 3, 2018, 1, 122, 12),
(3, 3, 2018, 2, 142, 12),
(4, 4, 2018, 1, 135, 13),
(5, 3, 2017, 1, 292, 16),
(6, 4, 2017, 1, 62, 10),
(7, 5, 2017, 1, 142, 10),
(8, 3, 2017, 2, 588, 25),
(9, 4, 2017, 2, 477, 30),
(10, 5, 2017, 2, 685, 24),
(11, 3, 2017, 3, 237, 24),
(12, 4, 2017, 3, 405, 18),
(13, 5, 2017, 3, 388, 18),
(14, 3, 2017, 4, 148, 12),
(15, 4, 2017, 4, 103, 10),
(16, 5, 2017, 4, 196, 11),
(17, 3, 2017, 5, 177, 18),
(18, 4, 2017, 5, 146, 12),
(19, 5, 2017, 5, 247, 12),
(20, 3, 2017, 6, 210, 13),
(21, 4, 2017, 6, 183, 10),
(22, 5, 2017, 6, 280, 12),
(23, 3, 2017, 7, 70, 10),
(24, 4, 2017, 7, 34, 6),
(25, 5, 2017, 7, 67, 11),
(26, 3, 2017, 8, 125, 7),
(27, 4, 2017, 8, 3, 3),
(28, 5, 2017, 8, 78, 5),
(29, 3, 2017, 9, 35, 6),
(30, 4, 2017, 9, 3, 3),
(31, 5, 2017, 9, 70, 7),
(32, 3, 2017, 10, 208, 14),
(33, 4, 2017, 10, 4, 4),
(34, 5, 2017, 10, 196, 14),
(35, 3, 2017, 11, 425, 20),
(36, 4, 2017, 11, 8, 8),
(37, 5, 2017, 11, 677, 25),
(38, 3, 2017, 12, 144, 14),
(39, 4, 2017, 12, 171, 9),
(40, 5, 2017, 12, 295, 18),
(41, 3, 2016, 1, 291, 15),
(42, 4, 2016, 1, 61, 8),
(43, 5, 2016, 1, 140, 9),
(44, 3, 2016, 2, 586, 23),
(45, 4, 2016, 2, 476, 29),
(46, 5, 2016, 2, 683, 22),
(47, 3, 2016, 3, 235, 21),
(48, 4, 2016, 3, 402, 16),
(49, 5, 2016, 3, 387, 16),
(50, 3, 2016, 4, 147, 11),
(51, 4, 2016, 4, 101, 8),
(52, 5, 2016, 4, 194, 9),
(53, 3, 2016, 5, 176, 16),
(54, 4, 2016, 5, 145, 11),
(55, 5, 2016, 5, 246, 11),
(56, 3, 2016, 6, 208, 12),
(57, 4, 2016, 6, 181, 8),
(58, 5, 2016, 6, 279, 10),
(59, 5, 2016, 6, 279, 10),
(60, 3, 2016, 7, 69, 8),
(61, 4, 2016, 7, 32, 4),
(62, 5, 2016, 8, 124, 6),
(63, 4, 2016, 8, 0, 0),
(64, 5, 2016, 8, 77, 3),
(65, 3, 2016, 9, 33, 5),
(66, 4, 2016, 9, 0, 0),
(67, 5, 2016, 9, 69, 5),
(68, 3, 2016, 10, 207, 13),
(69, 4, 2016, 10, 0, 0),
(70, 5, 2016, 10, 195, 12),
(71, 3, 2016, 11, 424, 19),
(72, 4, 2016, 11, 0, 0),
(73, 5, 2016, 11, 675, 22),
(74, 3, 2016, 12, 143, 12),
(75, 4, 2016, 12, 170, 8),
(76, 5, 2016, 12, 294, 17);

-- --------------------------------------------------------

--
-- Table structure for table `iklim_station`
--

CREATE TABLE `iklim_station` (
  `id_st` int(6) NOT NULL,
  `ket_st` varchar(50) NOT NULL,
  `alamat_st` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iklim_station`
--

INSERT INTO `iklim_station` (`id_st`, `ket_st`, `alamat_st`) VALUES
(3, 'Ciliwung', 'malang'),
(4, 'Kedung Kandang', 'malang'),
(5, 'Sukun', 'malang');

-- --------------------------------------------------------

--
-- Table structure for table `index_kes_daya_pend`
--

CREATE TABLE `index_kes_daya_pend` (
  `id_index` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `in_pend` varchar(10) NOT NULL,
  `in_kes` varchar(10) NOT NULL,
  `in_daya` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `index_kes_daya_pend`
--

INSERT INTO `index_kes_daya_pend` (`id_index`, `th`, `in_pend`, `in_kes`, `in_daya`) VALUES
(3, 2014, '73.43', '80.46', '83.33'),
(4, 2015, '76.05', '80.92', '83.37'),
(5, 2016, '76.52', '81.05', '83.98');

-- --------------------------------------------------------

--
-- Table structure for table `index_pem_manusia`
--

CREATE TABLE `index_pem_manusia` (
  `id_ipm` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `ipm` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `index_pem_manusia`
--

INSERT INTO `index_pem_manusia` (`id_ipm`, `th`, `ipm`) VALUES
(3, 2014, '78.96'),
(4, 2015, '80.05'),
(5, 2016, '80.46');

-- --------------------------------------------------------

--
-- Table structure for table `kepandudukan_jk`
--

CREATE TABLE `kepandudukan_jk` (
  `id_kepend_jk` int(11) NOT NULL,
  `th` int(11) NOT NULL,
  `rasio_jk` varchar(10) NOT NULL,
  `t_cowo` varchar(20) NOT NULL,
  `t_cewe` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kepandudukan_jk`
--

INSERT INTO `kepandudukan_jk` (`id_kepend_jk`, `th`, `rasio_jk`, `t_cowo`, `t_cewe`) VALUES
(3, 2014, '97.20', '416982', '428991'),
(4, 2015, '97.25', '419713', '431585'),
(5, 2016, '97.27', '422276', '434134');

-- --------------------------------------------------------

--
-- Table structure for table `kependudukan_kel_umur`
--

CREATE TABLE `kependudukan_kel_umur` (
  `id_kel_umur` int(11) NOT NULL,
  `kelompok` varchar(50) NOT NULL,
  `periode` int(11) NOT NULL,
  `jml_penduduk` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kependudukan_kel_umur`
--

INSERT INTO `kependudukan_kel_umur` (`id_kel_umur`, `kelompok`, `periode`, `jml_penduduk`) VALUES
(4, '0 - 14', 2014, '183388'),
(5, '0 - 14', 2015, '180358'),
(6, '0 - 14', 2016, '179299'),
(7, '15 - 64', 2014, '614628'),
(8, '15 - 64', 2015, '620802'),
(9, '15 - 64', 2016, '625473'),
(10, '> 65', 2014, '47957'),
(11, '> 65', 2015, '50138'),
(12, '> 65', 2016, '51638'),
(13, '0 - 14', 2019, '23423425324');

-- --------------------------------------------------------

--
-- Table structure for table `kependudukan_rasio_ketergantungan`
--

CREATE TABLE `kependudukan_rasio_ketergantungan` (
  `id_rasio` int(11) NOT NULL,
  `th_rasio` int(4) NOT NULL,
  `rasio_keter` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kependudukan_rasio_ketergantungan`
--

INSERT INTO `kependudukan_rasio_ketergantungan` (`id_rasio`, `th_rasio`, `rasio_keter`) VALUES
(3, 2014, '37.64'),
(4, 2015, '37.13'),
(5, 2016, '36.92');

-- --------------------------------------------------------

--
-- Table structure for table `kerja_angkatan`
--

CREATE TABLE `kerja_angkatan` (
  `id_angkatan` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_kerja` varchar(20) NOT NULL,
  `jml_no_kerja` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kerja_angkatan`
--

INSERT INTO `kerja_angkatan` (`id_angkatan`, `th`, `jml_kerja`, `jml_no_kerja`) VALUES
(3, 2014, '393050', '30581'),
(4, 2015, '377329', '29606'),
(5, 2016, '377329', '29606');

-- --------------------------------------------------------

--
-- Table structure for table `kerja_pengangguran`
--

CREATE TABLE `kerja_pengangguran` (
  `id_pengangguran` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kerja_pengangguran`
--

INSERT INTO `kerja_pengangguran` (`id_pengangguran`, `th`, `jml`) VALUES
(3, 2014, '6920'),
(4, 2015, '6257'),
(5, 2017, '6194');

-- --------------------------------------------------------

--
-- Table structure for table `kerja_ump`
--

CREATE TABLE `kerja_ump` (
  `id_ump` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kerja_ump`
--

INSERT INTO `kerja_ump` (`id_ump`, `th`, `jml`) VALUES
(4, 2014, '1587000'),
(5, 2015, '1882500'),
(6, 2016, '2099000');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_gizi_balita`
--

CREATE TABLE `kesehatan_gizi_balita` (
  `id_gizi_balita` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_gizi_buruk` varchar(10) NOT NULL,
  `jmh_kurang_gizi` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_gizi_balita`
--

INSERT INTO `kesehatan_gizi_balita` (`id_gizi_balita`, `th`, `jml_gizi_buruk`, `jmh_kurang_gizi`) VALUES
(3, 2014, '119', '1611'),
(4, 2015, '100', '1627'),
(5, 2016, '66', '1868');

-- --------------------------------------------------------

--
-- Table structure for table `keu_jenis`
--

CREATE TABLE `keu_jenis` (
  `id_jenis` int(11) NOT NULL,
  `ket` varchar(100) NOT NULL,
  `kategori` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keu_jenis`
--

INSERT INTO `keu_jenis` (`id_jenis`, `ket`, `kategori`) VALUES
(6, 'Bagi Hasil Pajak', '0'),
(7, 'Bagi Hasil Bukan Pajak / Sumber Daya Alam', '0'),
(8, 'Dana Alokasi Umum', '0'),
(9, 'Dana Alokasi Khusus', '0'),
(10, 'Belanja Pegawai', '1'),
(11, 'Belanja Modal', '1'),
(12, 'Belanja Lainnya', '1'),
(13, 'Pendapatan Asli Daerah', '0'),
(15, 'Lain-lain Pendapatan yang Sah', '0');

-- --------------------------------------------------------

--
-- Table structure for table `keu_jml`
--

CREATE TABLE `keu_jml` (
  `id_jml_keu` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keu_jml`
--

INSERT INTO `keu_jml` (`id_jml_keu`, `id_jenis`, `th`, `jml`) VALUES
(3, 6, 2014, '66740371.27'),
(4, 7, 2014, '50203519.87'),
(5, 8, 2014, '808447825'),
(6, 9, 2014, '31304060'),
(7, 10, 2014, '877246394.71'),
(8, 11, 2014, '318462052.42'),
(9, 12, 2014, '407291403.01'),
(10, 13, 2014, '372545396.29'),
(11, 14, 2014, '956695776.14'),
(12, 15, 2014, '435623517.23'),
(13, 16, 2014, '1764864689.66'),
(14, 17, 2014, '1602999850.14'),
(15, 6, 2015, '33850624'),
(16, 7, 2015, '53164497.78'),
(17, 8, 2015, '818758893'),
(18, 9, 2015, '20590560'),
(19, 10, 2015, '931090867.80'),
(20, 11, 2015, '337647558.97'),
(21, 12, 2015, '534682114.15'),
(22, 13, 2015, '424938755.52'),
(23, 14, 2015, '926364574.78'),
(24, 15, 2015, '477769359.41'),
(25, 16, 2015, '1829072689.71'),
(26, 17, 2015, '1803420540.92'),
(27, 6, 2016, '69368351.04'),
(28, 7, 2016, '45506060.94'),
(29, 8, 2016, '859678208'),
(30, 9, 2016, '94813827'),
(31, 10, 2016, '993592920'),
(32, 11, 2016, '193646732.35'),
(33, 12, 2016, '522678430.69'),
(34, 13, 2016, '447332655.83'),
(35, 14, 2016, '1069366446.98'),
(36, 15, 2016, '194486247.27'),
(37, 16, 2016, '1711185350.08'),
(38, 17, 2016, '1709918083.05');

-- --------------------------------------------------------

--
-- Table structure for table `kominfo_aplikasi`
--

CREATE TABLE `kominfo_aplikasi` (
  `id_aplikasi` int(11) NOT NULL,
  `fungsi` text NOT NULL,
  `aplikasi` text NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kominfo_aplikasi`
--

INSERT INTO `kominfo_aplikasi` (`id_aplikasi`, `fungsi`, `aplikasi`, `keterangan`) VALUES
(1, 'imm.ebdesk.com', 'Bagian Humas', 'imm.ebdesk.com'),
(2, 'anjab.malangkota.go.id', 'Bagian Organisasi', 'anjab.malangkota.go.id'),
(3, 'smep.malangkota.go.id', 'Bagian Pembangunan', 'smep.malangkota.go.id'),
(4, 'SI Pengadaan Barang Jasa', 'Bagian Pembangunan', 'sibaja.malangkota.go.id'),
(5, 'Manajemen Surat', 'Bagian Umum', 'manajemensurat.malangkota.go.id'),
(6, 'SI APBD', 'BPKAD', 'apbdweb.malangkota.go.id'),
(7, 'SIMBADA', 'BPKAD', 'simbada.malangkota.go.id'),
(8, 'SIPKD(e-finance)', 'BPKAD', 'efinance.malangkota.go.id (VPN)'),
(9, 'SIP GAJI (SI Pengelolaan Gaji)', 'BPKAD', 'desktop'),
(10, 'simpeg.malangkota.go.id', 'BKD', 'simpeg.malangkota.go.id:8080'),
(11, 'e-attendance.malangkota.go.id', 'BKD', 'e-attendance.malangkota.go.id'),
(12, 'E-Arsip BKD', 'BKD', 'lokal'),
(13, 'Smart Library Automation', 'Kantor Perpustakaan', 'digilib.malangkota.go.id'),
(14, 'petaniberniaga.com (Provinsi)', 'Dinas Pertanian', 'petaniberniaga.com'),
(15, 'RTTIC', 'Dishub', 'https://play.google.com/store/apps/detail s?id=com.kotamalang.rttic'),
(16, 'E-PKDRT', 'BKBPM', 'e-pkdrt.malangkota.go.id'),
(17, 'SMS Gateway (BKBPM)', 'BKBPM', 'desktop'),
(18, '(SISMIOP) SI PBB', 'Dispenda', 'desktop'),
(19, 'produkmalangan.com', 'Disperindag', 'produkmalangan.com'),
(20, 'Infopangan.malangkota.go.id', 'Disperindag', 'infopangan.malangkota.go.id'),
(21, 'registra.malangkota.go.id', 'Dispendukcapil', 'registra.malangkota.go.id (VPN)'),
(22, 'siakarsip.malangkota.go.id', 'Dispendukcapil', 'siakarsip.malangkota.go.id (VPN)'),
(23, 'SLRT(Sistem Layanan Rujukan dan Terpadu—> Pusat', 'DINSOS', 'pksa.depsos.go.id/kotamalang'),
(24, 'SIMTASKIN', 'BAPPEDA', 'simtaskinv2.malangkota.go.id'),
(25, 'SIMRENDA', 'BAPPEDA', 'simrenda.malangkota.go.id'),
(26, 'Data UKM', 'BAPPEDA', 'pelakuekonomi.malangkota.go.id'),
(27, 'MMC', 'DINKOMINFO', 'mmc.malangkota.go.id'),
(28, 'JDIH', 'Bagian Hukum', 'jdih.malangkota.go.id'),
(29, 'E-SURVEI', 'DINKOMINFO', 'esurvei.malangkota.go.id'),
(30, 'SIRaRa (SI Ruang Rapat)', 'UPT. Perkantoran Terpadu', 'sirara.malangkota.go.id'),
(31, 'SI TARIS', 'DINKOMINFO', 'sitaris.malangkota.go.id'),
(32, 'E-REPORT (LPSE)', 'UPT.LPSE', 'ereport.malangkota.go.id'),
(33, 'SPSE (LKPP)', 'UPT.LPSE', 'lpse.malangkota.go.id'),
(34, 'EKATALOG (LKPP)', 'UPT.LPSE', 'E-katalog.lkpp.go.id'),
(35, 'SIRUP (LKPP)', 'UPT.LPSE', 'sirup.lkpp.go.id'),
(36, 'SAMBAT', 'DINKOMINFO', 'sambat.malangkota.go.id'),
(37, 'E-AGENDA', 'DINKOMINFO', 'agenda.malangkota.go.id'),
(38, 'E-AGENDA Kominfo (Versi Android)', 'DINKOMINFO', 'https://play.google.com/store/apps/detail s?id=com.kominfo.webviewapp'),
(39, 'E-AGENDA Walikota (Versi Android)', 'DINKOMINFO', 'https://play.google.com/store/apps/detail s?id=com.kominfo.agendawalikota'),
(40, 'E-Agenda Kominfo (Versi Web)', 'Diskominfo ', 'board.malangkota.go.id'),
(41, 'Website Pemerintah Kota Malang', 'Dinas Komunikasi dan Informatika', 'http://www.malangkota.go.id'),
(42, 'e-Surat', 'Bagian Umum', 'http://e-surat.malangkota.go.id'),
(43, 'SIMTAP/Perijinan', 'Badan Pelayanan Perijinan Terpadu', 'http://bp2t.malangkota.go.id'),
(44, 'SIAK', 'Dinas Kependudukan dan Pencatatan Sipil', 'http://siak.malangkota.go.id'),
(45, 'SIM ANJAB dan ABK', 'Bagian Organisasi', 'http://anjab.malangkota.go.id'),
(46, 'E-ATTENDANCE dan Finger Print', 'Badan Kepegawaian Daerah', 'http://e-attendance.malangkota.go.id'),
(47, 'SLA(Smart Library Automation)', 'Kantor Perpustakaan dan Arsip Daerah', 'http://digilib.malangkota.go.id'),
(48, 'Aplikasi Monitoring NOC', 'Dinas Komunikasi dan Informatika', 'http://noc.mlangkota.go.id'),
(49, 'NMS (Network Monitoring System)', 'Dinas Komunikasi dan Informatika', 'http://nms.malangkota.go.id'),
(50, 'JDIH', 'Bag. Hukum', 'http://jdih.malangkota.go.id'),
(51, 'SIMAYA', 'Dinas Komunikasi dan Informatika', 'http://simaya.malangkota.go.id');

-- --------------------------------------------------------

--
-- Table structure for table `kominfo_domain`
--

CREATE TABLE `kominfo_domain` (
  `id_aplikasi` int(11) NOT NULL,
  `nama_skpd` text NOT NULL,
  `subdomain` text NOT NULL,
  `th` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kominfo_domain`
--

INSERT INTO `kominfo_domain` (`id_aplikasi`, `nama_skpd`, `subdomain`, `th`) VALUES
(1, 'Bagian Humas', 'http://humas.malangkota.go.id', 2014),
(2, 'Bagian Kesejahteraan Rakyat', 'http://kesra.malangkota.go.id', 2014),
(3, 'Bagian Hukum', 'http://hukum.malangkota.go.id', 2014),
(4, 'Bagian Perekonomian dan Usaha Daerah', 'http://bapeko.malangkota.go.id', 2014),
(5, 'Bagian Organisasi', 'http://organisasi.malangkota.go.id', 2014),
(6, 'Bagian Kerjasama dan Penanaman Modal', 'http://bkpm.malangkota.go.id', 2014),
(7, 'Bagian Pembangunan', 'http://pembangunan.malangkota.go.id', 2014),
(8, 'Bagian Pemerintahan', 'http://pemerintahan.malangkota.go.id', 2014),
(9, 'Bagian Umum', 'http://umum.malangkota.go.id', 2014),
(10, 'Badan Pengelola Keuangan dan Aset Daerah', 'http://bpkad.malangkota.go.id', 2014),
(11, 'Badan Kepegawaian Daerah', 'http://bkd.malangkota.go.id', 2014),
(12, 'Inspektorat', 'http://inspektorat.malangkota.go.id', 2014),
(13, 'Badan Perencanaan Pembangunan Daerah', 'http://bappeda.malangkota.go.id', 2014),
(14, 'Badan Lingkungan Hidup', 'http://blh.malangkota.go.id', 2014),
(15, 'Badan Kesatuan Bangsa dan Politik', 'http://bakesbangpol.malangkota.go.id', 2014),
(16, 'Badan Penanggulangan Bencana Daerah', 'http://bppd.malangkota.go.id', 2014),
(17, 'Satuan Polisi Pamong Praja', 'http://satpolpp.malangkota.go.id', 2014),
(18, 'Kantor Perpustakaan Umum dan Arsip Daerah', 'http://digilib.malangkota.go.id', 2014),
(19, 'Dinas Kepemudaan dan Olahraga', 'http://dispora.malangkota.go.id', 2014),
(20, 'Dinas Pendidikan', 'http://diknas.malangkota.go.id', 2014),
(21, 'Dinas Pertanian', 'http://pertanian.malangkota.go.id', 2014),
(22, 'Dinas Perhubungan', 'http://dishub.malangkota.go.id', 2014),
(23, 'Sekertariat Korpri', 'http://korpri.malangkota.go.id', 2014),
(24, 'Dinas Koperasi Dan UKM', 'http://dinkop.malangkota.go.id', 2014),
(25, 'Dinas Kesehatan', 'http://dinkes.malangkota.go.id', 2014),
(26, 'Badan Keluarga Berencana dan Pemberdayaan Masyarakat', 'http://bkbpm.malangkota.go.id', 2014),
(27, 'Badan Pelayanan Perijinan Terpadu', 'http://perijinan.malangkota.go.id', 2014),
(28, 'Dinas Pendapatan Daerah', 'http://dispenda.malangkota.go.id', 2014),
(29, 'Dinas Komunikasi dan Informatika', 'http://kominfo.malangkota.go.id', 2014),
(30, 'Dinas Komunikasi dan Informatika', 'http://mediacenter.malangkota.go.id', 2014),
(31, 'Dinas Komunikasi dan Informatika', 'http://telecenterdaragati.malangkota.go.id', 2014),
(32, 'Dinas Kebudayaan dan Pariwisata', 'http://budpar.malangkota.go.id', 2014),
(33, 'Dinas Ketenagakerjaan dan Transmigrasi', 'http://disnakertrans.malangkota.go.id', 2014),
(34, 'Dinas Perindustrian dan Perdagangan', 'http://disperindag.malangkota.go.id', 2014),
(35, 'Sekertariat DPRD', 'http://dprd-malangkota.go.id', 2014),
(36, 'Dinas Kependudukan dan Pencatatan Sipil', 'http://dispendukcapil.malangkota.go.id', 2014),
(37, 'Dinas Pasar', 'http://pasar.malangkota.go.id', 2014),
(38, 'Dinas Kebersihan dan Pertamanan', 'http://dkp.malangkota.go.id', 2014),
(39, 'Dinas Pekerjaan Umum, Perumahan dan Pengawasan Bangunan', 'http://dpuppb.malangkota.go.id', 2014),
(40, 'Dinas Sosial', 'http://dinsos.malangkota.go.id', 2014),
(41, 'Kantor Ketahan Pangan', 'http://kkp.malangkota.go.id', 2014),
(42, 'Kecamatan Klojen', 'http://kecklojen.malangkota.go.id', 2014),
(43, 'Kelurahan Gadingkasri', 'http://kelgadingkasri.malangkota.go.id', 2014),
(44, 'Kelurahan Samaan', 'http://kelsamaan.malangkota.go.id', 2014),
(45, 'Kelurahan Kauman', 'http://kelkauman.malangkota.go.id', 2014),
(46, 'Kelurahan Kasin', 'http://kelkasin.malangkota.go.id', 2014),
(47, 'Kelurahan Rampalcelaket', 'http://kelrampalcelaket.malangkota.go.id', 2014),
(48, 'Kelurahan Kiduldalem', 'http://kelkiduldalem.malangkota.go.id', 2014),
(49, 'Kelurahan Bareng', 'http://kelbareng.malangkota.go.id', 2014),
(50, 'Kelurahan Klojen', 'http://kelklojen.malangkota.go.id', 2014),
(51, 'Kelurahan Sukoharjo', 'http://kelsukoharjo.malangkota.go.id', 2014),
(52, 'Kelurahan Oro-oro Dowo', 'http://keloro-orodowo.malangkota.go.id', 2014),
(53, 'Kelurahan Penanggungan', 'http://penanggungan.malangkota.go.id', 2014),
(54, 'Kecamatan Kedungkandang', 'http://keckedungkandang.malangkota.go.id', 2014),
(55, 'Kelurahan Kotalama', 'http://kelkotalama.malangkota.go.id', 2014),
(56, 'Kelurahan Madyopuro', 'http://kelmadyopuro.malangkota.go.id', 2014),
(57, 'Kelurahan Kedungkandang', 'http://kelkedungkandang.malangkota.go.id', 2014),
(58, 'Kelurahan Buring', 'http://kelburing.malangkota.go.id', 2014),
(59, 'Kelurahan Sawojajar', 'http://kelsawojajar.malangkota.go.id', 2014),
(60, 'Kelurahan Cemorokandang', 'http://kelcemorokandang.malangkota.go.id', 2014),
(61, 'Kelurahan Mergosono', 'http://kelmergosono.malangkota.go.id', 2014),
(62, 'Kelurahan Bumiayu', 'http://kelbumiayu.malangkota.go.id', 2014),
(63, 'Kelurahan Wonokoyo', 'http://kelwonokoyo.malangkota.go.id', 2014),
(64, 'Kelurahan Lesanpuro', 'http://kellesanpuro.malangkota.go.id', 2014),
(65, 'Kelurahan Arjowinangun', 'http://kelarjowinangun.malangkota.go.id', 2014),
(66, 'Kelurahan Tlogowaru', 'http://keltlogowaru.malangkota.go.id', 2014),
(67, 'Kecamatan Lowokwaru', 'http://keclowokwaru.malangkota.go.id', 2014),
(68, 'Kelurahan Dinoyo', 'http://keldinoyo.malangkota.go.id', 2014),
(69, 'Kelurahan Merjosari', 'http://kelmerjosari.malangkota.go.id', 2014),
(70, 'Kelurahan Tulusrejo', 'http://keltulusrejo.malangkota.go.id', 2014),
(71, 'Kelurahan Tasikmadu', 'http://keltasikmadu.malangkota.go.id', 2014),
(72, 'Kelurahan Tlogomas', 'http://keltlogomas.malangkota.go.id', 2014),
(73, 'Kelurahan Lowokwaru', 'http://kellowokwaru.malangkota.go.id', 2014),
(74, 'Kelurahan Tunjungsekar', 'http://keltunjungsekar.malangkota.go.id', 2014),
(75, 'Kelurahan Tunggulwulung', 'http://keltunggulwulung.malangkota.go.id', 2014),
(76, 'Kelurahan Sumbersari', 'http://kelsumbersari.malangkota.go.id', 2014),
(77, 'Kelurahan Ketawanggede', 'http://kelketawanggede.malangkota.go.id', 2014),
(78, 'Kelurahan Jatimulyo', 'http://keljatimulyo.malangkota.go.id', 2014),
(79, 'Kelurahan Mojolangu', 'http://kelmojolangu.malangkota.go.id', 2014),
(80, 'Kecamatan Blimbing', 'http://kecblimbing.malangkota.go.id', 2014),
(81, 'Kelurahan Arjosari', 'http://kelarjosari.malangkota.go.id', 2014),
(82, 'Kelurahan Purwodadi', 'http://kelpurwodadi.malangkota.go.id', 2014),
(83, 'Kelurahan Balearjosari', 'http://kelbalearjosari.malangkota.go.id', 2014),
(84, 'Kelurahan Bunulrejo', 'http://kelbunulrejo.malangkota.go.id', 2014),
(85, 'Kelurahan Kesatrian', 'http://kelkesatrian.malangkota.go.id', 2014),
(86, 'Kelurahan Polehan', 'http://kelpolehan.malangkota.go.id', 2014),
(87, 'Kelurahan Blimbing', 'http://kelblimbing.malangkota.go.id', 2014),
(88, 'Kelurahan Polowijen', 'http://kelpolowijen.malangkota.go.id', 2014),
(89, 'Kelurahan Pandanwangi', 'http://kelpandanwangi.malangkota.go.id', 2014),
(90, 'Kelurahan Purwantoro', 'http://kelpurwantoro.malangkota.go.id', 2014),
(91, 'Kelurahan Jodipan', 'http://keljodipan.malangkota.go.id', 2014),
(92, 'Kecamatan Sukun', 'http://kecsukun.malangkota.go.id', 2014),
(93, 'Kelurahan Bandulan', 'http://kelbandulan.malangkota.go.id', 2014),
(94, 'Kelurahan Kebonsari', 'http://kelkebonsari.malangkota.go.id', 2014),
(95, 'Kelurahan Sukun', 'http://kelsukun.malangkota.go.id', 2014),
(96, 'Kelurahan Ciptomulyo', 'http://kelciptomulyo.malangkota.go.id', 2014),
(97, 'Kelurahan Gadang', 'http://kelgadang.malangkota.go.id', 2014),
(98, 'Kelurahan Bandungrejosari', 'http://kelbandungrejosari.malangkota.go.id', 2014),
(99, 'Kelurahan Tanjungrejo', 'http://keltanjungrejo.malangkota.go.id', 2014),
(100, 'Kelurahan Pisangcandi', 'http://kelpisangcandi.malangkota.go.id', 2014),
(101, 'Kelurahan Karangbesuki', 'http://kelkarangbesuki.malangkota.go.id', 2015),
(102, 'Kelurahan Mulyorejo', 'http://kelmulyorejo.malangkota.go.id', 2015),
(103, 'Kelurahan Bakalankrajan', 'http://kelbakalankrajan.malangkota.go.id', 2015),
(104, 'KONI Kota Malang', 'http://koni.malangkota.go.id', 2015),
(105, 'PKK Kota Malang', 'http://pkk.malangkota.go.id', 2015),
(106, 'Badan Zakat', 'http://baznas.malangkota.go.id', 2016),
(107, 'Palang Merah Indonesia', 'http://pmi.malangkota.go.id', 2016),
(108, 'Puskesmas Bareng', 'puskbareng.malangkota.go.id', 2017),
(109, 'Puskesmas Kedungkandang', 'puskkedungkandang.malangkota.go.id', 2017),
(110, 'Puskesmas Gribig', 'puskgribig.malangkota.go.id', 2017),
(111, 'Puskesmas Arjowinangun', 'puskarjowinangun.malangkota.go.id', 2017),
(112, 'Puskesmas Janti', 'puskjanti.malangkota.go.id', 2017),
(113, 'Puskesmas Ciptomulyo', 'puskciptomulyo.malangkota.go.id', 2017),
(114, 'Puskesmas Arjuno', 'puskarjuno.malangkota.go.id', 2017),
(115, 'Puskesmas Mulyorejo', 'puskmulyorejo.malangkota.go.id', 2017),
(116, 'Puskesmas Rampalcelaket', 'puskrampalcelaket.malangkota.go.id', 2017),
(117, 'Puskesmas Cisadea', 'puskcisadea.malangkota.go.id', 2017),
(118, 'Puskesmas Kendalkerep', 'puskkendalkerep.malangkota.go.id', 2017),
(119, 'Puskesmas Pandanwangi', 'puskpandanwangi.malangkota.go.id', 2017),
(120, 'Puskesmas Dinoyo', 'puskdinoyo.malangkota.go.id', 2017),
(121, 'Puskesmas Mojolangu', 'puskmojolangu.malangkota.go.id', 2017),
(122, 'Puskesmas Kendalsari', 'puskkendalsari.malangkota.go.id', 2017),
(123, 'Puskesmas Polowijen', 'puskpolowijen.malangkota.go.id', 2017),
(124, 'Labotarium Kesehatan', 'labkes.malangkota.go.id', 2017),
(125, 'Penanggulangan Pertama Pada Kecelakaan', 'pppk.malangkota.go.id', 2017),
(126, 'Pusat Pelayanan Kesehatan Olahraga', 'ppko.malangkota.go.id', 2017),
(127, 'Bagian SDA dan Pengembangan Infrastruktur', 'sda.malangkota.go.id', 2017),
(128, 'Dinas Komunikasi dan Informatika', 'telecenter-emashitam.malangkota.go.id', 2017),
(129, 'Bagian Pengembangan Perekonomian', 'ukmunggulanmalang.malangkota.go.id', 2017);

-- --------------------------------------------------------

--
-- Table structure for table `lp_aparat`
--

CREATE TABLE `lp_aparat` (
  `id` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(11) NOT NULL,
  `jml` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_aparat`
--

INSERT INTO `lp_aparat` (`id`, `id_jenis`, `th`, `jml`) VALUES
(11, 6, 2014, 1),
(12, 7, 2016, 8),
(23, 1, 2014, 959),
(27, 2, 2014, 134),
(28, 4, 2015, 27),
(29, 1, 2015, 968),
(30, 1, 2016, 959),
(31, 2, 2015, 120),
(32, 2, 2016, 116),
(33, 3, 2014, 1995),
(34, 3, 2015, 1995),
(35, 3, 2016, 2166),
(36, 4, 2014, 27),
(37, 4, 2016, 27),
(38, 5, 2014, 1267),
(39, 5, 2015, 1690),
(40, 5, 2016, 1690),
(41, 6, 2015, 1),
(42, 6, 2016, 1),
(43, 7, 2014, 8),
(44, 7, 2015, 8);

-- --------------------------------------------------------

--
-- Table structure for table `lp_aparat_jenis`
--

CREATE TABLE `lp_aparat_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_aparat_jenis`
--

INSERT INTO `lp_aparat_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Aparat keamanan (Polisi) (orang)'),
(2, 'Aparat Pamong Praja (Satpol PP) (orang)'),
(3, 'Aparat Perlindungan Masyarakat (Linmas)\r\n(orang)'),
(4, 'Pos Keamanan (Polisi)\r\n(unit)'),
(5, 'Pos Siskamling\r\n(unit)'),
(6, 'Pos Pemadam Kebakaran (unit)'),
(7, 'Mobil Pemadam Kebakaran (unit)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_bencana`
--

CREATE TABLE `lp_bencana` (
  `id_bencana` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_bencana`
--

INSERT INTO `lp_bencana` (`id_bencana`, `id_jenis`, `id_kec`, `th`, `jml`) VALUES
(1, 1, 'KEC1001', 2016, '8'),
(2, 2, 'KEC1001', 2016, '4'),
(3, 1, 'KEC1002', 2016, '10'),
(4, 2, 'KEC1002', 2016, '1'),
(5, 1, 'KEC1003', 2016, '9'),
(6, 2, 'KEC1003', 2016, '2'),
(7, 3, 'KEC1003', 2016, '1'),
(8, 4, 'KEC1003', 2016, '1'),
(9, 1, 'KEC1004', 2016, '5'),
(10, 2, 'KEC1004', 2016, '4'),
(11, 3, 'KEC1004', 2016, '1'),
(12, 1, 'KEC1005', 2016, '4'),
(13, 2, 'KEC1005', 2016, '1');

-- --------------------------------------------------------

--
-- Table structure for table `lp_bencana_jenis`
--

CREATE TABLE `lp_bencana_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_bencana_jenis`
--

INSERT INTO `lp_bencana_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Tanah Longsor'),
(2, 'Cuaca Ekstrim'),
(3, 'Banjir'),
(4, 'Gempa Bumi');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kajian_penelitian`
--

CREATE TABLE `lp_kajian_penelitian` (
  `id_kajian` int(11) NOT NULL,
  `nama_kajian` text NOT NULL,
  `th` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kajian_penelitian`
--

INSERT INTO `lp_kajian_penelitian` (`id_kajian`, `nama_kajian`, `th`) VALUES
(1, 'Analisis Angka Tingkat Pengangguran', 2014),
(2, 'Kajian Fasilitas dan Insentif Pendukung Kepala Investor di Kota Malang', 2014),
(3, 'Studi Kebutuhan Teknologi Untuk Pengembangan Usaha Kecil dan Menengah di Kota Malang', 2014),
(4, 'Profil Perlindungan Sosial dan Pemberdayaan Masyarakat di Kota Malang', 2014),
(5, 'Analisa Kesesuaian Toko Modern Terhadap Perijinan Kota Malang', 2014),
(6, 'Analisa Angka Pertumbuhan Ekonomi di Kota Malang', 2014),
(7, 'Kajian Strategi Pemberdayaan Usaha Kecil Menengah Dalam Menyongsong Ekonomi Global ASEAN 2015', 2014),
(8, 'Penyusunan Pemetaan Potensi dan Pengembangan Ekonomi Sektor Informal Kota Malang', 2014),
(9, 'Rencana Aksi Daerah Pangan dan Gizi Kota Malang', 2014),
(10, 'Rencana Umum Penanaman Modal Kota Malang', 2014),
(11, 'Analisa Indeks Pembangunan Manusia Kota Malang Tahun 2014', 2014),
(12, 'Penyusunan Naskah Akademik dan Ranperda Tentang Sistem Kesehatan Daerah Kota Malang', 2014),
(13, 'Penyusunan Studi Kelayakan Akses Jalan Tunggulwulung -Sudimoro - Karanglo Pada Bagian Wilayah Perkotaan Malang Utara', 2014),
(14, 'Naskah Akademis Rencana Induk Sistem Penyediaan Air Minum (RISPAM) dan Penyusunan Rencana Peraturan Walikota Tentang RISPAM', 2014),
(15, 'Penyusunan Study Kelayakan Sarana Angkutan Umum Masal (SAUM) Kota Malang', 2014),
(16, 'Penyusunan Study Kelayakan dan Pra Detail Engineering Design Jembatan - Tlogomas - Saksofon Pada Bagian Wilayah Perkotaan Malang Utara', 2014),
(17, 'Penyusunan Naskah Akademis dan Rancangan Peraturan Walikota Tentang Tata Cara Pemberian Insentif dan Disinsentif Pemanfaatan Ruang Kota', 2014),
(18, 'Rencana Program Investasi Jangka Menengah (RPIJM), Dokumen Etrategi Sanitasi Kota (SSK) dan Memorandum Program Sanitasi (MPS) Berdasarkan', 2014),
(19, 'Penyusunan Naskah Akademis dan Rancangan Peraturan Walikota Tentang Rencana Tata Bangunan dan Lingkungan Sub BWP Prioritas Pada BWP Malang Tengah', 2014),
(20, 'Penyusunan Naskah Akademis dan Rancangan Peraturan Walikota Tentang Rencana Tata Bangunan dan Lingkungan Sub BWP Prioritas Pada BWP Malang Utara', 2014),
(21, 'Penyusunan Studi Kelayakan Pelebaran Jalan Ki Ageng Gribig Pada Bagian Wilayah Perkantoran Malang Timur', 2014),
(22, 'Penyusunan Studi Kelayakan Pelebaran Jalan Mayjend Sungkono Pada Bagian Wilayah Perkotaan Malang Tenggara', 2014),
(23, 'Penyusunan Naskah Akademis dan Rancangan Peraturan Walikota Tentang Tata Cara Partisipasi Masyarakat Dalam Penataan Ruang', 2014),
(24, 'Pembuatan Sistem Informasi Penataan Ruang Kota Malang', 2014),
(25, 'Analisa Kebutuhan Kantong Perkir di Bagian Wilayah Perncanaan Malang Tengah', 2015),
(26, 'Kajian Pemanfaatan Pelayanan Pemerintahan Berbasis Elektronik', 2015),
(27, 'Kajian Peningkatan Kelembagaan Usaha Ekonomi Perempuan', 2015),
(28, 'Pengembangan Modal Subsidi Pendidikan Bagi Warga Miskin', 2015),
(29, 'Kajian Peningkatan Fasilitas Sarana dan Prasarana Penunjang Destinasi Periwisata', 2015),
(30, 'Analisis Penguatan Kemitraan di Sektor Pariwisata , Hotel, Restoran dan Transportasi Untuk Peningkatan Jumlah Kunjungan dan Lama Hari Berkunjung', 2015),
(31, 'Penyusunan Naskah Akademis dan RANPERDA Penataan UMKM di Kota Malang', 2015),
(32, 'Penyusunan Naskah Akademis dan RANPERWAL Fasilitas dan Insentif Pendukung Kepada Investor di Kota Malang', 2015),
(33, 'Kajian Inovasi dan Peningkatan Standarisasi Produk', 2015),
(34, 'Analisa Pengembangan Semangat Kewirausahaan Bagi Wirausaha Pemula Kota Malang', 2015),
(35, 'Kajian Peran Koperasi Dalam Ekonomi Kerakyatan', 2015),
(36, 'Kajian Minat Baca Masyarakat dan Akses ke Perpustakaan Umum Kota Malang', 2015),
(37, 'Analisa Sarana dan Prasarana Olah Raga di Kota Malang', 2015),
(38, 'Pemetaan Kesenian', 2015),
(39, 'Kajian dan Investarisasi Berbasis Masyarakat', 2015),
(40, 'Evaluasi Rencana Aksi Daerah Pangan dan Gizi Kota Malang', 2015),
(41, 'Analisa dan Perhitungan PDRB Kota Malang', 2015),
(42, 'Rencana Induk Pengembangan Ekonomi Kreatif Kota Malang', 2015),
(43, 'Analisis Situasi Ibu dan Anak Kota Malang', 2015),
(44, 'Analisis Indeks Pembangunan Manusia Kota Malang', 2015),
(45, 'Laporan Pencapaian MDG’s Kota Malang', 2015),
(46, 'Naskah Akademis dan Ranperda Perlindungan dan Pemenuhan Hak Anak', 2015),
(47, 'Penyusunan Database Infrastuktur Daerah', 2015),
(48, 'Penyusunan Database Hukum dan Keamanan Serta Insidensial', 2015),
(49, 'Penyusunan Datebase Sosial Budaya dan Ekonomi Daerah', 2015),
(50, 'Penyusunan Database Geografi Pemerintahan Demografi dan SDA', 2015),
(51, 'Penyusunan Grand Design Peruntukan DBHCHT', 2015),
(52, 'Penyusunan Rancangan Awal RKPD Tahun 2017', 2015),
(53, 'Evaluasi Hasil RKPD Tahun 2015', 2015),
(54, 'Penyusunan NA dan Ranperda Perubahan RPJMD 2013-2018', 2015),
(55, 'Penyusunan Profil Kota Malang Tahun 2014', 2015),
(56, 'Pendampingan Aplikasi Sistem Perencanaan Pembangunan', 2015),
(57, 'Penyusunan Data SIPD', 2015),
(58, 'Penyusunan Rencana Aksi Malang Sustainable Urban Development (SUD)', 2015),
(59, 'Penyusunan Studi Kelayakan Pembangunan Jalan Tembus Tidar- Genting', 2015),
(60, 'Penyusunan Studi Kelayakan Underpass Taman Trunojoyo – Taman Sriwijaya - Stasiun', 2015),
(61, 'Penyusunan Rencana Aksi Malang Kota Pusaka (Heritage City)', 2015),
(62, 'Penyusunan Rencana Aksi Malang Kota Hijau (Green City)', 2015),
(63, 'Penyusunan Rencana Aksi Malang Tanpa Kumuh (Slum Free City)', 2015),
(64, 'Penyusunan Materi Teknis dan Rancangan Peraturan Walikota Tentang Rencana Induk Sistem Proteksi Kebakaran', 2015),
(65, 'Pelaksanaan Enironmental Helth Risk Assesment (EHRA)', 2015),
(66, 'Penyusunan Database Pembangunan', 2015),
(67, 'Penyusunan Materi Teknis dan Rancangan Peraturan Walikota Tentang Tata Cara Pengenaan Sanksi Atas Pelanggaran Pemanfaatan Ruang', 2015),
(68, 'Penyusunan Materi Teknis dan Rancangan Peraturan Walikota Tentang Mekanisme Pengendalian Pemanfaat Ruang Kota Malang', 2015),
(69, 'Profil Kuliner Kota Malang', 2016),
(70, 'Analisa Dampak Sosial Ekonomi Budaya Urban Mahasiswa Terhadap Masyarakat Kota Malang', 2016),
(71, 'Kajian Peran Lembaga Keuangan Dalam Pembangunan UMKM', 2016),
(72, 'Kajian Penambahan Prasarana Sekolah', 2016),
(73, 'Analisa Derajat Kesehatan', 2016),
(74, 'Analisa Potensi Peredaran Uang di Pasar Tradisional', 2016),
(75, 'Analisa Dampak Ekonomi Bank Sampah Masyarakat Terhadap Peningkatan Kualitas hidup Masyarakat di Kota Malang', 2016),
(76, 'Analisa Keamanan Pangan Guna Meningkatkan Daya Saing Usaha Mikro, Kecil dan Menengah Dalam Rangka', 2016),
(77, 'Database Pelaku Ekonomi Kota Malang', 2016),
(78, 'Pengembangan Pola Kemitraan UMKM dan IKM', 2016),
(79, 'Profil Pangan dan Gizi Kota Malang', 2016),
(80, 'Analisis PDRB Kota Malang', 2016),
(81, 'Rencana Aksi Penanganan Penyandang Masalah Kesejahteraan Sosial (PMKS)', 2016),
(82, 'Analisis Indeks Pembangunan Manusia Kota Malang Tahun 2016', 2016),
(83, 'Profil Kemiskinan', 2016),
(84, 'Profil Kota Layak Anak', 2016),
(85, 'Profil Kota Sehat', 2016),
(86, 'Kegiatan Integerasi Pembangunan Aplikasi Perencanaan Pembangunan dan Penganggaran', 2016),
(87, 'Kegiatan Pengembangan Aplikasi Perencanaan Pembangunan dan Penganggaran Kota Malang', 2016),
(88, 'Penyusunan Indentifikasi dan Analisa Isu Strategis Pembangunan Daerah', 2016),
(89, 'Kegiatan Penyusunan Prpofil Kota Malang Tahun 2015', 2016),
(90, 'Kajian Kebijakan Publik Dalam Perencanaan Pembangunan Kota Malang', 2016),
(91, 'Penyusunan Updating Rencana Program Investasi Infrastruktur Jangka Menengah (RP12JM)', 2016),
(92, 'Penyusunan Updating Dokumen Strategis Sanitasi Kota (SSK) dan Memorandum Program Sanitasi (MPS)', 2016),
(93, 'Penyusunan Roadmap Land Banking Kota Malang', 2016),
(94, 'Penyusunan Roadmap Malang Kota Berketahanan Bencana dan Perubahan Iklim', 2016),
(95, 'Penyusunan Naskah Akademis dan Rancangan Peraturan Walikota Tentang Rencana Induk Pengelolaan Sampah Rumah Tangga dan Sampah Sejenis Rumah Tangga', 2016),
(96, 'Penyusunan Review Naskah Akademis dan Rancangan Peraturan Walikota Tentang Rencana Induk Jaringan Jalan Terintegerasi Malang Raya', 2016),
(97, 'Penyusunan Studi Kelayakan dan Konsep Desain Malang Art And Spatial Gallery', 2016),
(98, 'Pengembangan Integerasi Aplikasi Sisten Informasi Penataan Ruang', 2016),
(99, 'Review Rencana Tata Ruang Wilayah Kota Malang Tahun 2010-2030', 2016),
(100, 'Review Penyusunan Rencana Kawasan Stategis', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `lp_kel_umur`
--

CREATE TABLE `lp_kel_umur` (
  `id_kel_umur` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_l` varchar(10) NOT NULL,
  `jml_p` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kel_umur`
--

INSERT INTO `lp_kel_umur` (`id_kel_umur`, `id_jenis`, `th`, `jml_l`, `jml_p`) VALUES
(1, 1, 2014, '32352', '30756'),
(2, 1, 2015, '32560', '30938'),
(3, 1, 2016, '32756', '31113'),
(4, 2, 2014, '31577', '29959'),
(5, 2, 2015, '31779', '30135'),
(6, 2, 2016, '31969', '30308'),
(7, 3, 2014, '29736', '29008'),
(8, 3, 2015, '29928', '29181'),
(9, 3, 2016, '30109', '29350'),
(10, 4, 2014, '38692', '42931'),
(11, 4, 2015, '38940', '43185'),
(12, 4, 2016, '39173', '43436'),
(13, 5, 2014, '53113', '51380'),
(14, 5, 2015, '53449', '51674'),
(15, 5, 2016, '53763', '51967'),
(16, 6, 2014, '36831', '34415'),
(17, 6, 2015, '37066', '34613'),
(18, 6, 2016, '37284', '34810'),
(19, 7, 2014, '33641', '32884'),
(20, 7, 2015, '33855', '33079'),
(21, 7, 2016, '34057', '33268'),
(22, 8, 2014, '30322', '30879'),
(23, 8, 2015, '30519', '31063'),
(24, 8, 2016, '30701', '31244'),
(25, 9, 2014, '28666', '30578'),
(26, 9, 2015, '28854', '30765'),
(27, 9, 2016, '29032', '30947'),
(28, 10, 2014, '26078', '29000'),
(29, 10, 2015, '26254', '29179'),
(30, 10, 2016, '26418', '29358'),
(31, 11, 2014, '23368', '25874'),
(32, 11, 2015, '23528', '26039'),
(33, 11, 2016, '23678', '26200'),
(34, 12, 2014, '18958', '20092'),
(35, 12, 2015, '19092', '20220'),
(36, 12, 2016, '19216', '20350'),
(37, 13, 2014, '13322', '13604'),
(38, 13, 2015, '13417', '13692'),
(39, 13, 2016, '13506', '13780'),
(40, 14, 2014, '20326', '27631'),
(41, 14, 2015, '20472', '27822'),
(42, 14, 2016, '20614', '28003');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kel_umur_jenis`
--

CREATE TABLE `lp_kel_umur_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kel_umur_jenis`
--

INSERT INTO `lp_kel_umur_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, '0-4'),
(2, '5-9'),
(3, '10-14'),
(4, '15-19'),
(5, '20-24'),
(6, '25-29'),
(7, '30-34'),
(8, '35-39'),
(9, '40-44'),
(10, '45-49'),
(11, '50-54'),
(12, '55-59'),
(13, '60-64'),
(14, '65+');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kendaran_kec`
--

CREATE TABLE `lp_kendaran_kec` (
  `id_kendaraan` int(11) NOT NULL,
  `id_jenis_kendaraan` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kendaran_kec`
--

INSERT INTO `lp_kendaran_kec` (`id_kendaraan`, `id_jenis_kendaraan`, `id_kec`, `th`, `jml`) VALUES
(1, 1, 'KEC1001', 2015, '20044'),
(2, 1, 'KEC1001', 2016, '15470'),
(3, 2, 'KEC1001', 2015, '121'),
(4, 2, 'KEC1001', 2016, '113'),
(5, 3, 'KEC1001', 2015, '4158'),
(6, 3, 'KEC1001', 2016, '3529'),
(7, 4, 'KEC1001', 2015, '98035'),
(8, 4, 'KEC1001', 2016, '97595'),
(9, 1, 'KEC1002', 2015, '14607'),
(10, 1, 'KEC1002', 2016, '17170'),
(11, 2, 'KEC1002', 2015, '112'),
(12, 2, 'KEC1002', 2016, '163'),
(13, 3, 'KEC1002', 2015, '3398'),
(14, 3, 'KEC1002', 2016, '4410'),
(15, 4, 'KEC1002', 2015, '93795'),
(16, 4, 'KEC1002', 2016, '107218'),
(17, 1, 'KEC1003', 2015, '13068'),
(18, 1, 'KEC1003', 2016, '13306'),
(19, 2, 'KEC1003', 2015, '252'),
(20, 2, 'KEC1003', 2016, '265'),
(21, 3, 'KEC1003', 2015, '4092'),
(22, 3, 'KEC1003', 2016, '4060'),
(23, 4, 'KEC1003', 2015, '54967'),
(24, 4, 'KEC1003', 2016, '56385'),
(25, 1, 'KEC1004', 2015, '21985'),
(26, 1, 'KEC1004', 2016, '21098'),
(27, 2, 'KEC1004', 2015, '315'),
(28, 2, 'KEC1004', 2016, '137'),
(29, 3, 'KEC1004', 2015, '3463'),
(30, 3, 'KEC1004', 2016, '4445'),
(31, 4, 'KEC1004', 2015, '90455'),
(32, 4, 'KEC1004', 2016, '101541'),
(33, 1, 'KEC1005', 2015, '16387'),
(34, 1, 'KEC1005', 2016, '23014'),
(35, 2, 'KEC1005', 2015, '134'),
(36, 2, 'KEC1005', 2016, '288'),
(37, 3, 'KEC1005', 2015, '4356'),
(38, 3, 'KEC1005', 2016, '3558'),
(39, 4, 'KEC1005', 2015, '103871'),
(40, 4, 'KEC1005', 2016, '93954');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kendaran_plat`
--

CREATE TABLE `lp_kendaran_plat` (
  `id_kendaraan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_merah` varchar(10) NOT NULL,
  `jml_kuning` varchar(10) NOT NULL,
  `jml_hitam` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kendaran_plat`
--

INSERT INTO `lp_kendaran_plat` (`id_kendaraan`, `id_jenis`, `th`, `jml_merah`, `jml_kuning`, `jml_hitam`) VALUES
(1, 1, 2015, '13923', '165', '77'),
(2, 1, 2016, '13585', '174', '85'),
(3, 2, 2015, '8337', '-', '38'),
(4, 2, 2016, '8699', '-', '35'),
(5, 3, 2015, '60460', '2442', '649'),
(6, 3, 2016, '64417', '2401', '662'),
(7, 4, 2015, '379', '502', '53'),
(8, 4, 2016, '418', '481', '67'),
(9, 5, 2015, '15956', '3312', '199'),
(10, 5, 2016, '16473', '3317', '212'),
(11, 6, 2015, '439648', '-', '1475'),
(12, 6, 2016, '455073', '-', '1620');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kendaran_plat_jenis`
--

CREATE TABLE `lp_kendaran_plat_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kendaran_plat_jenis`
--

INSERT INTO `lp_kendaran_plat_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Sedan dan Sejenisnya'),
(2, 'Jeep dan Sejenisnya'),
(3, 'Station Wagon dan Sejenisnya'),
(4, 'Bus dan Sejenisnya'),
(5, 'Truck dan Sejenisnya'),
(6, 'Sepeda Motor');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kendaran_umum`
--

CREATE TABLE `lp_kendaran_umum` (
  `id_kendaraan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_jenis_kendaraan` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kendaran_umum`
--

INSERT INTO `lp_kendaran_umum` (`id_kendaraan`, `id_jenis`, `id_jenis_kendaraan`, `th`, `jml`) VALUES
(1, 1, 1, 2015, '2900'),
(2, 1, 1, 2016, '2329'),
(3, 2, 1, 2015, '85194'),
(4, 2, 1, 2016, '78318'),
(5, 3, 1, 2015, '739'),
(6, 3, 1, 2016, '746'),
(7, 1, 2, 2015, '356'),
(8, 1, 2, 2016, '371'),
(9, 2, 2, 2015, '367'),
(10, 2, 2, 2016, '377'),
(11, 3, 2, 2015, '60'),
(12, 3, 2, 2016, '63'),
(13, 1, 3, 2015, '2116'),
(14, 1, 3, 2016, '2799'),
(15, 2, 3, 2015, '13825'),
(16, 2, 3, 2016, '14137'),
(17, 3, 3, 2015, '168'),
(18, 3, 3, 2016, '188'),
(19, 1, 4, 2015, '-'),
(20, 1, 4, 2016, '-'),
(21, 2, 4, 2015, '463254'),
(22, 2, 4, 2016, '358219'),
(23, 3, 4, 2015, '1344'),
(24, 3, 4, 2016, '1374'),
(25, 1, 1, 2014, '2490'),
(26, 1, 1, 2017, '2197'),
(27, 1, 1, 2018, '1945'),
(28, 2, 1, 2014, '79010'),
(29, 2, 1, 2017, '74867'),
(30, 2, 1, 2018, '83526'),
(31, 3, 1, 2014, '722'),
(32, 3, 1, 2017, '767'),
(33, 3, 1, 2018, '766'),
(34, 1, 2, 2014, '342'),
(35, 1, 2, 2017, '339'),
(36, 1, 2, 2018, '326'),
(37, 2, 2, 2014, '358'),
(38, 2, 2, 2017, '424'),
(39, 2, 2, 2018, '429'),
(40, 3, 2, 2014, '56'),
(41, 3, 2, 2017, '78'),
(42, 3, 2, 2018, '84'),
(43, 1, 3, 2014, '1698'),
(44, 1, 3, 2017, '2603'),
(45, 1, 3, 2018, '2359'),
(46, 2, 3, 2014, '10803'),
(47, 2, 3, 2017, '14278'),
(48, 2, 3, 2018, '13895'),
(49, 3, 3, 2014, '154'),
(50, 3, 3, 2017, '210'),
(51, 3, 3, 2018, '218'),
(52, 1, 4, 2014, '0'),
(53, 1, 4, 2017, '0'),
(54, 1, 4, 2018, '0'),
(55, 2, 4, 2014, '423963'),
(56, 2, 4, 2017, '261213'),
(57, 2, 4, 2018, '348246'),
(58, 3, 4, 2014, '1298'),
(59, 3, 4, 2017, '1546'),
(60, 3, 4, 2018, '1520');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kendaran_umum_jenis`
--

CREATE TABLE `lp_kendaran_umum_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kendaran_umum_jenis`
--

INSERT INTO `lp_kendaran_umum_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Umum'),
(2, 'Non Umum (Pribadi)'),
(3, 'Dinas');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kesehatan`
--

CREATE TABLE `lp_kesehatan` (
  `id_kesehatan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kesehatan`
--

INSERT INTO `lp_kesehatan` (`id_kesehatan`, `id_jenis`, `id_sub_jenis`, `th`, `jml`) VALUES
(3, 1, 1, 2014, '655'),
(4, 1, 1, 2015, '653'),
(5, 1, 1, 2016, '647'),
(6, 27, 0, 2014, '57'),
(7, 27, 0, 2015, '57'),
(8, 27, 0, 2016, '57'),
(9, 5, 4, 2014, '15'),
(10, 5, 4, 2015, '15'),
(11, 5, 4, 2016, '15'),
(12, 5, 5, 2014, '34'),
(13, 5, 5, 2015, '34'),
(14, 5, 5, 2016, '34'),
(15, 5, 6, 2014, '15'),
(16, 5, 6, 2015, '15'),
(17, 5, 6, 2016, '15'),
(18, 6, 8, 2014, '1'),
(19, 6, 8, 2015, '1'),
(20, 6, 8, 2016, '1'),
(21, 6, 11, 2016, '1'),
(22, 7, 12, 2015, '2'),
(23, 7, 12, 2016, '4'),
(24, 7, 13, 2014, '5'),
(25, 7, 13, 2015, '4'),
(26, 7, 13, 2016, '5'),
(27, 7, 14, 2014, '3'),
(28, 7, 14, 2015, '3'),
(29, 7, 14, 2016, '1'),
(30, 8, 16, 2014, '13'),
(31, 8, 16, 2015, '13'),
(32, 8, 16, 2016, '13'),
(33, 28, 0, 2014, '589'),
(34, 28, 0, 2015, '570'),
(35, 28, 0, 2016, '600'),
(36, 9, 26, 2014, '2'),
(37, 9, 26, 2015, '2'),
(38, 9, 26, 2016, '2'),
(39, 10, 30, 2014, '155788'),
(40, 10, 30, 2015, '161143'),
(41, 10, 30, 2016, '169200'),
(42, 10, 31, 2014, '134907'),
(43, 10, 31, 2015, '134907'),
(44, 10, 31, 2016, '134907'),
(45, 10, 33, 2014, '220'),
(46, 10, 33, 2015, '220'),
(47, 10, 33, 2016, '220'),
(48, 11, 0, 2014, '847251'),
(49, 11, 0, 2015, '848542'),
(50, 11, 0, 2016, '856410'),
(51, 13, 0, 2014, '1'),
(52, 13, 0, 2015, '1'),
(53, 13, 0, 2016, '1'),
(54, 14, 0, 2014, '1'),
(55, 14, 0, 2015, '1'),
(56, 14, 0, 2016, '1'),
(57, 15, 0, 2014, '70'),
(58, 15, 0, 2015, '70'),
(59, 15, 0, 2016, '25'),
(60, 16, 0, 2014, '1'),
(61, 16, 0, 2015, '1'),
(62, 16, 0, 2016, '1'),
(63, 18, 0, 2016, '16'),
(64, 19, 0, 2014, '23'),
(65, 19, 0, 2015, '23'),
(66, 19, 0, 2016, '21'),
(67, 22, 38, 2014, '1611'),
(68, 22, 38, 2015, '1627'),
(69, 22, 38, 2016, '1868'),
(70, 22, 39, 2014, '40312'),
(71, 22, 39, 2015, '36909'),
(72, 22, 39, 2016, '36551'),
(73, 22, 40, 2014, '1611'),
(74, 22, 40, 2015, '1627'),
(75, 22, 40, 2016, '1868'),
(76, 22, 41, 2014, '119'),
(77, 22, 41, 2015, '100'),
(78, 22, 41, 2016, '66'),
(79, 22, 42, 2014, '119'),
(80, 22, 42, 2015, '100'),
(81, 22, 42, 2016, '66'),
(82, 23, 43, 2014, '1'),
(83, 23, 43, 2015, '3'),
(84, 23, 43, 2016, '3'),
(85, 24, 0, 2014, '768'),
(86, 24, 0, 2015, '668'),
(87, 24, 0, 2016, '643'),
(88, 25, 49, 2014, '2180'),
(89, 25, 49, 2015, '2271'),
(90, 25, 49, 2016, '2304'),
(91, 25, 50, 2014, '116057'),
(92, 25, 50, 2015, '101440'),
(93, 25, 50, 2016, '104609');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kesehatan_jenis`
--

CREATE TABLE `lp_kesehatan_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL,
  `id_kategori` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kesehatan_jenis`
--

INSERT INTO `lp_kesehatan_jenis` (`id_jenis`, `nama_jenis`, `id_kategori`) VALUES
(1, 'Posyandu', 1),
(5, 'Puskesmas', 1),
(6, 'Rumah Sakit Umum Daerah', 1),
(7, 'Rumah Sakit Umum Swasta', 1),
(8, 'Rumah Sakit Khusus', 1),
(9, 'Rumah Sakit Tentara', 1),
(10, 'Jumlah Rumah Tangga Yang Menggunakan Air Bersih', 1),
(11, 'Jumlah Penduduk Yang Mendapatkan Akses Air Minum', 1),
(12, 'Industri Farmasi Narkotika', 2),
(13, 'Industri Farmasi Produksi Obat Tradisional / Industri Obat Tradisional', 2),
(14, 'Gudang Farmasi\r\n', 2),
(15, 'PBF (Pedagang Besar Farmasi)', 2),
(16, 'Produk Alat Kesehatan', 2),
(17, 'Penyalur Obat Kesehatan', 2),
(18, 'Cabang Penyalur Obat Kesehatan', 2),
(19, 'Penyalur Alat Kesehatan', 2),
(20, 'Jumlah Orang Sakit Jiwa\r\n', 3),
(21, 'Jumlah Penderita Narkoba\r\n', 3),
(22, 'Jumlah Balita', 3),
(23, 'Jumlah Penduduk meninggal Menurut Wabah (Total)', 3),
(24, 'Jumlah ibu Hamil Gizi buruk', 3),
(25, ' Jumlah peserta Program KB Aktif', 3),
(26, 'Rata-rata Jumlah Penduduk Yang Sakit', 3),
(27, 'Poskesdes', 1),
(28, 'Klinik/Praktek', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lp_kesehatan_kategori`
--

CREATE TABLE `lp_kesehatan_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kesehatan_kategori`
--

INSERT INTO `lp_kesehatan_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Sarana Kesehatan'),
(2, ' Jumlah Sarana Industri Dan Industri Farmasi'),
(3, 'Kesehatan Masyarakat');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kesehatan_sub3_jenis`
--

CREATE TABLE `lp_kesehatan_sub3_jenis` (
  `id_3sub_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `nama_3sub_jenis` text NOT NULL,
  `satuan` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lp_kesehatan_sub_jenis`
--

CREATE TABLE `lp_kesehatan_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL,
  `satuan` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kesehatan_sub_jenis`
--

INSERT INTO `lp_kesehatan_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`, `satuan`) VALUES
(1, 1, 'Posyandu Terdaftar', 'unit'),
(2, 1, 'Posyandu Tidak Aktif', 'unit'),
(4, 5, 'Induk\r\n', 'unit'),
(5, 5, 'Pembantu\r\n', 'unit'),
(6, 5, 'Keliling', 'unit'),
(7, 5, 'Poliklinik', 'unit'),
(8, 6, 'Tipe A', 'unit'),
(9, 6, 'Tipe B', 'unit'),
(10, 6, 'Tipe C\r\n', 'unit'),
(11, 6, 'Tipe D\r\n', 'unit'),
(12, 7, 'Tipe A', 'unit'),
(13, 7, 'Tipe B', 'unit'),
(14, 7, 'Tipe C', 'unit'),
(15, 7, 'Tipe D', 'unit'),
(16, 8, 'Rumah Sakit Bersalin', 'unit'),
(17, 8, 'Rumah Sakit Jiwa', 'unit'),
(18, 8, 'Rumah Sakit Ketergantungan Obat', 'unit'),
(19, 8, 'Rumah Sakit Kusta\r\n', 'unit'),
(20, 8, 'Rumah Sakit Mata', 'unit'),
(21, 8, 'Rumah Sakit Bedah', 'unit'),
(22, 8, 'Rumah Sakit Jantung', 'unit'),
(23, 8, 'Rumah Sakit Paru', 'unit'),
(25, 8, 'Rumah Sakit Umum Pusat (RSUP)', 'unit'),
(26, 9, 'Rumah Sakit Angkatan Darat', 'unit'),
(27, 9, 'Rumah Sakit Angkatan Udara', 'unit'),
(28, 9, 'Rumah Sakit Angkatan Laut', 'unit'),
(29, 9, 'Rumah Sakit POLRI', 'unit'),
(30, 10, 'Leding (Perpipaan)', 'RT'),
(31, 10, 'Sumur Lindung', 'RT'),
(32, 10, 'Sumur Tidak Terlindungi\r\n', 'RT'),
(33, 10, 'Mata Air Terlindungi', 'RT'),
(34, 10, 'Mata Air Tidak Terlindungi', 'RT'),
(35, 10, 'Danau/Waduk', 'RT'),
(36, 10, 'Air Hujan', 'RT'),
(37, 10, 'Air Kemasan', 'RT'),
(38, 22, 'Jumlah Seluruh Balita Kurang Gizi', 'Balita'),
(39, 22, 'Jumlah Anak Balita 0-3 Tahun', 'Balita'),
(40, 22, 'Jumlah Balita Kurang Gizi Buruk', 'Balita'),
(41, 22, 'Jumlah Seluruh Balita Gizi Buruk', 'Balita'),
(42, 22, 'Jumlah balita Gizi Buruk Yang Mendapat Perawatan', 'Balita'),
(43, 23, 'Menurut Wabah Demam Berdarah', 'Jiwa'),
(44, 23, 'Menurut Wabah Muntaber', 'Jiwa'),
(45, 23, 'Menurut Wabah Infeksi Saluran Pernafasan', 'Jiwa'),
(46, 23, 'Menurut wabah Campak\r\n', 'Jiwa'),
(47, 23, 'Menurut Wabah Malaria', 'Jiwa'),
(48, 23, 'Menurut Wabah Lainnya', 'Jiwa'),
(49, 25, 'Laki-laki', 'Jiwa'),
(50, 25, 'Perempuan', 'Jiwa');

-- --------------------------------------------------------

--
-- Table structure for table `lp_keu`
--

CREATE TABLE `lp_keu` (
  `id_keu` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_keu`
--

INSERT INTO `lp_keu` (`id_keu`, `id_jenis`, `id_sub_jenis`, `th`, `jml`) VALUES
(2, 1, 2, 2014, '278885189.55'),
(3, 1, 2, 2015, '316682891.17'),
(4, 1, 2, 2016, '374641673.42'),
(5, 1, 3, 2014, '45557675.30'),
(6, 1, 3, 2015, '35281817.93'),
(7, 1, 3, 2016, '42782439.06'),
(8, 1, 4, 2014, '13385924.50'),
(9, 1, 4, 2015, '14649144.87'),
(10, 1, 4, 2016, '15785980.80'),
(11, 1, 5, 2014, '34716606.94'),
(12, 1, 5, 2015, '58324901.55'),
(13, 1, 5, 2016, '44122562.56'),
(14, 2, 6, 2014, '66740371.27'),
(15, 2, 6, 2015, '33850624.00'),
(16, 2, 6, 2016, '69368351.04'),
(17, 2, 7, 2014, '50203519.87'),
(18, 2, 7, 2015, '53164497.78'),
(19, 2, 7, 2016, '45506060.94'),
(20, 2, 8, 2014, '808447825.00'),
(21, 2, 8, 2015, '818758893.00'),
(22, 2, 8, 2016, '859678208.00'),
(23, 2, 9, 2014, '31304060.00'),
(24, 2, 9, 2015, '20590560.00'),
(25, 2, 9, 2016, '94813827.00'),
(26, 3, 10, 2014, '19023000.00'),
(27, 3, 10, 2015, '14781000.00'),
(28, 3, 10, 2016, '12606000.00'),
(29, 3, 12, 2014, '154505481.23'),
(30, 3, 12, 2015, '152348658.41'),
(31, 3, 12, 2016, '171619907.27'),
(32, 3, 13, 2014, '217906979.00'),
(33, 3, 13, 2015, '224102748.00'),
(34, 3, 13, 2016, '5000000.00'),
(35, 3, 14, 2014, '44188057.00'),
(36, 3, 14, 2015, '86536953.00'),
(37, 3, 14, 2016, '5260340.00'),
(38, 4, 16, 2014, '798826075.31'),
(39, 4, 16, 2015, '825456394.83'),
(40, 4, 16, 2016, '862559055.66'),
(41, 4, 19, 2014, '44148137.50'),
(42, 4, 19, 2015, '66492877.50'),
(43, 4, 19, 2016, '57425620.00'),
(44, 4, 20, 2014, '1402739.63'),
(45, 4, 20, 2015, '397500.00'),
(46, 4, 21, 2014, '78330.60'),
(47, 4, 21, 2015, '75036.60'),
(48, 4, 21, 2016, '78766.20'),
(49, 4, 22, 2014, '647332.84'),
(50, 4, 22, 2015, '635264.96'),
(51, 4, 22, 2016, '635264396.00'),
(52, 4, 23, 2014, '13261.35'),
(53, 4, 23, 2015, '657238.60'),
(54, 4, 23, 2016, '331951.13'),
(55, 5, 24, 2014, '78420319.40'),
(56, 5, 24, 2015, '105634472.97'),
(57, 5, 24, 2016, '131033864.34'),
(58, 5, 25, 2014, '361001601.09'),
(59, 5, 25, 2015, '466424196.49'),
(60, 5, 25, 2016, '464206828.40'),
(61, 5, 26, 2014, '318462052.42'),
(62, 5, 26, 2015, '337647558.97'),
(63, 5, 26, 2016, '193646732.35');

-- --------------------------------------------------------

--
-- Table structure for table `lp_keu_jenis`
--

CREATE TABLE `lp_keu_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL,
  `kategori` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_keu_jenis`
--

INSERT INTO `lp_keu_jenis` (`id_jenis`, `nama_jenis`, `kategori`) VALUES
(1, 'pendapatan asli daerah', '0'),
(2, 'dana perimbangan', '0'),
(3, 'lain-lain pendapatan yang sah', '0'),
(4, 'Belanja Tidak Langsung', '1'),
(5, 'Belanja Langsung', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lp_keu_sub_jenis`
--

CREATE TABLE `lp_keu_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_keu_sub_jenis`
--

INSERT INTO `lp_keu_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`) VALUES
(2, 1, 'pajak daerah'),
(3, 1, 'Retribusi Daerah'),
(4, 1, 'Hasil Perusahaan Milik Daerah dan Pengelolaan Kekayaan Daerah yang di Pisahkan'),
(5, 1, 'Lain-lain PAD yang sah'),
(6, 2, 'Bagi Hasil Pajak'),
(7, 2, 'Bagi Hasil Bukan Pajak/ Sumber Daya Alam'),
(8, 2, 'Dana Alokasi Umum'),
(9, 2, 'Dana Alokasi Khusus'),
(10, 3, 'Pendapatan hibah'),
(11, 3, 'Dana Darurat'),
(12, 3, 'Dana Bagi Hasil Pajak dari Provinsi dan Pemerintah Daerah Lainnya'),
(13, 3, 'Dana Penyesuaian dan Otonomi Daerah'),
(14, 3, 'Bantuan Keuangan dari Provinsi atau Pemerintah Daerah Lainnya'),
(15, 3, 'Lainnya'),
(16, 4, 'Belanja Pegawai '),
(17, 4, 'Belanja Bunga'),
(18, 4, 'Belanja Subsidi'),
(19, 4, 'Belanja Hibah'),
(20, 4, 'Belanja Bantuan Sosial'),
(21, 4, 'Belanja Bagi Hasil Kepada Provinsi/Kabupaten/ Kota'),
(22, 4, 'Belanja Bantuan Keuangan '),
(23, 4, 'Belanja Tidak Terduga'),
(24, 5, 'Belanja Pegawai'),
(25, 5, 'Belanja Barang dan Jasa'),
(26, 5, 'Belanja Modal');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kooperasi_jenis`
--

CREATE TABLE `lp_kooperasi_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kooperasi_jenis`
--

INSERT INTO `lp_kooperasi_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Koperasi'),
(2, 'Jumlah Pengusaha yang Bergabung Dalam Koperasi'),
(3, 'Mengadakan Rapat Tahunan (unit)'),
(4, 'Jumlah Koperasi yang Tidak Mengadakan Rapat Tahunan (unit)'),
(5, 'Jumlah Koperasi yang Dinyatakan Kolaps (unit)'),
(6, 'Jumlah Uang Yang Beredar pada Koperasi di Kota Malang(rupiah)'),
(7, 'Jumlah Koperasi Yang Mendapat Penghargaan (unit)'),
(8, 'Jumlah Anggota (orang)'),
(9, 'Jumlah Aset (rupiah)'),
(10, 'Sisa Hasil Usaha (rupiah)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_kooperasi_sub_jenis`
--

CREATE TABLE `lp_kooperasi_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_kooperasi_sub_jenis`
--

INSERT INTO `lp_kooperasi_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`) VALUES
(1, 1, 'Jumlah Koperasi Aktif (unit)'),
(2, 1, 'Jumlah Koperasi Tidak Aktif (unit)'),
(3, 1, 'Jumlah Induk Koperasi (unit)'),
(4, 1, 'Jumlah Koperasi Primer (unit)'),
(5, 1, 'Jumlah KUD (unit)'),
(6, 1, 'Jumlah Non KUD (unit)'),
(7, 2, 'Pengusaha Kecil (orang)'),
(8, 2, 'Pengusaha Menengah (orang)'),
(9, 2, 'Pengusaha Besar (orang)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_koperasi`
--

CREATE TABLE `lp_koperasi` (
  `id_koperasi` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_koperasi`
--

INSERT INTO `lp_koperasi` (`id_koperasi`, `id_jenis`, `id_sub_jenis`, `th`, `jml`) VALUES
(1, 1, 1, 2014, '395'),
(2, 1, 1, 2015, '415'),
(3, 1, 1, 2016, '549'),
(4, 1, 2, 2014, '366'),
(5, 1, 2, 2015, '360'),
(6, 1, 2, 2016, '169'),
(7, 1, 3, 2014, '7'),
(8, 1, 3, 2015, '6'),
(9, 1, 3, 2016, '6'),
(10, 1, 4, 2014, '759'),
(11, 1, 4, 2015, '22'),
(12, 1, 4, 2016, '22'),
(13, 1, 5, 2014, '4'),
(14, 1, 5, 2015, '4'),
(15, 1, 5, 2016, '4'),
(16, 1, 6, 2014, '0'),
(17, 1, 6, 2015, '0'),
(18, 1, 6, 2016, '0'),
(19, 2, 7, 2014, '415'),
(20, 2, 7, 2015, '0'),
(21, 2, 7, 2016, '0'),
(22, 3, 0, 2014, '305'),
(23, 3, 0, 2015, '313'),
(24, 3, 0, 2016, '301'),
(25, 4, 0, 2014, '462'),
(26, 4, 0, 2015, '462'),
(27, 4, 0, 2016, '417'),
(30, 5, 0, 2014, '99'),
(31, 5, 0, 2015, '17'),
(32, 5, 0, 2016, '12'),
(33, 6, 0, 2014, '850920000'),
(34, 6, 0, 2015, '892621300000'),
(35, 6, 0, 2016, '921001130000'),
(36, 7, 0, 2014, '3'),
(37, 7, 0, 2015, '3'),
(38, 7, 0, 2016, '0'),
(39, 8, 0, 2014, '72593'),
(40, 8, 0, 2015, '77966'),
(41, 8, 0, 2016, '92666'),
(42, 9, 0, 2014, '851513370000'),
(43, 9, 0, 2015, '857600232000'),
(44, 9, 0, 2016, '1008552778000'),
(45, 10, 0, 2014, '32960420000'),
(46, 10, 0, 2015, '32816020000'),
(47, 10, 0, 2016, '50686769443'),
(48, 1, 1, 2017, '573'),
(49, 1, 2, 2017, '169'),
(50, 1, 3, 2017, '6'),
(51, 1, 4, 2017, '22'),
(52, 1, 5, 2017, '4'),
(53, 1, 6, 2017, '0'),
(54, 2, 7, 2017, '0'),
(55, 3, 0, 2017, '317'),
(56, 4, 0, 2017, '425'),
(57, 5, 0, 2017, '15'),
(58, 6, 0, 2017, '922003311000'),
(59, 7, 0, 2017, '3'),
(60, 8, 0, 2017, '91749'),
(61, 9, 0, 2017, '1492128419000'),
(62, 10, 0, 2017, '62682678000');

-- --------------------------------------------------------

--
-- Table structure for table `lp_lahan`
--

CREATE TABLE `lp_lahan` (
  `id_lahan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_lahan`
--

INSERT INTO `lp_lahan` (`id_lahan`, `id_jenis`, `id_kec`, `th`, `jml`) VALUES
(1, 1, 'KEC1001', 2015, '591'),
(2, 1, 'KEC1001', 2016, '591'),
(3, 2, 'KEC1001', 2015, '1271'),
(4, 2, 'KEC1001', 2016, '1271'),
(5, 3, 'KEC1001', 2015, '2127'),
(6, 3, 'KEC1001', 2016, '2127'),
(7, 4, 'KEC1001', 2015, '3989'),
(8, 4, 'KEC1001', 2016, '3989'),
(9, 1, 'KEC1002', 2015, '251'),
(10, 1, 'KEC1002', 2016, '226'),
(11, 2, 'KEC1002', 2015, '710'),
(12, 2, 'KEC1002', 2016, '704'),
(13, 3, 'KEC1002', 2015, '1136'),
(14, 3, 'KEC1002', 2016, '1167'),
(15, 4, 'KEC1002', 2015, '2097'),
(16, 4, 'KEC1002', 2016, '2097'),
(17, 2, 'KEC1003', 2015, '8'),
(18, 2, 'KEC1003', 2016, '6'),
(19, 3, 'KEC1003', 2015, '875'),
(20, 3, 'KEC1003', 2016, '877'),
(21, 4, 'KEC1003', 2015, '883'),
(22, 4, 'KEC1003', 2016, '883'),
(23, 1, 'KEC1004', 2015, '87'),
(24, 1, 'KEC1004', 2016, '85'),
(25, 2, 'KEC1004', 2015, '6'),
(26, 2, 'KEC1004', 2016, '6'),
(27, 3, 'KEC1004', 2015, '1684'),
(28, 3, 'KEC1004', 2016, '1686'),
(29, 4, 'KEC1004', 2015, '1777'),
(30, 4, 'KEC1004', 2016, '1777'),
(31, 1, 'KEC1005', 2015, '241'),
(32, 1, 'KEC1005', 2016, '240'),
(33, 2, 'KEC1005', 2015, '87'),
(34, 2, 'KEC1005', 2016, '88'),
(35, 3, 'KEC1005', 2015, '1932'),
(36, 3, 'KEC1005', 2016, '1932'),
(37, 4, 'KEC1005', 2015, '2260'),
(38, 4, 'KEC1005', 2016, '2260'),
(39, 1, 'KEC1003', 2015, '0'),
(40, 1, 'KEC1003', 2016, '0');

-- --------------------------------------------------------

--
-- Table structure for table `lp_lahan_jenis`
--

CREATE TABLE `lp_lahan_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_lahan_jenis`
--

INSERT INTO `lp_lahan_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Sawah'),
(2, 'Pertanian Bukan Sawah'),
(3, 'Bukan Pertanian'),
(4, 'Total Luas Lahan');

-- --------------------------------------------------------

--
-- Table structure for table `lp_miskin`
--

CREATE TABLE `lp_miskin` (
  `id_miskin` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_miskin`
--

INSERT INTO `lp_miskin` (`id_miskin`, `id_jenis`, `th`, `jml`) VALUES
(1, 1, 2014, '40.64'),
(2, 1, 2015, '39.1'),
(3, 1, 2016, '37.03'),
(4, 2, 2014, '4.80'),
(5, 2, 2015, '4.6'),
(6, 2, 2016, '4.33'),
(7, 3, 2014, '0.61 '),
(8, 3, 2015, '0.53'),
(9, 3, 2016, '0.54'),
(10, 4, 2014, '0.12'),
(11, 4, 2015, '0.11'),
(12, 4, 2016, '0.09'),
(13, 5, 2014, '381400'),
(14, 5, 2015, '411709'),
(15, 5, 2016, '426527');

-- --------------------------------------------------------

--
-- Table structure for table `lp_miskin_jenis`
--

CREATE TABLE `lp_miskin_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_miskin_jenis`
--

INSERT INTO `lp_miskin_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Jumlah Penduduk Miskin (000)'),
(2, 'Persentase Penduduk Miskin (Po)'),
(3, 'Indeks Kedalaman Kemiskinan (P1)'),
(4, 'Indeks Keparahan Kemiskinan (P2)'),
(5, 'Garis Kemiskinan (Rp/Kab/Bulan)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_or`
--

CREATE TABLE `lp_or` (
  `id_or` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_or`
--

INSERT INTO `lp_or` (`id_or`, `id_jenis`, `th`, `jml`) VALUES
(1, 1, 2014, 43),
(2, 1, 2015, 43),
(3, 1, 2016, 44),
(4, 2, 2014, 201),
(5, 2, 2015, 201),
(6, 2, 2016, 201),
(7, 3, 2014, 74),
(8, 3, 2015, 74),
(9, 3, 2016, 74),
(10, 4, 2014, 55),
(11, 4, 2015, 153),
(12, 4, 2016, 220),
(13, 5, 2014, 22),
(14, 5, 2015, 104),
(15, 5, 2016, 62),
(16, 6, 2014, 23),
(17, 6, 2015, 106),
(18, 6, 2016, 101),
(19, 7, 2014, 20),
(20, 7, 2015, 78),
(21, 7, 2016, 102),
(22, 8, 2014, 12),
(23, 8, 2015, 89),
(24, 8, 2016, 118),
(25, 9, 2014, 23),
(26, 9, 2015, 106),
(27, 9, 2016, 101),
(28, 10, 2014, 20),
(29, 10, 2015, 78),
(30, 10, 2016, 102),
(31, 11, 2014, 12),
(32, 11, 2015, 89),
(33, 11, 2016, 118),
(34, 12, 2014, 174),
(35, 12, 2015, 180),
(36, 12, 2016, 185),
(37, 13, 2014, 52),
(38, 13, 2015, 50),
(39, 13, 2016, 51),
(40, 14, 2014, 164),
(41, 14, 2015, 164),
(42, 14, 2016, 164),
(43, 15, 2014, 6),
(44, 15, 2015, 9),
(45, 15, 2016, 9),
(46, 16, 2014, 42),
(47, 16, 2015, 40),
(48, 16, 2016, 40),
(49, 17, 2014, 3),
(50, 17, 2015, 2),
(51, 17, 2016, 1),
(52, 18, 2014, 1),
(53, 18, 2015, 4),
(54, 18, 2016, 6),
(55, 12, 2017, 185),
(56, 13, 2017, 51),
(57, 14, 2017, 164),
(58, 15, 2017, 9),
(59, 16, 2017, 40),
(60, 17, 2017, 1),
(61, 18, 2017, 4);

-- --------------------------------------------------------

--
-- Table structure for table `lp_or_jenis`
--

CREATE TABLE `lp_or_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL,
  `kategori` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_or_jenis`
--

INSERT INTO `lp_or_jenis` (`id_jenis`, `nama_jenis`, `kategori`) VALUES
(1, 'Jumlah cabang Olahraga yang mendapat pembinaan Koni', '0'),
(2, 'Jumlah Club Olahraga di bawah Pembinaan Koni', '0'),
(3, 'Jumlah Sarana dan Prasarana Olahraga\r\n', '0'),
(4, 'Jumlah Prestasi yang didapat\r\ndari ajang olahraga tingkat Jawa\r\nTimur', '0'),
(5, 'Jumlah Prestasi yang Didapat dari ajang olahraga tingkat Jawa Nasional\r\n', '0'),
(6, 'Jumlah atlet yang mendapatkan medali emas\r\n', '0'),
(7, 'Jumlah atlit yang mendapatkan medali perak\r\n', '0'),
(8, 'Jumlah atlit yang mendapatkan medali perunggu\r\n', '0'),
(9, 'Jumlah klub olahraga yang menerima medali emas\r\n', '0'),
(10, 'Jumlah klub olahraga yang menerima medali perak ', '0'),
(11, 'Jumlah klub olahraga yang menerima medali perunggu', '0'),
(12, 'Jumlah Klub Olahraga (Klub)\r\n', '1'),
(13, 'Jumlah Sarana dan Prasarana Olahraga (Unit) ', '1'),
(14, 'Organisasi Kemasyarakatan Pemuda (Buah) ', '1'),
(15, 'Kegiatan Kepemudaan (Kegiatan)', '1'),
(16, 'Organisasi Olahraga (Buah) ', '1'),
(17, 'Kegiatan Olahraga Tradisional (Kegiatan)', '1'),
(18, 'Kegiatan Olahraga Prestasi (Kegiatan) ', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pajak`
--

CREATE TABLE `lp_pajak` (
  `id_pajak` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(11) NOT NULL,
  `jml` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pajak`
--

INSERT INTO `lp_pajak` (`id_pajak`, `id_jenis`, `th`, `jml`) VALUES
(1, 1, 2014, '17169937868.68 '),
(2, 1, 2015, '22131094351.27'),
(3, 1, 2016, '27680570200.00'),
(4, 2, 2014, '28476534584.04'),
(5, 2, 2015, '28476534584.04'),
(6, 2, 2016, '34976534500.00'),
(7, 3, 2014, '4542595922.35'),
(8, 3, 2015, '4943004 465.24'),
(9, 3, 2016, '5543000 000.00'),
(10, 4, 2014, '156401433942.58'),
(11, 4, 2015, '18676522723.99'),
(12, 4, 2016, '18670522800.00'),
(13, 5, 2014, '37602101561.92'),
(14, 5, 2015, '40602101561.92'),
(15, 5, 2016, '44602106500.00'),
(16, 6, 2014, '-'),
(17, 6, 2015, '-'),
(18, 6, 2016, '-'),
(19, 7, 2014, '1947997758.99'),
(20, 7, 2015, '2501998407.60'),
(21, 7, 2016, '3501998000.00'),
(22, 8, 2014, '701130420.40'),
(23, 8, 2015, '749475964.90'),
(24, 8, 2016, '600000000.00'),
(25, 9, 2014, '-'),
(26, 9, 2015, '-'),
(27, 9, 2016, '-'),
(28, 10, 2014, '53869267940.29'),
(29, 10, 2015, '53869267940.29'),
(30, 10, 2016, '56869258000.00'),
(31, 11, 2014, '100050000000.75'),
(32, 11, 2015, '100050000000.75'),
(33, 11, 2016, '108550000000.00'),
(34, 12, 2014, '-'),
(35, 12, 2015, '-'),
(36, 12, 2016, '-'),
(37, 13, 2014, '-'),
(38, 13, 2015, '-'),
(39, 13, 2016, '-'),
(40, 1, 2017, '37180570300.00'),
(41, 1, 2018, '38000000000.00'),
(42, 2, 2017, '44976534500.00'),
(43, 2, 2018, '48000000000.00'),
(44, 3, 2017, '6293000000.00'),
(45, 3, 2018, '7000000000.00'),
(46, 4, 2017, '18176522700.00'),
(47, 4, 2018, '19000000000.00'),
(48, 5, 2017, '48102106500.00'),
(49, 5, 2018, '54000000000.00'),
(50, 6, 2017, '0'),
(51, 6, 2018, '0'),
(52, 7, 2017, '4501996000.00'),
(53, 7, 2018, '4750000000.00'),
(54, 8, 2017, '600000000.00'),
(55, 8, 2018, '800000000.00'),
(56, 9, 2017, '0'),
(57, 9, 2018, '0'),
(58, 10, 2017, '56869268000.00'),
(59, 10, 2018, '57000000000.00'),
(60, 11, 2017, '135800000000.00'),
(61, 11, 2018, '146450000000.00'),
(62, 12, 2017, '0'),
(63, 12, 2018, '0'),
(64, 13, 2017, '0'),
(65, 13, 2018, '0');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pajak_jenis`
--

CREATE TABLE `lp_pajak_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pajak_jenis`
--

INSERT INTO `lp_pajak_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Pajak Hotel'),
(2, 'Pajak Restoran'),
(3, 'Pajak Hiburan'),
(4, 'Pajak Reklame'),
(5, 'Pajak Penerangan Jalan '),
(6, 'Pajak Mineral Bukan Logam dan Batuan'),
(7, 'Pajak Parkir'),
(8, 'Pajak Air Tanah'),
(9, 'Pajak Sarang Burung Walet'),
(10, 'Pajak Bumi dan Bangunan Perdesaan dan Perkotaan (PBB-P2)'),
(11, 'Bea Perolehan Hak atas Tanah dan Bangunan (BPHTP)'),
(12, 'Jumlah Pajak yang Dikeluarkan'),
(13, 'Jumlah Insentif Pajak yang Mendukung Iklim Investasi');

-- --------------------------------------------------------

--
-- Table structure for table `lp_panen`
--

CREATE TABLE `lp_panen` (
  `id_panen` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `luas_panen` varchar(20) NOT NULL,
  `jml_panen` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_panen`
--

INSERT INTO `lp_panen` (`id_panen`, `id_jenis`, `id_kec`, `th`, `luas_panen`, `jml_panen`) VALUES
(5, 1, 'KEC1001', 2014, '666', '4065'),
(6, 1, 'KEC1001', 2015, '606', '4139'),
(7, 1, 'KEC1002', 2014, '556', '3671'),
(8, 1, 'KEC1002', 2015, '645', '4658'),
(9, 1, 'KEC1003', 2014, '0', '0'),
(10, 1, 'KEC1003', 2015, '0', '0'),
(11, 1, 'KEC1001', 2016, '628', '4442'),
(12, 1, 'KEC1002', 2016, '636', '4676'),
(13, 1, 'KEC1003', 2016, '0', '0'),
(14, 1, 'KEC1004', 2014, '253', '1689'),
(15, 1, 'KEC1004', 2015, '201', '1460'),
(16, 1, 'KEC1004', 2016, '191', '1312'),
(17, 1, 'KEC1005', 2014, '509', '3754'),
(18, 1, 'KEC1005', 2015, '525', '4008'),
(19, 1, 'KEC1005', 2016, '545', '4043'),
(20, 2, 'KEC1001', 2014, '145', '691'),
(21, 2, 'KEC1001', 2015, '126', '566'),
(22, 2, 'KEC1001', 2016, '79', '519'),
(23, 2, 'KEC1002', 2014, '10', '44'),
(24, 2, 'KEC1002', 2015, '10', '45'),
(25, 2, 'KEC1002', 2016, '10', '45'),
(26, 2, 'KEC1003', 2014, '0', '0'),
(27, 2, 'KEC1003', 2015, '0', '0'),
(28, 2, 'KEC1003', 2016, '0', '0'),
(29, 2, 'KEC1004', 2014, '0', '0'),
(30, 2, 'KEC1004', 2015, '1', '4'),
(31, 2, 'KEC1004', 2016, '0', '0'),
(32, 2, 'KEC1005', 2014, '8', '33'),
(33, 2, 'KEC1005', 2015, '14', '63'),
(34, 2, 'KEC1005', 2016, '7', '41'),
(35, 3, 'KEC1001', 2014, '10', '36'),
(36, 3, 'KEC1001', 2015, '3', '11'),
(37, 3, 'KEC1001', 2016, '0', '0'),
(38, 3, 'KEC1002', 2014, '4', '14'),
(39, 3, 'KEC1002', 2015, '4', '14'),
(42, 3, 'KEC1002', 2016, '3', '11'),
(43, 3, 'KEC1003', 2014, '0', '0'),
(44, 3, 'KEC1003', 2015, '0', '0'),
(45, 3, 'KEC1003', 2016, '0', '0'),
(46, 3, 'KEC1004', 2014, '0', '0'),
(47, 3, 'KEC1004', 2015, '0', '0'),
(48, 3, 'KEC1004', 2016, '0', '0'),
(49, 3, 'KEC1005', 2014, '0', '0'),
(50, 3, 'KEC1005', 2015, '2', '7'),
(51, 3, 'KEC1005', 2016, '1', '4'),
(52, 4, 'KEC1001', 2014, '119', '2420'),
(53, 4, 'KEC1001', 2015, '78', '2128'),
(54, 4, 'KEC1001', 2016, '73', '2978'),
(55, 4, 'KEC1002', 2014, '6', '122'),
(56, 4, 'KEC1002', 2015, '11', '308'),
(57, 4, 'KEC1002', 2016, '4', '202'),
(58, 4, 'KEC1003', 2014, '0', '0'),
(59, 4, 'KEC1003', 2015, '0', '0'),
(60, 4, 'KEC1003', 2016, '0', '0'),
(61, 4, 'KEC1004', 2014, '0', '0'),
(62, 4, 'KEC1004', 2015, '0', '0'),
(63, 4, 'KEC1004', 2016, '0', '0'),
(64, 4, 'KEC1005', 2014, '5', '102'),
(65, 4, 'KEC1005', 2015, '2', '40'),
(66, 4, 'KEC1005', 2016, '2', '101'),
(67, 5, 'KEC1001', 2014, '1', '22'),
(68, 5, 'KEC1001', 2015, '0', '0'),
(69, 5, 'KEC1001', 2016, '0', '0'),
(70, 5, 'KEC1002', 2014, '0', '0'),
(71, 5, 'KEC1002', 2015, '0', '0'),
(72, 5, 'KEC1002', 2016, '0', '0'),
(73, 5, 'KEC1003', 2014, '0', '0'),
(74, 5, 'KEC1003', 2015, '0', '0'),
(75, 3, 'KEC1003', 2016, '0', '0'),
(76, 5, 'KEC1004', 2014, '0', '0'),
(77, 5, 'KEC1004', 2015, '0', '0'),
(78, 5, 'KEC1004', 2016, '0', '0'),
(79, 5, 'KEC1005', 2014, '0', '0'),
(80, 5, 'KEC1005', 2015, '0', '0'),
(81, 5, 'KEC1005', 2016, '1', '19');

-- --------------------------------------------------------

--
-- Table structure for table `lp_panen_jenis`
--

CREATE TABLE `lp_panen_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_panen_jenis`
--

INSERT INTO `lp_panen_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'padisawah'),
(2, 'jagung'),
(3, 'kacangtanah'),
(4, 'ubikayu'),
(5, 'ubijalar');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pdb`
--

CREATE TABLE `lp_pdb` (
  `id_pdb` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pdb`
--

INSERT INTO `lp_pdb` (`id_pdb`, `id_jenis`, `id_kategori`, `th`, `jml`) VALUES
(2, 1, 2, 2014, '142.7'),
(3, 1, 2, 2015, '157.5'),
(4, 1, 2, 2016, '164.3'),
(5, 1, 3, 2014, '50.3'),
(6, 1, 3, 2015, '51.7'),
(7, 1, 3, 2016, '49.8'),
(8, 1, 4, 2014, '12637.7'),
(9, 1, 4, 2015, '13734.3'),
(10, 1, 4, 2016, '14521.8'),
(11, 1, 5, 2014, '13'),
(12, 1, 5, 2015, '14.5'),
(13, 1, 5, 2016, '17.5'),
(14, 1, 6, 2014, '91.2'),
(15, 1, 6, 2015, '97.1'),
(16, 1, 6, 2016, '106.8'),
(17, 1, 7, 2014, '5848.4'),
(18, 1, 7, 2015, '6496.5'),
(19, 1, 7, 2016, '7386.7'),
(20, 1, 8, 2014, '13257.1'),
(21, 1, 8, 2015, '14977.1'),
(22, 1, 8, 2016, '16890.3'),
(23, 1, 9, 2014, '1119.2'),
(24, 1, 9, 2015, '1250.6'),
(25, 1, 9, 2016, '1399.2'),
(26, 1, 10, 2014, '2271.3'),
(27, 1, 10, 2015, '2484.7'),
(28, 1, 10, 2016, '2802.7'),
(29, 1, 11, 2014, '1834.7'),
(30, 1, 11, 2015, '2057.3'),
(31, 1, 11, 2016, '2277.9'),
(32, 1, 12, 2014, '1359.6'),
(33, 1, 12, 2015, '1538.5'),
(34, 1, 12, 2016, '1740.5'),
(35, 1, 13, 2014, '633.6'),
(36, 1, 13, 2015, '729.6'),
(37, 1, 13, 2016, '808.2'),
(38, 1, 14, 2014, '348.6'),
(39, 1, 14, 2015, '399.5'),
(40, 1, 14, 2016, '447.7'),
(41, 1, 15, 2014, '733.6'),
(42, 1, 15, 2015, '788.6'),
(43, 1, 15, 2016, '844.5'),
(44, 1, 16, 2014, '3728.5'),
(45, 1, 16, 2015, '4224.5'),
(46, 1, 16, 2016, '4646.3'),
(47, 1, 17, 2014, '1135.6'),
(48, 1, 17, 2015, '1292'),
(49, 1, 17, 2016, '1428.7'),
(50, 1, 18, 2014, '1358'),
(51, 1, 18, 2015, '1534'),
(52, 1, 18, 2016, '1638.7'),
(53, 2, 2, 2014, '105.1'),
(54, 2, 2, 2015, '107.4'),
(55, 2, 2, 2016, '107.5'),
(56, 2, 3, 2014, '39.8'),
(57, 2, 3, 2015, '38.4'),
(58, 2, 3, 2016, '36.2'),
(59, 2, 4, 2014, '10011.8'),
(60, 2, 4, 2015, '10261.7'),
(61, 2, 4, 2016, '10463.3'),
(62, 2, 5, 2014, '15.5'),
(63, 2, 5, 2015, '15.5'),
(64, 2, 5, 2016, '16.8'),
(65, 2, 6, 2014, '83.9'),
(66, 2, 6, 2015, '87'),
(67, 2, 6, 2016, '91.3'),
(68, 2, 7, 2014, '4998.5'),
(69, 2, 7, 2015, '5263.4'),
(70, 2, 7, 2016, '5612.1'),
(71, 2, 8, 2014, '12221.5'),
(72, 2, 8, 2015, '13022.7'),
(73, 2, 8, 2016, '13844.8'),
(74, 2, 9, 2014, '977.5'),
(75, 2, 9, 2015, '1044.3'),
(76, 2, 9, 2016, '1122.3'),
(77, 2, 10, 2014, '1712'),
(78, 2, 10, 2015, '1851'),
(79, 2, 10, 2016, '1997.1'),
(80, 2, 11, 2014, '1843.1'),
(81, 2, 11, 2015, '1993.1'),
(82, 2, 11, 2016, '2174.2'),
(83, 2, 12, 2014, '1042.6'),
(84, 2, 12, 2015, '1117'),
(85, 2, 12, 2016, '1205.1'),
(86, 2, 13, 2014, '585.3'),
(87, 2, 13, 2015, '627.8'),
(88, 2, 13, 2016, '674.3'),
(89, 2, 14, 2014, '285.8'),
(90, 2, 14, 2015, '310.8'),
(91, 2, 14, 2016, '333'),
(92, 2, 15, 2014, '603.4'),
(93, 2, 15, 2015, '625.8'),
(94, 2, 15, 2016, '636.5'),
(95, 2, 16, 2014, '2957.3'),
(96, 2, 16, 2015, '3203.1'),
(97, 2, 16, 2016, '3456.8'),
(98, 2, 17, 2014, '967.8'),
(99, 2, 17, 2015, '1062.9'),
(100, 2, 17, 2016, '1152.1'),
(101, 2, 18, 2014, '1273.3'),
(102, 2, 18, 2015, '1319.6'),
(103, 2, 18, 2016, '1380.4'),
(104, 3, 2, 2014, '0.28'),
(105, 3, 2, 2015, '0.27'),
(106, 3, 2, 2016, '0.26'),
(107, 3, 3, 2014, '0.11'),
(108, 3, 3, 2015, '0.1'),
(109, 3, 3, 2016, '3.77'),
(110, 3, 4, 2014, '27.15'),
(111, 3, 4, 2015, '26.51'),
(112, 3, 4, 2016, '28.92'),
(113, 3, 5, 2014, '0.03'),
(114, 3, 5, 2015, '0.03'),
(115, 3, 5, 2016, '0.03'),
(116, 3, 6, 2014, '0.2'),
(117, 3, 6, 2015, '0.19'),
(118, 3, 6, 2016, '0.19'),
(119, 3, 7, 2014, '12.56'),
(120, 3, 7, 2015, '12.53'),
(121, 3, 7, 2016, '12.92'),
(122, 3, 8, 2014, '28.48'),
(123, 3, 8, 2015, '28.91'),
(124, 3, 8, 2016, '29.55'),
(125, 3, 9, 2014, '2.4'),
(126, 3, 9, 2015, '2.41'),
(127, 3, 9, 2016, '2.45'),
(128, 3, 10, 2014, '4.88'),
(129, 3, 10, 2015, '4.8'),
(130, 3, 10, 2016, '4.9'),
(131, 3, 11, 2014, '3.94'),
(132, 3, 11, 2015, '3.97'),
(133, 3, 11, 2016, '3.99'),
(134, 3, 12, 2014, '2.92'),
(135, 3, 12, 2015, '2.97'),
(136, 3, 12, 2016, '3.05'),
(137, 3, 13, 2014, '1.36'),
(138, 3, 13, 2015, '1.41'),
(139, 3, 13, 2016, '1.41'),
(140, 3, 14, 2014, '0.75'),
(141, 3, 14, 2015, '0.77'),
(142, 3, 14, 2016, '0.78'),
(143, 3, 15, 2014, '1.58'),
(144, 3, 15, 2015, '1.52'),
(145, 3, 15, 2016, '1.48'),
(146, 3, 16, 2014, '8.01'),
(147, 3, 16, 2015, '8.15'),
(148, 3, 16, 2016, '8.13'),
(149, 3, 17, 2014, '2.44'),
(150, 3, 17, 2015, '2.49'),
(151, 3, 17, 2016, '2.5'),
(152, 3, 18, 2014, '2.92'),
(153, 3, 18, 2015, '2.97'),
(154, 3, 18, 2016, '2.87'),
(155, 4, 2, 2014, '1.86'),
(156, 4, 2, 2015, '2.23'),
(157, 4, 2, 2016, '0.08'),
(158, 4, 3, 2014, '-1.87'),
(159, 4, 3, 2015, '-3.58'),
(160, 4, 3, 2016, '-5.58'),
(161, 4, 4, 2014, '2.81'),
(162, 4, 4, 2015, '2.51'),
(163, 4, 4, 2016, '1.95'),
(164, 4, 5, 2014, '4.23'),
(165, 4, 5, 2015, '-0.01'),
(166, 4, 5, 2016, '4.73'),
(167, 4, 6, 2014, '3.09'),
(168, 4, 6, 2015, '3.71'),
(169, 4, 6, 2016, '4.92'),
(170, 4, 7, 2014, '8.84'),
(171, 4, 7, 2015, '5.18'),
(172, 4, 7, 2016, '6.74'),
(173, 4, 8, 2014, '5.48'),
(174, 4, 8, 2015, '6.56'),
(175, 4, 8, 2016, '6.31'),
(176, 4, 9, 2014, '7.17'),
(177, 4, 9, 2015, '6.83'),
(178, 4, 9, 2016, '7.47'),
(179, 4, 10, 2014, '10.46'),
(180, 4, 10, 2015, '8.12'),
(181, 4, 10, 2016, '7.89'),
(182, 4, 11, 2014, '8.14'),
(183, 4, 11, 2015, '8.14'),
(184, 4, 11, 2016, '9.09'),
(185, 4, 12, 2014, '6.72'),
(186, 4, 12, 2015, '7.13'),
(187, 4, 12, 2016, '7.89'),
(188, 4, 13, 2014, '7.25'),
(189, 4, 13, 2015, '7.25'),
(190, 4, 13, 2016, '7.41'),
(191, 4, 14, 2014, '8.77'),
(192, 4, 14, 2015, '8.77'),
(193, 4, 14, 2016, '7.13'),
(194, 4, 15, 2014, '0.11'),
(195, 4, 15, 2015, '3.72'),
(196, 4, 15, 2016, '1.71'),
(197, 4, 16, 2014, '8.31'),
(198, 4, 16, 2015, '8.31'),
(199, 4, 16, 2016, '7.92'),
(200, 4, 17, 2014, '9.07'),
(201, 4, 17, 2015, '9.95'),
(202, 4, 17, 2016, '8.27'),
(203, 4, 18, 2014, '4.55'),
(204, 4, 18, 2015, '3.88'),
(205, 4, 18, 2016, '4.37');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pdb_jenis`
--

CREATE TABLE `lp_pdb_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL,
  `satuan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pdb_jenis`
--

INSERT INTO `lp_pdb_jenis` (`id_jenis`, `nama_jenis`, `satuan`) VALUES
(1, 'Produk Domestik Regional Bruto Atas Dasar Harga Berlaku Menurut Lapangan Usaha', 'miliar rupiah'),
(2, 'Produk Domestik Regional Bruto Atas Dasar Harga Konstan Menurut Lapangan Usaha ', 'miliar rupiah'),
(3, 'Distribusi Persentase Produk Domestik Regional Bruto Atas Dasar Harga Berlaku Menurut Lapangan Usaha', 'persen'),
(4, 'Laju pertumbuhan Produk Domestik Regional Bruto Atas Dasar Harga Konstan 2010 Menurut Lapangan Usaha', 'persen');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pdb_kategori`
--

CREATE TABLE `lp_pdb_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pdb_kategori`
--

INSERT INTO `lp_pdb_kategori` (`id_kategori`, `nama_kategori`) VALUES
(2, 'Pertanian, Kehutanan, dan Perikanan'),
(3, 'Pertambangan dan Penggalian'),
(4, 'Industri Pengolahan'),
(5, 'Pengadaan Listrik dan Gas'),
(6, 'Pengadaan Air, Pengelolaan Sampah, Limbah dan Daur Ulang'),
(7, 'Konstruksi'),
(8, 'Perdagangan Besar dan Eceran; Reparasi Mobil dan Sepeda Motor'),
(9, 'Transportasi dan Pergudangan'),
(10, 'Penyediaan Akomodasi dan Makan Minum'),
(11, 'Informasi dan Komunikasi'),
(12, 'Jasa Keuangan dan Asuransi'),
(13, 'Real Estate'),
(14, 'Jasa Perusahaan'),
(15, 'Administrasi Pemerintahan, Pertahanan dan Jaminan Sosial Wajib'),
(16, 'Jasa Pendidikan'),
(17, 'Jasa Kesehatan dan Kegiatan Sosial'),
(18, 'Jasa lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pendidikan`
--

CREATE TABLE `lp_pendidikan` (
  `id_pendidikan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_negeri` varchar(10) NOT NULL,
  `jml_swasta` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pendidikan`
--

INSERT INTO `lp_pendidikan` (`id_pendidikan`, `id_jenis`, `id_kategori`, `id_kec`, `th`, `jml_negeri`, `jml_swasta`) VALUES
(2, 1, 1, 'KEC1001', 2014, '2', '55'),
(3, 1, 1, 'KEC1001', 2015, '2', '61'),
(4, 1, 1, 'KEC1001', 2016, '2', '63'),
(5, 1, 1, 'KEC1002', 2014, '1', '67'),
(6, 1, 1, 'KEC1002', 2015, '1', '68'),
(7, 1, 1, 'KEC1002', 2016, '1', '68'),
(8, 1, 1, 'KEC1003', 2014, '1', '69'),
(9, 1, 1, 'KEC1003', 2015, '1', '72'),
(10, 1, 1, 'KEC1003', 2016, '1', '64'),
(11, 1, 1, 'KEC1004', 2014, '1', '64'),
(12, 1, 1, 'KEC1004', 2015, '1', '65'),
(13, 1, 1, 'KEC1004', 2016, '1', '66'),
(14, 1, 1, 'KEC1005', 2014, '0', '73'),
(15, 1, 1, 'KEC1005', 2015, '0', '73'),
(16, 1, 1, 'KEC1005', 2016, '0', '75'),
(17, 2, 1, 'KEC1001', 2014, '98', '4111'),
(18, 2, 1, 'KEC1001', 2015, '51', '3853'),
(19, 2, 1, 'KEC1001', 2016, '176', '4103'),
(20, 2, 1, 'KEC1002', 2014, '17', '3454'),
(21, 2, 1, 'KEC1002', 2015, '100', '4313'),
(22, 2, 1, 'KEC1002', 2016, '85', '4283'),
(23, 2, 1, 'KEC1003', 2014, '98', '3798'),
(24, 2, 1, 'KEC1003', 2015, '180', '4199'),
(25, 2, 1, 'KEC1003', 2016, '165', '4059'),
(26, 2, 1, 'KEC1004', 2014, '47', '3770'),
(27, 2, 1, 'KEC1004', 2015, '131', '3895'),
(28, 2, 1, 'KEC1004', 2016, '148', '3974'),
(29, 2, 1, 'KEC1005', 2014, '0', '4015'),
(30, 2, 1, 'KEC1005', 2015, '0', '3831'),
(31, 2, 1, 'KEC1005', 2016, '0', '3658'),
(32, 3, 1, 'KEC1001', 2014, '14', '261'),
(33, 3, 1, 'KEC1001', 2015, '4', '283'),
(34, 3, 1, 'KEC1001', 2016, '15', '536'),
(35, 3, 1, 'KEC1002', 2014, '5', '295'),
(36, 3, 1, 'KEC1002', 2015, '7', '307'),
(37, 3, 1, 'KEC1002', 2016, '6', '574'),
(38, 3, 1, 'KEC1003', 2014, '10', '301'),
(39, 3, 1, 'KEC1003', 2015, '14', '299'),
(40, 3, 1, 'KEC1003', 2016, '12', '523'),
(41, 3, 1, 'KEC1004', 2014, '5', '313'),
(42, 3, 1, 'KEC1004', 2015, '8', '327'),
(43, 3, 1, 'KEC1004', 2016, '9', '574'),
(44, 3, 1, 'KEC1005', 2014, '0', '319'),
(45, 3, 1, 'KEC1005', 2015, '0', '309'),
(46, 3, 1, 'KEC1005', 2016, '0', '578'),
(47, 4, 1, 'KEC1001', 2014, '7.00', '15.75'),
(48, 4, 1, 'KEC1001', 2015, '12.75', '13.61'),
(49, 4, 1, 'KEC1001', 2016, '11.73', '7.65'),
(50, 4, 1, 'KEC1002', 2014, '3.40', '11.71'),
(51, 4, 1, 'KEC1002', 2015, '14.29', '14.05'),
(52, 4, 1, 'KEC1002', 2016, '14.17', '7.46'),
(53, 4, 1, 'KEC1003', 2014, '9.80', '12.62'),
(54, 4, 1, 'KEC1003', 2015, '12.86', '14.04'),
(55, 4, 1, 'KEC1003', 2016, '13.75', '7.76'),
(56, 4, 1, 'KEC1004', 2014, '9.40', '16.44'),
(57, 4, 1, 'KEC1004', 2015, '16.38', '11.91'),
(58, 4, 1, 'KEC1004', 2016, '16.44', '6.92'),
(59, 4, 1, 'KEC1005', 2014, '0', '12.59'),
(60, 4, 1, 'KEC1005', 2015, '0', '12.40'),
(61, 4, 1, 'KEC1005', 2016, '0.00', '6.33'),
(62, 1, 2, 'KEC1001', 2014, '0', '1'),
(63, 1, 2, 'KEC1001', 2015, '0', '0'),
(64, 1, 2, 'KEC1001', 2016, '0', '1'),
(65, 1, 2, 'KEC1002', 2014, '0', '1'),
(66, 1, 2, 'KEC1002', 2015, '0', '1'),
(67, 1, 2, 'KEC1002', 2016, '0', '1'),
(68, 1, 2, 'KEC1003', 2014, '0', '1'),
(69, 1, 2, 'KEC1003', 2015, '0', '1'),
(70, 1, 2, 'KEC1003', 2016, '0', '1'),
(71, 1, 2, 'KEC1004', 2014, '0', '2'),
(72, 1, 2, 'KEC1004', 2015, '0', '2'),
(73, 1, 2, 'KEC1004', 2016, '0', '2'),
(74, 1, 2, 'KEC1005', 2014, '0', '2'),
(75, 1, 2, 'KEC1005', 2015, '0', '2'),
(76, 1, 2, 'KEC1005', 2016, '0', '2'),
(77, 2, 2, 'KEC1001', 2014, '0', '21'),
(78, 2, 2, 'KEC1001', 2015, '0', '0'),
(79, 2, 2, 'KEC1001', 2016, '0', '18'),
(80, 2, 2, 'KEC1002', 2014, '0', '13'),
(81, 2, 2, 'KEC1002', 2015, '0', '2'),
(82, 2, 2, 'KEC1002', 2016, '0', '0'),
(83, 2, 2, 'KEC1003', 2014, '0', '21'),
(84, 2, 2, 'KEC1003', 2015, '0', '13'),
(85, 2, 2, 'KEC1003', 2016, '0', '14'),
(86, 2, 2, 'KEC1004', 2014, '0', '16'),
(87, 2, 2, 'KEC1004', 2015, '0', '20'),
(88, 2, 2, 'KEC1004', 2016, '0', '9'),
(89, 2, 2, 'KEC1005', 2014, '0', '40'),
(90, 2, 2, 'KEC1005', 2015, '0', '27'),
(91, 2, 2, 'KEC1005', 2016, '0', '0'),
(92, 3, 2, 'KEC1001', 2014, '0', '23'),
(93, 3, 2, 'KEC1001', 2015, '0', '0'),
(94, 3, 2, 'KEC1001', 2016, '0', '4'),
(95, 3, 2, 'KEC1002', 2014, '0', '7'),
(96, 3, 2, 'KEC1002', 2015, '0', '1'),
(97, 3, 2, 'KEC1002', 2016, '0', '0'),
(98, 3, 2, 'KEC1003', 2014, '0', '8'),
(99, 3, 2, 'KEC1003', 2015, '0', '4'),
(100, 3, 2, 'KEC1003', 2016, '0', '1'),
(101, 3, 2, 'KEC1004', 2014, '0', '23'),
(102, 3, 2, 'KEC1004', 2015, '0', '5'),
(103, 3, 2, 'KEC1004', 2016, '0', '3'),
(104, 3, 2, 'KEC1005', 2014, '0', '26'),
(105, 3, 2, 'KEC1005', 2015, '0', '14'),
(106, 3, 2, 'KEC1005', 2016, '0', '11'),
(107, 4, 2, 'KEC1001', 2014, '0', '0.91'),
(108, 4, 2, 'KEC1001', 2015, '0', '0'),
(109, 4, 2, 'KEC1001', 2016, '0', '4.50'),
(110, 4, 2, 'KEC1002', 2014, '0', '1.86'),
(111, 4, 2, 'KEC1002', 2015, '0', '2.00'),
(112, 4, 2, 'KEC1002', 2016, '0', '0'),
(113, 4, 2, 'KEC1003', 2014, '0', '2.63'),
(114, 4, 2, 'KEC1003', 2015, '0', '3.25'),
(115, 4, 2, 'KEC1003', 2016, '0', '14.00'),
(116, 4, 2, 'KEC1004', 2014, '0', '0.70'),
(117, 4, 2, 'KEC1004', 2015, '0', '4.00'),
(118, 4, 2, 'KEC1004', 2016, '0', '3.00'),
(119, 4, 2, 'KEC1005', 2014, '0', '1.54'),
(120, 4, 2, 'KEC1005', 2015, '0', '1.93'),
(121, 4, 2, 'KEC1005', 2016, '0', '0'),
(122, 1, 3, 'KEC1001', 2014, '0', '32'),
(123, 1, 3, 'KEC1001', 2015, '0', '37'),
(124, 1, 3, 'KEC1001', 2016, '0', '36'),
(125, 1, 3, 'KEC1002', 2014, '0', '18'),
(126, 1, 3, 'KEC1002', 2015, '0', '18'),
(127, 1, 3, 'KEC1002', 2016, '0', '18'),
(128, 1, 3, 'KEC1003', 2014, '0', '5'),
(129, 1, 3, 'KEC1003', 2015, '0', '5'),
(130, 1, 3, 'KEC1003', 2016, '0', '6'),
(131, 1, 3, 'KEC1004', 2014, '0', '12'),
(132, 1, 3, 'KEC1004', 2015, '0', '15'),
(133, 1, 3, 'KEC1004', 2016, '0', '15'),
(134, 1, 3, 'KEC1005', 2014, '0', '25'),
(135, 1, 3, 'KEC1005', 2015, '0', '25'),
(136, 1, 3, 'KEC1005', 2016, '0', '26'),
(137, 2, 3, 'KEC1001', 2014, '0', '1939'),
(138, 2, 3, 'KEC1001', 2015, '0', '2000'),
(139, 2, 3, 'KEC1001', 2016, '0', '2000'),
(140, 2, 3, 'KEC1002', 2014, '0', '1437'),
(141, 2, 3, 'KEC1002', 2015, '0', '1409'),
(142, 2, 3, 'KEC1002', 2016, '0', '1396'),
(143, 2, 3, 'KEC1003', 2014, '0', '413'),
(144, 2, 3, 'KEC1003', 2015, '0', '466'),
(145, 2, 3, 'KEC1003', 2016, '0', '310'),
(146, 2, 3, 'KEC1004', 2014, '0', '996'),
(147, 2, 3, 'KEC1004', 2015, '0', '1255'),
(148, 2, 3, 'KEC1004', 2016, '0', '1289'),
(149, 2, 3, 'KEC1005', 2014, '0', '1290'),
(150, 2, 3, 'KEC1005', 2015, '0', '1334'),
(151, 2, 3, 'KEC1005', 2016, '0', '1634'),
(152, 3, 3, 'KEC1001', 2014, '0', '113'),
(153, 3, 3, 'KEC1001', 2015, '0', '138'),
(154, 3, 3, 'KEC1001', 2016, '0', '131'),
(155, 3, 3, 'KEC1002', 2014, '0', '83'),
(156, 3, 3, 'KEC1002', 2015, '0', '77'),
(157, 3, 3, 'KEC1002', 2016, '0', '75'),
(158, 3, 3, 'KEC1003', 2014, '0', '38'),
(159, 3, 3, 'KEC1003', 2015, '0', '30'),
(160, 3, 3, 'KEC1003', 2016, '0', '20'),
(161, 3, 3, 'KEC1004', 2014, '0', '86'),
(162, 3, 3, 'KEC1004', 2015, '0', '98'),
(163, 3, 3, 'KEC1004', 2016, '0', '103'),
(164, 3, 3, 'KEC1005', 2014, '0', '116'),
(165, 3, 3, 'KEC1005', 2015, '0', '99'),
(166, 3, 3, 'KEC1005', 2016, '0', '108'),
(167, 4, 3, 'KEC1001', 2014, '0', '17.16'),
(168, 4, 3, 'KEC1001', 2015, '0', '14.49'),
(169, 4, 3, 'KEC1001', 2016, '0', '15.80'),
(170, 4, 3, 'KEC1002', 2014, '0', '17.31'),
(171, 4, 3, 'KEC1002', 2015, '0', '18.30'),
(172, 4, 3, 'KEC1002', 2016, '0', '18.61'),
(173, 4, 3, 'KEC1003', 2014, '0', '10.87'),
(174, 4, 3, 'KEC1003', 2015, '0', '15.53'),
(175, 4, 3, 'KEC1003', 2016, '0', '15.50'),
(176, 4, 3, 'KEC1004', 2014, '0', '11.58'),
(177, 4, 3, 'KEC1004', 2015, '0', '12.81'),
(178, 4, 3, 'KEC1004', 2016, '0', '12.51'),
(179, 4, 3, 'KEC1005', 2014, '0', '11.12'),
(180, 4, 3, 'KEC1005', 2015, '0', '13.47'),
(181, 4, 3, 'KEC1005', 2016, '0', '15.13'),
(182, 1, 4, 'KEC1001', 2014, '45', '10'),
(183, 1, 4, 'KEC1001', 2015, '45', '10'),
(184, 1, 4, 'KEC1001', 2016, '45', '11'),
(185, 1, 4, 'KEC1002', 2014, '42', '16'),
(186, 1, 4, 'KEC1002', 2015, '42', '16'),
(187, 1, 4, 'KEC1002', 2016, '42', '16'),
(188, 1, 4, 'KEC1003', 2014, '19', '23'),
(189, 1, 4, 'KEC1003', 2015, '19', '23'),
(190, 1, 4, 'KEC1003', 2016, '19', '24'),
(191, 1, 4, 'KEC1004', 2014, '44', '13'),
(192, 1, 4, 'KEC1004', 2015, '44', '14'),
(193, 1, 4, 'KEC1004', 2016, '44', '13'),
(194, 1, 4, 'KEC1005', 2014, '45', '13'),
(195, 1, 4, 'KEC1005', 2015, '45', '14'),
(196, 1, 4, 'KEC1005', 2016, '45', '15'),
(197, 2, 4, 'KEC1001', 2014, '13920', '1339'),
(198, 2, 4, 'KEC1001', 2015, '13587', '1537'),
(199, 2, 4, 'KEC1001', 2016, '13738', '1738'),
(200, 2, 4, 'KEC1002', 2014, '14090', '2166'),
(201, 2, 4, 'KEC1002', 2015, '13786', '2581'),
(202, 2, 4, 'KEC1002', 2016, '13674', '2761'),
(203, 2, 4, 'KEC1003', 2014, '6519', '5784'),
(204, 2, 4, 'KEC1003', 2015, '6303', '5778'),
(205, 2, 4, 'KEC1003', 2016, '6057', '5847'),
(206, 2, 4, 'KEC1004', 2014, '13623', '3293'),
(207, 2, 4, 'KEC1004', 2015, '13235', '3279'),
(208, 2, 4, 'KEC1004', 2016, '12979', '3222'),
(209, 2, 4, 'KEC1005', 2014, '11726', '4339'),
(210, 2, 4, 'KEC1005', 2015, '11425', '4868'),
(211, 2, 4, 'KEC1005', 2016, '11190', '5612'),
(212, 3, 4, 'KEC1001', 2014, '612', '119'),
(213, 3, 4, 'KEC1001', 2015, '611', '128'),
(214, 3, 4, 'KEC1001', 2016, '617', '129'),
(215, 3, 4, 'KEC1002', 2014, '601', '205'),
(216, 3, 4, 'KEC1002', 2015, '583', '229'),
(217, 3, 4, 'KEC1002', 2016, '589', '235'),
(218, 3, 4, 'KEC1003', 2014, '305', '357'),
(219, 3, 4, 'KEC1003', 2015, '315', '363'),
(220, 3, 4, 'KEC1003', 2016, '303', '355'),
(221, 3, 4, 'KEC1004', 2014, '664', '258'),
(222, 3, 4, 'KEC1004', 2015, '605', '249'),
(223, 3, 4, 'KEC1004', 2016, '604', '235'),
(224, 3, 4, 'KEC1005', 2014, '584', '300'),
(225, 3, 4, 'KEC1005', 2015, '573', '314'),
(226, 3, 4, 'KEC1005', 2016, '593', '345'),
(227, 4, 4, 'KEC1001', 2014, '22.75', '11.25'),
(228, 4, 4, 'KEC1001', 2015, '22.24', '12.0'),
(229, 4, 4, 'KEC1001', 2016, '22.27', '13.47'),
(230, 4, 4, 'KEC1002', 2014, '23.44', '210.57'),
(231, 4, 4, 'KEC1002', 2015, '23.65', '1.27'),
(232, 4, 4, 'KEC1002', 2016, '23.22', '11.75'),
(233, 4, 4, 'KEC1003', 2014, '21.37', '16.20'),
(234, 4, 4, 'KEC1003', 2015, '20.01', '15.92'),
(235, 4, 4, 'KEC1003', 2016, '19.99', '16.47'),
(236, 4, 4, 'KEC1004', 2014, '20.52', '12.76'),
(237, 4, 4, 'KEC1004', 2015, '21.88', '13.17'),
(238, 4, 4, 'KEC1004', 2016, '21.49', '13.71'),
(239, 4, 4, 'KEC1005', 2014, '20.08', '14.46'),
(240, 4, 4, 'KEC1005', 2015, '19.94', '15.50'),
(241, 4, 4, 'KEC1005', 2016, '18.87', '16.27'),
(242, 1, 5, 'KEC1001', 2014, '1', '0'),
(243, 1, 5, 'KEC1001', 2015, '1', '0'),
(244, 1, 5, 'KEC1001', 2016, '1', '0'),
(245, 1, 5, 'KEC1002', 2014, '0', '1'),
(246, 1, 5, 'KEC1002', 2015, '0', '1'),
(247, 1, 5, 'KEC1002', 2016, '0', '1'),
(248, 1, 5, 'KEC1003', 2014, '0', '1'),
(249, 1, 5, 'KEC1003', 2015, '0', '1'),
(250, 1, 5, 'KEC1003', 2016, '0', '1'),
(251, 1, 5, 'KEC1004', 2014, '0', '3'),
(252, 1, 5, 'KEC1004', 2015, '0', '3'),
(253, 1, 5, 'KEC1004', 2016, '0', '3'),
(254, 1, 5, 'KEC1005', 2014, '0', '3'),
(255, 1, 5, 'KEC1005', 2015, '0', '2'),
(256, 1, 5, 'KEC1005', 2016, '0', '2'),
(257, 2, 5, 'KEC1001', 2014, '114', '0'),
(258, 2, 5, 'KEC1001', 2015, '130', '0'),
(259, 2, 5, 'KEC1001', 2016, '141', '0'),
(260, 2, 5, 'KEC1002', 2014, '0', '263'),
(261, 2, 5, 'KEC1002', 2015, '0', '241'),
(262, 2, 5, 'KEC1002', 2016, '0', '217'),
(263, 2, 5, 'KEC1003', 2014, '0', '39'),
(264, 2, 5, 'KEC1003', 2015, '0', '40'),
(265, 2, 5, 'KEC1003', 2016, '0', '40'),
(266, 2, 5, 'KEC1004', 2014, '0', '68'),
(267, 2, 5, 'KEC1004', 2015, '0', '82'),
(268, 2, 5, 'KEC1004', 2016, '0', '76'),
(269, 2, 5, 'KEC1005', 2014, '0', '93'),
(270, 2, 5, 'KEC1005', 2015, '0', '61'),
(271, 2, 5, 'KEC1005', 2016, '0', '68'),
(272, 3, 5, 'KEC1001', 2014, '40', '0'),
(273, 3, 5, 'KEC1001', 2015, '7', '0'),
(274, 3, 5, 'KEC1001', 2016, '9', '0'),
(275, 3, 5, 'KEC1002', 2014, '0', '63'),
(276, 3, 5, 'KEC1002', 2015, '0', '38'),
(277, 3, 5, 'KEC1002', 2016, '0', '32'),
(278, 3, 5, 'KEC1003', 2014, '0', '24'),
(279, 3, 5, 'KEC1003', 2015, '0', '7'),
(280, 3, 5, 'KEC1003', 2016, '0', '6'),
(281, 3, 5, 'KEC1004', 2014, '0', '61'),
(282, 3, 5, 'KEC1004', 2015, '0', '22'),
(283, 3, 5, 'KEC1004', 2016, '0', '23'),
(284, 3, 5, 'KEC1005', 2014, '0', '63'),
(285, 3, 5, 'KEC1005', 2015, '0', '17'),
(286, 3, 5, 'KEC1005', 2016, '0', '19'),
(287, 4, 5, 'KEC1001', 2014, '2.85', '0'),
(288, 4, 5, 'KEC1001', 2015, '18.57', '0'),
(289, 4, 5, 'KEC1001', 2016, '15.67', '0'),
(290, 4, 5, 'KEC1002', 2014, '0', '4.17'),
(291, 4, 5, 'KEC1002', 2015, '0', '6.34'),
(292, 4, 5, 'KEC1002', 2016, '0', '6.78'),
(293, 4, 5, 'KEC1003', 2014, '0', '1.63'),
(294, 4, 5, 'KEC1003', 2015, '0', '5.71'),
(295, 4, 5, 'KEC1003', 2016, '0', '6.67'),
(296, 4, 5, 'KEC1004', 2014, '0', '1.11'),
(297, 4, 5, 'KEC1004', 2015, '0', '3.73'),
(298, 4, 5, 'KEC1004', 2016, '0', '3.30'),
(299, 4, 5, 'KEC1005', 2014, '0', '1.48'),
(300, 4, 5, 'KEC1005', 2015, '0', '3.59'),
(301, 4, 5, 'KEC1005', 2016, '0', '3.58'),
(302, 1, 6, 'KEC1001', 2014, '0', '22'),
(303, 1, 6, 'KEC1001', 2015, '0', '24'),
(304, 1, 6, 'KEC1001', 2016, '0', '21'),
(305, 1, 6, 'KEC1002', 2014, '1', '11'),
(306, 1, 6, 'KEC1002', 2015, '1', '11'),
(307, 1, 6, 'KEC1002', 2016, '1', '10'),
(308, 1, 6, 'KEC1003', 2014, '1', '5'),
(309, 1, 6, 'KEC1003', 2015, '1', '5'),
(310, 1, 6, 'KEC1003', 2016, '1', '4'),
(311, 1, 6, 'KEC1004', 2014, '0', '5'),
(312, 1, 6, 'KEC1004', 2015, '0', '5'),
(313, 1, 6, 'KEC1004', 2016, '0', '4'),
(314, 1, 6, 'KEC1005', 2014, '0', '4'),
(315, 1, 6, 'KEC1005', 2015, '0', '5'),
(316, 1, 6, 'KEC1005', 2016, '0', '2'),
(317, 2, 6, 'KEC1001', 2014, '0', '3497'),
(318, 2, 6, 'KEC1001', 2015, '0', '3639'),
(319, 2, 6, 'KEC1001', 2016, '0', '3164'),
(320, 2, 6, 'KEC1002', 2014, '529', '2000'),
(321, 2, 6, 'KEC1002', 2015, '569', '2063'),
(322, 2, 6, 'KEC1002', 2016, '730', '1898'),
(323, 2, 6, 'KEC1003', 2014, '1336', '1384'),
(324, 2, 6, 'KEC1003', 2015, '1457', '1438'),
(325, 2, 6, 'KEC1003', 2016, '1594', '1432'),
(326, 2, 6, 'KEC1004', 2014, '0', '1115'),
(327, 2, 6, 'KEC1004', 2015, '0', '1217'),
(328, 2, 6, 'KEC1004', 2016, '0', '1086'),
(329, 2, 6, 'KEC1005', 2014, '0', '461'),
(330, 2, 6, 'KEC1005', 2015, '0', '749'),
(331, 2, 6, 'KEC1005', 2016, '0', '102'),
(332, 3, 6, 'KEC1001', 2014, '0', '239'),
(333, 3, 6, 'KEC1001', 2015, '0', '246'),
(334, 3, 6, 'KEC1001', 2016, '0', '194'),
(335, 3, 6, 'KEC1002', 2014, '29', '122'),
(336, 3, 6, 'KEC1002', 2015, '32', '122'),
(337, 3, 6, 'KEC1002', 2016, '37', '107'),
(338, 3, 6, 'KEC1003', 2014, '63', '74'),
(339, 3, 6, 'KEC1003', 2015, '65', '69'),
(340, 3, 6, 'KEC1003', 2016, '67', '61'),
(341, 3, 6, 'KEC1004', 2014, '0', '52'),
(342, 3, 6, 'KEC1004', 2015, '0', '56'),
(343, 3, 6, 'KEC1004', 2016, '0', '46'),
(344, 3, 6, 'KEC1005', 2014, '0', '34'),
(345, 3, 6, 'KEC1005', 2015, '0', '49'),
(346, 3, 6, 'KEC1005', 2016, '0', '11'),
(347, 4, 6, 'KEC1001', 2014, '0', '14.63'),
(348, 4, 6, 'KEC1001', 2015, '0', '14.79'),
(349, 4, 6, 'KEC1001', 2016, '0', '16.31'),
(350, 4, 6, 'KEC1002', 2014, '18.24', '16.39'),
(351, 4, 6, 'KEC1002', 2015, '17.78', '16.91'),
(352, 4, 6, 'KEC1002', 2016, '19.73', '17.74'),
(353, 4, 6, 'KEC1003', 2014, '21.21', '18.70'),
(354, 4, 6, 'KEC1003', 2015, '22.42', '20.84'),
(355, 4, 6, 'KEC1003', 2016, '23.79', '23.48'),
(356, 4, 6, 'KEC1004', 2014, '0', '21.44'),
(357, 4, 6, 'KEC1004', 2015, '0', '21.73'),
(358, 4, 6, 'KEC1004', 2016, '0', '23.61'),
(359, 4, 6, 'KEC1005', 2014, '0', '13.56'),
(360, 4, 6, 'KEC1005', 2015, '0', '15.29'),
(361, 4, 6, 'KEC1005', 2016, '0', '9.27'),
(362, 1, 7, 'KEC1001', 2014, '6', '9'),
(363, 1, 7, 'KEC1001', 2015, '6', '10'),
(364, 1, 7, 'KEC1001', 2016, '6', '11'),
(365, 1, 7, 'KEC1002', 2014, '4', '12'),
(366, 1, 7, 'KEC1002', 2015, '4', '12'),
(367, 1, 7, 'KEC1002', 2016, '4', '12'),
(368, 1, 7, 'KEC1003', 2014, '9', '16'),
(369, 1, 7, 'KEC1003', 2015, '9', '17'),
(370, 1, 7, 'KEC1003', 2016, '9', '16'),
(371, 1, 7, 'KEC1004', 2014, '6', '14'),
(372, 1, 7, 'KEC1004', 2015, '6', '16'),
(373, 1, 7, 'KEC1004', 2016, '6', '15'),
(374, 1, 7, 'KEC1005', 2014, '7', '14'),
(375, 1, 7, 'KEC1005', 2015, '7', '14'),
(376, 1, 7, 'KEC1005', 2016, '7', '14'),
(377, 2, 7, 'KEC1001', 2014, '4164', '1183'),
(378, 2, 7, 'KEC1001', 2015, '4298', '1449'),
(379, 2, 7, 'KEC1001', 2016, '4408', '1425'),
(380, 2, 7, 'KEC1002', 2014, '2745', '2735'),
(381, 2, 7, 'KEC1002', 2015, '2484', '2692'),
(382, 2, 7, 'KEC1002', 2016, '2534', '2622'),
(383, 2, 7, 'KEC1003', 2014, '7390', '4029'),
(384, 2, 7, 'KEC1003', 2015, '7226', '4241'),
(385, 2, 7, 'KEC1003', 2016, '7262', '3827'),
(386, 2, 7, 'KEC1004', 2014, '3667', '3202'),
(387, 2, 7, 'KEC1004', 2015, '3515', '3577'),
(388, 2, 7, 'KEC1004', 2016, '3592', '3381'),
(389, 2, 7, 'KEC1005', 2014, '4368', '2981'),
(390, 2, 7, 'KEC1005', 2015, '4004', '3292'),
(391, 2, 7, 'KEC1005', 2016, '4673', '3173'),
(392, 3, 7, 'KEC1001', 2014, '242', '138'),
(393, 3, 7, 'KEC1001', 2015, '248', '133'),
(394, 3, 7, 'KEC1001', 2016, '259', '148'),
(395, 3, 7, 'KEC1002', 2014, '144', '229'),
(396, 3, 7, 'KEC1002', 2015, '158', '228'),
(397, 3, 7, 'KEC1002', 2016, '159', '222'),
(398, 3, 7, 'KEC1003', 2014, '423', '288'),
(399, 3, 7, 'KEC1003', 2015, '438', '315'),
(400, 3, 7, 'KEC1003', 2016, '438', '300'),
(401, 3, 7, 'KEC1004', 2014, '221', '225'),
(402, 3, 7, 'KEC1004', 2015, '240', '267'),
(403, 3, 7, 'KEC1004', 2016, '246', '245'),
(404, 3, 7, 'KEC1005', 2014, '265', '269'),
(405, 3, 7, 'KEC1005', 2015, '238', '264'),
(406, 3, 7, 'KEC1005', 2016, '290', '254'),
(407, 4, 7, 'KEC1001', 2014, '17.21', '8.57'),
(408, 4, 7, 'KEC1001', 2015, '17.33', '10.89'),
(409, 4, 7, 'KEC1001', 2016, '17.02', '9.63'),
(410, 4, 7, 'KEC1002', 2014, '19.06', '11.94'),
(411, 4, 7, 'KEC1002', 2015, '15.72', '11.81'),
(412, 4, 7, 'KEC1002', 2016, '15.94', '11.81'),
(413, 4, 7, 'KEC1003', 2014, '17.47', '13.99'),
(414, 4, 7, 'KEC1003', 2015, '16.50', '13.46'),
(415, 4, 7, 'KEC1003', 2016, '16.58', '12.76'),
(416, 4, 7, 'KEC1004', 2014, '16.59', '14.23'),
(417, 4, 7, 'KEC1004', 2015, '14.65', '13.40'),
(418, 4, 7, 'KEC1004', 2016, '14.60', '13.80'),
(419, 4, 7, 'KEC1005', 2014, '16.48', '11.08'),
(420, 4, 7, 'KEC1005', 2015, '16.82', '12.47'),
(421, 4, 7, 'KEC1005', 2016, '16.11', '12.49'),
(422, 1, 8, 'KEC1001', 2014, '1', '0'),
(423, 1, 8, 'KEC1001', 2015, '1', '0'),
(424, 1, 8, 'KEC1001', 2016, '1', '0'),
(425, 1, 8, 'KEC1002', 2014, '0', '1'),
(426, 1, 8, 'KEC1002', 2015, '0', '1'),
(427, 1, 8, 'KEC1002', 2016, '0', '1'),
(428, 1, 8, 'KEC1003', 2014, '0', '1'),
(429, 1, 8, 'KEC1003', 2015, '0', '1'),
(430, 1, 8, 'KEC1003', 2016, '0', '1'),
(431, 1, 8, 'KEC1004', 2014, '0', '3'),
(432, 1, 8, 'KEC1004', 2015, '0', '3'),
(433, 1, 8, 'KEC1004', 2016, '0', '3'),
(434, 1, 8, 'KEC1005', 2014, '0', '2'),
(435, 1, 8, 'KEC1005', 2015, '0', '2'),
(436, 1, 8, 'KEC1005', 2016, '0', '2'),
(437, 2, 8, 'KEC1001', 2014, '48', '0'),
(438, 2, 8, 'KEC1001', 2015, '57', '0'),
(439, 2, 8, 'KEC1001', 2016, '57', '0'),
(440, 2, 8, 'KEC1002', 2014, '0', '102'),
(441, 2, 8, 'KEC1002', 2015, '0', '94'),
(442, 2, 8, 'KEC1002', 2016, '0', '101'),
(443, 2, 8, 'KEC1003', 2014, '0', '16'),
(444, 2, 8, 'KEC1003', 2015, '0', '17'),
(445, 2, 8, 'KEC1003', 2016, '0', '22'),
(446, 2, 8, 'KEC1004', 2014, '0', '49'),
(447, 2, 8, 'KEC1004', 2015, '0', '30'),
(448, 2, 8, 'KEC1004', 2016, '0', '25'),
(449, 2, 8, 'KEC1005', 2014, '0', '34'),
(450, 2, 8, 'KEC1005', 2015, '0', '32'),
(451, 2, 8, 'KEC1005', 2016, '0', '23'),
(452, 3, 8, 'KEC1001', 2014, '14', '0'),
(453, 3, 8, 'KEC1001', 2015, '14', '0'),
(454, 3, 8, 'KEC1001', 2016, '14', '0'),
(455, 3, 8, 'KEC1002', 2014, '0', '59'),
(456, 3, 8, 'KEC1002', 2015, '0', '14'),
(457, 3, 8, 'KEC1002', 2016, '0', '14'),
(458, 3, 8, 'KEC1003', 2014, '0', '14'),
(459, 3, 8, 'KEC1003', 2015, '0', '6'),
(460, 3, 8, 'KEC1003', 2016, '0', '5'),
(461, 3, 8, 'KEC1004', 2014, '0', '32'),
(462, 3, 8, 'KEC1004', 2015, '0', '14'),
(463, 3, 8, 'KEC1004', 2016, '0', '12'),
(464, 3, 8, 'KEC1005', 2014, '0', '28'),
(465, 3, 8, 'KEC1005', 2015, '0', '9'),
(466, 3, 8, 'KEC1005', 2016, '0', '8'),
(467, 4, 8, 'KEC1001', 2014, '3.43', '0'),
(468, 4, 8, 'KEC1001', 2015, '4.07', '0'),
(469, 4, 8, 'KEC1001', 2016, '4.07', '0'),
(470, 4, 8, 'KEC1002', 2014, '0', '1.73'),
(471, 4, 8, 'KEC1002', 2015, '0', '6.71'),
(472, 4, 8, 'KEC1002', 2016, '0', '7.21'),
(473, 4, 8, 'KEC1003', 2014, '0', '1.14'),
(474, 4, 8, 'KEC1003', 2015, '0', '2.83'),
(475, 4, 8, 'KEC1003', 2016, '0', '4.40'),
(476, 4, 8, 'KEC1004', 2014, '0', '1.53'),
(477, 4, 8, 'KEC1004', 2015, '0', '2.14'),
(478, 4, 8, 'KEC1004', 2016, '0', '2.08'),
(479, 4, 8, 'KEC1005', 2014, '0', '1.21'),
(480, 4, 8, 'KEC1005', 2015, '0', '3.56'),
(481, 4, 8, 'KEC1005', 2016, '0', '2.88'),
(482, 1, 9, 'KEC1001', 2014, '1', '11'),
(483, 1, 9, 'KEC1001', 2015, '1', '13'),
(484, 1, 9, 'KEC1001', 2016, '1', '13'),
(485, 1, 9, 'KEC1002', 2014, '0', '2'),
(486, 1, 9, 'KEC1002', 2015, '0', '3'),
(487, 1, 9, 'KEC1002', 2016, '0', '4'),
(488, 1, 9, 'KEC1003', 2014, '1', '4'),
(489, 1, 9, 'KEC1003', 2015, '1', '4'),
(490, 1, 9, 'KEC1003', 2016, '1', '5'),
(491, 1, 9, 'KEC1004', 2014, '0', '2'),
(492, 1, 9, 'KEC1004', 2015, '0', '2'),
(493, 1, 9, 'KEC1004', 2016, '0', '2'),
(494, 1, 9, 'KEC1005', 2014, '0', '5'),
(495, 1, 9, 'KEC1005', 2015, '0', '6'),
(496, 1, 9, 'KEC1005', 2016, '0', '8'),
(497, 2, 9, 'KEC1001', 2014, '535', '1004'),
(498, 2, 9, 'KEC1001', 2015, '539', '1138'),
(499, 2, 9, 'KEC1001', 2016, '468', '1130'),
(500, 2, 9, 'KEC1002', 2014, '0', '577'),
(501, 2, 9, 'KEC1002', 2015, '0', '738'),
(502, 2, 9, 'KEC1002', 2016, '0', '832'),
(503, 2, 9, 'KEC1003', 2014, '835', '804'),
(504, 2, 9, 'KEC1003', 2015, '893', '1134'),
(505, 2, 9, 'KEC1003', 2016, '955', '1431'),
(506, 2, 9, 'KEC1004', 2014, '0', '383'),
(507, 2, 9, 'KEC1004', 2015, '0', '542'),
(508, 2, 9, 'KEC1004', 2016, '0', '542'),
(509, 2, 9, 'KEC1005', 2014, '0', '1072'),
(510, 2, 9, 'KEC1005', 2015, '0', '1210'),
(511, 2, 9, 'KEC1005', 2016, '0', '1296'),
(512, 3, 9, 'KEC1001', 2014, '28', '158'),
(513, 3, 9, 'KEC1001', 2015, '34', '171'),
(514, 3, 9, 'KEC1001', 2016, '35', '165'),
(515, 3, 9, 'KEC1002', 2014, '0', '51'),
(516, 3, 9, 'KEC1002', 2015, '0', '57'),
(517, 3, 9, 'KEC1002', 2016, '0', '70'),
(518, 3, 9, 'KEC1003', 2014, '57', '78'),
(519, 3, 9, 'KEC1003', 2015, '59', '100'),
(520, 3, 9, 'KEC1003', 2016, '65', '105'),
(521, 3, 9, 'KEC1004', 2014, '0', '29'),
(522, 3, 9, 'KEC1004', 2015, '0', '32'),
(523, 3, 9, 'KEC1004', 2016, '0', '32'),
(524, 3, 9, 'KEC1005', 2014, '0', '88'),
(525, 3, 9, 'KEC1005', 2015, '0', '108'),
(526, 3, 9, 'KEC1005', 2016, '0', '134'),
(527, 4, 9, 'KEC1001', 2014, '19.11', '6.35'),
(528, 4, 9, 'KEC1001', 2015, '15.85', '6.65'),
(529, 4, 9, 'KEC1001', 2016, '13.37', '6.85'),
(530, 4, 9, 'KEC1002', 2014, '0', '11.31'),
(531, 4, 9, 'KEC1002', 2015, '0', '12.95'),
(532, 4, 9, 'KEC1002', 2016, '0', '11.89'),
(533, 4, 9, 'KEC1003', 2014, '14.65', '10.31'),
(534, 4, 9, 'KEC1003', 2015, '15.14', '11.34'),
(535, 4, 9, 'KEC1003', 2016, '14.69', '13.63'),
(536, 4, 9, 'KEC1004', 2014, '0', '13.21'),
(537, 4, 9, 'KEC1004', 2015, '0', '16.94'),
(538, 4, 9, 'KEC1004', 2016, '0', '16.94'),
(539, 4, 9, 'KEC1005', 2014, '0', '12.18'),
(540, 4, 9, 'KEC1005', 2015, '0', '11.20'),
(541, 4, 9, 'KEC1005', 2016, '0', '9.67'),
(542, 1, 10, 'KEC1001', 2014, '2', '3'),
(543, 1, 10, 'KEC1001', 2015, '2', '4'),
(544, 1, 10, 'KEC1001', 2016, '2', '4'),
(545, 1, 10, 'KEC1002', 2014, '0', '6'),
(546, 1, 10, 'KEC1002', 2015, '0', '5'),
(547, 1, 10, 'KEC1002', 2016, '0', '5'),
(548, 1, 10, 'KEC1003', 2014, '5', '7'),
(549, 1, 10, 'KEC1003', 2015, '5', '13'),
(550, 1, 10, 'KEC1003', 2016, '5', '13'),
(551, 1, 10, 'KEC1004', 2014, '0', '5'),
(552, 1, 10, 'KEC1004', 2015, '0', '6'),
(553, 1, 10, 'KEC1004', 2016, '0', '5'),
(554, 1, 10, 'KEC1005', 2014, '3', '7'),
(555, 1, 10, 'KEC1005', 2015, '3', '9'),
(556, 1, 10, 'KEC1005', 2016, '3', '8'),
(557, 2, 10, 'KEC1001', 2014, '1762', '144'),
(558, 2, 10, 'KEC1001', 2015, '1765', '291'),
(559, 2, 10, 'KEC1001', 2016, '1944', '274'),
(560, 2, 10, 'KEC1002', 2014, '0', '449'),
(561, 2, 10, 'KEC1002', 2015, '0', '544'),
(562, 2, 10, 'KEC1002', 2016, '0', '693'),
(563, 2, 10, 'KEC1003', 2014, '4418', '5255'),
(564, 2, 10, 'KEC1003', 2015, '4574', '5660'),
(565, 2, 10, 'KEC1003', 2016, '4679', '5728'),
(566, 2, 10, 'KEC1004', 2014, '0', '342'),
(567, 2, 10, 'KEC1004', 2015, '0', '319'),
(568, 2, 10, 'KEC1004', 2016, '0', '316'),
(569, 2, 10, 'KEC1005', 2014, '2909', '2016'),
(570, 2, 10, 'KEC1005', 2015, '2941', '2626'),
(571, 2, 10, 'KEC1005', 2016, '2995', '2749'),
(572, 3, 10, 'KEC1001', 2014, '131', '44'),
(573, 3, 10, 'KEC1001', 2015, '126', '58'),
(574, 3, 10, 'KEC1001', 2016, '125', '53'),
(575, 3, 10, 'KEC1002', 2014, '0', '104'),
(576, 3, 10, 'KEC1002', 2015, '0', '99'),
(577, 3, 10, 'KEC1002', 2016, '0', '95'),
(578, 3, 10, 'KEC1003', 2014, '338', '403'),
(579, 3, 10, 'KEC1003', 2015, '337', '432'),
(580, 3, 10, 'KEC1003', 2016, '341', '420'),
(581, 3, 10, 'KEC1004', 2014, '0', '86'),
(582, 3, 10, 'KEC1004', 2015, '0', '101'),
(583, 3, 10, 'KEC1004', 2016, '0', '81'),
(584, 3, 10, 'KEC1005', 2014, '206', '187'),
(585, 3, 10, 'KEC1005', 2015, '213', '258'),
(586, 3, 10, 'KEC1005', 2016, '209', '251'),
(587, 4, 10, 'KEC1001', 2014, '13.45', '3.27'),
(588, 4, 10, 'KEC1001', 2015, '14.01', '5.02'),
(589, 4, 10, 'KEC1001', 2016, '15.55', '5.17'),
(590, 4, 10, 'KEC1002', 2014, '0', '4.32'),
(591, 4, 10, 'KEC1002', 2015, '0', '5.49'),
(592, 4, 10, 'KEC1002', 2016, '0', '7.29'),
(593, 4, 10, 'KEC1003', 2014, '13.07', '13.04'),
(594, 4, 10, 'KEC1003', 2015, '13.57', '13.10'),
(595, 4, 10, 'KEC1003', 2016, '13.72', '13.64'),
(596, 4, 10, 'KEC1004', 2014, '0', '3.98'),
(597, 4, 10, 'KEC1004', 2015, '0', '3.16'),
(598, 4, 10, 'KEC1004', 2016, '0', '3.90'),
(599, 4, 10, 'KEC1005', 2014, '14.12', '10.78'),
(600, 4, 10, 'KEC1005', 2015, '13.81', '10.18'),
(601, 4, 10, 'KEC1005', 2016, '14.33', '10.95'),
(602, 1, 11, 'KEC1001', 2014, '0', '1'),
(603, 1, 11, 'KEC1001', 2015, '0', '1'),
(604, 1, 11, 'KEC1001', 2016, '0', '1'),
(605, 1, 11, 'KEC1002', 2014, '0', '1'),
(606, 1, 11, 'KEC1002', 2015, '0', '1'),
(607, 1, 11, 'KEC1002', 2016, '0', '1'),
(608, 1, 11, 'KEC1003', 2014, '0', '1'),
(609, 1, 11, 'KEC1003', 2015, '0', '1'),
(610, 1, 11, 'KEC1003', 2016, '0', '1'),
(611, 1, 11, 'KEC1004', 2014, '0', '2'),
(612, 1, 11, 'KEC1004', 2015, '0', '2'),
(613, 1, 11, 'KEC1004', 2016, '0', '1'),
(614, 1, 11, 'KEC1005', 2014, '0', '0'),
(615, 1, 11, 'KEC1005', 2015, '0', '1'),
(616, 1, 11, 'KEC1005', 2016, '0', '1'),
(617, 2, 11, 'KEC1001', 2014, '0', '30'),
(618, 2, 11, 'KEC1001', 2015, '0', '40'),
(619, 2, 11, 'KEC1001', 2016, '0', '41'),
(620, 2, 11, 'KEC1002', 2014, '0', '59'),
(621, 2, 11, 'KEC1002', 2015, '0', '63'),
(622, 2, 11, 'KEC1002', 2016, '0', '71'),
(623, 2, 11, 'KEC1003', 2014, '0', '19'),
(624, 2, 11, 'KEC1003', 2015, '0', '16'),
(625, 2, 11, 'KEC1003', 2016, '0', '14'),
(626, 2, 11, 'KEC1004', 2014, '0', '21'),
(627, 2, 11, 'KEC1004', 2015, '0', '18'),
(628, 2, 11, 'KEC1004', 2016, '0', '11'),
(629, 2, 11, 'KEC1005', 2014, '0', '11'),
(630, 2, 11, 'KEC1005', 2015, '0', '10'),
(631, 2, 11, 'KEC1005', 2016, '0', '12'),
(632, 3, 11, 'KEC1001', 2014, '0', '12'),
(633, 3, 11, 'KEC1001', 2015, '0', '8'),
(634, 3, 11, 'KEC1001', 2016, '0', '9'),
(635, 3, 11, 'KEC1002', 2014, '0', '19'),
(636, 3, 11, 'KEC1002', 2015, '0', '9'),
(637, 3, 11, 'KEC1002', 2016, '0', '9'),
(638, 3, 11, 'KEC1003', 2014, '0', '8'),
(639, 3, 11, 'KEC1003', 2015, '0', '4'),
(640, 3, 11, 'KEC1003', 2016, '0', '5'),
(641, 3, 11, 'KEC1004', 2014, '0', '11'),
(642, 3, 11, 'KEC1004', 2015, '0', '6'),
(643, 3, 11, 'KEC1004', 2016, '0', '3'),
(644, 3, 11, 'KEC1005', 2014, '0', '7'),
(645, 3, 11, 'KEC1005', 2015, '0', '4'),
(646, 3, 11, 'KEC1005', 2016, '0', '4'),
(647, 4, 11, 'KEC1001', 2014, '0', '2.50'),
(648, 4, 11, 'KEC1001', 2015, '0', '5.00'),
(649, 4, 11, 'KEC1001', 2016, '0', '4.56'),
(650, 4, 11, 'KEC1002', 2014, '0', '3.11'),
(651, 4, 11, 'KEC1002', 2015, '0', '7.00'),
(652, 4, 11, 'KEC1002', 2016, '0', '7.89'),
(653, 4, 11, 'KEC1003', 2014, '0', '2.38'),
(654, 4, 11, 'KEC1003', 2015, '0', '4.00'),
(655, 4, 11, 'KEC1003', 2016, '0', '2.80'),
(656, 4, 11, 'KEC1004', 2014, '0', '1.91'),
(657, 4, 11, 'KEC1004', 2015, '0', '3.00'),
(658, 4, 11, 'KEC1004', 2016, '0', '3.67'),
(659, 4, 11, 'KEC1005', 2014, '0', '1.57'),
(660, 4, 11, 'KEC1005', 2015, '0', '2.50'),
(661, 4, 11, 'KEC1005', 2016, '0', '3.00'),
(662, 1, 12, 'KEC1001', 2014, '0', '5'),
(663, 1, 12, 'KEC1001', 2015, '0', '5'),
(664, 1, 12, 'KEC1001', 2016, '0', '5'),
(665, 1, 12, 'KEC1002', 2014, '0', '1'),
(666, 1, 12, 'KEC1002', 2015, '0', '1'),
(667, 1, 12, 'KEC1002', 2016, '0', '1'),
(668, 1, 12, 'KEC1003', 2014, '1', '3'),
(669, 1, 12, 'KEC1003', 2015, '1', '3'),
(670, 1, 12, 'KEC1003', 2016, '1', '3'),
(671, 1, 12, 'KEC1004', 2014, '0', '1'),
(672, 1, 12, 'KEC1004', 2015, '0', '1'),
(673, 1, 12, 'KEC1004', 2016, '0', '1'),
(674, 1, 12, 'KEC1005', 2014, '1', '3'),
(675, 1, 12, 'KEC1005', 2015, '1', '3'),
(676, 1, 12, 'KEC1005', 2016, '1', '3'),
(677, 2, 12, 'KEC1001', 2014, '0', '341'),
(678, 2, 12, 'KEC1001', 2015, '0', '366'),
(679, 2, 12, 'KEC1001', 2016, '0', '329'),
(680, 2, 12, 'KEC1002', 2014, '0', '300'),
(681, 2, 12, 'KEC1002', 2015, '0', '318'),
(682, 2, 12, 'KEC1002', 2016, '0', '286'),
(683, 2, 12, 'KEC1003', 2014, '720', '180'),
(684, 2, 12, 'KEC1003', 2015, '748', '209'),
(685, 2, 12, 'KEC1003', 2016, '748', '227'),
(686, 2, 12, 'KEC1004', 2014, '0', '24'),
(687, 2, 12, 'KEC1004', 2015, '0', '23'),
(688, 2, 12, 'KEC1004', 2016, '0', '21'),
(689, 2, 12, 'KEC1005', 2014, '840', '350'),
(690, 2, 12, 'KEC1005', 2015, '886', '340'),
(691, 2, 12, 'KEC1005', 2016, '886', '344'),
(692, 3, 12, 'KEC1001', 2014, '0', '89'),
(693, 3, 12, 'KEC1001', 2015, '0', '73'),
(694, 3, 12, 'KEC1001', 2016, '0', '75'),
(695, 3, 12, 'KEC1002', 2014, '0', '21'),
(696, 3, 12, 'KEC1002', 2015, '0', '16'),
(697, 3, 12, 'KEC1002', 2016, '0', '16'),
(698, 3, 12, 'KEC1003', 2014, '68', '49'),
(699, 3, 12, 'KEC1003', 2015, '68', '63'),
(700, 3, 12, 'KEC1003', 2016, '67', '63'),
(701, 3, 12, 'KEC1004', 2014, '0', '13'),
(702, 3, 12, 'KEC1004', 2015, '0', '15'),
(703, 3, 12, 'KEC1004', 2016, '0', '4'),
(704, 3, 12, 'KEC1005', 2014, '63', '55'),
(705, 3, 12, 'KEC1005', 2015, '64', '57'),
(706, 3, 12, 'KEC1005', 2016, '64', '57'),
(707, 4, 12, 'KEC1001', 2014, '0', '3.83'),
(708, 4, 12, 'KEC1001', 2015, '0', '5.01'),
(709, 4, 12, 'KEC1001', 2016, '0', '4.39'),
(710, 4, 12, 'KEC1002', 2014, '0', '14.29'),
(711, 4, 12, 'KEC1002', 2015, '0', '19.88'),
(712, 4, 12, 'KEC1002', 2016, '0', '17.88'),
(713, 4, 12, 'KEC1003', 2014, '10.59', '3.67'),
(714, 4, 12, 'KEC1003', 2015, '11.00', '3.32'),
(715, 4, 12, 'KEC1003', 2016, '11.16', '3.60'),
(716, 4, 12, 'KEC1004', 2014, '0', '1.85'),
(717, 4, 12, 'KEC1004', 2015, '0', '1.53'),
(718, 4, 12, 'KEC1004', 2016, '0', '5.25'),
(719, 4, 12, 'KEC1005', 2014, '13.33', '6.36'),
(720, 4, 12, 'KEC1005', 2015, '13.84', '5.96'),
(721, 4, 12, 'KEC1005', 2016, '13.84', '6.04'),
(722, 1, 13, 'KEC1001', 2014, '3', '5'),
(723, 1, 13, 'KEC1001', 2015, '3', '5'),
(724, 1, 13, 'KEC1001', 2016, '3', '5'),
(725, 1, 13, 'KEC1002', 2014, '3', '7'),
(726, 1, 13, 'KEC1002', 2015, '3', '8'),
(727, 1, 13, 'KEC1002', 2016, '3', '9'),
(728, 1, 13, 'KEC1003', 2014, '2', '9'),
(729, 1, 13, 'KEC1003', 2015, '2', '9'),
(730, 1, 13, 'KEC1003', 2016, '2', '9'),
(731, 1, 13, 'KEC1004', 2014, '2', '7'),
(732, 1, 13, 'KEC1004', 2015, '2', '8'),
(733, 1, 13, 'KEC1004', 2016, '2', '9'),
(734, 1, 13, 'KEC1005', 2014, '3', '10'),
(735, 1, 13, 'KEC1005', 2015, '3', '10'),
(736, 1, 13, 'KEC1005', 2016, '3', '10'),
(737, 2, 13, 'KEC1001', 2014, '4996', '1253'),
(738, 2, 13, 'KEC1001', 2015, '4697', '1356'),
(739, 2, 13, 'KEC1001', 2016, '4820', '1440'),
(740, 2, 13, 'KEC1002', 2014, '3743', '1986'),
(741, 2, 13, 'KEC1002', 2015, '3735', '2045'),
(742, 2, 13, 'KEC1002', 2016, '4252', '2029'),
(743, 2, 13, 'KEC1003', 2014, '5736', '2133'),
(744, 2, 13, 'KEC1003', 2015, '3894', '1910'),
(745, 2, 13, 'KEC1003', 2016, '4220', '1841'),
(746, 2, 13, 'KEC1004', 2014, '1800', '1739'),
(747, 2, 13, 'KEC1004', 2015, '2206', '1785'),
(748, 2, 13, 'KEC1004', 2016, '2610', '2283'),
(749, 2, 13, 'KEC1005', 2014, '4039', '4098'),
(750, 2, 13, 'KEC1005', 2015, '4082', '3726'),
(751, 2, 13, 'KEC1005', 2016, '4244', '3666'),
(752, 3, 13, 'KEC1001', 2014, '299', '118'),
(753, 3, 13, 'KEC1001', 2015, '229', '129'),
(754, 3, 13, 'KEC1001', 2016, '299', '128'),
(755, 3, 13, 'KEC1002', 2014, '227', '241'),
(756, 3, 13, 'KEC1002', 2015, '229', '255'),
(757, 3, 13, 'KEC1002', 2016, '233', '257'),
(758, 3, 13, 'KEC1003', 2014, '246', '244'),
(759, 3, 13, 'KEC1003', 2015, '236', '230'),
(760, 3, 13, 'KEC1003', 2016, '239', '223'),
(761, 3, 13, 'KEC1004', 2014, '126', '190'),
(762, 3, 13, 'KEC1004', 2015, '132', '214'),
(763, 3, 13, 'KEC1004', 2016, '127', '280'),
(764, 3, 13, 'KEC1005', 2014, '251', '308'),
(765, 3, 13, 'KEC1005', 2015, '264', '289'),
(766, 3, 13, 'KEC1005', 2016, '266', '277'),
(767, 4, 13, 'KEC1001', 2014, '16.71', '10.62'),
(768, 4, 13, 'KEC1001', 2015, '20.51', '10.51'),
(769, 4, 13, 'KEC1001', 2016, '16.12', '11.25'),
(770, 4, 13, 'KEC1002', 2014, '16.49', '8.24'),
(771, 4, 13, 'KEC1002', 2015, '16.31', '8.02'),
(772, 4, 13, 'KEC1002', 2016, '18.25', '7.89'),
(773, 4, 13, 'KEC1003', 2014, '23.32', '8.74'),
(774, 4, 13, 'KEC1003', 2015, '16.50', '8.30'),
(775, 4, 13, 'KEC1003', 2016, '17.66', '8.26'),
(776, 4, 13, 'KEC1004', 2014, '14.29', '9.15'),
(777, 4, 13, 'KEC1004', 2015, '16.71', '8.34'),
(778, 4, 13, 'KEC1004', 2016, '20.55', '8.15'),
(779, 4, 13, 'KEC1005', 2014, '16.09', '13.31'),
(780, 4, 13, 'KEC1005', 2015, '15.46', '12.89'),
(781, 4, 13, 'KEC1005', 2016, '15.95', '13.23');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pendidikan_jenis`
--

CREATE TABLE `lp_pendidikan_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pendidikan_jenis`
--

INSERT INTO `lp_pendidikan_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Jumlah Sekolah'),
(2, 'Jumlah Murid'),
(3, 'Jumlah Guru'),
(4, 'Rasio Guru-Murid');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pendidikan_kategori`
--

CREATE TABLE `lp_pendidikan_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pendidikan_kategori`
--

INSERT INTO `lp_pendidikan_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Taman Kanak-Kanak'),
(2, 'Taman Kanak-Kanak Luar Biasa'),
(3, 'Roudlotul Athfal'),
(4, 'Sekolah Dasar'),
(5, 'Sekolah Dasar Luar Biasa'),
(6, 'Madrasah Ibtidaiyah'),
(7, 'Sekolah Menengah Pertama'),
(8, 'Sekolah Menengah Pertama Luar Biasa'),
(9, 'Madrasah Tsanawiyah'),
(10, 'Sekolah Menengah Atas'),
(11, 'Sekolah Menengah Atas Luar Biasa'),
(12, 'Madrasah Aliyah'),
(13, 'Sekolah Menengah Kejuruan');

-- --------------------------------------------------------

--
-- Table structure for table `lp_penduduk_jk`
--

CREATE TABLE `lp_penduduk_jk` (
  `id_penduduk` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_l` varchar(20) NOT NULL,
  `jml_p` varchar(20) NOT NULL,
  `rasio` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_penduduk_jk`
--

INSERT INTO `lp_penduduk_jk` (`id_penduduk`, `id_kec`, `th`, `jml_l`, `jml_p`, `rasio`) VALUES
(1, 'KEC1004', 2015, '97305', '97286', '0'),
(2, 'KEC1004', 2016, '98360', '98487', '98.16'),
(3, 'KEC1004', 2017, '99200', '99228', '0'),
(4, 'KEC1003', 2015, '53881', '55936', '0'),
(5, 'KEC1003', 2016, '54045', '56091', '90.86'),
(6, 'KEC1003', 2017, '53998', '56079', '0'),
(7, 'KEC1001', 2015, '102528', '101877', '0'),
(8, 'KEC1001', 2016, '104722', '102257', '98.99'),
(9, 'KEC1001', 2017, '106509', '105950', '0'),
(10, 'KEC1002', 2015, '101739', '101189', '0'),
(11, 'KEC1002', 2016, '103623', '102989', '98.7'),
(12, 'KEC1002', 2017, '104831', '104222', '0'),
(13, 'KEC1005', 2015, '84845', '85208', '0'),
(14, 'KEC1005', 2016, '86183', '86630', '96.94'),
(15, 'KEC1005', 2017, '86799', '87353', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pend_industri`
--

CREATE TABLE `lp_pend_industri` (
  `id_pend` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pend_industri`
--

INSERT INTO `lp_pend_industri` (`id_pend`, `id_jenis`, `id_kategori`, `th`, `jml`) VALUES
(1, 1, 1, 2014, '5'),
(2, 1, 2, 2014, '3788'),
(3, 1, 3, 2014, '80777671000'),
(4, 2, 1, 2014, '667'),
(5, 2, 2, 2014, '20639'),
(6, 2, 3, 2014, '1994378637'),
(7, 3, 1, 2014, '594'),
(8, 3, 2, 2014, '2398'),
(9, 3, 3, 2014, '65613010333'),
(10, 4, 1, 2014, '1746'),
(11, 4, 2, 2014, '5908'),
(12, 4, 3, 2014, '5386354000'),
(13, 1, 1, 2015, '5'),
(14, 1, 2, 2015, '3788'),
(15, 1, 3, 2015, '80777671000'),
(16, 2, 1, 2015, '696'),
(17, 2, 2, 2015, '21318'),
(18, 2, 3, 2015, '3648152072'),
(19, 3, 1, 2015, '594'),
(20, 3, 2, 2015, '2398'),
(21, 3, 3, 2015, '65613010333'),
(22, 4, 1, 2015, '1746'),
(23, 4, 2, 2015, '5908'),
(24, 4, 3, 2015, '5386354000'),
(25, 1, 1, 2016, '16'),
(26, 1, 2, 2016, '8015'),
(27, 1, 3, 2016, '1277678638000'),
(28, 2, 1, 2016, '646'),
(29, 2, 2, 2016, '13302'),
(30, 2, 3, 2016, '255759234000'),
(31, 3, 1, 2016, '2413'),
(32, 3, 2, 2016, '5696'),
(33, 2, 3, 2016, '121005432900'),
(34, 4, 1, 2016, '1746'),
(35, 4, 2, 2016, '5908'),
(36, 4, 3, 2016, '5386354000'),
(37, 1, 1, 2008, '5'),
(38, 1, 1, 2009, '5'),
(39, 1, 1, 2010, '5'),
(40, 1, 1, 2011, '5'),
(41, 1, 1, 2012, '5'),
(42, 1, 1, 2013, '5'),
(43, 1, 1, 2017, '18'),
(44, 1, 2, 2008, '3788'),
(45, 1, 2, 2009, '3788'),
(46, 1, 2, 2010, '3788'),
(47, 1, 2, 2011, '3788'),
(48, 1, 2, 2012, '3788'),
(49, 1, 2, 2013, '3788'),
(50, 1, 2, 2017, '8794'),
(51, 1, 3, 2008, '80777671000'),
(52, 1, 3, 2009, '80777671000'),
(53, 1, 3, 2010, '80777671000'),
(54, 1, 3, 2011, '80777671000'),
(55, 1, 3, 2012, '80777671000'),
(56, 1, 3, 2013, '80777671000'),
(57, 1, 3, 2017, '1841548502000'),
(58, 2, 1, 2008, '881'),
(59, 2, 1, 2009, '895'),
(60, 2, 1, 2010, '911'),
(61, 2, 1, 2011, '921'),
(62, 2, 1, 2012, '926'),
(63, 2, 1, 2013, '667'),
(64, 2, 1, 2017, '625'),
(65, 2, 2, 2008, '32494'),
(66, 2, 2, 2009, '33339'),
(67, 2, 2, 2010, '33589'),
(68, 2, 2, 2011, '33686'),
(69, 2, 2, 2012, '33686'),
(70, 2, 2, 2013, '33761'),
(71, 2, 2, 2017, '11369'),
(72, 2, 3, 2008, '264085870476'),
(73, 2, 3, 2009, '279828270476'),
(74, 2, 3, 2010, '283335870476'),
(75, 2, 3, 2011, '289965527476'),
(76, 2, 3, 2012, '289965527476'),
(77, 2, 3, 2013, '296040527476'),
(78, 2, 3, 2017, '1862682517000'),
(79, 3, 1, 2008, '838'),
(80, 3, 1, 2009, '838'),
(81, 3, 1, 2010, '838'),
(82, 3, 1, 2011, '838'),
(83, 3, 1, 2012, '838'),
(84, 3, 1, 2013, '924'),
(85, 3, 1, 2017, '1137'),
(86, 3, 2, 2008, '2716'),
(87, 3, 2, 2009, '2716'),
(88, 3, 2, 2010, '2716'),
(89, 3, 2, 2011, '2716'),
(90, 3, 2, 2012, '2716'),
(91, 3, 2, 2013, '3391'),
(92, 3, 2, 2017, '3779'),
(93, 3, 3, 2008, '2922525000'),
(94, 3, 3, 2009, '2922525000'),
(95, 3, 3, 2010, '2922525000'),
(96, 3, 3, 2011, '2922525000'),
(97, 3, 3, 2012, '2922525000'),
(98, 3, 3, 2013, '5278525'),
(99, 3, 3, 2017, '300816870000'),
(100, 4, 1, 2008, '1431'),
(101, 4, 1, 2009, '1451'),
(102, 4, 1, 2010, '1471'),
(103, 4, 1, 2011, '1486'),
(104, 4, 1, 2012, '1511'),
(105, 4, 1, 2013, '1746'),
(106, 4, 1, 2017, '2712'),
(107, 4, 2, 2008, '4921'),
(108, 4, 2, 2009, '4981'),
(109, 4, 2, 2010, '5041'),
(110, 4, 2, 2011, '5086'),
(111, 4, 2, 2012, '5173'),
(112, 4, 2, 2013, '5908'),
(113, 4, 2, 2017, '6332'),
(114, 4, 3, 2008, '1648854000'),
(115, 4, 3, 2009, '1658854000'),
(116, 4, 3, 2010, '1668854000'),
(117, 4, 3, 2011, '1676354000'),
(118, 4, 3, 2012, '2726451000'),
(119, 4, 3, 2013, '5386354000'),
(120, 4, 3, 2017, '231745361000');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pend_industri_jenis`
--

CREATE TABLE `lp_pend_industri_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pend_industri_jenis`
--

INSERT INTO `lp_pend_industri_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'industri besar (>10 Miliar)'),
(2, 'Industri Kecil Dan Menengah (750 Jt s/d 10 Miliar)'),
(3, 'Sentra Industri'),
(4, 'Industri Non Formal');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pend_industri_kategori`
--

CREATE TABLE `lp_pend_industri_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pend_industri_kategori`
--

INSERT INTO `lp_pend_industri_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'jumlah unit usaha (unit)'),
(2, 'Jumlah Tenaga Kerja (Orang)'),
(3, 'Jumlah Nilai Investasi (Rp.)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_perpustakaan`
--

CREATE TABLE `lp_perpustakaan` (
  `id_perpustakaan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_perpustakaan`
--

INSERT INTO `lp_perpustakaan` (`id_perpustakaan`, `id_jenis`, `th`, `jml`) VALUES
(1, 1, 2014, '254946'),
(2, 1, 2015, '227132'),
(3, 1, 2016, '226073'),
(4, 2, 2014, '156273'),
(5, 2, 2015, '164568'),
(6, 2, 2016, '175699'),
(7, 3, 2014, '70'),
(8, 3, 2015, '370'),
(9, 3, 2016, '370'),
(10, 4, 2014, '100'),
(11, 4, 2015, '100'),
(12, 4, 2016, '100'),
(13, 5, 2014, '4'),
(14, 5, 2015, '6'),
(15, 5, 2016, '6'),
(16, 6, 2014, '1'),
(17, 6, 2015, '1'),
(18, 6, 2016, '1');

-- --------------------------------------------------------

--
-- Table structure for table `lp_perpustakaan_jenis`
--

CREATE TABLE `lp_perpustakaan_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_perpustakaan_jenis`
--

INSERT INTO `lp_perpustakaan_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Jumlah Pengunjung Perpustakaan (Orang)'),
(2, 'jumlah penyediaan bahan perpustakaan (eksemplar)'),
(3, 'jumlah pembinaan masyarakat dan lembaga pendidikan binaan (perpustakaan)'),
(4, 'jumlah pelayanan perpustakaan keliling (binaan)'),
(5, 'jumlah sarana ruang baca perpustakaan (jenis)'),
(6, 'jumlah pengendalian hama/fumigasi (kali)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pertanian`
--

CREATE TABLE `lp_pertanian` (
  `id_pertanian` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `luas` varchar(20) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pertanian`
--

INSERT INTO `lp_pertanian` (`id_pertanian`, `id_sub_jenis`, `th`, `luas`, `jml`) VALUES
(1, 1, 2014, '0', '0'),
(2, 1, 2015, '0', '0'),
(3, 1, 2016, '0', '0'),
(4, 2, 2014, '0', '0'),
(5, 2, 2015, '1', '62'),
(6, 2, 2016, '0', '0'),
(7, 3, 2014, '0', '0'),
(8, 3, 2015, '0', '0'),
(9, 3, 2016, '0', '0'),
(10, 4, 2014, '1', '18'),
(11, 4, 2015, '0', '0'),
(12, 4, 2016, '0', '0'),
(13, 5, 2014, '0', '0'),
(14, 5, 2015, '0', '0'),
(15, 5, 2016, '0', '0'),
(16, 6, 2014, '1', '30'),
(17, 6, 2015, '0', '0'),
(18, 6, 2016, '0', '0'),
(19, 7, 2014, '10', '589'),
(20, 7, 2015, '34', '1596'),
(21, 7, 2016, '27', '278'),
(22, 8, 2014, '25', '646'),
(23, 8, 2015, '18', '1125'),
(24, 8, 2016, '11', '44'),
(25, 9, 2014, '10280', '417920'),
(26, 9, 2015, '85700', '1896600'),
(27, 9, 2016, '29600', '3547120'),
(28, 10, 2014, '0', '0'),
(29, 10, 2015, '0', '0'),
(30, 10, 2016, '0', '0'),
(31, 11, 2014, '7', '295'),
(32, 11, 2015, '2', '40'),
(33, 11, 2016, '1', '50'),
(34, 12, 2014, '4', '60'),
(35, 12, 2015, '0', '0'),
(36, 12, 2016, '0', '0'),
(37, 13, 2014, '0', '0'),
(38, 13, 2015, '0', '0'),
(39, 13, 2016, '4', '8'),
(40, 14, 2014, '0', '0'),
(41, 14, 2015, '0', '0'),
(42, 14, 2016, '0', '0'),
(43, 15, 2014, '4', '240'),
(44, 15, 2015, '1', '60'),
(45, 15, 2016, '1', '2'),
(46, 16, 2014, '0', '0'),
(47, 16, 2015, '0', '0'),
(48, 16, 2016, '0', '0'),
(49, 17, 2014, '1', '72'),
(50, 17, 2015, '0', '0'),
(51, 17, 2016, '0', '0'),
(52, 18, 2014, '0', '0'),
(53, 18, 2015, '0', '0'),
(54, 18, 2016, '0', '0'),
(55, 19, 2014, '0', '0'),
(56, 19, 2015, '0', '0'),
(57, 19, 2016, '0', '0'),
(58, 20, 2014, '150', '1875'),
(59, 20, 2015, '0', '0'),
(60, 20, 2016, '0', '0'),
(61, 21, 2014, '7', '155'),
(62, 21, 2015, '7', '320'),
(63, 21, 2016, '2', '42'),
(64, 22, 2014, '0', '0'),
(65, 22, 2015, '0', '0'),
(66, 22, 2016, '0', '0'),
(67, 23, 2014, '0', '0'),
(68, 23, 2015, '0', '0'),
(69, 23, 2016, '0', '0'),
(70, 24, 2014, '1', '46'),
(71, 24, 2015, '4', '140'),
(72, 24, 2016, '6', '20'),
(73, 25, 2014, '4', '352'),
(74, 25, 2015, '7', '227'),
(75, 25, 2016, '12', '82'),
(76, 26, 2014, '0', '0'),
(77, 26, 2015, '0', '0'),
(78, 26, 2016, '0', '0'),
(79, 27, 2014, '280', '251'),
(80, 27, 2015, '425', '247'),
(81, 27, 2016, '350', '200'),
(82, 28, 2014, '93', '66'),
(83, 28, 2015, '112', '77'),
(84, 28, 2016, '80', '55'),
(85, 29, 2014, '0', '0'),
(86, 29, 2015, '0', '0'),
(87, 29, 2016, '0', '0'),
(88, 30, 2014, '1470', '981'),
(89, 30, 2015, '1335', '821'),
(90, 30, 2016, '1665', '836'),
(91, 31, 2014, '53', '53'),
(92, 31, 2015, '60', '52'),
(93, 31, 2016, '65', '50'),
(94, 32, 2014, '370', '464'),
(95, 32, 2015, '385', '466'),
(96, 32, 2016, '510', '467'),
(97, 33, 2014, '609', '405'),
(98, 33, 2015, '619', '412'),
(99, 33, 2016, '714', '323'),
(100, 34, 2014, '1765', '920'),
(101, 34, 2015, '1933', '915'),
(102, 34, 2016, '1480', '815'),
(103, 35, 2014, '0', '0'),
(104, 35, 2015, '0', '0'),
(105, 35, 2016, '0', '0'),
(106, 36, 2014, '120', '221'),
(107, 36, 2015, '160', '200'),
(108, 36, 2016, '200', '200'),
(109, 37, 2014, '5085', '5888'),
(110, 37, 2015, '5060', '5875'),
(111, 37, 2016, '7520', '5429'),
(112, 38, 2014, '10442', '11315'),
(113, 38, 2015, '10241', '11459'),
(114, 38, 2016, '9871', '10137'),
(115, 39, 2014, '0', '0'),
(116, 39, 2015, '0', '0'),
(117, 39, 2016, '0', '0'),
(118, 40, 2014, '50', '5'),
(119, 40, 2015, '140', '49'),
(120, 40, 2016, '160', '65'),
(121, 41, 2014, '1256', '488'),
(122, 41, 2015, '1248', '487'),
(123, 41, 2016, '1300', '475'),
(124, 42, 2014, '1230', '1530'),
(125, 42, 2015, '1255', '1523'),
(126, 42, 2016, '1105', '1397'),
(127, 43, 2014, '0', '0'),
(128, 43, 2015, '0', '0'),
(129, 43, 2016, '0', '0'),
(130, 44, 2014, '1890', '1560'),
(131, 44, 2015, '1720', '1454'),
(132, 44, 2016, '1770', '1853'),
(133, 45, 2014, '788', '358'),
(134, 45, 2015, '1035', '411'),
(135, 45, 2016, '1165', '536'),
(136, 46, 2014, '5307', '2133'),
(137, 46, 2015, '5526', '2133'),
(138, 46, 2016, '5728', '2121'),
(139, 47, 2014, '1776', '860'),
(140, 47, 2015, '1775', '872'),
(141, 47, 2016, '2170', '877'),
(142, 48, 2014, '8290', '2651'),
(143, 48, 2015, '8085', '2466'),
(144, 48, 2016, '8088', '2310'),
(145, 49, 2014, '226', '135'),
(146, 49, 2015, '245', '145'),
(147, 49, 2016, '225', '139'),
(148, 50, 2014, '895', '459'),
(149, 50, 2015, '995', '434'),
(150, 50, 2016, '710', '161'),
(151, 51, 2014, '895', '677'),
(152, 51, 2015, '1306', '707'),
(153, 51, 2016, '1350', '649'),
(154, 52, 2014, '0', '0'),
(155, 52, 2015, '60', '120'),
(156, 52, 2016, '30', '60'),
(157, 53, 2014, '12112', '30324'),
(158, 53, 2015, '16612', '40412'),
(159, 53, 2016, '10263', '30326'),
(160, 54, 2014, '0', '0'),
(161, 54, 2015, '30', '15'),
(162, 54, 2016, '20', '10'),
(163, 55, 2014, '0', '0'),
(164, 55, 2015, '50', '200'),
(165, 55, 2016, '50', '200'),
(166, 56, 2014, '4', '11'),
(167, 56, 2015, '230', '235'),
(168, 56, 2016, '80', '200'),
(169, 57, 2014, '50106', '100199'),
(170, 57, 2015, '90906', '181512'),
(171, 57, 2016, '45005', '90015'),
(172, 58, 2014, '6105', '26870'),
(173, 58, 2015, '9250', '23020'),
(174, 58, 2016, '4100', '12750'),
(175, 59, 2014, '50', '140'),
(176, 59, 2015, '300', '365'),
(177, 59, 2016, '0', '0'),
(178, 60, 2014, '50', '200'),
(179, 60, 2015, '104', '416'),
(180, 60, 2016, '312', '1248'),
(181, 61, 2014, '8', '792'),
(182, 61, 2015, '75', '1818'),
(183, 61, 2016, '67', '4812'),
(184, 62, 2014, '17', '849'),
(185, 62, 2015, '108', '458'),
(186, 62, 2016, '83', '731'),
(187, 63, 2014, '100', '200'),
(188, 63, 2015, '307', '202'),
(189, 63, 2016, '257', '127'),
(190, 64, 2014, '100', '214'),
(191, 64, 2015, '350', '416'),
(192, 64, 2016, '50', '110'),
(193, 65, 2014, '0', '55'),
(194, 65, 2015, '250', '192'),
(195, 65, 2016, '0', '0'),
(196, 66, 2014, '107', '188'),
(197, 66, 2015, '400', '455'),
(198, 66, 2016, '50', '150'),
(199, 67, 2014, '86', '357'),
(200, 67, 2015, '11', '46'),
(201, 67, 2016, '140', '560'),
(202, 68, 2014, '232', '2370'),
(203, 68, 2015, '86', '121'),
(204, 68, 2016, '143', '413'),
(205, 69, 2014, '4269', '132039'),
(206, 69, 2015, '2422', '62473'),
(207, 69, 2016, '4952', '25484'),
(208, 70, 2014, '587', '6953'),
(209, 70, 2015, '190', '3450'),
(210, 70, 2016, '175', '665'),
(211, 71, 2014, '220', '2345'),
(212, 71, 2015, '5', '10'),
(213, 71, 2016, '340', '1160'),
(214, 72, 2014, '0', '0'),
(215, 72, 2015, '0', '0'),
(216, 72, 2016, '0', '0'),
(217, 73, 2014, '220', '3195'),
(218, 73, 2015, '0', '0'),
(219, 73, 2016, '100', '500'),
(220, 74, 2014, '55', '520'),
(221, 74, 2015, '80', '96'),
(222, 74, 2016, '75', '86'),
(223, 75, 2014, '164', '4350'),
(224, 75, 2015, '70', '116'),
(225, 75, 2016, '200', '346'),
(226, 76, 2014, '15', '30'),
(227, 76, 2015, '0', '0'),
(228, 76, 2016, '0', '0'),
(229, 77, 2014, '72', '332'),
(230, 77, 2015, '10', '10'),
(231, 77, 2016, '83', '332'),
(232, 78, 2014, '0', '0'),
(233, 78, 2015, '0', '0'),
(234, 78, 2016, '0', '0'),
(235, 79, 2014, '2', '40'),
(236, 79, 2015, '0', '0'),
(237, 79, 2016, '0', '0'),
(238, 80, 2014, '2', '4'),
(239, 80, 2015, '0', '0'),
(240, 80, 2016, '0', '0'),
(241, 81, 2014, '50', '100'),
(242, 81, 2015, '20', '46'),
(243, 81, 2016, '0', '0'),
(244, 82, 2014, '0', '0'),
(245, 82, 2015, '0', '0'),
(246, 82, 2016, '0', '0'),
(247, 83, 2014, '318', '13550'),
(248, 83, 2015, '75', '8275'),
(249, 83, 2016, '412', '1648'),
(250, 84, 2014, '32', '91'),
(251, 84, 2015, '105', '29'),
(252, 84, 2016, '214', '212'),
(253, 85, 2014, '7', '10'),
(254, 85, 2015, '165', '183'),
(255, 85, 2016, '83', '89'),
(256, 86, 2014, '92', '624'),
(257, 86, 2015, '150', '168'),
(258, 86, 2016, '89', '96'),
(259, 87, 2014, '230', '230'),
(260, 87, 2015, '520', '710'),
(261, 87, 2016, '1641', '1641'),
(262, 88, 2014, '90', '660'),
(263, 88, 2015, '150', '168'),
(264, 88, 2016, '135', '292'),
(265, 89, 2014, '1705', '12525'),
(266, 89, 2015, '25', '50'),
(267, 89, 2016, '305', '1020'),
(268, 90, 2014, '0', '0'),
(269, 90, 2015, '0', '0'),
(270, 90, 2016, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pertanian_jenis`
--

CREATE TABLE `lp_pertanian_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pertanian_jenis`
--

INSERT INTO `lp_pertanian_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Tanaman Sayuran dan Buah-Buahan Semusim'),
(2, 'Tanaman Sayuran dan Buah Buahan Komoditas'),
(3, 'Tanaman Biofarmaka Komoditas'),
(4, 'Tanaman Hias Komoditas');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pertanian_sub_jenis`
--

CREATE TABLE `lp_pertanian_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` varchar(86) NOT NULL,
  `satuan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pertanian_sub_jenis`
--

INSERT INTO `lp_pertanian_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`, `satuan`) VALUES
(1, 1, 'Bawang Daun', 'kuintal'),
(2, 1, 'Bawang Merah', 'Kuintal'),
(3, 1, 'Bawang Putih', 'Kuintal'),
(4, 1, 'Bayam', 'Kuintal'),
(5, 1, 'Blewah', 'Kuintal'),
(6, 1, 'Buncis', 'Kuintal'),
(7, 1, 'Cabai Besar', 'Kuintal'),
(8, 1, 'Cabai Rawit', 'Kuintal'),
(9, 1, 'Jamur', 'Kuintal'),
(10, 1, 'Kacang Merah', 'Kuintal'),
(11, 1, 'Kacang Panjang', 'Kuintal'),
(12, 1, 'Kangkung', 'Kuintal'),
(13, 1, 'Kembang Kol', 'Kuintal'),
(14, 1, 'Kentang', 'Kuintal'),
(15, 1, 'Ketimun', 'Kuintal'),
(16, 1, 'Kubis', 'Kuintal'),
(17, 1, 'Labu Siam', 'Kuintal'),
(18, 1, 'Lobak', 'Kuintal'),
(19, 1, 'Melon', 'Kuintal'),
(20, 1, 'Paprika', 'Kuintal'),
(21, 1, 'Petai/Sawi', 'Kuintal'),
(22, 1, 'Semangka', 'Kuintal'),
(23, 1, 'Stroberi', 'Kuintal'),
(24, 1, 'Terong', 'Kuintal'),
(25, 1, 'Tomat', 'Kuintal'),
(26, 1, 'Wortel', 'Kuintal'),
(27, 2, 'Alpukat', 'Kuintal'),
(28, 2, 'Anggur', 'Kuintal'),
(29, 2, 'Apel', 'Kuintal'),
(30, 2, 'Belimbing', 'Kuintal'),
(31, 2, 'Duku / Langsat', 'Kuintal'),
(32, 2, 'Durian', 'Kuintal'),
(33, 2, 'Jambu Air', 'Kuintal'),
(34, 2, 'Jambu Biji', 'Kuintal'),
(35, 2, 'Jengkol', 'Kuintal'),
(36, 2, 'Jeruk Besar', 'Kuintal'),
(37, 2, 'Jeruk Siam / Keprok', 'Kuintal'),
(38, 2, 'Mangga', 'Kuintal'),
(39, 2, 'Manggis', 'Kuintal'),
(40, 2, 'Markisa', 'Kuintal'),
(41, 2, 'Melinjo', 'Kuintal'),
(42, 2, 'Nangka / Cikampek', 'Kuintal'),
(43, 2, 'Nanas', 'Kuintal'),
(44, 2, 'Pepaya', 'Kuintal'),
(45, 2, 'Petai', 'Kuintal'),
(46, 2, 'Pisang', 'Kuintal'),
(47, 2, 'Rambutan', 'Kuintal'),
(48, 2, 'Salak', 'Kuintal'),
(49, 2, 'Sawo', 'Kuintal'),
(50, 2, 'Sirsak', 'Kuintal'),
(51, 2, 'Sukun', 'Kuintal'),
(52, 3, 'Dlingo', 'Kg'),
(53, 3, 'Jahe', 'Kg'),
(54, 3, 'Kapulaga', 'Kg'),
(55, 3, 'Keji Beling', 'Kg'),
(56, 3, 'Kencur', 'Kg'),
(57, 3, 'Kunyit', 'Kg'),
(58, 3, 'Laos / Lengkuas', 'Kg'),
(59, 3, 'Lempuyang', 'Kg'),
(60, 3, 'Lidah Buaya', 'Kg'),
(61, 3, 'Mahkota Dewa', 'Kg'),
(62, 3, 'Mengkudu', 'Kg'),
(63, 3, 'Sambiloto', 'Kg'),
(64, 3, 'Temuireng', 'Kg'),
(65, 3, 'Temukunci', 'Kg'),
(66, 3, 'Temulawak', 'Kg'),
(67, 4, 'Adenium (Kamboja Jepang)', 'Pohon'),
(68, 4, 'Aglonema', 'Pohon'),
(69, 4, 'Anggrek', 'Tangkai'),
(70, 4, 'Anthurium Bunga', 'Tangkai'),
(71, 4, 'Anthurium Daun', 'Pohon'),
(72, 4, 'Anyelir', 'Tangkai'),
(73, 4, 'Caladium', 'Pohon'),
(74, 4, 'Cordyline', 'Pohon'),
(75, 4, 'Diffenbachia', 'Pohon'),
(76, 4, 'Dracaena', 'Pohon'),
(77, 4, 'Euphorbia', 'Pohon'),
(78, 4, 'Gerbera (Herbras)', 'Tangkai'),
(79, 4, 'Gladiol', 'Tangkai'),
(80, 4, 'Heliconia (Pisang-Pisangan)', 'Tangkai'),
(81, 4, 'Ixora (Soka)', 'Pohon'),
(82, 4, 'Krisan', 'Tangkai'),
(83, 4, 'Mawar', 'Tangkai'),
(84, 4, 'Melati', 'Kg'),
(85, 4, 'Monstera', 'Pohon'),
(86, 4, 'Pakis', 'Pohon'),
(87, 4, 'Palem', 'Pohon'),
(88, 4, 'Phylodendron', 'Pohon'),
(89, 4, 'Sansevieria', 'Rumpun'),
(90, 4, 'Sedap Malam', 'Tangkai');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pidana`
--

CREATE TABLE `lp_pidana` (
  `id_pidana` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `lapor` varchar(10) NOT NULL,
  `selesai` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pidana`
--

INSERT INTO `lp_pidana` (`id_pidana`, `id_jenis`, `th`, `lapor`, `selesai`) VALUES
(1, 1, 2015, '1', '0'),
(2, 1, 2016, '0', '2'),
(3, 2, 2014, ' 3', ' 2'),
(4, 2, 2015, ' 47', ' 29'),
(5, 2, 2016, ' 17', ' 6'),
(6, 4, 2014, ' 96', '81'),
(7, 4, 2015, '40', '37'),
(8, 4, 2016, '57', '24'),
(9, 5, 2014, ' 19', ' 16'),
(10, 5, 2015, ' 12', ' 6'),
(11, 5, 2016, ' 29', ' 13'),
(12, 6, 2014, '66', '57'),
(13, 6, 2015, ' 64', '61'),
(14, 6, 2016, ' 86', ' 61'),
(15, 7, 2014, ' 27', ' 16'),
(16, 7, 2015, ' 136', ' 87'),
(17, 7, 2016, ' 125', '75'),
(18, 8, 2015, '3', '0'),
(19, 8, 2016, ' 1', '0'),
(20, 9, 2014, '82', '46'),
(21, 9, 2015, '86', '20'),
(22, 9, 2016, ' 85', ' 27'),
(23, 11, 2014, '10', '16'),
(24, 11, 2015, '4', ' 7'),
(25, 11, 2016, ' 16', ' 19'),
(26, 12, 2014, '2', '0'),
(27, 12, 2015, '1', '1'),
(28, 13, 2014, '0', '1'),
(29, 13, 2015, '1', '4'),
(30, 14, 2014, '16', '15'),
(31, 14, 2015, '20', '22'),
(32, 14, 2016, ' 72', ' 40'),
(33, 15, 2014, '116', '103'),
(34, 15, 2015, '335', ' 148'),
(35, 15, 2016, ' 450', ' 179'),
(36, 16, 2014, '379', '158'),
(37, 16, 2015, '871', ' 176'),
(38, 16, 2016, '1187', ' 151'),
(39, 17, 2014, '0', '24'),
(40, 17, 2015, '2', ' 31'),
(41, 17, 2016, ' 79', ' 48'),
(42, 18, 2014, '140', '112'),
(43, 18, 2015, '231', '152'),
(44, 18, 2016, ' 424', ' 150'),
(45, 19, 2014, '3', '3'),
(46, 19, 2015, '7', '2'),
(47, 19, 2016, ' 14', ' 3'),
(48, 20, 2014, '2', '3'),
(49, 20, 2015, '1', '1'),
(50, 20, 2016, '3', '2'),
(51, 21, 2014, '4', '6'),
(52, 21, 2015, '10', '5'),
(53, 21, 2016, '6', '2'),
(54, 22, 2015, '0', '1'),
(55, 22, 2016, '4', '7'),
(56, 23, 2014, '53', '53'),
(57, 23, 2015, '45', '101'),
(58, 23, 2016, ' 46', '63'),
(59, 24, 2014, '11', '9'),
(60, 24, 2015, '39', ' 14'),
(61, 24, 2016, '16', ' 4'),
(62, 25, 2014, '1', '0'),
(63, 25, 0, '1', ''),
(64, 25, 2016, '1', '0'),
(65, 26, 2014, '92', '92'),
(66, 26, 2015, '170', ' 171'),
(67, 26, 2016, ' 136', '136'),
(68, 27, 2014, '61', '61'),
(69, 27, 2015, '10', '15'),
(70, 27, 2016, '1', '2'),
(71, 27, 2015, '2', '2'),
(72, 29, 2014, '20', '16'),
(73, 29, 2015, '9', '3'),
(74, 29, 2016, '9', '8'),
(75, 30, 2014, '180', '89'),
(76, 30, 2015, '223', ' 54'),
(77, 30, 2016, ' 280', '76'),
(78, 31, 2014, '103', '87'),
(79, 31, 2015, '146', ' 53'),
(80, 31, 2016, '166', ' 67'),
(81, 32, 2014, '1', '3'),
(82, 32, 2015, '6', '1'),
(83, 32, 2016, '8', '8'),
(84, 33, 2014, '2', '1'),
(85, 33, 2015, '2', '0'),
(86, 33, 2016, '1', '0'),
(87, 37, 2014, '3', '3'),
(88, 37, 2015, '2', '1'),
(89, 37, 2016, '3', '1'),
(90, 38, 2014, '14', '11'),
(91, 38, 2015, '15', '6'),
(92, 38, 2016, '17', '4'),
(93, 39, 2015, '1', '0'),
(94, 40, 2014, '7', '9'),
(95, 40, 2015, '6', '8'),
(96, 41, 2014, '1', '0'),
(97, 41, 2016, '3', '1'),
(98, 42, 2014, '15', '6'),
(99, 42, 2015, '19', '2'),
(100, 42, 2016, '25', '7'),
(101, 43, 2015, '2', '3'),
(102, 43, 2016, '1', '1'),
(103, 44, 2014, '32', '32'),
(104, 44, 2015, '38', '25'),
(105, 44, 2016, '42', '41'),
(106, 45, 2014, '132', '110'),
(107, 45, 2015, '184', ' 200'),
(108, 45, 2016, '365', '315'),
(109, 46, 2014, '217', '13'),
(110, 46, 2015, '198', '2'),
(111, 46, 2016, ' 229', '1'),
(112, 47, 2014, '23', '24'),
(113, 47, 2015, '52', ' 7'),
(114, 47, 2016, '19', '4'),
(115, 48, 2014, '41', '23'),
(116, 48, 2015, '62', '41'),
(117, 48, 2016, '54', ' 22'),
(118, 49, 2014, '33', '25'),
(119, 49, 2015, '93', '16'),
(120, 49, 2016, '69', '31'),
(121, 50, 2014, '2', '2'),
(122, 50, 2015, '1', '0'),
(123, 50, 2016, '1', '0'),
(124, 51, 2014, '1', '0'),
(125, 51, 2015, '0', '1'),
(126, 51, 2016, '1', '0'),
(127, 52, 2014, '4', '1'),
(128, 52, 2015, '8', '0'),
(129, 52, 2016, '1', '0'),
(130, 53, 2014, '71', '125'),
(131, 53, 2015, '35', ' 124'),
(132, 53, 2016, '92', '115');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pidana_jenis`
--

CREATE TABLE `lp_pidana_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pidana_jenis`
--

INSERT INTO `lp_pidana_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Pembunuhan'),
(2, 'Penemuan Mayat'),
(3, 'Akibat Orang Mati'),
(4, 'Penganiayaan Berat (Anirat)'),
(5, 'Pengrusakan'),
(6, 'Pengroyokan'),
(7, 'Penganiayaan Ringan (Aniring)'),
(8, 'Mengakibatkan Orang Luka'),
(9, 'KDRT'),
(10, 'Penganiayaan Dalam Keluarga'),
(11, 'Senjata Tajam (Sajam)'),
(12, 'Penculikan'),
(13, 'Bawa Lari Gadis'),
(14, 'Pencurian Dengan Kekerasan\r\n(Curas)'),
(15, 'Pencurian Dengan Pemberatan\r\n(Curat)'),
(16, 'Curanmor'),
(17, 'Penadahan'),
(18, 'Curi Biasa'),
(19, 'Percobaan Pencurian'),
(20, 'Perkosaan'),
(21, 'Perzinahan'),
(22, 'Pornografi'),
(23, 'Perjudian'),
(24, 'Kebakaran'),
(25, 'Pembakaran'),
(26, 'Narkoba'),
(27, 'Minuman Keras (Miras)'),
(28, 'Jual Obat Keras\r\n'),
(29, 'Pemerasan'),
(30, 'Penipuan'),
(31, 'Penggelapan'),
(32, 'Perampasan'),
(33, 'Korupsi'),
(34, 'Pemberian Suap'),
(35, 'Penerimaan Suap'),
(36, 'Penyelundupan'),
(37, 'Penghinaan'),
(38, 'Perbuatan Tidak Enak (PTE)'),
(39, 'Martabat Presiden'),
(40, 'Kejahatan Asusila'),
(41, 'Pengancaman'),
(42, 'Pemalsuan Surat'),
(43, 'Uang Palsu'),
(44, 'Kecelakaan Meninggal Dunia'),
(45, 'Kecelakaan Mengakibatkan Luka'),
(46, 'Informasi Transaksi Elektronik (ITE)'),
(47, 'Fiducia'),
(48, 'Perlindungan Anak'),
(49, 'Pergi Tanpa Pamit'),
(50, 'Serobot Tanah'),
(51, 'Perbankan'),
(52, 'Ketertiban Umum'),
(53, 'Lain-Lain');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pidana_kec`
--

CREATE TABLE `lp_pidana_kec` (
  `id_pidana` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_lapor` varchar(10) NOT NULL,
  `jml_selesai` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pidana_kec`
--

INSERT INTO `lp_pidana_kec` (`id_pidana`, `id_kec`, `th`, `jml_lapor`, `jml_selesai`) VALUES
(1, 'KEC1001', 2015, '216', '165'),
(2, 'KEC1001', 2016, '203', '112'),
(3, 'KEC1002', 2015, '196', '106'),
(4, 'KEC1002', 2016, '265', '98'),
(5, 'KEC1003', 2015, '188', '154'),
(6, 'KEC1003', 2016, '244', '148'),
(7, 'KEC1004', 2015, '234', '99'),
(8, 'KEC1004', 2016, '232', '85'),
(9, 'KEC1005', 2015, '523 ', '253'),
(10, 'KEC1005', 2016, '548', '167'),
(11, '', 2015, '1984', '864'),
(12, '', 2016, '2749', '1106');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pmks`
--

CREATE TABLE `lp_pmks` (
  `id_pmks` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_l` varchar(10) NOT NULL,
  `jml_p` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pmks`
--

INSERT INTO `lp_pmks` (`id_pmks`, `id_jenis`, `th`, `jml_l`, `jml_p`) VALUES
(1, 1, 2014, '2', '2'),
(2, 1, 2015, '111', '89'),
(3, 1, 2016, '2', '3'),
(4, 2, 2014, '145', '83'),
(5, 2, 2015, '79', '53'),
(6, 2, 2016, '79', '53'),
(7, 3, 2014, '196', '31'),
(8, 3, 2015, '12', '3'),
(9, 3, 2016, '56', '48'),
(10, 4, 2014, '40', '17'),
(11, 4, 2015, '67', '63'),
(12, 5, 2014, '10', '8'),
(13, 5, 2015, '7', '0'),
(14, 5, 2016, '20', '5'),
(15, 6, 2014, '48', '22'),
(16, 6, 2015, '41', '41'),
(17, 6, 2016, '0', '5'),
(18, 7, 2014, '0', '2'),
(19, 7, 2015, '1', '0'),
(20, 7, 2016, '1', '0'),
(21, 8, 2014, '150', '19'),
(22, 8, 2015, '41', '3'),
(23, 8, 2016, '41', '3'),
(24, 9, 2014, '26', '30'),
(25, 9, 2015, '9', '1'),
(26, 9, 2016, '9', '1'),
(27, 10, 2015, '13', '26'),
(28, 10, 2016, '13', '26'),
(29, 11, 2014, '32', '57'),
(30, 11, 2015, '41', '44'),
(31, 12, 2014, '15', '20'),
(32, 12, 2015, '1', '1'),
(33, 12, 2016, '78', '83'),
(34, 13, 2014, '48', '3'),
(35, 13, 2015, '10', '1'),
(36, 14, 2014, '0', '35'),
(37, 14, 2015, '0', '4'),
(38, 14, 2016, '0', '4'),
(39, 15, 2014, '46', '87'),
(40, 15, 2015, '0', '1'),
(41, 15, 2016, '0', '1'),
(42, 17, 2014, '65', '109'),
(43, 17, 2015, '830', '1349'),
(44, 17, 2016, '830', '1349'),
(45, 18, 2014, '279', '71'),
(46, 20, 2014, '13', '18'),
(47, 20, 2015, '95', '25'),
(48, 20, 2016, '95', '25'),
(49, 21, 2014, '74', '110'),
(50, 21, 2016, '32', '39'),
(51, 22, 2014, '305', '197'),
(52, 22, 2015, '755', '642'),
(53, 22, 2016, '755', '642'),
(54, 23, 2014, '0', '303'),
(55, 23, 2015, '3', '916'),
(56, 23, 2016, '3', '916'),
(57, 24, 2014, '0', '35'),
(58, 24, 2016, '0', '4'),
(59, 25, 2014, '20946', '15340'),
(60, 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `lp_pmks_jenis`
--

CREATE TABLE `lp_pmks_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_pmks_jenis`
--

INSERT INTO `lp_pmks_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Anak Balita Terlantar'),
(2, 'Anak dengan Disabilitas'),
(3, 'Anak Jalanan'),
(4, 'Anak Terlantar'),
(5, 'Anak yang berhadapan dengan Hukum'),
(6, 'Anak yang Membutuhkan Perlindungan'),
(7, 'Anak yang menjadi korban tindak kekerasan\r\n'),
(8, 'Bekas Warga Binaan Pemasyarakatan\r\n'),
(9, 'Gelandangan dan Gelandangan Psikotik'),
(10, 'Kelompok Minoritas'),
(11, 'Keluarga Bermasalah Sosial Psikologis'),
(12, 'Korban Bencana Alam\r\n'),
(13, 'Korban Penyalagunaan NAPZA\r\n'),
(14, 'Korban Tindak Kekerasan atau yang diperlakukan salah\r\n'),
(15, 'Korban Trafficking/Keluarga Rentan\r\n'),
(16, 'Korban Bencana Sosial\r\n'),
(17, 'Lanjut Usia Telantar\r\n'),
(18, 'Orang dengan HIV/AIDS (ODHA)'),
(19, 'Pekerja Migran Bermasalah Sosial'),
(20, 'Pemulung\r\n'),
(21, 'Pengemis'),
(22, 'Penyandang Disabilitas (Orang dengan kedisabilitasan (ODK & bekas penderita penyakit kronis)'),
(23, 'Perempuan Rawan Sosial Ekonomi\r\n'),
(24, 'Tuna susila\r\n'),
(25, 'Fakir Miskin');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tehnologi`
--

CREATE TABLE `lp_tehnologi` (
  `id_tehnologi` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tehnologi`
--

INSERT INTO `lp_tehnologi` (`id_tehnologi`, `id_sub_jenis`, `th`, `jml`) VALUES
(1, 1, 2014, '57'),
(2, 1, 2015, '57'),
(3, 1, 2016, '57'),
(4, 2, 2014, '317'),
(5, 2, 2015, '342'),
(6, 2, 2016, '385'),
(7, 3, 2014, '9'),
(8, 3, 2015, '7'),
(9, 3, 2016, '5'),
(10, 4, 2014, '57'),
(11, 4, 2016, '57'),
(12, 5, 2014, '1'),
(13, 5, 2015, '1'),
(14, 5, 2016, '1'),
(15, 6, 2014, '18'),
(16, 4, 2015, '57'),
(17, 6, 2015, '17'),
(18, 6, 2016, '16'),
(19, 7, 2014, '1'),
(20, 7, 2015, '1'),
(21, 7, 2016, '1'),
(22, 8, 2014, '8'),
(23, 8, 2015, '8'),
(24, 8, 2016, '10'),
(25, 9, 2014, '92'),
(26, 9, 2015, '96'),
(27, 9, 2016, '97'),
(28, 10, 2014, '57'),
(29, 10, 2015, '57'),
(30, 10, 2016, '57'),
(31, 11, 2014, '317'),
(32, 11, 2015, '342'),
(33, 11, 2016, '385'),
(34, 12, 2014, '9'),
(35, 12, 2015, '7'),
(36, 12, 2016, '5'),
(37, 13, 2014, '57'),
(38, 13, 2015, '57'),
(39, 13, 2016, '57'),
(40, 14, 2014, '1'),
(41, 14, 2015, '1'),
(42, 14, 2016, '1'),
(43, 15, 2014, '18'),
(44, 15, 2015, '17'),
(45, 15, 2016, '16');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tehnologi_jenis`
--

CREATE TABLE `lp_tehnologi_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tehnologi_jenis`
--

INSERT INTO `lp_tehnologi_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Jumlah Sarana Pendukung Komunikasi dan Informasi'),
(2, 'Data Sistem Informasi ');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tehnologi_sub_jenis`
--

CREATE TABLE `lp_tehnologi_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL,
  `satuan` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tehnologi_sub_jenis`
--

INSERT INTO `lp_tehnologi_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`, `satuan`) VALUES
(1, 1, 'Layanan Telepon Seluler', 'Kelurahan'),
(2, 1, 'Jumlah BTS', 'BTS'),
(3, 1, 'Jumlah Provider', 'Provider '),
(4, 1, 'Layanan Internet', 'Kelurahan'),
(5, 1, 'Radio Pemerintah', 'Radio'),
(6, 1, 'Radio Swasta', 'Radio'),
(7, 1, 'Televisi Pemerintah', 'TV'),
(8, 1, 'Televisi Swasta', 'TV'),
(9, 1, 'Warnet', 'Warnet'),
(10, 2, 'Domain Pemkot Malang', 'Domain'),
(11, 2, 'Website SKPD Pemkot Malang', 'Website'),
(12, 2, 'Aplikasi Layanan Pemkot Malang', 'Aplikasi'),
(13, 2, 'Jumlah Pelayanan Internet di Area Publik Pemkot Malang', 'Lokasi'),
(14, 2, 'Jumlah Fasilitasi Jaringan Internet di Lingkup Kota Malang', 'Lokasi '),
(15, 2, 'Kelompok Informasi Masyarakat (KIM)', 'KIM');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tenaga`
--

CREATE TABLE `lp_tenaga` (
  `id_tenaga` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tenaga`
--

INSERT INTO `lp_tenaga` (`id_tenaga`, `id_jenis`, `id_kategori`, `th`, `jml`) VALUES
(1, 1, 1, 2015, '45'),
(2, 1, 1, 2016, '20'),
(3, 1, 2, 2015, '23'),
(4, 1, 2, 2016, '131'),
(5, 1, 3, 2015, '58115200310'),
(6, 1, 3, 2016, '62297965035'),
(7, 2, 1, 2015, '15'),
(8, 2, 1, 2016, '7'),
(9, 2, 2, 2015, '32'),
(10, 2, 2, 2016, '19'),
(11, 2, 3, 2015, '7631000000'),
(12, 2, 3, 2016, '15762500000'),
(13, 3, 1, 2015, '1'),
(14, 3, 1, 2016, '0'),
(15, 3, 2, 2015, '0'),
(16, 3, 2, 2016, '0'),
(17, 3, 3, 2015, '202000000'),
(18, 3, 3, 2016, '0'),
(19, 4, 1, 2015, '32'),
(20, 4, 1, 2016, '3'),
(21, 4, 2, 2015, '26'),
(22, 4, 2, 2016, '27'),
(23, 4, 3, 2015, '31802450000'),
(24, 4, 3, 2016, '1440000000'),
(25, 5, 1, 2015, '6'),
(26, 5, 1, 2016, '3'),
(27, 5, 2, 2015, '25'),
(28, 5, 2, 2016, '18'),
(29, 5, 3, 2015, '7110000000'),
(30, 5, 3, 2016, '11500000000'),
(31, 6, 1, 2015, '9'),
(32, 6, 1, 2016, '15'),
(33, 6, 2, 2015, '381'),
(34, 6, 2, 2016, '298'),
(35, 6, 3, 2015, ' 148695330000'),
(36, 6, 3, 2016, '33123136891'),
(37, 7, 1, 2015, '304'),
(38, 7, 1, 2016, '391'),
(39, 7, 2, 2015, '449'),
(40, 7, 2, 2016, '1420'),
(41, 7, 3, 2015, '138829196884'),
(42, 7, 3, 2016, '156525527137'),
(43, 8, 1, 2015, '0'),
(44, 8, 1, 2016, '1'),
(45, 8, 2, 2015, '0'),
(46, 8, 2, 2016, '124'),
(47, 8, 3, 2015, '0'),
(48, 8, 3, 2016, '25000000000'),
(49, 9, 1, 2015, '0'),
(50, 9, 1, 2016, '25'),
(51, 9, 2, 2015, '0'),
(52, 9, 2, 2016, '568'),
(53, 9, 3, 2015, '0'),
(54, 9, 3, 2016, '50835039094'),
(55, 10, 1, 2015, '29'),
(56, 10, 1, 2016, '25'),
(57, 10, 2, 2015, '52'),
(58, 10, 2, 2016, '228'),
(59, 10, 3, 2015, '103435924000'),
(60, 10, 3, 2016, '148572325000'),
(61, 11, 1, 2015, '0'),
(62, 11, 1, 2016, '0'),
(63, 11, 2, 2015, '0'),
(64, 11, 2, 2016, '0'),
(65, 11, 3, 2015, '0'),
(66, 11, 3, 2016, '0'),
(67, 12, 1, 2015, '142'),
(68, 12, 1, 2016, '120'),
(69, 12, 2, 2015, '254'),
(70, 12, 2, 2016, '567'),
(71, 12, 3, 2015, '181221614177'),
(72, 12, 3, 2016, '227123468310'),
(73, 13, 1, 2015, '0'),
(74, 13, 1, 2016, '0'),
(75, 13, 2, 2015, '0'),
(76, 13, 2, 2016, '0'),
(77, 14, 1, 2015, '40'),
(78, 14, 1, 2016, '37'),
(79, 14, 2, 2015, '81'),
(80, 14, 2, 2016, '144'),
(81, 14, 3, 2015, '27783274130'),
(82, 14, 3, 2016, '109346000000'),
(83, 15, 1, 2015, '22'),
(84, 15, 1, 2016, '22'),
(85, 15, 2, 2015, '29'),
(86, 15, 2, 2016, '109'),
(87, 15, 3, 2015, '18030000000'),
(88, 15, 3, 2016, '29828611852'),
(89, 16, 1, 2015, '10'),
(90, 16, 1, 2016, '23'),
(91, 16, 2, 2015, '39'),
(92, 16, 2, 2016, '373'),
(93, 16, 3, 2015, '46244984679'),
(94, 16, 3, 2016, '219740032558'),
(95, 17, 1, 2015, '8'),
(96, 17, 1, 2016, '12'),
(97, 17, 2, 2015, '60'),
(98, 17, 2, 2016, '89'),
(99, 17, 3, 2015, '3210159136'),
(100, 17, 3, 2016, '14661125118'),
(101, 18, 1, 2015, '113'),
(102, 18, 1, 2016, '127'),
(103, 18, 2, 2015, '145'),
(104, 18, 2, 2016, '545'),
(105, 18, 3, 2015, '66096182783'),
(106, 18, 3, 2016, '992499787832');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tenaga_jenis`
--

CREATE TABLE `lp_tenaga_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tenaga_jenis`
--

INSERT INTO `lp_tenaga_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Pertanian (Antara lain : Penggilingan Padi Dll.)'),
(2, 'Peternakan'),
(3, 'Perikanan'),
(4, 'Perkebunan / Kehutanan'),
(5, 'Pertambangan dan Galian Gol C'),
(6, 'Perindustrian'),
(7, 'Perdagangan'),
(8, 'Perhotelan'),
(9, 'Restoran / Rumah Makan Cafe'),
(10, 'Perumahan dan Ruko'),
(11, 'Perkantoran, Supermarket, dan Super Mall'),
(12, 'Jasa Konstruksi'),
(13, 'Pergudangan'),
(14, 'Transportasi Darat / Laut'),
(15, 'Kesehatan'),
(16, 'Koperasi'),
(17, 'Jasa Hiburan / Rekreasi'),
(18, 'Lain Lain');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tenaga_kategori`
--

CREATE TABLE `lp_tenaga_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tenaga_kategori`
--

INSERT INTO `lp_tenaga_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Jumlah Unit Usaha'),
(2, 'Jumlah Tenaga Kerja (Orang)'),
(3, 'Modal / Investasi');

-- --------------------------------------------------------

--
-- Table structure for table `lp_terminal`
--

CREATE TABLE `lp_terminal` (
  `id_terminal` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_sub_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_terminal`
--

INSERT INTO `lp_terminal` (`id_terminal`, `id_jenis`, `id_sub_jenis`, `th`, `jml`) VALUES
(1, 1, 1, 2014, '1'),
(2, 1, 1, 2015, '1'),
(3, 1, 1, 2016, '1'),
(4, 1, 2, 2014, '2'),
(5, 1, 2, 2015, '2'),
(6, 1, 2, 2016, '2'),
(7, 1, 3, 2014, '2'),
(8, 1, 3, 2015, '2'),
(9, 1, 3, 2016, '2'),
(10, 2, 4, 2014, '2659'),
(11, 2, 4, 2015, '2490'),
(12, 2, 4, 2016, '2900'),
(13, 2, 5, 2014, '2347'),
(14, 2, 5, 2015, '2297'),
(15, 2, 5, 2016, '2374'),
(16, 2, 6, 2014, '7465'),
(17, 2, 6, 2015, '7850'),
(18, 2, 6, 2016, '7923'),
(19, 2, 7, 2014, '134'),
(20, 2, 7, 2015, '113'),
(21, 2, 7, 2016, '127'),
(22, 2, 8, 2014, '49'),
(23, 2, 8, 2015, '70'),
(24, 2, 8, 2016, '83'),
(25, 3, 9, 2014, '6'),
(26, 3, 9, 2015, '6'),
(27, 3, 9, 2016, '6'),
(28, 3, 10, 2014, '6'),
(29, 3, 10, 2015, '6'),
(30, 3, 10, 2016, '6'),
(31, 3, 11, 2014, '6'),
(32, 3, 11, 2015, '6'),
(33, 3, 11, 2016, '6'),
(34, 3, 12, 2014, '6'),
(35, 3, 12, 2015, '6'),
(36, 3, 12, 2016, '6'),
(37, 3, 13, 2014, '6'),
(38, 3, 13, 2015, '6'),
(39, 3, 13, 2016, '6'),
(40, 3, 14, 2014, '6'),
(41, 3, 14, 2015, '6'),
(42, 3, 14, 2016, '6'),
(43, 4, 17, 2014, '179'),
(44, 4, 17, 2015, '179'),
(45, 4, 17, 2016, '179'),
(46, 4, 18, 2014, '1.30'),
(47, 4, 18, 2015, '1.30'),
(48, 4, 18, 2016, '1.30'),
(49, 4, 19, 2014, '4'),
(50, 4, 19, 2015, '4'),
(51, 4, 19, 2016, '4'),
(52, 4, 20, 2014, '37'),
(53, 4, 20, 2015, '37'),
(54, 4, 20, 2016, '37'),
(55, 4, 21, 2014, '25'),
(56, 4, 21, 2015, '25'),
(57, 4, 21, 2016, '25'),
(58, 5, 0, 2014, '25'),
(59, 5, 0, 2015, '25'),
(60, 5, 0, 2016, '25');

-- --------------------------------------------------------

--
-- Table structure for table `lp_terminal_jenis`
--

CREATE TABLE `lp_terminal_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_terminal_jenis`
--

INSERT INTO `lp_terminal_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Jumlah Terminal (unit)'),
(2, 'Jumlah Uji KIR (unit)'),
(3, 'Lama Pengujian KIR (bulan)'),
(4, 'Fasilitas Perlengkapan Jalan'),
(5, 'Jumlah Trayek (unit)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_terminal_sub_jenis`
--

CREATE TABLE `lp_terminal_sub_jenis` (
  `id_sub_jenis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_sub_jenis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_terminal_sub_jenis`
--

INSERT INTO `lp_terminal_sub_jenis` (`id_sub_jenis`, `id_jenis`, `nama_sub_jenis`) VALUES
(1, 1, 'Kelas A '),
(2, 1, 'Kelas B'),
(3, 1, 'Kelas C'),
(4, 2, 'Mobil Penumpang Umum'),
(5, 2, 'Mobil Bus'),
(6, 2, 'Mobil Barang'),
(7, 2, 'Kereta Gandengan'),
(8, 2, 'Kereta Tempelan'),
(9, 3, 'Mobil Penumpang Umum'),
(10, 3, 'Mobil Bus'),
(11, 3, 'Mobil Barang'),
(12, 3, 'Mobil Barang'),
(13, 3, 'Kereta Gandengan'),
(14, 3, 'Kereta Tempelan'),
(17, 4, 'Trotoar (Km)'),
(18, 4, 'Jalur Sepeda (Km)'),
(19, 4, 'Tempat Penyebrangan Pejalan Kaki'),
(20, 4, 'Halte (Unit)'),
(21, 4, 'Fasilitas Khusus Penyandang Cacat (Km)');

-- --------------------------------------------------------

--
-- Table structure for table `lp_tpa`
--

CREATE TABLE `lp_tpa` (
  `id_tpa` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_tpa`
--

INSERT INTO `lp_tpa` (`id_tpa`, `id_kec`, `th`, `jml`) VALUES
(5, 'KEC1001', 2015, '13'),
(6, 'KEC1001', 2016, '8'),
(7, 'KEC1002', 2015, '15'),
(8, 'KEC1002', 2016, '19'),
(9, 'KEC1003', 2015, '12'),
(10, 'KEC1003', 2016, '9'),
(11, 'KEC1004', 2015, '17'),
(12, 'KEC1004', 2016, '19'),
(13, 'KEC1005', 2015, '11'),
(14, 'KEC1005', 2016, '12'),
(15, 'KEC1001', 2017, '8'),
(16, 'KEC1002', 2017, '19'),
(17, 'KEC1003', 2017, '9'),
(18, 'KEC1004', 2017, '19'),
(19, 'KEC1005', 2017, '12');

-- --------------------------------------------------------

--
-- Table structure for table `main_dinas`
--

CREATE TABLE `main_dinas` (
  `id_dinas` int(11) NOT NULL,
  `nama_dinas` varchar(500) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_dinas`
--

INSERT INTO `main_dinas` (`id_dinas`, `nama_dinas`, `alamat`) VALUES
(2, 'Dinas Perdagangan', 'Malang'),
(3, 'Dinas Komunikasi dan Informatika', 'Malang'),
(4, 'Dinas Kebudayaan dan Pariwisata', 'Malang'),
(6, 'Dinas Perindustrian', 'Malang'),
(7, 'Dinas Pendidikan', 'Malang'),
(8, 'Dinas Kesehatan', 'Malang'),
(9, 'Dinas Perhubungan', 'Malang'),
(10, 'Dinas Pertanian dan Ketahanan Pangan', 'Malang'),
(11, 'Dinas Perumahan dan Kawasan Pemukiman', 'Malang'),
(14, 'Dinas Koperasi dan Usaha Mikro', 'Malang'),
(15, 'Dinas Kepemudaan dan Olahraga', ''),
(16, 'Dinas Kepedudukan dan Pencatatan Sipil', ''),
(17, 'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana', ''),
(18, 'Dinas Lingkungan Hidup', ''),
(19, 'Dinas Perpustakaan Umum dan Arsip Daerah', ''),
(20, 'Badan Kesatuan Bangsa dan Politik', ''),
(25, 'Dinas Pertanian', 'Malang');

-- --------------------------------------------------------

--
-- Table structure for table `main_jenis_kendaraan`
--

CREATE TABLE `main_jenis_kendaraan` (
  `id_jenis_kendaraan` int(11) NOT NULL,
  `keterangan` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_jenis_kendaraan`
--

INSERT INTO `main_jenis_kendaraan` (`id_jenis_kendaraan`, `keterangan`) VALUES
(1, 'Mobil Penumpang'),
(2, 'Bus'),
(3, 'Truk'),
(4, 'Sepeda Motor');

-- --------------------------------------------------------

--
-- Table structure for table `main_jenis_penyakit`
--

CREATE TABLE `main_jenis_penyakit` (
  `id_jenis_penyakit` int(11) NOT NULL,
  `keterangan` varchar(64) NOT NULL,
  `jml` varchar(10) NOT NULL,
  `th` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_jenis_penyakit`
--

INSERT INTO `main_jenis_penyakit` (`id_jenis_penyakit`, `keterangan`, `jml`, `th`) VALUES
(3, 'Headache', '7966', 2016),
(4, 'Dermatitis Kontak Alergi', '8718', 2016),
(5, 'Penyakit Pulpa Dan Jar Perapikal', '8819', 2016),
(6, 'Myalgia / Nyeri Otot', '9025', 2016),
(7, 'Obs Febris', '10773', 2016),
(8, 'Influensa, Virus Tidak Diidentifikasi', '12743', 2016),
(9, 'DM', '13815', 2016),
(10, 'Gastritis', '13840', 2016),
(11, 'Hipertensi Primer', '32109', 2016),
(12, 'Infeksi Saluran Pernapasan', '55351', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `main_jenjang_pend`
--

CREATE TABLE `main_jenjang_pend` (
  `id_jenjang` int(11) NOT NULL,
  `ket` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `main_kecamatan`
--

CREATE TABLE `main_kecamatan` (
  `id_kec` varchar(7) NOT NULL,
  `nama_kec` varchar(50) NOT NULL,
  `latlng` text NOT NULL,
  `luas` double NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_kecamatan`
--

INSERT INTO `main_kecamatan` (`id_kec`, `nama_kec`, `latlng`, `luas`, `is_delete`) VALUES
('KEC1001', 'Kedungkandang', 'null', 39.87, '0'),
('KEC1002', 'Sukun', 'null', 20.97, '0'),
('KEC1003', 'Klojen', 'null', 8.83, '0'),
('KEC1004', 'Blimbing', 'null', 17.77, '0'),
('KEC1005', 'Lowokwaru', 'null', 22.6, '0');

-- --------------------------------------------------------

--
-- Table structure for table `main_kelurahan`
--

CREATE TABLE `main_kelurahan` (
  `id_kel` varchar(7) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `nama_kel` varchar(64) NOT NULL,
  `latlng_kel` text NOT NULL,
  `luas_kel` double NOT NULL,
  `is_del` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_kelurahan`
--

INSERT INTO `main_kelurahan` (`id_kel`, `id_kec`, `nama_kel`, `latlng_kel`, `luas_kel`, `is_del`) VALUES
('KEL1001', 'KEC1001', 'Bumiayu', '0', 12, '0'),
('KEL1002', 'KEC1001', 'Mergosono', '12', 12, '0'),
('KEL1003', 'KEC1001', 'Kotalama', '12', 12, '0'),
('KEL1004', 'KEC1001', 'Wonokoyo', '12', 12, '0'),
('KEL1005', 'KEC1001', 'Buring', '12', 12, '0'),
('KEL1006', 'KEC1001', 'Kedungkandang', '12', 12, '0'),
('KEL1007', 'KEC1001', 'Lesanpuro', '12', 12, '0'),
('KEL1008', 'KEC1001', 'Sawojajar', '12', 12, '0'),
('KEL1009', 'KEC1001', 'Madyopuro', '12', 12, '0'),
('KEL1010', 'KEC1001', 'Cemorokandang', '12', 12, '0'),
('KEL1011', 'KEC1001', 'Arjowinangun', '12', 12, '0'),
('KEL1012', 'KEC1001', 'Tlogowaru', '12', 12, '0'),
('KEL1013', 'KEC1002', 'Ciptomulyo', '12', 12, '0'),
('KEL1014', 'KEC1002', 'Gadang', '12', 12, '0'),
('KEL1015', 'KEC1002', 'Kebonsari', '12', 12, '0'),
('KEL1016', 'KEC1002', 'Bandungrejosari', '12', 12, '0'),
('KEL1017', 'KEC1002', 'Sukun', '12', 12, '0'),
('KEL1018', 'KEC1005', 'Tunggulwulung', '1', 12, '0'),
('KEL1019', 'KEC1005', 'Merjosari', '1', 1, '0'),
('KEL1020', 'KEC1005', 'Tlogomas', '1', 1, '0'),
('KEL1021', 'KEC1003', 'Klojen', '1', 1, '0'),
('KEL1022', 'KEC1003', 'Samaan', '1', 1, '0'),
('KEL1023', 'KEC1003', 'Rampalcelaket', '1', 1, '0'),
('KEL1024', 'KEC1004', 'Balearjosari', '1', 1, '0'),
('KEL1025', 'KEC1004', 'Arjosari', '1', 1, '0'),
('KEL1026', 'KEC1004', 'Polowijen', '1', 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `main_partai`
--

CREATE TABLE `main_partai` (
  `id_partai` int(2) NOT NULL,
  `nama_partai` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_partai`
--

INSERT INTO `main_partai` (`id_partai`, `nama_partai`, `is_delete`) VALUES
(1, 'Partai Kebangkitan Bangsa (PKB)', '0'),
(2, 'Partai Gerakan Indonesia Raya (Gerindra)', '0'),
(3, 'Partai Demokrasi Indonesia Perjuangan (PDIP)', '0'),
(4, 'Partai Golongan Karya (Golkar)', '0'),
(5, 'Partai Demokrat', '0'),
(6, 'Partai Keadilan Sejahtera (PKS)', '0'),
(7, 'Partai Amanat Nasional (PAN)', '0'),
(8, 'Partai Gerakan Indonesia Raya (Gerindra)', '0'),
(9, 'Partai Persatuan Pembangunan (PPP)', '0'),
(10, 'Partai Hati Nurani Rakyat (Hanura)', '0'),
(11, 'Partai Nasional Demokrat (Nasdem)', '0'),
(12, 'Partai Bulan Bintang', '0'),
(13, 'Partai Keadilan dan Persatuan Indonesia (PKPI)', '0'),
(14, 'rwergfd ', '1');

-- --------------------------------------------------------

--
-- Table structure for table `main_pendidikan`
--

CREATE TABLE `main_pendidikan` (
  `id_pendidikan` int(11) NOT NULL,
  `jenjang` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `miskin_jml_pend`
--

CREATE TABLE `miskin_jml_pend` (
  `id_pend_miskin` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL,
  `agregat` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `miskin_jml_pend`
--

INSERT INTO `miskin_jml_pend` (`id_pend_miskin`, `th`, `prosentase`, `agregat`) VALUES
(4, 2014, '4.80', '40.64'),
(5, 2015, '4.60', '39.10'),
(6, 2016, '4.33', '37.03');

-- --------------------------------------------------------

--
-- Table structure for table `miskin_pengeluaran_perkapita`
--

CREATE TABLE `miskin_pengeluaran_perkapita` (
  `id_perkapita` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `pengeluaran_perkapita` varchar(20) NOT NULL,
  `garis_kemiskinan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `miskin_pengeluaran_perkapita`
--

INSERT INTO `miskin_pengeluaran_perkapita` (`id_perkapita`, `th`, `pengeluaran_perkapita`, `garis_kemiskinan`) VALUES
(3, 2014, '1215502', '381400'),
(4, 2015, '1260186', '411709'),
(5, 2016, '1355476', '426527');

-- --------------------------------------------------------

--
-- Table structure for table `pem_dpr`
--

CREATE TABLE `pem_dpr` (
  `id_pem_dpr` int(11) NOT NULL,
  `id_partai` int(2) NOT NULL,
  `th` varchar(12) NOT NULL,
  `jk` enum('0','1') NOT NULL,
  `jml` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pem_dpr`
--

INSERT INTO `pem_dpr` (`id_pem_dpr`, `id_partai`, `th`, `jk`, `jml`) VALUES
(3, 2, '2014', '0', 3),
(7, 2, '2014', '1', 1),
(8, 11, '2014', '0', 1),
(9, 10, '2014', '0', 2),
(10, 10, '2014', '1', 1),
(11, 9, '2014', '0', 1),
(12, 9, '2014', '1', 2),
(13, 7, '2014', '0', 4),
(14, 6, '2014', '0', 3),
(15, 1, '2014', '0', 6),
(16, 5, '2014', '0', 3),
(17, 5, '2014', '1', 2),
(18, 4, '2014', '0', 4),
(19, 4, '2014', '1', 1),
(20, 3, '2014', '0', 7),
(21, 3, '2014', '1', 4),
(22, 11, '2014', '1', 0),
(23, 7, '2014', '1', 0),
(24, 1, '2014', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pem_jml_asn_jenjang`
--

CREATE TABLE `pem_jml_asn_jenjang` (
  `id_jml_asn` varchar(6) NOT NULL,
  `id_jenjang` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pem_jml_rt_rw`
--

CREATE TABLE `pem_jml_rt_rw` (
  `id_rt_rw` int(11) NOT NULL,
  `id_kec` varchar(7) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_rt` int(11) NOT NULL,
  `jml_rw` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pem_jml_rt_rw`
--

INSERT INTO `pem_jml_rt_rw` (`id_rt_rw`, `id_kec`, `th`, `jml_rt`, `jml_rw`) VALUES
(1, 'KEC1001', 2018, 892, 116),
(2, 'KEC1002', 2018, 882, 94),
(4, 'KEC1004', 2018, 923, 127),
(5, 'KEC1003', 2018, 675, 89),
(6, 'KEC1005', 2018, 785, 120),
(7, 'KEC1001', 2017, 891, 115),
(8, 'KEC1002', 2017, 881, 92),
(9, 'KEC1003', 2017, 673, 88),
(10, 'KEC1003', 2016, 670, 85),
(11, 'KEC1004', 2017, 922, 125),
(12, 'KEC1005', 2017, 783, 118),
(13, 'KEC1001', 2016, 890, 114),
(14, 'KEC1002', 2016, 880, 90),
(15, 'KEC1004', 2016, 920, 123),
(16, 'KEC1005', 2016, 781, 116);

-- --------------------------------------------------------

--
-- Table structure for table `pendapatan_lap_usaha`
--

CREATE TABLE `pendapatan_lap_usaha` (
  `id_lap_usaha` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendapatan_lap_usaha`
--

INSERT INTO `pendapatan_lap_usaha` (`id_lap_usaha`, `id_jenis`, `th`, `jml`) VALUES
(2, 5, 2016, '1231'),
(3, 7, 2014, '28.47'),
(4, 7, 2015, '28.9'),
(5, 7, 2016, '29.54'),
(6, 8, 2014, '27.14'),
(7, 8, 2015, '26.51'),
(8, 8, 2016, '25.4'),
(9, 9, 2014, '12.56'),
(10, 9, 2015, '12.52'),
(11, 9, 2016, '12.92');

-- --------------------------------------------------------

--
-- Table structure for table `pendapatan_lap_usaha_jenis`
--

CREATE TABLE `pendapatan_lap_usaha_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendapatan_lap_usaha_jenis`
--

INSERT INTO `pendapatan_lap_usaha_jenis` (`id_jenis`, `nama_jenis`) VALUES
(7, 'Perdagangan Besar dan Eceran; Reparasi Mobil dan Sepeda Motor'),
(8, 'Industri Pengolahan'),
(9, 'Konstruksi');

-- --------------------------------------------------------

--
-- Table structure for table `pendapatan_regional`
--

CREATE TABLE `pendapatan_regional` (
  `id_pendapatan` int(11) NOT NULL,
  `th_pendapatan` int(4) NOT NULL,
  `jml_pendapatan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendapatan_regional`
--

INSERT INTO `pendapatan_regional` (`id_pendapatan`, `th_pendapatan`, `jml_pendapatan`) VALUES
(4, 2014, '46563213.3'),
(5, 2015, '51824393.8'),
(6, 2016, '57171601.6');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_baca_tulis`
--

CREATE TABLE `pendidikan_baca_tulis` (
  `id_baca_tulis` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_baca_tulis`
--

INSERT INTO `pendidikan_baca_tulis` (`id_baca_tulis`, `id_jenis`, `th`, `prosentase`) VALUES
(3, 1, 2014, '97.45'),
(4, 1, 2015, '98.16'),
(5, 1, 2016, '98.17'),
(6, 2, 2014, '2.55'),
(7, 2, 2015, '1.84'),
(8, 2, 2016, '1.83');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_baca_tulis_jenis`
--

CREATE TABLE `pendidikan_baca_tulis_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_baca_tulis_jenis`
--

INSERT INTO `pendidikan_baca_tulis_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Huruf Latin dan  atau Lainnya'),
(2, 'Buta Huruf');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_jml_sklh`
--

CREATE TABLE `pendidikan_jml_sklh` (
  `id_jml_sklh` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_jml_sklh`
--

INSERT INTO `pendidikan_jml_sklh` (`id_jml_sklh`, `id_jenis`, `th`, `prosentase`) VALUES
(2, 3, 2015, '2314'),
(5, 4, 2014, '333'),
(6, 4, 2015, '344'),
(7, 4, 2016, '341'),
(9, 5, 2014, '7'),
(10, 5, 2015, '6'),
(11, 5, 2016, '7'),
(12, 6, 2014, '92'),
(13, 6, 2015, '100'),
(14, 6, 2016, '101'),
(15, 7, 2014, '270'),
(16, 7, 2015, '272'),
(17, 7, 2016, '274'),
(18, 8, 2014, '9'),
(19, 8, 2015, '8'),
(20, 8, 2016, '8'),
(21, 9, 2014, '4'),
(22, 9, 2015, '52'),
(23, 9, 2016, '43'),
(24, 10, 2014, '97'),
(25, 10, 2015, '101'),
(26, 10, 2016, '100'),
(27, 11, 2014, '8'),
(28, 11, 2015, '8'),
(29, 11, 2016, '8'),
(30, 12, 2014, '26'),
(31, 12, 2015, '30'),
(32, 12, 2016, '34'),
(33, 13, 2014, '38'),
(34, 13, 2015, '47'),
(35, 13, 2016, '45'),
(36, 14, 2014, '5'),
(37, 14, 2015, '6'),
(38, 14, 2016, '5'),
(39, 15, 2014, '38'),
(40, 15, 2015, '47'),
(41, 15, 2016, '45'),
(42, 16, 2014, '15'),
(43, 16, 2015, '15'),
(44, 16, 2016, '15');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_jml_sklh_jenis`
--

CREATE TABLE `pendidikan_jml_sklh_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_jml_sklh_jenis`
--

INSERT INTO `pendidikan_jml_sklh_jenis` (`id_jenis`, `nama_jenis`) VALUES
(4, 'TK'),
(5, 'TK LB'),
(6, 'RA'),
(7, 'SD'),
(8, 'SD LB'),
(9, 'MI'),
(10, 'SMP'),
(11, 'SMP LB'),
(12, 'MTS'),
(13, 'SMA'),
(14, 'SMA LB'),
(15, 'SMK'),
(16, 'MA');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_partisipasi_sklh`
--

CREATE TABLE `pendidikan_partisipasi_sklh` (
  `id_partisipasi_sklh` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_partisipasi_sklh`
--

INSERT INTO `pendidikan_partisipasi_sklh` (`id_partisipasi_sklh`, `id_jenis`, `th`, `prosentase`) VALUES
(1, 1, 2014, '100'),
(2, 1, 2015, '100'),
(3, 1, 2016, '100'),
(4, 2, 2014, '99.08'),
(5, 2, 2015, '98.95'),
(6, 2, 2016, '95.75'),
(7, 3, 2014, '71.59'),
(8, 3, 2015, '78.91'),
(9, 3, 2016, '78.32');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_partisipasi_sklh_jenis`
--

CREATE TABLE `pendidikan_partisipasi_sklh_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_partisipasi_sklh_jenis`
--

INSERT INTO `pendidikan_partisipasi_sklh_jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'APS SD (7-12 tahun)'),
(2, 'APS SMP (13-15 tahun)'),
(3, 'APS SMA (16-18 tahun)');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_penduduk`
--

CREATE TABLE `pendidikan_penduduk` (
  `id_pendidikan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_penduduk`
--

INSERT INTO `pendidikan_penduduk` (`id_pendidikan`, `id_jenis`, `th`, `prosentase`) VALUES
(1, 3, 2014, '13.79'),
(2, 3, 2015, '8.39'),
(3, 3, 2016, '8.76'),
(4, 4, 2014, '16.16'),
(5, 4, 2015, '18.90'),
(6, 4, 2016, '26.35'),
(7, 5, 2014, '17.53'),
(8, 5, 2015, '19.83'),
(9, 5, 2016, '11.93'),
(10, 6, 2014, '35.67'),
(11, 6, 2015, '36.09'),
(12, 6, 2016, '35.94'),
(13, 7, 2014, '16.85'),
(14, 7, 2015, '16.79'),
(15, 7, 2016, '17.03');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_penduduk_jenis`
--

CREATE TABLE `pendidikan_penduduk_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_penduduk_jenis`
--

INSERT INTO `pendidikan_penduduk_jenis` (`id_jenis`, `nama_jenis`) VALUES
(3, 'Tidak Punya Ijazah SD'),
(4, 'SD / Sederajat'),
(5, 'SMP / Sederajat'),
(6, 'SMA / Sederajat'),
(7, 'PT / Universitas');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_rasio_guru_murid`
--

CREATE TABLE `pendidikan_rasio_guru_murid` (
  `id_rasio` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `prosentase` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_rasio_guru_murid`
--

INSERT INTO `pendidikan_rasio_guru_murid` (`id_rasio`, `id_jenis`, `th`, `prosentase`) VALUES
(1, 2, 2015, '1222'),
(2, 1, 2016, '120'),
(5, 6, 2014, '13'),
(6, 6, 2015, '13'),
(7, 6, 2016, '7'),
(8, 7, 2014, '19'),
(9, 7, 2015, '19'),
(10, 7, 2016, '19'),
(11, 8, 2014, '15'),
(12, 8, 2015, '15'),
(13, 8, 2016, '14'),
(14, 9, 2014, '12'),
(15, 9, 2015, '12'),
(16, 9, 2016, '12'),
(17, 10, 2014, '14'),
(18, 10, 2015, '13'),
(19, 10, 2016, '13');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_rasio_guru_murid_jenis`
--

CREATE TABLE `pendidikan_rasio_guru_murid_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_rasio_guru_murid_jenis`
--

INSERT INTO `pendidikan_rasio_guru_murid_jenis` (`id_jenis`, `nama_jenis`) VALUES
(6, 'TK'),
(7, 'SD'),
(8, 'SMP'),
(9, 'SMA'),
(10, 'SMK');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_penduduk`
--

CREATE TABLE `pengeluaran_penduduk` (
  `id_pengeluaran` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran_penduduk`
--

INSERT INTO `pengeluaran_penduduk` (`id_pengeluaran`, `id_jenis`, `th`, `jml`) VALUES
(2, 6, 2014, '40.93'),
(3, 6, 2015, '44.97'),
(4, 6, 2016, '50.38'),
(5, 7, 2014, '18.03'),
(6, 7, 2015, '14.19'),
(7, 7, 2016, '13.12'),
(8, 8, 2014, '23.32'),
(9, 8, 2015, '22.33'),
(10, 8, 2016, '14.66'),
(11, 9, 2014, '15.88'),
(12, 9, 2015, '16.11'),
(13, 9, 2016, '19.16'),
(14, 10, 2014, '1.84'),
(15, 10, 2015, '2.40'),
(16, 10, 2016, '2.69');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_penduduk_jenis`
--

CREATE TABLE `pengeluaran_penduduk_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran_penduduk_jenis`
--

INSERT INTO `pengeluaran_penduduk_jenis` (`id_jenis`, `nama_jenis`) VALUES
(6, '>= 1000000'),
(7, '750000 s/d 999999'),
(8, '500000 s/d 749999'),
(9, '300000 s/d 499999'),
(10, '<= 299999');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_rmt_tgg`
--

CREATE TABLE `pengeluaran_rmt_tgg` (
  `id_rmt_tgg` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_mkn` varchar(20) NOT NULL,
  `jml_non` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran_rmt_tgg`
--

INSERT INTO `pengeluaran_rmt_tgg` (`id_rmt_tgg`, `th`, `jml_mkn`, `jml_non`) VALUES
(5, 2014, '38.09', '61.91'),
(6, 2015, '39.21', '60.79'),
(7, 2016, '38.63', '61.37');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_holti_jenis`
--

CREATE TABLE `pertanian_holti_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL,
  `kategori` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_holti_jenis`
--

INSERT INTO `pertanian_holti_jenis` (`id_jenis`, `nama_jenis`, `kategori`) VALUES
(9, 'Sukun', '0'),
(10, 'Sirsak', '0'),
(11, 'Sawo', '0'),
(12, 'Salak', '0'),
(13, 'Rambutan', '0'),
(14, 'Pisang', '0'),
(15, 'Petai', '1'),
(16, 'Pepaya', '0'),
(17, 'Nanas', '0'),
(18, 'Nangka/Cempedak', '0'),
(19, 'Melinjo', '0'),
(20, 'Markisa/Konyal', '0'),
(21, 'Manggis', '0'),
(22, 'Mangga', '0'),
(23, 'Jeruk Siam/Keprok', '0'),
(24, 'Jeruk Besar', '0'),
(25, 'Jengkol', '1'),
(26, 'Jambu Biji', '0'),
(27, 'Jambu Air', '0'),
(28, 'Durian', '0'),
(29, 'Duku/Langsat', '0'),
(30, 'Belimbing', '0'),
(31, 'Apel', '0'),
(32, 'Anggur', '0'),
(33, 'Alpukat', '0'),
(34, 'Bawang Daun', '1'),
(35, 'Bawang Merah', '1'),
(36, 'Bawang Putih', '1'),
(37, 'Bayam', '1'),
(38, 'Blewah', '0'),
(39, 'Buncis', '0'),
(40, 'Cabai Besar', '0'),
(41, 'Cabai Rawit', '0'),
(42, 'Jamur', '1'),
(43, 'Kacang Merah', '1'),
(44, 'Kacang Panjang', '1'),
(45, 'Kangkung', '1'),
(46, 'Kembang Kol', '1'),
(47, 'Kentang ', '1'),
(48, 'Ketimun', '0'),
(49, 'Kubis', '1'),
(50, 'Labu Siam', '1'),
(51, 'Lobak', '1'),
(52, 'Melon', '0'),
(53, 'Paprika', '0'),
(54, 'Petsai/Sawi', '1'),
(55, 'Semangka', '0'),
(56, 'Stroberi', '0'),
(57, 'Terung', '1'),
(58, 'Tomat', '0'),
(59, 'Wortel', '1');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_holti_jml`
--

CREATE TABLE `pertanian_holti_jml` (
  `id_holti` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_holti_jml`
--

INSERT INTO `pertanian_holti_jml` (`id_holti`, `id_jenis`, `th`, `jml`) VALUES
(1, 9, 2014, '67.70'),
(2, 9, 2015, '70.70'),
(3, 9, 2016, '64.90'),
(4, 9, 2017, '36.10'),
(5, 10, 2014, '45.90'),
(6, 10, 2015, '43.40'),
(7, 10, 2016, '16.10'),
(8, 10, 2017, '33.90'),
(9, 11, 2014, '13.5'),
(10, 11, 2015, '14.5'),
(11, 11, 2016, '13.9'),
(12, 11, 2017, '3.1'),
(13, 12, 2014, '265.1'),
(14, 12, 2015, '246.6'),
(15, 12, 2016, '231.00'),
(16, 12, 2017, '110.8'),
(17, 13, 2014, '86.00'),
(18, 13, 2015, '87.2'),
(19, 13, 2016, '77.7'),
(20, 13, 2017, '14.5'),
(21, 14, 2014, '213.3'),
(22, 14, 2015, '213.3'),
(23, 14, 2016, '212.1'),
(24, 14, 2017, '191.00'),
(25, 15, 2014, '156.00'),
(26, 15, 2015, '145.4'),
(27, 15, 2016, '185.3'),
(28, 15, 2017, '168.8'),
(29, 16, 2014, '0.00'),
(30, 16, 2015, '0.00'),
(31, 16, 2016, '0.00'),
(32, 16, 2017, '0.00'),
(33, 17, 2014, '153.00'),
(34, 17, 2015, '152.3'),
(35, 17, 2016, '139.7'),
(36, 17, 2017, '194.2'),
(37, 18, 2014, '48.8'),
(38, 18, 2015, '48.7'),
(39, 18, 2016, '47.5'),
(40, 18, 2017, '3.8'),
(41, 19, 2014, '0.5'),
(42, 19, 2015, '4.9'),
(43, 19, 2016, '6.5'),
(44, 19, 2017, '3.5'),
(45, 20, 2014, '0.00'),
(46, 20, 2015, '0.00'),
(47, 20, 2016, '0.00'),
(48, 20, 2017, '0.00'),
(49, 21, 2014, '1131.5'),
(50, 21, 2015, '1145.9'),
(51, 21, 2016, '1013.7'),
(52, 21, 2017, '8569.00'),
(53, 22, 2014, '588.8'),
(54, 22, 2015, '587.5'),
(55, 22, 2016, '542.9'),
(56, 22, 2017, '558.1'),
(57, 23, 2014, '120.00'),
(58, 23, 2015, '160.00'),
(59, 23, 2016, '200.00'),
(60, 23, 2017, '0.00'),
(61, 24, 2014, '92.00'),
(62, 24, 2015, '91.5'),
(63, 24, 2016, '81.5'),
(64, 24, 2017, '25.6'),
(65, 25, 2014, '40.5'),
(66, 25, 2015, '41.2'),
(67, 25, 2016, '32.3'),
(68, 25, 2017, '9.6'),
(69, 26, 2014, '46.4'),
(70, 26, 2015, '46.4'),
(71, 26, 2016, '46.7'),
(72, 26, 2017, '19.1'),
(73, 27, 2014, '5.3'),
(74, 27, 2015, '5.2'),
(75, 27, 2016, '5.00'),
(76, 27, 2017, '1.7'),
(77, 28, 2014, '98.1'),
(78, 28, 2015, '82.1'),
(79, 28, 2016, '83.6'),
(80, 28, 2017, '89.3'),
(81, 29, 2014, '0.00'),
(82, 29, 2015, '0.00'),
(83, 29, 2016, '0.00'),
(84, 29, 2017, '0.00'),
(85, 30, 2014, '93.00'),
(86, 30, 2015, '112.00'),
(87, 30, 2016, '80.00'),
(88, 30, 2017, '0.00'),
(89, 31, 2014, '25.1'),
(90, 31, 2015, '24.7'),
(91, 31, 2016, '20.00'),
(92, 31, 2017, '0.99'),
(93, 32, 2014, '0.00'),
(94, 32, 2015, '0.00'),
(95, 32, 2016, '0.00'),
(96, 32, 2017, '0.00'),
(97, 33, 2014, '0.00'),
(98, 33, 2015, '0.00'),
(99, 33, 2016, '0.00'),
(100, 33, 2017, '0.00'),
(101, 34, 2014, '10.00'),
(102, 34, 2015, '34.00'),
(103, 34, 2016, '27.00'),
(104, 34, 2017, '0.00'),
(105, 35, 2014, '64.6'),
(106, 35, 2015, '112.5'),
(107, 35, 2016, '4.4'),
(108, 35, 2017, '32.5'),
(109, 36, 2014, '419795.00'),
(110, 36, 2015, '1896.6'),
(111, 36, 2016, '3547.12'),
(112, 36, 2017, '874.6'),
(113, 37, 2014, '0.00'),
(114, 37, 2015, '0.00'),
(115, 37, 2016, '0.00'),
(116, 37, 2017, '0.00'),
(117, 38, 2014, '29.5'),
(118, 38, 2015, '4.00'),
(119, 38, 2016, '5.00'),
(120, 38, 2017, '1.4'),
(121, 39, 2014, '6.00'),
(122, 39, 2015, '0.00'),
(123, 39, 2016, '0.00'),
(124, 39, 2017, '0.00'),
(125, 40, 2014, '0.00'),
(126, 40, 2015, '0.00'),
(127, 40, 2016, '0.8'),
(128, 40, 2017, '0.2'),
(129, 41, 2014, '0.00'),
(130, 41, 2015, '0.00'),
(131, 41, 2016, '0.00'),
(132, 41, 2017, '0.00'),
(133, 42, 2014, '24.00'),
(134, 42, 2015, '6.00'),
(135, 42, 2016, '2.00'),
(136, 42, 2017, '0.00'),
(137, 43, 2014, '0.01'),
(138, 43, 2015, '0.02'),
(139, 43, 2016, '0.03'),
(140, 43, 2017, '0.04'),
(141, 44, 2014, '7.2'),
(142, 44, 2015, '0.00'),
(143, 44, 2016, '0.00'),
(144, 44, 2017, '0.00'),
(145, 45, 2014, '0.00'),
(146, 45, 2015, '0.00'),
(147, 45, 2016, '0.00'),
(148, 45, 2017, '0.00'),
(149, 46, 2014, '0.00'),
(150, 46, 2015, '0.00'),
(151, 46, 2016, '0.00'),
(152, 46, 2017, '0.00'),
(153, 47, 2014, '1.5'),
(154, 47, 2015, '0.00'),
(155, 47, 2016, '0.00'),
(156, 47, 2017, '0.00'),
(157, 48, 2014, '35.8'),
(158, 48, 2015, '41.00'),
(159, 48, 2016, '53.6'),
(160, 48, 2017, '13.7'),
(161, 49, 2014, '0.00'),
(162, 49, 2015, '0.00'),
(163, 49, 2016, '0.00'),
(164, 49, 2017, '0.00'),
(165, 50, 2014, '0.00'),
(166, 50, 2015, '0.00'),
(167, 50, 2016, '0.00'),
(168, 50, 2017, '0.00'),
(169, 51, 2014, '416.00'),
(170, 51, 2015, '140.00'),
(171, 51, 2016, '20.00'),
(172, 51, 2017, '0.00'),
(173, 52, 2014, '35.2'),
(174, 52, 2015, '14.00'),
(175, 52, 2016, '2.00'),
(176, 52, 2017, '0.6'),
(177, 53, 2014, '0.00'),
(178, 53, 2015, '0.00'),
(179, 53, 2016, '0.00'),
(180, 53, 2017, '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_ikan_jenis`
--

CREATE TABLE `pertanian_ikan_jenis` (
  `id_jenis` int(11) NOT NULL,
  `kategori` enum('0','1') NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_ikan_jenis`
--

INSERT INTO `pertanian_ikan_jenis` (`id_jenis`, `kategori`, `nama_jenis`) VALUES
(11, '0', 'Ikan Nila'),
(12, '0', 'Ikan Tombro'),
(13, '0', 'Ikan Gurame'),
(14, '0', 'Ikan Lele'),
(15, '1', 'Ikan Nila'),
(16, '1', 'Ikan Tombro'),
(17, '1', 'Ikan Gurame'),
(18, '1', 'Ikan Lele');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_ikan_jml`
--

CREATE TABLE `pertanian_ikan_jml` (
  `id_ikan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_ikan_jml`
--

INSERT INTO `pertanian_ikan_jml` (`id_ikan`, `id_jenis`, `th`, `jml`) VALUES
(4, 11, 2014, '3499'),
(5, 11, 2015, '2735'),
(6, 11, 2016, '6258'),
(7, 12, 2014, '20'),
(8, 12, 2015, '-'),
(9, 12, 2016, '-'),
(10, 17, 2014, '15'),
(11, 13, 2015, '-'),
(12, 13, 2016, '40'),
(13, 14, 2014, '33830'),
(17, 14, 2015, '53771'),
(18, 14, 2016, '101231'),
(19, 15, 2014, '1495'),
(20, 15, 2015, '203'),
(21, 15, 2016, '1220'),
(22, 16, 2014, '1528'),
(23, 16, 2015, '550'),
(24, 16, 2016, '2176'),
(25, 17, 2014, '-'),
(26, 17, 2015, '-'),
(27, 17, 2016, '-'),
(28, 18, 2014, '-'),
(29, 18, 2015, '-'),
(30, 18, 2016, '-');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_kelapa_tebu`
--

CREATE TABLE `pertanian_kelapa_tebu` (
  `id_pertanian` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml_kelapa` varchar(20) NOT NULL,
  `jml_tebu` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_kelapa_tebu`
--

INSERT INTO `pertanian_kelapa_tebu` (`id_pertanian`, `th`, `jml_kelapa`, `jml_tebu`) VALUES
(3, 2016, '925', '53142'),
(4, 2015, '807', '60013.28'),
(5, 2014, '790', '62725');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_komoditas_jenis`
--

CREATE TABLE `pertanian_komoditas_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_komoditas_jenis`
--

INSERT INTO `pertanian_komoditas_jenis` (`id_jenis`, `nama_jenis`) VALUES
(6, 'Padi Sawah'),
(7, 'Padi Ladang'),
(8, 'Jagung'),
(9, 'Kedelai'),
(10, 'Kacang Tanah'),
(11, 'Kacang Hijau'),
(12, 'Ubi Kayu'),
(13, 'Ubi Jalar');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_komoditas_jml`
--

CREATE TABLE `pertanian_komoditas_jml` (
  `id_komoditas` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_komoditas_jml`
--

INSERT INTO `pertanian_komoditas_jml` (`id_komoditas`, `id_jenis`, `th`, `jml`) VALUES
(1, 6, 2012, '13478'),
(2, 6, 2013, '13381'),
(3, 6, 2014, '13271.00'),
(4, 6, 2015, '14347.00'),
(5, 6, 2016, '14285.00'),
(6, 6, 2017, '14640.00'),
(7, 7, 2012, '2'),
(8, 7, 2013, '0'),
(9, 7, 2014, '0'),
(10, 7, 2015, '0'),
(11, 7, 2016, '0'),
(12, 7, 2017, '0'),
(13, 8, 2012, '939'),
(14, 8, 2013, '971'),
(15, 8, 2014, '729.00'),
(16, 8, 2015, '676.00'),
(17, 8, 2016, '459.00'),
(18, 8, 2017, '249.00'),
(19, 9, 2012, '0'),
(20, 9, 2013, '0'),
(21, 9, 2014, '0'),
(22, 9, 2015, '0'),
(23, 9, 2016, '0'),
(24, 9, 2017, '0'),
(25, 10, 2012, '0'),
(26, 10, 2013, '0'),
(27, 10, 2014, '0'),
(28, 10, 2015, '0'),
(29, 10, 2016, '0'),
(30, 10, 2017, '0'),
(31, 11, 2012, '0'),
(32, 11, 2013, '0'),
(33, 11, 2014, '50.00'),
(34, 11, 2015, '33.00'),
(35, 11, 2016, '15.00'),
(36, 11, 2017, '18.00'),
(37, 12, 2012, '1167'),
(38, 12, 2013, '1849'),
(39, 12, 2014, '2642.00'),
(40, 12, 2015, '1995.00'),
(41, 12, 2016, '3858.00'),
(42, 12, 2017, '3205.00'),
(43, 13, 2012, '19'),
(44, 13, 2013, '0'),
(45, 13, 2014, '22.00'),
(46, 13, 2015, '0'),
(47, 13, 2016, '19.00'),
(48, 13, 2017, '0');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_lahan`
--

CREATE TABLE `pertanian_lahan` (
  `id_lahan` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jenis_lahan` varchar(64) NOT NULL,
  `luas_lahan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_lahan`
--

INSERT INTO `pertanian_lahan` (`id_lahan`, `th`, `jenis_lahan`, `luas_lahan`) VALUES
(1, 2017, 'Lahan Pertanian Sawah', '20.90'),
(3, 2016, 'Lahan Pertanian Sawah', '10.38'),
(4, 2016, 'Lahan Pertanian Bukan Sawah', '18.85'),
(5, 2016, 'Lahan Bukan Pertanian', '70.77');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_lahan_penggunaan`
--

CREATE TABLE `pertanian_lahan_penggunaan` (
  `id_peng` int(11) NOT NULL,
  `jenis_peng` text NOT NULL,
  `th` int(4) NOT NULL,
  `luas_pend` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_lahan_penggunaan`
--

INSERT INTO `pertanian_lahan_penggunaan` (`id_peng`, `jenis_peng`, `th`, `luas_pend`) VALUES
(1, 'Lahan Ditanami Padi (Sawah)', 2018, '2000.98'),
(3, 'Lahan Ditanami Padi (Sawah)', 2016, '844'),
(4, 'Lahan Ditanami Selain Padi (Bukan Sawah)', 2016, '292'),
(5, 'Lahan Tidak Ditanami', 2016, '6');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_ternak_jenis`
--

CREATE TABLE `pertanian_ternak_jenis` (
  `id_jenis` int(11) NOT NULL,
  `kategori` enum('0','1','2') NOT NULL,
  `nama_jenis` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_ternak_jenis`
--

INSERT INTO `pertanian_ternak_jenis` (`id_jenis`, `kategori`, `nama_jenis`) VALUES
(9, '0', 'Daging sapi'),
(10, '0', 'Daging Kambing/Domba'),
(11, '0', 'Daging Babi'),
(12, '1', 'Ayam Buras'),
(13, '1', 'Ayam Pedaging'),
(14, '1', 'Itik'),
(15, '1', 'Itik Manila'),
(16, '0', 'Susu'),
(17, '2', 'Ayam Buras'),
(18, '2', 'Ayam Petelur'),
(19, '2', 'Itik'),
(20, '2', 'Itik Manila'),
(21, '1', 'Ayam Petelur');

-- --------------------------------------------------------

--
-- Table structure for table `pertanian_ternak_jml`
--

CREATE TABLE `pertanian_ternak_jml` (
  `id_ternak` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanian_ternak_jml`
--

INSERT INTO `pertanian_ternak_jml` (`id_ternak`, `id_jenis`, `th`, `jml`) VALUES
(3, 9, 2014, '3465'),
(4, 9, 2015, '3614.84'),
(5, 9, 2016, '3246.32'),
(6, 10, 2014, '320'),
(7, 10, 2015, '430'),
(8, 10, 2015, '490.22'),
(9, 11, 2014, '511.80'),
(10, 11, 2015, '525.05'),
(11, 11, 2016, '508.39'),
(12, 12, 2014, '642.70'),
(13, 12, 2015, '551.74'),
(14, 12, 2016, '381.06'),
(19, 21, 2014, '14.90'),
(20, 21, 2015, '18.21'),
(21, 21, 2016, '21.61'),
(22, 14, 2014, '4.30'),
(23, 19, 2015, '23.24'),
(24, 13, 2014, '17673.30'),
(25, 13, 2015, '17513.17'),
(26, 13, 2016, '17877.11'),
(27, 14, 2014, '4.30'),
(28, 14, 2015, '23.24'),
(29, 14, 2016, '25.25'),
(30, 20, 2014, '2.10'),
(31, 20, 2015, '2.34'),
(32, 20, 2016, '2.28'),
(33, 16, 2014, '335085.60'),
(34, 16, 2015, '286496.99'),
(35, 16, 2016, '214417.74'),
(36, 17, 2014, '30.50'),
(37, 17, 2015, '27.45'),
(38, 17, 2016, '27.51'),
(39, 18, 2014, '1332.60'),
(40, 18, 2015, '1747.36'),
(41, 18, 2016, '1945.20'),
(42, 19, 2014, '4.70'),
(43, 19, 2015, '101.50'),
(44, 19, 2016, '77.34'),
(45, 20, 2014, '1.60'),
(46, 20, 2015, '0.89'),
(47, 20, 2016, '0.93');

-- --------------------------------------------------------

--
-- Table structure for table `tehnologi_jml_bts`
--

CREATE TABLE `tehnologi_jml_bts` (
  `id_jml_bts` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tehnologi_jml_bts`
--

INSERT INTO `tehnologi_jml_bts` (`id_jml_bts`, `th`, `jml`) VALUES
(5, 2014, 317),
(6, 2015, 342),
(7, 2016, 385);

-- --------------------------------------------------------

--
-- Table structure for table `tehnologi_jml_warnet`
--

CREATE TABLE `tehnologi_jml_warnet` (
  `id_jml_warnet` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tehnologi_jml_warnet`
--

INSERT INTO `tehnologi_jml_warnet` (`id_jml_warnet`, `th`, `jml`) VALUES
(4, 2014, 92),
(5, 2015, 96),
(6, 2016, 97);

-- --------------------------------------------------------

--
-- Table structure for table `tehnologi_jml_web_opd`
--

CREATE TABLE `tehnologi_jml_web_opd` (
  `id_web_opd` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tehnologi_jml_web_opd`
--

INSERT INTO `tehnologi_jml_web_opd` (`id_web_opd`, `th`, `jml`) VALUES
(3, 2014, 100),
(4, 2015, 105),
(5, 2016, 107);

-- --------------------------------------------------------

--
-- Table structure for table `trans_jln`
--

CREATE TABLE `trans_jln` (
  `id_jln` int(11) NOT NULL,
  `kategori_jln` varchar(64) NOT NULL,
  `jln_negara` varchar(20) NOT NULL,
  `jln_prov` varchar(20) NOT NULL,
  `jln_kota` varchar(20) NOT NULL,
  `th` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_jln`
--

INSERT INTO `trans_jln` (`id_jln`, `kategori_jln`, `jln_negara`, `jln_prov`, `jln_kota`, `th`) VALUES
(4, 'Kondisi Jalan Baik', '1.45', '48.45', '135.19', 2015),
(5, 'Kondisi Jalan Baik', '12.64', '10.44', '993.64', 2016),
(6, 'Kondisi Jalan Sedang', '0', '0', '0', 2015),
(7, 'Kondisi Jalan Sedang', '0', '0', '0', 2016),
(8, 'Kondisi Jalan Rusak', '0', '0.5', '5.59', 2015),
(9, 'Kondisi Jalan Rusak', '0', '0.5', '33.47', 2016),
(10, 'Kondisi Jalan Rusak Berat', '0', '0', '0', 2015),
(11, 'Kondisi Jalan Rusak Berat', '0', '0', '0', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `trans_jml_kendaraan`
--

CREATE TABLE `trans_jml_kendaraan` (
  `id_jml_kendaraan` int(11) NOT NULL,
  `id_jenis_kendaraan` int(11) NOT NULL,
  `th` int(4) NOT NULL,
  `jml` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_jml_kendaraan`
--

INSERT INTO `trans_jml_kendaraan` (`id_jml_kendaraan`, `id_jenis_kendaraan`, `th`, `jml`) VALUES
(1, 1, 2014, '75818'),
(2, 1, 2015, '86091'),
(3, 1, 2016, '90058'),
(4, 2, 2014, '861'),
(5, 2, 2015, '934'),
(6, 2, 2016, '966'),
(7, 3, 2014, '17949'),
(8, 3, 2015, '19457'),
(9, 3, 2016, '20002'),
(10, 4, 2014, '392559'),
(11, 4, 2015, '441123'),
(12, 4, 2016, '456693');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `admin_lv`
--
ALTER TABLE `admin_lv`
  ADD PRIMARY KEY (`id_lv`);

--
-- Indexes for table `dinas_perdagangan`
--
ALTER TABLE `dinas_perdagangan`
  ADD PRIMARY KEY (`id_perdagangan`);

--
-- Indexes for table `dinas_perdagangan_jenis`
--
ALTER TABLE `dinas_perdagangan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `dinas_perdagangan_sub_jenis`
--
ALTER TABLE `dinas_perdagangan_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `disbudpar_dts`
--
ALTER TABLE `disbudpar_dts`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `disbudpar_dtw_jenis`
--
ALTER TABLE `disbudpar_dtw_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `disbudpar_dtw_main`
--
ALTER TABLE `disbudpar_dtw_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `disbudpar_kunjungan_hotel_jenis`
--
ALTER TABLE `disbudpar_kunjungan_hotel_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `disbudpar_kunjungan_hotel_kate`
--
ALTER TABLE `disbudpar_kunjungan_hotel_kate`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `disbudpar_kunjungan_hotel_main`
--
ALTER TABLE `disbudpar_kunjungan_hotel_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `disbudpar_obj_pemajuan_bud_kate`
--
ALTER TABLE `disbudpar_obj_pemajuan_bud_kate`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `disbudpar_obj_pemajuan_bud_main`
--
ALTER TABLE `disbudpar_obj_pemajuan_bud_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `disbudpar_zona_kreatif`
--
ALTER TABLE `disbudpar_zona_kreatif`
  ADD PRIMARY KEY (`id_zona`);

--
-- Indexes for table `disnaker_jenis`
--
ALTER TABLE `disnaker_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `disnaker_kategori`
--
ALTER TABLE `disnaker_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `disnaker_main`
--
ALTER TABLE `disnaker_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `disnaker_sub_jenis`
--
ALTER TABLE `disnaker_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `disperkim_all`
--
ALTER TABLE `disperkim_all`
  ADD PRIMARY KEY (`id_disperkim`);

--
-- Indexes for table `disperkim_all_jenis`
--
ALTER TABLE `disperkim_all_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `disperkim_all_kategori`
--
ALTER TABLE `disperkim_all_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `disperkim_all_sub_jenis`
--
ALTER TABLE `disperkim_all_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `disperkim_csr`
--
ALTER TABLE `disperkim_csr`
  ADD PRIMARY KEY (`id_csr`);

--
-- Indexes for table `disperkim_dsu`
--
ALTER TABLE `disperkim_dsu`
  ADD PRIMARY KEY (`id_dsu`);

--
-- Indexes for table `disperkim_penanaman_pohon`
--
ALTER TABLE `disperkim_penanaman_pohon`
  ADD PRIMARY KEY (`id_pohon`);

--
-- Indexes for table `disperkim_penanaman_pohon_jenis`
--
ALTER TABLE `disperkim_penanaman_pohon_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `disperkim_taman`
--
ALTER TABLE `disperkim_taman`
  ADD PRIMARY KEY (`id_taman`);

--
-- Indexes for table `disperkim_taman_jenis`
--
ALTER TABLE `disperkim_taman_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `dpmptsp_jenis`
--
ALTER TABLE `dpmptsp_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `dpmptsp_main`
--
ALTER TABLE `dpmptsp_main`
  ADD PRIMARY KEY (`id_main`);

--
-- Indexes for table `id_kesehatan_sub_jenis`
--
ALTER TABLE `id_kesehatan_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `iklim_hujan`
--
ALTER TABLE `iklim_hujan`
  ADD PRIMARY KEY (`id_hujan`);

--
-- Indexes for table `iklim_station`
--
ALTER TABLE `iklim_station`
  ADD PRIMARY KEY (`id_st`);

--
-- Indexes for table `index_kes_daya_pend`
--
ALTER TABLE `index_kes_daya_pend`
  ADD PRIMARY KEY (`id_index`);

--
-- Indexes for table `index_pem_manusia`
--
ALTER TABLE `index_pem_manusia`
  ADD PRIMARY KEY (`id_ipm`);

--
-- Indexes for table `kepandudukan_jk`
--
ALTER TABLE `kepandudukan_jk`
  ADD PRIMARY KEY (`id_kepend_jk`);

--
-- Indexes for table `kependudukan_kel_umur`
--
ALTER TABLE `kependudukan_kel_umur`
  ADD PRIMARY KEY (`id_kel_umur`);

--
-- Indexes for table `kependudukan_rasio_ketergantungan`
--
ALTER TABLE `kependudukan_rasio_ketergantungan`
  ADD PRIMARY KEY (`id_rasio`);

--
-- Indexes for table `kerja_angkatan`
--
ALTER TABLE `kerja_angkatan`
  ADD PRIMARY KEY (`id_angkatan`);

--
-- Indexes for table `kerja_pengangguran`
--
ALTER TABLE `kerja_pengangguran`
  ADD PRIMARY KEY (`id_pengangguran`);

--
-- Indexes for table `kerja_ump`
--
ALTER TABLE `kerja_ump`
  ADD PRIMARY KEY (`id_ump`);

--
-- Indexes for table `kesehatan_gizi_balita`
--
ALTER TABLE `kesehatan_gizi_balita`
  ADD PRIMARY KEY (`id_gizi_balita`);

--
-- Indexes for table `keu_jenis`
--
ALTER TABLE `keu_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `keu_jml`
--
ALTER TABLE `keu_jml`
  ADD PRIMARY KEY (`id_jml_keu`);

--
-- Indexes for table `kominfo_aplikasi`
--
ALTER TABLE `kominfo_aplikasi`
  ADD PRIMARY KEY (`id_aplikasi`);

--
-- Indexes for table `kominfo_domain`
--
ALTER TABLE `kominfo_domain`
  ADD PRIMARY KEY (`id_aplikasi`);

--
-- Indexes for table `lp_aparat`
--
ALTER TABLE `lp_aparat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lp_aparat_jenis`
--
ALTER TABLE `lp_aparat_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_bencana`
--
ALTER TABLE `lp_bencana`
  ADD PRIMARY KEY (`id_bencana`);

--
-- Indexes for table `lp_bencana_jenis`
--
ALTER TABLE `lp_bencana_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_kajian_penelitian`
--
ALTER TABLE `lp_kajian_penelitian`
  ADD PRIMARY KEY (`id_kajian`);

--
-- Indexes for table `lp_kel_umur`
--
ALTER TABLE `lp_kel_umur`
  ADD PRIMARY KEY (`id_kel_umur`);

--
-- Indexes for table `lp_kel_umur_jenis`
--
ALTER TABLE `lp_kel_umur_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_kendaran_kec`
--
ALTER TABLE `lp_kendaran_kec`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indexes for table `lp_kendaran_plat`
--
ALTER TABLE `lp_kendaran_plat`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indexes for table `lp_kendaran_plat_jenis`
--
ALTER TABLE `lp_kendaran_plat_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_kendaran_umum`
--
ALTER TABLE `lp_kendaran_umum`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indexes for table `lp_kendaran_umum_jenis`
--
ALTER TABLE `lp_kendaran_umum_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_kesehatan`
--
ALTER TABLE `lp_kesehatan`
  ADD PRIMARY KEY (`id_kesehatan`);

--
-- Indexes for table `lp_kesehatan_jenis`
--
ALTER TABLE `lp_kesehatan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_kesehatan_kategori`
--
ALTER TABLE `lp_kesehatan_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `lp_kesehatan_sub3_jenis`
--
ALTER TABLE `lp_kesehatan_sub3_jenis`
  ADD PRIMARY KEY (`id_3sub_jenis`);

--
-- Indexes for table `lp_kesehatan_sub_jenis`
--
ALTER TABLE `lp_kesehatan_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `lp_keu`
--
ALTER TABLE `lp_keu`
  ADD PRIMARY KEY (`id_keu`);

--
-- Indexes for table `lp_keu_jenis`
--
ALTER TABLE `lp_keu_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_keu_sub_jenis`
--
ALTER TABLE `lp_keu_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `lp_kooperasi_jenis`
--
ALTER TABLE `lp_kooperasi_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_kooperasi_sub_jenis`
--
ALTER TABLE `lp_kooperasi_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `lp_koperasi`
--
ALTER TABLE `lp_koperasi`
  ADD PRIMARY KEY (`id_koperasi`);

--
-- Indexes for table `lp_lahan`
--
ALTER TABLE `lp_lahan`
  ADD PRIMARY KEY (`id_lahan`);

--
-- Indexes for table `lp_lahan_jenis`
--
ALTER TABLE `lp_lahan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_miskin`
--
ALTER TABLE `lp_miskin`
  ADD PRIMARY KEY (`id_miskin`);

--
-- Indexes for table `lp_miskin_jenis`
--
ALTER TABLE `lp_miskin_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_or`
--
ALTER TABLE `lp_or`
  ADD PRIMARY KEY (`id_or`);

--
-- Indexes for table `lp_or_jenis`
--
ALTER TABLE `lp_or_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pajak`
--
ALTER TABLE `lp_pajak`
  ADD PRIMARY KEY (`id_pajak`);

--
-- Indexes for table `lp_pajak_jenis`
--
ALTER TABLE `lp_pajak_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_panen`
--
ALTER TABLE `lp_panen`
  ADD PRIMARY KEY (`id_panen`);

--
-- Indexes for table `lp_panen_jenis`
--
ALTER TABLE `lp_panen_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pdb`
--
ALTER TABLE `lp_pdb`
  ADD PRIMARY KEY (`id_pdb`);

--
-- Indexes for table `lp_pdb_jenis`
--
ALTER TABLE `lp_pdb_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pdb_kategori`
--
ALTER TABLE `lp_pdb_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `lp_pendidikan`
--
ALTER TABLE `lp_pendidikan`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `lp_pendidikan_jenis`
--
ALTER TABLE `lp_pendidikan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pendidikan_kategori`
--
ALTER TABLE `lp_pendidikan_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `lp_penduduk_jk`
--
ALTER TABLE `lp_penduduk_jk`
  ADD PRIMARY KEY (`id_penduduk`);

--
-- Indexes for table `lp_pend_industri`
--
ALTER TABLE `lp_pend_industri`
  ADD PRIMARY KEY (`id_pend`);

--
-- Indexes for table `lp_pend_industri_jenis`
--
ALTER TABLE `lp_pend_industri_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pend_industri_kategori`
--
ALTER TABLE `lp_pend_industri_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `lp_perpustakaan`
--
ALTER TABLE `lp_perpustakaan`
  ADD PRIMARY KEY (`id_perpustakaan`);

--
-- Indexes for table `lp_perpustakaan_jenis`
--
ALTER TABLE `lp_perpustakaan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pertanian`
--
ALTER TABLE `lp_pertanian`
  ADD PRIMARY KEY (`id_pertanian`);

--
-- Indexes for table `lp_pertanian_jenis`
--
ALTER TABLE `lp_pertanian_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pertanian_sub_jenis`
--
ALTER TABLE `lp_pertanian_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `lp_pidana`
--
ALTER TABLE `lp_pidana`
  ADD PRIMARY KEY (`id_pidana`);

--
-- Indexes for table `lp_pidana_jenis`
--
ALTER TABLE `lp_pidana_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_pidana_kec`
--
ALTER TABLE `lp_pidana_kec`
  ADD PRIMARY KEY (`id_pidana`);

--
-- Indexes for table `lp_pmks`
--
ALTER TABLE `lp_pmks`
  ADD PRIMARY KEY (`id_pmks`);

--
-- Indexes for table `lp_pmks_jenis`
--
ALTER TABLE `lp_pmks_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_tehnologi`
--
ALTER TABLE `lp_tehnologi`
  ADD PRIMARY KEY (`id_tehnologi`);

--
-- Indexes for table `lp_tehnologi_jenis`
--
ALTER TABLE `lp_tehnologi_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_tehnologi_sub_jenis`
--
ALTER TABLE `lp_tehnologi_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `lp_tenaga`
--
ALTER TABLE `lp_tenaga`
  ADD PRIMARY KEY (`id_tenaga`);

--
-- Indexes for table `lp_tenaga_jenis`
--
ALTER TABLE `lp_tenaga_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_tenaga_kategori`
--
ALTER TABLE `lp_tenaga_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `lp_terminal`
--
ALTER TABLE `lp_terminal`
  ADD PRIMARY KEY (`id_terminal`);

--
-- Indexes for table `lp_terminal_jenis`
--
ALTER TABLE `lp_terminal_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `lp_terminal_sub_jenis`
--
ALTER TABLE `lp_terminal_sub_jenis`
  ADD PRIMARY KEY (`id_sub_jenis`);

--
-- Indexes for table `lp_tpa`
--
ALTER TABLE `lp_tpa`
  ADD PRIMARY KEY (`id_tpa`);

--
-- Indexes for table `main_dinas`
--
ALTER TABLE `main_dinas`
  ADD PRIMARY KEY (`id_dinas`);

--
-- Indexes for table `main_jenis_kendaraan`
--
ALTER TABLE `main_jenis_kendaraan`
  ADD PRIMARY KEY (`id_jenis_kendaraan`);

--
-- Indexes for table `main_jenis_penyakit`
--
ALTER TABLE `main_jenis_penyakit`
  ADD PRIMARY KEY (`id_jenis_penyakit`);

--
-- Indexes for table `main_jenjang_pend`
--
ALTER TABLE `main_jenjang_pend`
  ADD PRIMARY KEY (`id_jenjang`);

--
-- Indexes for table `main_kecamatan`
--
ALTER TABLE `main_kecamatan`
  ADD PRIMARY KEY (`id_kec`);

--
-- Indexes for table `main_kelurahan`
--
ALTER TABLE `main_kelurahan`
  ADD PRIMARY KEY (`id_kel`);

--
-- Indexes for table `main_partai`
--
ALTER TABLE `main_partai`
  ADD PRIMARY KEY (`id_partai`);

--
-- Indexes for table `main_pendidikan`
--
ALTER TABLE `main_pendidikan`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `miskin_jml_pend`
--
ALTER TABLE `miskin_jml_pend`
  ADD PRIMARY KEY (`id_pend_miskin`);

--
-- Indexes for table `miskin_pengeluaran_perkapita`
--
ALTER TABLE `miskin_pengeluaran_perkapita`
  ADD PRIMARY KEY (`id_perkapita`);

--
-- Indexes for table `pem_dpr`
--
ALTER TABLE `pem_dpr`
  ADD PRIMARY KEY (`id_pem_dpr`);

--
-- Indexes for table `pem_jml_asn_jenjang`
--
ALTER TABLE `pem_jml_asn_jenjang`
  ADD PRIMARY KEY (`id_jml_asn`);

--
-- Indexes for table `pem_jml_rt_rw`
--
ALTER TABLE `pem_jml_rt_rw`
  ADD PRIMARY KEY (`id_rt_rw`);

--
-- Indexes for table `pendapatan_lap_usaha`
--
ALTER TABLE `pendapatan_lap_usaha`
  ADD PRIMARY KEY (`id_lap_usaha`);

--
-- Indexes for table `pendapatan_lap_usaha_jenis`
--
ALTER TABLE `pendapatan_lap_usaha_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendapatan_regional`
--
ALTER TABLE `pendapatan_regional`
  ADD PRIMARY KEY (`id_pendapatan`);

--
-- Indexes for table `pendidikan_baca_tulis`
--
ALTER TABLE `pendidikan_baca_tulis`
  ADD PRIMARY KEY (`id_baca_tulis`);

--
-- Indexes for table `pendidikan_baca_tulis_jenis`
--
ALTER TABLE `pendidikan_baca_tulis_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_jml_sklh`
--
ALTER TABLE `pendidikan_jml_sklh`
  ADD PRIMARY KEY (`id_jml_sklh`);

--
-- Indexes for table `pendidikan_jml_sklh_jenis`
--
ALTER TABLE `pendidikan_jml_sklh_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_partisipasi_sklh`
--
ALTER TABLE `pendidikan_partisipasi_sklh`
  ADD PRIMARY KEY (`id_partisipasi_sklh`);

--
-- Indexes for table `pendidikan_partisipasi_sklh_jenis`
--
ALTER TABLE `pendidikan_partisipasi_sklh_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_penduduk`
--
ALTER TABLE `pendidikan_penduduk`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `pendidikan_penduduk_jenis`
--
ALTER TABLE `pendidikan_penduduk_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_rasio_guru_murid`
--
ALTER TABLE `pendidikan_rasio_guru_murid`
  ADD PRIMARY KEY (`id_rasio`);

--
-- Indexes for table `pendidikan_rasio_guru_murid_jenis`
--
ALTER TABLE `pendidikan_rasio_guru_murid_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pengeluaran_penduduk`
--
ALTER TABLE `pengeluaran_penduduk`
  ADD PRIMARY KEY (`id_pengeluaran`);

--
-- Indexes for table `pengeluaran_penduduk_jenis`
--
ALTER TABLE `pengeluaran_penduduk_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pengeluaran_rmt_tgg`
--
ALTER TABLE `pengeluaran_rmt_tgg`
  ADD PRIMARY KEY (`id_rmt_tgg`);

--
-- Indexes for table `pertanian_holti_jenis`
--
ALTER TABLE `pertanian_holti_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pertanian_holti_jml`
--
ALTER TABLE `pertanian_holti_jml`
  ADD PRIMARY KEY (`id_holti`);

--
-- Indexes for table `pertanian_ikan_jenis`
--
ALTER TABLE `pertanian_ikan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pertanian_ikan_jml`
--
ALTER TABLE `pertanian_ikan_jml`
  ADD PRIMARY KEY (`id_ikan`);

--
-- Indexes for table `pertanian_kelapa_tebu`
--
ALTER TABLE `pertanian_kelapa_tebu`
  ADD PRIMARY KEY (`id_pertanian`);

--
-- Indexes for table `pertanian_komoditas_jenis`
--
ALTER TABLE `pertanian_komoditas_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pertanian_komoditas_jml`
--
ALTER TABLE `pertanian_komoditas_jml`
  ADD PRIMARY KEY (`id_komoditas`);

--
-- Indexes for table `pertanian_lahan`
--
ALTER TABLE `pertanian_lahan`
  ADD PRIMARY KEY (`id_lahan`);

--
-- Indexes for table `pertanian_lahan_penggunaan`
--
ALTER TABLE `pertanian_lahan_penggunaan`
  ADD PRIMARY KEY (`id_peng`);

--
-- Indexes for table `pertanian_ternak_jenis`
--
ALTER TABLE `pertanian_ternak_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pertanian_ternak_jml`
--
ALTER TABLE `pertanian_ternak_jml`
  ADD PRIMARY KEY (`id_ternak`);

--
-- Indexes for table `tehnologi_jml_bts`
--
ALTER TABLE `tehnologi_jml_bts`
  ADD PRIMARY KEY (`id_jml_bts`);

--
-- Indexes for table `tehnologi_jml_warnet`
--
ALTER TABLE `tehnologi_jml_warnet`
  ADD PRIMARY KEY (`id_jml_warnet`);

--
-- Indexes for table `tehnologi_jml_web_opd`
--
ALTER TABLE `tehnologi_jml_web_opd`
  ADD PRIMARY KEY (`id_web_opd`);

--
-- Indexes for table `trans_jln`
--
ALTER TABLE `trans_jln`
  ADD PRIMARY KEY (`id_jln`);

--
-- Indexes for table `trans_jml_kendaraan`
--
ALTER TABLE `trans_jml_kendaraan`
  ADD PRIMARY KEY (`id_jml_kendaraan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_lv`
--
ALTER TABLE `admin_lv`
  MODIFY `id_lv` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dinas_perdagangan`
--
ALTER TABLE `dinas_perdagangan`
  MODIFY `id_perdagangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `dinas_perdagangan_jenis`
--
ALTER TABLE `dinas_perdagangan_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `dinas_perdagangan_sub_jenis`
--
ALTER TABLE `dinas_perdagangan_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `disbudpar_dts`
--
ALTER TABLE `disbudpar_dts`
  MODIFY `id_main` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `disbudpar_dtw_jenis`
--
ALTER TABLE `disbudpar_dtw_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `disbudpar_dtw_main`
--
ALTER TABLE `disbudpar_dtw_main`
  MODIFY `id_main` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT for table `disbudpar_kunjungan_hotel_jenis`
--
ALTER TABLE `disbudpar_kunjungan_hotel_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `disbudpar_kunjungan_hotel_kate`
--
ALTER TABLE `disbudpar_kunjungan_hotel_kate`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `disbudpar_kunjungan_hotel_main`
--
ALTER TABLE `disbudpar_kunjungan_hotel_main`
  MODIFY `id_main` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=338;

--
-- AUTO_INCREMENT for table `disbudpar_obj_pemajuan_bud_kate`
--
ALTER TABLE `disbudpar_obj_pemajuan_bud_kate`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `disbudpar_obj_pemajuan_bud_main`
--
ALTER TABLE `disbudpar_obj_pemajuan_bud_main`
  MODIFY `id_main` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=637;

--
-- AUTO_INCREMENT for table `disbudpar_zona_kreatif`
--
ALTER TABLE `disbudpar_zona_kreatif`
  MODIFY `id_zona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;

--
-- AUTO_INCREMENT for table `disnaker_jenis`
--
ALTER TABLE `disnaker_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `disnaker_kategori`
--
ALTER TABLE `disnaker_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `disnaker_main`
--
ALTER TABLE `disnaker_main`
  MODIFY `id_main` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `disnaker_sub_jenis`
--
ALTER TABLE `disnaker_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `disperkim_all`
--
ALTER TABLE `disperkim_all`
  MODIFY `id_disperkim` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT for table `disperkim_all_jenis`
--
ALTER TABLE `disperkim_all_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `disperkim_all_kategori`
--
ALTER TABLE `disperkim_all_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `disperkim_all_sub_jenis`
--
ALTER TABLE `disperkim_all_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `disperkim_csr`
--
ALTER TABLE `disperkim_csr`
  MODIFY `id_csr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `disperkim_dsu`
--
ALTER TABLE `disperkim_dsu`
  MODIFY `id_dsu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `disperkim_penanaman_pohon`
--
ALTER TABLE `disperkim_penanaman_pohon`
  MODIFY `id_pohon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `disperkim_penanaman_pohon_jenis`
--
ALTER TABLE `disperkim_penanaman_pohon_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `disperkim_taman`
--
ALTER TABLE `disperkim_taman`
  MODIFY `id_taman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `disperkim_taman_jenis`
--
ALTER TABLE `disperkim_taman_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `dpmptsp_jenis`
--
ALTER TABLE `dpmptsp_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `dpmptsp_main`
--
ALTER TABLE `dpmptsp_main`
  MODIFY `id_main` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT for table `id_kesehatan_sub_jenis`
--
ALTER TABLE `id_kesehatan_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `iklim_hujan`
--
ALTER TABLE `iklim_hujan`
  MODIFY `id_hujan` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `iklim_station`
--
ALTER TABLE `iklim_station`
  MODIFY `id_st` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `index_kes_daya_pend`
--
ALTER TABLE `index_kes_daya_pend`
  MODIFY `id_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `index_pem_manusia`
--
ALTER TABLE `index_pem_manusia`
  MODIFY `id_ipm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kepandudukan_jk`
--
ALTER TABLE `kepandudukan_jk`
  MODIFY `id_kepend_jk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kependudukan_kel_umur`
--
ALTER TABLE `kependudukan_kel_umur`
  MODIFY `id_kel_umur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `kependudukan_rasio_ketergantungan`
--
ALTER TABLE `kependudukan_rasio_ketergantungan`
  MODIFY `id_rasio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kerja_angkatan`
--
ALTER TABLE `kerja_angkatan`
  MODIFY `id_angkatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kerja_pengangguran`
--
ALTER TABLE `kerja_pengangguran`
  MODIFY `id_pengangguran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kerja_ump`
--
ALTER TABLE `kerja_ump`
  MODIFY `id_ump` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kesehatan_gizi_balita`
--
ALTER TABLE `kesehatan_gizi_balita`
  MODIFY `id_gizi_balita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `keu_jenis`
--
ALTER TABLE `keu_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `keu_jml`
--
ALTER TABLE `keu_jml`
  MODIFY `id_jml_keu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `kominfo_aplikasi`
--
ALTER TABLE `kominfo_aplikasi`
  MODIFY `id_aplikasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `kominfo_domain`
--
ALTER TABLE `kominfo_domain`
  MODIFY `id_aplikasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `lp_aparat`
--
ALTER TABLE `lp_aparat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `lp_aparat_jenis`
--
ALTER TABLE `lp_aparat_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `lp_bencana`
--
ALTER TABLE `lp_bencana`
  MODIFY `id_bencana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `lp_bencana_jenis`
--
ALTER TABLE `lp_bencana_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lp_kajian_penelitian`
--
ALTER TABLE `lp_kajian_penelitian`
  MODIFY `id_kajian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `lp_kel_umur`
--
ALTER TABLE `lp_kel_umur`
  MODIFY `id_kel_umur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `lp_kel_umur_jenis`
--
ALTER TABLE `lp_kel_umur_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `lp_kendaran_kec`
--
ALTER TABLE `lp_kendaran_kec`
  MODIFY `id_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `lp_kendaran_plat`
--
ALTER TABLE `lp_kendaran_plat`
  MODIFY `id_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `lp_kendaran_plat_jenis`
--
ALTER TABLE `lp_kendaran_plat_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `lp_kendaran_umum`
--
ALTER TABLE `lp_kendaran_umum`
  MODIFY `id_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `lp_kendaran_umum_jenis`
--
ALTER TABLE `lp_kendaran_umum_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lp_kesehatan`
--
ALTER TABLE `lp_kesehatan`
  MODIFY `id_kesehatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `lp_kesehatan_jenis`
--
ALTER TABLE `lp_kesehatan_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `lp_kesehatan_kategori`
--
ALTER TABLE `lp_kesehatan_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lp_kesehatan_sub3_jenis`
--
ALTER TABLE `lp_kesehatan_sub3_jenis`
  MODIFY `id_3sub_jenis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lp_kesehatan_sub_jenis`
--
ALTER TABLE `lp_kesehatan_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `lp_keu`
--
ALTER TABLE `lp_keu`
  MODIFY `id_keu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `lp_keu_jenis`
--
ALTER TABLE `lp_keu_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lp_keu_sub_jenis`
--
ALTER TABLE `lp_keu_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `lp_kooperasi_jenis`
--
ALTER TABLE `lp_kooperasi_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `lp_kooperasi_sub_jenis`
--
ALTER TABLE `lp_kooperasi_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `lp_koperasi`
--
ALTER TABLE `lp_koperasi`
  MODIFY `id_koperasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `lp_lahan`
--
ALTER TABLE `lp_lahan`
  MODIFY `id_lahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `lp_lahan_jenis`
--
ALTER TABLE `lp_lahan_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lp_miskin`
--
ALTER TABLE `lp_miskin`
  MODIFY `id_miskin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `lp_miskin_jenis`
--
ALTER TABLE `lp_miskin_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lp_or`
--
ALTER TABLE `lp_or`
  MODIFY `id_or` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `lp_or_jenis`
--
ALTER TABLE `lp_or_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `lp_pajak`
--
ALTER TABLE `lp_pajak`
  MODIFY `id_pajak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `lp_pajak_jenis`
--
ALTER TABLE `lp_pajak_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `lp_panen`
--
ALTER TABLE `lp_panen`
  MODIFY `id_panen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `lp_panen_jenis`
--
ALTER TABLE `lp_panen_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lp_pdb`
--
ALTER TABLE `lp_pdb`
  MODIFY `id_pdb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT for table `lp_pdb_jenis`
--
ALTER TABLE `lp_pdb_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lp_pdb_kategori`
--
ALTER TABLE `lp_pdb_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `lp_pendidikan`
--
ALTER TABLE `lp_pendidikan`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=782;

--
-- AUTO_INCREMENT for table `lp_pendidikan_jenis`
--
ALTER TABLE `lp_pendidikan_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lp_pendidikan_kategori`
--
ALTER TABLE `lp_pendidikan_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `lp_penduduk_jk`
--
ALTER TABLE `lp_penduduk_jk`
  MODIFY `id_penduduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `lp_pend_industri`
--
ALTER TABLE `lp_pend_industri`
  MODIFY `id_pend` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `lp_pend_industri_jenis`
--
ALTER TABLE `lp_pend_industri_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lp_pend_industri_kategori`
--
ALTER TABLE `lp_pend_industri_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lp_perpustakaan`
--
ALTER TABLE `lp_perpustakaan`
  MODIFY `id_perpustakaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `lp_perpustakaan_jenis`
--
ALTER TABLE `lp_perpustakaan_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `lp_pertanian`
--
ALTER TABLE `lp_pertanian`
  MODIFY `id_pertanian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;

--
-- AUTO_INCREMENT for table `lp_pertanian_jenis`
--
ALTER TABLE `lp_pertanian_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lp_pertanian_sub_jenis`
--
ALTER TABLE `lp_pertanian_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `lp_pidana`
--
ALTER TABLE `lp_pidana`
  MODIFY `id_pidana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `lp_pidana_jenis`
--
ALTER TABLE `lp_pidana_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `lp_pidana_kec`
--
ALTER TABLE `lp_pidana_kec`
  MODIFY `id_pidana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `lp_pmks`
--
ALTER TABLE `lp_pmks`
  MODIFY `id_pmks` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `lp_pmks_jenis`
--
ALTER TABLE `lp_pmks_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `lp_tehnologi`
--
ALTER TABLE `lp_tehnologi`
  MODIFY `id_tehnologi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `lp_tehnologi_jenis`
--
ALTER TABLE `lp_tehnologi_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lp_tehnologi_sub_jenis`
--
ALTER TABLE `lp_tehnologi_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `lp_tenaga`
--
ALTER TABLE `lp_tenaga`
  MODIFY `id_tenaga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `lp_tenaga_jenis`
--
ALTER TABLE `lp_tenaga_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `lp_tenaga_kategori`
--
ALTER TABLE `lp_tenaga_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lp_terminal`
--
ALTER TABLE `lp_terminal`
  MODIFY `id_terminal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `lp_terminal_jenis`
--
ALTER TABLE `lp_terminal_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lp_terminal_sub_jenis`
--
ALTER TABLE `lp_terminal_sub_jenis`
  MODIFY `id_sub_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `lp_tpa`
--
ALTER TABLE `lp_tpa`
  MODIFY `id_tpa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `main_dinas`
--
ALTER TABLE `main_dinas`
  MODIFY `id_dinas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `main_jenis_kendaraan`
--
ALTER TABLE `main_jenis_kendaraan`
  MODIFY `id_jenis_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `main_jenis_penyakit`
--
ALTER TABLE `main_jenis_penyakit`
  MODIFY `id_jenis_penyakit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `main_jenjang_pend`
--
ALTER TABLE `main_jenjang_pend`
  MODIFY `id_jenjang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_partai`
--
ALTER TABLE `main_partai`
  MODIFY `id_partai` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `main_pendidikan`
--
ALTER TABLE `main_pendidikan`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `miskin_jml_pend`
--
ALTER TABLE `miskin_jml_pend`
  MODIFY `id_pend_miskin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `miskin_pengeluaran_perkapita`
--
ALTER TABLE `miskin_pengeluaran_perkapita`
  MODIFY `id_perkapita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pem_dpr`
--
ALTER TABLE `pem_dpr`
  MODIFY `id_pem_dpr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pem_jml_rt_rw`
--
ALTER TABLE `pem_jml_rt_rw`
  MODIFY `id_rt_rw` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pendapatan_lap_usaha`
--
ALTER TABLE `pendapatan_lap_usaha`
  MODIFY `id_lap_usaha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pendapatan_lap_usaha_jenis`
--
ALTER TABLE `pendapatan_lap_usaha_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pendapatan_regional`
--
ALTER TABLE `pendapatan_regional`
  MODIFY `id_pendapatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pendidikan_baca_tulis`
--
ALTER TABLE `pendidikan_baca_tulis`
  MODIFY `id_baca_tulis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pendidikan_baca_tulis_jenis`
--
ALTER TABLE `pendidikan_baca_tulis_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pendidikan_jml_sklh`
--
ALTER TABLE `pendidikan_jml_sklh`
  MODIFY `id_jml_sklh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `pendidikan_jml_sklh_jenis`
--
ALTER TABLE `pendidikan_jml_sklh_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pendidikan_partisipasi_sklh`
--
ALTER TABLE `pendidikan_partisipasi_sklh`
  MODIFY `id_partisipasi_sklh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pendidikan_partisipasi_sklh_jenis`
--
ALTER TABLE `pendidikan_partisipasi_sklh_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pendidikan_penduduk`
--
ALTER TABLE `pendidikan_penduduk`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pendidikan_penduduk_jenis`
--
ALTER TABLE `pendidikan_penduduk_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pendidikan_rasio_guru_murid`
--
ALTER TABLE `pendidikan_rasio_guru_murid`
  MODIFY `id_rasio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `pendidikan_rasio_guru_murid_jenis`
--
ALTER TABLE `pendidikan_rasio_guru_murid_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pengeluaran_penduduk`
--
ALTER TABLE `pengeluaran_penduduk`
  MODIFY `id_pengeluaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pengeluaran_penduduk_jenis`
--
ALTER TABLE `pengeluaran_penduduk_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pengeluaran_rmt_tgg`
--
ALTER TABLE `pengeluaran_rmt_tgg`
  MODIFY `id_rmt_tgg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pertanian_holti_jenis`
--
ALTER TABLE `pertanian_holti_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `pertanian_holti_jml`
--
ALTER TABLE `pertanian_holti_jml`
  MODIFY `id_holti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT for table `pertanian_ikan_jenis`
--
ALTER TABLE `pertanian_ikan_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pertanian_ikan_jml`
--
ALTER TABLE `pertanian_ikan_jml`
  MODIFY `id_ikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pertanian_kelapa_tebu`
--
ALTER TABLE `pertanian_kelapa_tebu`
  MODIFY `id_pertanian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pertanian_komoditas_jenis`
--
ALTER TABLE `pertanian_komoditas_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pertanian_komoditas_jml`
--
ALTER TABLE `pertanian_komoditas_jml`
  MODIFY `id_komoditas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `pertanian_lahan`
--
ALTER TABLE `pertanian_lahan`
  MODIFY `id_lahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pertanian_lahan_penggunaan`
--
ALTER TABLE `pertanian_lahan_penggunaan`
  MODIFY `id_peng` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pertanian_ternak_jenis`
--
ALTER TABLE `pertanian_ternak_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `pertanian_ternak_jml`
--
ALTER TABLE `pertanian_ternak_jml`
  MODIFY `id_ternak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `tehnologi_jml_bts`
--
ALTER TABLE `tehnologi_jml_bts`
  MODIFY `id_jml_bts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tehnologi_jml_warnet`
--
ALTER TABLE `tehnologi_jml_warnet`
  MODIFY `id_jml_warnet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tehnologi_jml_web_opd`
--
ALTER TABLE `tehnologi_jml_web_opd`
  MODIFY `id_web_opd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `trans_jln`
--
ALTER TABLE `trans_jln`
  MODIFY `id_jln` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `trans_jml_kendaraan`
--
ALTER TABLE `trans_jml_kendaraan`
  MODIFY `id_jml_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

var chart;
var graph;
var categoryAxis;

var chartData = 
[
  {
	  "country": "Kedungkandang",
		"visits": 12,
    "color": "#3c8dbc"
	},
	{
		"country": "Sukun",
		"visits": 11,
    "color": "#f56954"
	}, 
	{
		"country": "Klojen",
		"visits": 11,
    "color": "#00a65a"
	}, 
	{
	  "country": "Blimbing",
		"visits": 11,
    "color": "#f9f934"
	}, 
	{
		"country": "Lowokwaru",
		"visits": 12,
    "color": "#7a2bda"
	}
];


AmCharts.ready(function () {
  chart = new AmCharts.AmSerialChart();
	chart.dataProvider = chartData;
	chart.categoryField = "country";
  chart.position = "left";
  chart.angle = 30;
	chart.depth3D = 15;
  chart.startDuration = 1;
  
  categoryAxis = chart.categoryAxis;
	categoryAxis.labelRotation = 0;
  categoryAxis.dashLength = 5; //
  categoryAxis.gridPosition = "start";
  categoryAxis.autoGridCount = false;
	categoryAxis.gridCount = chartData.length;
  
    
	graph = new AmCharts.AmGraph();
	graph.valueField = "visits";
	graph.type = "column";	
  graph.colorField = "color";
	graph.lineAlpha = 0;
  graph.fillAlphas = 0.8;
  graph.balloonText = "[[category]]: <b>[[value]]</b>";
  
  chart.addGraph(graph);
  
  chart.write('chartdiv');
});



// Reminder: you need to put https://www.google.com/jsapi in the head of your document or as an external resource on codepen //
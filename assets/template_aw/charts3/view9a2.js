$(function () {
        $('#containerer2').lowcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '____',
                style: {
                    color: 'white'
                }
            },
            xAxis: {
                categories: ['RT']
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (lowcharts.theme && lowcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: 300,
                verticalAlign: 'top',
                y: -7,
                floating: true,
                backgroundColor: (lowcharts.theme && lowcharts.theme.legendBackgroundColorSolid) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        this.series.name +': '+ this.y;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (lowcharts.theme && lowcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: [{
                name: 'Kota Malang',
                data: [4157]
            }, {
                name: 'Lowokwaru',
                data: [785]
            }, {
                name: 'Blimbing',
                data: [923]
            }, {
                name: 'Klojen',
                data: [675]
            }, {
                name: 'Sukun',
                data: [882]
            }, {
                name: 'Kedungkandang',
                data: [892]
            } ]
        });
    });
$(function () {
        $('#containerer1').lowcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '____',
                style: {
                    color: 'white'
                }
            },
            xAxis: {
                categories: ['RW']
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (lowcharts.theme && lowcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -170,
                verticalAlign: 'top',
                y: -7,
                floating: true,
                backgroundColor: (lowcharts.theme && lowcharts.theme.legendBackgroundColorSolid) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        this.series.name +': '+ this.y +'<br/>'+
                        'Total: '+ this.point.stackTotal;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (lowcharts.theme && lowcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: [{
                name: 'Kota Malang',
                data: [546]
            }, {
                name: 'Lowokwaru',
                data: [120]
            }, {
                name: 'Blimbing',
                data: [127]
            }, {
                name: 'Klojen',
                data: [89]
            }, {
                name: 'Sukun',
                data: [94]
            }, {
                name: 'Kedungkandang',
                data: [116]
            } ]
        });
    });
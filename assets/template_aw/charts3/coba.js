// config mysql
var knex = require('knex')({
  client: 'mysql',
  connection: {
    socketPath : '/path/to/socket.sock',
    user : 'localhost',
    password : '',
    database : 'mop'
  }
});

// pakai query builder nya
knex.select().from('rtrw') // return ini 'promise ', jadi kalo mau ambil value nya di 'chaining' pakai '.then'
.then(function(values) {
console.log(values)
})
Outputs:
select * from `rtrw`

$(function () {
        $('#containerer').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '____',
                style: {
                    color: 'white'
                }
            },
            xAxis: {
                categories: ['2014', '2015', '2016']
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -170,
                verticalAlign: 'top',
                y: -7,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        this.series.name +': '+ this.y +'<br/>'+
                        'Total: '+ this.point.stackTotal;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: [{
                name: 'RW',
                data: [544,544,546]
            }, {
                name: 'RT',
                data: [4088, 4125, 4157]
            }  ]
        });
    });
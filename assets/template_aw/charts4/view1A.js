window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: ""
      
      },   
      data: [{        
        type: "column",
        dataPoints: [
        { label: "2014", y: 845973},
        { label: "2015", y: 851298},
        { label: "2016", y: 856410}
        ]
      },
      {        
        type: "line",
        dataPoints: [
        {label: "2014", y: 416982},
        {label: "2015", y: 419713},
        {label: "2016", y: 422276}
        ]
      },
      {        
        type: "line",
        dataPoints: [
        {label: "2014", y: 428991},
        {label: "2015", y: 431585},
        {label: "2016", y: 434134}
        ]
      }

        
      ]
    });

    chart.render();
  }
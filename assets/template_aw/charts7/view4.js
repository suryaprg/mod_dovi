window.onload = function () {
    var chart = new CanvasJS.Chart("Contain",
    {
      title:{
        text: ""
      
      },   
      data: [{        
        type: "column",
        dataPoints: [
        { label: "2014", y: 4.80},
        { label: "2015", y: 4.60},
        { label: "2016", y: 4.33}
        ]
      },
      {        
        type: "line",
        dataPoints: [
        {label: "2014", y: 40.64},
        {label: "2015", y: 39.10},
        {label: "2016", y: 37.03}
        ]
      }

        
      ]
    });

    chart.render();
  }
$(function () {
        $('#contains').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '---'
            },
            xAxis: {
                categories: ['≥ 1000 000','750 000 S/D 999 999','500 000 S/D 749 999','300 000 S/D 499 999','≤ 299 999']
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -265,
                verticalAlign: 'top',
                y: 0,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        this.series.name +': '+ this.y +'<br/>'+
                        'Total: '+ this.point.stackTotal;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: [{
                name: '2014',
                data: [40.93, 18.03, 23.32, 15.88, 1.84]
            }, {
                name: '2015',
                data: [44.97, 14.19, 22.33, 16.11, 2.40]
            }, {
                name: '2016',
                data: [50.38, 13.12, 14.66, 19.16, 2.69]
            }  ]
        });
    });
$(function () {
        $('#containing').lowcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '---'
            },
            xAxis: {
                categories: ['2014','2015','2016']
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (lowcharts.theme && lowcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -185,
                verticalAlign: 'top',
                y: 0,
                floating: true,
                backgroundColor: (lowcharts.theme && lowcharts.theme.legendBackgroundColorSolid) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        this.series.name +': '+ this.y +'<br/>'+
                        'Total: '+ this.point.stackTotal;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (lowcharts.theme && lowcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: [{
                name: 'Pengeluaran Non Makanan',
                data: [61.91, 60.79, 61.37]
            }, {
                name: 'Pengeluaran Makanan',
                data: [38.09, 39.21, 38.63]
            }  ]
        });
    });
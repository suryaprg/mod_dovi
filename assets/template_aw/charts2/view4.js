$(function () {
        $('#containers').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '---',
                style: {
                    color: 'white'
                }
            },
            xAxis: {
                categories: ['Nasdem','Hanura','PPP','Gerindra','PAN','PKS','PKB','Demokrat','Golkar','PDI Perjuangan']
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -250,
                verticalAlign: 'top',
                y: 0,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        this.series.name +': '+ this.y +'<br/>'+
                        'Total: '+ this.point.stackTotal;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: [{
                name: 'Perempuan',
                data: [0,1,2,1,0,0,0,2,1,4]
            }, {
                name: 'Laki - laki',
                data: [1,2,1,3,4,3,6,3,4,7]
            }  ]
        });
    });
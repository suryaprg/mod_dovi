$(function () {
  $('#contain').highcharts({
    title: {
      text: '',
      x: -20 //center
    },
    colors: ['blue', 'red'],
    plotOptions: {
      line: {
        lineWidth: 3
      },
      tooltip: {
        hideDelay: 200
      }
    },
    subtitle: {
      text: '',
      x: -20
    },
    xAxis: {
      categories: ['2014','2015','2016']
    },
    yAxis: {
      title: {
        text: ''
      },
      plotLines: [{
        value: 0,
        width: 1
      }]
    },
    tooltip: {
      valueSuffix: '',
      crosshairs: true,
      shared: true
    },
    legend: {
      layout: 'vertical',
      align: 'center',
      verticalAlign: 'bottom',
      borderWidth: 0
    },
    series: [
    {
      name: 'Jumlah Balita Kurang Gizi',
      color: 'rgba(0,0,0,0.75)',
      lineWidth: 2,
      marker: {
        radius: 6
      },
      data: [1611, 1627, 1868]
    },
    {
      name: 'Jumlah Balita Gizi Buruk',
      color: 'rgba(165,6,149,0.75)',
      lineWidth: 2,
      data: [119, 100, 66]
    } ]
  });
});
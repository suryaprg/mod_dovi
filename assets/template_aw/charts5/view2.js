$(function () {
        $('#containering').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '---'
            },
            xAxis: {
                categories: ['Headache','Dermatitis Kontak Alergi','Penyakit Pula dan Jar Perapikal','Myalgia/Nyeri Otot','OBS Febris','Influensa, Virus tidak diidentifikasi','DM','Gastritis','Hipertensi Primer','Infeksi Saluran Pernafasan']
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: 0,
                verticalAlign: 'top',
                y: 0,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        this.series.name +': '+ this.y +'<br/>'+
                        'Total: '+ this.point.stackTotal;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: [{
                name: 'Kasus',
                data: [7966, 8718, 8819, 9025, 10773, 12743, 13815, 13840, 32109, 55351]
            }  ]
        });
    });
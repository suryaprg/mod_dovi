<html>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_aw/css/bootstraps.min.css">
<link rel="shortcut icon" href="https://malangkota.go.id/wp-content/themes/malangkotanew/images/favicon.png">

<head>
    <title>Login Admin</title>
</head>

<body>
    <center>
        <form style="margin-top: 8em;" class="form-signin" action="<?php echo base_url()?>adminlogin/auth" method="post">
            <br>
            <br>
            <h3>Malang Open Data</h3>
            <br>
                    <div class="form-group col-md-4">
                        <label for="user_login">Username</label>
                        <input type="text" name="email" class="form-control" size="20" placeholder="Username" autocomplete="off" required autofocus>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="user_pass">Password</label>
                        <input type="password" name="password" class="form-control" size="20" placeholder="Password" required>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-brand btn-info col-md-1" value="Sign In">
                    </div>

        </form>
    </center>

    <script src="<?php echo base_url(); ?>assets/template_aw//js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/template_aw/js/bootstrap.bundle.min.js"></script>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MALANG OPEN DATA</title>
	
	<!--ini favicon-->
    <link rel="icon" type="image/png" href="<?=base_url()."assets";?>/template/resource/img/logo.png" />

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()."assets";?>/template/resource/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?=base_url()."assets";?>/template/resource/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="<?=base_url()."assets";?>/template/resource/css/agency.min.css" rel="stylesheet">
      
    <!-- menu -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?=base_url()."assets";?>/template/resource/css/menu.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="<?=base_url()."assets";?>/template/resource/js/page.js"></script>
      
  </head>

  <body id="page-top">



    <!-- Header -->
    <body>
<header class="heropanel--video" data-vide-bg="mp4: img/east.mp4, poster: https://www.gordonmac.com/wp-content/uploads/storm-1.png" data-vide-options="posterType: png, loop: false, muted: true, position: 50% 50%">
    <div class="heropanel__content">
         <h1><a class="js-scroll-trigger" href="#services" rel="home">Malang Open Data</a></h1>
         <p>Subtitle</p>
    </div>
 </header>
      
    <!-- Navigation -->
    <div id='cssmenu'>
        <ul>
            <li><a href='<?=base_url()."beranda/kepegawaian";?>'>Kepegawaian</a></li>
           <li><a href='<?=base_url()."beranda/unitkerja";?>'>Unit Kerja</a></li>
           <li class='active'><a href='<?=base_url()."beranda/tentang";?>'>Tentang</a></li>
           <li><a href='<?=base_url()."beranda/profil";?>'>Profil</a></li>
           <li><a href='<?=base_url()."beranda/";?>'>Home</a></li>
        </ul>
      </div>


    <!-- Services -->
    <section id="services">
      <div class="container">  
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Profil Kota Malang</h2>
            <h3 class="section-subheading text-muted">Profil singkat Kota Malang</h3>
              <h4 align="justify" class="service-heading">Geografis dan Iklim</h4>
              <p align="justify">Kota malang adalah sebuah kota di Provinsi Jawa Timur Indonesia. Kota ini berada di dataran tinggi cukup sejuk pada ketinggian antara 440-667 meter diatas permukaan air laut, terletak pada 90 km sebelah selatan kota surabaya dan wilayahnya di kelilingi oleh Kabupaten Malang. Secara astronomis terletak 112,06° - 112,07° Bujur Timur dan  7,06° - 8,02° Lintang Selatan, dengan batas wilayah sebagai berikut :</p>
              <ol align="justify">
                  <li>Sebelah Utara, Kecamatan Singosari dan Kec. Karangploso Kabupaten Malang</li>
                  <li>Sebelah Timur, Kecamatan Pakis dan Kecamatan Tumpang Kabupaten Malang</li>
                  <li>Sebelah Selatan, Kecamatan Tajinan dan Kecamatan Pakisaji Kabupaten Malang</li>
                  <li>Sebelah Barat, Kecamatan Wagir dan Kecamatan Dau Kabupaten Malang</li>
              </ol>
              <p align="justify">Dan juga dikelilingi oleh gunung-gunung :</p>
              <ol align="justify">
                  <li>Gunung Arjuno di sebelah Utara</li>
                  <li>Gunung Semeru di sebelah Timur</li>
                  <li>Gunung Kawi dan Panderman di sebelah Barat</li>
                  <li>Gunung Kelud di sebelah Selatan</li>
              </ol>
              <p align="justify">
              Karena letaknya yang berada di dataran tinggi, membuat Kota Malang mempunyai udara yang sejuk. Suhu udara berkisar antara 22,2°C – 24,5°C. Suhu maksimum mencapai 32,2°C dan suhu miminum 17,8°C. Seperti halnya daerah lain di Indonesia, Malang mengikuti perubahan putaran 2 iklim yaitu musim hujan, dan musim kemarau. </p>
              <p align="justify">Kota Malang adalah kota besar kedua di Jawa Timur setelah Ibu kota Propinsi, Surabaya. Kota Malang mulai beranjak menjadi kota Metropolis. Namun yang pasti, Kota Malang menawarkan kesejukan dan keramahan udara yang berbeda dari kota Metropolis lainya. Kota Malang hanya berjarak sekitar 15 km dari Kota Batu yang terkenal dengan produksi apel dan juga berbagai wisata alamnya.</p>
              <p align="justify">Kota Malang dilalui oleh sebuah sungai besar, yaitu Sungai Brantas yang merupakan sungai terpanjang kedua di pulau Jawa.</p>
              <p align="justify">Ada 4 macam tanah di wilayah Kota Malang, antara lain :</p>
              <ol align="justify">
                  <li>aluvial kelabu kehitaman dengan luas 6.930.267 ha</li>
                  <li>mediteran coklat dengan luas 1.225.160 ha</li>
                  <li>asosiasi lotosol coklat kemerahan atau keabu-abuan dengan luas 1.942.160 ha</li>
                  <li>sosiasi andosol coklat dan humus kelabu dengan luas 1.765,160 ha</li>
              </ol>
              <p align="justify">Agama mayoritas di Kota Malang adalah agama Islam, diikuti dengan Kristen Protestan, Kristen Katolik, Hindu, Buddha, dan Kong Hu Cu. Bangunan tempat ibadah banyak yang telah berdiri semenjak zaman dahulu antara lain Masjid Agung Jami’ Kota Malang, Gereja Hati Kudus Yesus, Katedral Ijen (Santa Perawan Maria dari Gunung Karmel), Klenteng Eng An Kiong di Kotalama, dan sebuah Pura di puncak Buring. Meskipun agama mayoritasnya adalah islam, Kota Malang menjadi salah satu kota yang memiliki jumlah penduduk kristen terbesar di Jawa Timur.</p>
              <p align="justify">Pada Februari 2015, Pemerintah Kota Malang meluncurkan sistem angkutan bus tingkat berwarna hijau yang dinamai Bus Macito, singkatan dari Malang City Tour yang disediakan secara gratis dan khusus untuk para wisatawan. Bus ini beroperasi di Kota Malang dari depan gedung DPRD Kota Malang dan rutenya melewati beberapa titik-titik penting di tiap sudut kota, diantaranya beberapa museum-museum penting, kawasan jalan Ijen, wisata kuliner,dan sebagainya. Bus ini berkapasitas 40 penumpang dengan jatah keliling sebanyak tiga kali.</p>
              <h4 align="justify" class="service-heading">Ekonomi</h4>
              <p align="justify">Kota Malang memiliki perekonomian yang maju dan majemuk dan merupakan kawasan ekonomi yang disorot oleh pemprov Jawa Timur. PDRB Kota Malang mencapai 57.171,60 miliar rupiah dengan kontribusi ekonomi sebesar 3,06% terhadap PDRB Jawa Timur; Kota Malang adalah Kota dengan PDRB terbesar ketiga se-Jawa Timur. PDRB per kapita Kota Malang, yakni 66.758,1 ratus ribu rupiah merupakan keenam terbesar se-Jawa Timur, setelah Kabupaten Pasuruan.</p>
              <p align="justify">Perekonomian Kota Malang ditunjang dari berbagai sektor, di antaranya industri, jasa, perdagangan, dan pariwisata. Sektor yang menyumbang terbanyak adalah perdagangan yang menyumbang 29,53% dari total PDRB Kota Malang. Malang pun terkenal dengan salah satu perusahaan rokok terkenal, yaitu Bentoel.</p>
              <p align="justify">Adanya peranan UMKM menjadikan ekonomi di Kota Malang berkembang pesat.</p>
              <p align="justify">Pada tahun 2016, ekonomi Kota Malang tumbuh sebesar 5,61%. Pertumbuhan ekonomi yang cukup pesat ini didongkrak oleh pariwisata. Selain itu, pertumbuhan ekonomi pesat pun dikontribusikan oleh UMKM, industri, dan perdagangan.</p>
              <h4 align="justify" class="service heading">Suku Bangsa</h4>
              <p align="justify">Sebagian besar masyarakat Kota Malang berasal dari suku Jawa. Namun, jika dibanding dengan masyaarakat Jawa pada umumnya, suku Jawa di Malang memiliki temperamen yang sedikit lebih keras dan egalitar. Salah satu penyebabnya adalah tipologi arek Malang terinspirasi oleh Ken Arok yang diceritakan sebagai raja yang tegas dan lugas meskipun lebih mengarah keras. Terdapat pula sejumlah suku-suku minoritas seperti Madura, Arab, Tionghoa, dan lain-lain. Sebagai kota pendidikan, Malang juga menjadi tempat tinggal mahasiswa dari berbagai daerah dari seluruh Indonesia, bahkan diantara mereka juga membentuk wadah komunitas sendiri.</p>
              <h4 align="justify" class="service heading">Monumen dan Tugu</h4>
              <p align="justify">Sebagai kota besar, Kota Malang terlibat dalam berbagai peristiwa bersejarah yang terjadi di Indonesia. Untuk menandai peristiwa tersebut, dibangunlah berbagai monumen dan tugu peringatan. Kota Malang mengoleksi banyak monumen dan tugu peringatan yang melambangkan peristiwa bersejarah, sejarah prapenjajahan Malang, capaian Kota Malang, dan lain-lain. Peristiwa-peristiwa bersejarah, terutama perjuangan kemerdekaanlah yang memiliki monumen terbanyak. Monumen-monumen tersebut, di antaranya Monumen Tugu Malang yang menandakan kemerdekaan dari Belanda; Monumen TGP (Tentara Genie Pelajar) yang dibangun untuk mengenang perjuangan para TGP; Monumen Pahlawan Tentara Republik Indonesia Pelajar (TRIP), monumen pengenangan para sosok pahlawan TRIP; Monumen Juang '45 yang menandakan runtuhnya penjajahan; Monumen Hamid Rusdi yang mengenang Hamid Rusdi; Monumen Panglima Sudirman yang mengenang perjuangan Panglima Soedirman; Monumen KNIP Malang, monumen sejarah Komite Nasional Indonesia Pusat (KNIP); dan Monumen Melati (Monumen Kadet Suropati), monumen penghargaan terhadap sekolah darurat di awal pembentukan Tentara Keamanan Rakyat (TKR).</p>
              <h4 align="justify" class="service-heading">Penghargaan</h4>
              <p align="justify">Di bidang lingkungan, Kota Malang telah beberapa kali meraih penghargaan di antaranya Adipura, Adiwiyata, dan sebagainya. Selain itu, Kota Malang adalah kota dengan jumlah sekolah Adiwiyata terbanyak di Indonesia, yaitu 173 sekolah yang tersebar dari SD hingga SMP. Dinas Lingkungan Hidup Kota Malang pun mendapatkan penghargaan Air Minum dan Penyehatan Lingkungan (AMPL) 2017 dari Menteri PPN/Kepala Bappenas. AMPL pun diraih oleh kota karena kota mampu mengurangi sampah di 2016 yang mencapai 15,1% dan cakupan akses layanan persampahan sebesar 74,8%. Pada tahun 2017, kota berhasil meraih penghargaan Wahana Tata Nugraha karena mampu mengubah lingkungan kumuh menjadi objek wisata seperti Kampung Tematik Jodipan. Banyaknya penghargaan yang diperoleh kota pun memberikan dampak pada peningkatan Dana Insentif Daerah (DID) dari 7,5 miliar rupiah pada tahun 2017 menjadi 25,5 miliar pada tahun 2018.</p>
              <h4 align="justify" class="service-heading">Visi dan Misi</h4>
              <h5 align="justify">Visi :</h5>
              <p align="justify">“MENJADIKAN KOTA MALANG SEBAGAI KOTA BERMARTABAT”</p>
              <h5 align="justify">Misi :</h5>
              <ul align="justify">
                  <li><strong>Meningkatkan kualitas, aksesibilitas, dan pemerataan pelayanan pendidikan dan kesehatan</strong>
                  <p>Tujuan 1: Terwujudnya peningkatan kualitas, aksesibilitas dan pemerataan pelayanan pendidikan</p>
                      <p>Tujuan 2: Terwujudnya peningkatan kualitas, aksesibilitas dan pemerataan pelayanan kesehatan</p>
                  </li>
                  <li><strong>Meningkatkan produktivitas dan daya saing daerah</strong>
                  <p>Tujuan 1: Terwujudnya peningkatan perekonomian daerah melalui penguatan sektor koperasi dan usaha kecil menengah, perindustrian dan perdagangan, serta pariwisata daerah.</p>
                      <p>Tujuan 2: Terwujudnya perluasan kesempatan kerja</p>
                      <p>Tujuan 3: Terwujudnya ketersediaan dan akses pangan</p>
                  </li>
                  <li><strong>Meningkatkan kesejahteraan dan perlindungan terhadap masyarakat rentan, pengarusutamaan gender, serta kerukunan sosial</strong>
                      <p>Tujuan 1: Terwujudnya peningkatan perlindungan terhadap masyarakat rentan dan pengentasan kemiskinan</p>
                      <p>Tujuan 2: Terwujudnya peningkatan kualitas kehidupan dan peran peran perempuan, serta terjaminnya pengarusutamaan gender</p>
                      <p>Tujuan 3: Terwujudnya peningkatan kualitas kerukunan sosial masyarakat</p>
                  </li>
                  <li><strong>Meningkatnya pembangunan infrastruktur dan daya dukung Kota yang terpadu dan berkelanjutan, tertib penataan ruang serta berwawasan lingkungan</strong>
                      <p>Tujuan 1: Terwujudnya peningkatan kualitas infrastruktur dan daya dukung kota</p>
                      <p>Tujuan 2: Terwujudnya peningkatan tertib pemanfaatan ruang kota sesuai peruntukannya</p>
                  </li>
                  <li><strong>Mewujudkan pelaksanaan reformasi birokrasi dan kualitas pelayanan publik yang profesional, akuntabel dan berorientasi pada kepuasan masyarakat</strong>
                      <p>Tujuan 1: Terwujudnya transparansi dan akuntabilitas Kinerja Pemerintah Daerah</p>
                      <p>Tujuan 2: Terwujudnya peningkatan kualitas pelayanan publik yang profesional, akuntabel, dan berorientasi pada kepuasan masyarakat</p>
                  </li>
              </ul>
          </div>
        </div>
        <div class="row text-center">
    
        </div>
      </div>
    </section>
   
    <!-- Contact -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Kirim Pesan</h2>
            <!--<h3 class="section-subheading text-muted">Ketik Komentar Anda Disini</h3> -->
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="name" type="text" placeholder="Nama *" required data-validation-required-message="Nama wajib diisi.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" type="email" placeholder="Email *" required data-validation-required-message="Email wajib diisi.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <!--<div class="form-group">
                    <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required data-validation-required-message="Please enter your phone number.">
                    <p class="help-block text-danger"></p>
                  </div> -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="Pesan *" required data-validation-required-message="Pesan tidak boleh kosong."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-xl" type="submit">Kirim</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section> 

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Malang Open Data 2018</span>
          </div>
         <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="https://twitter.com/share?url=http://localhost/malang/index.html" target="_blank">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="http://www.facebook.com/sharer.php?url=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=&lt;?php echo $url; ?&gt; &amp;title=&lt;?php echo $title;?&gt;&amp;summary=&lt;?php echo $summary;?&gt;&amp;source=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-pinterest"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="https://plus.google.com/share?url=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-google-plus"></i>
                </a>
              </li>
            </ul>
          </div>
          <!-- <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div> -->
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/vendor/jquery/jquery.min.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/vendor/popper/popper.min.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/js/jqBootstrapValidation.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?=base_url()."assets";?>/template/resource/js/agency.min.js"></script>
      
    <!-- scroll top top -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
var scrolltotop={setting:{startline:100,scrollto:0,scrollduration:1e3,fadeduration:[500,100]},controlHTML:'<img src="https://i1155.photobucket.com/albums/p559/scrolltotop/arrow3.png" />',controlattrs:{offsetx:5,offsety:5},anchorkeyword:"#top",state:{isvisible:!1,shouldvisible:!1},scrollup:function(){this.cssfixedsupport||this.$control.css({opacity:0});var t=isNaN(this.setting.scrollto)?this.setting.scrollto:parseInt(this.setting.scrollto);t="string"==typeof t&&1==jQuery("#"+t).length?jQuery("#"+t).offset().top:0,this.$body.animate({scrollTop:t},this.setting.scrollduration)},keepfixed:function(){var t=jQuery(window),o=t.scrollLeft()+t.width()-this.$control.width()-this.controlattrs.offsetx,s=t.scrollTop()+t.height()-this.$control.height()-this.controlattrs.offsety;this.$control.css({left:o+"px",top:s+"px"})},togglecontrol:function(){var t=jQuery(window).scrollTop();this.cssfixedsupport||this.keepfixed(),this.state.shouldvisible=t>=this.setting.startline?!0:!1,this.state.shouldvisible&&!this.state.isvisible?(this.$control.stop().animate({opacity:1},this.setting.fadeduration[0]),this.state.isvisible=!0):0==this.state.shouldvisible&&this.state.isvisible&&(this.$control.stop().animate({opacity:0},this.setting.fadeduration[1]),this.state.isvisible=!1)},init:function(){jQuery(document).ready(function(t){var o=scrolltotop,s=document.all;o.cssfixedsupport=!s||s&&"CSS1Compat"==document.compatMode&&window.XMLHttpRequest,o.$body=t(window.opera?"CSS1Compat"==document.compatMode?"html":"body":"html,body"),o.$control=t('<div id="topcontrol">'+o.controlHTML+"</div>").css({position:o.cssfixedsupport?"fixed":"absolute",bottom:o.controlattrs.offsety,right:o.controlattrs.offsetx,opacity:0,cursor:"pointer"}).attr({title:"Scroll to Top"}).click(function(){return o.scrollup(),!1}).appendTo("body"),document.all&&!window.XMLHttpRequest&&""!=o.$control.text()&&o.$control.css({width:o.$control.width()}),o.togglecontrol(),t('a[href="'+o.anchorkeyword+'"]').click(function(){return o.scrollup(),!1}),t(window).bind("scroll resize",function(t){o.togglecontrol()})})}};scrolltotop.init();</script>
<noscript>Not seeing a <a href="https://www.scrolltotop.com/">Scroll to Top Button</a>? Go to our FAQ page for more info.</noscript>
        
    <!-- vide -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://www.gordonmac.com/wp-content/themes/2016/vendor/vide/jquery.vide.min.js"></script>


      
  </body>

</html>

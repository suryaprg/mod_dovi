<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MALANG OPEN DATA</title>
	
	<!--ini favicon-->
    <link rel="icon" type="image/png" href="<?=base_url()."assets";?>/template/resource/img/logo.png" />

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()."assets";?>/template/resource/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?=base_url()."assets";?>/template/resource/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="<?=base_url()."assets";?>/template/resource/css/agency.min.css" rel="stylesheet">
      
    <!-- menu -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?=base_url()."assets";?>/template/resource/css/menu.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="<?=base_url()."assets";?>/template/resource/js/page.js"></script>
      
  </head>

  <body id="page-top">



    <!-- Header -->
    <body>
        <header class="heropanel--video" data-vide-bg="mp4: img/east.mp4, poster: https://www.gordonmac.com/wp-content/uploads/storm-1.png" data-vide-options="posterType: png, loop: false, muted: true, position: 50% 50%">
            <div class="heropanel__content">
                <h1><a class="js-scroll-trigger" href="#services" rel="home">Malang Open Data</a></h1>
                <p>Subtitle</p>
            </div>
        </header>
      
    <!-- Navigation -->
    <div id='cssmenu'>
        <ul>
            <li><a href='<?=base_url()."beranda/kepegawaian";?>'>Kepegawaian</a></li>
           <li><a href='<?=base_url()."beranda/unitkerja";?>'>Unit Kerja</a></li>
           <li class='active'><a href='<?=base_url()."beranda/tentang";?>'>Tentang</a></li>
           <li><a href='<?=base_url()."beranda/profil";?>'>Profil</a></li>
           <li><a href='<?=base_url()."beranda/";?>'>Home</a></li>
        </ul>
      </div>


    <!-- Services -->
    <section id="services">
      <div class="container">  
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Kepegawaian</h2>
            <h3 class="section-subheading text-muted">Kepegawaian Diskominfo Kota Malang</h3>
              <h4 align="center" class="service-heading">Struktur Organisasi</h4>
              <p align=center><img src="img/struktur_organisasi.jpg" class="responsive" width="800" height="500">
              </p>
                  
              <h4 align="center" class="service-heading">Pejabat Struktural</h4>
              <table align="center" bgcolor="honeydew" bordercolor="gainsboro" rules="none" border="1" width=70%>
                  <tr>
                      <td bgcolor="lightblue" width=7%><strong>No.</strong></td>
                      <td bgcolor="lightblue" align="left" width=40%><strong>Jabatan</strong></td>
                      <td bgcolor="lightblue" align="left" width=30%><strong>Nama Pejabat</strong></td>
                  </tr>
                  <tr>
                      <td>1.</td>
                      <td align="left" width=40%><strong>Kepala Dinas</strong></td>
                      <td align="left" width=30%><strong>ZULKIFLI AMRIZAL, S.Sos, M.Si</strong></td>
                  </tr>
                  <tr>
                      <td>2.</td>
                      <td align="left" width=40%><strong>Sekretaris Dinas</strong></td>
                      <td align="left" width=30%><strong>Ir. YAYUK HERMIATI, MH</strong></td>
                  </tr>
                  <tr>
                      <td>3.</td>
                      <td align="left" width=40%>Kasubbag Perencanaan</td>
                      <td align="left" width=30%>Dra. Ec. SRI SUHARTINI</td>
                  </tr>
                  <tr>
                      <td>4.</td>
                      <td align="left" width=40%>Kasubbag Keuangan</td>
                      <td align="left" width=30%>PRIHATNI SETYANINGSIH, SE, MM</td>
                  </tr>
                  <tr>
                      <td>5.</td>
                      <td align="left" width=40%>Kasubbag Umum dan Kepegawaian</td>
                      <td align="left" width=30%>SRI MULYANI, SH</td>
                  </tr>
                  <tr>
                      <td>6.</td>
                      <td align="left" width=40%><strong>Kepala Bidang Statistik</strong></td>
                      <td align="left" width=30%><strong>Drs. SUKARYONO</strong></td>
                  </tr>
                  <tr>
                      <td>7.</td>
                      <td align="left" width=40%>Kepala Seksi Pengelolaan Data Statistik</td>
                      <td align="left" width=30%>BASTIAN PADMA H., SE, M.Si</td>
                  </tr>
                  <tr>
                      <td>8.</td>
                      <td align="left" width=40%>Kepala Seksi Layanan Data dan Informasi Statistik</td>
                      <td align="left" width=30%>Dra. ERNA WIYATI</td>
                  </tr>
                  <tr>
                      <td>9.</td>
                      <td align="left" width=40%>Kepala Seksi Evaluasi dan Pelaporan</td>
                      <td align="left" width=30%>MOHAMAD RIDWAN, STP, MM</td>
                  </tr>
                  <tr>
                      <td>10.</td>
                      <td align="left" width=40%><strong>Kepala Bidang Komunikasi Publik dan Persandian</strong></td>
                      <td align="left" width=30%><strong>Ir. AGUS TRI PUTRANTO, MT</strong></td>
                  </tr>
                  <tr>
                      <td>11.</td>
                      <td align="left" width=40%>Kepala Seksi Sarana dan Prasarana Komunikasi Publik</td>
                      <td align="left" width=30%>Dra. NUR CHAMIDAH</td>
                  </tr>
                  <tr>
                      <td>12.</td>
                      <td align="left" width=40%>Kepala Seksi Pengelolaan dan Kemitraan Komunikasi Publik</td>
                      <td align="left" width=30%>FEBRIAN RETNOSARI, S.Sos, M.Si</td>
                  </tr>
                  <tr>
                      <td>13.</td>
                      <td align="left" width=40%>Kepala Seksi Persandian</td>
                      <td align="left" width=30%>Dra. MOELYO ARDIATI, MM</td>
                  </tr>
                  <tr>
                      <td>14.</td>
                      <td align="left" width=40%><strong>Kepala Bidang Aplikasi Informatika (Aptika)</strong></td>
                      <td align="left" width=30%><strong>Ir. TITIS ANDAYANI, MM</strong></td>
                  </tr>
                  <tr>
                      <td>15.</td>
                      <td align="left" width=40%>Kepala Seksi Pengelolaan e-Gov</td>
                      <td align="left" width=30%>ITA NURDIYAH, S.Kom</td>
                  </tr>
                  <tr>
                      <td>16.</td>
                      <td align="left" width=40%>Kepala Seksi  Sarana Prasana Informatika</td>
                      <td align="left" width=30%>DIDIK SUPRIYADI</td>
                  </tr>
                  <tr>
                      <td>17.</td>
                      <td align="left" width=40%>Kepala Seksi Pemberdayaan Teknologi Informasi dan Komunikasi Aplikasi</td>
                      <td align="left" width=30%>ZULKIFLI EKO PRIONO, BSC</td>
                  </tr>
                  <tr>
                      <td>18.</td>
                      <td align="left" width=40%><strong>Kepala Bidang Informasi Publik (BIP)</strong></td>
                      <td align="left" width=30%><strong>ISMINTARTI, SP</strong></td>
                  </tr>
                  <tr>
                      <td>19.</td>
                      <td align="left" width=40%>Kepala Seksi Pengelolaan Informasi Publik</td>
                      <td align="left" width=30%>LAODE KB AL FITRA, SP, MM</td>
                  </tr>
                  <tr>
                      <td>20.</td>
                      <td align="left" width=40%>Kepala Seksi Layanan Informasi</td>
                      <td align="left" width=30%>DEDY SURFIANTO, SE</td>
                  </tr>
                  <tr>
                      <td>21.</td>
                      <td align="left" width=40%>Kepala Seksi Pemberdayaan dan Kemitraaan Informasi Publik</td>
                      <td align="left" width=30%>DJOKO PURNOMO</td>
                  </tr>
                  <tr>
                      <td>22.</td>
                      <td align="left" width=40%><strong>Kepala UPT Layanan Pengadaan Secara Elektronik (LPSE)	</strong></td>
                      <td align="left" width=30%><strong>REDY JANANDJAJA SUSELOHADI, SH, MM</strong></td>
                  </tr>
                  <tr>
                      <td>23.</td>
                      <td align="left" width=40%>Kasubbag Tata Usaha</td>
                      <td align="left" width=30%>ERKAN WIKANTO, S.Sos, M.Si</td>
                  </tr>
                  <tr>
                      <td>24.</td>
                      <td align="left" width=40%><strong>Kepala UPT Pusat Kendali (Command Center)</strong></td>
                      <td align="left" width=30%><strong>M. WAHYU HIDAYAT , S.Kom, MM</strong></td>
                  </tr>
              </table>
                            
          </div>
        </div>
        <div class="row text-center">
       
        </div>
      </div>
    </section>
      
      <!-- contact -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Kirim Pesan</h2>
            <!--<h3 class="section-subheading text-muted">Ketik Komentar Anda Disini</h3> -->
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="name" type="text" placeholder="Nama *" required data-validation-required-message="Nama wajib diisi.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" type="email" placeholder="Email *" required data-validation-required-message="Email wajib diisi.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <!--<div class="form-group">
                    <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required data-validation-required-message="Please enter your phone number.">
                    <p class="help-block text-danger"></p>
                  </div> -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="Pesan *" required data-validation-required-message="Pesan tidak boleh kosong."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-xl" type="submit">Kirim</button> 
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
   
   

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Malang Open Data 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="https://twitter.com/share?url=http://localhost/malang/index.html" target="_blank">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="http://www.facebook.com/sharer.php?url=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=&lt;?php echo $url; ?&gt; &amp;title=&lt;?php echo $title;?&gt;&amp;summary=&lt;?php echo $summary;?&gt;&amp;source=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-pinterest"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="https://plus.google.com/share?url=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-google-plus"></i>
                </a>
              </li>
            </ul>
          </div>
          <!-- <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div> -->
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/vendor/jquery/jquery.min.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/vendor/popper/popper.min.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/js/jqBootstrapValidation.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?=base_url()."assets";?>/template/resource/js/agency.min.js"></script>
      
    <!-- scroll top top -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://arrow.scrolltotop.com/arrow3.js"></script>
    <noscript>Not seeing a <a href="http://www.scrolltotop.com/">Scroll to Top Button</a>? Go to our FAQ page for more info.</noscript>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
var scrolltotop={setting:{startline:100,scrollto:0,scrollduration:1e3,fadeduration:[500,100]},controlHTML:'<img src="https://i1155.photobucket.com/albums/p559/scrolltotop/arrow3.png" />',controlattrs:{offsetx:5,offsety:5},anchorkeyword:"#top",state:{isvisible:!1,shouldvisible:!1},scrollup:function(){this.cssfixedsupport||this.$control.css({opacity:0});var t=isNaN(this.setting.scrollto)?this.setting.scrollto:parseInt(this.setting.scrollto);t="string"==typeof t&&1==jQuery("#"+t).length?jQuery("#"+t).offset().top:0,this.$body.animate({scrollTop:t},this.setting.scrollduration)},keepfixed:function(){var t=jQuery(window),o=t.scrollLeft()+t.width()-this.$control.width()-this.controlattrs.offsetx,s=t.scrollTop()+t.height()-this.$control.height()-this.controlattrs.offsety;this.$control.css({left:o+"px",top:s+"px"})},togglecontrol:function(){var t=jQuery(window).scrollTop();this.cssfixedsupport||this.keepfixed(),this.state.shouldvisible=t>=this.setting.startline?!0:!1,this.state.shouldvisible&&!this.state.isvisible?(this.$control.stop().animate({opacity:1},this.setting.fadeduration[0]),this.state.isvisible=!0):0==this.state.shouldvisible&&this.state.isvisible&&(this.$control.stop().animate({opacity:0},this.setting.fadeduration[1]),this.state.isvisible=!1)},init:function(){jQuery(document).ready(function(t){var o=scrolltotop,s=document.all;o.cssfixedsupport=!s||s&&"CSS1Compat"==document.compatMode&&window.XMLHttpRequest,o.$body=t(window.opera?"CSS1Compat"==document.compatMode?"html":"body":"html,body"),o.$control=t('<div id="topcontrol">'+o.controlHTML+"</div>").css({position:o.cssfixedsupport?"fixed":"absolute",bottom:o.controlattrs.offsety,right:o.controlattrs.offsetx,opacity:0,cursor:"pointer"}).attr({title:"Scroll to Top"}).click(function(){return o.scrollup(),!1}).appendTo("body"),document.all&&!window.XMLHttpRequest&&""!=o.$control.text()&&o.$control.css({width:o.$control.width()}),o.togglecontrol(),t('a[href="'+o.anchorkeyword+'"]').click(function(){return o.scrollup(),!1}),t(window).bind("scroll resize",function(t){o.togglecontrol()})})}};scrolltotop.init();</script>
<noscript>Not seeing a <a href="https://www.scrolltotop.com/">Scroll to Top Button</a>? Go to our FAQ page for more info.</noscript>
        
        <!-- vide -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://www.gordonmac.com/wp-content/themes/2016/vendor/vide/jquery.vide.min.js"></script>


      
  </body>

</html>

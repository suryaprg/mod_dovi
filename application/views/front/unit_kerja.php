<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MALANG OPEN DATA</title>
	
	<!--ini favicon-->
    <link rel="icon" type="image/png" href="<?=base_url()."assets";?>/template/resource/img/logo.png" />

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()."assets";?>/template/resource/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?=base_url()."assets";?>/template/resource/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="<?=base_url()."assets";?>/template/resource/css/agency.min.css" rel="stylesheet">
      
    <!-- menu -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?=base_url()."assets";?>/template/resource/css/menu.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="<?=base_url()."assets";?>/template/resource/js/page.js"></script>
      
  </head>

  <body id="page-top">



    <!-- Header -->
    <body>
        <header class="heropanel--video" data-vide-bg="mp4: img/east.mp4, poster: https://www.gordonmac.com/wp-content/uploads/storm-1.png" data-vide-options="posterType: png, loop: false, muted: true, position: 50% 50%">
            <div class="heropanel__content">
                <h1><a class="js-scroll-trigger" href="#services" rel="home">Malang Open Data</a></h1>
                <p>Subtitle</p>
            </div>
        </header>
      
    <!-- Navigation -->
    <div id='cssmenu'>
        <ul>
            <li><a href='<?=base_url()."beranda/kepegawaian";?>'>Kepegawaian</a></li>
           <li><a href='<?=base_url()."beranda/unitkerja";?>'>Unit Kerja</a></li>
           <li class='active'><a href='<?=base_url()."beranda/tentang";?>'>Tentang</a></li>
           <li><a href='<?=base_url()."beranda/profil";?>'>Profil</a></li>
           <li><a href='<?=base_url()."beranda/";?>'>Home</a></li>
        </ul>
      </div>


    <!-- Services -->
    <section id="services">
      <div class="container">  
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Unit Kerja</h2>
            <h3 class="section-subheading text-muted">Unit Kerja Dinas Komunikasi dan Informatika</h3>
              <h4 align="center" class="service-heading">Unit Kerja Dinas Komunikasi dan Informatika, Terdiri Dari:</h4>
              <table align="center" bgcolor="gainsboro" bordercolor="white" rules="rows" border="2" width=70%>
                  <tr align="left">
                      <td align="center" width=4%>a.</td>
                      <td>Kepala Dinas;</td>
                  </tr>
                  <tr align="left">
                      <td align="center">b.</td>
                      <td>Sekretariat, terdiri dari:</td>
                  </tr>
                  <tr>
                      <td></td>
                      <td>
                          <table rules="none" border="0">
                              <tr align="left">
                                  <td>1.</td>
                                  <td>Subbagian Penyusunan Program;</td>
                              </tr>
                              <tr align="left">
                                  <td>2.</td>
                                  <td>Subbagian Keuangan;</td>
                              </tr>
                              <tr align="left">
                                  <td>3.</td>
                                  <td>Subbagian Umum;</td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr align="left">
                      <td align="center">c.</td>
                      <td>Bidang Informasi Publik, terdiri dari :</td>
                  </tr>
                  <tr>
                      <td></td>
                      <td>
                          <table rules="none" border="0">
                              <tr align="left">
                                  <td>1.</td>
                                  <td>Seksi Pengelolaan Informasi Publik;</td>
                              </tr>
                              <tr align="left">
                                  <td>2.</td>
                                  <td>Seksi Layanan Informasi Publik;</td>
                              </tr>
                              <tr align="left">
                                  <td>3.</td>
                                  <td>Seksi Pemberdayaan dan Kemitraan Informasi Publik;</td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr align="left">
                      <td align="center">d.</td>
                      <td>Bidang Aplikasi Informatika, terdiri dari:</td>
                  </tr>
                  <tr align="left">
                      <td></td>
                      <td>
                          <table rules="none" border="0">
                              <tr align="left">
                                  <td>1.</td>
                                  <td>Seksi Pengelolaan e-Gov;</td>
                              </tr>
                              <tr align="left">
                                  <td>2.</td>
                                  <td>Seksi Sarana Prasarana Informatika;</td>
                              </tr>
                              <tr align="left">
                                  <td>3.</td>
                                  <td>Seksi Pemberdayaan Teknologi Informasi dan Komunikasi (TIK);</td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr align="left">
                      <td align="center">e.</td>
                      <td>Bidang Statistik, terdiri dari:</td>
                  </tr>
                  <tr align="left">
                      <td></td>
                      <td>
                          <table rules="none" border="0">
                              <tr align="left">
                                  <td>1.</td>
                                  <td>Seksi Pengelolaan Data Statistik;</td>
                              </tr>
                              <tr align="left">
                                  <td>2.</td>
                                  <td>Seksi Layanan Data dan Informasi Statistik;</td>
                              </tr>
                              <tr align="left">
                                  <td>3.</td>
                                  <td>Seksi Evaluasi dan Pelaporan;</td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr align="left">
                      <td align="center">f.</td>
                      <td>Bidang Pengelolaan Komunikasi Publik dan Persandian, terdiri dari :</td>
                  </tr>
                  <tr align="left">
                      <td></td>
                      <td>
                          <table rules="none" border="0">
                              <tr align="left">
                                  <td>1.</td>
                                  <td>Seksi Sarana Prasarana Komunikasi Publik;</td>
                              </tr>
                              <tr align="left">
                                  <td>2.</td>
                                  <td>Seksi Pengelolaan dan Kemitraan Komunikasi Publik;</td>
                              </tr>
                              <tr align="left">
                                  <td>3.</td>
                                  <td>Seksi Persandian;</td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr align="left">
                      <td align="center">g.</td>
                      <td>Unit Pelaksana Teknis (UPT);</td>
                  </tr>
                  <tr align="left">
                      <td align="center">h.</td>
                      <td>Kelompok Jabatan Fungsional.</td>
                  </tr>
                  <tr align="left">
                      <td align="center">i.</td>
                      <td>Sekretariat dipimpin oleh Sekretaris dan Bidang dipimpin oleh Kepala Bidang yang dalam melaksanakan tugas pokok dan fungsinya berada di bawah dan bertanggung jawab kepada Kepala Dinas.</td>
                  </tr>
              </table>
              
          </div>
        </div>
        <div class="row text-center">
       
        </div>
      </div>
    </section>
   
       <!-- Contact -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Kirim Pesan</h2>
            <!--<h3 class="section-subheading text-muted">Ketik Komentar Anda Disini</h3> -->
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="name" type="text" placeholder="Nama *" required data-validation-required-message="Nama wajib diisi.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" type="email" placeholder="Email *" required data-validation-required-message="Email wajib diisi.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <!--<div class="form-group">
                    <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required data-validation-required-message="Please enter your phone number.">
                    <p class="help-block text-danger"></p>
                  </div> -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="Pesan *" required data-validation-required-message="Pesan tidak boleh kosong."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-xl" type="submit">Kirim</button> 
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Malang Open Data 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="https://twitter.com/share?url=http://localhost/malang/index.html" target="_blank">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="http://www.facebook.com/sharer.php?url=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=&lt;?php echo $url; ?&gt; &amp;title=&lt;?php echo $title;?&gt;&amp;summary=&lt;?php echo $summary;?&gt;&amp;source=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-pinterest"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="https://plus.google.com/share?url=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-google-plus"></i>
                </a>
              </li>
            </ul>
          </div>
          <!-- <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div> -->
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/vendor/jquery/jquery.min.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/vendor/popper/popper.min.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/js/jqBootstrapValidation.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?=base_url()."assets";?>/template/resource/js/agency.min.js"></script>
      
    <!-- scroll top top -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
var scrolltotop={setting:{startline:100,scrollto:0,scrollduration:1e3,fadeduration:[500,100]},controlHTML:'<img src="https://i1155.photobucket.com/albums/p559/scrolltotop/arrow3.png" />',controlattrs:{offsetx:5,offsety:5},anchorkeyword:"#top",state:{isvisible:!1,shouldvisible:!1},scrollup:function(){this.cssfixedsupport||this.$control.css({opacity:0});var t=isNaN(this.setting.scrollto)?this.setting.scrollto:parseInt(this.setting.scrollto);t="string"==typeof t&&1==jQuery("#"+t).length?jQuery("#"+t).offset().top:0,this.$body.animate({scrollTop:t},this.setting.scrollduration)},keepfixed:function(){var t=jQuery(window),o=t.scrollLeft()+t.width()-this.$control.width()-this.controlattrs.offsetx,s=t.scrollTop()+t.height()-this.$control.height()-this.controlattrs.offsety;this.$control.css({left:o+"px",top:s+"px"})},togglecontrol:function(){var t=jQuery(window).scrollTop();this.cssfixedsupport||this.keepfixed(),this.state.shouldvisible=t>=this.setting.startline?!0:!1,this.state.shouldvisible&&!this.state.isvisible?(this.$control.stop().animate({opacity:1},this.setting.fadeduration[0]),this.state.isvisible=!0):0==this.state.shouldvisible&&this.state.isvisible&&(this.$control.stop().animate({opacity:0},this.setting.fadeduration[1]),this.state.isvisible=!1)},init:function(){jQuery(document).ready(function(t){var o=scrolltotop,s=document.all;o.cssfixedsupport=!s||s&&"CSS1Compat"==document.compatMode&&window.XMLHttpRequest,o.$body=t(window.opera?"CSS1Compat"==document.compatMode?"html":"body":"html,body"),o.$control=t('<div id="topcontrol">'+o.controlHTML+"</div>").css({position:o.cssfixedsupport?"fixed":"absolute",bottom:o.controlattrs.offsety,right:o.controlattrs.offsetx,opacity:0,cursor:"pointer"}).attr({title:"Scroll to Top"}).click(function(){return o.scrollup(),!1}).appendTo("body"),document.all&&!window.XMLHttpRequest&&""!=o.$control.text()&&o.$control.css({width:o.$control.width()}),o.togglecontrol(),t('a[href="'+o.anchorkeyword+'"]').click(function(){return o.scrollup(),!1}),t(window).bind("scroll resize",function(t){o.togglecontrol()})})}};scrolltotop.init();</script>
<noscript>Not seeing a <a href="https://www.scrolltotop.com/">Scroll to Top Button</a>? Go to our FAQ page for more info.</noscript>

      <!-- vide -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://www.gordonmac.com/wp-content/themes/2016/vendor/vide/jquery.vide.min.js"></script>
        
  </body>

</html>

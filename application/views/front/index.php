<!doctype html>
<html lang=''>
<head>
    <!-- dari index -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MALANG OPEN DATA</title>
  
  <!--ini favicon-->
    <link rel="icon" type="image/png" href="<?=base_url()."assets";?>/template/resource/img/logo.png" />

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()."assets";?>/template/resource/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?=base_url()."assets";?>/template/resource/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="<?=base_url()."assets";?>/template/resource/css/agency.min.css" rel="stylesheet">
    <link href="<?=base_url()."assets";?>/template/resource/css/custom.css" rel="stylesheet">
      
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

  <!-- menu -->  
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="<?=base_url()."assets";?>/template/resource/css/menu.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="<?=base_url()."assets";?>/template/resource/js/page.js"></script>
    
    <!--img-hover-direction --> 
    <link href='http://fonts.googleapis.com/css?family=Alegreya+SC:700,400italic' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="<?=base_url()."assets";?>/template/resource/css/style.css" />
    <script src="<?=base_url()."assets";?>/template/resource/js/modernizr.custom.97074.js"></script>
    <noscript><link rel="stylesheet" type="text/css" href="<?=base_url()."assets";?>/template/resource/css/noJS.css"/></noscript>


    
</head>
<body id="page-top">
    
<!-- header -->  
<body>
<header class="heropanel--video" data-vide-bg="mp4: <?=base_url()."assets";?>/template/resource/img/east.mp4, poster: https://www.gordonmac.com/wp-content/uploads/storm-1.png" data-vide-options="posterType: png, loop: true, muted: true, position: 50% 50%">
    <div class="heropanel__content">
         <h1><a class="js-scroll-trigger" href="#services" rel="home">Malang Open Data</a></h1>
         <p>Pemerintah Kota Malang</p>
    </div>
 </header>
    
<!-- menu -->
<div id='cssmenu'>
<ul>
   <li><a href='<?=base_url()."beranda/kepegawaian";?>'>Kepegawaian</a></li>
   <li><a href='<?=base_url()."beranda/unitkerja";?>'>Unit Kerja</a></li>
   <li><a href='<?=base_url()."beranda/tentang";?>'>Tentang</a></li>
   <li><a href='<?=base_url()."beranda/profil";?>'>Profil</a></li>
   <li class='active'><a href='<?=base_url()."beranda/";?>'>Home</a></li>
</ul>
</div>
    
<!-- Services -->
    <section id="services">
      <div class="container">  
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading"><strong> Kategori</strong></h2>
            <h3 class="section-subheading text-muted">Pilih Kategori</h3> 
          </div>
        </div>
        <div class="row text-center">
            
          <!--untuk geografi, iklim, & lingkungan hidup-->
        <div class="col-md-3">
              <div class="thumbnail">
                  <div class="caption">
                      <h4>Geografi, Iklim & Lingkunan Hidup</h4>
                        <p>Description</p>
                        <p>
                          <a href="opendata/Posisi_Geografi_dan_Batas_Wilayah" class="btn btn-danger tombol">Read More</a>
                        </p>
                    </div>
                    <img src="<?=base_url()."assets";?>/template/resource/img/icon_kategori/geografi.jpg">
                </div>
            </div>

           <!--untuk sosial, politik, & keamanan-->
          <div class="col-md-3">
              <div class="thumbnail">
                  <div class="caption">
                      <h4>Sosial, Politik, & Keamanan</h4>
                        <p>Description</p>
                        <p>
                          <a href="opendata/Organisasi_Pemuda" class="btn btn-danger tombol">Read More</a>
                        </p>
                    </div>
                    <img src="<?=base_url()."assets";?>/template/resource/img/icon_kategori/sosial.jpg">
                </div>
            </div>
    
          <!--untuk pemerintahan-->
         <div class="col-md-3">
              <div class="thumbnail">
                  <div class="caption">
                      <h4>Pemerintahan</h4>
                        <p>Description</p>
                        <p>
                          <a href="opendata/Kepegawaian" class="btn btn-danger tombol">Read More</a>
                        </p>
                    </div>
                    <img class="responsive" src="<?=base_url()."assets";?>/template/resource/img/icon_kategori/pemerintahan1.jpg">
                </div>
            </div>
           <!--untuk penduduk dan angkatan kerja-->
         <div class="col-md-3">
              <div class="thumbnail">
                  <div class="caption">
                      <h4>Penduduk Dan Angkatan Kerja</h4>
                        <p>Description</p>
                        <p>
                          <a href="opendata/Berdasar_Jenis_Kelamin" class="btn btn-danger tombol">Read More</a>
                        </p>
                    </div>
                    <img src="<?=base_url()."assets";?>/template/resource/img/icon_kategori/kependudukan1.jpg">
                </div>
            </div>
            
        <!--untuk kesehatan-->
          <div class="col-md-3">
              <div class="thumbnail">
                  <div class="caption">
                      <h4>Kesehatan</h4>
                        <p>Description</p>
                        <p>
                          <a href="opendata/Balita_Kurang_Gizi_dan_Gizi_buruk" class="btn btn-danger tombol">Read More</a>
                        </p>
                    </div>
                    <img class="responsive" src="<?=base_url()."assets";?>/template/resource/img/icon_kategori/kesehatan.jpg">
                </div>
            </div>
            
        <!--untuk pendidikan-->
         <div class="col-md-3">
              <div class="thumbnail">
                  <div class="caption">
                      <h4>Pendidikan</h4>
                        <p>Description</p>
                        <p>
                          <a href="opendata/Angka_Buta_Aksara" class="btn btn-danger tombol">Read More</a>
                        </p>
                    </div>
                    <img class="responsive" src="<?=base_url()."assets";?>/template/resource/img/icon_kategori/perpustakaan1.jpg">
                </div>
            </div>
            
        <!--untuk ekonomi, perdagangan, dan keuangan-->
          <div class="col-md-3">
              <div class="thumbnail">
                  <div class="caption">
                      <h4>Ekonomi, Perdagangan, dan Keuangan</h4>
                        <p>Description</p>
                        <p>
                          <a href="opendata/Ketahanan_Pangan" class="btn btn-danger tombol">Read More</a>
                        </p>
                    </div>
                    <img class="responsive" src="<?=base_url()."assets";?>/template/resource/img/icon_kategori/ekonomi1.jpg">
                </div>
            </div>
            
        <!--untuk infrastruktur transportasi dan infromasi-->
          <div class="col-md-3">
              <div class="thumbnail">
                  <div class="caption">
                      <h4>Infrastruktur Transportasi dan Informasi</h4>
                        <p>Description</p>
                        <p>
                          <a href="opendata/Jalan" class="btn btn-danger tombol">Read More</a>
                        </p>
                    </div>
                    <img class="responsive" src="<?=base_url()."assets";?>/template/resource/img/icon_kategori/infrastruktur.jpg">
                </div>
            </div>
            
        </div>
      </div>
    </section>

<!-- Contact -->
   <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading"><strong>Kirim Pesan</strong></h2>
            <h3 class="section-subheading text-muted"></h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="name" type="text" placeholder="Nama *" required data-validation-required-message="Please enter your name.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" type="email" placeholder="Email *" required data-validation-required-message="Please enter your email address.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <!--<div class="form-group">
                    <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required data-validation-required-message="Please enter your phone number.">
                    <p class="help-block text-danger"></p>
                  </div>-->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="Pesan *" required data-validation-required-message="Please enter a message."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="button" type="submit">Kirim</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    
<!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Malang Open Data 2018</span>
          </div>
         <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="https://twitter.com/share?url=http://localhost/malang/index.html" target="_blank">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="http://www.facebook.com/sharer.php?url=http://localhost/malang/index.html" target="_blank">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
            <li class="list-inline-item">
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=&lt;?php echo $url; ?&gt; &amp;title=&lt;?php echo $title;?&gt;&amp;summary=&lt;?php echo $summary;?&gt;&amp;source=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-pinterest"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="https://plus.google.com/share?url=http://localhost/malang/index.html" target="_blank">
                  <i class="fa fa-google-plus"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    

    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/vendor/jquery/jquery.min.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/vendor/popper/popper.min.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/js/jqBootstrapValidation.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?=base_url()."assets";?>/template/resource/js/agency.min.js"></script>
    
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
      <!-- Latest compiled and minified JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

      <script>
       $('.thumbnail').hover(
             function(){
                 $(this).find('.caption').slideDown(250); //.fadeIn(250)
              },
              function(){
                  $(this).find('.caption').slideUp(250); //.fadeOut(205)
              }
        ); 
      </script>
    
    <!-- img-hover-directions -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="<?=base_url()."assets";?>/template/resource/js/jquery.hoverdir.js"></script> 
    <script type="text/javascript">
      $(function() {
      
        $(' #da-thumbs > li ').each( function() { $(this).hoverdir(); } );

      });
    </script>
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://www.gordonmac.com/wp-content/themes/2016/vendor/vide/jquery.vide.min.js"></script>

</body>
<html>

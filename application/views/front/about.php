<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Malang Open Data</title>
	
	<!--ini favicon-->
    <link rel="icon" type="image/png" href="<?=base_url()."assets";?>/template/resource/img/logo.png" />

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()."assets";?>/template/resource/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="<?=base_url()."assets";?>/template/resource/css/agency.min.css" rel="stylesheet">
      
    <!-- menu -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?=base_url()."assets";?>/template/resource/css/menu.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="<?=base_url()."assets";?>/template/resource/js/page.js"></script>
      
  </head>

  <body id="page-top">



    <!-- Header -->
    <body>
        <header class="heropanel--video" data-vide-bg="mp4: img/east.mp4, poster: https://www.gordonmac.com/wp-content/uploads/storm-1.png" data-vide-options="posterType: png, loop: false, muted: true, position: 50% 50%">
            <div class="heropanel__content">
                <h1><a class="js-scroll-trigger" href="#services" rel="home">Malang Open Data</a></h1>
                <p>Subtitle</p>
            </div>
        </header>
      
    <!-- Navigation -->
    <div id='cssmenu'>
        <ul>
           <li><a href='<?=base_url()."beranda/kepegawaian";?>'>Kepegawaian</a></li>
           <li><a href='<?=base_url()."beranda/unitkerja";?>'>Unit Kerja</a></li>
           <li class='active'><a href='<?=base_url()."beranda/tentang";?>'>Tentang</a></li>
           <li><a href='<?=base_url()."beranda/profil";?>'>Profil</a></li>
           <li><a href='<?=base_url()."beranda/";?>'>Home</a></li>
        </ul>
      </div>

    <!-- Services -->
    <section id="services">
      <div class="container">  
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Tentang Kota Malang</h2>
            <h3 class="section-subheading text-muted"><strong>Sejarah Singkat Kota Malang</strong></h3>
              <p align="justify">Berawal dari munculnya kerajaan Kanjuruhan, yang oleh para ahli dipandang sebagai tonggak awal pertumbuhan pusat pemerintahan yang sampai saat ini, setelah 12 abad berselang, telah berkembang menjadi Kota Malang </p>
              <p align="justify">Setelah Kerajaan Kanjuruhan, pada masa emas kerajaan Singasari (1000 tahun setelah Masehi) Malang masih ditemukan satu kerajaan yang makmur, banyak penduduknya serta tanah-tanah pertanian yang amat subur. Ketika Islam menaklukkan Kerajaan Majapahit sekitar tahun 1400, Patih Majapahit melarikan diri ke daerah Malang. Kemudian, ia mendirikan sebuah kerajaan Hindu yang merdeka, yang diperjuangkan menjadi satu kerajaan yang maju oleh putranya. Pusat kerajaan yang terletak di kota Malang sampai saat ini masih terlihat sisa-sisa bangunan bentengnya yang kokoh bernama Kutobedah di Desa Kutobedah. Sultan Mataram dari Jawa Tengahlah yang akhirnya datang dan berhasil menaklukkan daerah ini pada tahun 1614 setelah mendapat perlawanan yang tangguh dari penduduk daerah ini.</p>
              <p align="justify">Seperti halnya kebanyakan kota-kota lain di Indonesia, Kota Malang modern tumbuh dan berkembang setelah hadirnya pemerintahan Kolonial Hindia Belanda. Fasilitas umum direncanakan sedemikian rupa agar memenuhi kebutuhan keluarga Belanda. Kesan diskriminatif masih berbekas hingga sekarang, misalnya Jalan Besar Ijen dan kawasan sekitarnya. Pada mulanya hanya dinikmati oleh keluarga-keluarga Belanda dan Bangsa Eropa lainnya, sementara penduduk pribumi harus puas bertempat tinggal di pinggiran kota dengan fasilitas yang kurang memadai. Kawasan perumahan itu sekarang menjadi monumen hidup dan seringkali dikunjungi oleh keturunan keluarga-keluarga Belanda yang pernah bermukim di sana.</p>
              <p align="justify">Pada masa penjajagan Hindia Belanda, Kota Malang dijadikan wilayah gemente (Kota).</p>
              <p align="justify">Pada tahun 1879, jalur kereta api mulai dioperasikan. Adanya jalur kereta api membuat Kota Malang mulai tumbuh dan berkembang pesat.</p>
              <p align="justify">Pada masa kependudukan Jepang di Indonesia, Malang yang termasuk dari bagian Indonesia tak luput dari jajahan Jepang. Bala tentara Dai Nippon mulai menduduki Kota Malang pada 7 Maret 1942.</p>
              <p align="justify">Alur singkat sejarah Kota Malang :</p>
              <ul>
                  <li align="justify">Abad ke-8 M, Malang menjadi ibu kota Kerajaan Kanjuruhan dengan rajanya, yaitu Gajayana</li>
                  <li align="justify">Tahun 1767, Kompeni Hindia Belanda memasuki Kota Malang</li>
                  <li align="justify">Tahun 1821, kependudukan Pemerintah Kolonial Belanda dipusatkan di sekitar Kali Brantas </li>
                  <li align="justify">Tahun 1824, Malang mulai mempunyai asisten residen karena sudah menjadi afdeling</li>
                  <li align="justify">Tahun 1882, Rumah-rumah didirikan di bagian barat kota dan alun-alun dibangun</li>
                  <li align="justify">1 April 1914, Malang ditetapkan sebagai kotapraja dan tanggal ini pun sekligus menjadi tanggal hari ulang tahun Kota Malang</li>
                  <li align="justify">8 Maret 1942, Malang diduduki Jepang melalui pemerintah kolonialnya</li>
                  <li align="justify">21 September 1945, Malang menjadi bagian dari republik Indonesia</li>
                  <li align="justify">22 Juli 1947, Malang diduduki kembali oleh Belanda</li>
                  <li align="justify">2 Maret 1947, Pemerintah Republik Indonesia kembali memasuki Kota Malang</li>
                  <li align="justify">1 Januari 2001, pemerintahan diubah menjadi Pemerintahan Kota Malang</li>
              </ul>
              <h4 align="justify" class="service-heading">Arti Kata Malang</h4>
              <p align="justify">Pada Lambang Kota Malang sekarang, tertera tulisan Malangkuceςwara yang memiliki arti <strong>Tuhan menghancurkann yang bathil (jahat) dan menegakkan yang benar</strong>. Sedangkan kata malang sendiir memiliki arti dalam bahasa Jawa menghalangi/membentarng, dan berasal dari kata palang. Dari bahasa Jawa inilah nama Kota Malang berasal. Namun banyak orang mengartikannya dalam bahasa Indonesia yang artinya sial/bernasib buruk, padahal tidak demikina aritnya.</p>
              <p align="justify">Pada masa Kolonial, Kota Malang sempat memiliki semboyan “Malang Nominor, Sursum Moveor” yang artinya “Malang Namaku, Maju Tujuanku”. Semboyan ini tertulisa dalam lambang Kota Malang pada tahun 1914.</p>
              <p align="justify">Berikut lambang Kota Malang tahun 1914 :</p>
              <div class="col-md-3">
                <a href="kategori_dbdes.php"><img src="<?=base_url()."assets";?>/template/resource/img/lambang-kota-malang-1.jpg" class="img-responsive" alt="Database" width="170" height="150">
                </a>
              </div>
              <p align="justify">Lalu pada tahun 1964 digunakan semboyan “MALANGKUCEςWARA “ dengan lambang yang baru pula. Berikut adalah lambang Kota Malang yang baru pada tahun 1964 :</p>
              <div class="col-md-3">
                <a href="kategori_dbdes.php"><img src="<?=base_url()."assets";?>/template/resource/img/lambang-kota-malang-2.jpg" class="img-responsive" alt="Database" width="170" height="150">
                </a>
              </div>
              <p align="justify">Yang memiliki arti sebagai begikut  :</p>
              <ul align="justify">
                  <li><strong>Merah, Putih</strong> : adalah warna lambang bendera nasional Indonesia</li>
                  <li><strong>Kuning</strong> : berarti Keluruhan dan Kebesaran</li>
                  <li><strong>Hijau</strong> : adalah Kesuburan</li>
                  <li><strong>Biru Muda</strong> : berarti kesetiaan pada Tuhan, Negara dan Bangsa </li>
                  <li>Segilima berbentuk perisai bermakna semangat perjuangan kepahlawanan, kondisi geografis, pegunungan, serta semangat membangun untuk mencapai masyarakat yang adil dan makmur berdasarkan Pancasila.</li>
              </ul>
              <p align="justify">Lambang ini digunakan sampai sekarang.</p>
              <h4 align="justify" class="service-heading">Boso Walikan</h4>
              <p align="justify">Orang malang terkenal suka memakai bahasa walikan, misal : Ngalup, Nakam, dll. Cara membacanya hanya tinggal dibalik saja kosakatanya.</p>
              <p align="justify">Jadi bahasa walikan khas Ngalam ini awalnya adalah bahasa yang biasa digunakan oleh preman pada jaman pejajahan Belanda. Preman disini adalah preman-preman baik yang membantu perjuangan rakyat kecil dari penindasan yang dilakukan oleh Belanda. Untuk membingungkan belanda dan mata-mata Belanda, maka para preman sengaja mempergunakan bahasa yang dibalik-balik dalam pengucapanya. Bahasa walikan ini mulai muncul pada tahun 1930-an dan yang dibalik tidak hanya bahasa Indonesia atau bahasa Jawa, tapi juga bahasa Arab dan bahasa Madura.</p>
          </div>
        </div>
        <div class="row text-center">
       
        </div>
      </div>
    </section>


    <!-- Contact -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Kirim Pesan</h2>
            <!--<h3 class="section-subheading text-muted">Ketik Komentar Anda Disini</h3> -->
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="name" type="text" placeholder="Nama *" required data-validation-required-message="Nama wajib diisi.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" type="email" placeholder="Email *" required data-validation-required-message="Email wajib diisi.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <!--<div class="form-group">
                    <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required data-validation-required-message="Please enter your phone number.">
                    <p class="help-block text-danger"></p>
                  </div> -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="Pesan *" required data-validation-required-message="Pesan tidak boleh kosong."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-xl" type="submit">Kirim</button> 
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
      

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Malang Open Data 2018</span>
          </div>
           <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="https://twitter.com/share?url=http://localhost/malang/index.html" target="_blank">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="http://www.facebook.com/sharer.php?url=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=&lt;?php echo $url; ?&gt; &amp;title=&lt;?php echo $title;?&gt;&amp;summary=&lt;?php echo $summary;?&gt;&amp;source=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-pinterest"></i>
                </a>
              </li>
                <li class="list-inline-item">
                <a href="https://plus.google.com/share?url=http://localhost/malang/profile.html" target="_blank">
                  <i class="fa fa-google-plus"></i>
                </a>
              </li>
              <!--<li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>-->
            </ul>
          </div>
          <!-- <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div> -->
        </div>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/vendor/jquery/jquery.min.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/vendor/popper/popper.min.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="<?=base_url()."assets";?>/template/resource/js/jqBootstrapValidation.js"></script>
    <script src="<?=base_url()."assets";?>/template/resource/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?=base_url()."assets";?>/template/resource/js/agency.min.js"></script>
      
   <!-- scroll top top -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
    var scrolltotop={setting:{startline:100,scrollto:0,scrollduration:1e3,fadeduration:[500,100]},controlHTML:'<img src="https://i1155.photobucket.com/albums/p559/scrolltotop/arrow3.png" />',controlattrs:{offsetx:5,offsety:5},anchorkeyword:"#top",state:{isvisible:!1,shouldvisible:!1},scrollup:function(){this.cssfixedsupport||this.$control.css({opacity:0});var t=isNaN(this.setting.scrollto)?this.setting.scrollto:parseInt(this.setting.scrollto);t="string"==typeof t&&1==jQuery("#"+t).length?jQuery("#"+t).offset().top:0,this.$body.animate({scrollTop:t},this.setting.scrollduration)},keepfixed:function(){var t=jQuery(window),o=t.scrollLeft()+t.width()-this.$control.width()-this.controlattrs.offsetx,s=t.scrollTop()+t.height()-this.$control.height()-this.controlattrs.offsety;this.$control.css({left:o+"px",top:s+"px"})},togglecontrol:function(){var t=jQuery(window).scrollTop();this.cssfixedsupport||this.keepfixed(),this.state.shouldvisible=t>=this.setting.startline?!0:!1,this.state.shouldvisible&&!this.state.isvisible?(this.$control.stop().animate({opacity:1},this.setting.fadeduration[0]),this.state.isvisible=!0):0==this.state.shouldvisible&&this.state.isvisible&&(this.$control.stop().animate({opacity:0},this.setting.fadeduration[1]),this.state.isvisible=!1)},init:function(){jQuery(document).ready(function(t){var o=scrolltotop,s=document.all;o.cssfixedsupport=!s||s&&"CSS1Compat"==document.compatMode&&window.XMLHttpRequest,o.$body=t(window.opera?"CSS1Compat"==document.compatMode?"html":"body":"html,body"),o.$control=t('<div id="topcontrol">'+o.controlHTML+"</div>").css({position:o.cssfixedsupport?"fixed":"absolute",bottom:o.controlattrs.offsety,right:o.controlattrs.offsetx,opacity:0,cursor:"pointer"}).attr({title:"Scroll to Top"}).click(function(){return o.scrollup(),!1}).appendTo("body"),document.all&&!window.XMLHttpRequest&&""!=o.$control.text()&&o.$control.css({width:o.$control.width()}),o.togglecontrol(),t('a[href="'+o.anchorkeyword+'"]').click(function(){return o.scrollup(),!1}),t(window).bind("scroll resize",function(t){o.togglecontrol()})})}};scrolltotop.init();</script>
    <noscript>Not seeing a <a href="https://www.scrolltotop.com/">Scroll to Top Button</a>? Go to our FAQ page for more info.</noscript>
        
    <!-- vide -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://www.gordonmac.com/wp-content/themes/2016/vendor/vide/jquery.vide.min.js"></script>

      
  </body>

</html>

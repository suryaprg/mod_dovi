<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GEOGRAFI, IKLIM DAN LINGKUNGAN HIDUP</title>
  <link rel="shortcut icon" href="https://malangkota.go.id/wp-content/themes/malangkotanew/images/favicon.png"> 
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">


  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>

  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts1/views1.css">
  
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="fh5co-loader"></div>
    <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
       <div style="position: absolute; left: 50px; top: 0.1px;"><img  height="47px" style="padding: 5px" src="<?php echo base_url();?>/assets/dist/img/ncc-logo-white.png" alt="NCC logo"></div>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

            <li class="dropdown user user-menu">
              <a href="../index.php">
                 <div style="position: absolute; top: 11px; right: 130px;"><img src="<?php echo base_url();?>/assets/dist/img/logo.png" width="25" height="25" alt="User Image"></div>&nbsp &nbsp &nbsp &nbsp &nbsp
               <span class="hidden-xs">Malang Open Data</span>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <br>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">KATEGORI</li>
        <li class="active">
          <a href="Posisi_Geografi_dan_Batas_Wilayah">
            <span>Posisi Geografi dan Batas Wilayah</span>
          </a>
        </li>
        <li>
          <a href="Pembagian_Wilayah_Administratif">
            <span>Pembagian Wilayah Administratif</span>
          </a>
        </li>
        <li>
          <a href="Iklim">
           <span>Iklim</span>
          </a>
        </li>
        <li>
          <a href="Sawah_dan_Kebun">
           <span>Sawah dan Kebun</span>
          </a>
        </li>
        <li>
          <a href="Sungai">
            <span>Sungai</span>
          </a>
        </li>
        <li>
          <a href="Hutan">
            <span>Hutan</span>
          </a>
        </li>
        <li>
          <a href="Taman_dan_RTH">
            <span>Taman dan RTH</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <?php
      switch ($menu) {
      #-----------------------------------------Geografi-----------------------------------------------
        case "geo": 
          switch ($page) {
            case "perumahan": include "";
              break;
            
            case "pertanahan": include "";
              break;
            
            case "lingkungan": include "";
              break;
          }
        break;

        #-----------------------------------------Ekonomi-----------------------------------------------
        case "ekonomi":
          switch ($page) {
            case "koperasi": include "";
              break;
            
            case "perindustrian": include "";
              break;
            
            case "penanaman_modal": include "";
              break;
          }
        break;

        #-----------------------------------------sosial-----------------------------------------------
        case "sosial": 
          switch ($page) {
            case "perempuan_anak": include "";
              break;
            
            case "keluarga_berencana": include "";
              break;
            
            case "sosial_sub": include "";
              break;

            case "bangsa_politik": include "";
              break;

            case "pemberdayaan_masyarakat": include "";
              break;
          }
          break;

        #-----------------------------------------kependudukan-----------------------------------------------
        case "kependudukan":
          switch ($page) {
            case "catatan_sipil": include "";
              break;
            
            case "ketenagakerjaan": include "";
              break;
          }
          break;

        #-----------------------------------------kesehatan-----------------------------------------------
        case "kesehatan": 
          switch ($page) {
            case "sehat": include "";
              break;
          }
          break;

        #-----------------------------------------pemerintahan-----------------------------------------------
        case "pemerintahan": 
          switch ($page) {
            case "otonomi_daerah": include "";
              break;
          }
          break;


        #-----------------------------------------infrastruktur-----------------------------------------------
        case "infra": 
          switch ($page) {
            case "penataan_ruang": include "";
              break;
            
            case "rencana_pembangunan": include "";
              break;
            
            case "perhubungan": include "";
              break;

            case "komunikasi": include "";
              break;
          }
          break;

        #-----------------------------------------pendidikan-----------------------------------------------
        case "pendidikan": 
          switch ($page) {
            case "pendidikan": include "";
              break;
          }
          break;
        

        default:
          include "";
          break;
      }
    ?>   
  </div>



  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2018 <a href="http://kominfo.malangkota.go.id/" target="_blank">Kominfo Kota Malang</a>
  </footer>



<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>/assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts1/view1.min.js" type="text/javascript"></script>
<script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
<script  src="<?php echo base_url();?>assets/template_aw/charts1/view1.js"></script>
<script  src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>
</body>
</html>
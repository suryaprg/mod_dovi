	<section class="content-header">
		
	</section>

	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Bencana</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 4.5%">Jenis</th>
										<th class="text-center" style="width: 1.5%">Kecamatan</th>
										<th class="text-center" style="width: 1.5%">Tahun</th>
										<th class="text-center" style="width: 0.5%">Jumlah</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpbencana/insert_lp_bencana/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">Pilih</option>
													<?php foreach ($list_bencana_jenis as $lbj) { ?>
														<option value="<?php echo $lbj->id_jenis ?>"><?php echo $lbj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td>
												<select name="id_kec" class="form-control" id="id_kec">
													<option value="">Pilih</option>
													<?php foreach ($list_main_kec as $lmk) { ?>
														<option value="<?php echo $lmk->id_kec ?>"><?php echo $lmk->nama_kec ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="text" name="th" class="form-control" id="th"></td>
											<td><input type="text" name="jml" class="form-control" id="jml"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_bencana as $lb) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lb->nama_jenis ?></td>
											<td align="left"><?php echo $lb->nama_kec ?></td>
											<td align="center"><?php echo $lb->th ?></td>
											<td align="center"><?php echo $lb->jml ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_bencana(<?php echo $lb->id_bencana ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_bencana(<?php echo $lb->id_bencana ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Jenis Bencana</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 8%">Jenis</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpbencana/insert_lp_bencana_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_bencana_jenis as $lbj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lbj->nama_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_bencana_jenis(<?php echo $lbj->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_bencana_jenis(<?php echo $lbj->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
		</section>
	</div>	

	<script type="text/javascript">
//===================================================== LP BENCANA =========================================//		
	  	function up_lp_bencana(id_bencana){
	    	clear_mod_up_lp_bencana();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_bencana', id_bencana);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/Lpbencana/index_up_lp_bencana/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_bencana(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_bencana").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_bencana(){
	      	$("#_id_jenis").val("");
	      	$("#_id_kec").val("");
	      	$("#_th").val("");
	      	$("#_jml").val("");
	  	}

  		function res_update_lp_bencana(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_bencana").attr('readonly','readonly');                                        
	        	$("#_id_bencana").val(main_data.id_bencana);
	          	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_id_kec").val(main_data.id_kec);
	          	$("#_th").val(main_data.th);
	          	$("#_jml").val(main_data.jml);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_bencana(id_bencana){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_bencana+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_bencana+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpbencana/delete_lp_bencana/";?>"+id_bencana;
	      	}else{

	      	}
	  	}	
//===================================================== LP BENCANA =========================================//		

//===================================================== LP BENCANA JENIS =========================================//
	  	function up_lp_bencana_jenis(id_jenis){
	    	clear_mod_up_lp_bencana_jenis();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_jenis', id_jenis);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/Lpbencana/index_up_lp_bencana_jenis/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_bencana_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_bencana_jenis").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_bencana_jenis(){
	      	$("#_nama_jenis2").val("");
	  	}

  		function res_update_lp_bencana_jenis(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_jenis2").attr('readonly','readonly');
	          	$("#_id_jenis2").val(main_data.id_jenis);
	          	$("#_nama_jenis").val(main_data.nama_jenis);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_bencana_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpbencana/delete_lp_bencana_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}		
//===================================================== LP BENCANA JENIS =========================================//		
	  		
	</script>

	<div class="modal fade" id="modal_up_lp_bencana">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Bencana</h4>
	      		</div>
	      		
	      		<?php echo form_open("front_lp/lpbencana/update_lp_bencana"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Bencana</label>
	                        				<input type="number" class="form-control" id="_id_bencana" name="id_bencana" placeholder="ID Bencana">
	                      				</div>                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Jenis</label>
                        					<select name="id_jenis" class="form-control" id="_id_jenis">
	                          					<option value="">-- select jenis bencana --</option>
	                          					<?php foreach ($list_bencana_jenis as $jenis) { ?>
	                            					<option value="<?php echo $jenis->id_jenis ?>"><?php echo $jenis->nama_jenis ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Kec</label>
	                        				<!-- <input type="text" class="form-control" id="_id_kec" name="id_kec" placeholder="ID Kec"> -->
											<select name="id_kec" class="form-control" id="_id_kec">
												<option value="">Pilih</option>
												<?php foreach ($list_main_kec as $lmk) { ?>
													<option value="<?php echo $lmk->id_kec ?>"><?php echo $lmk->nama_kec ?></option>
												<?php } ?>
											</select>	                        				
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Tahun</label>
	                        				<input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
	                      				</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah</label>
					                        <input type="number" class="form-control" id="_jml" name="jml" placeholder="Jumlah">
				                      	</div>

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>

	<div class="modal fade" id="modal_up_lp_bencana_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Bencana Jenis</h4>
	      		</div>
	      		
	      		<?php echo form_open("front_lp/lpbencana/update_lp_bencana_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
	                        				<input type="number" class="form-control" id="_id_jenis2" name="id_jenis" placeholder="ID Jenis">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama Jenis</label>
	                        				<input type="text" class="form-control" id="_nama_jenis" name="nama_jenis" placeholder="Nama Jenis">
	                      				</div>

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>

	<section class="content-header">
		
	</section>

	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<div class="row">
								<div class="col-md-6">
									<h3 class="box-title">Data Lampiran Aparat</h3>							
								</div>
								<div class="col-md-6">
									<!-- <a href="<?php echo base_url()."front_lp/Lpaparat/generate_pdf/" ?>">Download</a> -->
								</div>
							</div>
						</div>

						<div class="box-body">
							<!-- <table class="table ">
								<thead>
									<tr>
										<td style="width: 1%"></td>
										<td style="width: 6%"></td>
										<td style="width: 1%"></td>
										<td style="width: 1%"></td>
										<td style="width: 1%"></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpaparat/insert_lp_aparat/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">-- Pilih Jenis --</option>
													<?php foreach ($list_aparat_jenis as $laj) { ?>
														<option value="<?php echo $laj->id_jenis ?>"><?php echo $laj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="number" name="th" class="form-control" id="th"></td>
											<td><input type="number" name="jml" class="form-control" id="jml"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>									
								</tbody>								
							</table> -->							
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpaparat/insert_lp_aparat/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">-- Pilih Jenis --</option>
													<?php foreach ($list_aparat_jenis as $laj) { ?>
														<option value="<?php echo $laj->id_jenis ?>"><?php echo $laj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="number" name="th" class="form-control" id="th"></td>
											<td><input type="number" name="jml" class="form-control" id="jml"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>									
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 6%">Jenis</th>
										<th class="text-center" style="width: 1%">Tahun</th>
										<th class="text-center" style="width: 1%">Jumlah</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1; foreach ($list_aparat as $la) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $la->nama_jenis ?></td>
											<td align="center"><?php echo $la->th ?></td>
											<td align="center"><?php echo $la->jml ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_aparat(<?php echo $la->id ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_aparat(<?php echo $la->id ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Jenis Aparat</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 8%">Jenis</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpaparat/insert_lp_aparat_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_aparat_jenis as $laj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $laj->nama_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_aparat_jenis(<?php echo $laj->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_aparat_jenis(<?php echo $laj->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
			</div>

		</section>
	</div>


	<script type="text/javascript">

//======================================================== LP APARAT =============================================//
	  	function up_lp_aparat(id){
	    	clear_mod_up_lp_aparat();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id', id);    
		      $.ajax({
		        url: "<?php echo base_url()."/front_lp/Lpaparat/index_up_lp_aparat/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_aparat(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_aparat").modal('show');
	  	} 

	  	function clear_mod_up_lp_aparat(){
	      	$("#_id_jenis").val("");
	      	$("#_th").val("");
	      	$("#_jml").val("");
	  	}

	  	function res_update_lp_aparat(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id").attr('readonly','readonly');                                        
	          	$("#_id").val(main_data.id);
	          	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_th").val(main_data.th);
	          	$("#_jml").val(main_data.jml);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_aparat(id){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpaparat/delete_lp_aparat/";?>"+id;
	      	}else{

	      	}
	  	}      		
//======================================================== LP APARAT =============================================//

//======================================================== LP APARAT Jenis =============================================//
	  	function up_lp_aparat_jenis(id_jenis){
	    	clear_mod_up_lp_aparat_jenis();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_jenis', id_jenis);    
		      $.ajax({
		        url: "<?php echo base_url()."/front_lp/Lpaparat/index_up_lp_aparat_jenis/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_aparat_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_aparat_jenis").modal('show');
	  	} 

	  	function clear_mod_up_lp_aparat_jenis(){
	      	$("#_nama_jenis").val("");
	  	}

	  	function res_update_lp_aparat_jenis(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_jenis2").attr('readonly','readonly');                                        
	          	$("#_id_jenis2").val(main_data.id_jenis);
	          	$("#_nama_jenis").val(main_data.nama_jenis);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_aparat_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpaparat/delete_lp_aparat_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}
//======================================================== LP APARAT Jenis =============================================//
	</script>	

	<div class="modal fade" id="modal_up_lp_aparat">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data Lp Aparat</h4>
	      	</div>
	      	<?php echo form_open("front_lp/lpaparat/update_lp_aparat"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID</label>
	                        	<input type="number" class="form-control" id="_id" name="id" placeholder="ID">
	                      	</div>                      
	                      
	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jenis</label>
	                        	<select name="id_jenis" class="form-control" id="_id_jenis">
	                          		<option value="">-- select id jenis --</option>
	                          		<?php foreach ($list_aparat_jenis as $jenis) { ?>
                            		<option value="<?php echo $jenis->id_jenis ?>"><?php echo $jenis->nama_jenis ?></option>
	                          		<?php } ?>
                        		</select>
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Tahun</label>
	                        	<input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jumlah</label>
	                        	<input type="number" class="form-control" id="_jml" name="jml" placeholder="Jumlah">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>

	<div class="modal fade" id="modal_up_lp_aparat_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">&times;</span>
	          		</button>
	        		<h4 class="modal-title">Ubah Data Lp Aparat Jenis</h4>
	      		</div>
	      		<?php echo form_open("front_lp/lpaparat/update_lp_aparat_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
	                        				<input type="number" class="form-control" id="_id_jenis2" name="id_jenis" placeholder="ID Jenis">
	                      				</div>     

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama Jenis</label>
	                        				<input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Nama Jenis">
	                      				</div>

	                  					</div>
	                				</div>      
	            				</div>
	          				</div>
	        			</div>
	      			</div>
	      		<div class="modal-footer">
			        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
			        <button type="submit" class="btn btn-primary">Ubah Data</button>
	      		</div>
	      		</form>
	    	</div>
	  	</div>
	</div>			
</div>	
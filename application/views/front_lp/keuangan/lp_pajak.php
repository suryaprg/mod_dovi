  <section class="content-header">
    
  </section>

  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Lampiran Pajak</h3>
            </div>
            <table class="table table-responsive">
            <thead>
                  <tr>
                    <th style="width: 1%"></th>
                    <th style="width: 3%"></th>
                    <th style="width: 2%"></th>
                    <th style="width: 2%"></th>
                    <th style="width: 2%"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <form action="<?php echo base_url()."/front_lp/Lppajak/insert_lp_pajak/" ?>" method="post">
                      <td>
                        <select name="id_jenis" class="form-control" id="id_jenis">
                          <option value="">-- Pilih Jenis --</option>
                          <?php foreach ($list_pajak_jenis as $llj) { ?>
                          <option value="<?php echo $llj->id_jenis ?>"><?php echo $llj->nama_jenis ?></option>
                          <?php } ?>
                        </select>
                      </td>
                      <td><input type="number" name="th" class="form-control" id="th" placeholder="Tahun"></td>
                      <td><input type="number" name="jml" class="form-control" id="jml" placeholder="Jumlah"></td>
                      <td align="center">
                        <button type="submit" class="btn btn-success">
                          <span class="glyphicon glyphicon-plus"> Tambah</span>
                        </button>
                      </td>
                    </form>
                  </tr>
              
            </table>

            <div class="box-body">
              <table class="table table-bordered table-striped display" id="example1">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 1%">No</th>
                            <!-- <th class="text-center">Id</th> -->
                          <th class="text-center" style="width: 3%">Jenis</th>
                            <th class="text-center" style="width: 2%">Tahun</th>
                            <th class="text-center" style="width: 2%">Jumlah</th>
                            <th class="text-center" style="width: 2%">Action</th>
                  </tr>
                </thead>
                  <?php $no = 1; foreach ($list_pajak as $la) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td align="left"><?php echo $la->nama_jenis ?></td>
                      <td align="center"><?php echo $la->th ?></td>
                      <td align="center"><?php echo $la->jml ?></td>
                      <td align="center">
                        <a href="#" onclick="up_lp_pajak(<?php echo $la->id_pajak ?>)">
                          <button class="btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span>
                          </button>
                        </a>
                        <a href="#" onclick="del_lp_pajak(<?php echo $la->id_pajak ?>)">
                          <button class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-trash"></span>
                          </button>
                        </a>                        
                      </td>
                    </tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Lampiran Jenis Pajak</h3>
            </div>
            <table class="table table-responsive">
            <thead>
            <tr>
              <th style="width: 1%"></th>
              <th style="width: 8%"></th>
              <th style="width: 1%"></th>
              </tr>
              </thead>

              <tbody>
                  <tr>
                    <td></td>
                    <form action="<?php echo base_url()."/front_lp/Lppajak/insert_lp_pajak_jenis/" ?>" method="post">
                      <td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis" placeholder="Nama Jenis"></td>
                      <td align="center">
                        <button type="submit" class="btn btn-success">
                          <span class="glyphicon glyphicon-plus"></span>
                        </button>
                      </td>
                    </form>
                  </tr>
            </table>

            <div class="box-body">
              <table class="table table-bordered table-striped display" id="example2">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Jenis</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                  <?php $no = 1; foreach ($list_pajak_jenis as $lpj) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td align="left"><?php echo $lpj->nama_jenis ?></td>
                      <td align="center">
                        <a href="#" onclick="up_lp_pajak_jenis(<?php echo $lpj->id_jenis ?>)">
                          <button class="btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span>
                          </button>
                        </a>
                        <a href="#" onclick="del_lp_pajak_jenis(<?php echo $lpj->id_jenis ?>)">
                          <button class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-trash"></span>
                          </button>
                        </a>                        
                      </td>
                    </tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </section>
  </div>


  <script type="text/javascript">
    // $(document).ready(function(){
    //  $("#aparat").DataTable();
    // });

//======================================================== LP PAJAK=============================================//
   function up_lp_pajak(id_pajak){
    clear_mod_up_lp_pajak();
    // console.lo

    var data_main =  new FormData();
    data_main.append('id_pajak', id_pajak);    
      $.ajax({
        url: "<?php echo base_url()."front_lp/Lppajak/index_up_lp_pajak/";?>", // point to serv
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: data_main,                         
        type: 'post',
        success: function(res){
            console.log(res);
            res_update_lp_pajak(res);
            // $("#out_up_mhs").html(res);
        }
    });
    
    $("#modal_up_lp_pajak").modal('show');
  } 

  function clear_mod_up_lp_pajak(){
      $("#_id_jenis").val("");
      $("#_th").val("");
      $("#_jml").val("");
  }

  function res_update_lp_pajak(res){
      var data = JSON.parse(res);

      if(data.status){
          var main_data = data.val;
          console.log(main_data);
          
          $("#_id_pajak").attr('readonly','readonly');                                        
          $("#_id_pajak").val(main_data.id_pajak);
          $("#_id_jenis").val(main_data.id_jenis);
          $("#_th").val(main_data.th);
          $("#_jml").val(main_data.jml);
          
      }else{
          clear_mod_up_ik();
      }
  }    

  function del_lp_pajak(id_pajak){
      var conf = confirm("Apakah anda yakin untuk menghapus "+id_pajak+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_pajak+" akan terhapus semua.. !!!!!!! ");
      if(conf){
        window.location.href = "<?= base_url()."front_lp/Lppajak/delete_lp_pajak/";?>"+id_pajak;
      }else{

      }
  }   
//======================================================== LP PAJAK JENIS ===============================//
  function up_lp_pajak_jenis(id_jenis){
    clear_mod_up_lp_pajak_jenis();
    // console.lo

    var data_main =  new FormData();
    data_main.append('id_jenis', id_jenis);    
      $.ajax({
        url: "<?php echo base_url()."front_lp/Lppajak/index_up_lp_pajak_jenis/";?>", // point to serv
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: data_main,                         
        type: 'post',
        success: function(res){
            console.log(res);
            res_update_lp_pajak_jenis(res);
            // $("#out_up_mhs").html(res);
        }
    });
    
    $("#modal_up_lp_pajak_jenis").modal('show');
  }

  function clear_mod_up_lp_pajak_jenis(){
      $("#_nama_jenis").val("");
  }  

  function res_update_lp_pajak_jenis(res){
      var data = JSON.parse(res);

      if(data.status){
          var main_data = data.val;
          console.log(main_data);
          
          $("#_id_jenis2").attr('readonly','readonly');                                        
          $("#_id_jenis2").val(main_data.id_jenis);
          $("#_nama_jenis").val(main_data.nama_jenis);
          
      }else{
          clear_mod_up_ik();
      }
  }  

  function del_lp_pajak_jenis(id_jenis){
      var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
      if(conf){
        window.location.href = "<?= base_url()."front_lp/Lppajak/delete_lp_pajak_jenis/";?>"+id_jenis;
      }else{

      }
  }  
//======================================================== MODAL  PAJAK=============================================//
  </script> 


<div class="modal fade" id="modal_up_lp_pajak">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ubah Data Lp pajak</h4>
      </div>
      <?php echo form_open("front_lp/Lppajak/update_lp_pajak"); ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">

                      <div class="form-group">
                        <label for="exampleInputPassword1">ID</label>
                        <input type="number" class="form-control" id="_id_pajak" name="id_pajak" placeholder="ID">
                      </div>                      
                      
                      <div class="form-group">
                        <label for="exampleInputPassword1">Jenis</label>
                        <select name="id_jenis" class="form-control" id="_id_jenis">
                          <option value="">-- select id jenis --</option>
                          <?php foreach ($list_pajak_jenis as $jenis) { ?>
                            <option value="<?php echo $jenis->id_jenis ?>"><?php echo $jenis->nama_jenis ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Tahun</label>
                        <input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Jumlah</label>
                        <input type="number" class="form-control" id="_jml" name="jml" placeholder="Jumlah">
                      </div>

                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>

      </form>
    </div>
  </div>
</div>
</div>


  

<div class="modal fade" id="modal_up_lp_pajak_jenis">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ubah Data Lp pajak Jenis</h4>
      </div>
      <?php echo form_open("front_lp/Lppajak/update_lp_pajak_jenis"); ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">

                      <div class="form-group">
                        <label for="exampleInputPassword1">ID Jenis</label>
                        <input type="number" class="form-control" id="_id_jenis2" name="id_jenis" placeholder="ID Jenis">
                      </div>     

                      <div class="form-group">
                        <label for="exampleInputPassword1">Nama Jenis</label>
                        <input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Nama Jenis">
                      </div>

                  </div>

                </div>      
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>
	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Tenaga</h3>
						</div>

						<div class="box-body">							
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 4%">Jenis</th>
										<th class="text-center" style="width: 2%">Kategori</th>
										<th class="text-center" style="width: 1%">Tahun</th>
										<th class="text-center" style="width: 1%">Jumlah</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/admin_opd/geo/Lptenaga/insert_lp_tenaga/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">-- Pilih Jenis --</option>
													<?php foreach ($list_tenaga_jenis as $lkj) { ?>
														<option value="<?php echo $lkj->id_jenis ?>"><?php echo $lkj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td>
												<select name="id_kategori" class="form-control" id="id_kategori">
													<option value="">-- Pilih Kategori --</option>
													<?php foreach ($list_tenaga_kategori as $ltk) { ?>
														<option value="<?php echo $ltk->id_kategori ?>"><?php echo $ltk->nama_kategori ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="number" name="th" class="form-control" id="th" placeholder="Tahun"></td>
											<td><input type="number" name="jml" class="form-control" id="jml" placeholder="Jumlah"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_tenaga as $lt) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lt->nama_jenis ?></td>
											<td align="left"><?php echo $lt->nama_kategori ?></td>
											<td align="center"><?php echo $lt->th ?></td>
											<td align="center"><?php echo $lt->jml ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_tenaga(<?php echo $lt->id_tenaga ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_tenaga(<?php echo $lt->id_tenaga ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-7">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Jenis Tenaga</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 8%">Nama</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/admin_opd/geo/Lptenaga/insert_lp_tenaga_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis" placeholder="Nama Jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_tenaga_jenis as $ltj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $ltj->nama_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_tenaga_jenis(<?php echo $ltj->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_tenaga_jenis(<?php echo $ltj->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
				<div class="col-md-5">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kategori Tenaga</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped " id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 8%">Nama</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/admin_opd/geo/Lptenaga/insert_lp_tenaga_kategori/" ?>" method="post">
											<td><input type="text" name="nama_kategori" class="form-control" id="nama_kategori" placeholder="Nama Kategori"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_tenaga_kategori as $ltj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $ltj->nama_kategori ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_tenaga_kategori(<?php echo $ltj->id_kategori ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_tenaga_kategori(<?php echo $ltj->id_kategori ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

		</section>
	</div>

	<script type="text/javascript">
//======================================================== LP TENAGA =============================================//
	  	function up_lp_tenaga(id_tenaga){
	    	clear_mod_up_lp_tenaga();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_tenaga', id_tenaga);    
		      $.ajax({
		        url: "<?php echo base_url()."front_lp/lptenaga/index_up_lp_tenaga/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_tenaga(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_tenaga").modal('show');
	  	} 

	  	function clear_mod_up_lp_tenaga(){
	      	$("#_id_jenis").val("");
	      	$("#_id_kategori").val("");
	      	$("#_th").val("");
	      	$("#_jml").val("");
	  	}

	  	function res_update_lp_tenaga(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_tenaga").attr('readonly','readonly');                                        
	          	$("#_id_tenaga").val(main_data.id_tenaga);
	          	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_id_kategori").val(main_data.id_kategori);
	          	$("#_th").val(main_data.th);
	          	$("#_jml").val(main_data.jml);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_tenaga(id_tenaga){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_tenaga+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_tenaga+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lptenaga/delete_lp_tenaga/";?>"+id_tenaga;
	      	}else{

	      	}
	  	}      		
//======================================================== LP TENAGA =============================================//

//======================================================== LP TENAGA JENIS =======================================//
	  	function up_lp_tenaga_jenis(id_jenis){
	    	clear_mod_up_lp_tenaga_jenis();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_jenis', id_jenis);    
		      $.ajax({
		        url: "<?php echo base_url()."front_lp/lptenaga/index_up_lp_tenaga_jenis/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_tenaga_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_tenaga_jenis").modal('show');
	  	} 

	  	function clear_mod_up_lp_tenaga_jenis(){
	      	$("#_nama_jenis").val("");
	  	}

	  	function res_update_lp_tenaga_jenis(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_jenis2").attr('readonly','readonly');                                        
	          	$("#_id_jenis2").val(main_data.id_jenis);
	          	$("#_nama_jenis").val(main_data.nama_jenis);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_tenaga_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lptenaga/delete_lp_tenaga_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}      		
//======================================================== LP TENAGA JENIS =========================================//

//======================================================== LP TENAGA JENIS =======================================//
	  	function up_lp_tenaga_kategori(id_kategori){
	    	clear_mod_up_lp_tenaga_kategori();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_kategori', id_kategori);    
		      $.ajax({
		        url: "<?php echo base_url()."front_lp/lptenaga/index_up_lp_tenaga_kategori/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_tenaga_kategori(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_tenaga_kategori").modal('show');
	  	} 

	  	function clear_mod_up_lp_tenaga_kategori(){
	      	$("#_nama_kategori").val("");
	  	}

	  	function res_update_lp_tenaga_kategori(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_kategori2").attr('readonly','readonly');                                        
	          	$("#_id_kategori2").val(main_data.id_kategori);
	          	$("#_nama_kategori").val(main_data.nama_kategori);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_tenaga_kategori(id_kategori){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_kategori+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kategori+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lptenaga/delete_lp_tenaga_kategori/";?>"+id_kategori;
	      	}else{

	      	}
	  	}      		
//======================================================== LP TENAGA JENIS =========================================//		
	</script>

	<div class="modal fade" id="modal_up_lp_tenaga">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data LP Tenaga</h4>
	      	</div>
	      	<?php echo form_open("front_lp/lptenaga/update_lp_tenaga"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID Tenaga</label>
	                        	<input type="number" name="id_tenaga" id="_id_tenaga" class="form-control">
	                      	</div>                      
	                      
	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jenis</label>
	                        	<select name="id_jenis" class="form-control" id="_id_jenis">
	                          		<option value="">-- Select Jenis --</option>
	                          		<?php foreach ($list_tenaga_jenis as $jenis) { ?>
                            		<option value="<?php echo $jenis->id_jenis ?>"><?php echo $jenis->nama_jenis ?></option>
	                          		<?php } ?>
                        		</select>
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Sub Jenis</label>
								<select name="id_kategori" class="form-control" id="_id_kategori">
									<option value="">-- Pilih Kategori --</option>
									<?php foreach ($list_tenaga_kategori as $ltk) { ?>
										<option value="<?php echo $ltk->id_kategori ?>"><?php echo $ltk->nama_kategori ?></option>
									<?php } ?>
								</select>	                      	
	                      	</div>	                      	


	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Tahun</label>
	                        	<input type="number" name="th" class="form-control" id="_th" placeholder="Tahun">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jumlah</label>
	                        	<input type="number" name="jml" class="form-control" id="_jml" placeholder="Jumlah">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>

	<div class="modal fade" id="modal_up_lp_tenaga_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data LP Jenis Tenaga</h4>
	      	</div>
	      	<?php echo form_open("front_lp/lptenaga/update_lp_tenaga_jenis"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID Jenis</label>
	                        	<input type="number" name="id_jenis" id="_id_jenis2" class="form-control">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Nama</label>
	                        	<input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Nama">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>

	<div class="modal fade" id="modal_up_lp_tenaga_kategori">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data LP Kategori Tenaga</h4>
	      	</div>
	      	<?php echo form_open("front_lp/lptenaga/update_lp_tenaga_kategori"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID Kategori</label>
	                        	<input type="number" name="id_kategori" id="_id_kategori2" class="form-control">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Nama</label>
	                        	<input type="text" name="nama_kategori" class="form-control" id="_nama_kategori" placeholder="Nama">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>	
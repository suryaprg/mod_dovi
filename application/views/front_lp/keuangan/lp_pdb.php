	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Produk Domestik Bruto</h3>
						</div>

						<table class="table table-responsive" >
							<thead>
								<tr>
									<th class="text-center" style="width: 1%"></th>
									<th class="text-center" style="width: 3%"></th>
									<th class="text-center" style="width: 3%"></th>
									<th class="text-center" style="width: 1%"></th>
									<th class="text-center" style="width: 1%"></th>
									<th class="text-center" style="width: 1%"></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<form action="<?php echo base_url()."/admin_opd/geo/Lppdb/insert_lp_pdb/" ?>" method="post">
										<td>
											<select name="id_jenis" class="form-control">
												<option value="">Jenis</option>
												<?php foreach ($list_pdb_jenis as $lpj) { ?>
													<option value="<?php echo $lpj->id_jenis ?>"><?php echo $lpj->nama_jenis ?></option>
												<?php } ?>
											</select>
										</td>
										<td>
											<select name="id_kategori" class="form-control">
												<option value="">Kecamatan</option>
												<?php foreach ($list_pdb_kategori as $lpk) { ?>
													<option value="<?php echo $lpk->id_kategori ?>"><?php echo $lpk->nama_kategori ?></option>
												<?php } ?>
											</select>
										</td>
										<td><input type="text" name="th" class="form-control" placeholder="tahun"></td>
										<td><input type="text" name="jml" class="form-control" placeholder="jumlah"></td>
										<td align="center">
											<button type="submit" class="btn btn-success">
												<span class="glyphicon glyphicon-plus"></span>
											</button>
										</td>
									</form>
								</tr>								
							</tbody>
						</table>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 3%">Jenis</th>
										<th class="text-center" style="width: 3%">Kategori</th>
										<th class="text-center" style="width: 1%">Tahun</th>
										<th class="text-center" style="width: 1%">Jumlah</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1; foreach ($list_pdb as $lp) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lp->nama_jenis ?></td>
											<td align="left"><?php echo $lp->nama_kategori ?></td>
											<td align="center"><?php echo $lp->th ?></td>
											<td align="center"><?php echo $lp->jml ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_pdb(<?php echo $lp->id_pdb ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_pdb(<?php echo $lp->id_pdb ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-6">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Jenis PDB</h3>
						</div>

						<table class="table">
							<thead>
								<tr>
									<th style="width: 1%"></th>
									<th style="width: 6%"></th>
									<th style="width: 2%"></th>
									<th style="width: 1%"></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<form action="<?php echo base_url()."/admin_opd/geo/Lppdb/insert_lp_pdb_jenis/" ?>" method="post">
										<td><input type="text" name="nama_jenis" class="form-control" placeholder="nama"></td>
										<td><input type="text" name="satuan" class="form-control" placeholder="satuan"></td>
										<td align="center">
											<button type="submit" class="btn btn-success">
												<span class="glyphicon glyphicon-plus"></span>
											</button>
										</td>
									</form>
								</tr>								
							</tbody>
						</table>

						<div class="box-body">
							<table class="table table-bordered table-striped" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 6%">Nama</th>
										<th class="text-center" style="width: 2%">Satuan</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1; foreach ($list_pdb_jenis as $lpj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lpj->nama_jenis ?></td>
											<td align="left"><?php echo $lpj->satuan ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_pdb_jenis(<?php echo $lpj->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_pdb_jenis(<?php echo $lpj->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Kategori PDB</h3>
						</div>

						<table class="table">
							<thead>
								<tr>
									<th style="width: 1%"></th>
									<th style="width: 8%"></th>
									<th style="width: 1%"></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<form action="<?php echo base_url()."/admin_opd/geo/Lppdb/insert_lp_pdb_kategori/" ?>" method="post">
										<td><input type="text" name="nama_kategori" class="form-control" placeholder="nama"></td>
										<td align="center">
											<button type="submit" class="btn btn-success">
												<span class="glyphicon glyphicon-plus"></span>
											</button>
										</td>
									</form>
								</tr>								
							</tbody>
						</table>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 8%">Nama</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1; foreach ($list_pdb_kategori as $lpk) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lpk->nama_kategori ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_pdb_kategori(<?php echo $lpk->id_kategori ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_pdb_kategori(<?php echo $lpk->id_kategori ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>				

			</div>
		</section>
	</div>

	<script type="text/javascript">
//===================================================== LP PDB =========================================//		
	  	function up_lp_pdb(id_pdb){
	    	clear_mod_up_lp_pdb();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_pdb', id_pdb);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/admin_opd/geo/Lppdb/index_up_lp_pdb/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_pdb(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_pdb").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_pdb(){
	      	$("#_id_jenis").val("");
	      	$("#_id_kategori").val("");
	      	$("#_th").val("");
	      	$("#_jml").val("");
	  	}

  		function res_update_lp_pdb(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_pdb").attr('readonly','readonly');                                        
	        	$("#_id_pdb").val(main_data.id_pdb);
	          	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_id_kategori").val(main_data.id_kategori);
	          	$("#_th").val(main_data.th);
	          	$("#_jml").val(main_data.jml);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_pdb(id_pdb){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_pdb+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_pdb+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."admin_opd/geo/lppdb/delete_pdb/";?>"+id_pdb;
	      	}else{

	      	}
	  	}	
//===================================================== LP PDB =========================================//

//===================================================== LP PDB JENIS =========================================//		
	  	function up_lp_pdb_jenis(id_jenis){
	    	clear_mod_up_lp_pdb_jenis();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_jenis', id_jenis);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/admin_opd/geo/Lppdb/index_up_lp_pdb_jenis/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_pdb_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_pdb_jenis").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_pdb_jenis(){
	      	$("#_nama_jenis").val("");
	      	$("#_satuan").val("");
	  	}

  		function res_update_lp_pdb_jenis(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_jenis2").attr('readonly','readonly');                                        
	        	$("#_id_jenis2").val(main_data.id_jenis);
	          	$("#_nama_jenis").val(main_data.nama_jenis);
	          	$("#_satuan").val(main_data.satuan);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_pdb_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."admin_opd/geo/lppdb/delete_pdb_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}	
//===================================================== LP PDB JENIS =========================================//

//===================================================== LP PDB JENIS =========================================//		
	  	function up_lp_pdb_kategori(id_kategori){
	    	clear_mod_up_lp_pdb_kategori();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_kategori', id_kategori);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/admin_opd/geo/Lppdb/index_up_lp_pdb_kategori/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_pdb_kategori(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_pdb_kategori").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_pdb_kategori(){
	      	$("#_nama_kategori").val("");
	  	}

  		function res_update_lp_pdb_kategori(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_kategori2").attr('readonly','readonly');                                        
	        	$("#_id_kategori2").val(main_data.id_kategori);
	          	$("#_nama_kategori").val(main_data.nama_kategori);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_pdb_kategori(id_kategori){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."admin_opd/geo/lppdb/delete_pdb_kategori/";?>"+id_jenis;
	      	}else{

	      	}
	  	}	
//===================================================== LP PDB JENIS =========================================//		
	</script>

	<div class="modal fade" id="modal_up_lp_pdb">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data LP PDB</h4>
	      		</div>
	      		
	      		<?php echo form_open("admin_opd/geo/lppdb/update_lp_pdb"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID PDB</label>
	                        				<input type="number" class="form-control" id="_id_pdb" name="id_pdb">
	                      				</div>                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Jenis</label>
                        					<select name="id_jenis" class="form-control" id="_id_jenis">
	                          					<option value="">-- select jenis pdb --</option>
	                          					<?php foreach ($list_pdb_jenis as $jenis) { ?>
	                            					<option value="<?php echo $jenis->id_jenis ?>"><?php echo $jenis->nama_jenis ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Kategori</label>
											<select name="id_kategori" class="form-control" id="_id_kategori">
												<option value="">Pilih</option>
												<?php foreach ($list_pdb_kategori as $ktg) { ?>
													<option value="<?php echo $ktg->id_kategori ?>"><?php echo $ktg->nama_kategori ?></option>
												<?php } ?>
											</select>	                        				
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Tahun</label>
	                        				<input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
	                      				</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah</label>
					                        <input type="number" class="form-control" id="_jml" name="jml" placeholder="Jumlah">
				                      	</div>

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>

	<div class="modal fade" id="modal_up_lp_pdb_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data LP PDB Jenis</h4>
	      		</div>
	      		
	      		<?php echo form_open("admin_opd/geo/lppdb/update_lp_pdb_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
	                        				<input type="number" class="form-control" id="_id_jenis2" name="id_jenis">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama</label>
	                        				<input type="text" class="form-control" id="_nama_jenis" name="nama_jenis" placeholder="Nama">
	                      				</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Satuan</label>
					                        <input type="text" class="form-control" id="_satuan" name="satuan" placeholder="Satuan">
				                      	</div>

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>

	<div class="modal fade" id="modal_up_lp_pdb_kategori">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data LP PDB Kategori</h4>
	      		</div>
	      		
	      		<?php echo form_open("admin_opd/geo/lppdb/update_lp_pdb_kategori"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Kategori</label>
	                        				<input type="number" class="form-control" id="_id_kategori2" name="id_kategori">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama</label>
	                        				<input type="text" class="form-control" id="_nama_kategori" name="nama_kategori" placeholder="Nama">
	                      				</div>

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>	
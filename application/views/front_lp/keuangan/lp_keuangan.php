	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Keuangan</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 3%">Jenis</th>
										<th class="text-center" style="width: 3%">Sub Jenis</th>
										<th class="text-center" style="width: 1%">Tahun</th>
										<th class="text-center" style="width: 1%">Jumlah</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkeu/insert_lp_keu/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">Pilih</option>
													<?php foreach ($list_keu_jenis as $lkj) { ?>
														<option value="<?php echo $lkj->id_jenis ?>"><?php echo $lkj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td>
												<select name="id_sub_jenis" class="form-control" id="id_sub_jenis">
													<option value="">Pilih</option>
													<?php foreach ($list_keu_sub_jenis as $lksj) { ?>
														<option value="<?php echo $lksj->id_sub_jenis ?>"><?php echo $lksj->nama_sub_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="text" name="th" class="form-control" id="th" placeholder="Tahun"></td>
											<td><input type="text" name="jml" class="form-control" id="jml" placeholder="Jumlah"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_keu as $lk) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lk->nama_jenis ?></td>
											<td align="left"><?php echo $lk->nama_sub_jenis ?></td>
											<td align="center"><?php echo $lk->th ?></td>
											<td align="center"><?php echo $lk->jml ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_keu(<?php echo $lk->id_keu ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_keu(<?php echo $lk->id_keu ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-5">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Jenis Keuangan</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped " id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 7%">Nama</th>
										<th class="text-center" style="width: 1%">Kategori</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkeu/insert_lp_keu_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis" placeholder="Nama Jenis"></td>
											<td><input type="text" name="kategori" class="form-control" id="kategori" placeholder="Masukan Kategori"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_keu_jenis as $lkj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lkj->nama_jenis ?></td>
											<td align="center"><?php echo $lkj->kategori ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_keu_jenis(<?php echo $lkj->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_keu_jenis(<?php echo $lkj->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="col-md-7">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Sub Jenis Keuangan</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 0.5%">No</th>
										<th class="text-center" style="width: 3.5%">Jenis</th>
										<th class="text-center" style="width: 5%">Nama</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkeu/insert_lp_keu_sub_jenis/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">Pilih</option>
													<?php foreach ($list_keu_jenis as $lkj) { ?>
														<option value="<?php echo $lkj->id_jenis ?>"><?php echo $lkj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>											
											<td><input type="text" name="nama_sub_jenis" class="form-control" id="nama_sub_jenis" placeholder="Masukan Nama Sub Jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_keu_sub_jenis as $lksj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lksj->nama_jenis ?></td>
											<td align="left"><?php echo $lksj->nama_sub_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_keu_sub_jenis(<?php echo $lksj->id_sub_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_keu_sub_jenis(<?php echo $lksj->id_sub_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>				

			</div>
		</section>
	</div>

	<script type="text/javascript">
//===================================================== LP KEU =========================================//		
	  	function up_lp_keu(id_keu){
	    	clear_mod_up_lp_keu();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_keu', id_keu);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/Lpkeu/index_up_lp_keu/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_keu(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_keu").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_keu(){
	      	$("#_id_jenis").val("");
	      	$("#_id_sub_jenis").val("");
	      	$("#_th").val("");
	      	$("#_jml").val("");
	  	}

  		function res_update_lp_keu(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_keu").attr('readonly','readonly');                                        
	        	$("#_id_keu").val(main_data.id_keu);
	          	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_id_sub_jenis").val(main_data.id_sub_jenis);
	          	$("#_th").val(main_data.th);
	          	$("#_jml").val(main_data.jml);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_keu(id_keu){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_keu+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_keu+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkeu/delete_lp_keu/";?>"+id_keu;
	      	}else{

	      	}
	  	}	
//===================================================== LP KEU =========================================//

//===================================================== LP KEU JENIS =========================================//		
	  	function up_lp_keu_jenis(id_jenis){
	    	clear_mod_up_lp_keu_jenis();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_jenis', id_jenis);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/Lpkeu/index_up_lp_keu_jenis/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_keu_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_keu_jenis").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_keu_jenis(){
	      	$("#_nama_jenis").val("");
	      	$("#_kategori").val("");
	  	}

  		function res_update_lp_keu_jenis(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_jenis2").attr('readonly','readonly');                                        
	        	$("#_id_jenis2").val(main_data.id_jenis);
	          	$("#_nama_jenis").val(main_data.nama_jenis);
	          	$("#_kategori").val(main_data.kategori);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_keu_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkeu/delete_lp_keu_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}	
//===================================================== LP KEU JENIS =========================================//

//===================================================== LP KEU SUB JENIS =========================================//		
	  	function up_lp_keu_sub_jenis(id_sub_jenis){
	    	clear_mod_up_lp_keu_sub_jenis();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_sub_jenis', id_sub_jenis);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/Lpkeu/index_up_lp_keu_sub_jenis/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_keu_sub_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_keu_sub_jenis").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_keu_sub_jenis(){
	      	$("#_id_jenis3").val("");
	      	$("#_nama_sub_jenis").val("");
	  	}

  		function res_update_lp_keu_sub_jenis(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_sub_jenis2").attr('readonly','readonly');                                        
	        	$("#_id_sub_jenis2").val(main_data.id_sub_jenis);
	          	$("#_id_jenis3").val(main_data.id_jenis);
	          	$("#_nama_sub_jenis").val(main_data.nama_sub_jenis);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_keu_sub_jenis(id_sub_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_sub_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_sub_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkeu/delete_lp_keu_sub_jenis/";?>"+id_sub_jenis;
	      	}else{

	      	}
	  	}	
//===================================================== LP KEU SUB JENIS =========================================//	
	</script>

	<div class="modal fade" id="modal_up_lp_keu">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Keuangan</h4>
	      		</div>
	      		
	      		<?php echo form_open("front_lp/lpkeu/update_lp_keu"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Keuangan</label>
	                        				<input type="number" class="form-control" id="_id_keu" name="id_keu" placeholder="ID Keuangan">
	                      				</div>                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Jenis</label>
                        					<select name="id_jenis" class="form-control" id="_id_jenis">
	                          					<option value="">-- Select Jenis --</option>
	                          					<?php foreach ($list_keu_jenis as $jenis) { ?>
	                            					<option value="<?php echo $jenis->id_jenis ?>"><?php echo $jenis->nama_jenis ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Sub Jenis</label>
											<select name="id_sub_jenis" class="form-control" id="_id_sub_jenis">
												<option value="">-- Select Sub Jenis --</option>
												<?php foreach ($list_keu_sub_jenis as $sub) { ?>
													<option value="<?php echo $sub->id_sub_jenis ?>"><?php echo $sub->nama_sub_jenis ?></option>
												<?php } ?>
											</select>	                        				
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Tahun</label>
	                        				<input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
	                      				</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah</label>
					                        <input type="text" class="form-control" id="_jml" name="jml" placeholder="Jumlah">
				                      	</div>

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>

	<div class="modal fade" id="modal_up_lp_keu_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Jenis Keuangan</h4>
	      		</div>
	      		
	      		<?php echo form_open("front_lp/lpkeu/update_lp_keu_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
	                        				<input type="number" class="form-control" id="_id_jenis2" name="id_jenis" placeholder="ID Jenis">
	                      				</div>                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama</label>
	                        				<input type="tex" name="nama_jenis" id="_nama_jenis" class="form-control">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Kategori</label>
	                        				<input type="text" class="form-control" id="_kategori" name="kategori" placeholder="Kategori">
	                      				</div>

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>

	<div class="modal fade" id="modal_up_lp_keu_sub_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Sub Jenis Keuangan</h4>
	      		</div>
	      		
	      		<?php echo form_open("front_lp/lpkeu/update_lp_keu_sub_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Sub Jenis</label>
	                        				<input type="number" class="form-control" id="_id_sub_jenis2" name="id_sub_jenis" placeholder="ID Sub Jenis">
	                      				</div>                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Jenis</label>
											<select name="id_jenis" class="form-control" id="_id_jenis3">
												<option value="">-- Pilih Jenis --</option>
												<?php foreach ($list_keu_jenis as $lkj) { ?>
													<option value="<?php echo $lkj->id_jenis ?>"><?php echo $lkj->nama_jenis ?></option>
												<?php } ?>
											</select>
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama</label>
	                        				<input type="text" class="form-control" id="_nama_sub_jenis" name="nama_sub_jenis" placeholder="Sub Jenis">
	                      				</div>

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>	
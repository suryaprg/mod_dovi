<section class="content-header">
        </section>

  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <section class="content">
      <div class="row">

        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Kunjungan Hotel Jenis </h3>
            </div>
            <table class="table table-responsive">
            <thead>
                  <tr>
                    <th style="width: 1%"></th>
                            <!-- <th class="text-center">Id</th> -->
                          <th style="width: 2%"></th>
                            <th style="width: 2%"></th>
                            <th style="width: 3.5%"></th>
                            <th style="width: 1%"></th>
                            <th style="width: 0.5%"></th>
                  </tr>               
              </thead>
               <tbody>
                  <tr>
                    <td></td>
                    <form action="<?php echo base_url()."/admin_opd/budpar/Disbudparkunjunganhoteljenis/insert_disbudpar_kunjungan_hotel_jenis/" ?>" method="post">
                    <td>
                      <select name="id_kategori" class="form-control" id="id_kategori">
                          <option value="">-- Pilih kategori --</option>
                          <?php foreach ($list_disbudpar_kunjungan_hotel_kate as $lpj) { ?>
                          <option value="<?php echo $lpj->id_kategori ?>"><?php echo $lpj->nama_kategori?></option>
                          <?php } ?>
                        </select>
                        </td>
                        <td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis" placeholder="Nama Jenis"></td>
                      <td><input type="text" name="alamat" class="form-control" id="alamat" placeholder="Tahun"></td>
                      <td><input type="text" name="jml_kmr" class="form-control" id="jml_kmr" placeholder="Jumlah Kamar"></td>
                      <td align="center">
                      <button type="submit" class="btn btn-success">
                      <span class="glyphicon glyphicon-plus"></span>
                        </button>
                      </td>
                    </form>
                  </tr>
              
            </table>

        <div class="box-body">
          <table class="table table-bordered table-striped display" id="example1">
            <thead>
              <th class="text-center">No</th>
              <th class="text-center">Kategori</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">Alamat</th>
              <th class="text-center">Jml Kamar</th>
              <th class="text-center">Aksi</th>
            </thead>
              <?php 
              $no = 1;  
              foreach ($list_disbudpar_kunjungan_hotel_jenis as $aj) { ?>
                <tr>
                  <td align="center"><?php echo $no ?></td>
                  <td align="center"><?php echo $aj->nama_kategori ?></td>
                  <td align="center"><?php echo $aj->nama_jenis ?></td>
                  <td align="center"><?php echo $aj->alamat ?></td>
                  <td align="center"><?php echo $aj->jml_kmr ?></td>
                  <td align="center">
                    <a href="#" onclick="up_disbudpar_kunjungan_hotel_jenis(<?php echo $aj->id_jenis ?>)">
                      <button class="btn btn-primary">
                        <span class="glyphicon glyphicon-pencil"></span>
                      </button>
                    </a>
                    <a href="#" onclick="del_disbudpar_kunjungan_hotel_jenis(<?php echo $aj->id_jenis ?>)">
                      <button class="btn btn-danger">
                        <span class="glyphicon glyphicon-trash"></span>
                      </button>                 
                    </a>                    
                  </td>
                </tr>  
              <?php $no++; } ?>
            </tbody>
          </table>
        </div>
      </div>


      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Lampiran Jenis  Kunjungan Hotel </h3>
            </div>
            <table class="table table-responsive" id="">
            <thead>
                  <tr>
                    <th style="width: 2%"></th>
                            <!-- <th class="text-center">Id</th> -->
                    <th style="width: 4%"></th>
                    <th style="width: 1%"></th>
                    <th style="width: 1%"></th>
                    <th style="width: 1%"></th>
                    <th style="width: 1%"></th>
                  </tr>               
              </thead>
              <tbody>
                  <tr>
                    <td></td>
                    <form action="<?php echo base_url()."/admin_opd/budpar/Disbudparkunjunganhoteljenis/insert_disbudpar_kunjungan_hotel_main/" ?>" method="post">
                     <td>
                        <select name="id_jenis" class="form-control" id="id_jenis">
                          <option value="">-- Pilih Jenis --</option>
                          <?php foreach ($list_disbudpar_kunjungan_hotel_jenis as $lpj) { ?>
                          <option value="<?php echo $lpj->id_jenis ?>"><?php echo $lpj->nama_jenis?></option>
                          <?php } ?>
                        </select>
                      </td>
                      
                      <td><input type="text" name="th" class="form-control" id="th" placeholder="Tahun"></td>
                      <td><input type="text" name="wisman" class="form-control" id="wisman" placeholder="Wisata Mancanegara"></td>
                      <td><input type="text" name="wisnus" class="form-control" id="wisnus" placeholder="Wisata Nusantaras"></td>
                      <td align="center">
                      <button type="submit" class="btn btn-success">
                      <span class="glyphicon glyphicon-plus"> Tambah</span>
                        </button>
                      </td>
                    </form>
                  </tr>
              
            </table>
            <div class="box-body">
          <table class="table table-bordered table-striped display" id="example2">
            <thead>
              <th class="text-center">No</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">Tahun</th>
              <th class="text-center">Wisman</th>
              <th class="text-center">Wisnus</th>
              <th class="text-center">Aksi</th>
            </thead>
              <?php 
              $no = 1;  
              foreach ($list_disbudpar_kunjungan_hotel_main as $aj) { ?>
                <tr>
                  <td align="center"><?php echo $no ?></td>
                  <td align="center"><?php echo $aj->nama_jenis ?></td>
                  <td align="center"><?php echo $aj->th ?></td>
                  <td align="center"><?php echo $aj->wisman ?></td>
                  <td align="center"><?php echo $aj->wisnus ?></td>
                  <td align="center">
                    <a href="#" onclick="up_disbudpar_kunjungan_hotel_main(<?php echo $aj->id_main ?>)">
                      <button class="btn btn-primary">
                        <span class="glyphicon glyphicon-pencil"></span>
                      </button>
                    </a>
                    <a href="#" onclick="del_disbudpar_kunjungan_hotel_main(<?php echo $aj->id_main ?>)">
                      <button class="btn btn-danger">
                        <span class="glyphicon glyphicon-trash"></span>
                      </button>                 
                    </a>                    
                  </td>
                </tr>  
              <?php $no++; } ?>
            </tbody>
          </table>
        </div>
      </div>
  
            <div class="row">
        <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Lampiran Kategori Kunjungan Hotel </h3>
            </div>
            <div class="box-body">
          <table class="table table-bordered table-striped " id="lahan_jenis">
            <thead>
              <th class="text-center">No</th>
              <th class="text-center">Nama Kategori</th>
              <th class="text-center">Aksi</th>
            </thead>
            <tbody>
                  <tr>
                    <td></td>
                    <form action="<?php echo base_url()."/admin_opd/budpar/Disbudparkunjunganhoteljenis/insert_disbudpar_kunjungan_hotel_kate/" ?>" method="post">
                    <td><input type="text" name="nama_kategori" class="form-control" id="nama_kategori" placeholder="Nama Kategori"></td>
                      <td align="center">
                      <button type="submit" class="btn btn-success">
                      <span class="glyphicon glyphicon-plus"> Tambah</span>
                        </button>
                      </td>
                    </form>
                  </tr>
              <?php 
              $no = 1;  
              foreach ($list_disbudpar_kunjungan_hotel_kate as $aj) { ?>
                <tr>
                  <td align="center"><?php echo $no ?></td>
                  <td align="center"><?php echo $aj->nama_kategori ?></td>
                  <td align="center">
                    <a href="#" onclick="up_disbudpar_kunjungan_hotel_kate(<?php echo $aj->id_kategori ?>)">
                      <button class="btn btn-primary">
                        <span class="glyphicon glyphicon-pencil"></span>
                      </button>
                    </a>
                    <a href="#" onclick="del_disbudpar_kunjungan_hotel_kate(<?php echo $aj->id_kategori ?>)">
                      <button class="btn btn-danger">
                        <span class="glyphicon glyphicon-trash"></span>
                      </button>                 
                    </a>                    
                  </td>
                </tr>  
              <?php $no++; } ?>
            </tbody>
          </table>
        </div>
      </div>
  </div>

<div class="modal fade" id="modal_up_disbudpar_kunjungan_hotel_jenis">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ubah Data Disbudpar Kunjungan Hotel Jenis</h4>
      </div>
      <?php echo form_open("admin_opd/budpar/Disbudparkunjunganhoteljenis/update_disbudpar_kunjungan_hotel_jenis"); ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">

                  <div class="form-group">
                    <label for="exampleInputPassword1">Kategori</label>
                    <select name="id_kategori" class="form-control" id="_id_kategori">
                      <option value="">-- select id kategori --</option>
                      <?php foreach ($list_disbudpar_kunjungan_hotel_kate as $kategori) { ?>
                        <option value="<?php echo $kategori->id_kategori ?>"><?php echo $kategori->nama_kategori ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Nama Jenis</label>
                    <input type="text" class="form-control" id="_nama_jenis" name="nama_jenis" placeholder="Nama Jenis">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Alamat</label>
                    <input type="text" class="form-control" id="_alamat" name="alamat" placeholder="Alamat">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Jumlah Kamar</label>
                    <input type="number" class="form-control" id="_jml_kmr" name="jml_kmr" placeholder="Jumlah Kamar">
                  </div>

                </div>              
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_up_disbudpar_kunjungan_hotel_kate">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ubah Data Disbudpar Kunjungan Hotel Kate</h4>
      </div>
      <?php echo form_open("admin_opd/budpar/Disbudparkunjunganhoteljenis/update_disbudpar_kunjungan_hotel_kate"); ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">

                      <div class="form-group">
                        <label for="exampleInputPassword1">ID Kategori</label>
                        <input type="number" class="form-control" id="_id_kategori" name="id_kategori" placeholder="ID Kategori">
                      </div>     

                      <div class="form-group">
                        <label for="exampleInputPassword1">Nama Kategori</label>
                        <input type="text" name="nama_kategori" class="form-control" id="_nama_kategori" placeholder="Nama Kategori">
                      </div>

                  </div>

                </div>      
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_up_disbudpar_kunjungan_hotel_main">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ubah Data Disbudpar Kunjungan Hotel Main</h4>
      </div>
      <?php echo form_open("admin_opd/budpar/Disbudparkunjunganhoteljenis/update_disbudpar_kunjungan_hotel_main"); ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">

                    <div class="form-group">
                    <label for="exampleInputPassword1">ID main</label>
                    <input type="number" class="form-control" id="_id_main" name="id_main" placeholder="ID">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">ID jenis</label>
                    <select name="id_jenis" class="form-control" id="_id_jenis">
                      <option value="">-- select id jenis --</option>
                      <?php foreach ($list_disbudpar_kunjungan_hotel_jenis as $key) { ?>
                        <option value="<?php echo $key->id_jenis ?>"><?php echo $key->nama_jenis ?></option>
                      <?php } ?>
                    </select> 
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Th</label>
                    <input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Wisman</label>
                    <input type="number" class="form-control" id="_wisman" name="wisman" placeholder="Wisman">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Wisnus</label>
                    <input type="number" class="form-control" id="_wisnus" name="wisnus" placeholder="Wisnus">
                  </div>

                </div>              
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>
      </form>
    </div>
  </div>
</div>


  <script type="text/javascript">


function up_disbudpar_kunjungan_hotel_jenis(id_jenis){
    clear_mod_up_disbudpar_kunjungan_hotel_jenis();
    // console.lo

    var data_main =  new FormData();
    data_main.append('id_jenis', id_jenis);    
      $.ajax({
        url: "<?php echo base_url()."admin_opd/budpar/Disbudparkunjunganhoteljenis/index_up_disbudpar_kunjungan_hotel_jenis/";?>", // point to serv
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: data_main,                         
        type: 'post',
        success: function(res){
            console.log(res);
            res_update_disbudpar_kunjungan_hotel_jenis(res);
            // $("#out_up_mhs").html(res);
        }
    });
    
    $("#modal_up_disbudpar_kunjungan_hotel_jenis").modal('show');
  }

  function clear_mod_up_disbudpar_kunjungan_hotel_jenis(){
      $("#_id_kategori").val("");
      $("#_nama_jenis").val("");
      $("#_alamat").val("");
      $("#_jml_kmr").val(""); 
  }  

  function res_update_disbudpar_kunjungan_hotel_jenis(res){
      var data = JSON.parse(res);

      if(data.status){
          var main_data = data.val;
          console.log(main_data);
          
          $("#_id_kategori").attr('readonly','readonly');                                        
          $("#_id_kategori").val(main_data.id_kategori);
          $("#_nama_jenis").val(main_data.nama_jenis);
          $("#_alamat").val(main_data.alamat);
          $("#_jml_kmr").val(main_data.jml_kmr);
          
      }else{
          clear_mod_up_ik();
      }
  }  

  function del_disbudpar_kunjungan_hotel_jenis(id_jenis){
      var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
      if(conf){
        window.location.href = "<?= base_url()."admin_opd/budpar/Disbudparkunjunganhoteljenis/delete_disbudpar_kunjungan_hotel_jenis/";?>"+id_jenis;
      }else{

      }
  }  



   function up_disbudpar_kunjungan_hotel_kate(id_kategori){
    clear_mod_up_disbudpar_kunjungan_hotel_kate();
    // console.lo

    var data_main =  new FormData();
    data_main.append('id_kategori', id_kategori);    
      $.ajax({
        url: "<?php echo base_url()."admin_opd/budpar/Disbudparkunjunganhoteljenis/index_up_disbudpar_kunjungan_hotel_kate/";?>", // point to serv
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: data_main,                         
        type: 'post',
        success: function(res){
            console.log(res);
            res_update_disbudpar_kunjungan_hotel_kate(res);
            // $("#out_up_mhs").html(res);
        }
    });
    
    $("#modal_up_disbudpar_kunjungan_hotel_kate").modal('show');
  }

  function clear_mod_up_disbudpar_kunjungan_hotel_kate(){
      $("#_nama_kategori").val("");
  }  

  function res_update_disbudpar_kunjungan_hotel_kate(res){
      var data = JSON.parse(res);

      if(data.status){
          var main_data = data.val;
          console.log(main_data);
          
          $("#_id_kategori").attr('readonly','readonly');                                        
          $("#_id_kategori").val(main_data.id_kategori);
          $("#_nama_kategori").val(main_data.nama_kategori);
          
      }else{
          clear_mod_up_ik();
      }
  }  

  function del_disbudpar_kunjungan_hotel_kate(id_kategori){
      var conf = confirm("Apakah anda yakin untuk menghapus "+id_kategori+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kategori+" akan terhapus semua.. !!!!!!! ");
      if(conf){
        window.location.href = "<?= base_url()."admin_opd/budpar/Disbudparkunjunganhoteljenis/delete_disbudpar_kunjungan_hotel_kate/";?>"+id_kategori;
      }else{

      }
  }  




  function up_disbudpar_kunjungan_hotel_main(id_main){
    clear_mod_up_disbudpar_kunjungan_hotel_main();
    // console.lo

    var data_main =  new FormData();
    data_main.append('id_main', id_main);    
      $.ajax({
        url: "<?php echo base_url()."admin_opd/budpar/Disbudparkunjunganhoteljenis/index_up_disbudpar_kunjungan_hotel_main/";?>", // point to serv
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: data_main,                         
        type: 'post',
        success: function(res){
            console.log(res);
            res_update_disbudpar_kunjungan_hotel_main(res);
            // $("#out_up_mhs").html(res);
        }
    });
    
    $("#modal_up_disbudpar_kunjungan_hotel_main").modal('show');
  }

  function clear_mod_up_disbudpar_kunjungan_hotel_main(){
      $("#_id_jenis").val("");
      $("#_th").val("");
      $("#_wisman").val("");
      $("#_wisnus").val("");
  }  

  function res_update_disbudpar_kunjungan_hotel_main(res){
      var data = JSON.parse(res);

      if(data.status){
          var main_data = data.val;
          console.log(main_data);
          
          $("#_id_main").attr('readonly','readonly');                                        
          $("#_id_main").val(main_data.id_main);
          $("#_id_jenis").val(main_data.id_jenis);
          $("#_th").val(main_data.th);
          $("#_wisman").val(main_data.wisman);
          $("#_wisnus").val(main_data.wisnus);
          
      }else{
          clear_mod_up_ik();
      }
  }  

  function del_disbudpar_kunjungan_hotel_main(id_main){
      var conf = confirm("Apakah anda yakin untuk menghapus "+id_main+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_main+" akan terhapus semua.. !!!!!!! ");
      if(conf){
        window.location.href = "<?= base_url()."admin_opd/budpar/Disbudparkunjunganhoteljenis/delete_disbudpar_kunjungan_hotel_main/";?>"+id_main;
      }else{

      }
  }  
</script>


</div>

	<section class="content-header">
		
	</section>

	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Daya Tarik Strategis</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center">No</th>
										<th class="text-center">Jenis</th>
										<th class="text-center">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/admin_opd/geo/Disbudpardts/insert_disbudpar_dts/" ?>" method="post">
											<td><input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Daya Tarik Strategis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_dts as $eek) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $eek->nama ?></td>
											<td align="center">
												<a href="#" onclick="up_disbudpar_dts(<?php echo $eek->id_main ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_disbudpar_dts(<?php echo $eek->id_main ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>				
			</div>

		</section>
	</div>


	<script type="text/javascript">
		// $(document).ready(function(){
		// 	$("#aparat").DataTable();
		// });

//======================================================== LP APARAT =============================================//
	  	function up_disbudpar_dts(id_main){
	    	clear_mod_up_disbudpar_dts();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_main', id_main);    
		      $.ajax({
		        url: "<?php echo base_url()."admin_opd/budpar/Disbudpardts/index_up_disbudpar_dts/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_disbudpar_dts(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_disbudpar_dts").modal('show');
	  	} 

	  	function clear_mod_up_disbudpar_dts(){
	      	$("#_nama").val("");
	  	}

	  	function res_update_disbudpar_dts(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_main").attr('readonly','readonly');                                        
          		$("#_id_main").val(main_data.id_main);
          		$("#_nama").val(main_data.nama);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_disbudpar_dts(id_main){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_main+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_main+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."admin_opd/budpar/Disbudpardts/delete_disbudpar_dts/";?>"+id_main;
	      	}else{

	      	}
	  	}
//======================================================== LP PERTANIAN =============================================//
//======================================================== LP PERTANIANSUBJenis ===============================//

//======================================================== LP APARAT Jenis =============================================//
	</script>	
<div class="modal fade" id="modal_up_disbudpar_dts">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">&times;</span>
	          		</button>
	        		<h4 class="modal-title">Ubah Data Daya Tarik Kawasan</h4>
	      		</div>
	      		<?php echo form_open("admin_opd/budpar/Disbudpardts/update_disbudpar_dts"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
	                        				<input type="number" class="form-control" id="_id_main" name="id_main" placeholder="ID Jenis">
	                      				</div>     

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama Jenis</label>
	                        				<input type="text" name="nama" class="form-control" id="_nama" placeholder="Nama">
	                      				</div>

	                  					</div>
	                				</div>      
	            				</div>
	          				</div>
	        			</div>
	      			</div>
	      		<div class="modal-footer">
			        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
			        <button type="submit" class="btn btn-primary">Ubah Data</button>
	      		</div>
	      		</form>
	    	</div>
	  	</div>
	</div>		


</div>	
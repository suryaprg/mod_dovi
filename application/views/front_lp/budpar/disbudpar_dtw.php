<section class="content-header">
		
	</section>

	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
	
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Data Lampiran Daya Tarik Wisatawan  </h3>
					</div>

					<div class="box-body">
						<table class="table table-bordered table-striped display " id="example1">
							<thead>
								<tr>
									<th class="text-center" style="width: 1%">No</th>
									<!-- <th class="text-center" style="width: 1%">Id Main</th> -->
									<th class="text-center" style="width: 3%">Jenis</th>
									<th class="text-center" style="width: 1%">Tahun</th>
									<th class="text-center" style="width: 1%">Wisman</th>
									<th class="text-center" style="width: 1%">Wisnus</th>
									<th class="text-center" style="width: 2%">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<form action="<?php echo base_url()."/admin_opd/budpar/Disbudpardtw/insert_disbudpar_dtw_main/" ?>" method="post">
									<!-- <td><input type="text" name="id_main" class="form-control" id="id_main"></td> -->
										<td>
											<select name="id_jenis" class="form-control" id="id_jenis">
												<option value="">-- Pilih Jenis --</option>
												<?php foreach ($list_dtw_jenis as $lpj) { ?>
												<option value="<?php echo $lpj->id_jenis ?>"><?php echo $lpj->nama_jenis?></option>
												<?php } ?>
											</select>
										</td>
										<td><input type="text" name="th" class="form-control" id="th" placeholder="Tahun"></td>
										<td><input type="text" name="wisman" class="form-control" id="wisman" placeholder="Wisatawan Mancanegara"></td>
										<td><input type="text" name="wisnus" class="form-control" id="wisnus" placeholder="Wisatawan Nusantara"></td>
										<td align="center">
										<button type="submit" class="btn btn-success">
										<span class="glyphicon glyphicon-plus"> Tambah</span>
											</button>
										</td>
									</form>
								</tr>
								<?php $no = 1; foreach ($list_dtw_main as $lpsj) { ?>
									<tr>
										<td align="center"><?php echo $no ?></td>
										<!-- <td align="center"><?php echo $lpsj->id_main ?></td> -->
										<td align="left"><?php echo $lpsj->nama_jenis ?></td>
										<td align="center"><?php echo $lpsj->th ?></td>
										<td align="center"><?php echo $lpsj->wisman ?></td>
										<td align="center"><?php echo $lpsj->wisnus ?></td>
										<td align="center">
											<a href="#" onclick="up_disbudpar_dtw_main(<?php echo $lpsj->id_main?>)">
												<button class="btn btn-primary btn-xs">
													<span class="glyphicon glyphicon-pencil"></span>
												</button>
											</a>
											<a href="#" onclick="del_disbudpar_dtw_main(<?php echo $lpsj->id_main ?>)">
												<button class="btn btn-danger btn-xs">
													<span class="glyphicon glyphicon-trash"></span>
												</button>
											</a>												
										</td>
									</tr>
								<?php $no++; } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Data Lampiran Daya Tarik Wisatawan</h3>
					</div>

					<div class="box-body">
						<table class="table table-bordered table-striped display" id="dst">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-center">Jenis</th>
									<th class="text-center">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<form action="<?php echo base_url()."/admin_opd/budpar/Disbudpardtw/insert_disbudpar_dtw_jenis/" ?>" method="post">
										<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis" placeholder="Nama Jenis"></td>
										<td align="center">
											<button type="submit" class="btn btn-success">
												<span class="glyphicon glyphicon-plus"> Tambah</span>
											</button>
										</td>
									</form>
								</tr>
								<?php $no = 1; foreach ($list_dtw_jenis as $asd) { ?>
									<tr>
										<td align="center"><?php echo $no ?></td>
										<td align="left"><?php echo $asd->nama_jenis ?></td>
										<td align="center">
											<a href="#" onclick="up_disbudpar_dtw_jenis(<?php echo $asd->id_jenis ?>)">
												<button class="btn btn-primary btn-xs">
													<span class="glyphicon glyphicon-pencil"></span>
												</button>
											</a>
											<a href="#" onclick="del_disbudpar_dtw_jenis(<?php echo $asd->id_jenis ?>)">
												<button class="btn btn-danger btn-xs">
													<span class="glyphicon glyphicon-trash"></span>
												</button>
											</a>												
										</td>
									</tr>
								<?php $no++; } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>				
		</div>

		</section>
	</div>


	<script type="text/javascript">
		// $(document).ready(function(){
		// 	$("#aparat").DataTable();
		// });


function up_disbudpar_dtw_main(id_main){
	    	clear_mod_up_disbudpar_dtw_main();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_main', id_main);    
		      $.ajax({
		        url: "<?php echo base_url()."/admin_opd/budpar/Disbudpardtw/index_up_disbudpar_dtw_main/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_disbudpar_dtw_main(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_disbudpar_dtw_main").modal('show');
	  	} 

	  	function clear_mod_up_disbudpar_dtw_main(){
	      	$("#_id_jenis").val("");
	      	$("#_th").val("");
	      	$("#_wisman").val("");
	      	$("#_wisnus").val("");
	  	}

	  	function res_update_disbudpar_dtw_main(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_main").attr('readonly','readonly');                                        
          		$("#_id_main").val(main_data.id_main);
        		 // $("#_id_jenis").attr('readonly','readonly');
          		$("#_id_jenis2").val(main_data.id_jenis);
         	 	$("#_th").val(main_data.th);
          		$("#_wisman").val(main_data.wisman);
          		$("#_wisnus").val(main_data.wisnus);
          
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_disbudpar_dtw_main(id_main){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_main+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_main+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."admin_opd/budpar/Disbudpardtw/delete_disbudpar_dtw_main/";?>"+id_main;
	      	}else{

	      	}
	  	}
//======================================================== LP PERTANIAN Jenis ====================================//
	function up_disbudpar_dtw_jenis(id_jenis){
	    	clear_mod_up_disbudpar_dtw_jenis();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_jenis', id_jenis);    
		      $.ajax({
		        url: "<?php echo base_url()."/admin_opd/budpar/Disbudpardtw/index_up_disbudpar_dtw_jenis/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_disbudpar_dtw_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_disbudpar_dtw_jenis").modal('show');
	  	} 

	  	function clear_mod_up_disbudpar_dtw_jenis(){
	      	$("#_nama_jenis").val("");
	  	}

	  	function res_update_disbudpar_dtw_jenis(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_jenis3").attr('readonly','readonly');                                        
          		$("#_id_jenis3").val(main_data.id_jenis);
          		$("#_nama_jenis").val(main_data.nama_jenis);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_disbudpar_dtw_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."admin_opd/budpar/Disbudpardtw/delete_disbudpar_dtw_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}
	 </script>
<div class="modal fade" id="modal_up_disbudpar_dtw_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">&times;</span>
	          		</button>
	        		<h4 class="modal-title">Ubah Data daya tarik wisatawan Jenis</h4>
	      		</div>
	      		<?php echo form_open("admin_opd/budpar/Disbudpardtw/update_disbudpar_dtw_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
	                        				<input type="number" class="form-control" id="_id_jenis3" name="id_jenis" placeholder="ID Jenis">
	                      				</div>     

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama Jenis</label>
	                        				<input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Nama Jenis">
	                      				</div>

	                  					</div>
	                				</div>      
	            				</div>
	          				</div>
	        			</div>
	      			</div>
	      		<div class="modal-footer">
			        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
			        <button type="submit" class="btn btn-primary">Ubah Data</button>
	      		</div>
	      		</form>
	    	</div>
	  	</div>
	</div>			


</div>	


<div class="modal fade" id="modal_up_disbudpar_dtw_main">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">&times;</span>
	          		</button>
	        		<h4 class="modal-title">Ubah Data daya tarik wisatawan main</h4>
	      		</div>
	      		<?php echo form_open("admin_opd/budpar/Disbudpardtw/update_disbudpar_dtw_main"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Main</label>
	                        				<input type="number" class="form-control" id="_id_main" name="id_main" placeholder="ID Main">
	                      				</div>     

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">id_Jenis</label>
	                        				<input type="text" name="id_jenis" class="form-control" id="_id_jenis2" placeholder="id Jenis">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Tahun</label>
	                        				<input type="text" name="th" class="form-control" id="_th" placeholder="Th">
	                      				</div>
	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">wisman</label>
	                        				<input type="text" name="wisman" class="form-control" id="_wisman" placeholder="Wisman">
	                      				</div>
	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Wisnus</label>
	                        				<input type="text" name="wisnus" class="form-control" id="_wisnus" placeholder="Wisnus">
	                      				</div>

	                  					</div>
	                				</div>      
	            				</div>
	          				</div>
	        			</div>
	      			</div>
	      		<div class="modal-footer">
			        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
			        <button type="submit" class="btn btn-primary">Ubah Data</button>
	      		</div>
	      		</form>
	    	</div>
	  	</div>
	</div>			


</div>	
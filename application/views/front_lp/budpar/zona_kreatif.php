	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">

			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Draft SK Zona Kreatif</h3>
						</div>

						<table class="table">
							<thead>
								<tr>
									<th style="width: 1%"></th>
									<th style="width: 8%"></th>
									<th style="width: 8%"></th>
									<th style="width: 1%"></th>
								</tr>
							</thead>		
							<tbody>
								<tr>
									<td></td>
									<form action="<?php echo base_url()."admin_opd/budpar/disbudpar_zona_kreatif/insert_disbudpar_zona/" ?>" method="post">
										<td><input type="text" name="nama_zona" class="form-control" id="nama_zona" placeholder="Zona Kreatif"></td>
												<td><input type="text" name="alamat" class="form-control" id="alamat" placeholder="Alamat"></td>
										<td align="center">
											<button type="submit" class="btn btn-success">
												<span class="glyphicon glyphicon-plus"> Tambah</span>
											</button>
										</td>
									</form>
								</tr>								
							</tbody>					
						</table>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 8%">Nama Zona</th>
										<th class="text-center" style="width: 8%">Alamat</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1; foreach ($list_zona as $zona) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $zona->nama_zona ?></td>
												<td align="left"><?php echo $zona->alamat ?></td>
											<td align="center">
												<a href="#" onclick="up_zona(<?php echo $zona->id_zona ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_zona(<?php echo $zona->id_zona ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>				

			</div>			
		</section>
	</div>

	<script type="text/javascript">
//====================================================== LP DISNAKER KATEGORI ====================================//
	  	function up_zona(id_zona){
	    	clear_mod_up_zona();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_zona', id_zona);    
		      $.ajax({
		        url: "<?php echo base_url()."/admin_opd/budpar/disbudpar_zona_kreatif/index_up_disbudpar_zona";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_zona(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_zona").modal('show');
	  	} 

	  	function clear_mod_up_zona(){
	      	$("#_nama_zona").val("");
	  	}

	  	function res_update_zona(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_zona").attr('readonly','readonly');                                        
	          	$("#_id_zona").val(main_data.id_zona);
	          	$("#_nama_zona").val(main_data.nama_zona);
	          		$("#_alamat").val(main_data.alamat);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_zona(id_zona){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_zona+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_zona+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."admin_opd/budpar/disbudpar_zona_kreatif/delete_disbudpar_zona/";?>"+id_zona;
	      	}else{

	      	}
	  	}      		
//====================================================== LP DISNAKER KATEGORI ====================================//		
	</script>

	<div class="modal fade" id="modal_up_zona">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data Kategori</h4>
	      	</div>
	      	<?php echo form_open("admin_opd/budpar/disbudpar_zona_kreatif/update_disbudpar_zona"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID</label>
	                        	<input type="number" class="form-control" id="_id_zona" name="id_zona">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Nama</label>
	                        	<input type="text" class="form-control" id="_nama_zona" name="nama_zona" placeholder="Nama">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Alamat</label>
	                        	<input type="text" class="form-control" id="_alamat" name="alamat" placeholder="Alamat">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>	
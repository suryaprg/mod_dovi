	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Umum Objek Pemajuan Kebudayaan</h3>
						</div>

						<table class="table">
							<thead>
								<tr>
									<th style="width: 1%"></th>
									<th style="width: 3%"></th>
									<th style="width: 3%"></th>
									<th style="width: 1%"></th>
									<th style="width: 1%"></th>
									<th style="width: 1%"></th>
								</tr>								
							</thead>
							<tbody>
								<tr>
									<td></td>
									<form action="<?php echo base_url()."admin_opd/budpar/Obj_main/insert_obj/" ?>" method="post">
										<td>
											<select name="id_kategori" class="form-control">
												<option value="">- Kategori -</option>
												<?php foreach ($list_obj_kategori as $ktg) { ?>
													<option value="<?php echo $ktg->id_kategori ?>"><?php echo $ktg->nama_kategori ?></option>
												<?php } ?>
											</select>
										</td>											
										<td><input type="text" name="nama_obj" class="form-control"></td>
										<td><input type="text" name="th" class="form-control"></td>
										<td><input type="text" name="lokasi" class="form-control"></td>
										<td align="center">
											<button type="submit" class="btn btn-success">
												<span class="glyphicon glyphicon-plus"></span>
											</button>
										</td>
									</form>
								</tr>								
							</tbody>
						</table>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 3%">Kategori</th>
										<th class="text-center" style="width: 3%">Nama</th>
										<th class="text-center" style="width: 1%">Tahun</th>
										<th class="text-center" style="width: 1%">Lokasi</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1; foreach ($list_obj_main as $key) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $key->nama_kategori ?></td>
											<td align="left"><?php echo $key->nama_obj ?></td>
											<td align="center"><?php echo $key->th ?></td>
											<td align="center"><?php echo $key->lokasi ?></td>
											<td align="center">
												<a href="#" onclick="up_obj(<?php echo $key->id_main ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_obj(<?php echo $key->id_main ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="row">
				
				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Kategori</h3>
						</div>

						<table class="table">
							<thead>
								<tr>
									<th style="width: 1%"></th>
									<th style="width: 8%"></th>
									<th style="width: 1%"></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<form action="<?php echo base_url()."/admin_opd/budpar/Obj_main/insert_obj_kategori/" ?>" method="post">
										<td><input type="text" name="nama_kategori" class="form-control" placeholder="nama kategori"></td>
										<td align="center">
											<button type="submit" class="btn btn-success">
												<span class="glyphicon glyphicon-plus"></span>
											</button>
										</td>
									</form>
								</tr>
							</tbody>
						</table>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 8%">Nama</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1; foreach ($list_obj_kategori as $ktg) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $ktg->nama_kategori ?></td>
											<td align="center">
												<a href="#" onclick="up_obj_kategori(<?php echo $ktg->id_kategori ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="delete_obj_kategori(<?php echo $ktg->id_kategori ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

		</section>
	</div>

	<script type="text/javascript">
//========================================================== OBJ =========================================//
		  function up_obj(id_main){
		    clear_mod_up_obj();
		    // console.lo

		    var data_main =  new FormData();
		    data_main.append('id_main', id_main);    
		      $.ajax({
		        url: "<?php echo base_url()."admin_opd/budpar/Obj_main/index_up_obj/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_obj(res);
		            // $("#out_up_mhs").html(res);
		        }
		    });
		    
		    $("#modal_up_obj").modal('show');
		  } 

		  function clear_mod_up_obj(){
		      $("#_id_kategori").val("");
		       $("#_nama_obj").val("");
		      $("#_th").val("");
		      $("#_lokasi").val("");
		  }

		  function res_update_obj(res){
		      var data = JSON.parse(res);

		      if(data.status){
		          var main_data = data.val;
		          console.log(main_data);
		          
		          $("#_id_main").attr('readonly','readonly');                                        
		          $("#_id_main").val(main_data.id_main);
		          $("#_id_kategori").val(main_data.id_kategori);
		          $("#_nama_obj").val(main_data.nama_obj);
		          $("#_th").val(main_data.th);
		          $("#_lokasi").val(main_data.lokasi);
		          
		      }else{
		          clear_mod_up_ik();
		      }
		  }    

		  function del_obj(id_main){
		      var conf = confirm("Apakah anda yakin untuk menghapus "+id_main+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_main+" akan terhapus semua.. !!!!!!! ");
		      if(conf){
		        window.location.href = "<?= base_url()."admin_opd/budpar/Obj_main/delete_obj/";?>"+id_main;
		      }else{

		      }
		  }   
//========================================================== OBJ =========================================//

//========================================================== OBJ KTG =========================================//
		  function up_obj_kategori(id_kategori){
		    clear_mod_up_obj_kategori();
		    // console.lo

		    var data_main =  new FormData();
		    data_main.append('id_kategori', id_kategori);    
		      $.ajax({
		        url: "<?php echo base_url()."admin_opd/budpar/Obj_main/index_up_obj_kategori/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_obj_kategori(res);
		            // $("#out_up_mhs").html(res);
		        }
		    });
		    
		    $("#modal_up_obj_kategori").modal('show');
		  } 

		  function clear_mod_up_obj_kategori(){
		     
		      $("#_nama_kategori").val("");
		  }

		  function res_update_obj_kategori(res){
		      var data = JSON.parse(res);

		      if(data.status){
		          var main_data = data.val;
		          console.log(main_data);
		          
		          $("#_id_kategori2").attr('readonly','readonly');                                        
		          $("#_id_kategori2").val(main_data.id_kategori);
		          $("#_nama_kategori").val(main_data.nama_kategori);
		          
		      }else{
		          clear_mod_up_ik();
		      }
		  }    

		  function delete_obj_kategori(id_kategori){
		      var conf = confirm("Apakah anda yakin untuk menghapus "+id_kategori+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kategori+" akan terhapus semua.. !!!!!!! ");
		      if(conf){
		        window.location.href = "<?= base_url()."admin_opd/budpar/Obj_main/delete_obj_kategori/";?>"+id_kategori;
		      }else{

		      }
		  }   
//========================================================== OBJ KTG =========================================//		
	</script>

	<div class="modal fade" id="modal_up_obj">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Ubah Data OBJ</h4>
	      </div>
	      <?php echo form_open("admin_opd/budpar/Obj_main/update_obj"); ?>
	      <div class="modal-body">
	        <div class="row">
	          <div class="col-md-12">
	            <div class="box box-primary">
	                <div class="box-body">
	                  <div class="row">
	                    <div class="col-md-12">

	                      <div class="form-group">
	                        <label for="exampleInputPassword1">ID</label>
	                        <input type="number" class="form-control" id="_id_main" name="id_main" placeholder="ID">
	                      </div>                      
	                
	                  <div class="form-group">
	                    <label for="exampleInputPassword1">Kategori</label>
	                    <select name="id_kategori" class="form-control" id="_id_kategori">
	                      <option value="">-- select id kategori --</option>
	                      <?php foreach ($list_obj_kategori as $kategori) { ?>
	                        <option value="<?php echo $kategori->id_kategori ?>"><?php echo $kategori->nama_kategori ?></option>
	                      <?php } ?>
	                    </select>
	                  </div>
	                  <div class="form-group">
	                    <label for="exampleInputPassword1">Nama</label>
	                    <input type="text" class="form-control" id="_nama_obj" name="nama_obj" placeholder="Nama OBJ">
	                  </div>

	                  <div class="form-group">
	                    <label for="exampleInputPassword1">Tahun</label>
	                    <input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
	                  </div>

	                  <div class="form-group">
	                    <label for="exampleInputPassword1">Lokasi</label>
	                    <input type="text" class="form-control" id="_lokasi" name="lokasi" placeholder="Lokasi">
	                  </div>

	                  </div>
	                </div>
	            </div>
	          </div>
	        </div>
	      </div>

	      <div class="modal-footer">
	        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        <button type="submit" class="btn btn-primary">Ubah Data</button>
	      </div>

	      </form>
	    </div>
	  </div>
	</div>	
</div>

	<div class="modal fade" id="modal_up_obj_kategori">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Ubah Data OBJ Kategori</h4>
	      </div>
	      <?php echo form_open("admin_opd/budpar/Obj_main/update_obj_kategori"); ?>
	      <div class="modal-body">
	        <div class="row">
	          <div class="col-md-12">
	            <div class="box box-primary">
	                <div class="box-body">
	                  <div class="row">
	                    <div class="col-md-12">

	                      <div class="form-group">
	                        <label for="exampleInputPassword1">ID</label>
	                        <input type="number" class="form-control" id="_id_kategori2" name="id_kategori" placeholder="ID">
	                      </div>                      
	                  <div class="form-group">
	                    <label for="exampleInputPassword1">Nama Kategori</label>
	                    <input type="text" class="form-control" id="_nama_kategori" name="nama_kategori" placeholder="Nama Kategori">
	                  </div>

	                  </div>
	                </div>
	            </div>
	          </div>
	        </div>
	      </div>

	      <div class="modal-footer">
	        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        <button type="submit" class="btn btn-primary">Ubah Data</button>
	      </div>

	      </form>
	    </div>
	  </div>
	</div>
	</div>
  <section class="content-header">
    
  </section>

  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Lampiran Panen</h3>
            </div>
            <table class="table table-responsive">
            <thead>
                  <tr>
                    <th style="width: 1%"></th>
                            <!-- <th class="text-center">Id</th> -->
                    <th style="width: 3%"></th>
                    <th style="width: 2%"></th>
                    <th style="width: 2%"></th>
                    <th style="width: 2%"></th>
                    <th style="width: 2%"></th>
                    <th style="width: 2%"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <form action="<?php echo base_url()."/front_lp/Lppanen/insert_lp_panen/" ?>" method="post">
                      <td>
                        <select name="id_jenis" class="form-control" id="id_jenis">
                          <option value="">-- Pilih Jenis --</option>
                          <?php foreach ($list_panen_jenis as $llj) { ?>
                          <option value="<?php echo $llj->id_jenis ?>"><?php echo $llj->nama_jenis ?></option>
                          <?php } ?>
                        </select>
                      </td>
                    <td>
                        <select name="id_kec" class="form-control" id="id_kec">
                          <option value="">-- Pilih Jenis --</option>
                          <?php foreach ($list_main_kecamatan as $llj) { ?>
                          <option value="<?php echo $llj->id_kec ?>"><?php echo $llj->nama_kec ?></option>
                          <?php } ?>
                        </select>
                      </td>
                      <td><input type="number" name="th" class="form-control" id="th"></td>
                      <td><input type="number" name="luas_panen" class="form-control" id="luas_panen" placeholder="Luas Panen"></td>
                      <td><input type="number" name="jml_panen" class="form-control" id="jml_panen" placeholder="Jumlah Panen"></td>
                      <td align="center">
                        <button type="submit" class="btn btn-success">
                          <span class="glyphicon glyphicon-plus"></span>
                        </button>
                      </td>
                    </form>
                  </tr>
              
            </table>

            <div class="box-body">
              <table class="table table-bordered table-striped display" id="example1">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 1%">No</th>
                            <!-- <th class="text-center">Id</th> -->
                    <th class="text-center" style="width: 3%">Jenis</th>
                    <th class="text-center" style="width: 2%">nama kec</th>
                    <th class="text-center" style="width: 2%">Tahun</th>
                    <th class="text-center" style="width: 2%">Luas Panen</th>
                    <th class="text-center" style="width: 2%">Jumlah panen</th>
                    <th class="text-center" style="width: 2%">Action</th>
                  </tr>
                </thead>
                  <?php $no = 1; foreach ($list_panen as $la) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td align="left"><?php echo $la->nama_jenis ?></td>
                      <td align="center"><?php echo $la->nama_kec ?></td>
                      <td align="center"><?php echo $la->th ?></td>
                      <td align="center"><?php echo $la->luas_panen ?></td>
                      <td align="center"><?php echo $la->jml_panen ?></td>
                      <td align="center">
                        <a href="#" onclick="up_lp_panen(<?php echo $la->id_panen ?>)">
                          <button class="btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span>
                          </button>
                        </a>
                        <a href="#" onclick="del_lp_panen(<?php echo $la->id_panen ?>)">
                          <button class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-trash"></span>
                          </button>
                        </a>                        
                      </td>
                    </tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Lampiran Jenis panen</h3>
            </div>
            <table class="table table-responsive">
            <thead>
                  <tr>
                  <th style="width: 2.5%"></th>
                  <th style="width: 4.5%"></th>
                  <th style="width: 3%"></th> 
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <form action="<?php echo base_url()."/front_lp/Lppanen/insert_lp_panen_jenis/" ?>" method="post">
                      <td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis" placeholder="Nama Jenis"></td>
                      <td align="center">
                        <button type="submit" class="btn btn-success">
                          <span class="glyphicon glyphicon-plus"> Tambah</span>
                        </button>
                      </td>
                    </form>
                  </tr>
              
            </table>

            <div class="box-body">
              <table class="table table-bordered table-striped display" id="panen">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Jenis</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                  <?php $no = 1; foreach ($list_panen_jenis as $lpj) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td align="left"><?php echo $lpj->nama_jenis ?></td>
                      <td align="center">
                        <a href="#" onclick="up_lp_panen_jenis(<?php echo $lpj->id_jenis ?>)">
                          <button class="btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span>
                          </button>
                        </a>
                        <a href="#" onclick="del_lp_panen_jenis(<?php echo $lpj->id_jenis ?>)">
                          <button class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-trash"></span>
                          </button>
                        </a>                        
                      </td>
                    </tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </section>
  </div>


  <script type="text/javascript">
    // $(document).ready(function(){
    //  $("#aparat").DataTable();
    // });

//======================================================== LP PANEN =============================================//
  function up_lp_panen(id_panen){
    clear_mod_up_lp_panen();
    // console.lo

    var data_main =  new FormData();
    data_main.append('id_panen', id_panen);    
      $.ajax({
        url: "<?php echo base_url()."/front_lp/Lppanen/index_up_lp_panen/";?>", // point to serv
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: data_main,                         
        type: 'post',
        success: function(res){
            console.log(res);
            res_update_lp_panen(res);
            // $("#out_up_mhs").html(res);
        }
    });
    
    $("#modal_up_lp_panen").modal('show');
  } 

  function clear_mod_up_lp_panen(){
      $("#_id_jenis").val("");
      $("#_id_kec").val("");
      $("#_th").val("");
    }

  function res_update_lp_panen(res){
      var data = JSON.parse(res);

      if(data.status){
          var main_data = data.val;
          console.log(main_data);
          
          $("#_id_panen").attr('readonly','readonly');                                        
          $("#_id_panen").val(main_data.id_panen);
          $("#_id_jenis").val(main_data.id_jenis);
          $("#_id_kec").val(main_data.id_kec);
          $("#_th").val(main_data.th);
          $("#_luas_panen").val(main_data.luas_panen);
          $("#_jml_panen").val(main_data.jml_panen);
          
      }else{
          clear_mod_up_ik();
      }
  }    

  function del_lp_panen(id_panen){
      var conf = confirm("Apakah anda yakin untuk menghapus "+id_panen+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_panen+" akan terhapus semua.. !!!!!!! ");
      if(conf){
        window.location.href = "<?= base_url()."/front_lp/Lppanen/delete_lp_panen/";?>"+id_panen;
      }else{

      }
  }   
//======================================================== LP PANEN JENIS ===============================//
 function up_lp_panen_jenis(id_jenis){
    clear_mod_up_lp_panen_jenis();
    // console.lo

    var data_main =  new FormData();
    data_main.append('id_jenis', id_jenis);    
      $.ajax({
        url: "<?php echo base_url()."/front_lp/Lppanen/index_up_lp_panen_jenis/";?>", // point to serv
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: data_main,                         
        type: 'post',
        success: function(res){
            console.log(res);
            res_update_lp_panen_jenis(res);
            // $("#out_up_mhs").html(res);
        }
    });
    
    $("#modal_up_lp_panen_jenis").modal('show');
  }

  function clear_mod_up_lp_panen_jenis(){
      $("#_nama_jenis").val("");
  }  

  function res_update_lp_panen_jenis(res){
      var data = JSON.parse(res);

      if(data.status){
          var main_data = data.val;
          console.log(main_data);
          
          $("#_id_jenis2").attr('readonly','readonly');                                        
          $("#_id_jenis2").val(main_data.id_jenis);
          $("#_nama_jenis").val(main_data.nama_jenis);
          
      }else{
          clear_mod_up_ik();
      }
  }  

  function del_lp_panen_jenis(id_jenis){
      var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
      if(conf){
        window.location.href = "<?= base_url()."/front_lp/Lppanen/delete_lp_panen_jenis/";?>"+id_jenis;
      }else{

      }
  }  
//======================================================== MODAL  OR =============================================//
  </script> 


<div class="modal fade" id="modal_up_lp_panen">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ubah Data Lp panen</h4>
      </div>
      <?php echo form_open("/front_lp/Lppanen/update_lp_panen"); ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">

                    <div class="form-group">
                        <label for="exampleInputPassword1">ID</label>
                        <input type="number" class="form-control" id="_id_panen" name="id_panen" placeholder="ID">
                      </div>
                  
                      
                      <div class="form-group">
                        <label for="exampleInputPassword1">Jenis</label>
                        <select name="id_jenis" class="form-control" id="_id_jenis">
                          <option value="">-- select id jenis --</option>
                          <?php foreach ($list_panen_jenis as $jenis) { ?>
                            <option value="<?php echo $jenis->id_jenis ?>"><?php echo $jenis->nama_jenis ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Kecamatan</label>
                        <select name="id_kec" class="form-control" id="_id_kec">
                          <option value="">-- select id kec --</option>
                          <?php foreach ($list_main_kecamatan as $jenis) { ?>
                            <option value="<?php echo $jenis->id_kec ?>"><?php echo $jenis->nama_kec ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Tahun</label>
                        <input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Luas Panen</label>
                        <input type="number" class="form-control" id="_luas_panen" name="luas_panen" placeholder="Luas Panen">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Jumlah Panen</label>
                        <input type="number" class="form-control" id="_jml_panen" name="jml_panen" placeholder="Jumlah Panen">
                      </div>

                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>

      </form>
    </div>
  </div>
</div>
</div>


  

<div class="modal fade" id="modal_up_lp_panen_jenis">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ubah Data Lp panen Jenis</h4>
      </div>
      <?php echo form_open("/front_lp/Lppanen/update_lp_panen_jenis"); ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">

                      <div class="form-group">
                        <label for="exampleInputPassword1">ID Jenis</label>
                        <input type="number" class="form-control" id="_id_jenis2" name="id_jenis" placeholder="ID Jenis">
                      </div>     

                      <div class="form-group">
                        <label for="exampleInputPassword1">Nama Jenis</label>
                        <input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Nama Jenis">
                      </div>

                  </div>

                </div>      
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>
<!DOCTYPE html>
<html>
<head>
	<title>keuangan</title>

	<script type="text/javascript" src="<?=base_url();?>assets/js/jquery-3.2.1.js"></script>
</head>
<body>
	<table border="1" width="35%">
		<thead>
			<tr>
				<th width="5%">No</th>
				<th>Keterangan</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($th_st){
					$str_header_sub = "<thead>
											<tr>
												<th>No. </th>
												<th>Sub. Jenis</th>";
					for ($i=$th_st; $i <= $th_fn ; $i++) { 
						$str_header_sub .= "<th>".$i."</th>";
					}

					$str_header_sub .= "	</tr>
										</thead>";	
				}
			?>
			
				
				
			<?php
				if(isset($keuangan)){
					if($keuangan){

						$no = 1;
						foreach ($keuangan as $r_date => $v_data) {
							echo "<tr class=\"jenis_click\" id=\"jenis_".$v_data["jenis"]["id_jenis"]."\">
									<td><a href=\"#\">".$no."</a></td>
									<td><a href=\"#\">".$v_data["jenis"]["nama_jenis"]."</a></td>
								</tr>";
							echo "<tr class=\"sub_jenis_out\" id=\"tmp_".$v_data["jenis"]["id_jenis"]."\">
									<td>&nbsp;</td>
									<td>
										<table border='1' width=\"100%\" id=\"output_".$v_data["jenis"]["id_jenis"]."\">";
							
										echo $str_header_sub;
										
										echo "<tbody>";

										$no_sub = 1;
										foreach ($v_data["sub_jenis"] as $r_sub => $v_sub) {
											echo "<tr class=\"sub_jenis\" id=\"sub_".$v_sub["id_sub_jenis"]."\">
													<td width=\"5%\">".$no_sub."</td>
													<td width=\"35%\">".$v_sub["nama_sub_jenis"]."</td>";
											for ($i=$th_st; $i <= $th_fn ; $i++) { 
												if($v_sub["value"][$i]){
													echo "<td width=\"20%\" align=\"right\">".$v_sub["value"][$i]."</td>";
												}else{
													echo "<td width=\"20%\" align=\"center\">-</td>";
												}
												
											}
											echo "</tr>";
											$no_sub++;
										}
										echo "</tbody>";

							echo "		</table>
									</td>
								</tr>";

							$no++;
						}
					}
				}
			?>						
		</tbody>
	</table>

	<div id="chartdiv" style="width: 100%; height: 500px"></div>

	<!-- Resources -->
	<script src="<?= base_url();?>assets/chart_main/amchart_multi_line/core.js"></script>
	<script src="<?= base_url();?>assets/chart_main/amchart_multi_line/charts.js"></script>
	<script src="<?= base_url();?>assets/chart_main/amchart_multi_line/animated.js"></script>
	
	<script type="text/javascript">
		var json_data = JSON.parse('<?php print_r($json_data); ?>');
		var json_sub = [];
		console.log(json_data);

		$(document).ready(function(){
			$(".sub_jenis_out").hide();
		});

		$(".jenis_click").click(function(){
			var id_jenis_html = $(this).attr("id");

			var id_jenis = id_jenis_html.split("_")[1];

			console.log(id_jenis);
			// console.log(data_main);

			var id_tr_output = "tmp_"+id_jenis;
			var output_data = "output_"+id_jenis;

			if($("#"+id_tr_output).is(":visible")){
				$("#"+id_tr_output).hide(500);
			}else{
				$(".sub_jenis_out").hide();
				$("#"+id_tr_output).show(500);
				json_sub = json_data[id_jenis];
			}
			
		});

		$(".sub_jenis").click(function(){
			var id_html_sub = $(this).attr("id");
			var id_sub_jenis = id_html_sub.split("_")[1];

			var obj_val = json_sub.sub_jenis[id_sub_jenis]["data_graph"];
			var caption = json_sub.sub_jenis[id_sub_jenis]["nama_sub_jenis"]

			create_graph(obj_val, caption);

			// console.log(id_sub_jenis);
			// console.log(json_sub.sub_jenis[id_sub_jenis]["data_graph"]);
		});

		function create_graph(obj_val, caption){
			am4core.useTheme(am4themes_animated);
			// Themes end

			// Create chart instance
			var chart = am4core.create("chartdiv", am4charts.XYChart);

			// Add data
			chart.data = obj_val;

			// Create category axis
			var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
			categoryAxis.dataFields.category = "year";
			categoryAxis.renderer.opposite = true;

			// Create value axis
			var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
			valueAxis.renderer.inversed = true;
			valueAxis.title.text = caption;
			valueAxis.renderer.minLabelPosition = 0.01;

			// Create series
			var series1 = chart.series.push(new am4charts.LineSeries());
			series1.dataFields.valueY = "val_jml";
			series1.dataFields.categoryX = "year";
			series1.name = "Jumlah";
			series1.strokeWidth = 3;
			series1.bullets.push(new am4charts.CircleBullet());
			series1.tooltipText = caption+" {name} pada {categoryX}: {valueY}";
			series1.legendSettings.valueText = "{valueY}";
			series1.visible  = false;

			

			// Add chart cursor
			chart.cursor = new am4charts.XYCursor();
			chart.cursor.behavior = "zoomY";

			// Add legend
			chart.legend = new am4charts.Legend();
		}
	</script>

</body>
</html>
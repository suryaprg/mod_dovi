
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><i class="fa fa-clipboard"></i> KATEGORI</li>
        
       
        

      
        <li class="treeview" >
          <a href="#">
            <i class="fa fa-money"></i> <span>Ekonomi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'keuangan_daerah') echo "class='active'";?>><a href="<?=base_url()."admin/keuangan_daerah";?>"><i class="fa fa-circle-o"></i>Keuangan Daerah</a></li>
            <li <?php if($this->uri->segment(2) == 'pendapatan_regional') echo "class='active'";?>><a href="<?=base_url()."admin/pendapatan_regional";?>"><i class="fa fa-circle-o"></i>Pendapatan Regional</a></li>
            <li <?php if($this->uri->segment(2) == 'kegiatan_usaha') echo "class='active'";?>><a href="<?=base_url()."admin/kegiatan_usaha";?>"><i class="fa fa-circle-o"></i>Kegiatan Usaha</a></li>
            <li <?php if($this->uri->segment(2) == 'pengeluaran_penduduk') echo "class='active'";?>><a href="<?=base_url()."admin/pengeluaran_penduduk";?>"><i class="fa fa-circle-o"></i>Pengeluaran Penduduk</a></li>
            <li <?php if($this->uri->segment(2) == 'pengeluaran_rumah_tangga') echo "class='active'";?>><a href="<?=base_url()."admin/pengeluaran_rumah_tangga";?>"><i class="fa fa-circle-o"></i>Pengeluaran Rumah Tangga</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-building"></i> <span>Ketenagakerjaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'kerja_angkatan') echo "class='active'";?>><a href="<?=base_url()."admin/kerja_angkatan";?>"><i class="fa fa-circle-o"></i>Angkatan Kerja</a></li>
            <li <?php if($this->uri->segment(2) == 'kerja_pengangguran') echo "class='active'";?>><a href="<?=base_url()."admin/kerja_pengangguran";?>"><i class="fa fa-circle-o"></i>Pengangguran</a></li>
            <li <?php if($this->uri->segment(2) == 'kerja_ump') echo "class='active'";?>><a href="<?=base_url()."admin/kerja_ump";?>"><i class="fa fa-circle-o"></i>Upah</a></li>
          </ul>
        </li>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Kependudukan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'kependudukan_jk') echo "class='active'";?>><a href="<?=base_url()."admin/kependudukan_jk";?>"><i class="fa fa-circle-o"></i>Berdasarkan Jenis Kelamin</a></li>
            <li <?php if($this->uri->segment(2) == 'kependudukan_umur') echo "class='active'";?>><a href="<?=base_url()."admin/kependudukan_umur";?>"><i class="fa fa-circle-o"></i>Berdasarkan Umur</a></li>
            <li <?php if($this->uri->segment(2) == 'kependudukan_ketergantungan') echo "class='active'";?>><a href="<?=base_url()."admin/kependudukan_ketergantungan";?>"><i class="fa fa-circle-o"></i>Index Ketergantungan</a></li>
            <li <?php if($this->uri->segment(2) == 'kependudukan_ipm') echo "class='active'";?>><a href="<?=base_url()."admin/kependudukan_ipm";?>"><i class="fa fa-circle-o"></i>IPM</a></li>
            <li <?php if($this->uri->segment(2) == 'kependudukan_ipm_bid') echo "class='active'";?>><a href="<?=base_url()."admin/kependudukan_ipm_bid";?>"><i class="fa fa-circle-o"></i>Pengembangan IPM</a></li>
             <li <?php if($this->uri->segment(2) == 'penduduk_miskin') echo "class='active'";?>><a href="<?=base_url()."admin/penduduk_miskin";?>"><i class="fa fa-circle-o"></i>Jumlah Penduduk Miskin</a></li>
            <li <?php if($this->uri->segment(2) == 'garis_miskin') echo "class='active'";?>><a href="<?=base_url()."admin/garis_miskin";?>"><i class="fa fa-circle-o"></i>Garis Kemiskinan</a></li>
          </ul>
        </li>
            <li class="treeview">
          <a href="#">
            <i class="fa fa-plus"></i> <span>Kesehatan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'kesehatan_gizi') echo "class='active'";?>><a href="<?=base_url()."admin/kesehatan_gizi";?>"><i class="fa fa-circle-o"></i>Gizi Balita</a></li>
            <li <?php if($this->uri->segment(2) == 'kesehatan_penyakit') echo "class='active'";?>><a href="<?=base_url()."admin/kesehatan_penyakit";?>"><i class="fa fa-circle-o"></i>Penyakit</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-tv"></i> <span>Komunikasi dan Informasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'tehno_bts') echo "class='active'";?>><a href="<?=base_url()."admin/tehno_bts";?>"><i class="fa fa-circle-o"></i>Jumlah BTS</a></li>
            <li <?php if($this->uri->segment(2) == 'tehno_warnet') echo "class='active'";?>><a href="<?=base_url()."admin/tehno_warnet";?>"><i class="fa fa-circle-o"></i>Jumlah Warnet</a></li>
            <li <?php if($this->uri->segment(2) == 'tehno_web') echo "class='active'";?>><a href="<?=base_url()."admin/tehno_web";?>"><i class="fa fa-circle-o"></i>Jumlah Website OPD</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-map"></i> <span>Lingkungan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'iklims') echo "class='active'";?>><a href="<?=base_url()."admin/iklim";?>"><i class="fa fa-circle-o"></i>Data Iklim dan Curah Hujan</a></li>
            <li <?php if($this->uri->segment(2) == 'kelkec') echo "class='active'";?>><a href="<?=base_url()."super/kelkec";?>"><i class="fa fa-circle-o"></i>Data Kecamatan dan Kelurahan</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-briefcase"></i> <span>Politik Hukum dan Pemerintahan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'rt_rw') echo "class='active'";?>><a href="<?=base_url()."admin/rt_rw";?>"><i class="fa fa-circle-o"></i>RT dan RW</a></li>
             <li <?php if($this->uri->segment(2) == 'partai') echo "class='active'";?>><a href="<?=base_url()."super/partai";?>"><i class="fa fa-circle-o"></i>Data Partai</a></li>
          </ul>
        </li>
 <li class="treeview">
          <a href="#">
            <i class="fa fa-graduation-cap"></i> <span>Pendidikan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'pendidikan_baca_tulis') echo "class='active'";?>><a href="<?=base_url()."admin/pendidikan_baca_tulis";?>"><i class="fa fa-circle-o"></i>Baca dan Tulis</a></li>
            <li <?php if($this->uri->segment(2) == 'pendidikan_partisipasi_sklh') echo "class='active'";?>><a href="<?=base_url()."admin/pendidikan_partisipasi_sklh";?>"><i class="fa fa-circle-o"></i>Partisipasi Sekolah</a></li>
            <li <?php if($this->uri->segment(2) == 'pendidikan_penduduk') echo "class='active'";?>><a href="<?=base_url()."admin/pendidikan_penduduk";?>"><i class="fa fa-circle-o"></i>Pendidikan Penduduk</a></li>
            <li <?php if($this->uri->segment(2) == 'jumlah_sekolah') echo "class='active'";?>><a href="<?=base_url()."admin/jumlah_sekolah";?>"><i class="fa fa-circle-o"></i>Jumlah Sekolah</a></li>
            <li <?php if($this->uri->segment(2) == 'rasio_guru_murid') echo "class='active'";?>><a href="<?=base_url()."admin/rasio_guru_murid";?>"><i class="fa fa-circle-o"></i>Rasio Guru dan Murid</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-car"></i> <span>Perhubungan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'trans_kendaraan') echo "class='active'";?>><a href="<?=base_url()."admin/trans_kendaraan";?>"><i class="fa fa-circle-o"></i>Kendaraan</a></li>
            <li <?php if($this->uri->segment(2) == 'trans_jalan') echo "class='active'";?>><a href="<?=base_url()."admin/trans_jalan";?>"><i class="fa fa-circle-o"></i>Jalan</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-leaf"></i> <span>Pertanian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'pertanian_lahan') echo "class='active'";?>><a href="<?=base_url()."admin/pertanian_lahan";?>"><i class="fa fa-circle-o"></i>Lahan Berdasarkan Jenis</a></li>
            <li <?php if($this->uri->segment(2) == 'pertanian_lahan_penggunaan') echo "class='active'";?>><a href="<?=base_url()."admin/pertanian_lahan_penggunaan";?>"><i class="fa fa-circle-o"></i>Lahan Berdasarkan Penggunaan</a></li>
            <li <?php if($this->uri->segment(2) == 'kelapa_tebu') echo "class='active'";?>><a href="<?=base_url()."admin/kelapa_tebu";?>"><i class="fa fa-circle-o"></i>Kelapa dan Tebu</a></li>
            <li <?php if($this->uri->segment(2) == 'pertanian_komoditi') echo "class='active'";?>><a href="<?=base_url()."admin/pertanian_komoditi";?>"><i class="fa fa-circle-o"></i>Komoditi</a></li>
            <li <?php if($this->uri->segment(2) == 'pertanian_hortikultura') echo "class='active'";?>><a href="<?=base_url()."admin/pertanian_hortikultura";?>"><i class="fa fa-circle-o"></i>Hortikultura</a></li>
            <li <?php if($this->uri->segment(2) == 'peternakan') echo "class='active'";?>><a href="<?=base_url()."admin/peternakan";?>"><i class="fa fa-circle-o"></i>Peternakan</a></li>
            <li <?php if($this->uri->segment(2) == 'perikanan') echo "class='active'";?>><a href="<?=base_url()."admin/perikanan";?>"><i class="fa fa-circle-o"></i>Perikanan</a></li>
          </ul>
        </li>
      </ul>

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><i class="fa fa-list-alt"></i> LAMPIRAN</li>
   <li class="treeview" >
          <a href="#">
            <i class="fa fa-money"></i> <span>Ekonomi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li <?php if($this->uri->segment(2) == 'industri') echo "class='active'";?>><a href="<?=base_url()."admin/industri";?>"><i class="fa fa-circle-o"></i>Data Pendapatan Industri</a></li>
             <li <?php if($this->uri->segment(2) == 'koperasi') echo "class='active'";?>><a href="<?=base_url()."admin/koperasi";?>"><i class="fa fa-circle-o"></i>Data Koperasi</a></li>
             <li <?php if($this->uri->segment(2) == 'keuangan') echo "class='active'";?>><a href="<?=base_url()."admin/keuangan";?>"><i class="fa fa-circle-o"></i>Data Keuangan</a></li>
             <li <?php if($this->uri->segment(2) == 'tenaga') echo "class='active'";?>><a href="<?=base_url()."admin/tenaga";?>"><i class="fa fa-circle-o"></i>Data LP Tenaga</a></li>
               <li <?php if($this->uri->segment(2) == 'tpa') echo "class='active'";?>><a href="<?=base_url()."admin/perdagangan";?>"><i class="fa fa-circle-o"></i>Data Perdagangan</a></li>
               <li <?php if($this->uri->segment(2) == 'pajak') echo "class='active'";?>><a href="<?=base_url()."admin/pajak";?>"><i class="fa fa-circle-o"></i>Pajak</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-building"></i> <span>Ketenagakerjaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li <?php if($this->uri->segment(3) == 'disnaker') echo "class='active'";?>><a href="<?=base_url()."admin/disnaker";?>"><i class="fa fa-circle-o"></i>Disnaker</a></li>
          </ul>
        </li>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Kependudukan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'umur') echo "class='active'";?>><a href="<?=base_url()."admin/umur";?>"><i class="fa fa-circle-o"></i>Data LP Kel Umur</a></li>
             <li <?php if($this->uri->segment(2) == 'rasio') echo "class='active'";?>><a href="<?=base_url()."admin/rasio";?>"><i class="fa fa-circle-o"></i>Data Gender</a></li>
              <li <?php if($this->uri->segment(2) == 'miskin') echo "class='active'";?>><a href="<?=base_url()."admin/miskin";?>"><i class="fa fa-circle-o"></i>Data Miskin</a></li>
          </ul>
        </li>
            <li class="treeview">
          <a href="#">
            <i class="fa fa-plus"></i> <span>Kesehatan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li <?php if($this->uri->segment(2) == 'kesehatan') echo "class='active'";?>><a href="<?=base_url()."admin/kesehatan";?>"><i class="fa fa-circle-o"></i>Data Kesehatan</a></li>
            <li <?php if($this->uri->segment(3) == 'jenis') echo "class='active'";?>><a href="<?=base_url()."admin/kesehatan/jenis";?>"><i class="fa fa-circle-o"></i>Data Kesehatan Jenis</a></li>
            <li <?php if($this->uri->segment(3) == 'sub') echo "class='active'";?>><a href="<?=base_url()."admin/kesehatan/sub";?>"><i class="fa fa-circle-o"></i>Data Kesehatan Sub Jenis</a></li>
            
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-tv"></i> <span>Komunikasi dan Informasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'aplikasi') echo "class='active'";?>><a href="<?=base_url()."admin/aplikasi";?>"><i class="fa fa-circle-o"></i>Data Aplikasi</a></li>
              <li <?php if($this->uri->segment(2) == 'domain') echo "class='active'";?>><a href="<?=base_url()."admin/domain";?>"><i class="fa fa-circle-o"></i>Data Domain</a></li>
               <li <?php if($this->uri->segment(2) == 'tehnologi') echo "class='active'";?>><a href="<?=base_url()."admin/tehnologi";?>"><i class="fa fa-circle-o"></i>Data Tehnologi</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-map"></i> <span>Lingkungan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
               <li <?php if($this->uri->segment(2) == 'bencana') echo "class='active'";?>><a href="<?=base_url()."admin/bencana";?>"><i class="fa fa-circle-o"></i>Data LP Bencana</a></li>
                 <li <?php if($this->uri->segment(2) == 'tpa') echo "class='active'";?>><a href="<?=base_url()."admin/tpa";?>"><i class="fa fa-circle-o"></i>Data LP TPA</a></li>
               
                 <li <?php if($this->uri->segment(2) == 'csr') echo "class='active'";?>><a href="<?=base_url()."admin/csr";?>"><i class="fa fa-circle-o"></i>Data CSR</a></li>
                 <li <?php if($this->uri->segment(2) == 'dsu') echo "class='active'";?>><a href="<?=base_url()."admin/dsu";?>"><i class="fa fa-circle-o"></i>Data PSU</a></li>
                 <li <?php if($this->uri->segment(2) == 'taman') echo "class='active'";?>><a href="<?=base_url()."admin/taman";?>"><i class="fa fa-circle-o"></i>Data Taman</a></li>
                 <li <?php if($this->uri->segment(2) == 'pohon') echo "class='active'";?>><a href="<?=base_url()."admin/pohon";?>"><i class="fa fa-circle-o"></i>Data Pohon</a></li>
                  <li <?php if($this->uri->segment(2) == 'disperkim') echo "class='active'";?>><a href="<?=base_url()."admin/disperkim/all";?>"><i class="fa fa-circle-o"></i>Data Disperkim</a></li>
          </ul>
        </li>
           <li class="treeview">
          <a href="#">
            <i class="fa fa-plane"></i> <span>Pariwisata dan Kebudayaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li <?php if($this->uri->segment(2) == 'dts') echo "class='active'";?>><a href="<?=base_url()."admin/dts";?>"><i class="fa fa-circle-o"></i>Daya Tarik Strategis</a></li>
            <li <?php if($this->uri->segment(2) == 'dtw') echo "class='active'";?>><a href="<?=base_url()."admin/dtw";?>"><i class="fa fa-circle-o"></i>Daya Tarik Wisatawan</a></li>
            <li <?php if($this->uri->segment(2) == 'dth') echo "class='active'";?>><a href="<?=base_url()."admin/dth";?>"><i class="fa fa-circle-o"></i>Data Pengunjung Hotel</a></li>
               <li <?php if($this->uri->segment(2) == 'zona_kreatif') echo "class='active'";?>><a href="<?=base_url()."admin/zona_kreatif";?>"><i class="fa fa-circle-o"></i>Data Zona Kreatif</a></li>
                  <li <?php if($this->uri->segment(2) == 'obj') echo "class='active'";?>><a href="<?=base_url()."admin/obj";?>"><i class="fa fa-circle-o"></i>Data Objek Budaya</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-briefcase"></i> <span>Politik Hukum dan Pemerintahan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          
           <li <?php if($this->uri->segment(2) == 'aparat') echo "class='active'";?>><a href="<?=base_url()."admin/aparat";?>"><i class="fa fa-circle-o"></i>Data LP Aparat</a></li>
           <li <?php if($this->uri->segment(2) == 'pidana') echo "class='active'";?>><a href="<?=base_url()."admin/pidana";?>"><i class="fa fa-circle-o"></i>Data LP Pidana</a></li>
             <li <?php if($this->uri->segment(2) == 'dpmptsp') echo "class='active'";?>><a href="<?=base_url()."admin/dpmptsp";?>"><i class="fa fa-circle-o"></i>Data DPMPTSP</a></li>
              <li <?php if($this->uri->segment(3) == 'pmks') echo "class='active'";?>><a href="<?=base_url()."admin/pmks";?>"><i class="fa fa-circle-o"></i>PMKS</a></li>
          </ul>     
         </li>
         <li class="treeview active">
          <a href="#">
            <i class="fa fa-graduation-cap"></i> <span>Pendidikan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           <li><a href="<?=base_url()."admin/pendidikan";?>"><i class="fa fa-circle-o"></i>Data Pendidikan</a></li>
           <li><a href="<?=base_url()."admin/perpustakaan";?>"><i class="fa fa-circle-o"></i>Data Perpustakaan</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-car"></i> <span>Perhubungan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'kendaraan') echo "class='active'";?>><a href="<?=base_url()."admin/kendaraan";?>"><i class="fa fa-circle-o"></i>Data Kendaraan Kecamatan</a></li>
            <li <?php if($this->uri->segment(3) == 'plat') echo "class='active'";?>><a href="<?=base_url()."admin/kendaraan/plat";?>"><i class="fa fa-circle-o"></i>Data Kendaraan Plat</a></li>
            <li <?php if($this->uri->segment(3) == 'umum') echo "class='active'";?>><a href="<?=base_url()."admin/kendaraan/umum";?>"><i class="fa fa-circle-o"></i>Data Kendaraan Umum</a></li>
             <li <?php if($this->uri->segment(2) == 'terminal') echo "class='active'";?>><a href="<?=base_url()."admin/terminal";?>"><i class="fa fa-circle-o"></i>Data LP Terminal</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-circle"></i> <span>Olahraga</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'olahraga') echo "class='active'";?>><a href="<?=base_url()."admin/or";?>"><i class="fa fa-circle-o"></i>Olahraga</a></li>
            
          </ul>
        </li>


        <li class="treeview">
          <a href="#">
            <i class="fa fa-leaf"></i> <span>Pertanian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li <?php if($this->uri->segment(2) == 'pertanian') echo "class='active'";?>><a href="<?=base_url()."admin/pertanian";?>"><i class="fa fa-circle-o"></i>Data Pertanian</a></li>
             <li <?php if($this->uri->segment(2) == 'lahan') echo "class='active'";?>><a href="<?=base_url()."admin/lahan";?>"><i class="fa fa-circle-o"></i>Data Lahan</a></li>
              <li <?php if($this->uri->segment(2) == 'panen') echo "class='active'";?>><a href="<?=base_url()."admin/panen";?>"><i class="fa fa-circle-o"></i>Data Panen</a></li>
          
            <!-- <li <?php if($this->uri->segment(2) == 'pertanian_lahan') echo "class='active'";?>><a href="<?=base_url()."admin/pertanian_lahan";?>"><i class="fa fa-circle-o"></i>Lahan Berdasarkan Jenis</a></li>
            <li <?php if($this->uri->segment(2) == 'pertanian_lahan_penggunaan') echo "class='active'";?>><a href="<?=base_url()."admin/pertanian_lahan_penggunaan";?>"><i class="fa fa-circle-o"></i>Lahan Berdasarkan Penggunaan</a></li>
            <li <?php if($this->uri->segment(2) == 'kelapa_tebu') echo "class='active'";?>><a href="<?=base_url()."admin/kelapa_tebu";?>"><i class="fa fa-circle-o"></i>Kelapa dan Tebu</a></li>
            <li <?php if($this->uri->segment(2) == 'pertanian_komoditi') echo "class='active'";?>><a href="<?=base_url()."admin/pertanian_komoditi";?>"><i class="fa fa-circle-o"></i>Komoditi</a></li>
            <li <?php if($this->uri->segment(2) == 'pertanian_hortikultura') echo "class='active'";?>><a href="<?=base_url()."admin/pertanian_hortikultura";?>"><i class="fa fa-circle-o"></i>Hortikultura</a></li>
            <li <?php if($this->uri->segment(2) == 'peternakan') echo "class='active'";?>><a href="<?=base_url()."admin/peternakan";?>"><i class="fa fa-circle-o"></i>Peternakan</a></li>
            <li <?php if($this->uri->segment(2) == 'perikanan') echo "class='active'";?>><a href="<?=base_url()."admin/perikanan";?>"><i class="fa fa-circle-o"></i>Perikanan</a></li> -->
          </ul>
        </li>

        

     
     

     
          

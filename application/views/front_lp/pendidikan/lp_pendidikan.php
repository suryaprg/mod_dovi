	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Pendidikan</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 0.5%">No</th>
										<th class="text-center" style="width: 2%">Jenis</th>
										<th class="text-center" style="width: 5%">Kategori</th>
										<th class="text-center" style="width: 0.5%">Kecamatan</th>
										<th class="text-center" style="width: 0.5%">Tahun</th>
										<th class="text-center" style="width: 0.5%">Negeri</th>
										<th class="text-center" style="width: 0.5%">Swasta</th>
										<th class="text-center" style="width: 0.5%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lppendidikan/insert_lp_pendidikan/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control">
													<option value="">- Jenis -</option>
													<?php foreach ($list_pendidikan_jenis as $lpj) { ?>
														<option value="<?php echo $lpj->id_jenis ?>"><?php echo $lpj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td>
												<select name="id_kategori" class="form-control">
													<option value="">- Kategori -</option>
													<?php foreach ($list_pendidikan_kategori as $lpk) { ?>
														<option value="<?php echo $lpk->id_kategori ?>"><?php echo $lpk->nama_kategori ?></option>
													<?php } ?>
												</select>
											</td>
											<td>
												<select name="id_kec" class="form-control">
													<option value="">- Kecamatan -</option>
													<?php foreach ($list_kecamatan as $lk) { ?>
														<option value="<?php echo $lk->id_kec ?>"><?php echo $lk->nama_kec ?></option>
													<?php } ?>
												</select>
											</td>											
											<td><input type="text" name="th" class="form-control"></td>
											<td><input type="text" name="jml_negeri" class="form-control"></td>
											<td><input type="text" name="jml_swasta" class="form-control"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_pendidikan as $lp) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lp->nama_jenis ?></td>
											<td align="left"><?php echo $lp->nama_kategori ?></td>
											<td align="left"><?php echo $lp->nama_kec ?></td>
											<td align="center"><?php echo $lp->th ?></td>
											<td align="center"><?php echo $lp->jml_negeri ?></td>
											<td align="center"><?php echo $lp->jml_swasta ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_pend(<?php echo $lp->id_pendidikan ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_pend(<?php echo $lp->id_pendidikan ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-6">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Jenis Pendidikan</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped " id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 8%">Nama</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lppendidikan/insert_lp_pendidikan_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_pendidikan_jenis as $lpj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lpj->nama_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_pend_jenis(<?php echo $lpj->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_pend_jenis(<?php echo $lpj->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kategori Pendidikan</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 8%">Nama</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lppendidikan/insert_lp_pendidikan_kategori/" ?>" method="post">									
											<td><input type="text" name="nama_kategori" class="form-control" id="nama_kategori"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_pendidikan_kategori as $lpk) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lpk->nama_kategori ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_pend_kategori(<?php echo $lpk->id_kategori ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_pend_kategori(<?php echo $lpk->id_kategori ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>				

			</div>
		</section>
	</div>

	<script type="text/javascript">
//======================================================== LP PENDIDIKAN ===========================================//
	  	function up_lp_pend(id_pendidikan){
	    	clear_mod_up_lp_pend();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_pendidikan', id_pendidikan);    
		      $.ajax({
		        url: "<?php echo base_url()."/front_lp/Lppendidikan/index_up_lp_pendidikan/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_pend(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_pendidikan").modal('show');
	  	} 

	  	function clear_mod_up_lp_pend(){
	      	$("#_id_jenis").val("");
	      	$("#_id_kategori").val("");
	      	$("#_id_kec").val("");
	      	$("#_th").val("");
	      	$("#_jml_negeri").val("");
	      	$("#_jml_swasta").val("");
	  	}

	  	function res_update_lp_pend(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_pendidikan").attr('readonly','readonly');                                        
	          	$("#_id_pendidikan").val(main_data.id_pendidikan);
	          	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_id_kategori").val(main_data.id_kategori);
	          	$("#_id_kec").val(main_data.id_kec);
	          	$("#_th").val(main_data.th);
	          	$("#_jml_negeri").val(main_data.jml_negeri);
	          	$("#_jml_swasta").val(main_data.jml_swasta);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_pend(id_pendidikan){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_pendidikan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_pendidikan+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lppendidikan/delete_lp_pendidikan/";?>"+id_pendidikan;
	      	}else{

	      	}
	  	}      		
//======================================================== LP PENDIDIKAN ===========================================//

//======================================================== LP PENDIDIKAN JENIS =====================================//
	  	function up_lp_pend_jenis(id_jenis){
	    	clear_mod_up_lp_pend_jenis();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_jenis', id_jenis);    
		      $.ajax({
		        url: "<?php echo base_url()."/front_lp/Lppendidikan/index_up_lp_pendidikan_jenis/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_pend_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_pendidikan_jenis").modal('show');
	  	} 

	  	function clear_mod_up_lp_pend_jenis(){
	      	$("#_nama_jenis").val("");
	  	}

	  	function res_update_lp_pend_jenis(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_jenis2").attr('readonly','readonly');                                        
	          	$("#_id_jenis2").val(main_data.id_jenis);
	          	$("#_nama_jenis").val(main_data.nama_jenis);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_pend_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lppendidikan/delete_lp_pendidikan_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}      		
//======================================================== LP PENDIDIKAN JENIS =====================================//

//======================================================== LP PENDIDIKAN KATEGORI ==================================//
	  	function up_lp_pend_kategori(id_kategori){
	    	clear_mod_up_lp_pend_kategori();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_kategori', id_kategori);    
		      $.ajax({
		        url: "<?php echo base_url()."/front_lp/Lppendidikan/index_up_lp_pendidikan_kategori/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_pend_kategori(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_pendidikan_kategori").modal('show');
	  	} 

	  	function clear_mod_up_lp_pend_kategori(){
	      	$("#_nama_jenis").val("");
	  	}

	  	function res_update_lp_pend_kategori(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_kategori2").attr('readonly','readonly');                                        
	          	$("#_id_kategori2").val(main_data.id_kategori);
	          	$("#_nama_kategori").val(main_data.nama_kategori);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_pend_kategori(id_kategori){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_kategori+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kategori+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lppendidikan/delete_lp_pendidikan_kategori/";?>"+id_kategori;
	      	}else{

	      	}
	  	}      		
//======================================================== LP PENDIDIKAN KATEGORI ==================================//		
	</script>

	<div class="modal fade" id="modal_up_lp_pendidikan">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data LP Pendidikan</h4>
	      	</div>
	      	<?php echo form_open("front_lp/lppendidikan/update_lp_pendidikan"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID Pendidikan</label>
	                        	<input type="number" name="id_pendidikan" id="_id_pendidikan" class="form-control">
	                      	</div>                      
	                      
	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jenis</label>
	                        	<select name="id_jenis" class="form-control" id="_id_jenis">
	                          		<option value="">-- Select Jenis --</option>
	                          		<?php foreach ($list_pendidikan_jenis as $jenis) { ?>
                            		<option value="<?php echo $jenis->id_jenis ?>"><?php echo $jenis->nama_jenis ?></option>
	                          		<?php } ?>
                        		</select>
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Kategori</label>
								<select name="id_kategori" class="form-control" id="_id_kategori">
									<option value="">-- Pilih Kategori --</option>
									<?php foreach ($list_pendidikan_kategori as $lpk) { ?>
										<option value="<?php echo $lpk->id_kategori ?>"><?php echo $lpk->nama_kategori ?></option>
									<?php } ?>
								</select>	                      	
	                      	</div>	                      	

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Kecamatan</label>
								<select name="id_kec" class="form-control" id="_id_kec">
									<option value="">-- Pilih Kecamatan --</option>
									<?php foreach ($list_kecamatan as $lk) { ?>
										<option value="<?php echo $lk->id_kec ?>"><?php echo $lk->nama_kec ?></option>
									<?php } ?>
								</select>	                      	
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Tahun</label>
	                        	<input type="number" name="th" class="form-control" id="_th" placeholder="Tahun">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jumlah Negeri</label>
	                        	<input type="number" name="jml_negeri" class="form-control" id="_jml_negeri" placeholder="Jumlah Negeri">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jumlah Swasta</label>
	                        	<input type="number" name="jml_swasta" class="form-control" id="_jml_swasta" placeholder="Jumlah Swasta">
	                      	</div>	                      	

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>

	<div class="modal fade" id="modal_up_lp_pendidikan_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data LP Jenis Pendidikan</h4>
	      	</div>
	      	<?php echo form_open("front_lp/lppendidikan/update_lp_pendidikan_jenis"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID Jenis</label>
	                        	<input type="number" name="id_jenis" id="_id_jenis2" class="form-control">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Nama</label>
	                        	<input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Jenis">
	                      	</div>                      	

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>

	<div class="modal fade" id="modal_up_lp_pendidikan_kategori">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data LP Kategori Pendidikan</h4>
	      	</div>
	      	<?php echo form_open("front_lp/lppendidikan/update_lp_pendidikan_kategori"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID Kategori</label>
	                        	<input type="number" name="id_kategori" id="_id_kategori2" class="form-control">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Nama</label>
	                        	<input type="text" name="nama_kategori" class="form-control" id="_nama_kategori" placeholder="Kategori">
	                      	</div>                      	

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>	
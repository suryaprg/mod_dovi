	<section class="content-header">
		
	</section>

	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kajian Penelitian</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="kajian">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 4.5%">Kajian</th>
										<th class="text-center" style="width: 1.5%">Tahun</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkajianpenelitian/insert_lp_kajian/" ?>" method="post">
											<td><input type="text" name="nama_kajian" class="form-control" id="nama_kajian"></td>
											<td><input type="number" name="th" class="form-control" id="th"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_kajian as $lk) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="justify"><?php echo $lk->nama_kajian ?></td>
											<td align="center"><?php echo $lk->th ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_kajian(<?php echo $lk->id_kajian ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_kajian(<?php echo $lk->id_kajian ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
		</section>
	</div>

	<script type="text/javascript">
	  	function up_lp_kajian(id_kajian){
	    	clear_mod_up_lp_kajian();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_kajian', id_kajian);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/Lpkajianpenelitian/index_up_lp_kajian/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_kajian(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_kajian").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_kajian(){
	      	$("#_nama_kajian").val("");
	      	$("#_th").val("");
	  	}

  		function res_update_lp_kajian(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_kajian").attr('readonly','readonly');                                        
	        	$("#_id_kajian").val(main_data.id_kajian);
	          	$("#_nama_kajian").val(main_data.nama_kajian);
	          	$("#_th").val(main_data.th);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_kajian(id_kajian){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_kajian+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kajian+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkajianpenelitian/delete_lp_kajian/";?>"+id_kajian;
	      	}else{

	      	}
	  	}		
	</script>

	<div class="modal fade" id="modal_up_lp_kajian">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kajian Penelitian</h4>
	      		</div>
	      		
	      		<?php echo form_open("front_lp/lpkajianpenelitian/update_lp_kajian"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Kajian</label>
	                        				<input type="number" class="form-control" id="_id_kajian" name="id_kajian" placeholder="ID Bencana">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama Kajian</label>
	                        				<input type="text" class="form-control" id="_nama_kajian" name="nama_kajian" placeholder="Nama Kajian">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Tahun</label>
	                        				<input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
	                      				</div>

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>	
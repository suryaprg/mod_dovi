	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Perpustakaan</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 6%">Jenis</th>
										<th class="text-center" style="width: 1%">Tahun</th>
										<th class="text-center" style="width: 1%">Jumlah</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpperpustakaan/insert_lp_perpus/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control">
													<option value="">- Jenis -</option>
													<?php foreach ($list_perpus_jenis as $lpj) { ?>
														<option value="<?php echo $lpj->id_jenis ?>"><?php echo $lpj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>											
											<td><input type="text" name="th" class="form-control"></td>
											<td><input type="text" name="jml" class="form-control"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_perpus as $lp) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lp->nama_jenis ?></td>
											<td align="center"><?php echo $lp->th ?></td>
											<td align="center"><?php echo $lp->jml ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_perpus(<?php echo $lp->id_perpustakaan ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_perpus(<?php echo $lp->id_perpustakaan ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Jenis Perpustakaan</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped " id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 8%">Nama</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpperpustakaan/insert_lp_perpus_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_perpus_jenis as $lpj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lpj->nama_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_perpus_jenis(<?php echo $lpj->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_perpus_jenis(<?php echo $lpj->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>				

			</div>
		</section>
	</div>

	<script type="text/javascript">
//======================================================== LP APARAT =============================================//
	  	function up_lp_perpus(id_perpustakaan){
	    	clear_mod_up_lp_perpus();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_perpustakaan', id_perpustakaan);    
		      $.ajax({
		        url: "<?php echo base_url()."/front_lp/Lpperpustakaan/index_up_lp_perpus/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_perpus(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_perpus").modal('show');
	  	} 

	  	function clear_mod_up_lp_perpus(){
	      	$("#_id_jenis").val("");
	      	$("#_th").val("");
	      	$("#_jml").val("");
	  	}

	  	function res_update_lp_perpus(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_perpustakaan").attr('readonly','readonly');                                        
	          	$("#_id_perpustakaan").val(main_data.id_perpustakaan);
	          	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_th").val(main_data.th);
	          	$("#_jml").val(main_data.jml);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_perpus(id_perpustakaan){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_perpustakaan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_perpustakaan+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpperpustakaan/delete_lp_perpus/";?>"+id_perpustakaan;
	      	}else{

	      	}
	  	}      		
//======================================================== LP APARAT =============================================//

//======================================================== LP APARAT =============================================//
	  	function up_lp_perpus_jenis(id_jenis){
	    	clear_mod_up_lp_perpus_jenis();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_jenis', id_jenis);    
		      $.ajax({
		        url: "<?php echo base_url()."/front_lp/Lpperpustakaan/index_up_lp_perpus_jenis/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_perpus_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_perpus_jenis").modal('show');
	  	} 

	  	function clear_mod_up_lp_perpus_jenis(){
	      	$("#_nama_jenis").val("");
	  	}

	  	function res_update_lp_perpus_jenis(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_jenis2").attr('readonly','readonly');                                        
	          	$("#_id_jenis2").val(main_data.id_jenis);
	          	$("#_nama_jenis").val(main_data.nama_jenis);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_perpus_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpperpustakaan/delete_lp_perpus_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}      		
//======================================================== LP APARAT =============================================//		
	</script>

	<div class="modal fade" id="modal_up_lp_perpus">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data Lp Perpustakaan</h4>
	      	</div>
	      	<?php echo form_open("front_lp/lpperpustakaan/update_lp_perpus"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID Perpustakaan</label>
	                        	<input type="number" class="form-control" id="_id_perpustakaan" name="id_perpustakaan">
	                      	</div>                      
	                      
	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jenis</label>
	                        	<select name="id_jenis" class="form-control" id="_id_jenis">
	                          		<option value="">-- select id jenis --</option>
	                          		<?php foreach ($list_perpus_jenis as $jenis) { ?>
                            		<option value="<?php echo $jenis->id_jenis ?>"><?php echo $jenis->nama_jenis ?></option>
	                          		<?php } ?>
                        		</select>
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Tahun</label>
	                        	<input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jumlah</label>
	                        	<input type="number" class="form-control" id="_jml" name="jml" placeholder="Jumlah">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>	

	<div class="modal fade" id="modal_up_lp_perpus_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">&times;</span>
	          		</button>
	        		<h4 class="modal-title">Ubah Data Lp Perpustakaan Jenis</h4>
	      		</div>
	      		<?php echo form_open("front_lp/lpperpustakaan/update_lp_perpus_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
	                        				<input type="number" class="form-control" id="_id_jenis2" name="id_jenis" placeholder="ID Jenis">
	                      				</div>     

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama Jenis</label>
	                        				<input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Nama Jenis">
	                      				</div>

	                  					</div>
	                				</div>      
	            				</div>
	          				</div>
	        			</div>
	      			</div>
	      		<div class="modal-footer">
			        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
			        <button type="submit" class="btn btn-primary">Ubah Data</button>
	      		</div>
	      		</form>
	    	</div>
	  	</div>
	</div>			
</div>
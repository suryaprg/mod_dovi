	<section class="content-header">
		
	</section>

	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">

			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kendaraan Kecamatan</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center">No</th>
										<th class="text-center">Jenis</th>
										<th class="text-center">Kecamatan</th>
										<th class="text-center">Tahun</th>
										<th class="text-center">Jumlah</th>
										<th class="text-center">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkendaraan/insert_lp_kendaraan_kec/" ?>" method="post">
											<td>
												<select name="id_jenis_kendaraan" class="form-control" id="id_jenis_kendaraan">
													<option value="">Pilih</option>
													<?php foreach ($main_jenis_kendaraan as $mkj) { ?>
														<option value="<?php echo $mkj->id_jenis_kendaraan ?>"><?php echo $mkj->keterangan ?></option>
													<?php } ?>
												</select>
											</td>
											<td>
												<select name="id_kec" class="form-control" id="id_kec">
													<option value="">Pilih</option>
													<?php foreach ($main_kecamatan as $mk) { ?>
														<option value="<?php echo $mk->id_kec ?>"><?php echo $mk->nama_kec ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="number" name="th" class="form-control" id="th"></td>
											<td><input type="number" name="jml" class="form-control" id="jml"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_kendaraan_kec as $lkk) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="center"><?php echo $lkk->keterangan ?></td>
											<td align="center"><?php echo $lkk->nama_kec ?></td>
											<td align="center"><?php echo $lkk->th ?></td>
											<td align="center"><?php echo $lkk->jml ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_kendaraan(<?php echo $lkk->id_kendaraan ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_kendaraan(<?php echo $lkk->id_kendaraan ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

<!-- 			<div class="row">
				<div class="col-md-7">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kendaraan Umum</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped" id="umum">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 3%">Jenis</th>
										<th class="text-center" style="width: 3%">Jenis Kendaraan</th>
										<th class="text-center" style="width: 1%">Tahun</th>	
										<th class="text-center" style="width: 1%">Jumlah</th>	
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkendaraan/insert_lp_kendaraan_umum/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">Pilih</option>
													<?php foreach ($list_kendaraan_umum_jenis as $lkuj) { ?>
														<option value="<?php echo $lkuj->id_jenis ?>"><?php echo $lkuj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td>
												<select name="id_jenis_kendaraan" class="form-control" id="id_jenis_kendaraan">
													<option value="">Pilih</option>
													<?php foreach ($main_jenis_kendaraan as $mkj2) { ?>
														<option value="<?php echo $mkj2->id_jenis_kendaraan ?>"><?php echo $mkj2->keterangan ?></option>
													<?php } ?>
												</select>
											</td>											
											<td><input type="text" name="th" class="form-control" id="th"></td>
											<td><input type="text" name="jml" class="form-control" id="jml"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_kendaraan_umum as $lku) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lku->nama_jenis ?></td>
											<td align="center"><?php echo $lku->keterangan ?></td>
											<td align="center"><?php echo $lku->th ?></td>
											<td align="center"><?php echo $lku->jml ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_kendaraan_umum(<?php echo $lku->id_kendaraan ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_kendaraan_umum(<?php echo $lku->id_kendaraan ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="col-md-5">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kendaraan Umum Jenis</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped" id="umum_jenis">
								<thead>
									<tr>
										<th class="text-center">No</th>	
										<th class="text-center">Jenis</th>	
										<th class="text-center">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkendaraan/insert_lp_kendaraan_umum_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_kendaraan_umum_jenis as $lkuj2) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lkuj2->nama_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_kendaraan_umum_jenis(<?php echo $lkuj2->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_kendaraan_umum_jenis(<?php echo $lkuj2->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>				
			</div> -->			

		</section>
	</div>

	<script type="text/javascript">
//===================================================== LP KENDARAAN KEC =========================================//		
	  	function up_lp_kendaraan(id_kendaraan){
	    	clear_mod_up_lp_kendaraan();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_kendaraan', id_kendaraan);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/lpkendaraan/index_up_lp_kendaraan_kec/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_kendaraan(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_kendaraan").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_kendaraan(){
	      	$("#_id_jenis_kendaraan").val("");
	      	$("#_id_kec").val("");
	      	$("#_th").val("");
	      	$("#_jml").val("");
	  	}

  		function res_update_lp_kendaraan(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_kendaraan").attr('readonly','readonly');                                        
	        	$("#_id_kendaraan").val(main_data.id_kendaraan);
	          	$("#_id_jenis_kendaraan").val(main_data.id_jenis_kendaraan);
	          	$("#_id_kec").val(main_data.id_kec);
	          	$("#_th").val(main_data.th);
	          	$("#_jml").val(main_data.jml);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_kendaraan(id_kendaraan){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_kendaraan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kendaraan+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkendaraan/delete_lp_kendaraan_kec/";?>"+id_kendaraan;
	      	}else{

	      	}
	  	}
//===================================================== LP KENDARAAN KEC =========================================//	

//===================================================== LP KENDARAAN PLAT =========================================//		
// 	  	function up_lp_kendaraan_plat(id_kendaraan){
// 	    	clear_mod_up_lp_kendaraan_plat();
// 	    	// console.lo

// 	    	var data_main =  new FormData();
// 	    	data_main.append('id_kendaraan', id_kendaraan);    
// 	      	$.ajax({
// 	        	url: "<?php echo base_url()."/front_lp/lpkendaraan/index_up_lp_kendaraan_plat/";?>", // point to serv
// 	        	dataType: 'html',  // what to expect back from the PHP script, if anything
// 	        	cache: false,
// 		        contentType: false,
// 		        processData: false,
// 		        data: data_main,                         
// 		        type: 'post',
// 		        success: function(res){
// 		            console.log(res);
// 		            res_update_lp_kendaraan_plat(res);
// 		            // $("#out_up_mhs").html(res);
// 		        }
// 	    	});
// 	    	$("#modal_up_lp_kendaraan_plat").modal('show');
// 	  	}	  			

// 	  	function clear_mod_up_lp_kendaraan_plat(){
// 	      	$("#_id_jenis").val("");
// 	      	$("#_th").val("");
// 	      	$("#_jml_merah").val("");
// 	      	$("#_jml_kuning").val("");
// 	      	$("#_jml_hitam").val("");
// 	  	}

//   		function res_update_lp_kendaraan_plat(res){
//       		var data = JSON.parse(res);

//       		if(data.status){
//           		var main_data = data.val;
//           		console.log(main_data);
          
// 	       	   	$("#_id_kendaraan2").attr('readonly','readonly');                                        
// 	        	$("#_id_kendaraan2").val(main_data.id_kendaraan);
// 	          	$("#_id_jenis").val(main_data.id_jenis);
// 	          	$("#_th2").val(main_data.th);
// 	          	$("#_jml_merah").val(main_data.jml_merah);
// 	          	$("#_jml_kuning").val(main_data.jml_kuning);
// 	          	$("#_jml_hitam").val(main_data.jml_hitam);
//       		}else{
//           		clear_mod_up_ik();
//       		}
//   		}

// 	  	function del_lp_kendaraan_plat(id_kendaraan){
// 	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_kendaraan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kendaraan+" akan terhapus semua.. !!!!!!! ");
// 	      	if(conf){
// 	        	window.location.href = "<?= base_url()."front_lp/lpkendaraan/delete_lp_kendaraan_plat/";?>"+id_kendaraan;
// 	      	}else{

// 	      	}
// 	  	}
// //===================================================== LP KENDARAAN PLAT =========================================//

// //===================================================== LP KENDARAAN PLAT JENIS ======================================//	
// 	  	function up_lp_kendaraan_plat_jenis(id_jenis){
// 	    	clear_mod_up_lp_kendaraan_plat_jenis();
// 	    	// console.lo

// 	    	var data_main =  new FormData();
// 	    	data_main.append('id_jenis', id_jenis);    
// 	      	$.ajax({
// 	        	url: "<?php echo base_url()."/front_lp/lpkendaraan/index_up_lp_kendaraan_plat_jenis/";?>", // point to serv
// 	        	dataType: 'html',  // what to expect back from the PHP script, if anything
// 	        	cache: false,
// 		        contentType: false,
// 		        processData: false,
// 		        data: data_main,                         
// 		        type: 'post',
// 		        success: function(res){
// 		            console.log(res);
// 		            res_update_lp_kendaraan_plat_jenis(res);
// 		            // $("#out_up_mhs").html(res);
// 		        }
// 	    	});
// 	    	$("#modal_up_lp_kendaraan_plat_jenis").modal('show');
// 	  	}	  			

// 	  	function clear_mod_up_lp_kendaraan_plat_jenis(){
// 	      	$("#_nama_jenis").val("");
// 	  	}

//   		function res_update_lp_kendaraan_plat_jenis(res){
//       		var data = JSON.parse(res);

//       		if(data.status){
//           		var main_data = data.val;
//           		console.log(main_data);
          
// 	       	   	$("#_id_jenis2").attr('readonly','readonly');                                        
// 	        	$("#_id_jenis2").val(main_data.id_jenis);
// 	          	$("#_nama_jenis").val(main_data.nama_jenis);
//       		}else{
//           		clear_mod_up_ik();
//       		}
//   		}

// 	  	function del_lp_kendaraan_plat_jenis(id_jenis){
// 	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
// 	      	if(conf){
// 	        	window.location.href = "<?= base_url()."front_lp/lpkendaraan/delete_lp_kendaraan_plat_jenis/";?>"+id_jenis;
// 	      	}else{

// 	      	}
// 	  	}
// //===================================================== LP KENDARAAN PLAT JENIS ======================================//

// //===================================================== LP KENDARAAN UMUM ======================================//	
// 	  	function up_lp_kendaraan_umum(id_kendaraan){
// 	    	clear_mod_up_lp_kendaraan_umum();
// 	    	// console.lo

// 	    	var data_main =  new FormData();
// 	    	data_main.append('id_kendaraan', id_kendaraan);    
// 	      	$.ajax({
// 	        	url: "<?php echo base_url()."/front_lp/lpkendaraan/index_up_lp_kendaraan_umum/";?>", // point to serv
// 	        	dataType: 'html',  // what to expect back from the PHP script, if anything
// 	        	cache: false,
// 		        contentType: false,
// 		        processData: false,
// 		        data: data_main,                         
// 		        type: 'post',
// 		        success: function(res){
// 		            console.log(res);
// 		            res_update_lp_kendaraan_umum(res);
// 		            // $("#out_up_mhs").html(res);
// 		        }
// 	    	});
// 	    	$("#modal_up_lp_kendaraan_umum").modal('show');
// 	  	}	  			

// 	  	function clear_mod_up_lp_kendaraan_umum(){
// 	      	$("#_id_jenis3").val("");
// 	      	$("#_id_jenis_kendaraan3").val("");
// 	      	$("#_th3").val("");
// 	      	$("#_jml").val("");
// 	  	}

//   		function res_update_lp_kendaraan_umum(res){
//       		var data = JSON.parse(res);

//       		if(data.status){
//           		var main_data = data.val;
//           		console.log(main_data);
          
// 	       	   	$("#_id_kendaraan3").attr('readonly','readonly');                                        
// 	          	$("#_id_kendaraan3").val(main_data._id_kendaraan);
// 	          	$("#_id_jenis3").val(main_data.id_jenis);
// 	        	$("#_id_jenis_kendaraan3").val(main_data.id_jenis_kendaraan);
// 	          	$("#_th3").val(main_data.th);
// 	          	$("#_jml2").val(main_data.jml);
//       		}else{
//           		clear_mod_up_ik();
//       		}
//   		}

// 	  	function del_lp_kendaraan_umum(id_kendaraan){
// 	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_kendaraan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kendaraan+" akan terhapus semua.. !!!!!!! ");
// 	      	if(conf){
// 	        	window.location.href = "<?= base_url()."front_lp/lpkendaraan/delete_lp_kendaraan_umum/";?>"+id_kendaraan;
// 	      	}else{

// 	      	}
// 	  	}
// //===================================================== LP KENDARAAN UMUM ======================================//	

	</script>

	<div class="modal fade" id="modal_up_lp_kendaraan">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kendaraan Kecamatan</h4>
	      		</div>
	      		<?php echo form_open("front_lp/lpkendaraan/update_lp_kendaraan_kec"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID</label>
	                        				<input type="number" class="form-control" id="_id_kendaraan" name="id_kendaraan" placeholder="ID Kendaraan">
	                      				</div>                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Jenis Kendaraan</label>
                        					<select name="id_jenis_kendaraan" class="form-control" id="_id_jenis_kendaraan">
	                          					<option value="">-- select jenis --</option>
	                          					<?php foreach ($main_jenis_kendaraan as $mjk_) { ?>
	                            					<option value="<?php echo $mjk_->id_jenis_kendaraan ?>"><?php echo $mjk_->keterangan ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>

                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Kecamatan</label>
                        					<select name="id_kec" class="form-control" id="_id_kec">
	                          					<option value="">-- select jenis --</option>
	                          					<?php foreach ($main_kecamatan as $mk_) { ?>
	                            					<option value="<?php echo $mk_->id_kec ?>"><?php echo $mk_->nama_kec ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>	                      				

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Tahun</label>
	                        				<input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
	                      				</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah</label>
					                        <input type="number" class="form-control" id="_jml" name="jml" placeholder="Jumlah">
				                      	</div>				                      	

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>
</div>

<!-- 	<div class="modal fade" id="modal_up_lp_kendaraan_plat">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kendaraan Plat</h4>
	      		</div>
	      		<?php echo form_open("front_lp/lpkendaraan/update_lp_kendaraan_plat"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Kendaraan</label>
	                        				<input type="number" class="form-control" id="_id_kendaraan2" name="id_kendaraan" placeholder="ID Kendaraan">
	                      				</div>                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Jenis Kendaraan</label>
                        					<select name="id_jenis" class="form-control" id="_id_jenis">
	                          					<option value="">-- select jenis --</option>
	                          					<?php foreach ($list_kendaraan_plat_jenis as $lkpj_) { ?>
	                            					<option value="<?php echo $lkpj_->id_jenis ?>"><?php echo $lkpj_->nama_jenis ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>	                      				

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Tahun</label>
	                        				<input type="number" class="form-control" id="_th2" name="th" placeholder="Tahun">
	                      				</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah Merah</label>
					                        <input type="number" class="form-control" id="_jml_merah" name="jml_merah" placeholder="Jumlah Merah">
				                      	</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah Kuning</label>
					                        <input type="number" class="form-control" id="_jml_kuning" name="jml_kuning" placeholder="Jumlah Kuning">
				                      	</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah Hitam</label>
					                        <input type="number" class="form-control" id="_jml_hitam" name="jml_hitam" placeholder="Jumlah Hitam">
				                      	</div>				                      	

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>
</div>

	<div class="modal fade" id="modal_up_lp_kendaraan_plat_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kendaraan Plat Jenis</h4>
	      		</div>
	      		<?php echo form_open("front_lp/lpkendaraan/update_lp_kendaraan_plat_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
	                        				<input type="number" class="form-control" id="_id_jenis2" name="id_jenis" placeholder="ID Jenis">
	                      				</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Nama Jenis</label>
					                        <input type="text" class="form-control" id="_nama_jenis" name="nama_jenis" placeholder="Nama Jenis">
				                      	</div>				                      	

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>
</div>		

	<div class="modal fade" id="modal_up_lp_kendaraan_umum">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kendaraan Umum</h4>
	      		</div>
	      		<?php echo form_open("front_lp/lpkendaraan/update_lp_kendaraan_umum"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Kendaraan</label>
	                        				<input type="number" class="form-control" id="_id_kendaraan3" name="id_kendaraan" placeholder="ID Kendaraan">
	                      				</div>                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Jenis</label>
                        					<select name="id_jenis" class="form-control" id="_id_jenis3">
	                          					<option value="">-- select jenis --</option>
	                          					<?php foreach ($list_kendaraan_umum_jenis as $lkuj_) { ?>
	                            					<option value="<?php echo $lkuj_->id_jenis ?>"><?php echo $lkuj_->nama_jenis ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>

                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Jenis Kendaraan</label>
                        					<select name="id_jenis_kendaraan" class="form-control" id="_id_jenis_kendaraan3">
	                          					<option value="">-- select jenis --</option>
	                          					<?php foreach ($main_jenis_kendaraan as $mjk_) { ?>
	                            					<option value="<?php echo $mjk_->id_jenis_kendaraan ?>"><?php echo $mjk_->keterangan ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>	                      					                      				

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Tahun</label>
	                        				<input type="number" class="form-control" id="_th3" name="th" placeholder="Tahun">
	                      				</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah</label>
					                        <input type="number" class="form-control" id="_jml2" name="jml" placeholder="Jumlah">
				                      	</div>				                      	

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>
</div> -->	
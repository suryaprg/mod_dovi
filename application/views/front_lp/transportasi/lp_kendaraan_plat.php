
	<section class="content-header">
		
	</section>

	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">

			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kendaraan Plat</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 0.5%">No</th>
										<th class="text-center" style="width: 4%">Jenis</th>
										<th class="text-center" style="width: 1%">Tahun</th>
										<th class="text-center" style="width: 1%">Jumlah Merah</th>	
										<th class="text-center" style="width: 1%">Jumlah Kuning</th>	
										<th class="text-center" style="width: 1%">Jumlah Hitam</th>	
										<th class="text-center" style="width: 1.5%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkendaraan_plat/insert_lp_kendaraan_plat/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">Pilih</option>
													<?php foreach ($list_kendaraan_plat_jenis as $lkpj) { ?>
														<option value="<?php echo $lkpj->id_jenis ?>"><?php echo $lkpj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="text" name="th" class="form-control" id="th"></td>
											<td><input type="text" name="jml_merah" class="form-control" id="jml_merah"></td>
											<td><input type="text" name="jml_kuning" class="form-control" id="jml_kuning"></td>
											<td><input type="text" name="jml_hitam" class="form-control" id="jml_hitam"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_kendaraan_plat as $lkp) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lkp->nama_jenis ?></td>
											<td align="center"><?php echo $lkp->th ?></td>
											<td align="center"><?php echo $lkp->jml_merah ?></td>
											<td align="center"><?php echo $lkp->jml_kuning ?></td>
											<td align="center"><?php echo $lkp->jml_hitam ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_kendaraan_plat(<?php echo $lkp->id_kendaraan ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_kendaraan_plat(<?php echo $lkp->id_kendaraan ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kendaraan Plat Jenis</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped" id="plat_jenis">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>	
										<th class="text-center" style="width: 8%">Jenis</th>	
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkendaraan_plat/insert_lp_kendaraan_plat_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_kendaraan_plat_jenis as $lkpj2) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lkpj2->nama_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_kendaraan_plat_jenis(<?php echo $lkpj2->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_kendaraan_plat_jenis(<?php echo $lkpj2->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>				
			</div>

		</section>
	</div>	



	<script type="text/javascript">
		$(document).ready(function (){
			// $('#plat').dataTable();

		});
//===================================================== LP KENDARAAN PLAT =========================================//		
	  	function up_lp_kendaraan_plat(id_kendaraan){
	    	clear_mod_up_lp_kendaraan_plat();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_kendaraan', id_kendaraan);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/lpkendaraan_plat/index_up_lp_kendaraan_plat/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_kendaraan_plat(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_kendaraan_plat").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_kendaraan_plat(){
	      	$("#_id_jenis").val("");
	      	$("#_th").val("");
	      	$("#_jml_merah").val("");
	      	$("#_jml_kuning").val("");
	      	$("#_jml_hitam").val("");
	  	}

  		function res_update_lp_kendaraan_plat(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_kendaraan").attr('readonly','readonly');                                        
	        	$("#_id_kendaraan").val(main_data.id_kendaraan);
	          	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_th").val(main_data.th);
	          	$("#_jml_merah").val(main_data.jml_merah);
	          	$("#_jml_kuning").val(main_data.jml_kuning);
	          	$("#_jml_hitam").val(main_data.jml_hitam);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_kendaraan_plat(id_kendaraan){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_kendaraan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kendaraan+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkendaraan_plat/delete_lp_kendaraan_plat/";?>"+id_kendaraan;
	      	}else{

	      	}
	  	}
//===================================================== LP KENDARAAN PLAT =========================================//

//===================================================== LP KENDARAAN PLAT JENIS ======================================//	
	  	function up_lp_kendaraan_plat_jenis(id_jenis){
	    	clear_mod_up_lp_kendaraan_plat_jenis();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_jenis', id_jenis);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/lpkendaraan_plat/index_up_lp_kendaraan_plat_jenis/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_kendaraan_plat_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_kendaraan_plat_jenis").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_kendaraan_plat_jenis(){
	      	$("#_nama_jenis").val("");
	  	}

  		function res_update_lp_kendaraan_plat_jenis(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_jenis2").attr('readonly','readonly');                                        
	        	$("#_id_jenis2").val(main_data.id_jenis);
	          	$("#_nama_jenis").val(main_data.nama_jenis);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_kendaraan_plat_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkendaraan_plat/delete_lp_kendaraan_plat_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}
//===================================================== LP KENDARAAN PLAT JENIS ======================================//		
	</script>

	<div class="modal fade" id="modal_up_lp_kendaraan_plat">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kendaraan Plat</h4>
	      		</div>
	      		<?php echo form_open("front_lp/lpkendaraan_plat/update_lp_kendaraan_plat"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Kendaraan</label>
	                        				<input type="number" class="form-control" id="_id_kendaraan" name="id_kendaraan" placeholder="ID Kendaraan">
	                      				</div>                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Jenis Kendaraan</label>
                        					<select name="id_jenis" class="form-control" id="_id_jenis">
	                          					<option value="">-- select jenis --</option>
	                          					<?php foreach ($list_kendaraan_plat_jenis as $lkpj_) { ?>
	                            					<option value="<?php echo $lkpj_->id_jenis ?>"><?php echo $lkpj_->nama_jenis ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>	                      				

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Tahun</label>
	                        				<input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
	                      				</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah Merah</label>
					                        <input type="number" class="form-control" id="_jml_merah" name="jml_merah" placeholder="Jumlah Merah">
				                      	</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah Kuning</label>
					                        <input type="number" class="form-control" id="_jml_kuning" name="jml_kuning" placeholder="Jumlah Kuning">
				                      	</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah Hitam</label>
					                        <input type="number" class="form-control" id="_jml_hitam" name="jml_hitam" placeholder="Jumlah Hitam">
				                      	</div>				                      	

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>
</div>	

	<div class="modal fade" id="modal_up_lp_kendaraan_plat_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kendaraan Plat Jenis</h4>
	      		</div>
	      		<?php echo form_open("front_lp/lpkendaraan_plat/update_lp_kendaraan_plat_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
	                        				<input type="number" class="form-control" id="_id_jenis2" name="id_jenis" placeholder="ID Jenis">
	                      				</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Nama Jenis</label>
					                        <input type="text" class="form-control" id="_nama_jenis" name="nama_jenis" placeholder="Nama Jenis">
				                      	</div>				                      	

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>
</div>


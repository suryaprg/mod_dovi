<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KOMUNIKASI DAN INFORMASIS</title>
    <link rel="shortcut icon" href="https://malangkota.go.id/wp-content/themes/malangkotanew/images/favicon.png">

    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts8/tabeldata.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/css/style.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/template_aw/charts/css/style.css">

    <link rel="stylesheet" href="<?= base_url();?>assets/chart_main/amchart_vertical/export.css" type="text/css" media="all" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="fh5co-loader"></div>
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="index2.html" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>A</b>LT</span>
                <!-- logo for regular state and mobile devices -->
                <div style="position: absolute; left: 50px; top: 0.1px;"><img height="47px" style="padding: 5px" src="<?php echo base_url();?>/assets/template_aw/dist/img/ncc-logo-white.png" alt="NCC logo"></div>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <li class="dropdown user user-menu">
                            <a href="<?php echo base_url();?>web_home/web">
                                <div style="position: absolute; top: 11px; right: 130px;"><img src="<?php echo base_url();?>/assets/template_aw/dist/img/logo.png" width="25" height="25" alt="User Image"></div>&nbsp &nbsp &nbsp &nbsp &nbsp
                                <span class="hidden-xs">Malang Open Data</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo base_url();?>/assets/template_aw/dist/img/logo.png" alt="User Image">
                    </div>
                    <p class="contoh3" style="position: absolute; top: 15px; left: 61px; color: white; ">KOMUNIKASI DAN INFORMASI</p>
                    <div class="pull-left info" style="position: absolute; left: 45px; bottom: 5px;">
                        <br>
                        <br>
                        <a href="#"><i class="fa fa-circle text-success"></i>Pemerintah Kota Malang</a>
                    </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <?php //include "nav_menu.php"; ?>
            </section>
            <!-- /.sidebar -->
        </aside>


<!-- HTML -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h1> Data Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor </h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="box-body chart-responsive">
                            <table class="data-table" style="width: 20%">
                                <caption class="title"></caption>
                                <thead>
                                    <?php print_r($str_header);?>
                                </thead>
                                <tbody>
                                    <?php print_r($str_tbl); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <br><br>
                        <div class="box box-solid box-primary">
                            <div class="box-header with-border">
                                <center>
                                    <h3 class="box-title">Data Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor di Kota Malang Tahun <?= $th_st ." - ". $th_fn; ?></h3></center>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <div class="box-body chart-responsive">
                                <div>
                                    <div id="chartdiv" style="width: 100%; height: 700px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                      
            </div>   

                <!-- <div class="row">
                    <div class="col-sm-4">
                        <div class="box-body chart-responsive">
                            
                        </div>
                    </div>

                    <div class="col-sm-8">
                        <br><br>
                        <div class="box box-solid box-primary">
                            <div class="box-header with-border">
                                <center>
                                    <h3 class="box-title">Data Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor di Kota Malang Tahun <?= $th_st ." - ". $th_fn; ?></h3></center>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <div class="box-body chart-responsive">
                                <div>
                                    <div id="chartdiv" style="height: 500px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                   
        </div>
    </div>
</div>





        <footer class="main-footer">
            <strong>Copyright &copy; 2018 <a href="http://kominfo.malangkota.go.id/" target="_blank">Kominfo Kota Malang</a>
        </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>

<!-- Resources -->
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/core.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/charts.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/animated.js"></script>

    <script type="text/javascript">
        var data_main = JSON.parse('<?php print_r("$data_graph");?>');

        $(document).ready(function(){
            create_graph(data_main[1])
            console.log(data_main);
        });

        $(".jenis").click(function(){
            var id_jenis_html = $(this).attr("id");
            var id_jenis = id_jenis_html.split("_")[1];
            
            console.log(data_main[id_jenis]);

            create_graph(data_main[id_jenis]);
        });

        

        function create_graph(data_graph){  
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add percent sign to all numbers
        chart.numberFormatter.numberFormat = "#";

        // Add data
        chart.data = data_graph;

        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "year";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "Data Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor";
        valueAxis.title.fontWeight = 800;

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = "lapor";
        series.dataFields.categoryX = "year";
        series.clustered = false;
        series.tooltipText = "Laporan yang diterima pada {categoryX}: [bold]{valueY}[/]";

        var series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = "selesai";
        series2.dataFields.categoryX = "year";
        series2.clustered = false;
        series2.columns.template.width = am4core.percent(50);
        series2.tooltipText = "Laporan yang ditindaklanjuti pada {categoryX}: [bold]{valueY}[/]";

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
        }
    </script>

</body>
</html>
	<section class="content-header">
		
	</section>

	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Tehnologi</h3>
						</div>
						<table class="table table-responsive">
						<thead>
									<tr>
										<th style="width: 1%"></th>
              							<!-- <th class="text-center">Id</th> -->
             						 	<th style="width: 3%"></th>
              							<th style="width: 2%"></th>
              							<th style="width: 2%"></th>
              							<th style="width: 2%"></th>
									</tr>
								</thead>
							<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lptehnologi/insert_lp_tehnologi/" ?>" method="post">
											<td>
												<select name="id_sub_jenis" class="form-control" id="id_sub_jenis">
													<option value="">-- Pilih Jenis --</option>
													<?php foreach ($list_tehnologi_sub_jenis as $lpj) { ?>
													<option value="<?php echo $lpj->id_sub_jenis ?>"><?php echo $lpj->nama_sub_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="number" name="th" class="form-control" id="th" placeholder="Tahun"></td>
											<td><input type="number" name="jml" class="form-control" id="jml" placeholder="Jumlah"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
							
						</table>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
              							<!-- <th class="text-center">Id</th> -->
             						 	<th class="text-center" style="width: 3%">Jenis</th>
              							<th class="text-center" style="width: 2%">Tahun</th>
              							<th class="text-center" style="width: 2%">Jumlah</th>
              							<th class="text-center" style="width: 2%">Action</th>
									</tr>
								</thead>
									<?php $no = 1; foreach ($list_tehnologi as $la) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $la->nama_sub_jenis ?></td>
											<td align="center"><?php echo $la->th ?></td>
											<td align="center"><?php echo $la->jml ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_tehnologi(<?php echo $la->id_tehnologi ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_tehnologi(<?php echo $la->id_tehnologi?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Sub Tehnologi</h3>
						</div>
						<table class="table table-responsive">
						<thead>
									<tr>
										<th style="width: 1%"></th>
              							<!-- <th class="text-center">Id</th> -->
             						 	<th style="width: 3%"></th>
              							<th style="width: 2%"></th>
              							<th style="width: 2%"></th>
              							<th style="width: 2%"></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lptehnologi/insert_lp_tehnologi_sub_jenis/" ?>" method="post">
										<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">-- Pilih Jenis --</option>
													<?php foreach ($list_tehnologi_jenis as $lpj) { ?>
													<option value="<?php echo $lpj->id_jenis ?>"><?php echo $lpj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>

											
											<td><input type="text" name="nama_sub_jenis" class="form-control" id="nama_sub_jenis" placeholder="ama Jenis"></td>
											<td><input type="text" name="satuan" class="form-control" id="satuan" placeholder="Satuan"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus">Tambah</span>
												</button>
											</td>
										</form>
									</tr>
							
						</table>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example2">
								<thead>
									<tr>
										<th class="text-center">No</th>
										<th class="text-center">Nama Jenis</th>
										<th class="text-center">Nama Sub jenis</th>
										<th class="text-center">Satuan</th>
										<th class="text-center">Aksi</th>
									</tr>
								</thead>
									<?php $no = 1; foreach ($list_tehnologi_sub_jenis as $lpsj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="center"><?php echo $lpsj->nama_jenis ?></td>
											<td align="left"><?php echo $lpsj->nama_sub_jenis ?></td>
											<td align="center"><?php echo $lpsj->satuan ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_tehnologi_sub_jenis(<?php echo $lpsj->id_sub_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_tehnologi_sub_jenis(<?php echo $lpsj->id_sub_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Jenis Tehnologi</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped" id="tehnologi">
								<thead>
									<tr>
										<th class="text-center">No</th>
										<th class="text-center">Jenis</th>
										<th class="text-center">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lptehnologi/insert_lp_tehnologi_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis" placeholder="Nama Jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_tehnologi_jenis as $lpj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lpj->nama_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_tehnologi_jenis(<?php echo $lpj->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_tehnologi_jenis(<?php echo $lpj->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</section>
	</div>


	<script type="text/javascript">
		// $(document).ready(function(){
		// 	$("#aparat").DataTable();
		// });

//======================================================== LP APARAT =============================================//
	  	function up_lp_tehnologi(id_tehnologi){
	    	clear_mod_up_lp_tehnologi();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_tehnologi', id_tehnologi);    
		      $.ajax({
		        url: "<?php echo base_url()."/front_lp/Lptehnologi/index_up_lp_tehnologi/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_tehnologi(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_tehnologi").modal('show');
	  	} 

	  	function clear_mod_up_lp_tehnologi(){
	      	$("#_id_jenis").val("");
	      	$("#_th").val("");
	      	$("#_jml").val("");
	  	}

	  	function res_update_lp_tehnologi(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_tehnologi").attr('readonly','readonly');                                        
          		$("#_id_tehnologi").val(main_data.id_tehnologi);
          		$("#_id_sub_jenis").val(main_data.id_sub_jenis);
          		$("#_th").val(main_data.th);
          		$("#_jml").val(main_data.jml);
          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_tehnologi(id_tehnologi){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_tehnologi+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_tehnologi+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/Lptehnologi/delete_lp_tehnologi/";?>"+id_tehnologi;
	      	}else{

	      	}
	  	}      		
//======================================================== LP PERTANIAN =============================================//
//======================================================== LP PERTANIANSUBJenis ===============================//
function up_lp_tehnologi_sub_jenis(id_sub_jenis){
	    	clear_mod_up_lp_tehnologi_sub_jenis();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_sub_jenis', id_sub_jenis);    
		      $.ajax({
		        url: "<?php echo base_url()."/front_lp/Lptehnologi/index_up_lp_tehnologi_sub_jenis/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_tehnologi_sub_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_tehnologi_sub_jenis").modal('show');
	  	} 

	  	function clear_mod_up_lp_tehnologi_sub_jenis(){
	      	$("#_id_jenis").val("");
	      	$("#_nama_sub_jenis").val("");
	      	$("#_satuan").val("");
	  	}

	  	function res_update_lp_tehnologi_sub_jenis(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_sub_jenis2").attr('readonly','readonly');                                        
          		$("#_id_sub_jenis2").val(main_data.id_sub_jenis);
        		 // $("#_id_jenis").attr('readonly','readonly');
          		$("#_id_jenis2").val(main_data.id_jenis);
         	 	$("#_nama_sub_jenis").val(main_data.nama_sub_jenis);
          		$("#_satuan").val(main_data.satuan);
          
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_tehnologi_sub_jenis(id_sub_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_sub_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_sub_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/Lptehnologi/delete_lp_tehnologi_sub_jenis/";?>"+id_sub_jenis;
	      	}else{

	      	}
	  	}
//======================================================== LP PERTANIAN Jenis =============================================//
	  	function up_lp_tehnologi_jenis(id_jenis){
	    	clear_mod_up_lp_tehnologi_jenis();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_jenis', id_jenis);    
		      $.ajax({
		        url: "<?php echo base_url()."/front_lp/Lptehnologi/index_up_lp_tehnologi_jenis/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_tehnologi_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_tehnologi_jenis").modal('show');
	  	} 

	  	function clear_mod_up_lp_tehnologi_jenis(){
	      	$("#_nama_jenis").val("");
	  	}

	  	function res_update_lp_tehnologi_jenis(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_jenis3").attr('readonly','readonly');                                        
          		$("#_id_jenis3").val(main_data.id_jenis);
          		$("#_nama_jenis").val(main_data.nama_jenis);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_tehnologi_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/Lptehnologi/delete_lp_tehnologi_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}
//======================================================== LP APARAT Jenis =============================================//
	</script>	

	<div class="modal fade" id="modal_up_lp_tehnologi">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data Lp tehnologi</h4>
	      	</div>
	      	<?php echo form_open("front_lp/lptehnologi/update_lp_tehnologi"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID</label>
	                        	<input type="number" class="form-control" id="_id_tehnologi" name="id_tehnologi" placeholder="ID">
	                      	</div>                      
	                      
	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1"> Sub Jenis</label>
	                        	<select name="id_sub_jenis" class="form-control" id="_id_sub_jenis">
	                          		<option value="">-- select id sub jenis --</option>
	                          		<?php foreach ($list_tehnologi_sub_jenis as $jenis) { ?>
                            		<option value="<?php echo $jenis->id_sub_jenis ?>"><?php echo $jenis->nama_sub_jenis ?></option>
	                          		<?php } ?>
                        		</select>
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Tahun</label>
	                        	<input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jumlah</label>
	                        	<input type="number" class="form-control" id="_jml" name="jml" placeholder="Jumlah">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
	</div>

	<div class="modal fade" id="modal_up_lp_tehnologi_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">&times;</span>
	          		</button>
	        		<h4 class="modal-title">Ubah Data Lp tehnologi Jenis</h4>
	      		</div>
	      		<?php echo form_open("front_lp/lptehnologi/update_lp_tehnologi_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
	                        				<input type="number" class="form-control" id="_id_jenis3" name="id_jenis" placeholder="ID Jenis">
	                      				</div>     

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama Jenis</label>
	                        				<input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Nama Jenis">
	                      				</div>

	                  					</div>
	                				</div>      
	            				</div>
	          				</div>
	        			</div>
	      			</div>
	      		<div class="modal-footer">
			        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
			        <button type="submit" class="btn btn-primary">Ubah Data</button>
	      		</div>
	      		</form>
	    	</div>
	  	</div>
	</div>			


</div>	


<div class="modal fade" id="modal_up_lp_tehnologi_sub_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">&times;</span>
	          		</button>
	        		<h4 class="modal-title">Ubah Data Lp tehnologi sub Jenis</h4>
	      		</div>
	      		<?php echo form_open("front_lp/lptehnologi/update_lp_tehnologi_sub_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID sub Jenis</label>
	                        				<input type="number" class="form-control" id="_id_sub_jenis2" name="id_sub_jenis" placeholder="ID sub Jenis">
	                      				</div>     

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">id_Jenis</label>
	                        				<input type="text" name="id_jenis" class="form-control" id="_id_jenis2" placeholder="id Jenis">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">nama-sub_Jenis</label>
	                        				<input type="text" name="nama_sub_jenis" class="form-control" id="_nama_sub_jenis" placeholder="nama sub Jenis">
	                      				</div>
	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">satuan</label>
	                        				<input type="text" name="satuan" class="form-control" id="_satuan" placeholder="Satuan">
	                      				</div>

	                  					</div>
	                				</div>      
	            				</div>
	          				</div>
	        			</div>
	      			</div>
	      		<div class="modal-footer">
			        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
			        <button type="submit" class="btn btn-primary">Ubah Data</button>
	      		</div>
	      		</form>
	    	</div>
	  	</div>
	</div>			


</div>	
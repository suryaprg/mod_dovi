	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran TPA</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped " id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 4%">Kecamatan</th>
										<th class="text-center" style="width: 2%">Tahun</th>
										<th class="text-center" style="width: 2%">Jumlah</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lptpa/insert_lp_tpa/" ?>" method="post">
											<td>
												<select name="id_kec" class="form-control" id="id_kec">
													<option value="">Pilih</option>
													<?php foreach ($list_kecamatan as $lk) { ?>
														<option value="<?php echo $lk->id_kec ?>"><?php echo $lk->nama_kec ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="text" name="th" class="form-control" id="th"></td>
											<td><input type="text" name="jml" class="form-control" id="jml"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_tpa as $lt) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lt->nama_kec ?></td>
											<td align="center"><?php echo $lt->th ?></td>
											<td align="center"><?php echo $lt->jml ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_tpa(<?php echo $lt->id_tpa ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_tpa(<?php echo $lt->id_tpa ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

		</section>
	</div>

	<script type="text/javascript">
//======================================================== LP TPA =============================================//
	  	function up_lp_tpa(id_tpa){
	    	clear_mod_up_lp_tpa();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_tpa', id_tpa);    
		      $.ajax({
		        url: "<?php echo base_url()."/front_lp/Lptpa/index_up_lp_tpa/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_tpa(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_tpa").modal('show');
	  	} 

	  	function clear_mod_up_lp_tpa(){
	      	$("#_id_kec").val("");
	      	$("#_th").val("");
	      	$("#_jml").val("");
	  	}

	  	function res_update_lp_tpa(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_tpa").attr('readonly','readonly');                                        
	          	$("#_id_tpa").val(main_data.id_tpa);
	          	$("#_id_kec").val(main_data.id_kec);
	          	$("#_th").val(main_data.th);
	          	$("#_jml").val(main_data.jml);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_tpa(id_tpa){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_tpa+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_tpa+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lptpa/delete_lp_tpa/";?>"+id_tpa;
	      	}else{

	      	}
	  	}      		
//======================================================== LP TPA =============================================//		
	</script>

	<div class="modal fade" id="modal_up_lp_tpa">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data LP Koperasi</h4>
	      	</div>
	      	<?php echo form_open("front_lp/lptpa/update_lp_tpa"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID TPA</label>
	                        	<input type="number" name="id_tpa" id="_id_tpa" class="form-control">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Sub Jenis</label>
								<select name="id_kec" class="form-control" id="_id_kec">
									<option value="">Pilih</option>
									<?php foreach ($list_kecamatan as $lk) { ?>
										<option value="<?php echo $lk->id_kec ?>"><?php echo $lk->nama_kec ?></option>
									<?php } ?>
								</select>	                      	
	                      	</div>	                      	


	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Tahun</label>
	                        	<input type="number" name="th" class="form-control" id="_th" placeholder="Tahun">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jumlah</label>
	                        	<input type="number" name="jml" class="form-control" id="_jml" placeholder="Jumlah">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>	
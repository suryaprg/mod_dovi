  <section class="content-header">
    
  </section>

  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Lampiran OR</h3>
            </div>
            <table class="table table-responsive">
            <thead>
                  <tr>
                    <th style="width: 1%"></th>
                            <!-- <th class="text-center">Id</th> -->
                    <th style="width: 3%"></th>
                    <th style="width: 2%"></th>
                    <th style="width: 2%"></th>
                    <th style="width: 2%"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <form action="<?php echo base_url()."/front_lp/Lpor/insert_lp_or/" ?>" method="post">
                      <td>
                        <select name="id_jenis" class="form-control" id="id_jenis">
                          <option value="">-- Pilih Jenis --</option>
                          <?php foreach ($list_or_jenis as $llj) { ?>
                          <option value="<?php echo $llj->id_jenis ?>"><?php echo $llj->nama_jenis ?></option>
                          <?php } ?>
                        </select>
                      </td>
                      <td><input type="number" name="th" class="form-control" id="th" placeholder="Tahun"></td>
                      <td><input type="number" name="jml" class="form-control" id="jml" placeholder="Jumlah"></td>
                      <td align="center">
                        <button type="submit" class="btn btn-success">
                          <span class="glyphicon glyphicon-plus"></span>
                        </button>
                      </td>
                    </form>
                  </tr>
              
            </table>

            <div class="box-body">
              <table class="table table-bordered table-striped display" id="example1">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 1%">No</th>
                            <!-- <th class="text-center">Id</th> -->
                          <th class="text-center" style="width: 3%">Jenis</th>
                            <th class="text-center" style="width: 2%">Tahun</th>
                            <th class="text-center" style="width: 2%">Jumlah</th>
                            <th class="text-center" style="width: 2%">Action</th>
                  </tr>
                </thead>
                  <?php $no = 1; foreach ($list_or as $la) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td align="left"><?php echo $la->nama_jenis ?></td>
                      <td align="center"><?php echo $la->th ?></td>
                      <td align="center"><?php echo $la->jml ?></td>
                      <td align="center">
                        <a href="#" onclick="up_lp_or(<?php echo $la->id_or ?>)">
                          <button class="btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span>
                          </button>
                        </a>
                        <a href="#" onclick="del_lp_or(<?php echo $la->id_or ?>)">
                          <button class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-trash"></span>
                          </button>
                        </a>                        
                      </td>
                    </tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Lampiran Jenis Or</h3>
            </div>
            <table class="table table-responsive" id="example">
              <thead>
                  <tr>
                    <th style="width: 1%"></th>
                            <!-- <th class="text-center">Id</th> -->
                    <th style="width: 8%"></th>
                    <th style="width: 1%"></th>
                    <tbody>
                  <tr>
                    <td></td>
                    <form action="<?php echo base_url()."/front_lp/Lpor/insert_lp_or_jenis/" ?>" method="post">
                      <td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis" placeholder="Nama Jenis"></td>
                      <td align="center">
                        <button type="submit" class="btn btn-success">
                          <span class="glyphicon glyphicon-plus"> Tambah</span>
                        </button>
                      </td>
                    </form>
                  </tr>
                </thead>
            </table>

            <div class="box-body">
              <table class="table table-bordered table-striped display" id="example2">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Jenis</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                  <?php $no = 1; foreach ($list_or_jenis as $lpj) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td align="left"><?php echo $lpj->nama_jenis ?></td>
                      <td align="center">
                        <a href="#" onclick="up_lp_or_jenis(<?php echo $lpj->id_jenis ?>)">
                          <button class="btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span>
                          </button>
                        </a>
                        <a href="#" onclick="del_lp_or_jenis(<?php echo $lpj->id_jenis ?>)">
                          <button class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-trash"></span>
                          </button>
                        </a>                        
                      </td>
                    </tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </section>
  </div>


  <script type="text/javascript">
    // $(document).ready(function(){
    //  $("#aparat").DataTable();
    // });

//======================================================== LP OR =============================================//
  function up_lp_or(id_or){
    clear_mod_up_lp_or();
    // console.lo

    var data_main =  new FormData();
    data_main.append('id_or', id_or);    
      $.ajax({
        url: "<?php echo base_url()."/front_lp/Lpor/index_up_lp_or/";?>", // point to serv
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: data_main,                         
        type: 'post',
        success: function(res){
            console.log(res);
            res_update_lp_or(res);
            // $("#out_up_mhs").html(res);
        }
    });
    
    $("#modal_up_lp_or").modal('show');
  } 

  function clear_mod_up_lp_or(){
      $("#_id_jenis").val("");
      $("#_th").val("");
      $("#_jml").val("");
  }

  function res_update_lp_or(res){
      var data = JSON.parse(res);

      if(data.status){
          var main_data = data.val;
          console.log(main_data);
          
          $("#_id_or").attr('readonly','readonly');                                        
          $("#_id_or").val(main_data.id_or);
          $("#_id_jenis").val(main_data.id_jenis);
          $("#_th").val(main_data.th);
          $("#_jml").val(main_data.jml);
          
      }else{
          clear_mod_up_ik();
      }
  }    

  function del_lp_or(id_or){
      var conf = confirm("Apakah anda yakin untuk menghapus "+id_or+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_or+" akan terhapus semua.. !!!!!!! ");
      if(conf){
        window.location.href = "<?= base_url()."/front_lp/lpor/delete_lp_or/";?>"+id_or;
      }else{

      }
  }   
//======================================================== LP OR JENIS ===============================//
  function up_lp_or_jenis(id_jenis){
    clear_mod_up_lp_or_jenis();
    // console.lo

    var data_main =  new FormData();
    data_main.append('id_jenis', id_jenis);    
      $.ajax({
        url: "<?php echo base_url()."/front_lp/Lpor/index_up_lp_or_jenis/";?>", // point to serv
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: data_main,                         
        type: 'post',
        success: function(res){
            console.log(res);
            res_update_lp_or_jenis(res);
            // $("#out_up_mhs").html(res);
        }
    });
    
    $("#modal_up_lp_or_jenis").modal('show');
  }

  function clear_mod_up_lp_or_jenis(){
      $("#_nama_jenis").val("");
  }  

  function res_update_lp_or_jenis(res){
      var data = JSON.parse(res);

      if(data.status){
          var main_data = data.val;
          console.log(main_data);
          
          $("#_id_jenis2").attr('readonly','readonly');                                        
          $("#_id_jenis2").val(main_data.id_jenis);
          $("#_nama_jenis").val(main_data.nama_jenis);
          
      }else{
          clear_mod_up_ik();
      }
  }  

  function del_lp_or_jenis(id_jenis){
      var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
      if(conf){
        window.location.href = "<?= base_url()."/front_lp/lpor/delete_lp_or_jenis/";?>"+id_jenis;
      }else{

      }
  }  
//======================================================== MODAL  OR =============================================//
  </script> 


<div class="modal fade" id="modal_up_lp_or">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ubah Data Lp or</h4>
      </div>
      <?php echo form_open("/front_lp/lpor/update_lp_or"); ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">                   
                      
                      <div class="form-group">
                        <label for="exampleInputPassword1">ID</label>
                        <input type="number" class="form-control" id="_id_or" name="id_or" placeholder="ID">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Jenis</label>
                        <select name="id_jenis" class="form-control" id="_id_jenis">
                          <option value="">-- select id jenis --</option>
                          <?php foreach ($list_or_jenis as $jenis) { ?>
                            <option value="<?php echo $jenis->id_jenis ?>"><?php echo $jenis->nama_jenis ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Tahun</label>
                        <input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Jumlah</label>
                        <input type="number" class="form-control" id="_jml" name="jml" placeholder="Jumlah">
                      </div>

                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>

      </form>
    </div>
  </div>
</div>
</div>


  

<div class="modal fade" id="modal_up_lp_or_jenis">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ubah Data Lp or Jenis</h4>
      </div>
      <?php echo form_open("/front_lp/lpor/update_lp_or_jenis"); ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">

                      <div class="form-group">
                        <label for="exampleInputPassword1">ID Jenis</label>
                        <input type="number" class="form-control" id="_id_jenis2" name="id_jenis" placeholder="ID Jenis">
                      </div>     

                      <div class="form-group">
                        <label for="exampleInputPassword1">Nama Jenis</label>
                        <input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Nama Jenis">
                      </div>

                  </div>

                </div>      
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>
	<section class="content-header">
		
	</section>

	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kesehatan</h3>
						</div>

						<div class="box-body" style="overflow-x:auto;">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 3%">Jenis</th>
										<th class="text-center" style="width: 3%">Sub Jenis</th>
										<th class="text-center" style="width: 1%">Tahun</th>
										<th class="text-center" style="width: 1%">Jumlah</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkesehatan/insert_lp_kesehatan/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">Pilih</option>
													<?php foreach ($list_kesehatan_jenis as $lkj) { ?>
														<option value="<?php echo $lkj->id_jenis ?>"><?php echo $lkj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td>
												<select name="id_sub_jenis" class="form-control" id="id_sub_jenis">
													<option value="">Pilih</option>
													<?php foreach ($list_kesehatan_sub_jenis as $lksb) { ?>
														<option value="<?php echo $lksb->id_sub_jenis ?>"><?php echo $lksb->nama_sub_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="text" name="th" class="form-control" id="th" placeholder="Tahun"></td>
											<td><input type="text" name="jml" class="form-control" id="jml" placeholder="Jumlah"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_kesehatan as $lk) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lk->nama_jenis ?></td>
											<td align="left"><?php echo $lk->nama_sub_jenis ?></td>
											<td align="center"><?php echo $lk->th ?></td>
											<td align="center"><?php echo $lk->jml ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_kesehatan(<?php echo $lk->id_kesehatan ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_kesehatan(<?php echo $lk->id_kesehatan ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		</section>	
	</div>	

	<script type="text/javascript">
//===================================================== LP KESEHATAN =========================================//		
	  	function up_lp_kesehatan(id_kesehatan){
	    	clear_mod_up_lp_kesehatan();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_kesehatan', id_kesehatan);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/lpkesehatan/index_up_lp_kesehatan/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_kesehatan(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_kesehatan").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_kesehatan(){
	      	$("#_id_jenis").val("");
	      	$("#_id_sub_jenis").val("");
	      	$("#_th").val("");
	      	$("#_jml").val("");
	  	}

  		function res_update_lp_kesehatan(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_kesehatan").attr('readonly','readonly');                                        
	        	$("#_id_kesehatan").val(main_data.id_kesehatan);
	          	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_id_sub_jenis").val(main_data.id_sub_jenis);
	          	$("#_th").val(main_data.th);
	          	$("#_jml").val(main_data.jml);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_kesehatan(id_kesehatan){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_kesehatan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kesehatan+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkesehatan/delete_lp_kesehatan/";?>"+id_kesehatan;
	      	}else{

	      	}
	  	}	
//===================================================== LP KESEHATAN =========================================//
	</script>

	<div class="modal fade" id="modal_up_lp_kesehatan">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kesehatan</h4>
	      		</div>
	      		
	      		<?php echo form_open("front_lp/lpkesehatan/update_lp_kesehatan"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Kesehatan</label>
	                        				<input type="number" class="form-control" id="_id_kesehatan" name="id_kesehatan" placeholder="ID Kesehatan">
	                      				</div>                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Jenis</label>
                        					<select name="id_jenis" class="form-control" id="_id_jenis">
	                          					<option value="">-- select jenis --</option>
	                          					<?php foreach ($list_kesehatan_jenis as $jenis) { ?>
	                            					<option value="<?php echo $jenis->id_jenis ?>"><?php echo $jenis->nama_jenis ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Sub Jenis</label>
                        					<select name="id_sub_jenis" class="form-control" id="_id_sub_jenis">
	                          					<option value="">-- select sub jenis --</option>
	                          					<?php foreach ($list_kesehatan_sub_jenis as $sub_jenis) { ?>
	                            					<option value="<?php echo $sub_jenis->id_jenis ?>"><?php echo $sub_jenis->nama_sub_jenis ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Tahun</label>
	                        				<input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
	                      				</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah</label>
					                        <input type="number" class="form-control" id="_jml" name="jml" placeholder="Jumlah">
				                      	</div>

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	

</div>	



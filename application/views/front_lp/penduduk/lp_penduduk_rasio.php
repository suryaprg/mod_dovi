    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Data Rasio Gender</h3>
                        </div>

<!--                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 1%"></th>
                                    <th class="text-center" style="width: 3%"></th>
                                    <th class="text-center" style="width: 3%"></th>
                                    <th class="text-center" style="width: 1%"></th>
                                    <th class="text-center" style="width: 1%"></th>
                                    <th class="text-center" style="width: 1%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <form action="<?php echo base_url()."/admin_opd/geo/Lppdb/insert_lp_pdb/" ?>" method="post">
                                        <td>
                                            <select name="id_jenis" class="form-control">
                                                <option value="">Jenis</option>
                                                <?php foreach ($list_pdb_jenis as $lpj) { ?>
                                                    <option value="<?php echo $lpj->id_jenis ?>"><?php echo $lpj->nama_jenis ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="id_kategori" class="form-control">
                                                <option value="">Kecamatan</option>
                                                <?php foreach ($list_pdb_kategori as $lpk) { ?>
                                                    <option value="<?php echo $lpk->id_kategori ?>"><?php echo $lpk->nama_kategori ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td><input type="text" name="th" class="form-control" placeholder="tahun"></td>
                                        <td><input type="text" name="jml" class="form-control" placeholder="jumlah"></td>
                                        <td align="center">
                                            <button type="submit" class="btn btn-success">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </td>
                                    </form>
                                </tr>                               
                            </tbody>
                        </table> -->

                        <div class="box-body">
                            <table class="table table-bordered table-striped display" id="">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 1%">No</th>
                                        <th class="text-center" style="width: 4%">Laki Laki</th>
                                        <th class="text-center" style="width: 4%">Perempuan</th>
                                        <th class="text-center" style="width: 4%">Rasio</th>
                                        <th class="text-center" style="width: 1%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach ($list_rasio as $lp) { ?>
                                        <tr>
                                            <td align="center"><?php echo $no ?></td>
                                            <td align="left"><?php echo $lp->jml_l ?></td>
                                            <td align="left"><?php echo $lp->jml_p ?></td>
                                            <td align="left"><?php echo $lp->rasio ?></td>
                                            <td align="center">
                                                <a href="#" onclick="up_lp_rasio(<?php echo $lp->id_penduduk ?>)">
                                                    <button class="btn btn-primary btn-xs">
                                                        <span class="glyphicon glyphicon-pencil"></span>
                                                    </button>
                                                </a>
                                                <a href="#" onclick="del_lp_rasio(<?php echo $lp->id_penduduk ?>)">
                                                    <button class="btn btn-danger btn-xs">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </button>
                                                </a>                                                
                                            </td>
                                        </tr>
                                    <?php $no++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

<!--            <div class="row">

                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Data Jenis PDB</h3>
                        </div>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="width: 1%"></th>
                                    <th style="width: 6%"></th>
                                    <th style="width: 2%"></th>
                                    <th style="width: 1%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <form action="<?php echo base_url()."/admin_opd/geo/Lppdb/insert_lp_pdb_jenis/" ?>" method="post">
                                        <td><input type="text" name="nama_jenis" class="form-control" placeholder="nama"></td>
                                        <td><input type="text" name="satuan" class="form-control" placeholder="satuan"></td>
                                        <td align="center">
                                            <button type="submit" class="btn btn-success">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </td>
                                    </form>
                                </tr>                               
                            </tbody>
                        </table>

                        <div class="box-body">
                            <table class="table table-bordered table-striped" id="">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 1%">No</th>
                                        <th class="text-center" style="width: 6%">Nama</th>
                                        <th class="text-center" style="width: 2%">Satuan</th>
                                        <th class="text-center" style="width: 1%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach ($list_pdb_jenis as $lpj) { ?>
                                        <tr>
                                            <td align="center"><?php echo $no ?></td>
                                            <td align="left"><?php echo $lpj->nama_jenis ?></td>
                                            <td align="left"><?php echo $lpj->satuan ?></td>
                                            <td align="center">
                                                <a href="#" onclick="up_lp_pdb_jenis(<?php echo $lpj->id_jenis ?>)">
                                                    <button class="btn btn-primary btn-xs">
                                                        <span class="glyphicon glyphicon-pencil"></span>
                                                    </button>
                                                </a>
                                                <a href="#" onclick="del_lp_pdb_jenis(<?php echo $lpj->id_jenis ?>)">
                                                    <button class="btn btn-danger btn-xs">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </button>
                                                </a>                                                
                                            </td>
                                        </tr>
                                    <?php $no++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Data Kategori PDB</h3>
                        </div>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="width: 1%"></th>
                                    <th style="width: 8%"></th>
                                    <th style="width: 1%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <form action="<?php echo base_url()."/admin_opd/geo/Lppdb/insert_lp_pdb_kategori/" ?>" method="post">
                                        <td><input type="text" name="nama_kategori" class="form-control" placeholder="nama"></td>
                                        <td align="center">
                                            <button type="submit" class="btn btn-success">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </td>
                                    </form>
                                </tr>                               
                            </tbody>
                        </table>

                        <div class="box-body">
                            <table class="table table-bordered table-striped display" id="">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 1%">No</th>
                                        <th class="text-center" style="width: 8%">Nama</th>
                                        <th class="text-center" style="width: 1%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach ($list_pdb_kategori as $lpk) { ?>
                                        <tr>
                                            <td align="center"><?php echo $no ?></td>
                                            <td align="left"><?php echo $lpk->nama_kategori ?></td>
                                            <td align="center">
                                                <a href="#" onclick="up_lp_pdb_kategori(<?php echo $lpk->id_kategori ?>)">
                                                    <button class="btn btn-primary btn-xs">
                                                        <span class="glyphicon glyphicon-pencil"></span>
                                                    </button>
                                                </a>
                                                <a href="#" onclick="del_lp_pdb_kategori(<?php echo $lpk->id_kategori ?>)">
                                                    <button class="btn btn-danger btn-xs">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </button>
                                                </a>                                                
                                            </td>
                                        </tr>
                                    <?php $no++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>              

            </div> -->
        </section>
    </div>

    <script type="text/javascript">
    // $(document).ready(function(){
    //  $("#aparat").DataTable();
    // });

//======================================================== LP PMKS =============================================//
 function up_lp_rasio(id_penduduk){
    clear_mod_up_lp_rasio();
    // console.lo

    var data_main =  new FormData();
    data_main.append('id_penduduk', id_penduduk);    
      $.ajax({
        url: "<?php echo base_url()."/front_lp/Rasio/index_up_lp_rasio/";?>", // point to serv
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: data_main,                         
        type: 'post',
        success: function(res){
            console.log(res);
            res_update_lp_rasio(res);
            // $("#out_up_mhs").html(res);
        }
    });
    
    $("#modal_up_lp_rasio").modal('show');
  } 

  function clear_mod_up_lp_rasio(){
 
      $("#_jml_l").val("");
      $("#_jml_p").val("");
      $("#_rasio").val("");
  }

  function res_update_lp_rasio(res){
      var data = JSON.parse(res);

      if(data.status){
          var main_data = data.val;
          console.log(main_data);
          
          $("#_id_penduduk").attr('readonly','readonly');                                        
          $("#_id_penduduk").val(main_data.id_penduduk);
          $("#_jml_l").val(main_data.jml_l);
          $("#_jml_p").val(main_data.jml_p);
          $("#_rasio").val(main_data.rasio);

          
      }else{
          clear_mod_up_ik();
      }
  }    

  function del_lp_rasio(id_penduduk){
      var conf = confirm("Apakah anda yakin untuk menghapus "+id_penduduk+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_penduduk+" akan terhapus semua.. !!!!!!! ");
      if(conf){
        window.location.href = "<?= base_url()."front_lp/Rasio/delete_lp_rasio/";?>"+id_penduduk;
      }else{

      }
  }
//======================================================== LP PMKS JENIS ===============================//
</script> 


<div class="modal fade" id="modal_up_lp_rasio">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ubah Data Lp Rasio Gender</h4>
      </div>
      <?php echo form_open("front_lp/Rasio/update_lp_rasio"); ?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">                   
                      
                      <div class="form-group">
                        <label for="exampleInputPassword1">ID penduduk</label>
                        <input type="number" class="form-control" id="_id_penduduk" name="id_penduduk" placeholder="ID">
                      </div> 

                      <div class="form-group">
                        <label for="exampleInputPassword1">Jumlah L</label>
                        <input type="number" class="form-control" id="_jml_l" name="jml_l" placeholder="Jumlah L">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Jumlah P</label>
                        <input type="number" class="form-control" id="_jml_p" name="jml_p" placeholder="Jumlah P">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Rasio</label>
                        <input type="text" class="form-control" id="_rasio" name="rasio" placeholder="Rsaio">
                      </div>

                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>

      </form>
    </div>
  </div>
</div>
</div>
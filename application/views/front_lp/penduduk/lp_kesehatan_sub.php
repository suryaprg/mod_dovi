	<section class="content-header">
		
	</section>

	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kesehatan Sub Jenis</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 3%">Jenis</th>
										<th class="text-center" style="width: 4%">Nama</th>
										<th class="text-center" style="width: 1%">Satuan</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkesehatan_sub/insert_lp_kesehatan_sub/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">Pilih</option>
													<?php foreach ($list_kesehatan_jenis as $lkj) { ?>
														<option value="<?php echo $lkj->id_jenis ?>"><?php echo $lkj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="text" name="nama_sub_jenis" class="form-control" placeholder="Nama Sub Jenis"></td>
											<td><input type="text" name="satuan" class="form-control" placeholder="Satuan"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_kesehatan_sub_jenis as $lksj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lksj->nama_jenis ?></td>
											<td align="left"><?php echo $lksj->nama_sub_jenis ?></td>
											<td align="center"><?php echo $lksj->satuan ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_kesehatan_sub(<?php echo $lksj->id_sub_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_kesehatan_sub(<?php echo $lksj->id_sub_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="col-md-5">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kesehatan Sub 3 Jenis</h3>
						</div>

						<div class="box-body">
							<table class="table table-striped table-bordered" id="kesehatan_sub3_jenis">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 4%">Sub Jenis</th>
										<th class="text-center" style="width: 3%">Nama</th>
										<th class="text-center" style="width: 1%">Satuan</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkesehatan_sub/insert_lp_kesehatan_sub3/" ?>" method="post">
											<td>
												<select name="id_sub_jenis" class="form-control" id="id_sub_jenis">
													<option value="">Pilih</option>
													<?php foreach ($list_kesehatan_sub_jenis as $lksj2) { ?>
														<option value="<?php echo $lksj2->id_sub_jenis ?>"><?php echo $lksj2->nama_sub_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="text" name="nama_3sub_jenis" class="form-control" placeholder="Nama Sub Jenis"></td>
											<td><input type="text" name="satuan" class="form-control" placeholder="Satuan"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_kesehatan_sub3_jenis as $lks3j) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lks3j->nama_sub_jenis ?></td>
											<td align="left"><?php echo $lks3j->nama_3sub_jenis ?></td>
											<td align="center"><?php echo $lks3j->satuan ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_kesehatan_sub3(<?php echo $lks3j->id_3sub_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_kesehatan_sub3(<?php echo $lks3j->id_3sub_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>				

			</div>
		</section>
	</div>

	<script type="text/javascript">
//===================================================== LP SUB KESEHATAN =========================================//		
	  	function up_lp_kesehatan_sub(id_sub_jenis){
	    	clear_mod_up_lp_kesehatan_sub();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_sub_jenis', id_sub_jenis);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/lpkesehatan_sub/index_up_lp_kesehatan_sub/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_kesehatan_sub(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_kesehatan_sub").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_kesehatan_sub(){
	      	$("#_id_jenis").val("");
	      	$("#_nama_sub_jenis").val("");
	      	$("#_satuan").val("");
	  	}

  		function res_update_lp_kesehatan_sub(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_sub_jenis").attr('readonly','readonly');                                        
	        	$("#_id_sub_jenis").val(main_data.id_sub_jenis);
	          	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_nama_sub_jenis").val(main_data.nama_sub_jenis);
	          	$("#_satuan").val(main_data.satuan);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_kesehatan_sub(id_sub_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_sub_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_sub_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkesehatan_sub/delete_lp_kesehatan_sub/";?>"+id_sub_jenis;
	      	}else{

	      	}
	  	}	
//===================================================== LP SUB KESEHATAN =========================================//

//===================================================== LP SUB 3 KESEHATAN =========================================//		
	  	function up_lp_kesehatan_sub3(id_3sub_jenis){
	    	clear_mod_up_lp_kesehatan_sub3();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_3sub_jenis', id_3sub_jenis);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/lpkesehatan_sub/index_up_lp_kesehatan_sub3/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_kesehatan_sub3(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_kesehatan_sub3").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_kesehatan_sub3(){
	      	$("#_id_sub_jenis").val("");
	      	$("#_nama_3sub_jenis").val("");
	      	$("#_satuan").val("");
	  	}

  		function res_update_lp_kesehatan_sub3(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_3sub_jenis").attr('readonly','readonly');                                        
	        	$("#_id_3sub_jenis").val(main_data.id_3sub_jenis);
	          	$("#_id_sub_jenis2").val(main_data.id_sub_jenis);
	          	$("#_nama_3sub_jenis").val(main_data.nama_3sub_jenis);
	          	$("#_satuan2").val(main_data.satuan);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_kesehatan_sub3(id_3sub_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_3sub_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_3sub_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkesehatan_sub/delete_lp_kesehatan_sub3/";?>"+id_3sub_jenis;
	      	}else{

	      	}
	  	}	
//===================================================== LP SUB 3 KESEHATAN =========================================//				
	</script>

	<div class="modal fade" id="modal_up_lp_kesehatan_sub">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kesehatan Sub Jenis</h4>
	      		</div>
	      		
	      		<?php echo form_open("front_lp/lpkesehatan_sub/update_lp_kesehatan_sub"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Sub Jenis</label>
	                        				<input type="number" class="form-control" id="_id_sub_jenis" name="id_sub_jenis" placeholder="ID Sub Jenis">
	                      				</div>
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Jenis</label>
                        					<select name="id_jenis" class="form-control" id="_id_jenis">
	                          					<option value="">-- select Jenis --</option>
	                          					<?php foreach ($list_kesehatan_jenis as $jns) { ?>
	                            					<option value="<?php echo $jns->id_jenis ?>"><?php echo $jns->nama_jenis ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama Sub Jenis</label>
	                        				<input type="text" name="nama_sub_jenis" id="_nama_sub_jenis" class="form-control">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Satuan</label>
	                        				<input type="text" class="form-control" id="_satuan" name="satuan" placeholder="Satuan">
	                      				</div>	                      				                      

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>

	<div class="modal fade" id="modal_up_lp_kesehatan_sub3">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kesehatan Sub 3 Jenis</h4>
	      		</div>
	      		
	      		<?php echo form_open("front_lp/lpkesehatan_sub/update_lp_kesehatan_sub3"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Sub 3 Jenis</label>
	                        				<input type="number" class="form-control" id="_id_3sub_jenis" name="id_3sub_jenis" placeholder="ID Sub Jenis">
	                      				</div>
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Sub Jenis</label>
                        					<select name="id_sub_jenis" class="form-control" id="_id_sub_jenis2">
	                          					<option value="">-- select Jenis --</option>
	                          					<?php foreach ($list_kesehatan_sub_jenis as $lksj4) { ?>
	                            					<option value="<?php echo $lksj4->id_sub_jenis ?>"><?php echo $lksj4->nama_sub_jenis ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama</label>
	                        				<input type="text" name="nama_3sub_jenis" id="_nama_3sub_jenis" class="form-control">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Satuan</label>
	                        				<input type="text" class="form-control" id="_satuan2" name="satuan" placeholder="Satuan">
	                      				</div>	                      				                      

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>		
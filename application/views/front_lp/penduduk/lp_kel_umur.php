	<section class="content-header">
		
	</section>

	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kelompok Umur</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 3%">Jenis</th>
										<th class="text-center" style="width: 1%">Tahun</th>
										<th class="text-center" style="width: 2%">Laki Laki</th>
										<th class="text-center" style="width: 2%">Perempuan</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkel_umur/insert_lp_umur/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">Pilih</option>
													<?php foreach ($list_umur_jenis as $luj) { ?>
														<option value="<?php echo $luj->id_jenis ?>"><?php echo $luj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="number" name="th" class="form-control" id="th" placeholder="Tahun"></td>
											<td><input type="number" name="jml_l" class="form-control" id="jml_l" placeholder="Jumlah Laki Laki"></td>
											<td><input type="number" name="jml_p" class="form-control" id="jml_p" placeholder="Jumlah Perempuan"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_umur as $lu) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="center"><?php echo $lu->nama_jenis ?></td>
											<td align="center"><?php echo $lu->th ?></td>
											<td align="center"><?php echo $lu->jml_l ?></td>
											<td align="center"><?php echo $lu->jml_p ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_umur(<?php echo $lu->id_kel_umur ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_umur(<?php echo $lu->id_kel_umur ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="col-md-5">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Jenis Umur</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="umur_jenis">
								<thead>
									<tr>
										<th class="text-center">No</th>
										<th class="text-center">Jenis</th>
										<th class="text-center">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkel_umur/insert_lp_umur_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis" placeholder="Nama Jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_umur_jenis as $luj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="center"><?php echo $luj->nama_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_umur_jenis(<?php echo $luj->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_umur_jenis(<?php echo $luj->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
		</section>
	</div>

	<script type="text/javascript">
//===================================================== LP KEL UMUR Jenis =========================================//		
	  	function up_lp_umur(id_kel_umur){
	    	clear_mod_up_lp_umur();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_kel_umur', id_kel_umur);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/lpkel_umur/index_up_lp_umur/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_umur(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_umur").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_umur(){
	      	$("#_id_jenis").val("");
	      	$("#_th").val("");
	      	$("#_jml_l").val("");
	      	$("#_jml_p").val("");
	  	}

  		function res_update_lp_umur(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_kel_umur").attr('readonly','readonly');                                        
	        	$("#_id_kel_umur").val(main_data.id_kel_umur);
	          	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_th").val(main_data.th);
	          	$("#_jml_l").val(main_data.jml_l);
	          	$("#_jml_p").val(main_data.jml_p);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_umur(id_kel_umur){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_kel_umur+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kel_umur+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkel_umur/delete_lp_umur/";?>"+id_kel_umur;
	      	}else{

	      	}
	  	}
//===================================================== LP KEL UMUR Jenis =========================================//

//===================================================== LP KEL UMUR Jenis =========================================//		
	  	function up_lp_umur_jenis(id_jenis){
	    	clear_mod_up_lp_umur_jenis();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_jenis', id_jenis);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/lpkel_umur/index_up_lp_umur_jenis/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_umur_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_umur_jenis").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_umur_jenis(){
	      	$("#_nama_jenis").val("");
	  	}

  		function res_update_lp_umur_jenis(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_jenis2").attr('readonly','readonly');                                        
	        	$("#_id_jenis2").val(main_data.id_jenis);
	          	$("#_nama_jenis").val(main_data.nama_jenis);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_umur_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkel_umur/delete_lp_umur_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}	
//===================================================== LP KEL UMUR JENIS =========================================//		
	</script>

	<div class="modal fade" id="modal_up_lp_umur_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kel Umur Jenis</h4>
	      		</div>
	      		<?php echo form_open("front_lp/lpkel_umur/update_lp_umur_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
	                        				<input type="number" class="form-control" id="_id_jenis2" name="id_jenis" placeholder="ID Jenis">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama Jenis</label>
	                        				<input type="text" class="form-control" id="_nama_jenis" name="nama_jenis" placeholder="Nama Jenis">
	                      				</div>				                      	

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>
</div>	

	<div class="modal fade" id="modal_up_lp_umur">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kel Umur</h4>
	      		</div>
	      		<?php echo form_open("front_lp/lpkel_umur/update_lp_umur"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Kel Umur</label>
	                        				<input type="number" class="form-control" id="_id_kel_umur" name="id_kel_umur" placeholder="ID Kel Umur">
	                      				</div>                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
                        					<select name="id_jenis" class="form-control" id="_id_jenis">
	                          					<option value="">-- select jenis --</option>
	                          					<?php foreach ($list_umur_jenis as $jenis) { ?>
	                            					<option value="<?php echo $jenis->id_jenis ?>"><?php echo $jenis->nama_jenis ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Tahun</label>
	                        				<input type="number" class="form-control" id="_th" name="th" placeholder="Tahun">
	                      				</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah L</label>
					                        <input type="number" class="form-control" id="_jml_l" name="jml_l" placeholder="Jumlah L">
				                      	</div>

				                      	<div class="form-group">
					                        <label for="exampleInputPassword1">Jumlah P</label>
					                        <input type="number" class="form-control" id="_jml_p" name="jml_p" placeholder="Jumlah P">
				                      	</div>				                      	

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>
</div>
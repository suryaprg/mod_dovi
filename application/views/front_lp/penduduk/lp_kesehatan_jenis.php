	<section class="content-header">
		
	</section>

	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kesehatan Jenis</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="example1">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 4%">Nama</th>
										<th class="text-center" style="width: 4%">Kategori</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkesehatan_jenis/insert_lp_kesehatan_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis" placeholder="Nama Jenis"></td>
											<td>
												<select name="id_kategori" class="form-control" id="id_kategori">
													<option value="">Pilih</option>
													<?php foreach ($list_kesehatan_kategori as $lkk) { ?>
														<option value="<?php echo $lkk->id_kategori ?>"><?php echo $lkk->nama_kategori ?></option>
													<?php } ?>
												</select>
											</td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_kesehatan_jenis as $lkj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lkj->nama_jenis ?></td>
											<td align="left"><?php echo $lkj->nama_kategori ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_kesehatan_jenis(<?php echo $lkj->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_kesehatan_jenis(<?php echo $lkj->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="col-md-5">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Lampiran Kesehatan Kategori</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped" id="kesehatan_kategori">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 8%">Kategori</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/front_lp/Lpkesehatan_jenis/insert_lp_kesehatan_kategori/" ?>" method="post">
											<td><input type="text" name="nama_kategori" class="form-control" id="nama_kategori" placeholder="Nama Kategori"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_kesehatan_kategori as $lkk) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lkk->nama_kategori ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_kesehatan_kategori(<?php echo $lkk->id_kategori ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_kesehatan_kategori(<?php echo $lkk->id_kategori ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>				

			</div>

		</section>	
	</div>	

	<script type="text/javascript">
//===================================================== LP KESEHATAN =========================================//		
	  	function up_lp_kesehatan_jenis(id_jenis){
	    	clear_mod_up_lp_kesehatan_jenis();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_jenis', id_jenis);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/lpkesehatan_jenis/index_up_lp_kesehatan_jenis/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_kesehatan_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_kesehatan_jenis").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_kesehatan_jenis(){
	      	$("#_nama_jenis").val("");
	      	$("#_id_kategori").val("");
	  	}

  		function res_update_lp_kesehatan_jenis(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_jenis").attr('readonly','readonly');                                        
	        	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_nama_jenis").val(main_data.nama_jenis);
	          	$("#_id_kategori").val(main_data.id_kategori);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_kesehatan_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkesehatan_jenis/delete_lp_kesehatan_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}	
//===================================================== LP KESEHATAN =========================================//

//===================================================== LP KESEHATAN =========================================//		
	  	function up_lp_kesehatan_kategori(id_kategori){
	    	clear_mod_up_lp_kesehatan_kategori();
	    	// console.lo

	    	var data_main =  new FormData();
	    	data_main.append('id_kategori', id_kategori);    
	      	$.ajax({
	        	url: "<?php echo base_url()."/front_lp/lpkesehatan_jenis/index_up_lp_kesehatan_kategori/";?>", // point to serv
	        	dataType: 'html',  // what to expect back from the PHP script, if anything
	        	cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_kesehatan_kategori(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    	$("#modal_up_lp_kesehatan_kategori").modal('show');
	  	}	  			

	  	function clear_mod_up_lp_kesehatan_kategori(){
	      	$("#_nama_kategori").val("");
	  	}

  		function res_update_lp_kesehatan_kategori(res){
      		var data = JSON.parse(res);

      		if(data.status){
          		var main_data = data.val;
          		console.log(main_data);
          
	       	   	$("#_id_kategori2").attr('readonly','readonly');                                        
	        	$("#_id_kategori2").val(main_data.id_kategori);
	          	$("#_nama_kategori").val(main_data.nama_kategori);
      		}else{
          		clear_mod_up_ik();
      		}
  		}

	  	function del_lp_kesehatan_kategori(id_kategori){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_kategori+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kategori+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpkesehatan_jenis/delete_lp_kesehatan_kategori/";?>"+id_kategori;
	      	}else{

	      	}
	  	}	
//===================================================== LP KESEHATAN =========================================//		
	</script>

	<div class="modal fade" id="modal_up_lp_kesehatan_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kesehatan Jenis</h4>
	      		</div>
	      		
	      		<?php echo form_open("front_lp/lpkesehatan_jenis/update_lp_kesehatan_jenis"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Jenis</label>
	                        				<input type="number" class="form-control" id="_id_jenis" name="id_jenis" placeholder="ID Jenis">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama Jenis</label>
	                        				<input type="text" class="form-control" id="_nama_jenis" name="nama_jenis" placeholder="Nama Jenis">
	                      				</div>	                      				                      
	                      
                      					<div class="form-group">
	                        				<label for="exampleInputPassword1">Kategori</label>
                        					<select name="id_kategori" class="form-control" id="_id_kategori">
	                          					<option value="">-- select Kategori --</option>
	                          					<?php foreach ($list_kesehatan_kategori as $ktg) { ?>
	                            					<option value="<?php echo $ktg->id_kategori ?>"><?php echo $ktg->nama_kategori ?></option>
	                          					<?php } ?>
	                        				</select>
	                      				</div>

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>

	<div class="modal fade" id="modal_up_lp_kesehatan_kategori">
	  	<div class="modal-dialog">
	    	<div class="modal-content">

	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Ubah Data Lp Kesehatan Kategori</h4>
	      		</div>
	      		
	      		<?php echo form_open("front_lp/lpkesehatan_jenis/update_lp_kesehatan_kategori"); ?>
	      		<div class="modal-body">
	        		<div class="row">
	          			<div class="col-md-12">
	            			<div class="box box-primary">
	                			<div class="box-body">
	                  				<div class="row">
	                    				<div class="col-md-12">

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">ID Kategori</label>
	                        				<input type="number" class="form-control" id="_id_kategori2" name="id_kategori" placeholder="ID Kategori">
	                      				</div>

	                      				<div class="form-group">
	                        				<label for="exampleInputPassword1">Nama Kategori</label>
	                        				<input type="text" class="form-control" id="_nama_kategori" name="nama_kategori" placeholder="Nama Kategori">
	                      				</div>

	                  					</div>
	                				</div>
	            				</div>
	          				</div>
	        			</div>
	      			</div>

	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        			<button type="submit" class="btn btn-primary">Ubah Data</button>
	      			</div>

	      		</form>
    		</div>
	  	</div>
	</div>	
</div>	
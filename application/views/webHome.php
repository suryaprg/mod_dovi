<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Malang Open Data</title>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <!-- Custom Fonts -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>home/custom-font/fonts.css" />
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>home/css/bootstrap.min.css" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>home/css/font-awesome.min.css" />
        <!-- Bootsnav -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>home/css/bootsnav.css">
        <!-- Fancybox -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>home/css/jquery.fancybox.css?v=2.1.5" media="screen" />	
        <!-- Custom stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>home/css/custom.css" />
    
    </head>
    <body>

        <!-- Preloader -->

        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                </div>
            </div>
        </div>

        <!--End off Preloader -->

        <!-- Header -->
         <header>
            <!-- Top Navbar -->
            <div class="top_nav">
                <div class="container">
                    <ul class="list-inline info">
                        <li><font color="#ffffff">Temukan Data Seluruh Unit Kerja Pemerintah Kota Malang</font></li>
                    </ul>
                    <ul class="list-inline social_icon">
                        <li><a href=""><span class="fa fa-facebook"></span></a></li>
                        <li><a href=""><span class="fa fa-twitter"></span></a></li>
                        <li><a href=""><span class="fa fa-behance"></span></a></li>
                        <li><a href=""><span class="fa fa-dribbble"></span></a></li>
                        <li><a href=""><span class="fa fa-linkedin"></span></a></li>
                        <li><a href=""><span class="fa fa-youtube"></span></a></li>
                    </ul>           
                </div>
            </div><!-- Top Navbar end -->

            <!-- Navbar -->
            <nav class="navbar bootsnav">
                <!-- Top Search -->
                <div class="top-search">
                    <div class="container">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" class="form-control" placeholder="Search">
                            <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <!-- Atribute Navigation -->
                    <div class="attr-nav">
                        <ul>
                            <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                        </ul>
                    </div>
                    <!-- Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href=""><img class="logo" src="<?php echo base_url(); ?>home/img/logo.png" alt="" style="width: 500px; height: 50px"></a>
                    </div>
                    <!-- Navigation -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav menu">
                            <li><a href="">Home</a></li>                    
                            <li><a href="#about">Data</a></li>
                            <li><a href="#services">Visualisasi</a></li>
                            <li><a href="#portfolio">Pejabat Struktural</a></li>
                            <li><a href="#contact_form">Dokumentasi</a></li>
                            <li><a href="#contact_form">Tentang</a></li>
                        </ul>
                    </div>
                </div>   
            </nav><!-- Navbar end -->
        </header>


        <section id="home" class="home">
            <div class="header-container">
      <div class="video-container">
         <video preload="true" autoplay="autoplay" muted >
          <source src="<?php echo base_url(); ?>home/video/mod.mp4" type="video/mp4">
        </video>
    
      </div>
    </div>
        </section>


</br>
        <!-- Kategori -->
     <section class="bg-light" id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <u><h2 class="section-heading text-uppercase">Kategori</h2></u><br><br>
            
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 col-sm-5 portfolio-item">
            <a class="portfolio-link" href="<?php echo base_url(); ?>front_data/mainpendidikan/index">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  
                </div>
              </div>
              <img class="img-fluid" src="<?php echo base_url(); ?>home/img/portfolio/01-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Pendidikan</h4>
            </div>
          </div>
          <div class="col-md-3 col-sm-5 portfolio-item">
            <a class="portfolio-link" href="<?php echo base_url(); ?>front_data/mainkesehatan/index">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  
                </div>
              </div>
              <img class="img-fluid" src="<?php echo base_url(); ?>home/img/portfolio/02-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Kesehatan</h4>
            </div>
          </div>
          <div class="col-md-3 col-sm-5 portfolio-item">
            <a class="portfolio-link" href="<?php echo base_url(); ?>front_data/mainkesehatan/index"">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                 
                </div>
              </div>
              <img class="img-fluid" src="<?php echo base_url(); ?>home/img/portfolio/03-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Keuangan</h4>
            </div>
          </div>
          <div class="col-md-3 col-sm-5 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  
                </div>
              </div>
              <img class="img-fluid" src="<?php echo base_url(); ?>home/img/portfolio/04-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Kependudukan</h4>
            </div>
          </div>
          <div class="col-md-3 col-sm-5 portfolio-item">
            <a class="portfolio-link" href="<?php echo base_url(); ?>front_data/mainkependudukan/index">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  
                </div>
              </div>
              <img class="img-fluid" src="<?php echo base_url(); ?>home/img/portfolio/05-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Transportasi</h4>
              
            </div>
          </div>
          <div class="col-md-3 col-sm-5 portfolio-item">
            <a class="portfolio-link" href="<?php echo base_url(); ?>front_data/mainsosial/index">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  
                </div>
              </div>
              <img class="img-fluid" src="<?php echo base_url(); ?>home/img/portfolio/06-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Sosial, Politik, dan Hukum</h4>
            </div>
          </div>
         <div class="col-md-3 col-sm-5 portfolio-item">
            <a class="portfolio-link" href="<?php echo base_url(); ?>front_data/maininfra/index">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  
                </div>
              </div>
              <img class="img-fluid" src="<?php echo base_url(); ?>home/img/portfolio/07-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Budidaya</h4>
             
            </div>
          </div>
          <div class="col-md-3 col-sm-5 portfolio-item">
            <a class="portfolio-link" href="<?php echo base_url(); ?>front_data/mainsosial/index">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                 
                </div>
              </div>
              <img class="img-fluid" src="<?php echo base_url(); ?>home/img/portfolio/08-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Pemerintahan</h4>
            </div>
          </div>
          <div class="col-md-3 col-sm-5 portfolio-item">
            <a class="portfolio-link" href="<?php echo base_url(); ?>front_data/mainpemerintahan/index">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                 
                </div>
              </div>
              <img class="img-fluid" src="<?php echo base_url(); ?>home/img/portfolio/09-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Ketenagakerjaan</h4>
            </div>
          </div>
          <div class="col-md-3 col-sm-5 portfolio-item">
            <a class="portfolio-link" href="<?php echo base_url(); ?>front_data/maingeo/index">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  
                </div>
              </div>
              <img class="img-fluid" src="<?php echo base_url(); ?>home/img/portfolio/10-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Geografi</h4>
              
            </div>
          </div>
          <div class="col-md-3 col-sm-5 portfolio-item">
            <a class="portfolio-link" href="<?php echo base_url(); ?>front_data/maingeo/index">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  
                </div>
              </div>
              <img class="img-fluid" src="<?php echo base_url(); ?>home/img/portfolio/10-thumbnail.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Komunikasi dan Informasi</h4>
              
            </div>
          </div>
  
        </div>
      </div>
    </section>
   
         <!--End of Kategori -->

        <!-- Contact form -->
        <section id="contact_form">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Do you have any questions?</h2>
                        <h2 class="second_heading">Feel free to contact us!</h2>
                    </div>
                    <form role="form" class="form-inline text-right col-md-6" >
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="5" id="msg" placeholder="Message"></textarea>
                        </div>
                        <button type="submit" class="btn submit_btn">Submit</button>
                    </form>				
                </div>
            </div>
        </section><!-- Contact form end -->
        

        <!-- Footer -->
        <footer>
            <!-- Footer top -->
            <div class="container footer_top">
                <div class="row">
                    <div class="col-lg-5 col-sm-7">
                        <div class="footer_item">
                            <h4>Petunjuk Arah</h4>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.6782028920125!2d112.6409980537108!3d-8.03207598675561!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd627eab4241a03%3A0x6763c56ee92a8ada!2sPerkantoran+Terpadu+Malang!5e0!3m2!1sid!2sid!4v1541818835217" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-sm-7">
                        <div class="footer_item">
                            <h4>Tentang</h4>
                            <img class="logo" src="img/logo.png" alt="Construction" />
                            <p align="justify">Lahir sebagai cita cita Pemerintah Kota Malang untuk menyediakan satu basis data pembangunan yang akurat, terbuka, terpusat dan terintegrasi dan mewujudkan pelayanan publik yang prima</p>

                            <ul class="list-inline footer_social_icon">
                                <li><a href=""><span class="fa fa-facebook"></span></a></li>
                                <li><a href=""><span class="fa fa-twitter"></span></a></li>
                                <li><a href=""><span class="fa fa-youtube"></span></a></li>
                                <li><a href=""><span class="fa fa-google-plus"></span></a></li>
                                <li><a href=""><span class="fa fa-linkedin"></span></a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-sm-5">
                        <div class="footer_item">
                            <h4>Hubungi Kami</h4>
                            <ul class="list-unstyled footer_contact">
                                <li><a href=""><span class="fa fa-map-marker"></span><p align="justify"> Perkantoran Terpadu Pemerintah Kota Malang, Gedung A lantai 4</a></li>
                                <li><a href=""><span class="fa fa-envelope"></span>kominfo@malangkota.go.id</a></li>
                                <li><a href=""><span class="fa fa-mobile"></span><p>(0341) 751550</p></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- Footer top end -->

            <!-- Footer bottom -->
            <div class="footer_bottom text-center">
                <p class="wow fadeInRight">
                    Made with 
                    <i class="fa fa-heart"></i>
                    by 
                    <a target="_blank" href="http://bootstrapthemes.co">Bootstrap Themes</a> 
                    . All Rights Reserved
                </p>
            </div><!-- Footer bottom end -->
        </footer><!-- Footer end -->

        <!-- JavaScript -->
        <script src="<?php echo base_url(); ?>home/js/jquery-1.12.1.min.js"></script>
        <script src="<?php echo base_url(); ?>home/js/bootstrap.min.js"></script>
        <script>
    $(document).ready(function(){

        $('.col-4-lg').hover(
            // trigger when mouse hover
            function(){
                $(this).animate({
                    marginTop: "-=1%",
                },200);
            },

            // trigger when mouse out
            function(){
                $(this).animate({
                    marginTop: "0%"
                },200);
            }
        );
    });
</script>


<!--  -->



        <!-- Bootsnav js -->
        <script src="<?php echo base_url(); ?>home/js/bootsnav.js"></script>

        <!-- JS Implementing Plugins -->
        <script src="<?php echo base_url(); ?>home/js/isotope.js"></script>
        <script src="<?php echo base_url(); ?>home/js/isotope-active.js"></script>
        <script src="<?php echo base_url(); ?>home/js/jquery.fancybox.js?v=2.1.5"></script>

        <script src="<?php echo base_url(); ?>home/js/jquery.scrollUp.min.js"></script>

        <script src="<?php echo base_url(); ?>home/js/main.js"></script>
    </body>	
</html>	
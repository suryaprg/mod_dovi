  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Jalan</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section><br>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Jalan merupakan suatu kebutuhan yang paling esensial dalam transportasi. Tanpa adanya 
jalan tak mungkin disediakan jasa transportasi bagi pemakainya. Jalan ditujukan dan disediakan 
sebagai basis bagi alat angkutan untuk bergerak dari suatu tempat asal ke tempat tujuannya. 
Dengan tersedanya prasarana jalan yang semakin baik dan luas akan memperlancar arus 
pengangkutan manusia dan barang serta memberikan manfaat yang sangat besar bagi 
kesejahteraan penduduk. Maka dengan demikian prasarana jalan yang baik dan lancer akan 
menunjang kelancaran arus pengangkutan manusia, barang dan jasa serta melancarkan 
hubungan antar kota dengan desa dan sebaliknya. Untuk melancarkan hubungan antar daerah, 
antar desa dan antar kota diperlukan perbaikan jalan baik panjang jalan maupun kualitasnya. 
Jalan negara merupakan jalan arteri dan jalan kolektor dalam sistem jaringan jalan primer 
yang menghubungkan antar ibukota provinsi dan jalan strategis nasional serta jalan tol. Jalan 
negara ternyata pada tahun 2016 mengalami kenaikan, panjang jalan negara di Kota Malang 
tahun 2016 mencapai 12,64 km, mengalami kenaikan sepanjang 11,19 km atau naik sebesar 
771,72 persen jika dibandingkan tahun 2015 yang panjangnya hanya mencapai 1,45 km. Jalan 
negara yang ada di Kota Malang sepanjang 12,64 km kondisinya baik. Tanggung jawab atas 
penyelenggaraan jalan negara ini berdasarkan pasal 14 ayat 1 UU 38 2004 adalah pemerintah 
pusat. Dengan adanya kenaikan jalan negara mudah-mudahan bisa memperlancar transportasi 
antar ibukota provinsi dan jalan strategis nasional serta jalan tol yang terhubung dengan Kota 
Malang.
  </p>
</section><br><br>  

    <table class="data-table table-responsive" style="position: relative; bottom: 30px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="7" style="font-size: 17px;">Panjang Jalan menurut Kondisi Jalan (KM) di Kota Malang, 2015-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Kondisi Jalan</td>
        <td colspan="2">Jalan Negara</td>
        <td colspan="2">Jalan Provinsi</td>
        <td colspan="2">Jalan Kota</td>
      </tr>
      <tr>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;

    $tot1 = 0;
    foreach ($data_jalan as $jalan) 
    {
      $negara1 = $jalan->negara1;
      $negara2 = $jalan->negara2;
      $provinsi1 = $jalan->provinsi1;
      $provinsi2 = $jalan->provinsi2;
      $kota1 = $jalan->kota1;
      $kota2 = $jalan->kota2;

      echo '<tr>
          <td>'.$jalan->kondisi.'</td>
          <td>'.$negara1. '</td>
          <td>'.$negara2.'</td>
          <td>'.$provinsi1. '</td>
          <td>'.$provinsi2.'</td>
          <td>'.$kota1. '</td>
          <td>'.$kota2.'</td>

         
          </tr>';
      $total1 += $jalan->negara1;
      $total2 += $jalan->negara2;

      $total3 += $jalan->provinsi1;
      $total4 += $jalan->provinsi2;

      $total5 += $jalan->kota1;
      $total6 += $jalan->kota2;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1">Jumlah</th>
        <th><?=number_format($total1, 2, ',', '')?></th>
        <th><?=number_format($total2, 2, ',', '')?></th>
        <th><?=number_format($total3, 2, ',', '')?></th>
        <th><?=number_format($total4, 2, ',', '')?></th>
        <th><?=number_format($total5, 2, ',', '')?></th>
        <th><?=number_format($total6, 2, ',', '')?></th>
        
      </tr>
    </tfoot>
  </table>

    </div>
  </div>
</div>
</div>
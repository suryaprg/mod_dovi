 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Kendaraan Bermotor</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Transportasi merupakan salah satu fasilitas bagi suatu daerah untuk maju dan 
berkembang serta transportasi dapat meningkatkan aksesbilitas atau hubungan suatu daerah 
untuk maju dan berkembang. Transportasi dapat meningkatkan aksesbilitas atau hubungan 
suatu daerah karena aksebiltas sering dikaitkan dengan daerah. Untuk membangun suatu 
pedesaan keberadaan prasarana dan sarana transportasi tidak dapat terpisahkan dalam suatu 
program pembangunan. Kelangsungan proses produksi yang efisien, investasi dan 
perkembangan teknologi serta terciptanya pasar dan nilai selalu didukung oleh sistem 
transportasi yang baik. Transportasi merupakan faktor yang sangat penting dan strategis untuk 
dikembangkan, diantaranya adalah untuk melayani angkutan barang dan manusia dari satu 
daerah ke daerah lainnya dan menunjang pengembangan kegiatan-kegiatan sektor lain untuk 
meningkatkan pembangunan nasional di Indonesia.
  </p>
</section><br><br>

  <table class="data-table table-responsive" style="position: relative; bottom: 30px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="9">Jumlah Kendaraan Bermotor Menurut Kecamatan dan Jenis Kendaraan di Kota Malang, 2015-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Kecamatan</td>
        <td colspan="2">Mobil Penumpang</td>
        <td colspan="2">Bus</td>
        <td colspan="2">Truk</td>
        <td colspan="2">Sepeda Motor</td>
      </tr>
      <tr>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;


    foreach ($data_kendaraan1 as $kendaraan1) 
    {
      $mobil1 = $kendaraan1->mobil1;
      $mobil2 = $kendaraan1->mobil2;
      $bus1 = $kendaraan1->bus1;
      $bus2 = $kendaraan1->bus2;
      $truk1 = $kendaraan1->truk1;
      $truk2 = $kendaraan1->truk2;
      $motor1 = $kendaraan1->motor1;
      $motor2 = $kendaraan1->motor2;

      echo '<tr>
          <td>'.$kendaraan1->kec.'</td>
          <td>'.$mobil1. '</td>
          <td>'.$mobil2.'</td>
          <td>'.$bus1. '</td>
          <td>'.$bus2.'</td>
          <td>'.$truk1. '</td>
          <td>'.$truk2.'</td>
          <td>'.$motor1. '</td>
          <td>'.$motor2.'</td>

         
          </tr>';
      $total1 += $kendaraan1->mobil1;
      $total2 += $kendaraan1->mobil2;
      $total3 += $kendaraan1->bus1;
      $total4 += $kendaraan1->bus2;
      $total5 += $kendaraan1->truk1;
      $total6 += $kendaraan1->truk2;
      $total7 += $kendaraan1->motor1;
      $total8 += $kendaraan1->motor2;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1">Jumlah</th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        <th><?=number_format($total7, 0, ',', '')?></th>
        <th><?=number_format($total8, 0, ',', '')?></th>
        
      </tr>
    </tfoot>
  </table>
<br><br>
  <table class="data-table table-responsive" style="position: relative; bottom: 30px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="3">Jumlah Kendaraan Bermotor Menurut Jenis Kendaraan di Kota Malang, 2015-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Jenis Kendaraan</td>
        <td colspan="2">Jumlah Kendaraan</td>
      </tr>
      <tr>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php


    foreach ($data_kendaraan2 as $kendaraan2) 
    {
      $tahun1 = $kendaraan2->tahun1;
      $tahun2 = $kendaraan2->tahun2;

      echo '<tr>
          <td>'.$kendaraan2->kendaraan.'</td>
          <td>'.$tahun1. '</td>
          <td>'.$tahun2.'</td>

         
          </tr>';

    }?> -->
    </tbody>
  </table>
<br><br>
  <table class="data-table table-responsive" style="position: relative; bottom: 30px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="7">Jumlah Kendaraan Bermotor Berdasarkan Plat Nomor di Kota Malang, 2015-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Jenis Kendaraan</td>
        <td colspan="2">Hitam</td>
        <td colspan="2">Kuning</td>
        <td colspan="2">Merah</td>
      </tr>
      <tr>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
  <!--   <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;

    foreach ($data_kendaraan3 as $kendaraan3) 
    {
      $hitam1 = $kendaraan3->hitam1;
      $hitam2 = $kendaraan3->hitam2;
      $kuning1 = $kendaraan3->kuning1;
      $kuning2 = $kendaraan3->kuning2;
      $merah1 = $kendaraan3->merah1;
      $merah2 = $kendaraan3->merah2;

      echo '<tr>
          <td>'.$kendaraan3->kendaraan.'</td>
          <td>'.$hitam1. '</td>
          <td>'.$hitam2.'</td>
          <td>'.$kuning1. '</td>
          <td>'.$kuning2.'</td>
          <td>'.$merah1. '</td>
          <td>'.$merah2.'</td>

         
          </tr>';
      $total1 += $kendaraan3->hitam1;
      $total2 += $kendaraan3->hitam2;
      $total3 += $kendaraan3->kuning1;
      $total4 += $kendaraan3->kuning2;
      $total5 += $kendaraan3->merah1;
      $total6 += $kendaraan3->merah2;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1">Jumlah</th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        
      </tr>
    </tfoot>
  </table>
  

      </div>
    </div>
  </div>
</div>
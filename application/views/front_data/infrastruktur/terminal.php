 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Terminal</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="height: 1070px"><br>

    <table class="data-table table-responsive" style="position: relative; bottom: 30px;" width="91%">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 18px;">
        <td colspan="7">Jumlah Terminal, Uji KIR, Lama Pengujian KIR, Fasilitas Perlengkapan Jalan, dan Trayek di Kota Malang, 2014-2016</td>
      </tr>
      <tr style="font-size: 16px;">
        <th><center>No</center></th>
        <th><center>Uraian</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
      <tr>
        <th style="background-color: #cbdaef; color: black;"><center>1</center></th>
        <th style="background-color: #cbdaef; color: black;" colspan="7">Jumlah Terminal (unit)</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_terminal_bgn1 as $terminal_bgn1)
    {

      echo '<tr style="font-size: 17px;">
          <td><center>'.$terminal_bgn1->no.'</center></td>
          <td width="509">'.$terminal_bgn1->uraian.'</td>
          <td><center>'.$terminal_bgn1->tahun1.'</center></td>
          <td><center>'.$terminal_bgn1->tahun2.'</center></td>
          <td><center>'.$terminal_bgn1->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 60px;" width="91%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;"><center>2</center></th>
        <th style="background-color: #cbdaef; color: black;" colspan="7">Jumlah Uji KIR (unit)</th>
      </tr>
    </thead>
    <tbody>
<!--     <?php

    $tot1 = 0;
    foreach ($data_terminal_bgn2 as $terminal_bgn2)
    {

      echo '<tr style="font-size: 17px;">
          <td width="85"><center>'.$terminal_bgn2->no.'</center></td>
          <td width="509">'.$terminal_bgn2->uraian.'</td>
          <td><center>'.$terminal_bgn2->tahun1.'</center></td>
          <td><center>'.$terminal_bgn2->tahun2.'</center></td>
          <td><center>'.$terminal_bgn2->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 90px;" width="91%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;"><center>3</center></th>
        <th style="background-color: #cbdaef; color: black;" colspan="7">Lama Pengujian KIR (bulan)</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_terminal_bgn3 as $terminal_bgn3)
    {

      echo '<tr style="font-size: 17px;">
          <td width="85"><center>'.$terminal_bgn3->no.'</center></td>
          <td width="509">'.$terminal_bgn3->uraian.'</td>
          <td><center>'.$terminal_bgn3->tahun1.'</center></td>
          <td><center>'.$terminal_bgn3->tahun2.'</center></td>
          <td><center>'.$terminal_bgn3->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 120px;" width="91%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;"><center>4</center></th>
        <th style="background-color: #cbdaef; color: black;" colspan="7">Fasilitas Perlengkapan Jalan</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_terminal_bgn45 as $terminal_bgn45)
    {

      echo '<tr style="font-size: 17px;">
          <td width="85"><center>'.$terminal_bgn45->no.'</center></td>
          <td width="509">'.$terminal_bgn45->uraian.'</td>
          <td><center>'.$terminal_bgn45->tahun1.'</center></td>
          <td><center>'.$terminal_bgn45->tahun2.'</center></td>
          <td><center>'.$terminal_bgn45->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

      </div>
    </div>
  </div>
</div>  
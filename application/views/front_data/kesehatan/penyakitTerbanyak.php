  <link rel="stylesheet" href="<?php echo base_url();?>/assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/style.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/charts/css/style.css">

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Penyakit Dengan Jumlah Kasus Terbanyak</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Selain kasus balita yang mengalami kurang gizi dan gizi buruk di Kota Malang yang perlu 
untuk ditindaklanjuti, ternyata masih ada 10 (sepuluh) penyakit dengan jumlah kasus yang paling 
banyak yang perlu mendapatkan perhatian bersama terutama pihak berwenang yang terkait di 
Kota Malang. Pada tahun 2016, penyakit dengan jumlah kasus terbanyak di Kota Malang adalah 
penyakit infeksi saluran pernapasan, dengan jumlah kasus sebanyak 55.531 kasus. Infeksi saluran 
pernapasan akut atau sering disebut (ISPA) adalah infeksi yang mengganggu proses pernapasan 
seseorang. Infeksi ini umumnya disebabkan oleh virus yang menyerang hidung, trakea (pipa 
pernapasan), atau bahkan paru-paru. ISPA menyebabkan fungsi pernapasan menjadi terganggu 
dan jika tidak segera ditangani, infeksi dapat menyebar ke seluruh sistem pernapasan dan 
menyebabkan tubuh tidak mendapatkan oksigen. Kondisi ini bisa berakibat fatal bahkan bisa 
sampai berujung pada kematian. Jenis penyakit dengan jumlah kasus terbanyak kedua di Kota Malang adalah penyakit 
Hipertensi Primer dengan jumlah kasus sebanyak 32.109 kasus. Hipertensi Primer adalah jenis 
hipertensi yang tidak diketahui secara pasti penyebabnya dan erat kaitannya dengan faktor 
keturunan keluarga. Seseorang yang di dalam keluarganya terindikasi penyakit hipertensi, secara 
otomatis penyakit ini akan diturunkan pada keturunan berikutnya.
  </p>
</section> 
    <br>
    <center><div class="box box-solid box-primary" style="width: 930px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Sepuluh Penyakit dengan Jumlah Kasus Terbanyak di Kota Malang Tahun 2016</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div>
                <div id="containering"></div>
              </div>
            </div>
          </div></center>

      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url();?>/assets/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>/assets/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>/assets/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>/assets/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>/assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>/assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>/assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>/assets/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>/assets/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>/assets/js/loading.js"></script>
<script src="<?php echo base_url();?>/assets/charts5/view2.js"></script>
<script src="<?php echo base_url();?>/assets/charts5/views2.js"></script>
<script src="<?php echo base_url();?>/assets/charts5/view2.min.js"></script>

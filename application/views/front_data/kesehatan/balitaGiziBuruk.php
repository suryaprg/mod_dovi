<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Balita Kurang Gizi dan Gizi Buruk</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Jumlah balita di Kota Malang ternyata masih ada yang mengalami kurang gizi dan gizi 
buruk. Berdasarkan data dari Dinas Kesehatan Kota Malang, jumlah balita yang mengalami 
kurang gizi di Kota Malang dari tahun 2014-2016 cenderung meningkat, tahun 2016 jumlah balita 
yang mengalami kurang gizi sebanyak 1.868 balita mengalami peningkatan sebanyak 241 balita 
atau naik sebanyak 14,81 persen jika dibandingkan dengan tahun 2015 sebanyak 1.627 balita. 
Kasus kekurangan gizi ini perlu mendapatkan perhatian yang serius dan perlu segera 
ditindaklanjuti oleh pihat terkait yang berwenang mengingat kekurangan gizi ini merupakan 
gangguan kesehatan yang serius dan jumlah balita yang mengalami kurang gizi dalam kurun 
waktu 3 (tiga) tahun terakhir cenderung mengalami peningkatan. Upaya pemerintah Kota Malang dalam mengatasi kasus gizi buruk di Kota Malang 
membuahkan hasil, berdasarkan data dari Dinas Kesehatan Kota Malang jumlah balita yang 
menderita gizi buruk di Kota Malang mengalami penurunan secara berturut-turut dari tahun 
2014-2016. Pada tahun 2016 jumlah balita yang menderita gizi buruk sebanyak 1.868 balita, 
mengalami penurunan sebanyak 241 balita atau turun sebanyak 14,81 persen jika dibandingkan 
dengan tahun 2015 yang sebanyak 1.627 balita. Penurunan jumlah kasus balita yang mengalami 
gizi buruk tidak lantas membuat kita berhenti pada tahapan tersebut karena masih banyak balita yang menderita kurang gizi di Kota Malang dan sudah mendapatkan perawatan namun 
jumlahnya justru dalam kurun waktu 3 (tiga) tahun terakhir mengalami peningkatan.  
  </p>
</section> 
    <br>
    <center><div class="box box-solid box-primary" style="width: 750px;">
            <div class="box-header with-border">
              <center><h3 class="box-title"> Jumlah Balita yang Kurang Gizi dan Gizi Buruk di Kota Malang Tahun 2014-2016</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div><br>
                <div id="contain"></div>
              </div>
            </div>

          </div></center><br>

    <table class="data-table" style="position: relative; bottom: 30px; width: 920px;">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 19px;">
        <td colspan="7">Jumlah Balita yang Kurang Gizi dan Gizi Buruk di Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr style="font-size: 16px;">
        <th>No</th>
        <th><center>Uraian</center></th>
        <th>Satuan</th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_balita_gizi as $balita_gizi)
    {

      echo '<tr style="font-size: 17px;">
          <td>'.$balita_gizi->id.'</td>
          <td>'.$balita_gizi->uraian.'</td>
          <td><center>'.$balita_gizi->satuan.'</center></td>
          <td><center>'.$balita_gizi->tahun1.'</center></td>
          <td><center>'.$balita_gizi->tahun2.'</center></td>
          <td><center>'.$balita_gizi->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>
      </div>
    </div>
  </div>
</div>
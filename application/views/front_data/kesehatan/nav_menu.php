<!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><i class="fa fa-clipboard"></i>  KATEGORI</li>

        <li <?php if($this->uri->segment(2) == 'kesehatan_gizi_balita') echo "class='active'";?>>
          <a href="<?= base_url()."data/kesehatan_gizi_balita/".date("Y");?>">
            <i class="fa fa-circle"></i>Balita Kurang Gizi dan Gizi Buruk
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'jenis_penyakit') echo "class='active'";?>>
          <a href="<?= base_url()."data/jenis_penyakit/".date("Y");?>">
             <i class="fa fa-circle"></i> Penyakit Dengan Jumlah
            <br>
              Kasus Terbanyak
          </a>
        </li>
         <li class="header"><i class="fa fa-list"></i>  LAMPIRAN</li>
             <li <?php if($this->uri->segment(2) == 'lp_kesehatan_all') echo "class='active'";?>>
          <a href="<?= base_url()."data/lp_kesehatan_all/1/".date("Y");?>">
             <i class="fa fa-circle"></i> Lampiran Data Kesehatan
          </a>
        </li>
      </ul>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KESEHATAN</title>
    <link rel="shortcut icon" href="https://malangkota.go.id/wp-content/themes/malangkotanew/images/favicon.png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->

    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts5/datatable.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/css/style.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts/css/style.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="fh5co-loader"></div>
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="index2.html" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>A</b>LT</span>
                <!-- logo for regular state and mobile devices -->
                <div style="position: absolute; left: 50px; top: 0.1px;"><img height="47px" style="padding: 5px" src="<?php echo base_url();?>assets/template_aw/dist/img/ncc-logo-white.png" alt="NCC logo"></div>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <li class="dropdown user user-menu">
                            <a href="<?php echo base_url();?>web_home/web">
                                <div style="position: absolute; top: 11px; right: 130px;"><img src="<?php echo base_url();?>assets/template_aw/dist/img/logo.png" width="25" height="25" alt="User Image"></div>&nbsp &nbsp &nbsp &nbsp &nbsp
                                <span class="hidden-xs">Malang Open Data</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo base_url();?>assets/template_aw/dist/img/logo.png" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>KESEHATAN</p>
                        <a href="#"><i class="fa fa-circle text-success"></i>Pemerintah Kota Malang</a>
                    </div>
                </div>
                <br>
                 <?php include "nav_menu.php";?>
            </section>
            <!-- /.sidebar -->
        </aside>

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
            </section>
            <div class="content body" style="margin-right: 3em; margin-left: 3em;">
                <div class="box box-solid">
                    <div class="box-header with-border">
                       <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Balita Kurang Gizi dan Gizi Buruk</h1>
                <h2 align="center" > <?php print_r($th_st." - ".$th_fn); ?>     </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <section>
                            <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Jumlah balita di Kota Malang ternyata masih ada yang mengalami kurang gizi dan gizi buruk. Berdasarkan data dari Dinas Kesehatan Kota Malang, jumlah balita yang mengalami kurang gizi di Kota Malang dari tahun <?= $th_st." - ".$th_fn;?> cenderung meningkat, tahun 2016 jumlah balita yang mengalami kurang gizi sebanyak 1.868 balita mengalami peningkatan sebanyak 241 balita atau naik sebanyak 14,81 persen jika dibandingkan dengan tahun 2015 sebanyak 1.627 balita. Kasus kekurangan gizi ini perlu mendapatkan perhatian yang serius dan perlu segera ditindaklanjuti oleh pihat terkait yang berwenang mengingat kekurangan gizi ini merupakan gangguan kesehatan yang serius dan jumlah balita yang mengalami kurang gizi dalam kurun waktu 3 (tiga) tahun terakhir cenderung mengalami peningkatan. Upaya pemerintah Kota Malang dalam mengatasi kasus gizi buruk di Kota Malang membuahkan hasil, berdasarkan data dari Dinas Kesehatan Kota Malang jumlah balita yang menderita gizi buruk di Kota Malang mengalami penurunan secara berturut-turut dari tahun <?= $th_st." - ".$th_fn;?>. Pada tahun 2016 jumlah balita yang menderita gizi buruk sebanyak 1.868 balita, mengalami penurunan sebanyak 241 balita atau turun sebanyak 14,81 persen jika dibandingkan dengan tahun 2015 yang sebanyak 1.627 balita. Penurunan jumlah kasus balita yang mengalami gizi buruk tidak lantas membuat kita berhenti pada tahapan tersebut karena masih banyak balita yang menderita kurang gizi di Kota Malang dan sudah mendapatkan perawatan namun jumlahnya justru dalam kurun waktu 3 (tiga) tahun terakhir mengalami peningkatan.
                            </p>
                        </section>
                                  <center>
                            <table class="data-table" style="width: 50%;">
                                <caption class="title"></caption>
                                <thead>
                                    <tr style="font-size: 19px;">
                                        <td colspan="3">Jumlah Balita yang Kurang Gizi dan Gizi Buruk di Kota Malang Tahun <?= $th_st." - ".$th_fn;?> (Jiwa)</td>
                                    </tr>
                                    <tr style="font-size: 16px;">
                                        <th>No</th>
                                        <th>Jumlah Penduduk Kurang Gizi</th>
                                        <th>Jumlah Penduduk Gizi Buruk</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                                if(isset($list_data)){
                                                    if($list_data){
                                                        // print_r("<pre>");
                                                        // print_r($list_data);
                                                        $no = 0;
                                                        foreach ($list_data as $key => $value) {
                                                            echo "<tr>
                                                                    <td>".$value["th"]."</td>
                                                                    <td>".number_format($value["jmh_kurang_gizi"], 0, ".", ",")."</td>
                                                                    <td>".number_format($value["jml_gizi_buruk"], 0, ".", ",")."</td>
                                                                </tr>";
                                                            $no++;
                                                        }
                                                    }
                                                }
                                    ?>
                                </tbody>
                            </table>
                                 <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/kesehatan_gizi_balita/".($th_fn));?>" id="btn_next" target="_blank" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                        </center>

                        <br><br>
                        <br>
                        <center>
                            <div class="box box-solid box-primary" style="width: 750px;">
                                <div class="box-header with-border">
                                    <center>
                                        <h3 class="box-title"> Jumlah Balita yang Kurang Gizi dan Gizi Buruk di Kota Malang Tahun <?= $th_st." - ".$th_fn;?> (Jiwa)</h3></center>
                                    <div class="box-tools pull-right">
                                    </div>
                                </div>
                                <div class="box-body chart-responsive">
                                    <div>
                                        <br>
                                        <div id="contain"></div>
                                    </div>
                                </div>


                            </div>
                            <h4 class="box-title"> Sumber : Dinas Kesehatan Kota Malang</h4>
                        </center>
                        <br>

                        <!-- <table class="data-table" style="position: relative; bottom: 30px; width: 920px;">
                            <caption class="title"></caption>
                            <thead>
                                <tr style="font-size: 19px;">
                                    <td colspan="7">Jumlah Balita yang Kurang Gizi dan Gizi Buruk di Kota Malang Tahun <?= $th_st." - ".$th_fn;?></td>
                                </tr>
                                <tr style="font-size: 16px;">
                                    <th>No</th>
                                    <th>
                                        <center>Uraian</center>
                                    </th>
                                    <th>Satuan</th>
                                    <th>
                                        <center>2014</center>
                                    </th>
                                    <th>
                                        <center>2015</center>
                                    </th>
                                    <th>
                                        <center>2016</center>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table> -->
                    </div>
                </div>
            </div>
        </div>

        <footer class="main-footer">
            <strong>Copyright &copy; 2018 <a href="http://kominfo.malangkota.go.id/" target="_blank">Kominfo Kota Malang</a>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts5/view1.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/template_aw/charts5/view1.js"></script> -->
<script type="text/javascript">
       $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/kesehatan_gizi_balita/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/kesehatan_gizi_balita/".($th_fn-1));?>';
            window.location.href = prev_url;
        });

  $(function () {
  $('#contain').highcharts({
    title: {
      text: '',
      x: -20 //center
    },
    colors: ['blue', 'red'],
    plotOptions: {
      line: {
        lineWidth: 3
      },
      tooltip: {
        hideDelay: 200
      }
    },
    subtitle: {
      text: '',
      x: -20
    },
    xAxis: {
      categories: <?= $val_th;?>
    },
    yAxis: {
      title: {
        text: ''
      },
      plotLines: [{
        value: 0,
        width: 1
      }]
    },
    tooltip: {
      valueSuffix: '',
      crosshairs: true,
      shared: true
    },
    legend: {
      layout: 'vertical',
      align: 'center',
      verticalAlign: 'bottom',
      borderWidth: 0
    },
    series: [
    {
      name: 'Jumlah Balita Kurang Gizi',
      color: 'rgba(0,0,0,0.75)',
      lineWidth: 2,
      marker: {
        radius: 6
      },
      data: <?= $val_buruk;?>
    },
    {
      name: 'Jumlah Balita Gizi Buruk',
      color: 'rgba(165,6,149,0.75)',
      lineWidth: 2,
      data: <?= $val_kurang;?>
    } ]
  });
});
</script>
</body>
</html>
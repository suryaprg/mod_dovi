<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KESEHATAN</title>
    <link rel="shortcut icon" href="https://malangkota.go.id/wp-content/themes/malangkotanew/images/favicon.png">

    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts8/tabeldata.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/css/style.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/template_aw/charts/css/style.css">

    <link rel="stylesheet" href="<?= base_url();?>assets/chart_main/amchart_vertical/export.css" type="text/css" media="all" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<td></td>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="fh5co-loader"></div>
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="index2.html" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>A</b>LT</span>
                <!-- logo for regular state and mobile devices -->
                <div style="position: absolute; left: 50px; top: 0.1px;"><img height="47px" style="padding: 5px" src="<?php echo base_url();?>assets/template_aw/dist/img/ncc-logo-white.png" alt="NCC logo"></div>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <li class="dropdown user user-menu">
                            <a href="<?php echo base_url();?>web_home/web">
                                <div style="position: absolute; top: 11px; right: 130px;"><img src="<?php echo base_url();?>assets/template_aw/dist/img/logo.png" width="25" height="25" alt="User Image"></div>&nbsp &nbsp &nbsp &nbsp &nbsp
                                <span class="hidden-xs">Malang Open Data</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo base_url();?>assets/template_aw/dist/img/logo.png" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>KESEHATAN</p>
                        <a href="#"><i class="fa fa-circle text-success"></i>Pemerintah Kota Malang</a>
                    </div>
                </div>
                <br>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <?php include "nav_menu.php";?>
            </section>
            <!-- /.sidebar -->
        </aside>

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
            </section>
            <div class="content body" style="margin-right: 3em; margin-left: 3em;">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Penyakit Dengan Jumlah Kasus Terbanyak</h1>
                <h2 align="center" > <?php print_r($th_st." - ".$th_fn); ?>     </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
                       
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <section>
                            <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Selain kasus balita yang mengalami kurang gizi dan gizi buruk di Kota Malang yang perlu untuk ditindaklanjuti, ternyata masih ada 10 (sepuluh) penyakit dengan jumlah kasus yang paling banyak yang perlu mendapatkan perhatian bersama terutama pihak berwenang yang terkait di Kota Malang. Pada tahun <?=$th_fn;?>, penyakit dengan jumlah kasus terbanyak di Kota Malang adalah penyakit infeksi saluran pernapasan, dengan jumlah kasus sebanyak 55.531 kasus. Infeksi saluran pernapasan akut atau sering disebut (ISPA) adalah infeksi yang mengganggu proses pernapasan seseorang. Infeksi ini umumnya disebabkan oleh virus yang menyerang hidung, trakea (pipa pernapasan), atau bahkan paru-paru. ISPA menyebabkan fungsi pernapasan menjadi terganggu dan jika tidak segera ditangani, infeksi dapat menyebar ke seluruh sistem pernapasan dan menyebabkan tubuh tidak mendapatkan oksigen. Kondisi ini bisa berakibat fatal bahkan bisa sampai berujung pada kematian. Jenis penyakit dengan jumlah kasus terbanyak kedua di Kota Malang adalah penyakit Hipertensi Primer dengan jumlah kasus sebanyak 32.109 kasus. Hipertensi Primer adalah jenis hipertensi yang tidak diketahui secara pasti penyebabnya dan erat kaitannya dengan faktor keturunan keluarga. Seseorang yang di dalam keluarganya terindikasi penyakit hipertensi, secara otomatis penyakit ini akan diturunkan pada keturunan berikutnya.
                            </p>
                        </section>
                        
                        <center>
                            <div class="box-body chart-responsive" style="width: 61%;">
                                <table class="data-table" width="100%">
                                    <caption class="title"></caption>
                                    <thead>
                                        <tr style="font-size: 19px;">
                                            <td colspan="3">Sepuluh Penyakit dengan Jumlah Kasus Terbanyak di Kota Malang Tahun <?=$th_fn;?>(Jiwa)</td>
                                        </tr>
                                        <tr style="font-size: 16px;">
                                            <td>No</td>
                                            <td>Nama Penyakit</td>
                                            <td>Total penderita</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            // print_r("<pre>");
                                            // print_r($list_data);

                                            if(isset($list_data)){
                                                if($list_data){
                                                    $no = 0;
                                                    foreach ($list_data as $key => $value) {
                                                        echo "<tr>
                                                                <td>".$no."</td>
                                                                <td>".$value->keterangan."</td>
                                                                <td>".number_format($value->jml, 0, ".", ",")."</td>
                                                            </tr>";
                                                        $no++;
                                                    }
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                                     <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/jenis_penyakit/".($th_fn));?>" id="btn_next" target="_blank" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                            </div>
                        </center>
                        <br><br>
                        <center>
                            <div class="box box-solid box-primary" style="width: 60%;">
                                <div class="box-header with-border">
                                    <center>
                                        <h3 class="box-title">Sepuluh Penyakit dengan Jumlah Kasus Terbanyak di Kota Malang Tahun <?=$th_fn;?> (Jiwa)</h3></center>
                                    <div class="box-tools pull-right">
                                    </div>
                                </div>
                                <div class="box-body chart-responsive">
                                    <div>
                                        <div id="containering"></div>
                                    </div>
                                </div>
                            </div>
                        </center>
                        <h4 align="center">Sumber : Dinas Kesehatan Kota Malang</h4>
                        

                    </div>
                </div>
            </div>
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <strong>Copyright &copy; 2018 <a href="http://kominfo.malangkota.go.id/" target="_blank">Kominfo Kota Malang</a>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts5/view2.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/template_aw/charts5/views2.js"></script> -->
<script type="text/javascript">
       $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/jenis_penyakit/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/jenis_penyakit/".($th_fn-1));?>';
            window.location.href = prev_url;
        });

    $(function () {
        $('#containering').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '---'
            },
            xAxis: {
                categories: [<?=$str_penyakit;?>]
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -410,
                verticalAlign: 'top',
                y: 0,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        this.series.name +': '+ this.y +'<br/>'+
                        'Total: '+ this.point.stackTotal;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: [{
                name: 'Kasus',
                data: [<?=$str_val;?>]
            }  ]
        });
    });
</script>
</body>
</html>
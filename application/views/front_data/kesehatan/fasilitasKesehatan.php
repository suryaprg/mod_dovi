  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Fasilitas Kesehatan</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body box-responsive" style="height: 1500px"><br>

    <table class="data-table table-responsive" style="position: relative; bottom: 30px;" width="92.3%">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 19px;">
        <td colspan="7">Fasilitas Kesehatan di Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr style="font-size: 16px;">
        <th><center>Uraian</center></th>
        <th><center>Satuan</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
      <tr>
        <th style="background-color: #cbdaef; color: black;" colspan="7">1. Posyandu</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_fasilitas_kesehatan1 as $fasilitas_kesehatan1)
    {

      echo '<tr style="font-size: 17px;">
          <td width="509">'.$fasilitas_kesehatan1->uraian.'</td>
          <td width="104"><center>'.$fasilitas_kesehatan1->satuan.'</center></td>
          <td><center>'.$fasilitas_kesehatan1->tahun1.'</center></td>
          <td><center>'.$fasilitas_kesehatan1->tahun2.'</center></td>
          <td><center>'.$fasilitas_kesehatan1->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 60px;" width="92.3%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;" colspan="7">3. Puskesmas</th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php

    $tot1 = 0;
    foreach ($data_fasilitas_kesehatan2 as $fasilitas_kesehatan2)
    {

      echo '<tr style="font-size: 17px;">
          <td width="509">'.$fasilitas_kesehatan2->uraian.'</td>
          <td width="104"><center>'.$fasilitas_kesehatan2->satuan.'</center></td>
          <td><center>'.$fasilitas_kesehatan2->tahun1.'</center></td>
          <td><center>'.$fasilitas_kesehatan2->tahun2.'</center></td>
          <td><center>'.$fasilitas_kesehatan2->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 90px;" width="92.3%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;" colspan="7">4. Rumah Sakit Umum Daerah</th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php

    $tot1 = 0;
    foreach ($data_fasilitas_kesehatan3 as $fasilitas_kesehatan3)
    {

      echo '<tr style="font-size: 17px;">
          <td width="509">'.$fasilitas_kesehatan3->uraian.'</td>
          <td width="104"><center>'.$fasilitas_kesehatan3->satuan.'</center></td>
          <td><center>'.$fasilitas_kesehatan3->tahun1.'</center></td>
          <td><center>'.$fasilitas_kesehatan3->tahun2.'</center></td>
          <td><center>'.$fasilitas_kesehatan3->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 120px;" width="92.3%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;" colspan="7">5. Rumah Sakit Umum Swasta</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_fasilitas_kesehatan4 as $fasilitas_kesehatan4)
    {

      echo '<tr style="font-size: 17px;">
          <td width="509">'.$fasilitas_kesehatan4->uraian.'</td>
          <td width="104"><center>'.$fasilitas_kesehatan4->satuan.'</center></td>
          <td><center>'.$fasilitas_kesehatan4->tahun1.'</center></td>
          <td><center>'.$fasilitas_kesehatan4->tahun2.'</center></td>
          <td><center>'.$fasilitas_kesehatan4->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 150px;" width="92.3%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;" colspan="7">6. Rumah Sakit Khusus</th>
      </tr>
    </thead>
    <tbody>
  <!--   <?php

    $tot1 = 0;
    foreach ($data_fasilitas_kesehatan5 as $fasilitas_kesehatan5)
    {

      echo '<tr style="font-size: 17px;">
          <td>'.$fasilitas_kesehatan5->uraian.'</td>
          <td><center>'.$fasilitas_kesehatan5->satuan.'</center></td>
          <td><center>'.$fasilitas_kesehatan5->tahun1.'</center></td>
          <td><center>'.$fasilitas_kesehatan5->tahun2.'</center></td>
          <td><center>'.$fasilitas_kesehatan5->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 180px;" width="92.3%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;" colspan="7">9. Rumah Sakit Tentara</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_fasilitas_kesehatan6 as $fasilitas_kesehatan6)
    {

      echo '<tr style="font-size: 17px;">
          <td width="509">'.$fasilitas_kesehatan6->uraian.'</td>
          <td width="104"><center>'.$fasilitas_kesehatan6->satuan.'</center></td>
          <td><center>'.$fasilitas_kesehatan6->tahun1.'</center></td>
          <td><center>'.$fasilitas_kesehatan6->tahun2.'</center></td>
          <td><center>'.$fasilitas_kesehatan6->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

      </div>
    </div>
  </div>
</div>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Pengeluaran Penduduk</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Dalam kacamata ekonomi, kesejahteraan penduduk dapat dilihat dari besaran 
pendapatannya. Semakin tinggi pendapatan perkapita penduduk, dianggap semakin sejahtera. 
Namun untuk memperoleh informasi tentang pendapatan rumahtangga sangatlah sulit, kegiatan 
Survei Sosial Ekonomi Nasional (Susenas) yang dilakukan oleh Badan Pusat Statistik bermanfaat 
dalam mengukur kesejahteraan dengan menggunakan pendekatan pengeluaran. Secara umum 
jumlah pengeluaran berbanding lurus dengan pendatapatan. Rumahtangga yang 
pengeluarannya banyak tentunya mempunyai pendapatan yang besar pula, kondisi ini dapat 
mencerminkan tingkat kemampuan ekonomi masyarakat. Kemampuan daya beli masyarakat dapat memberikan gambaran tentang tingkat 
kesejahteraan masyarakat. Semakin tinggi daya beli masyarakat menunjukkan peningkatan 
kemampuan dalam memenuhi kebutuhan hidupnya dan menjadi salah satu indikasi peningkatan 
kesejahteraan masyarakat. Berdasarkan data dari BPS Kota Malang, hasil Susenas tahun 2014
2016 pada data kelompok pengeluaran menunjukkan adanya kenaikan persentase penduduk 
pada kelompok pengeluaran 1.000.000 rupiah ke atas perkapita per bulan di Kota Malang.
  </p>
</section> 
    <br>
    <center><div class="box box-solid box-primary" style="width: 820px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Persentase Penduduk menurut Golongan Pengeluaran Perkapita di Kota Malang Tahun 2014-2016</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div>
                <div id="contains"></div>
              </div>
            </div>
          </div></center><br><br>

<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Pergeseran persentase pengeluaran rumah tangga dari kelas pengeluaran yang lebih 
rendah ke kelas pengeluaran yang lebih tinggi, mengandung dua kondisi, yaitu pertama terjadi 
karena adanya peningkatan kesejahteraan rumah tangga atau kedua karena adanya peningkatan 
harga berbagai kebutuhan rumah tangga. Meningkatnya kesejahteraan penduduk biasanya juga 
ditandai dengan semakin berkurangnya proporsi pengeluaran untuk keperluan makanan yang 
selanjutnya bergeser pada pengeluaran untuk keperluan bukan makanan.
  </p>
</section>
  <br>
    <center><div class="box box-solid box-primary" style="width: 820px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Persentase Pengeluaran Rumah Tangga di Kota Malang Tahun 2014-2016</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div>
                <div id="containing"></div>
              </div>
            </div>
          </div></center><br>

      </div>
    </div>
  </div>
</div>

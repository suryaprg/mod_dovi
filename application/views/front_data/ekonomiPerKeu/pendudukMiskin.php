<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;" align="justify">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Jumlah Penduduk Miskin</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Berdasarkan data dari BPS Kota Malang, Jumlah penduduk miskin di Kota Malang dalam 
kurun waktu 3 (tiga) tahun terakhir mengalami penurunan. Tahun 2016 terdapat 37,03 ribu jiwa 
penduduk miskin, turun sebesar 2,07 ribu jiwa jika dibandingkan tahun 2015. Sedangkan bila 
dibandingkan dengan tahun 2014 turun sekitar 3,61 ribu jiwa. Persentase penduduk miskin juga 
mengalami penurunan 0,27 persen poin pada tahun 2016 dibandingkan dengan tahun 
sebelumnya, dimana tahun 2015 terdapat 4,60 persen penduduk miskin menjadi 4,33 persen 
pada tahun 2016 seperti disajikan pada grafik di atas. Sedangkan bila dibandingkan dengan tahun 
2014, turun sekitar 0,47 persen, yaitu dari 4,80 persen ke 4,33 persen.
  </p>
</section> 
    <br>
    <center><div class="box box-solid box-primary" style="width: 660px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Persentase dan Jumlah Penduduk Miskin di Kota Malang Tahun 2014-2016</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body">
              <br>
              <div id="Contain" style="height: 300px; width: 100%;"></div>
              <img  height="20px" style="position: absolute; bottom: 40px; left: 0px;" src="<?php echo base_url();?>/assets/dist/img/mark.png" alt="mark1">
              <img  height="20px" style="position: absolute; bottom: 40px; left: 540px;" src="<?php echo base_url();?>/assets/dist/img/mark.png" alt="mark2">
            </div>
            <div class="box-footer">
            <i class="fa fa-square" style="color: #7ba9f2"></i> Persentase (PD)
            &nbsp&nbsp&nbsp
            <i class="fa fa-circle" style="color: #c0504e"></i> Jumlah (000)
          </div>
          </div></center>

      </div>
    </div>
  </div>
</div>
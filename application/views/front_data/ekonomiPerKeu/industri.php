<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Data Industri</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="height: 920px">
      <center><div>
      <div class="box-body chart-responsive">

  <table class="data-table table-responsive" style="position: relative; bottom: 30px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 20px;">
        <td colspan="7">Rekapan Pendataan Industri menurut Jenis Industri di Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr style="font-size: 20px;">
        <th><center>Uraian</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
      <tr>
        <th colspan="7" style="background-color: #cbdaef; color: black;">Industri Besar(>10 Miliar)</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_industri_bgn1 as $industri_bgn1)
    {

      $uraian = $industri_bgn1->uraian;
      $tahun1 = $industri_bgn1->tahun1 == 0 ? '' : number_format($industri_bgn1->tahun1, 0, ',', '.');
      $tahun2 = $industri_bgn1->tahun2 == 0 ? '' : number_format($industri_bgn1->tahun2, 0, ',', '.');
      $tahun3 = $industri_bgn1->tahun3 == 0 ? '' : number_format($industri_bgn1->tahun3, 0, ',', '.');

      echo '<tr style="font-size: 15px;">
          <td>'.$uraian.'</td>
          <td><center>'.$tahun1.'</center></td>
          <td><center>'.$tahun2.'</center></td>
          <td><center>'.$tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 60px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <th colspan="7" style="background-color: #cbdaef; color: black;">Industri Kecil Dan Menengah<br>(750 Jt s/d 10 Miliar)</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_industri_bgn2 as $industri_bgn2)
    {

      $uraian = $industri_bgn2->uraian;
      $tahun1 = $industri_bgn2->tahun1 == 0 ? '' : number_format($industri_bgn2->tahun1, 0, ',', '.');
      $tahun2 = $industri_bgn2->tahun2 == 0 ? '' : number_format($industri_bgn2->tahun2, 0, ',', '.');
      $tahun3 = $industri_bgn2->tahun3 == 0 ? '' : number_format($industri_bgn2->tahun3, 0, ',', '.');

      echo '<tr style="font-size: 15px;">
          <td width="295">'.$uraian.'</td>
          <td width="190"><center>'.$tahun1.'</center></td>
          <td width="185"><center>'.$tahun2.'</center></td>
          <td><center>'.$tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 90px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <th colspan="7" style="background-color: #cbdaef; color: black;">Sentra Industri</th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php

    $tot1 = 0;
    foreach ($data_industri_bgn3 as $industri_bgn3)
    {

      $uraian = $industri_bgn3->uraian;
      $tahun1 = $industri_bgn3->tahun1 == 0 ? '' : number_format($industri_bgn3->tahun1, 0, ',', '.');
      $tahun2 = $industri_bgn3->tahun2 == 0 ? '' : number_format($industri_bgn3->tahun2, 0, ',', '.');
      $tahun3 = $industri_bgn3->tahun3 == 0 ? '' : number_format($industri_bgn3->tahun3, 0, ',', '.');

      echo '<tr style="font-size: 15px;">
          <td width="295">'.$uraian.'</td>
          <td width="190"><center>'.$tahun1.'</center></td>
          <td width="185"><center>'.$tahun2.'</center></td>
          <td><center>'.$tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 120px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <th colspan="7" style="background-color: #cbdaef; color: black;">Industri Non Formal</th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php

    $tot1 = 0;
    foreach ($data_industri_bgn4 as $industri_bgn4)
    {

      $uraian = $industri_bgn4->uraian;
      $tahun1 = $industri_bgn4->tahun1 == 0 ? '' : number_format($industri_bgn4->tahun1, 0, ',', '.');
      $tahun2 = $industri_bgn4->tahun2 == 0 ? '' : number_format($industri_bgn4->tahun2, 0, ',', '.');
      $tahun3 = $industri_bgn4->tahun3 == 0 ? '' : number_format($industri_bgn4->tahun3, 0, ',', '.');

      echo '<tr style="font-size: 15px;">
          <td width="295">'.$uraian.'</td>
          <td width="190"><center>'.$tahun1.'</center></td>
          <td width="185"><center>'.$tahun2.'</center></td>
          <td><center>'.$tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 150px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <th colspan="7" style="background-color: #cbdaef; color: black;">Jumlah Industri</th>
      </tr>
    </thead>
    <tbody>
 <!--    <?php

    $tot1 = 0;
    foreach ($data_industri_bgn5 as $industri_bgn5)
    {

      $uraian = $industri_bgn5->uraian;
      $tahun1 = $industri_bgn5->tahun1 == 0 ? '' : number_format($industri_bgn5->tahun1, 0, ',', '.');
      $tahun2 = $industri_bgn5->tahun2 == 0 ? '' : number_format($industri_bgn5->tahun2, 0, ',', '.');
      $tahun3 = $industri_bgn5->tahun3 == 0 ? '' : number_format($industri_bgn5->tahun3, 0, ',', '.');

      echo '<tr style="font-size: 15px;">
          <td width="295">'.$uraian.'</td>
          <td width="190"><center>'.$tahun1.'</center></td>
          <td width="185"><center>'.$tahun2.'</center></td>
          <td><center>'.$tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

            </div>
        </div></center><br><br>

      </div>
      </div>
    </div> 
  </div>
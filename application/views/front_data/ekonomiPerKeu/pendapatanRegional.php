 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Pendapatan Regional</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Produk Domestik Regional Bruto (PDRB) merupakan salah satu indikator penting untuk 
mengetahui kondisi ekonomi di suatu daerah dalam suatu periode tertentu baik atas dasar harga 
berlaku maupun atas dasar harga konstan. PDRB pada dasarnya merupakan jumlah nilai tambah 
yang dihasilkan oleh seluruh unit usaha dalam suatu daerah tertentu, atau merupakan jumlah 
nilai barang dan jasa akhir yang dihasilkan oleh seluruh unit ekonomi pada suatu daerah. 
Pertumbuhan ekonomi merupakan salah satu indikator ekonomi yang paling ditunggu. 
Keberhasilan pembangunan tercermin melalui pertumbuhan ekonominya. Berdasarkan data dari 
Badan Pusat Statistik Kota Malang besaran PDRB atas dasar harga berlaku di Kota Malang tahun 
2016 mencapai Rp. 57.171.601,6 juta rupiah. Nilai PDRB atas dasar harga berlaku dari tahun 
2014-2016 mengalami kenaikan secara terus menerus.
  </p>
</section> 
    <br>
    <center><div class="box box-solid box-primary" style="width: 780px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">PDRB Kota Malang Atas Dasar Harga Berlaku Tahun 2014-2016</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div>
                <div id="contains"></div>
              </div>
            </div>
          </div></center><br><br>

<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Melalui penghitungan PDRB diketahui bahwa pada tahun 2016 pertumbuhan ekonomi 
Kota Malang atas dasar harga konstan tahun 2010 sebesar 5,61 persen. Pertumbuhan ekonomi 
tahun 2016 sama nilainya dengan tahun 2015, yang membedakan adalah rincian pertumbuhan 
ekonomi pada masing-masing lapangan usaha. Laju pertumbuhan tahun 2015  sebesar 5,61 
persen mengalami penurunan sebesar 0,19 jika dibandingkan dengan tahun 2014 sebesar 5,80 
persen.  Sumber pertumbuhan PDRB terbesar Kota Malang selama 3 (tiga) tahun mulai tahun 
2014-2016 berasal dari kegiatan lapangan usaha perdagangan besar dan eceran; reparasi mobil, 
dan sepeda motor dengan kontribusi, disusul oleh Industri pengolahan dengan kontribusi dan 
ketiga adalah lapangan usaha konstruksi.
  </p>
</section>
  <br>
    <center><div class="box box-solid box-primary" style="width: 780px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Tiga Kegiatan Lapangan Usaha dengan Kontribusi Terbesar di Kota Malang Tahun 2014-2016</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div><br>
                <div id="bar-chart"></div>
              </div>
            </div>
          </div></center><br>

      </div>
    </div>
  </div>
</div>
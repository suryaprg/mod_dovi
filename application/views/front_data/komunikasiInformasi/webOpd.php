 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Website OPD</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="height: 1070px"><br>

  
              <section><br>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Kebutuhan internet di Kota Malang juga merambah ke institusi pemerintah. Semakin
maju teknologi informasi, kebutuhan instansi pemerintah akan layanan teknologi informasi juga
semakin besar. Salah satu layanan teknologi informasi yang digunakan oleh Pemerintah Kota
Malang adalah layanan website. Kota Malang sudah memiliki website/domain tersendiri yaitu
www.malangkota.go.id, dimana di website ini dimuat segala publikasi, laporan, dan output lain
dari hasil kerja Pemerintah Kota Malang. Kemajuan teknologi informasi juga menuntut setiap
Organisasi Perangkat Daerah (OPD) di Kota Malang mempunyai website tersendiri, sehingga hasil
kerja bisa diketahui masyarakat dengan akses yang mudah dan membuat pelayanan masyarakat
menjadi maksimal. Jumlah OPD yang sudah memiliki website pun naik setiap tahunnya, yaitu
sebanyak 100 OPD di Tahun 2014 menjadi 107 OPD di tahun 2016. Selain website, jumlah
pelayanan internet di area publik Pemerintah Kota Malang-pun semakin naik setiap tahunnya.
Data menunjukkan jumlah pelayanan internet di area publik pemerintah Kota Malang pada tahun
2014 mencapai 11 lokasi dan naik menjadi 13 lokasi pada tahun 2016.
  </p>
</section>  

    <section class="content">
      <div class="row1">
        <div class="col-md-12">
          <center><div class="box box-solid box-primary" style="width: 850px;">
            <div class="box-header with-border">
              <center><h3 class="box-title"> Jumlah Website OPD Kota Malang Tahun 2014-2016</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body"><br>
              <div id="contaminer" style="width: 810px; height: 400px; margin: 0 auto"></div>
            </div>
          </div></center><br><br>
        </div>
      </div>

    </section>
      </div>
    </div>
  </div>
</div>  
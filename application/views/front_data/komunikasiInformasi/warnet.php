 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Warnet</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="height: 1070px"><br>

          <section><br>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
   Teknologi telah mempengaruhi gaya hidup dan pola pikir masyarakat sehingga bisa
mempengaruhi kehidupan sosial. Seperti contohnya fasilitas internet yang bisa di bilang hal wajib
saat ini membuatnya mendapat tempat sebagai pusat informasi dan menjadi wadah trend
pergaulan masyarakat kota besar di zaman modern ini, bukan lagi buku, koran, dll. Hal tersebut
dikarenakan manusia lebih senang dengan sesuatu yang praktis dan cepat. Perkembangan
teknologi di Kota Malang sangat cepat, banyak sarana dan prasarana teknologi yang sudah
tersedia di Kota Malang. Bahkan seluruh wilayah/kelurahan di Kota Malang sudah bisa digunakan
untuk mengakses internet, baik melalui jaringan kabel ataupun nirkabel. Akses wifi juga banyak
tersedia di fasilitas umum di Kota Malang. Hal ini ditunjang pula oleh jumlah warung internet
(warnet) yang setiap tahun semakin bertambah, yang menunjukkan bahwa kebutuhan internet
di Kota Malang cukup besar.
  </p>
</section>

<section class="content">
      <div class="row1">
        <div class="col-md-12">
          <center><div class="box box-solid box-primary" style="width: 850px;">
            <div class="box-header with-border">
              <center><h3 class="box-title"> Jumlah Warnet Kota Malang Tahun 2014-2016</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body"><br>
              <div id="contaminer" style="width: 810px; height: 400px; margin: 0 auto"></div>
            </div>
          </div></center><br><br>
        </div>
      </div>

    </section>

      </div>
    </div>
  </div>
</div>  
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Data Sistem Informasi</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="height: 1070px"><br>

   <table class="data-table table-responsive" style="position: relative; bottom: 30px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="7" style="font-size: 17px;">Data Sistem Informasi Pemerintah Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr>
        <td rowspan="2" colspan="3">uraian</td>
        <td ROWspan="2">Satuan</td>
        <td COLspan="3">Tahun</td>
        
      </tr>
      <tr>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      
       
      </tr>
    </thead>
    <tbody>
   <!--  <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;

    $tot1 = 0;
    foreach ($data_jalan as $jalan) 
    {
      $negara1 = $jalan->negara1;
      $negara2 = $jalan->negara2;
      $provinsi1 = $jalan->provinsi1;
      $provinsi2 = $jalan->provinsi2;
      $kota1 = $jalan->kota1;
      $kota2 = $jalan->kota2;

      echo '<tr>
          <td>'.$jalan->kondisi.'</td>
          <td>'.$negara1. '</td>
          <td>'.$negara2.'</td>
          <td>'.$provinsi1. '</td>
          <td>'.$provinsi2.'</td>
          <td>'.$kota1. '</td>
          <td>'.$kota2.'</td>

         
          </tr>';
      $total1 += $jalan->negara1;
      $total2 += $jalan->negara2;

      $total3 += $jalan->provinsi1;
      $total4 += $jalan->provinsi2;

      $total5 += $jalan->kota1;
      $total6 += $jalan->kota2;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1">Jumlah</th>
        <th><?=number_format($total1, 2, ',', '')?></th>
        <th><?=number_format($total2, 2, ',', '')?></th>
        <th><?=number_format($total3, 2, ',', '')?></th>
        <th><?=number_format($total4, 2, ',', '')?></th>
        <th><?=number_format($total5, 2, ',', '')?></th>
        <th><?=number_format($total6, 2, ',', '')?></th>
        
      </tr>
    </tfoot>
  </table>


      </div>
    </div>
  </div>
</div>  
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KOMUNIKASI DAN INFORMASIS</title>
    <link rel="shortcut icon" href="https://malangkota.go.id/wp-content/themes/malangkotanew/images/favicon.png">

    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts8/tabeldata.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/css/style.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/template_aw/charts/css/style.css">

    <link rel="stylesheet" href="<?= base_url();?>assets/chart_main/amchart_vertical/export.css" type="text/css" media="all" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- <div class="fh5co-loader"></div> -->
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="index2.html" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>A</b>LT</span>
                <!-- logo for regular state and mobile devices -->
                <div style="position: absolute; left: 50px; top: 0.1px;"><img height="47px" style="padding: 5px" src="<?php echo base_url();?>assets/template_aw/dist/img/ncc-logo-white.png" alt="NCC logo"></div>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <li class="dropdown user user-menu">
                            <a href="<?php echo base_url();?>web_home/web">
                                <div style="position: absolute; top: 11px; right: 130px;"><img src="<?php echo base_url();?>assets/template_aw/dist/img/logo.png" width="25" height="25" alt="User Image"></div>&nbsp &nbsp &nbsp &nbsp &nbsp
                                <span class="hidden-xs">Malang Open Data</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo base_url();?>assets/template_aw/dist/img/logo.png" alt="User Image">
                    </div>
                    <div class="pull-left info">
                       <p>KEPENDUDUKAN DAN 
                        KEMISKINAN</p>
                        <a href="#"><i class="fa fa-circle text-success"></i>Pemerintah Kota Malang</a>
                    </div>
                </div>
                <br>
                <?php include "nav_menu.php";?>
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
            </section>
            <div class="content body" style="margin-right: 3em; margin-left: 3em;">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Jumlah Penduduk Berdasarkan Umur</h1>
                <h2 align="center" > <?php print_r($th_st." - ".$th_fn); ?>     </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      
                        <br>
                        <section>
                            <br>
                            <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Kelompok usia produktif adalah penduduk yang berusia 15 sampai dengan 64 tahun. Perbandingan antara penduduk usia tidak produktif dengan usia produktif menunjukkan angka ketergantungan (age dependency ratio). Semakin tinggi rasio ketergantungan menunjukkan semakin berat pula beban yang ditanggung oleh penduduk di kelompok usia produktif. Berdasarkan data dari BPS Kota Malang Rasio Ketergantungan penduduk Kota Malang Tahun 2016 adalah 36,92 persen yang artinya setiap 100 penduduk usia produktif di Kota Malang menanggung sebanyak 36-37 penduduk usia tidak produktif.
                            </p>
                        </section>

                                    <?php 
                                        if(isset($list_data)){
                                            if($list_data){
                                                $var_th = "";
                                                for ($i=$th_st; $i <= $th_fn ; $i++) { 
                                                    if($i == $th_fn){
                                                        $var_th .= "'".$i."'";
                                                    }else{
                                                        $var_th .= "'".$i."',";
                                                    }
                                                }
                                            }
                                        }
                                        ?>


                        <br>
                        <center>
                            <div class="box-body chart-responsive">
                                <table class="data-table" width="71%">
                                    
                                    <thead>
                                        <?php print_r($str_header); ?>
                                    </thead>
                                    <tbody>                                 

                                    <?php
                                        print_r($str_tbl);
                                    ?>                      
                                    </tbody>
                                </table>
                                     <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/kependudukan_kel_umur/".($th_fn));?>" id="btn_next" target="_blank" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                            </div>
                        </center>

                        <center>
                            <div class="box box-solid box-primary" style="width: 70%;">
                                <div class="box-header with-border">
                                    <center>
                                        <h3 class="box-title"><?php print_r($title); ?></h3></center>

                                    <div class="box-tools pull-right">
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <br>
                                    <div id="chartdiv" style="width: 100%;"></div>
                                </div>
                            </div>
                        </center>
                        <h4 align="center">Sumber : BPS Kota Malang</h4>
                        <br>

                        <br>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong>Copyright &copy; 2018 <a href="http://kominfo.malangkota.go.id/" target="_blank">Kominfo Kota Malang</a>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>

<!-- Resources -->
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/core.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/charts.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/animated.js"></script>
    
    <script type="text/javascript">

           $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/kependudukan_kel_umur/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/kependudukan_kel_umur/".($th_fn-1));?>';
            window.location.href = prev_url;
        });


        var data_main = JSON.parse('<?php print_r($data_graph);?>');
        console.log(data_main);

        function create_graph(data_main){
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

             // Create chart instance
            var chart = am4core.create("chartdiv", am4charts.XYChart);

            // Add data
            chart.data = data_main;

            // Create axes
            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "year";
            categoryAxis.numberFormatter.numberFormat = "#";
            categoryAxis.renderer.inversed = true;
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.cellStartLocation = 0.1;
            categoryAxis.renderer.cellEndLocation = 0.9;

            var  valueAxis = chart.xAxes.push(new am4charts.ValueAxis()); 
            valueAxis.renderer.opposite = true;

            // Create series
            function createSeries(field, name) {
              var series = chart.series.push(new am4charts.ColumnSeries());
              series.dataFields.valueX = field;
              series.dataFields.categoryY = "year";
              series.name = name;
              series.columns.template.tooltipText = "{name}: Rp. [bold]{valueX}[/]";
              series.columns.template.height = am4core.percent(100);
              series.sequencedInterpolation = true;

              var valueLabel = series.bullets.push(new am4charts.LabelBullet());
              valueLabel.label.text = "{valueX}";
              valueLabel.label.horizontalCenter = "left";
              valueLabel.label.dx = 10;
              valueLabel.label.hideOversized = false;
              valueLabel.label.truncate = false;

              var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
              categoryLabel.label.text = "{name}";
              categoryLabel.label.horizontalCenter = "right";
              categoryLabel.label.dx = -10;
              categoryLabel.label.fill = am4core.color("#fff");
              categoryLabel.label.hideOversized = false;
              categoryLabel.label.truncate = false;
            }

            // createSeries("daya_beli", "Daya Beli");
            // createSeries("kesehatan", "Kesehatan");
            // createSeries("pendidikan", "Pendidikan");
            <?php print_r($series); ?>

        }


        create_graph(data_main);
    </script>
</body>
</html>
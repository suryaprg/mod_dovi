<!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><i class="fa fa-clipboard"></i>  KATEGORI</li>
  <!--       <li <?php if($this->uri->segment(2) == 'index_pembangunan_manusia') echo "class='active'";?>>
          <a href="<?= base_url()."data/index_pembangunan_manusia/". date("Y");?>">
            <span>Index Pembangunan Manusia</span>
          </a>
        </li> -->
        <li <?php if($this->uri->segment(2) == 'index_pembangunan_manusia') echo "class='active'";?>>
          <a href="<?= base_url()."data/index_pembangunan_manusia/". date("Y");?>">
             <i class="fa fa-circle"></i>Index Pembangunan Manusia
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'pengembangan_index') echo "class='active'";?>>
          <a href="<?= base_url()."data/pengembangan_index/". date("Y");?>">
             <i class="fa fa-circle"></i>Index Pendidikan, Kesehatan 
              <br>dan Daya Beli
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'jumlah_penduduk_miskin') echo "class='active'";?>>
          <a href="<?= base_url()."data/jumlah_penduduk_miskin/". date("Y");?>">
             <i class="fa fa-circle"></i>Jumlah Penduduk Miskin
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'pengeluaran_perkapita') echo "class='active'";?>>
          <a href="<?= base_url()."data/pengeluaran_perkapita/". date("Y");?>">
            <i class="fa fa-circle"></i>Pengeluaran Perkapita dan Garis 
              <br>Kemiskinan
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'kepandudukan_jk') echo "class='active'";?>>
          <a href="<?= base_url()."data/kepandudukan_jk/". date("Y");?>">
             <i class="fa fa-circle"></i>Jumlah Penduduk menurut 
              <br>Jenis Kelamin
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'kependudukan_kel_umur') echo "class='active'";?>>
          <a href="<?= base_url()."data/kependudukan_kel_umur/". date("Y");?>">
             <i class="fa fa-circle"></i>Jumlah Penduduk Kota Malang  
              <br>menurut Kelompok Umur
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'rasio_ketergantungan') echo "class='active'";?>>
          <a href="<?= base_url()."data/rasio_ketergantungan/". date("Y");?>">
             <i class="fa fa-circle"></i>Rasio Ketergantungan
          </a>
        </li>
        <li class="header"><i class="fa fa-align-justify"></i>   LAMPIRAN</li>

           
            <li <?php if($this->uri->segment(2) == 'lp_penduduk_jk') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_penduduk_jk/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Rasio Jenis Kelamin</a>
            </li>
            
                <li <?php if($this->uri->segment(2) == 'lp_penduduk_rasio') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_penduduk_rasio/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Rasio Gender</a>
            </li>
<li <?php if($this->uri->segment(2) == 'lp_penduduk_umur') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_penduduk_umur/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Rasio Umur</a>
            </li>
                <li <?php if($this->uri->segment(2) == 'lp_miskin') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_miskin/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Penduduk Miskin</a>
            </li>
        
    </ul>
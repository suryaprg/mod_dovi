<div class="content-wrapper">
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Jumlah Penduduk</h1>
            </div>
            <div class="box-body">
<h2 style="margin-left: 0.5em;">Jumlah Angkatan Kerja</h2>
    <br>
<section><br>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Berdasarkan data dari Dinas Tenaga Kerja Kota Malang, selama 3 (tiga) tahun terakhir 
(2014-2016), data jumlah angkatan kerja di Kota Malang mengalami penurunan, yaitu sebanyak 
423.631 pada tahun 2014 menjadi 406.935 di tahun 2016. Data pada tahun 2014, angkatan kerja 
sebanyak 423.631 jiwa terdiri dari 393.050 jiwa yang bekerja dan 30.581 jiwa yang sedang 
mencari pekerjaan. Sedangkan pada tahun 2016, dari 406.935 jiwa angkatan kerja, sebanyak 
377.329 jiwa merupakan penduduk yang bekerja dan 29.606 jiwa penduduk sedang mencari 
pekerjaan. Terjadinya penurunan angkatan kerja di Kota Malang karena penduduk sekolah 
lulusan SMA/Sederajat banyak yang memutuskan untuk melanjutkan pendidikan ke jenjang 
pendidikan yang lebih tinggi (perguruan tinggi/akademi) daripada memilih untuk langsung 
bekerja. Banyaknya universitas/akademi baik negeri maupun swasta yang tersedia di Kota 
Malang membuat penduduk Kota Malang lebih leluasa untuk memilih ke jenjang universitas. 
  </p>
</section>

        <br><center><div class="box box-solid box-primary" style="width: 660px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Jumlah Angkatan Kerja di Kota Malang Tahun 2014-2016</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body">
              <br>
              <div id="chartContain" style="height: 300px; width: 100%;"></div>
              <img height="27px" style="position: absolute; bottom: 40px; left: 0px;" src="<?php echo base_url();?>/assets/dist/img/mark.png" alt="mark1">
              <img height="20px" style="position: absolute; bottom: 40px; left: 540px;" src="<?php echo base_url();?>/assets/dist/img/mark.png" alt="mark2">
            </div>
            <div class="box-footer">
            <i class="fa fa-square" style="color: #7ba9f2"></i> Bejerja
            &nbsp&nbsp&nbsp
            <i class="fa fa-circle" style="color: #c0504e"></i> Mencari Pekerjaan
          </div>
          </div></center><br>
        </div>
      </div>
    </div>
  </div>
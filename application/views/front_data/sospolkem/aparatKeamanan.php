<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Aparat dan Sarana Keamanan</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
      <center><div>
      <div class="box-body chart-responsive" id="output_tabel">

  <table class="data-table" style="max-width: 95%; height: 480px;">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 20px;">
        <td colspan="7">Jumlah Aparat dan Sarana Keamanan di Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr style="font-size: 20px;">
        <th><center>Uraian</center></th>
        
        <?php 
        $th = 0;
        if(isset($default_th)){
          $th = $default_th;
        }
          for ($i=$th; $i<=$th+2 ; $i++){             # code...
            echo "<th><center>".$i."</center></th>";
          }
        ?>
      </tr>
    </thead>
    <tbody>
      <!-- <td></td> -->
 <!--    <?php
    


    if(isset($tabel_keamanan)){
      foreach ($tabel_keamanan as $r_val => $val) {
        //echo "<pre>";
        //print_r($val);
        echo "<tr>
              <td>".$val["uraian"][0]->uraian."</td>";

              if(count($val["data"]) != 0){
                  for ($i=$th; $i <= $th+2 ; $i++) { 
                      if(isset($val["data"][$i])){
                          echo "<td align=\"center\">".$val["data"][$i]."</td>";
                      }else{
                        echo "<td align=\"center\"> - </td>";    
                      }
                  }
              }else{
                echo "<td align=\"center\"> - </td>
                      <td align=\"center\"> - </td>
                      <td align=\"center\"> - </td>";  
              }
              
              
        echo  "</tr>";
        # code...
      }

    }

    ?> -->
    </tbody>
  </table>
            </div>
        </div></center>

        <button type="button" class="pull-right btn btn-default" id="btn_next">Next
                <i class="fa fa-arrow-circle-right"></i></button>

        <button type="button" class="pull-left btn btn-default" id="btn_prev"><i class="fa fa-arrow-circle-left"></i> Prev</button>

      </div>
      </div>
    </div> 
  </div>
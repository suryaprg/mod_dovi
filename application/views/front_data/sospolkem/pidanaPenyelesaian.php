  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Tindak Pidana dan Penyelesaiannya</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body"><br>
<center><div style="height: 390px;">
      <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 30px;" width="89%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="7">Jumlah Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor di Kota Malang Tahun 2015-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Kecamatan</td>
        <td colspan="2">Lapor</td>
        <td colspan="2">Selesai</td>
        <td colspan="2">Sisa</td>
      </tr>
      <tr>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;

    $tot1 = 0;
    foreach ($data_tindak_pidana as $tindak_pidana) 
    {
      $lapor1 = $tindak_pidana->lapor1;
      $lapor2 = $tindak_pidana->lapor2;
      $selesai1 = $tindak_pidana->selesai1;
      $selesai2 = $tindak_pidana->selesai2;
      $sisa1 = $tindak_pidana->sisa1;
      $sisa2 = $tindak_pidana->sisa2;

      echo '<tr>
          <td>'.$tindak_pidana->kec.'</td>
          <td>'.$lapor1. '</td>
          <td>'.$lapor2.'</td>
          <td>'.$selesai1. '</td>
          <td>'.$selesai2.'</td>
          <td>'.$sisa1. '</td>
          <td>'.$sisa2.'</td>

         
          </tr>';
      $total1 += $tindak_pidana->lapor1;
      $total2 += $tindak_pidana->lapor2;
      $total3 += $tindak_pidana->selesai1;
      $total4 += $tindak_pidana->selesai2;
      $total5 += $tindak_pidana->sisa1;
      $total6 += $tindak_pidana->sisa2;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1">Jumlah</th>
       <!--  <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th> -->
        
      </tr>
    </tfoot>
  </table>
            </div>
        </div></center><br><br>
    <center>
    <div style="height: 2050px;">
      <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 50px;" width="89%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="7">Jumlah Tindak Kejahatan Yang Dilaporkan dan Yang Diselesaikan Menurut Jenis Kejahatan di Kota Malang Tahun 2014-2016
        </td>
      </tr>
      <tr>
        <td rowspan="2">Kecamatan</td>
        <td colspan="2">2014</td>
        <td colspan="2">2015</td>
        <td colspan="2">2015</td>
      </tr>
      <tr>
        <th><center>Lapor</center></th>
        <th><center>Selesai</center></th>
        <th><center>Lapor</center></th>
        <th><center>Selesai</center></th>
        <th><center>Lapor</center></th>
        <th><center>Selesai</center></th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;

    $tot1 = 0;
    foreach ($data_tindak_kejahatan as $tindak_kejahatan) 
    {
      $lapor1 = $tindak_kejahatan->lapor1;
      $lapor2 = $tindak_kejahatan->lapor2;
      $lapor3 = $tindak_kejahatan->lapor3;
      $selesai1 = $tindak_kejahatan->selesai1;
      $selesai2 = $tindak_kejahatan->selesai2;
      $selesai3 = $tindak_kejahatan->selesai3;

      echo '<tr>
          <td>'.$tindak_kejahatan->jenis.'</td>
          <td>'.$lapor1. '</td>
          <td>'.$selesai1. '</td>
          <td>'.$lapor2.'</td>
          <td>'.$selesai2.'</td>
          <td>'.$lapor3. '</td>
          <td>'.$selesai3.'</td>

         
          </tr>';
      $total1 += $tindak_kejahatan->lapor1;
      $total3 += $tindak_kejahatan->selesai1;

      $total2 += $tindak_kejahatan->lapor2;
      $total4 += $tindak_kejahatan->selesai2;

      $total5 += $tindak_kejahatan->lapor3;
      $total6 += $tindak_kejahatan->selesai3;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1">Jumlah</th>
    <!--     <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>

        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th> -->
        
      </tr>
    </tfoot>
  </table>
            </div>
          </div></center>
        </div>
      </div>
    </div>
  </div>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Parlementaria</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Dewan Perwakilan Rakyat Daerah Kabupaten/Kota (DPRD) merupakan lembaga 
perwakilan rakyat daerah yang berkedudukan sebagai unsur penyelenggara pemerintahan 
daerah kabupaten/kota. DPRD kabupaten/kota terdiri atas anggota partai politik peserta 
pemilihan umum yang dipilih melalui pemilihan umum  Jumlah anggota DPRD Kota Malang tahun 
2014-2016 jumlahnya masih sama yaitu sebanyak 45 orang dengan jumlah terbanyak partai PDI
P sebanyak 11 orang. Posisi kedua ditempati oleh PKB sebanyak 6 orang.  
Dari 45 anggota DPRD Kota Malang masih didominasi laki-laki sebanyak 75,56 persen dan 
sisanya sebanyak 24,44 persen adalah perempuan.  Tingkat pendidikan anggota DPRD Kota 
Malang juga sudah semakin baik karena sebagian besar merupakan lulusan S1/S2 (82,22 %) 
sehingga diharapkan bias menghasilkan produk-produk hukum yang lebih baik dan lebih 
diperlukan oleh masyarakat Kota Malang. 
  </p>
</section> 
    <br>
    <center><div class="box box-solid box-primary" style="width: 930px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Jumlah Anggota DPRD Menurut Jenis Partai dan Jenis Kelamin di Kota Malang Tahun 2016</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div>
                <div id="containering"></div>
              </div>
            </div>
          </div></center>

      </div>
    </div>
  </div>
</div>
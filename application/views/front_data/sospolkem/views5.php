
<?php header('Access-Control-Allow-Origin: *'); ?>
  <table class="data-table" style="max-width: 95%; height: 480px;">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 20px;">
        <td colspan="7">Jumlah Aparat dan Sarana Keamanan di Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr style="font-size: 20px;">
        <th><center>Uraian</center></th>
        
        <?php 
        if(isset($default_th)){
          $th = $default_th;
        }
          for ($i=$th; $i<=$th+2 ; $i++){
            echo "<th><center>".$i."</center></th>";
          }
        ?>
        
      </tr>
    </thead>
    <tbody>
      <!-- <td></td> -->
    <?php
    


    if(isset($tabel_keamanan)){
      foreach ($tabel_keamanan as $r_val => $val) {
        //echo "<pre>";
        //print_r($val);
        echo "<tr>
              <td>".$val["uraian"][0]->uraian."</td>";

              if(count($val["data"]) != 0){
                  for ($i=$th; $i <= $th+2 ; $i++) { 
                      if(isset($val["data"][$i])){
                          echo "<td align=\"center\">".$val["data"][$i]."</td>";
                      }else{
                        echo "<td align=\"center\"> - </td>";    
                      }
                  }
              }else{
                echo "<td align=\"center\"> - </td>
                      <td align=\"center\"> - </td>
                      <td align=\"center\"> - </td>";  
              }
              
        echo  "</tr>";
        # code...
      }

    }

    ?>
    </tbody>
  </table>
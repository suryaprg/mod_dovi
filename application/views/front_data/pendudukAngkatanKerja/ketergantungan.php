  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Jumlah Penduduk</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<h2 style="margin-left: 0.5em;">Rasio Ketergantungan</h2>
    <br>
<section><br>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Rasio Ketergantungan (Dependency Ratio) adalah perbandingan antara jumlah penduduk 
umur 0-14 tahun ditambah dengan jumlah penduduk 65 tahun ke atas (keduanya disebut dengan 
bukan angkatan kerja) dibandingkan dengan jumlah penduduk usia 15-64 tahun (angkatan kerja). 
Rasio ketergantungan merupakan salah satu indikator demografi yang penting, semakin 
tingginya persentase dependency ratio menunjukkan semakin tingginya beban yang harus 
ditanggung penduduk yang produktif untuk membiayai hidup penduduk yang belum produktif 
dan tidak produktif lagi. Berdasarkan data dari BPS Kota Malang, angka rasio ketergantungan di Kota Malang 
tahun 2016 terus mengalami penurunan dibandingkan tahun sebelumnya. Pada tahun 2015 rasio 
ketergantungan di Kota Malang sebesar 37,13 persen sedangkan tahun 2014 sebesar 36,92 
persen. Penurunan angka rasio ketergantungan ini wajar dikarenakan banyaknya penduduk usia 
produktif yang terus bertambah tiap tahun. Pertambahan penduduk usia produktif ini sangat 
dimaklumi karena Kota Malang adalah Kota Pendidikan sehingga sebagai magnet para pelajar 
dan juga sebagai kota tujuan untuk mencari kerja.
  </p>
</section>  

        <br><center><div class="box box-solid box-primary" style="width: 560px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Rasio Ketergantungan Kota Malang Tahun 2014-2016</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <br>
              <div id="bar-chart" style="position: relative; right: 5px"></div>
            </div>
          </div></center><br>
        </div>
      </div>
    </div>
  </div>

  <script src="<?php echo base_url();?>/assets/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- AdminLTE for demo purposes -->
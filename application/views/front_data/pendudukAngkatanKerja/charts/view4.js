window.onload = function () {
    var chart = new CanvasJS.Chart("chartContain",
    {
      title:{
        text: ""
      
      },   
      data: [{        
        type: "column",
        dataPoints: [
        { label: "2014", y: 393050},
        { label: "2015", y: 377329},
        { label: "2016", y: 377329}
        ]
      },
      {        
        type: "line",
        dataPoints: [
        {label: "2014", y: 30581},
        {label: "2015", y: 29606},
        {label: "2016", y: 29606}
        ]
      }

        
      ]
    });

    chart.render();
  }
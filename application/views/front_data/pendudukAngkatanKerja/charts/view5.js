$(function () {
  $('#contamine').highcharts({
    title: {
      text: '',
      x: -20 //center
    },
    colors: ['blue', 'red'],
    plotOptions: {
      line: {
        lineWidth: 3
      },
      tooltip: {
        hideDelay: 200
      }
    },
    subtitle: {
      text: '',
      x: -20
    },
    xAxis: {
      categories: ['2014','2015','2016']
    },
    yAxis: {
      title: {
        text: ''
      },
      plotLines: [{
        value: 0,
        width: 1
      }]
    },
    tooltip: {
      valueSuffix: ' Jiwa',
      crosshairs: true,
      shared: true
    },
    legend: {
      layout: 'vertical',
      align: 'center',
      verticalAlign: 'bottom',
      borderWidth: 0
    },
    series: [ {
      name: 'Pengangguran Terbuka (Jiwa)',
      color: 'rgba(0,120,200,0.75)',
      marker: {
        radius: 6
      },
      data: [6920, 6257, 6194]
    }]
  });
});
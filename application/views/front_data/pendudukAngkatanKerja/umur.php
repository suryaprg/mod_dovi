
  
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/charts/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/charts4/view1A.css">

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Jumlah Penduduk</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<h2 style="margin-left: 0.5em;">Berdasar Kelompok Umur</h2>
    <br>
<section><br>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Kelompok usia produktif adalah penduduk yang berusia 15 sampai dengan 64 tahun. 
Perbandingan antara penduduk usia tidak produktif dengan usia produktif menunjukkan angka 
ketergantungan (age dependency ratio). Semakin tinggi rasio ketergantungan menunjukkan 
semakin berat pula beban yang ditanggung oleh penduduk di kelompok usia produktif. 
Berdasarkan data dari BPS Kota Malang Rasio Ketergantungan penduduk Kota Malang Tahun 
2016 adalah 36,92 persen yang artinya setiap 100 penduduk usia produktif di Kota Malang 
menanggung sebanyak 36-37 penduduk usia tidak produktif.  
  </p>
</section>  

        <br><center><div class="box box-solid box-primary" style="width: 560px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Jumlah Penduduk Kota Malang menurut Kelompok Umur Tahun 2014-2016</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <br>
              <div id="bar-chart" style="position: relative; right: 5px"></div>
            </div>
          </div></center><br>

<center><div>
            <div class="box-body chart-responsive">

  <table class="data-table table-responsive" style="position: relative; bottom: 30px;" width="96%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="10" style="font-size: 15px;">Jumlah Penduduk Menurut Kelompok Umur dan Jenis Kelamin di Kota Malang, 2014-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Kelompok Umur</td>
        <td colspan="3">Laki - laki</td>
        <td colspan="3">Perempuan</td>
        <td colspan="3">Jumlah</td>
      </tr>
      <tr>
        <th>2014</th>
        <th>2015</th>
        <th>2016</th>
        <th>2014</th>
        <th>2015</th>
        <th>2016</th>
        <th>2014</th>
        <th>2015</th>
        <th>2016</th>
      </tr>
    </thead>
    <tbody>
  <!--   <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;
    $total9 = 0;

    $tot1 = 0;
    $tot2 = 0;
    $tot3 = 0;
    foreach ($data_ku_dan_jk as $ku_dan_jk)
    {
      $laki1 = $ku_dan_jk->laki1;
      $laki2 = $ku_dan_jk->laki2;
      $laki3 = $ku_dan_jk->laki3;
      $prmp1 = $ku_dan_jk->prmp1;
      $prmp2 = $ku_dan_jk->prmp2;
      $prmp3 = $ku_dan_jk->prmp3;

      $tot1 = $ku_dan_jk->laki1 + $ku_dan_jk->prmp1;
      $tot2 = $ku_dan_jk->laki2 + $ku_dan_jk->prmp2;
      $tot3 = $ku_dan_jk->laki3 + $ku_dan_jk->prmp3;
      echo '<tr>
          <td><center>'.$ku_dan_jk->klmp.'</center></td>
          <td><center>'.$laki1. '</center></td>
          <td><center>'.$laki2.'</center></td>
          <td><center>'.$laki3.'</center></td>
          <td><center>'.$prmp1. '</center></td>
          <td><center>'.$prmp2.'</center></td>
          <td><center>'.$prmp3.'</center></td>
          <td><center>'.$tot1.'</center></td>
          <td><center>'.$tot2.'</center></td>
          <td><center>'.$tot3.'</center></td>

         
          </tr>';
      $total1 += $ku_dan_jk->laki1;
      $total2 += $ku_dan_jk->laki2;
      $total3 += $ku_dan_jk->laki3;
      $total4 += $ku_dan_jk->prmp1;
      $total5 += $ku_dan_jk->prmp2;
      $total6 += $ku_dan_jk->prmp3;

      $total7 += $tot1;
      $total8 += $tot2;
      $total9 += $tot3;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1"><center>Jumlah</center></th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        <th><?=number_format($total7, 0, ',', '')?></th>
        <th><?=number_format($total8, 0, ',', '')?></th>
        <th><?=number_format($total9, 0, ',', '')?></th>
        
      </tr>
    </tfoot>
  </table>
            </div>
          </div>
        </center><br>


    </div>
  </div>
</div>
</div>

<script src="<?php echo base_url();?>/assets/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->

<script src="<?php echo base_url();?>/assets/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>/assets/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>/assets/js/loading.js"></script>
<script src="<?php echo base_url();?>/assets/charts4/view1A.js"></script>
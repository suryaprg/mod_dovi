 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Jumlah Penduduk</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<h2 style="margin-left: 0.5em;">Indeks Pembangunan Manusia</h2>
    <br>
<section><br>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Indeks yang menggambarkan kualitas hidup manusia disebut dengan Indeks 
Pembangunan Manusia (IPM). IPM merupakan data strategis karena selain sebagai ukuran 
kinerja pemerintah, IPM juga digunakan sebagai salah satu alokator penentuan Dana Alokasi 
Umum (DAU). IPM adalah indeks komposit yang mengukur keberhasilan pembangunan manusia 
dari tiga dimensi, yaitu dimensi kesehatan , dimensi pengetahuan dan dimensi standar kehidupan 
yang layak. Dimensi kesehatan diukur dari angka harapan hidup, dimensi pengetahuan diukur 
dari angka harapan sekolah dan rata-rata lama sekolah dan dimensi standar kehidupan yang 
layak diukur dengan kemampuan daya beli. Dengan adanya IPM dapat ditentukan peringkat atau 
level pembangunan suatu negara/pemerintah. IPM Kota Malang menempati peringkat pertama 
di Jawa Timur, artinya kualitas SDM Kota Malang merupakan paling baik jika diukur dari sisi 
kesehatan, pendidikan, dan ekonomi.
  </p>
</section>  

        <br><center><div class="box box-solid box-primary" style="width: 560px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Perkembangan IPM Kota Malang Tahun 2014-2016</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <br>
              <div id="containered" style="position: relative; right: 5px"></div>
            </div>
          </div></center><br>
        </div>
      </div>
    </div>
  </div>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PENDUDUK DAN ANGKATAN KERJA</title>
  <link rel="shortcut icon" href="https://malangkota.go.id/wp-content/themes/malangkotanew/images/favicon.png"> 
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts1/tabeldata.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/css/style.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
  <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts/css/style.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts4/view1B.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts4/view1D.css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts4/view2.css"> -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="fh5co-loader"></div>
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
     <div style="position: absolute; left: 50px; top: 0.1px;"><img  height="47px" style="padding: 5px" src="<?php echo base_url();?>assets/template_aw/dist/img/ncc-logo-white.png" alt="NCC logo"></div>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <li class="dropdown user user-menu">
            <a href="<?php echo base_url();?>../index.php">
               <div style="position: absolute; top: 11px; right: 130px;"><img src="<?php echo base_url();?>assets/template_aw/dist/img/logo.png" width="25" height="25" alt="User Image"></div>&nbsp &nbsp &nbsp &nbsp &nbsp
             <span class="hidden-xs">Malang Open Data</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url();?>assets/template_aw/dist/img/logo.png" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>KEPENDUDUKAN</p>
          <a href="#"><i class="fa fa-circle text-success"></i>Pemerintah Kota Malang</a>
        </div>
      </div>
      <br>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">KATEGORI</li>
        <li class="active">
          <a href="">
             <span>Jumlah Penduduk</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="index"><i class="fa fa-circle-o"></i>Berdasar Jenis Kelamin</a></li>
            <li><a href="umur"><i class="fa fa-circle-o"></i>Berdasar Kelompok Umur</a></li>
            <li><a href="gender"><i class="fa fa-circle-o"></i>Rasio Gender</a></li>
            <li><a href="ketergantungan"><i class="fa fa-circle-o"></i>Rasio Ketergantungan</a></li>
            <li><a href="gini"><i class="fa fa-circle-o"></i>Rasio GINI</a></li>
          </ul>
        </li>
        <li <?php if($this->uri->segment(3) == 'indeksPemM
        anusia') echo "class='active'";?>>
          <a href="indeksPemManusia">
            <span>Indeks Pembangunan Manusia</span>
          </a>
        </li>
          <!-- <li <?php if($this->uri->segment(3) == 'ketenagakerjaan') echo "class='active'";?>>
          <a href="ketenagakerjaan">
            <span>Data Ketenagakerjaan</span>
          </a>
        </li>
          <li <?php if($this->uri->segment(3) == 'angkatanKerja') echo "class='active'";?>>
          <a href="angkatanKerja">
            <span>Jumlah Angkatan Kerja</span>
          </a>
        </li>
          <li <?php if($this->uri->segment(3) == 'pengangguran') echo "class='active'";?>>
          <a href="pengangguran">
            <span>Jumlah Pengangguran Terbuka</span>
          </a>
        </li>
          <li <?php if($this->uri->segment(3) == 'upahMinimum') echo "class='active'";?>>
          <a href="upahMinimum">
            <span>Upah Minimum</span>
          </a>
        </li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  
  <?php
            switch($page){
                case 'jenis_kelamin'      : include "jenisKelamin.php";
               break;
                case 'umur'      : include "umur.php";
               break;
                case 'gender'      : include "gender.php";
               break;
                case 'ketergantungan'      : include "ketergantungan.php";
               break;
                case 'gini'      : include "gini.php";
               break;
                case 'indeksPemManusia'      : include "indeksPemManusia.php";
               break;
               //  case 'Mainkependudukan/ketenagakerjaan'      : include "ketenagakerjaan.php";
               // break;    
               //    case 'Mainkependudukan/angkatanKerja'      : include "angkatanKerja.php";
               // break;
               //  case 'Mainkependudukan/pengangguran'      : include "pengangguran.php";
               // break;
               //  case 'Mainkependudukan/upahMinimum'      : include "upahMinimum.php";
               // break;    

            }
            ?>


  <footer class="main-footer">
    <strong>Copyright &copy; 2018 <a href="http://kominfo.malangkota.go.id/" target="_blank">Kominfo Kota Malang</a>
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script type="text/javascript" src="https://canvasjs.comassets/template_aw/script/canvasjs.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts4/view1A.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts4/view1B.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts4/view1D.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts4/view2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts4/view2.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts4/view4.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts4/view4.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts4/view5.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts4/view5.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts4/views6.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts4/views6.min.js"></script>

</body>
</html>

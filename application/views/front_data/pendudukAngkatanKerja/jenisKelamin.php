<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts/css/style.css"> -->
  <!-- <link rel="stylesheet" href="http://localhost:8080/mop/opendata//assets/charts/css/style.css"> -->
  <script src="https://www.koolchart.com/demo/LicenseKey/KoolChartLicense.js"></script>
  <script src="https://www.koolchart.com/demo/KoolChart/JS/KoolIntegration.js"></script>
  <link rel="stylesheet" href="https://www.koolchart.com/demo/KoolChart/Assets/Css/KoolChart.css"/>
  <link rel="stylesheet" href="https://www.koolchart.com/demo/Samples/Web/sample.css"/>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Jumlah Penduduk</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<h2 style="margin-left: 0.5em;">Berdasar Jenis Kelamin</h2>
    <br>
<section><br>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Data Jumlah penduduk Kota Malang diperoleh dari BPS Kota Malang. Data jumlah 
penduduk untuk tahun yang tidak dilaksanakan sensus penduduk diperoleh dari hasil proyeksi. 
Proyeksi penduduk merupakan suatu perhitungan ilmiah yang didasarkan pada asumsi dari 
komponen-komponen perubahan penduduk, yaitu kelahiran, kematian, dan migrasi. Proyeksi 
penduduk menggunakan data dasar penduduk hasil SP2010. Hasil proyeksi penduduk tahun 
2016, jumlah penduduk di Kota Malang adalah 856.410 jiwa bertambah sekitar 10.437 jiwa sejak 
tahun 2014 sebanyak 845.973 jiwa. Dengan luas wilayah daratan Kota Malang sebesar 110,06 km2, maka tingkat kepadatan penduduk Kota Malang tahun 2016 adalah 7.781 jiwa per kilometer 
persegi. Jika dibandingkan dengan kepadatan penduduk tahun 2015 sebesar 7.735 jiwa per 
kilometer persegi, mengalami peningkatan sekitar 46 jiwa per kilometer persegi. Sedangkan pada 
tahun 2014, kepadatan penduduk di Kota Malang sebesar 7.686 jiwa per kilometer persegi. 
  </p>
</section>

        <br><center><div class="box box-solid box-primary" style="width: 70%;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Jumlah Penduduk menurut Jenis Kelamin di Kota Malang Tahun 2014-2016</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body">
              <br>
              <!-- <div id="chartContainer" style="height: 300px; width: 100%"></div> -->
              <div id="chartHolder" style="width:100%; height:500px;"></div>
              <img  height="27px" style="position: absolute; bottom: 35px; left: 0px;" src="<?php echo base_url();?>assets/template_aw/dist/img/mark.png" alt="mark1">
              <img  height="20px" style="position: absolute; bottom: 40px; left: 580px;" src="<?php echo base_url();?>assets/template_aw/dist/img/mark.png" alt="mark2">
            </div>
            <div class="box-footer">
            <i class="fa fa-square" style="color: #4f81bc"></i> Penduduk
            &nbsp&nbsp&nbsp
            <i class="fa fa-circle" style="color: #c0504e"></i> Laki - laki
            &nbsp&nbsp&nbsp
            <i class="fa fa-circle" style="color: #9bbb58"></i> Perrempuan
          </div>
          </div></center><br>
        </div>
      </div>
    </div>
  </div>

<!-- jQuery 2.2.0 -->

<!-- AdminLTE for demo purposes -->
<!-- <script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script> -->
<!-- <script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script> -->
<!-- <script src="<?php echo base_url();?>assets/template_aw/charts4/view1A.js"></script> -->
<script>
var chartVars = "KoolOnLoadCallFunction=chartReadyHandler";

KoolChart.create("chart1", "chartHolder", chartVars, "100%", "100%");

function chartReadyHandler(id) {
  document.getElementById(id).setLayout(layoutStr);
  document.getElementById(id).setData(chartData);
}

var layoutStr =
   '<KoolChart backgroundColor="#FFFFFF"  borderStyle="none">'
   +'<Options>'
    +'<Caption text="Annual Report"/>'
   +'</Options>'
   +'<NumberFormatter id="numfmt" useThousandsSeparator="true"/>' 
   +'<Combination2DChart showDataTips="true">'
    +'<horizontalAxis>'
     +'<CategoryAxis categoryField="Month" padding="1"/>' 
    +'</horizontalAxis>'
    +'<verticalAxis>'
     +'<LinearAxis id="vAxis1" formatter="{numfmt}" maximum="100" interval="10"/>'
    +'</verticalAxis>'
    +'<series>'
     +'<Column2DSeries labelPosition="outside" yField="Profit" displayName="Profit" showValueLabels="[5]" columnWidthRatio="0.54">'
      +'<fill>'
       +'<SolidColor color="#41b2e6"/>'
      +'</fill>'
      +'<showDataEffect>'
       +'<SeriesInterpolate/>' 
      +'</showDataEffect>'
     +'</Column2DSeries>'
     +'<Line2DSeries radius="6" yField="Cost" displayName="Cost" itemRenderer="CircleItemRenderer">'
      +'<verticalAxis>'
       +'<LinearAxis id="vAxis2" interval="40" maximum="320"/>'
      +'</verticalAxis>'
      +'<showDataEffect>'
       +'<SeriesInterpolate/>' 
      +'</showDataEffect>'
      +'<lineStroke>'
       +'<Stroke color="#f9bd03" weight="4"/>'
      +'</lineStroke>'
      +'<stroke>'
       +'<Stroke color="#f9bd03" weight="3"/>'
      +'</stroke>'
     +'</Line2DSeries>'
    +'</series>'
    +'<verticalAxisRenderers>'
     +'<Axis2DRenderer axis="{vAxis1}" showLine="false"/>'
     +'<Axis2DRenderer axis="{vAxis2}" showLine="false"/>'
    +'</verticalAxisRenderers>'
   +'</Combination2DChart>'
  +'</KoolChart>';

var chartData =
  [{"Month":"Jan","Profit":20,"Cost":120,"Revenue":2300},
  {"Month":"Feb","Profit":30,"Cost":160,"Revenue":1200},
  {"Month":"Mar","Profit":52,"Cost":150,"Revenue":1600},
  {"Month":"Apr","Profit":45,"Cost":190,"Revenue":1300},
  {"Month":"May","Profit":62,"Cost":180,"Revenue":1000},
  {"Month":"Jun","Profit":82,"Cost":250,"Revenue":1200},
  {"Month":"Jul","Profit":48,"Cost":130,"Revenue":1000},
  {"Month":"Aug","Profit":39,"Cost":100,"Revenue":1600},
  {"Month":"Sep","Profit":61,"Cost":180,"Revenue":1200},
  {"Month":"Oct","Profit":42,"Cost":120,"Revenue":1000}];
</script>

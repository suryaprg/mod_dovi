
            <!-- /.box-header -->
            <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
        <div class="box box-solid">
            <div class="box-header with-border">
                       <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Keuangan Daerah</h1>
                <h2 align="center" ><?php print_r($th_st." - ".$th_fn); ?>     </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
            </div>
            <!-- /.box-header -->
                   <div class="box-body">
                <h2 style="margin-left: 0.5em;">Realisasi Pendapatan Daerah</h2>
                <br>
                <section>
                    <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Berdasarkan data dari Badan Pengelolaan Keuangan dan Aset Daerah Kota Malang realisasi pendapatan Kota Malang tahun 2016 adalah sebanyak 1711.185.350,08 ribu rupiah, mengalami penurunan sebesar Rp.93.502.457,87 ribu rupiah atau turun 5,18 persen dibanding tahun 2015. Penurunan pendapatan Kota Malang tahun 2016 disebabkan adanya penurunan dari item lain-lain pendapatan yang sah sebesar 283.283.112,14 ribu rupiah atau turun 59,29 persen khususnya pada dana penyesuaian dan otonomi khusus serta bantuan keuangan dari provinsi atau pemerintah daerah lainnya. Realisasi pendapatan Kota Malang mengalami penurunan, akan tetapi dari sisi Pendapatan Asli Daerah (PAD) Kota Malang justru dari tahun <?= $th_st." - ".$th_fn;?> terus mengalami peningkatan, tercatat pada tahun 2016 PAD Kota Malang mengalami kenaikan sebesar 22.393.900,31 ribu rupiah atau 5,27 persen meskipun kenaikan tersebut terbilang masih lebih kecil dibandingkan dengan kenaikan tahun 2015 yang mencapai 14,06 persen
                    </p>
                </section>
                <center>
                    <table class="data-table" style="width: 70%;">
                        <caption class="title"></caption>
                        <thead>
                            <?php print_r($_get_realisasi["str_header"]);?>
                        </thead>
                        <tbody>
                            <?php print_r($_get_realisasi["str_tbl"]);?>
                        </tbody>
                    </table>
                    <br>
                     <center>
                            <a href="<?php print_r(base_url()."pdf/realisasi_pendapatan_daerah/".($th_fn));?>" target="_blank" id="btn_next" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                </center>
                <br><br>
                <center>
                    <div class="box box-solid box-primary" style="width: 70%;">
                        <div class="box-header with-border">
                            <center>
                                <h3 class="box-title"><?php print_r($_get_realisasi["title"]); ?></h3></center>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body chart-responsive">
                            <div>
                                <div id="chartdiv_realisasi" style="width: 100%; height: 500px;"></div>
                            </div>
                        </div>
                    </div>
                </center>
                <h4 class="box-title" align="center"> Sumber : Badan Pengelola Keuangan dan Aset Daerah Kota Malang</h4>
                <br>
                <br>

            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <h2 style="margin-left: 0.5em;">Realisasi Belanja Daerah</h2>
                <br>
                <section>
                    <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Dana alokasi umum yang merupakan bagian dari dana perimbangan menjadi sumber terbesar penerimaan daerah Kota Malang. Tahun 2016 Pemerintah Kota Malang mendapat DAU sebesar Rp. 859.678208 ribu rupiah atau 50,24 persen dari total pendapatan daerah. Hal ini menunjukkan bahwa betapa besar ketergantungan daerah terhadap pemerintah pusat. Pajak dan retribusi daerah yang merupakan pendapatan asli daerah hanya 24,39 persen. Seiring dengan menurunnya pendapatan Pemerintah Kota Malang, jumlah realisasi belanja Pemerintah Kota Malang juga menurun pada tahun 2016 dibanding tahun 2015. Realisasi belanja tahun 2016 sebesar Rp. 1.709.918.083,05 ribu rupiah mengalami penurunan sebesar Rp. 93.502.457,87 ribu rupiah atau 5,18 persen. Berdasarkan strukturnya porsi belanja pegawai Pemerintah Kota Malang merupakan porsi paling besar dibanding belanja modal dan belanja lainnya yang mencapai 58,11 persen dari realisasi belanja Kota Malang. Jumlah belanja pegawai dari tahun <?= $th_st." - ".$th_fn;?> cenderung mengalami kenaikan.
                    </p>
                </section>
                <center>
                    <table class="data-table" style="width: 70%;">
                        <caption class="title"></caption>
                        <thead>
                            <?php print_r($_get_belanja["str_header"]);?>
                        </thead>
                        <tbody>
                            <?php print_r($_get_belanja["str_tbl"]);?>
                        </tbody>
                    </table>
                    <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/benaja_daerah/".($th_fn));?>" target="_blank" id="btn_next" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                </center>

                <br><br>
                <center>
                    <div class="box box-solid box-primary" style="width: 70%;">
                        <div class="box-header with-border">
                            <center>
                                <h3 class="box-title"><?php print_r($_get_belanja["title"]); ?></h3></center>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body chart-responsive">
                            <div>
                                <div id="chartdiv_belanja" style="width: 100%; height: 500px;"></div>
                            </div>
                        </div>
                    </div>
                </center>
<h4 class="box-title" align="center"> Sumber : Badan Pengelola Keuangan dan Aset Daerah Kota Malang</h4>
                <br>
                <br>

            </div>
        </div>

    </div>
</div>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>
<!-- Resources -->
<script src="<?= base_url();?>assets/chart_main/amchart_vertical/amcharts.js"></script>
<script src="<?= base_url();?>assets/chart_main/amchart_vertical/serial.js"></script>
<script src="<?= base_url();?>assets/chart_main/amchart_vertical/export.min.js"></script>
<script src="<?= base_url();?>assets/chart_main/amchart_vertical/light.js"></script>

<!-- Resources -->
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/core.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/charts.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/animated.js"></script>

   <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    
<script type="text/javascript">
        var data_main_realisasi = JSON.parse('<?php print_r($_get_realisasi['data_graph']);?>');
        var data_main_belanja = JSON.parse('<?php print_r($_get_belanja['data_graph']);?>');

        $(document).ready(function(){
            create_graph_realisasi(data_main_realisasi);
            create_graph_belanja(data_main_belanja);
            // console.log(data_main);
        });


            $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/keuangan_daerah/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/keuangan_daerah/".($th_fn-1));?>';
            window.location.href = prev_url;
        });

        function create_graph_realisasi(data_graph_realisasi){
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("chartdiv_realisasi", am4charts.XYChart);

            // Add data
            chart.data = data_graph_realisasi;

            // Create category axis
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "year";
            categoryAxis.renderer.opposite = true;

            // Create value axis
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.inversed = true;
            valueAxis.title.text = "";
            valueAxis.renderer.minLabelPosition = 0.01;

            // Create series
            <?php print_r($_get_realisasi["series"])?>

            // Add chart cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.behavior = "zoomY";

            // Add legend
            chart.legend = new am4charts.Legend();
        }

        function create_graph_belanja(data_graph_belanja){
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("chartdiv_belanja", am4charts.XYChart);

            

            // Add data
            chart.data = data_graph_belanja;

            // Create category axis
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "year";
            categoryAxis.renderer.opposite = true;

            // Create value axis
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.inversed = true;
            valueAxis.title.text = "";
            valueAxis.renderer.minLabelPosition = 0.01;

            // Create series
            <?php print_r($_get_realisasi["series"])?>

            // Add chart cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.behavior = "zoomY";

            // Add legend
            chart.legend = new am4charts.Legend();

        }
</script>
</body>
</html>
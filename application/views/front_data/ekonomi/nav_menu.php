<!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><i class="fa fa-clipboard"></i>   KATEGORI</li>
       <!--  <li <?php if($this->uri->segment(2) == 'index') echo "class='active'";?>>
          <a href="#">
            <span>Kepegawaian</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'rt_rw') echo "class='active'";?>>
          <a href="<?= base_url()."data/rt_rw/". date("Y");?>">
            <span>RT dan RW</span>
          </a>
        </li> -->
        <li <?php if($this->uri->segment(2) == 'keuangan_daerah') echo "class='active'";?>>
          <a href="<?= base_url()."data/keuangan_daerah/". date("Y");?>">
            <i class="fa fa-circle"></i>Keuangan Daerah
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'pengeluaran_penduduk') echo "class='active'";?>>
          <a href="<?= base_url()."data/pengeluaran_penduduk/". date("Y");?>">
            <i class="fa fa-circle"></i>Pengeluaran Penduduk
          </a>
        </li>
         <li <?php if($this->uri->segment(2) == 'pendapatan_regional') echo "class='active'";?>>
          <a href="<?= base_url()."data/pendapatan_regional/". date("Y");?>">
            <i class="fa fa-circle"></i>Pendapatan Regional
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'pengeluaran_rumah_tangga') echo "class='active'";?>>
          <a href="<?= base_url()."data/pengeluaran_rumah_tangga/". date("Y");?>">
             <i class="fa fa-circle"></i>Pengeluaran Rumah Tangga
          </a>
        </li>
         <li <?php if($this->uri->segment(2) == 'pendapatan_lap_usaha') echo "class='active'";?>>
          <a href="<?= base_url()."data/pendapatan_lap_usaha/". date("Y");?>">
             <i class="fa fa-circle"></i>Pendapatan Usaha
          </a>
        </li>
         <li class="header"><i class="fa fa-align-justify"></i>   LAMPIRAN</li>

           
            <li <?php if($this->uri->segment(2) == 'lp_keuangan_e') echo "class='active'";?> >
              <a href="<?= base_url()."data/lp_keuangan_e/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Realisasi Daerah
              </a>
            </li>
                        <li <?php if($this->uri->segment(2) == 'tenaga') echo "class='active'";?> >
              <a href="<?= base_url()."data/tenaga/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Bidang Usaha
              </a>
            </li>
                        <li <?php if($this->uri->segment(2) == 'lp_koperasi') echo "class='active'";?> >
              <a href="<?= base_url()."data/lp_koperasi/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Koperasi
              </a>
            </li>
                        <li <?php if($this->uri->segment(2) == 'lp_pendapatan_indutri') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_pendapatan_indutri/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Pendapatan Industri
              </a>
            </li>          
            <li <?php if($this->uri->segment(2) == 'lp_pajak') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_pajak/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Pajak
              </a>
            </li>
           <li <?php if($this->uri->segment(2) == 'lp_pdb') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_pdb/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data PDB
              </a>
            </li>
            <li <?php if($this->uri->segment(2) == 'perdagangan') echo "class='active'";?>>
              <a href="<?= base_url()."data/perdagangan/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Perdagangan
              </a>
            </li>
          </ul>

        </li>

      </ul>

        
    </ul>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
        <div class="box box-solid">
            <div class="box-header with-border">
                <!-- <h1> Realisasi Pendapatan Daerah </h1> -->
            </div>

                                    <!-- next - prev -->
        <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Realisasi Pendapatan dan Belanja Daerah</h1>
                <h2 align="center" > <?php print_r($th_st." - ".$th_fn); ?>     </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
            <!-- /.box-header -->
            <div class="box-body">

                <!-- <section>
                            <br>
                            <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Berdasarkan data dari Badan Pengelolaan Keuangan dan Aset Daerah Kota Malang realisasi pendapatan Kota Malang tahun 2016 adalah sebanyak 1711.185.350,08 ribu rupiah, mengalami penurunan sebesar Rp.93.502.457,87 ribu rupiah atau turun 5,18 persen dibanding tahun 2015. Penurunan pendapatan Kota Malang tahun 2016 disebabkan adanya penurunan dari item lain-lain pendapatan yang sah sebesar 283.283.112,14 ribu rupiah atau turun 59,29 persen khususnya pada dana penyesuaian dan otonomi khusus serta bantuan keuangan dari provinsi atau pemerintah daerah lainnya. Realisasi pendapatan Kota Malang mengalami penurunan, akan tetapi dari sisi Pendapatan Asli Daerah (PAD) Kota Malang justru dari tahun <?= $th_st." - ".$th_fn;?> terus mengalami peningkatan, tercatat pada tahun 2016 PAD Kota Malang mengalami kenaikan sebesar 22.393.900,31 ribu rupiah atau 5,27 persen meskipun kenaikan tersebut terbilang masih lebih kecil dibandingkan dengan kenaikan tahun 2015 yang mencapai 14,06 persen 
                            </p>
                </section> -->
                <br>
                <center>
                    <div>
                        <div class="box-body chart-responsive">

                            <table class="data-table" style="position: relative; bottom: 30px; height: 480px;" width="98%">
                                <caption class="title"></caption>
                
  <thead class="thead-dark">
                                    <tr style="font-size: 20px;">
                                        <td colspan="7">Realisasi Pendapatan dan Belanja Daerah Kota Malang Tahun <?= $th_st." - ".$th_fn;?> (Rupiah)</td>
                                    </tr>
                                    <tr style="font-size: 17px;">
                                    	<th width="5%">No</th>
										<th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	
								<?php
									if($th_st){
										$str_header_sub = "<thead>
																<tr>
																	<th>No. </th>
																	<th>Sub. Jenis</th>";
										for ($i=$th_st; $i <= $th_fn ; $i++) { 
											$str_header_sub .= "<th>".$i."</th>";
										}

										$str_header_sub .= "	</tr>
															</thead>";	
									}
								?>
								
									
									
								<?php
									if(isset($keuangan)){
										if($keuangan){

											echo "<tr>
			                                		<td colspan=\"2\" align=\"center\">Realisasi Pendapatan Daerah</td>
			                                	</tr>";
											$no = 1;
											foreach ($keuangan as $r_date => $v_data) {
												if($v_data["jenis"]["kategori"] == 0){
												echo "<tr class=\"jenis_click\" id=\"jenis_".$v_data["jenis"]["id_jenis"]."\">
														<td><a href=\"#\">".$no."</a></td>
														<td><a href=\"#\">".$v_data["jenis"]["nama_jenis"]."</a></td>
													</tr>";
												echo "<tr class=\"sub_jenis_out\" id=\"tmp_".$v_data["jenis"]["id_jenis"]."\">
														<td>&nbsp;</td>
														<td>
															<table border='1' width=\"100%\" id=\"output_".$v_data["jenis"]["id_jenis"]."\">";
												
															echo $str_header_sub;
															
															echo "<tbody>";

															$no_sub = 1;
															foreach ($v_data["sub_jenis"] as $r_sub => $v_sub) {
																echo "<tr class=\"sub_jenis\" id=\"sub_".$v_sub["id_sub_jenis"]."\">
																		<td width=\"5%\">".$no_sub."</td>
																		<td width=\"35%\">".$v_sub["nama_sub_jenis"]."</td>";
																for ($i=$th_st; $i <= $th_fn ; $i++) { 
																	if($v_sub["value"][$i]){
																		echo "<td width=\"20%\" align=\"right\">".number_format($v_sub["value"][$i],2,".",",")."</td>";
																	}else{
																		echo "<td width=\"20%\" align=\"center\">-</td>";
																	}
																	
																}
																echo "</tr>";
																$no_sub++;
															}
															echo "</tbody>";

												echo "		</table>
														</td>
													</tr>";

												$no++;
												}
											}


											echo "<tr>
			                                		<td colspan=\"2\">Realisasi Belanja Daerah</td>
			                                	</tr>";

											foreach ($keuangan as $r_date => $v_data) {
												if($v_data["jenis"]["kategori"] == 1){
												echo "<tr class=\"jenis_click\" id=\"jenis_".$v_data["jenis"]["id_jenis"]."\">
														<td><a href=\"#\">".$no."</a></td>
														<td><a href=\"#\">".$v_data["jenis"]["nama_jenis"]."</a></td>
													</tr>";
												echo "<tr class=\"sub_jenis_out\" id=\"tmp_".$v_data["jenis"]["id_jenis"]."\">
														<td>&nbsp;</td>
														<td>
															<table border='1' width=\"100%\" id=\"output_".$v_data["jenis"]["id_jenis"]."\">";
												
															echo $str_header_sub;
															
															echo "<tbody>";

															$no_sub = 1;
															foreach ($v_data["sub_jenis"] as $r_sub => $v_sub) {
																echo "<tr class=\"sub_jenis\" id=\"sub_".$v_sub["id_sub_jenis"]."\">
																		<td width=\"5%\">".$no_sub."</td>
																		<td width=\"35%\">".$v_sub["nama_sub_jenis"]."</td>";
																for ($i=$th_st; $i <= $th_fn ; $i++) { 
																	if($v_sub["value"][$i]){
																		echo "<td width=\"20%\" align=\"right\">".number_format($v_sub["value"][$i],2,".",",")."</td>";
																	}else{
																		echo "<td width=\"20%\" align=\"center\">-</td>";
																	}
																	
																}
																echo "</tr>";
																$no_sub++;
															}
															echo "</tbody>";

												echo "		</table>
														</td>
													</tr>";

												$no++;
												}
											}
										}
									}
								?>						
    							</tbody>
                            </table>
                              <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/lp_keuangan_e/".($th_fn));?>" target="_blank" id="btn_next" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                        </div>

                    </div>
                <br><br>
                <center>
                    <div class="box box-solid box-primary" style="width: 70%;">
                        <div class="box-header with-border">
                            <center>
                                <h3 class="box-title">Grafik Realisasi Pendapatan dan Belanja Daerah Kota Malang Tahun <?= $th_st ." - ". $th_fn; ?> (Rupiah)</h3></center>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body chart-responsive">
                            <div>
                                <div id="chartdiv" style="width: 100%; height: 500px"></div>
                            </div>
                        </div>
                    </div>
                    <h4 class="box-title" align="center"> Sumber : Badan Pengelola Aset dan Kekayaan Daerah Kota Malang</h4> 
                </center>                
            </div>        
        </div>
    </div>
</div>





   
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>

<!-- Resources -->
	<script src="<?= base_url();?>assets/chart_main/amchart_multi_line/core.js"></script>
	<script src="<?= base_url();?>assets/chart_main/amchart_multi_line/charts.js"></script>
	<script src="<?= base_url();?>assets/chart_main/amchart_multi_line/animated.js"></script>
	
	<script type="text/javascript">
		var json_data = JSON.parse('<?php print_r($json_data); ?>');
		var json_sub = [];
		console.log(json_data);

		$(document).ready(function(){
			$(".sub_jenis_out").hide();
		});

		$(".jenis_click").click(function(){
			var id_jenis_html = $(this).attr("id");

			var id_jenis = id_jenis_html.split("_")[1];

			console.log(id_jenis);
			// console.log(data_main);

			var id_tr_output = "tmp_"+id_jenis;
			var output_data = "output_"+id_jenis;

			if($("#"+id_tr_output).is(":visible")){
				$("#"+id_tr_output).hide(500);
			}else{
				$(".sub_jenis_out").hide();
				$("#"+id_tr_output).show(500);
				json_sub = json_data[id_jenis];
			}
			
		});

		  $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/lp_keuangan_e/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/lp_keuangan_e/".($th_fn-1));?>';
            window.location.href = prev_url;
        });

		$(".sub_jenis").click(function(){
			var id_html_sub = $(this).attr("id");
			var id_sub_jenis = id_html_sub.split("_")[1];

			var obj_val = json_sub.sub_jenis[id_sub_jenis]["data_graph"];
			var caption = json_sub.sub_jenis[id_sub_jenis]["nama_sub_jenis"]

			create_graph(obj_val, caption);

			// console.log(id_sub_jenis);
			// console.log(json_sub.sub_jenis[id_sub_jenis]["data_graph"]);
		});

		function create_graph(obj_val, caption){
			am4core.useTheme(am4themes_animated);
			// Themes end

			// Create chart instance
			var chart = am4core.create("chartdiv", am4charts.XYChart);

			// Add data
			chart.data = obj_val;

			// Create category axis
			var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
			categoryAxis.dataFields.category = "year";
			categoryAxis.renderer.opposite = true;

			// Create value axis
			var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
			valueAxis.renderer.inversed = true;
			valueAxis.title.text = caption;
			valueAxis.renderer.minLabelPosition = 0.01;

			// Create series
			var series1 = chart.series.push(new am4charts.LineSeries());
			series1.dataFields.valueY = "val_jml";
			series1.dataFields.categoryX = "year";
			series1.name = "Jumlah";
			series1.strokeWidth = 3;
			series1.bullets.push(new am4charts.CircleBullet());
			series1.tooltipText = caption+" {name} pada {categoryX}: {valueY}";
			series1.legendSettings.valueText = "{valueY}";
			series1.visible  = false;

			

			// Add chart cursor
			chart.cursor = new am4charts.XYCursor();
			chart.cursor.behavior = "zoomY";

			// Add legend
			chart.legend = new am4charts.Legend();
		}
	</script>

</body>
</html>

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
            </section>
            <div class="content body" style="margin-right: 3em; margin-left: 3em;">
                <div class="box box-solid">
                    <div class="box-header with-border">
                                <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Tiga Kegiatan Lapangan Usaha dengan Kontribusi Terbesar</h1>
                <h2 align="center" > <?php print_r($th_st." - ".$th_fn); ?>     </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <section>
                            <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Melalui penghitungan PDRB diketahui bahwa pada tahun 2016 pertumbuhan ekonomi
Kota Malang atas dasar harga konstan tahun 2010 sebesar 5,61 persen. Pertumbuhan ekonomi
tahun 2016 sama nilainya dengan tahun 2015, yang membedakan adalah rincian pertumbuhan
ekonomi pada masing-masing lapangan usaha. Laju pertumbuhan tahun 2015 sebesar 5,61
persen mengalami penurunan sebesar 0,19 jika dibandingkan dengan tahun 2014 sebesar 5,80
persen. Sumber pertumbuhan PDRB terbesar Kota Malang selama 3 (tiga) tahun mulai tahun
2014-2016 berasal dari kegiatan lapangan usaha perdagangan besar dan eceran; reparasi mobil,
dan sepeda motor dengan kontribusi, disusul oleh Industri pengolahan dengan kontribusi dan
ketiga adalah lapangan usaha konstruksi.
                            </p>
                        </section>
                        <br>
                        <center>
                            <table class="data-table" style="width: 50%;">
                                <caption class="title"></caption>
                                <thead>
                                    <?php print_r($str_header);?>
                                </thead>
                                <tbody>
                                    <?php
                                        print_r($str_tbl);
                                    ?>
                                </tbody>
                            </table>
                            <br>
                               <center>
                            <a href="<?php print_r(base_url()."pdf/pendapatan_lap_usaha/".($th_fn));?>" target="_blank" id="btn_next" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                        </center>

                        <br><br>
                        <center>
                            <div class="box box-solid box-primary" style="width: 50%;">
                                <div class="box-header with-border">
                                    <center>
                                        <h3 class="box-title"><?php print_r($title);?></h3></center>
                                    <div class="box-tools pull-right">
                                    </div>
                                </div>
                                <div class="box-body chart-responsive">
                                    <div>
                                        <br>
                                        <div id="chartdiv" style="width: 100%; height: 450px;"></div>
                                    </div>
                                </div>

                            </div>
                        </center>
                        <h4 align="center">Sumber : BPS Kota Malang</h4>
                        <br>

                        
                    </div>
                </div>
            </div>
        </div>


  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>

<!-- Resources -->
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/core.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/charts.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/animated.js"></script>
    
<script type="text/javascript">
        var data_main = JSON.parse('<?php print_r("$data_graph");?>');

        $(document).ready(function(){
            create_graph(data_main);
            console.log(data_main);
        });


           $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/pendapatan_lap_usaha/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/pendapatan_lap_usaha/".($th_fn-1));?>';
            window.location.href = prev_url;
        });


        function create_graph(data_graph){
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("chartdiv", am4charts.XYChart);

            // Add data
            chart.data = data_graph;

            // Create category axis
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "year";
            categoryAxis.renderer.opposite = true;

            // Create value axis
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.inversed = true;
            valueAxis.title.text = "";
            valueAxis.renderer.minLabelPosition = 0.01;

            // Create series
            <?php print_r($series); ?>

            // Add chart cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.behavior = "zoomY";

            // Add legend
            chart.legend = new am4charts.Legend();
        }
</script>
</body>
</html>
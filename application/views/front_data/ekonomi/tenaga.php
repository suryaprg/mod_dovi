

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h1> </h1>
            </div>
                 <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Jumlah Unit, Tenaga Kerja, dan Nilai Investasi Menurut Bidang Usaha </h1>
                <h2 align="center" > <?php print_r($th_st." - ".$th_fn); ?>     </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
            <!-- /.box-header -->
            <div class="box-body">

                <center>
                   
                    <div>
                        <div class="box-body chart-responsive">

                            <table class="data-table" style="position: relative; bottom: 30px; height: 480px;" width="98%">
                                <caption class="title"></caption>
                                <thead>
                                        <?php
                                            if($kategori){
                                                $str_header1 ="<tr>"; 
                                                $str_header2 ="<tr>"; 

                                                $t_col = 2;
                                                foreach ($kategori as $r_data => $v_data) {
                                                    $str_header1 .= "<th  colspan=\"".$jml_th."\">".$v_data->nama_kategori."</th>";

                                                    for ($i=$th_st; $i <= $th_fn ; $i++) { 
                                                        $str_header2 .= "<th>".$i."</th>";
                                                        $t_col++;
                                                    }
                                                }
                                                $str_header1 .="</tr>";
                                                $str_header2 .="</tr>";
                                            }
                                        ?>
                                    <tr style="font-size: 20px;">
                                        <td colspan="<?=$t_col;?>">Jumlah Unit, Tenaga Kerja, dan Nilai Investasi Menurut Bidang Usaha Kota Malang Tahun <?= $th_st." - ".$th_fn;?></td>
                                    </tr>
                                    <tr style="font-size: 17px;">
                                    	<th rowspan="3" width="5%">No</th>
										<th rowspan="3">Keterangan</th>
                                    </tr>
                                    

                                    <?php 
                                        echo $str_header1;
                                        echo $str_header2;
                                    ?>
                                </thead>
                                <tbody>									

								<?php
									if(isset($str_tbl)){
										if($str_tbl){
                                           print_r($str_tbl);
										}
									}
								?>						
							    </tbody>
                            </table>
                            <center>
                            <a href="<?php print_r(base_url()."pdf/tenaga/".($th_fn));?>" target="_blank" id="btn_next" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                        </div>
                    </div>
                </center>

                <br><br>
                <center>
                    <div class="box box-solid box-primary" style="width: 98%;">
                        <div class="box-header with-border">
                            <center>
                                <h3 class="box-title">Jumlah Unit, Tenaga Kerja, dan Nilai Investasi Menurut Bidang Usaha di Kota Malang Tahun <?= $th_st ." - ". $th_fn; ?></h3></center>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body chart-responsive">
                            <div>
                                <div id="chartdiv" style="width: 100%; height: 500px"></div>
                            </div>
                        </div>
                    </div>
                    <h4>Sumber: Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kota Malang</h4>
                </center>                
            </div>        
        </div>
    </div>
</div>

<tr ></tr>




      

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>

<!-- Resources -->
	<script src="<?= base_url();?>assets/chart_main/amchart_multi_line/core.js"></script>
	<script src="<?= base_url();?>assets/chart_main/amchart_multi_line/charts.js"></script>
	<script src="<?= base_url();?>assets/chart_main/amchart_multi_line/animated.js"></script>
	
	<script type="text/javascript">
        // console.log(JSON.parse('<?php print_r($graph_data);?>'));
        var data_main = JSON.parse('<?php print_r("$graph_data");?>');

		$(document).ready(function(){
			// create_graph()
            console.log(data_main);
		});

        $(".jenis").click(function(){
            var id_jenis_html = $(this).attr("id");
            var id_jenis = id_jenis_html.split("_")[1];
            console.log(data_main[id_jenis]);

            create_graph(data_main[id_jenis]);
        });

		 $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/tenaga/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/tenaga/".($th_fn-1));?>';
            window.location.href = prev_url;
        });

		function create_graph(obj_graph){
			// Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

             // Create chart instance
            var chart = am4core.create("chartdiv", am4charts.XYChart);

            // Add data
            chart.data = obj_graph;

            // Create axes
            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "year";
            categoryAxis.numberFormatter.numberFormat = "#";
            categoryAxis.renderer.inversed = true;
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.cellStartLocation = 0.1;
            categoryAxis.renderer.cellEndLocation = 0.9;

            var  valueAxis = chart.xAxes.push(new am4charts.ValueAxis()); 
            valueAxis.renderer.opposite = true;

            // Create series
            function createSeries(field, name) {
              var series = chart.series.push(new am4charts.ColumnSeries());
              series.dataFields.valueX = field;
              series.dataFields.categoryY = "year";
              series.name = name;
              series.columns.template.tooltipText = "{name}: Rp. [bold]{valueX}[/]";
              series.columns.template.height = am4core.percent(100);
              series.sequencedInterpolation = true;

              var valueLabel = series.bullets.push(new am4charts.LabelBullet());
              valueLabel.label.text = "{valueX}";
              valueLabel.label.horizontalCenter = "left";
              valueLabel.label.dx = 10;
              valueLabel.label.hideOversized = false;
              valueLabel.label.truncate = false;

              var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
              categoryLabel.label.text = "{name}";
              categoryLabel.label.horizontalCenter = "right";
              categoryLabel.label.dx = -10;
              categoryLabel.label.fill = am4core.color("#fff");
              categoryLabel.label.hideOversized = false;
              categoryLabel.label.truncate = false;
            }

            <?php print_r($str_series); ?>
		}
	</script>

</body>
</html>
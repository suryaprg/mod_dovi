<!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><i class="fa fa-clipboard"></i>  KATEGORI</li>

        <li <?php if($this->uri->segment(2) == 'infrastruktur_jalan') echo "class='active'";?>>
          <a href="<?= base_url()."data/infrastruktur_jalan/".date("Y");?>">
           <i class="fa fa-circle"></i>Jalan Raya
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'kendaraan') echo "class='active'";?>>
          <a href="<?= base_url()."data/kendaraan/".date("Y");?>">
            <i class="fa fa-circle"></i>Transportasi
          </a>
        </li>
        <li class="header"><i class="fa fa-align-justify"></i>   LAMPIRAN</li>

           
            <li <?php if($this->uri->segment(2) == 'lp_terminal') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_terminal/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Jumlah Terminal
              </a>
            </li>
                        <li <?php if($this->uri->segment(2) == 'lp_kendaraan') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_kendaraan/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Jumlah kendaraan
              </a>
            </li>
                        <li <?php if($this->uri->segment(2) == 'lp_kendaraan_plat') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_kendaraan_plat/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Plat Kendaraan
              </a>
            </li>
                        <li <?php if($this->uri->segment(2) == 'lp_kendaraan_umum') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_kendaraan_umum/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Kendaraan Umum
              </a>
            </li>          
           
           
          </ul>

        </li>

        
        <!-- <li <?php if($this->uri->segment(3) == 'pendudukMiskin') echo "class='active'";?>>
          <a href="pendudukMiskin">
            <span>Jumlah Penduduk Miskin</span>
          </a>
        </li> -->
        <!-- <li <?php if($this->uri->segment(3) == 'koperasi') echo "class='active'";?>>
          <a href="koperasi">
            <span>Data Koperasi</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(3) == 'industri') echo "class='active'";?>>
          <a href="industri">
            <span>Data Industri</span>
          </a>
        </li> -->
        <!-- <li <?php if($this->uri->segment(3) == 'jasaKeuangan') echo "class='active'";?>>
          <a href="jasaKeuangan">
            <span>Data Jasa Keuangan</span>
          </a>
        </li>
            <li <?php if($this->uri->segment(3) == 'pajak') echo "class='active'";?>>
          <a href="pajak">
            <span>Pajak</span>
          </a>
        </li> -->
      </ul>
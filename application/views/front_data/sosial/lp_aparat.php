

<!-- HTML -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
        <div class="box box-solid">
            <div class="box-header with-border">
               <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Jumlah Aparat dan Sarana Keamanan</h1>
                <h2 align="center" > <?php print_r($th_st." - ".$th_fn); ?>     </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box-body chart-responsive">
                            <table class="data-table" style="width: 90%; vertical-align: top;">
                                <caption class="title"></caption>
                                <thead>
                                    <tr style="font-size: 20px;">
                                        <td colspan="7">Data Jumlah Aparat dan Sarana Keamanan di Kota Malang Tahun <?= $th_st." - ".$th_fn;?></td>
                                    </tr>
                                    <tr style="font-size: 17px;">
                                        <th width="5%">No</th>
                                        <th>Keterangan</th>
                                        <?php
                                            if($th_st){
                                                for ($i=$th_st; $i <= $th_fn ; $i++) { 
                                                    echo "<th>".$i."</th>";
                                                }
                                            }
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>                                 

                                <?php
                                    if(isset($list_data)){
                                        if($list_data){
//                                             print_r("<pre>")
// ;                                           print_r($list_data);
                                            $no = 1;
                                            foreach ($list_data as $r_data => $v_data) {
                                                echo "<tr class=\"jenis\" id=\"jenis_".$v_data["main"]["id_jenis"]."\">
                                                        <td><a href=\"\">".$no."</a></td>
                                                        <td><a href=\"\">".$v_data["main"]["aparat"]."</a></td>";

                                                for ($i=$th_st; $i <= $th_fn ; $i++) {
                                                    if($v_data["value"][$i]){
                                                        echo "<td>".$v_data["value"][$i]."</td>";
                                                    }else{
                                                        echo "<td>-</td>";
                                                    }
                                                    
                                                }

                                                echo "                           
                                                    </tr>";
                                                $no++;
                                            }
                                        }
                                    }
                                ?>                      
                                </tbody>
                            </table>
                                 <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/lp_aparat/".($th_fn));?>" id="btn_next"  target="_blank" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <br><br>
                        <div class="box box-solid box-primary">
                            <div class="box-header with-border">
                                <center>
                                    <h3 class="box-title">Jumlah Aparat dan Sarana Keamanan di Kota Malang Tahun <?= $th_st ." - ". $th_fn; ?></h3></center>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <div class="box-body chart-responsive">
                                <div>
                                    <div id="chartdiv" style="width: 100%;"></div>
                                </div>
                            </div>
                            <h4 align="center">Sumber : Satuan Polisi Pamong Praja Kota Malang</h4>
                        </div>
                    </div>
                </div>
                      
            </div>        
        </div>
    </div>
</div>





      
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>

<!-- Resources -->
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/core.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/charts.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/animated.js"></script>

    <script type="text/javascript">
        var data_main = JSON.parse('<?php print_r("$graph_data");?>');

        $(document).ready(function(){
            create_graph(data_main[1])
            console.log(data_main);
        });

        $(".jenis").click(function(){
            var id_jenis_html = $(this).attr("id");
            var id_jenis = id_jenis_html.split("_")[1];
            
            console.log(data_main[id_jenis]);

            create_graph(data_main[id_jenis]);
        });

                 $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/lp_aparat/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/lp_aparat/".($th_fn-1));?>';
            window.location.href = prev_url;
        });


        function create_graph(graph_data){
            am4core.useTheme(am4themes_animated);

            var chart = am4core.create("chartdiv", am4charts.XYChart);


            chart.data = graph_data;

            chart.padding(40, 40, 40, 40);

            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.dataFields.category = "year";
            categoryAxis.renderer.minGridDistance = 60;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.categoryX = "year";
            series.dataFields.valueY = "val_data";
            series.tooltipText = "{valueY.value}"
            series.columns.template.strokeOpacity = 0;

            chart.cursor = new am4charts.XYCursor();

            // as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
            series.columns.template.adapter.add("fill", function (fill, target) {
                return chart.colors.getIndex(target.dataItem.index);
            });
        }
    </script>

</body>
</html>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
             <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Parlementaria</h1>
                <h2 align="center" > <?php print_r($th_st." - ".$th_fn); ?>     </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Dewan Perwakilan Rakyat Daerah Kabupaten/Kota (DPRD) merupakan lembaga 
perwakilan rakyat daerah yang berkedudukan sebagai unsur penyelenggara pemerintahan 
daerah kabupaten/kota. DPRD kabupaten/kota terdiri atas anggota partai politik peserta 
pemilihan umum yang dipilih melalui pemilihan umum  Jumlah anggota DPRD Kota Malang tahun 
2014-2016 jumlahnya masih sama yaitu sebanyak 45 orang dengan jumlah terbanyak partai PDI
P sebanyak 11 orang. Posisi kedua ditempati oleh PKB sebanyak 6 orang.  
Dari 45 anggota DPRD Kota Malang masih didominasi laki-laki sebanyak 75,56 persen dan 
sisanya sebanyak 24,44 persen adalah perempuan.  Tingkat pendidikan anggota DPRD Kota 
Malang juga sudah semakin baik karena sebagian besar merupakan lulusan S1/S2 (82,22 %) 
sehingga diharapkan bisa menghasilkan produk-produk hukum yang lebih baik dan lebih 
diperlukan oleh masyarakat Kota Malang. 
  </p>
</section> 
    <br>
    <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box-body chart-responsive">

                            <table class="data-table" style="width: 100%;"
                                <caption class="title"></caption>
                                <thead>
                                    <?php print_r($str_header);?>
                                </thead>
                                <tbody>
                                    <?php print_r($str_tbl); ?>
                                </tbody>
                            </table>
                                 <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/dpr/".($th_fn));?>" id="btn_next"  target="_blank" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                        </div>
                    </div>
                    <div class="col-sm-12"><br>
                        <div class="box box-solid box-primary">
                            <div class="box-header with-border">
                                <center>
                                    <h3 class="box-title">Data Parlementaria di Kota Malang Tahun <?= $th_fn; ?></h3></center>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <div class="box-body chart-responsive">
                                <div>
                                    <div id="chartdiv" style="width: 100%; height: 650px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>       
            </div> 

      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>

<!-- Resources -->
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/core.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/charts.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/animated.js"></script>

    <script type="text/javascript">
        var data_main = JSON.parse('<?php print_r("$data_graph");?>');

        $(document).ready(function(){
            create_graph(data_main);
            console.log(data_main);
        });

        // $(".jenis").click(function(){
        //     var id_jenis_html = $(this).attr("id");
        //     var id_jenis = id_jenis_html.split("_")[1];
            
        //     console.log(data_main[id_jenis]);

        //     create_graph(data_main[id_jenis]);
        // });
           $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/parlementeria/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/parlementeria/".($th_fn-1));?>';
            window.location.href = prev_url;
        });


        function create_graph(data_graph){
           // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("chartdiv", am4charts.XYChart);

            // Add data
            chart.data = data_graph;

            chart.legend = new am4charts.Legend();
            chart.legend.position = "right";

            // Create axes
            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "nama_jenis";
            categoryAxis.renderer.grid.template.opacity = 0;

            var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
            valueAxis.min = 0;
            valueAxis.renderer.grid.template.opacity = 0;
            valueAxis.renderer.ticks.template.strokeOpacity = 0.5;
            valueAxis.renderer.ticks.template.stroke = am4core.color("#495C43");
            valueAxis.renderer.ticks.template.length = 10;
            valueAxis.renderer.line.strokeOpacity = 0.5;
            valueAxis.renderer.baseGrid.disabled = true;
            valueAxis.renderer.minGridDistance = 40;

            // Create series
            function createSeries(field, name) {
              var series = chart.series.push(new am4charts.ColumnSeries());
              series.dataFields.valueX = field;
              series.dataFields.categoryY = "nama_jenis";
              series.stacked = true;
              series.name = name;
              
              var labelBullet = series.bullets.push(new am4charts.LabelBullet());
              labelBullet.locationX = 0.5;
              labelBullet.label.text = "{valueX}";
              labelBullet.label.fill = am4core.color("#fff");
            }

            createSeries("val_l", "Jumlah Laki-Laki");
            createSeries("val_p", "Jumlah Perempuan");
            createSeries("val_jml", "Jumlah Keseluruhan");
        }
    </script>

</body>
</html>
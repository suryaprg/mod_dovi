<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SOSIAL, POLITIK DAN KEAMANAN</title>
  <link rel="shortcut icon" href="https://malangkota.go.id/wp-content/themes/malangkotanew/images/favicon.png"> 
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
   <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts7/tabledata.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/css/style.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts/css/style.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="fh5co-loader"></div>
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
     <div style="position: absolute; left: 50px; top: 0.1px;"><img  height="47px" style="padding: 5px" src="<?php echo base_url();?>assets/template_aw/dist/img/ncc-logo-white.png" alt="NCC logo"></div>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <li class="dropdown user user-menu">
            <a href="<?php echo base_url();?>web_home/web">
               <div style="position: absolute; top: 11px; right: 130px;"><img src="<?php echo base_url();?>assets/template_aw/dist/img/logo.png" width="25" height="25" alt="User Image"></div>&nbsp &nbsp &nbsp &nbsp &nbsp
             <span class="hidden-xs">Malang Open Data</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url();?>assets/template_aw/dist/img/logo.png" alt="User Image">
        </div>
        <div class="pull-left info" style="position: absolute; left: 50px;">
          <p>SOSIAL, POLITIK DAN <br> KEAMANAN</p>
          <a href="#"><i class="fa fa-circle text-success"></i>Pemerintah Kota Malang</a>
        </div>
      </div>
      <br>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><i class="fa fa-clipboard"></i>  KATEGORI</li>

        <li <?php if($this->uri->segment(2) == 'parlementeria') echo "class='active'";?>>
          <a href="<?php echo base_url()."data/parlementeria/". date("Y");?>">
            <i class="fa fa-circle"></i>Parlementaria
          </a>
        </li>
             <li <?php if($this->uri->segment(2) == 'rt_rw') echo "class='active'";?>>
              <a href="<?= base_url()."data/rt_rw/".date("Y");?>">
                <i class="fa fa-circle"></i>Kelembagaan RT RW
              </a>
            </li>
<!--         <li <?php if($this->uri->segment(2) == 'aparat_keamanan') echo "class='active'";?>>
          <a href="<?php echo base_url();?>data/aparat_keamanan">
             <i class="fa fa-circle"></i>Aparat dan Sarana Keamanan
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'pidana_penyelesaian') echo "class='active'";?>>
          <a href="<?php echo base_url();?>data/pidana_penyelesaian">
             <i class="fa fa-circle"></i>Tindak Pidana dan Penyelesaiannya
          </a>
        </li> -->
            <li class="header"><i class="fa fa-align-justify"></i>   LAMPIRAN</li>

           
            <li <?php if($this->uri->segment(2) == 'lp_aparat') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_aparat/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Aparat
              </a>
            </li>
              <li <?php if($this->uri->segment(2) == 'lp_pidana') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_pidana/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Pidana
              </a>
            </li>
              <li <?php if($this->uri->segment(2) == 'lp_pidana_kec') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_pidana_kec/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Pidana Kecamatan
              </a>
            </li>
              <li <?php if($this->uri->segment(2) == 'lp_pmks') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_pmks/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran PMKS
              </a>
            </li>
                <li <?php if($this->uri->segment(2) == 'daftar_perijinan_terbit') echo "class='treeview active'";?><?php if($this->uri->segment(2) == 'daftar_perijinan_investasi') echo "class='treeview active'";?><?php if($this->uri->segment(2) == 'daftar_perijinan_tk') echo "class='treeview active'";?>>
          <a href="#">
            <i class="fa fa-circle-o"></i> <span>Data Perijinan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'daftar_perijinan_terbit') echo "class='active'";?>><a href="<?=base_url()."data/daftar_perijinan_terbit/".date("Y");?>"><i class="fa fa-circle-o"></i>Terbit</a></li>
            <li <?php if($this->uri->segment(2) == 'daftar_perijinan_tk') echo "class='active'";?>><a href="<?=base_url()."data/daftar_perijinan_tk/".date("Y");?>"><i class="fa fa-circle-o"></i>TK</a></li>
            <li <?php if($this->uri->segment(2) == 'daftar_perijinan_investasi') echo "class='active'";?>><a href="<?=base_url()."data/daftar_perijinan_investasi/".date("Y");?>"><i class="fa fa-circle-o"></i>Investasi</a></li>
          </ul>
        </li>
          
           
          </ul>

        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->

    <?php
            switch($page){
             
                case 'parpol'      : include "parpol.php";
               break;
                case 'parlementeria'      : include "parlementeria.php";
               break;
                   case 'rt_rw'      : include "rt_rw.php";
               break;
                case 'aparat_keamanan'      : include "aparatKeamanan.php";
               break;
                case 'pidana_penyelesaian'      : include "pidanaPenyelesaian.php";
               break;
                 case 'aparat'      : include "lp_aparat.php";
               break;
                 case 'pidana'      : include "pidana.php";
               break;
                 case 'pidana_kec'      : include "pidana_kecamatan.php";
               break;
               case 'pmks'      : include "pmks.php";
               break;

               case 'investasi'      : include "investasi.php";
               break;
                case 'terbit'      : include "terbit.php";
               break;
                case 'tk'      : include "tk.php";
               break;
                case 'bkd'      : include "bkd.php";
               break;
                 
                 
            }
            ?>

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2018 <a href="http://kominfo.malangkota.go.id/" target="_blank">Kominfo Kota Malang</a>
  </footer>


</body>

</html>

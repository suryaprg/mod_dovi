

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
        <div class="box box-solid">
            <div class="box-header with-border">
                <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Data Perijinan TK</h1>
                <h2 align="center" > <?php print_r($th_fn); ?>     </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>

            </div>
            <!-- /.box-header -->
            <div class="box-body"  style="overflow-x:auto;">
                <center>
                    <div class="box box-solid box-primary" style="width: 70%;">
                        <div class="box-header with-border">
                            <center>
                                <h3 class="box-title"><?php print_r($title);?></h3></center>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body chart-responsive">
                            <div>
                                <div id="chartdiv" style="width: 100%; height: 500px"></div>
                            </div>
                        </div>
                    </div>
                </center>
                <td colspan=\"2\"></td>
                <br><br>
                <center>
                    <div>
 
                        <div class="box-body chart-responsive">

                            <table class="data-table" style="position: relative; bottom: 30px; width: 40%;"
                                <caption class="title"></caption>
                                <thead>
                                    <?php print_r($str_header);?>
                                </thead>
                                <tbody>
                                    <?php print_r($str_tbl); ?>
                                </tbody>
                            </table>
                                <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/daftar_perijinan_tk/".($th_fn));?>" id="btn_next"  target="_blank" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                        </div>
                    </div>
                </center>
               <h4 align="center">Sumber : Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kota Malang</h4>
            </div>        
        </div>
    </div>
</div>







  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script type="text/javascript">

           var data_main = JSON.parse('<?php print_r("$data_graph");?>');

            $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/daftar_perijinan_tk/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/daftar_perijinan_tk/".($th_fn-1));?>';
            window.location.href = prev_url;
        });

       

        $(".jenis").click(function(){
            var id_jenis_html = $(this).attr("id");
            var id_jenis = id_jenis_html.split("_")[1];

            create_graph(data_main[id_jenis]);
            
        });

        function create_graph(data_graph){  
            var chart = AmCharts.makeChart("chartdiv", {
              "type": "serial",
              "theme": "none",
              "marginRight": 70,
              "dataProvider": data_graph,
              "valueAxes": [{
                "axisAlpha": 0,
                "position": "left",
                "title": "Perijinan Terbit"
              }],
              "startDuration": 1,
              "graphs": [{
                "balloonText": "<b>[[category]]: [[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "val_data"
              }],
              "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
              },
              "categoryField": "bulan",
              "categoryAxis": {
                "gridPosition": "start",
                "labelRotation": 45
              },
              "export": {
                "enabled": true
              }

            });
        }

</script>

</body>
</html>
      <ul class="sidebar-menu">
        <li class="header"><i class="fa fa-clipboard"></i>  KATEGORI</li>
        <li>
          <a href="#">
             <i class="fa fa-circle"></i> Lahan Pertanian <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <!-- <li <?php if($this->uri->segment(3) == 'pertanian_lahan') echo "class='active'";?>>
              <a href="<?= base_url()."data/pertanian_lahan/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lahan Berdasarkan Jenis
              </a>
            </li> -->
            <li <?php if($this->uri->segment(3) == 'pertanian_lahan_penggunaan') echo "class='active'";?>>
              <a href="<?= base_url()."data/pertanian_lahan_penggunaan/".date("Y");?>">
               <i class="fa fa-circle"></i> Lahan Berdasarkan <br>Penggunaannya
              </a>
            </li>
            
          </ul>
        </li>
        <li>
          <a href="#">
             <i class="fa fa-circle"></i> Pertanian dan Hortikultura <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(3) == 'komoditi') echo "class='active'";?>>
              <a href="<?= base_url()."data/komoditi/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Produksi Tanaman Pangan
              </a>
            </li>
            <li <?php if($this->uri->segment(3) == 'hortikultura') echo "class='active'";?>>
              <a href="<?= base_url()."data/hortikultura/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Produksi Buah dan Sayuran
              </a>
            </li>
            <li <?php if($this->uri->segment(3) == 'pertanian_kelapa_tebu') echo "class='active'";?>>
              <a href="<?= base_url()."data/pertanian_kelapa_tebu/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Produksi Kelapa dan Tebu
              </a>
            </li>
          </ul>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-circle"></i> Peternakan dan Perikanan<i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(3) == 'peternakan') echo "class='active'";?>>
              <a href="<?= base_url()."data/peternakan/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Produksi Peternakan
              </a>
            </li>
          </ul>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(3) == 'perikanan') echo "class='active'";?>>
              <a href="<?= base_url()."data/perikanan/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Produksi Perikanan
              </a>
            </li>
          </ul>
        </li>
         <li class="header"><i class="fa fa-align-justify"></i>   LAMPIRAN</li>

           
            <li <?php if($this->uri->segment(2) == 'lp_lahan_kec') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_lahan_kec/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Luas Lahan Kecamatan
              </a>
            </li>
                        <li <?php if($this->uri->segment(2) == 'lp_panen') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_panen/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Luas Hasil Panen
              </a>
            </li>
                    <li <?php if($this->uri->segment(2) == 'lp_hasil_panen') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_hasil_panen/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Produksi Hasil Panen
              </a>
            </li>
                      <li <?php if($this->uri->segment(2) == 'lp_prod_panen') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_prod_panen/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Produktivitas 
                <br>Hasil Panen
              </a>
            </li>
      
      </ul>
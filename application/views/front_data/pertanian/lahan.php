<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PERTANIAN DAN PETERNAKAN</title>
    <link rel="shortcut icon" href="https://malangkota.go.id/wp-content/themes/malangkotanew/images/favicon.png">

    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts8/tabeldata.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/css/style.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/template_aw/charts/css/style.css">

    <link rel="stylesheet" href="<?= base_url();?>assets/chart_main/amchart_vertical/export.css" type="text/css" media="all" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="fh5co-loader"></div>
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="index2.html" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>A</b>LT</span>
                <!-- logo for regular state and mobile devices -->
                <div style="position: absolute; left: 50px; top: 0.1px;"><img height="47px" style="padding: 5px" src="<?php echo base_url();?>/assets/template_aw/dist/img/ncc-logo-white.png" alt="NCC logo"></div>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <li class="dropdown user user-menu">
                            <a href="<?php echo base_url();?>web_home/web">
                                <div style="position: absolute; top: 11px; right: 130px;"><img src="<?php echo base_url();?>/assets/template_aw/dist/img/logo.png" width="25" height="25" alt="User Image"></div>&nbsp &nbsp &nbsp &nbsp &nbsp
                                <span class="hidden-xs">Malang Open Data</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo base_url();?>/assets/template_aw/dist/img/logo.png" alt="User Image">
                    </div>
                    <p class="contoh3" style="position: absolute; top: 15px; left: 61px; color: white; ">PERTANIAN DAN PETERNAKAN</p>
                    <div class="pull-left info" style="position: absolute; left: 45px; bottom: 5px;">
                        <br>
                        <br>
                        <a href="#"><i class="fa fa-circle text-success"></i>Pemerintah Kota Malang</a>
                    </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <?php include "nav_menu.php"; ?>
            </section>
            <!-- /.sidebar -->
        </aside>

        


<!-- HTML -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
        <div class="box box-solid">
            <div class="box-header with-border">
               <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Jenis Lahan</h1>
                <h2 align="center" > <?php print_r($th_st." - ".$th_fn); ?>     </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <section>
                            <br>
                            <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Sektor pertanian mempunyai peranan yang penting dan strategis dalam pembangunan
                                daerah, peranan tersebut antara lain sebagai penyedia lapangan kerja, perolehan nilai tambah
                                dan daya saing, dan pemenuhan kebutuhan konsumsi dan bahan baku industri serta optimalisasi
                                pengelolaan sumber daya alam secara berkelanjutan. Sektor Pertanian walaupun di Kota Malang
                                bukan merupakan sektor utama yang menunjang perekonomian, namun sektor ini penting untuk
                                dikembangkan mengingat peranannya yang penting dan strategis.
                            </p>
                </section>
                <br><br>

                <center>
                    <div class="box box-solid box-primary" style="width: 70%;">
                        <div class="box-header with-border">
                            <center>
                                <h3 class="box-title">Persentase Luas Lahan menurut Jenis Lahan di Kota Malang Tahun <?= $th_fn; ?></h3></center>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body chart-responsive">
                            <div>
                                <div id="chartdiv_pie" style="width: 100%; height: 500px;"></div>
                            </div>
                        </div>
                    </div>
                </center>
                
                            <br>
                            <table class="data-table table-responsive" style="position: relative; bottom: 30px; width: 50%;">
                                <caption class="title"></caption>
                                <thead>
                                    <tr>
                                        <td colspan="5" style="font-size: 17px;">Persentase Luas Lahan menurut Jenis Lahan di Kota Malang Tahun <?= $th_fn; ?></td>
                                    </tr>
                                    <tr>
                                        <th>No</th>
                                        <th>Jenis Lahan</th>
                                        <th>Luas Lahan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(isset($list_data_pn)){
                                        if($list_data_pn){
                                            $no = 1; 
                                            $str_data_pn = "";
                                            foreach ($list_data_pn as $r_data => $v_data) {
                                                echo "<tr>
                                                        <td>".$no."</td>
                                                        <td>".$v_data->jenis_lahan."</td>
                                                        <td>".$v_data->luas_lahan."</td>
                                                    </tr>";
                                                $str_data_pn .= "{
                                                                \"jenis_lahan\": '\"".$v_data->jenis_lahan."\"',
                                                                \"luas_lahan\": ".$v_data->luas_lahan."
                                                              },";          
                                            }
                                        }
                                    }
                                    
                                    ?>

                                </tbody>


                            </table>
                                <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/pertanian_lahan/".($th_fn));?>" id="btn_next"  target="_blank" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                             <h4 align="center">Sumber : Dinas Pertanian dan Ketahanan Pangan Kota Malang</h4>
                             <br>
                             <br>




                <center>
                    <div class="box box-solid box-primary" style="width: 70%;">
                        <div class="box-header with-border">
                            <center>
                                <h3 class="box-title">Luas Lahan Berdasarkan Penggunaannya di Kota Malang Tahun <?= $th_fn; ?> (Hektar)</h3></center>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body chart-responsive">
                            <div>
                                <div id="chartdiv" style="width: 100%; height: 500px;"></div>
                            </div>
                        </div>
                    </div>
                </center>
                
                            <br>
                            <table class="data-table table-responsive" style="position: relative; bottom: 30px; width: 50%;">
                                <caption class="title"></caption>
                                <thead>
                                    <tr>
                                        <td colspan="5" style="font-size: 17px;">Luas Lahan Berdasarkan Penggunaannya di Kota Malang Tahun <?= $th_fn; ?></td>
                                    </tr>
                                    <tr>
                                        <th>No</th>
                                        <th>Jenis Lahan</th>
                                        <th>Luas Lahan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(isset($list_data)){
                                        if($list_data){
                                            $no = 1; 
                                            $str_data = "";
                                            foreach ($list_data as $r_data => $v_data) {
                                                echo "<tr>
                                                        <td>".$no."</td>
                                                        <td>".$v_data->jenis_peng."</td>
                                                        <td>".$v_data->luas_pend."</td>
                                                    </tr>";
                                                $str_data .= "{
                                                                    \"year\": '".$v_data->jenis_peng."',
                                                                    \"luas_lahan\": ".$v_data->luas_pend."
                                                                },";          
                                            }
                                        }
                                    }
                                    
                                    ?>

                                </tbody>


                            </table>
                                <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/pertanian_lahan_penggunaan/".($th_fn));?>" id="btn_next" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                            <h4 align="center">Sumber : Dinas Pertanian dan Ketahanan Pangan Kota Malang</h4>
                
            </div>        
        </div>
    </div>
</div>





        <footer class="main-footer">
            <strong>Copyright &copy; 2018 <a href="http://kominfo.malangkota.go.id/" target="_blank">Kominfo Kota Malang</a>
        </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>
<!-- Resources -->
<script src="<?= base_url();?>assets/chart_main/amchart_vertical/amcharts.js"></script>
<script src="<?= base_url();?>assets/chart_main/amchart_vertical/serial.js"></script>
<script src="<?= base_url();?>assets/chart_main/amchart_vertical/export.min.js"></script>
<script src="<?= base_url();?>assets/chart_main/amchart_vertical/light.js"></script>

<!-- Chart code -->
<script>
var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
     "theme": "light",
    "categoryField": "year",
    "rotate": true,
    "startDuration": 1,
    "categoryAxis": {
        "gridPosition": "start",
        "position": "left"
    },
    "trendLines": [],
    "graphs": [{
            "balloonText": "luas_lahan:[[value]]",
            "fillAlphas": 0.8,
            "id": "AmGraph-1",
            "lineAlpha": 0.2,
            "title": "luas_lahan",
            "type": "column",
            "valueField": "luas_lahan"
        }],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "position": "top",
            "axisAlpha": 0
        }
    ],
    "allLabels": [],
    "balloon": {},
    "titles": [],
    "dataProvider": [<?=$str_data;?>],
    "export": {
        "enabled": true
     }

});

   

</script>

<!-- Resources -->
<!-- <script src="<?= base_url();?>assets/chart_main/amchart_donut/amcharts.js"></script> -->
<script src="<?= base_url();?>assets/chart_main/amchart_donut/pie.js"></script>
<!-- <script src="<?= base_url();?>assets/chart_main/amchart_donut/export.min.js"></script> -->
<!-- <link rel="stylesheet" href="<?= base_url();?>assets/chart_main/amchart_donut/export.css" type="text/css" media="all" /> -->
<!-- <script src="<?= base_url();?>assets/chart_main/amchart_donut/light.js"></script> -->

<script type="text/javascript">
    var chart = AmCharts.makeChart( "chartdiv_pie", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [<?=$str_data_pn;?>],
  "valueField": "luas_lahan",
  "titleField": "jenis_lahan",
   "balloon":{
   "fixedPosition":true
  },
  "export": {
    "enabled": true
  }
} );

</script>
<script type="text/javascript">
    $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/pertanian_lahan_penggunaan/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/pertanian_lahan_penggunaan/".($th_fn-1));?>';
            window.location.href = prev_url;
        });
</script>
</body>
</html>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PERTANIAN DAN PETERNAKAN</title>
    <link rel="shortcut icon" href="https://malangkota.go.id/wp-content/themes/malangkotanew/images/favicon.png">

    <style type="text/css">
        .contoh1 {
            font-size: 16px;
        }
        
        .contoh2 {
            font-size: 16px;
            line-height: 20px;
        }
        
        .contoh3 {
            font-size: 11px;
            line-height: 1.1em;
        }
        
        .contoh4 {
            line-height: 1.1;
        }
    </style>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/charts8/tabeldata.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/css/style.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/template_aw/charts/css/style.css">

    <link rel="stylesheet" href="<?= base_url();?>assets/chart_main/amcharts_left/export.css" type="text/css" media="all" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="fh5co-loader"></div>
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="index2.html" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>A</b>LT</span>
                <!-- logo for regular state and mobile devices -->
                <div style="position: absolute; left: 50px; top: 0.1px;"><img height="47px" style="padding: 5px" src="<?php echo base_url();?>/assets/template_aw/dist/img/ncc-logo-white.png" alt="NCC logo"></div>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <li class="dropdown user user-menu">
                            <a href="<?php echo base_url();?>web_home/web">
                                <div style="position: absolute; top: 11px; right: 130px;"><img src="<?php echo base_url();?>/assets/template_aw/dist/img/logo.png" width="25" height="25" alt="User Image"></div>&nbsp &nbsp &nbsp &nbsp &nbsp
                                <span class="hidden-xs">Malang Open Data</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo base_url();?>/assets/template_aw/dist/img/logo.png" alt="User Image">
                    </div>
                    <p class="contoh3" style="position: absolute; top: 15px; left: 61px; color: white; ">PERTANIAN DAN PETERNAKAN</p>
                    <div class="pull-left info" style="position: absolute; left: 45px; bottom: 5px;">
                        <br>
                        <br>
                        <a href="#"><i class="fa fa-circle text-success"></i>Pemerintah Kota Malang</a>
                    </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <?php include "nav_menu.php"; ?>
            </section>
            <!-- /.sidebar -->
        </aside>

        


<!-- HTML -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
        <div class="box box-solid">
            <div class="box-header with-border">
                <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Produksi Tanaman Buah dan Sayur</h1>
                <h2 align="center" > <?php print_r($th_st); ?>     </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <section>
                            <br>
                            <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Subsektor hortikultura telah berkontribusi secara nyata dalam mendukung
                                perekonomian baik nasional maupun daerah dalam hal penyediaan produk pangan, kesehatan,
                                dan kosmetika, budaya dan pariwisata, perdagangan, penciptaan produk domestik bruto
                                maupun dalam penyerapan tenaga kerja. Dengan berkembangnya perekonomian dan
                                pengetahuan masyarakat, makin meningkat pula kesadaran akan pentingnya buah-buahan dan
                                sayuran sebagai sumber gizi dan pangan sehari-hari.
                            </p>
                </section>
                <section>
                            <h3> Produksi Tanaman Buah</h3>
                            <br>
                            <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
                                 Tanaman buah-buahan tahunan adalah tanaman sumber vitamin, mineral, dan lain-lain
                                yang dikonsumsi dari bagian tanaman berupa buah dan merupakan tanaman tahunan, umumnya
                                dapat dikonsumsi tanpa dimasak terlebih dahulu (dikonsumsi segar). Berdasarkan data dari Dinas
                                Pertanian dan Ketahanan Pangan Kota Malang, dapat diketahui bahwa pada tahun 2016 hampir
                                semua jenis tanaman buah-buahan tahunan di Kota Malang sudah berproduksi kecuali tanaman
                                manggis, nenas, jengkol, dan apel. Pada tahun 2016 buah mangga merupakan buah dengan
                                produksi yang paling banyak di Kota Malang yaitu mencapai 1.013,70 kuintal. Buah Jeruk
                                Siam/Keprok merupakan tanaman buah tahunan dengan produksi terbanyak kedua setelah buah
                                mangga di Kota Malang pada tahun 2016 dengan jumlah produksi mencapai 5.429 ton,
                                sedangkan buah yang produksinya paling sedikit di Kota Malang adalah buah duku/langsat
                                dengan jumlah produksi hanya mencapai 5 kuintal. 
                            </p>
                </section>
                <center>
                    <div class="box box-solid box-primary" style="width: 70%;">
                        <div class="box-header with-border">
                            <center>
                                <h3 class="box-title">Grafik Produksi Tanaman Buah Tahunan di Kota Malang Tahun <?= $th_st; ?> (kuintal)</h3></center>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body chart-responsive">
                            <div>
                                <div id="chartdiv_buah" style="width: 100%; height: 500px;"></div>
                            </div>
                        </div>
                    </div>
                </center>
                <br>
                            <br>
                            <table class="data-table table-responsive" style="position: relative; bottom: 30px; width: 50%;">
                                <caption class="title"></caption>
                                <thead>
                                    <tr>
                                        <td colspan="7" style="font-size: 17px;">Produksi Tanaman Buah Tahunan di Kota Malang Tahun <?= $th_st; ?> (kuintal)</td>
                                    </tr>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Sayuran</th>
                                        <th>Jumlah Panen (kuintal)</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    if(isset($list_data)){
                                        if($list_data){
                                            $no = 1;
                                            $data_buah = "";
                                            foreach ($list_data as $r_data => $v_data) {
                                                if($v_data){
                                                    if($v_data["kategori"] == 0){
                                                        echo "<tr>
                                                            <td>".$no."</td>
                                                            <td>".$v_data["nama_jenis"]."</td>
                                                            <td>".$v_data["jml"]."</td>
                                                        </tr>";
                                                        $data_buah .= "{
                                                                         \"year\": '".$v_data["nama_jenis"]."',
                                                                         \"income\": ".$v_data["jml"]."
                                                                     },";
                                                        $no++;    
                                                    }
                                                    
                                                }
                                                
                                            } 
                                        }
                                    }
                                    
                                        
                                    ?>

                                </tbody>


                            </table>
                                 <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/hortikultura/".($th_fn));?>" id="btn_next" target="_blank" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                            <h4 align="center">Sumber : Dinas Pertanian dan Ketahanan Pangan Kota Malang</h4>

                <section>
                            <h3> Produksi Tanaman Sayur</h3>
                            <br>
                            <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
                                 Tanaman sayuran semusim adalah tanaman sumber vitamin, mineral, dan lain-lain yang
                                dikonsumsi dari bagian tanaman yang berupa daun, bunga, buah, dan umbinya yang berumur
                                kurang dari satu tahun. Sedangkan tanaman buah-buahan semusim adalah tanaman sumber
                                vitamin, mineral, dan lain-lain yang dikonsumsi dari bagian tanaman berupa buah yang berumur
                                kurang dari satu tahun. Berdasarkan data dari Dinas Pertanian dan Ketahanan Kota Malang
                                diketahui bahwa tanaman sayuran dan buah-buahan semusim pada tahun 2016 di Kota Malang
                                yang berproduksi hanya terdapat 9 (sembilan) jenis tanaman sayuran semusim,sedangkan untuk
                                buah-buahan semusim tidak ada yang berproduksi. 
                            </p>
                </section>

                <center>
                    <div class="box box-solid box-primary" style="width: 80%;">
                        <div class="box-header with-border">
                            <center>
                                <h3 class="box-title">Grafik Produksi Tanaman Sayur Tahunan di Kota Malang Tahun <?= $th_st; ?> (kuintal)</h3></center>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body chart-responsive">
                            <div>
                                <div id="chartdiv" style="width: 100%; height: 500px;"></div>
                            </div>
                        </div>
                    </div>
                </center>
                <br>
                            <br>
                            <table class="data-table table-responsive" style="position: relative; bottom: 30px; width: 50%;">
                                <caption class="title"></caption>
                                <thead>
                                    <tr>
                                        <td colspan="7" style="font-size: 17px;">Produksi Tanaman Sayur Tahunan di Kota Malang Tahun <?= $th_st; ?> (kuintal)</td>
                                    </tr>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Sayuran</th>
                                        <th>Jumlah Panen (kuintal)</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    if(isset($list_data)){
                                        if($list_data){
                                            $no = 1;
                                            $data_sayur = "";
                                            foreach ($list_data as $r_data => $v_data) {
                                                if($v_data){
                                                    
                                                    
                                                    if($v_data["kategori"] == 1){
                                                        echo "<tr>
                                                            <td>".$no."</td>
                                                            <td>".$v_data["nama_jenis"]."</td>
                                                            <td>".$v_data["jml"]."</td>
                                                        </tr>";
                                                        $data_sayur .= "{
                                                                         \"year\": '".$v_data["nama_jenis"]."',
                                                                         \"income\": ".$v_data["jml"]."
                                                                     },";
                                                        $no++;    
                                                    }
                                                    
                                                }
                                                
                                            } 
                                        }
                                    }
                                    
                                        
                                    ?>

                                </tbody>


                            </table>
                            <h4 align="center">Sumber : Dinas Pertanian dan Ketahanan Pangan Kota Malang</h4>
                <br>
                <br>

                
            </div>        
        </div>
    </div>
</div>





        <footer class="main-footer">
            <strong>Copyright &copy; 2018 <a href="http://kominfo.malangkota.go.id/" target="_blank">Kominfo Kota Malang</a>
        </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>

<!-- Resources -->
<script src="<?= base_url();?>assets/chart_main/amcharts_left/amcharts.js"></script>
<script src="<?= base_url();?>assets/chart_main/amcharts_left/serial.js"></script>
<script src="<?= base_url();?>assets/chart_main/amcharts_left/export.min.js"></script>

<script src="<?= base_url();?>assets/chart_main/amcharts_left/light.js"></script>

<!-- Chart code -->
<script>
var chart = AmCharts.makeChart("chartdiv", {
    "theme": "light",
    "type": "serial",
    "dataProvider": [<?=$data_sayur;?>],
    "valueAxes": [{
        "title": "Produksi per Tahun (Kuintal)"
    }],
    "graphs": [{
        "balloonText": "Produksi in [[category]]:[[value]]",
        "fillAlphas": 1,
        "lineAlpha": 0.2,
        "title": "Income",
        "type": "column",
        "valueField": "income"
    }],
    "depth3D": 0,
    "angle": 0,
    "rotate": true,
    "categoryField": "year",
    "categoryAxis": {
        "gridPosition": "start",
        "fillAlpha": 0.05,
        "position": "left"
    },
    "export": {
        "enabled": true
     }
});

var chart_buah = AmCharts.makeChart("chartdiv_buah", {
    "theme": "light",
    "type": "serial",
    "dataProvider": [<?=$data_buah;?>],
    "valueAxes": [{
        "title": "Produksi per Tahun (Kuintal)"
    }],
    "graphs": [{
        "balloonText": "Produksi in [[category]]:[[value]]",
        "fillAlphas": 1,
        "lineAlpha": 0.2,
        "title": "Income",
        "type": "column",
        "valueField": "income"
    }],
    "depth3D": 0,
    "angle": 0,
    "rotate": true,
    "categoryField": "year",
    "categoryAxis": {
        "gridPosition": "start",
        "fillAlpha": 0.05,
        "position": "left"
    },
    "export": {
        "enabled": true
     }
});
</script>
<script type="text/javascript">
       $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/hortikultura/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/hortikultura/".($th_fn-1));?>';
            window.location.href = prev_url;
        });

</script>
</body>
</html>
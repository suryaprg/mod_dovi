      <ul class="sidebar-menu">
        <li class="header"><i class="fa fa-clipboard"></i>  KATEGORI</li>
       <li <?php if($this->uri->segment(2) == 'pendidikan_baca_tulis') echo "class='active'";?>>
          <a href="<?= base_url()."data/pendidikan_baca_tulis/". date("Y");?>">
              <i class="fa fa-circle"></i>Angka Buta Aksara
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'pendidikan_partisipasi_sklh') echo "class='active'";?>>
          <a href="<?= base_url()."data/pendidikan_partisipasi_sklh/". date("Y");?>">
              <i class="fa fa-circle"></i>Angka Partisipasi Sekolah
          </a>
        </li>
        
        <li <?php if($this->uri->segment(3) == 'pendidikan_penduduk') echo "class='active'";?>>
          <a href="<?= base_url()."data/pendidikan_penduduk/". date("Y");?>">
              <i class="fa fa-circle"></i>Jumlah Lulusan
          </a>
        </li>
    
       <!--  <li <?php if($this->uri->segment(3) == 'pendidikanFormal') echo "class='active'";?>>
          <a href="pendidikanFormal">
            <span>Lembaga Pendidikan Formal</span>
          </a>
        </li> -->
        
         <li class="header"><i class="fa fa-align-justify"></i>   LAMPIRAN</li>

            <li <?php if($this->uri->segment(2) == 'data_pokok') echo "class='active'";?>>
              <a href="<?= base_url()."data/data_pokok/"?>">
                <i class="fa fa-circle-o"></i>Data Pokok Pendidikan
              </a>
            </li>
            <li <?php if($this->uri->segment(2) == 'lp_perpustakaan') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_perpustakaan/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Perpustakaan
              </a>
            </li>
             <li <?php if($this->uri->segment(2) == 'lp_pendidikan_all') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_pendidikan_all/1/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Pendidikan
              </a>
            </li>
              
          </ul>
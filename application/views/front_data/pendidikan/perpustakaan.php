<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Fasilitas Pendidikan Lain</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
    <h2 style="margin-left: 0.5em;">Perpustakaan</h2>
      <center><div>
      <div class="box-body chart-responsive">
        <br>
  <table class="data-table table-responsive" style="position: relative; bottom: 30px; height: 480px;" width="91%">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 20px;">
        <td colspan="7">Data Perpustakaan Umum dan Arsip Daerah Kota Malang, 2014-2016</td>
      </tr>
      <tr style="font-size: 20px;">
        <th><center>Uraian</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_perpustakaan as $perpustakaan)
    {

      $uraian = $perpustakaan->uraian;
      $tahun1 = $perpustakaan->tahun1;
      $tahun2 = $perpustakaan->tahun2;
      $tahun3 = $perpustakaan->tahun3;

      echo '<tr style="font-size: 19px;">
          <td>'.$uraian.'</td>
          <td>'.$tahun1. '</td>
          <td>'.$tahun2. '</td>
          <td>'.$tahun3. '</td>
          </tr>';

    }?> -->
    </tbody>
  </table>
            </div>
        </div></center><br><br>

      </div>
      </div>
    </div> 
  </div>

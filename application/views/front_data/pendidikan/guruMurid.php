  <link rel="stylesheet" href="<?php echo base_url();?>assets/charts/css/style.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Rasio Guru dan Murid</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

    <br>
    <center><div class="box box-solid box-primary" style="width: 930px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Rasio Guru-Murid menurut Jenis Sekolah di Kota Malang Tahun 2014-2016</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div>
                <div id="containering"></div>
              </div>
            </div>
          </div></center>

      </div>
    </div>
  </div>
</div>


<script src="<?php echo base_url();?>assets/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/js/loading.js"></script>
<script src="<?php echo base_url();?>assets/charts6/views4.js"></script>
<script src="<?php echo base_url();?>assets/charts6/views4.min.js"></script>
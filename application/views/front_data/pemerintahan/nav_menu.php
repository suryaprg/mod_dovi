<!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">KATEGORI</li>
        <li <?php if($this->uri->segment(2) == 'index') echo "class='active'";?>>
          <a href="#">
            <span>Kepegawaian</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'rt_rw') echo "class='active'";?>>
          <a href="<?= base_url()."data/rt_rw/". date("Y");?>">
            <span>RT dan RW</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'keuangan_daerah') echo "class='active'";?>>
          <a href="<?= base_url()."data/keuangan_daerah/". date("Y");?>">
            <span>Keuangan Daerah</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'pengeluaran_penduduk') echo "class='active'";?>>
          <a href="<?= base_url()."data/pengeluaran_penduduk/". date("Y");?>">
            <span>Pengeluaran Penduduk</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'keuangan_daerah') echo "class='active'";?>>
          <a href="<?= base_url()."data/pengeluaran_rumah_tangga/". date("Y");?>">
            <span>Pengeluaran Rumah Tangga</span>
          </a>
        </li>
        
        
    </ul>
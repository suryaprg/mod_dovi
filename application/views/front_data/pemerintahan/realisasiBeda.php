<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h1>Keuangan Daerah</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <h2 style="margin-left: 0.5em;">Realisasi Belanja Daerah</h2>
                <br>
                <section>
                    <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Dana alokasi umum yang merupakan bagian dari dana perimbangan menjadi sumber terbesar penerimaan daerah Kota Malang. Tahun 2016 Pemerintah Kota Malang mendapat DAU sebesar Rp. 859.678208 ribu rupiah atau 50,24 persen dari total pendapatan daerah. Hal ini menunjukkan bahwa betapa besar ketergantungan daerah terhadap pemerintah pusat. Pajak dan retribusi daerah yang merupakan pendapatan asli daerah hanya 24,39 persen. Seiring dengan menurunnya pendapatan Pemerintah Kota Malang, jumlah realisasi belanja Pemerintah Kota Malang juga menurun pada tahun 2016 dibanding tahun 2015. Realisasi belanja tahun 2016 sebesar Rp. 1.709.918.083,05 ribu rupiah mengalami penurunan sebesar Rp. 93.502.457,87 ribu rupiah atau 5,18 persen. Berdasarkan strukturnya porsi belanja pegawai Pemerintah Kota Malang merupakan porsi paling besar dibanding belanja modal dan belanja lainnya yang mencapai 58,11 persen dari realisasi belanja Kota Malang. Jumlah belanja pegawai dari tahun 2014-2016 cenderung mengalami kenaikan.
                    </p>
                </section>
                <center>
                    <div>
                        <div class="box-body chart-responsive">

                            <table class="data-table" style="position: relative; bottom: 30px; height: 480px;" width="98%">
                                <caption class="title"></caption>
                                <thead>
                                    <tr style="font-size: 20px;">
                                        <td colspan="7">Realisasi Belanja Daerah Kota Malang Tahun 2014-2016</td>
                                    </tr>
                                    <tr style="font-size: 17px;">
                                        <th>No</th>
                                        <th>
                                            <center>Jenis Pendapatan</center>
                                        </th>
                                        <th>
                                            <center>2014</center>
                                        </th>
                                        <th>
                                            <center>2015</center>
                                        </th>
                                        <th>
                                            <center>2016</center>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <?php

    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    foreach ($data_realisasi_belanja as $realisasi_belanja)
    {

      $tahun1 = $realisasi_belanja->tahun1 == 0 ? '' : number_format($realisasi_belanja->tahun1, 2, ',', '.');
      $tahun2 = $realisasi_belanja->tahun2 == 0 ? '' : number_format($realisasi_belanja->tahun2, 2, ',', '.');
      $tahun3 = $realisasi_belanja->tahun3 == 0 ? '' : number_format($realisasi_belanja->tahun3, 2, ',', '.');
      echo '<tr style="font-size: 16px;">
          <td>'.$realisasi_belanja->no.'</td>
          <td>'.$realisasi_belanja->jenis.'</td>
          <td><center>'.$tahun1. '</center></td>
          <td><center>'.$tahun2. '</center></td>
          <td><center>'.$tahun3. '</center></td>
          </tr>';

      $total1 += $realisasi_belanja->tahun1;
      $total2 += $realisasi_belanja->tahun2;
      $total3 += $realisasi_belanja->tahun3;
    }?> -->
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="2">
                                            <center>Jumlah</center>
                                        </th>
                                        <th>
                                            <?=number_format($total1 * 0.5, 2, ',', '.')?>
                                        </th>
                                        <th>
                                            <?=number_format($total2 * 0.5, 2, ',', '.')?>
                                        </th>
                                        <th>
                                            <?=number_format($total3 * 0.5, 2, ',', '.')?>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </center>
                <br>
                <br>

            </div>
        </div>
    </div>
</div>
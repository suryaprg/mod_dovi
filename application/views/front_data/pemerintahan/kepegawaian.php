  <link rel="stylesheet" href="<?php echo base_url();?>/assets/charts3/view1.css">


 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
     <div class="content body" style="margin-right: 3em; margin-left: 3em;" align="justify">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Kepegawaian</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="height: 4600px">
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Berdasarkan data dari Badan Kepegawaian Daerah Kota Malang jumlah Pegawai Negeri 
Sipil (PNS) dilingkup pemerintah Kota Malang tahun 2016 sebanyak 7.854 pegawai. Jumlah PNS 
tahun 2016 mengalami penurunan sebanyak 1.083 pegawai atau 18,67 persen jika dibandingkan 
tahun 2015. Pada tahun 2016, jumlah PNS laki-laki di Kota Malang masih mendominasi dengan 
perbandingan 4.024 laki-laki sebanyak 4.024 pegawai (51,24 %) dan perempuan sebanyak 3.830 
(48,76 %). Berdasarkan tingkat pendidikan, pada tahun 2016 PNS dengan latar belakang 
pendidikan S1, S2, dan S3 menempati jumlah terbanyak yaitu 4.383 pegawai atau 55,81 persen. 
Jumlah PNS yang lebih dari 50 persen pendidikannya S1, S2 dan S3 pada tahun 2015 dan 2016 
diharapkan pegawai  dapat bekerja lebih efektif dan efisien sehingga dapat meningkatkan kinerja 
pegawai. 
  </p>
</section><br>

<center><div class="box box-solid box-primary" style="width: 620px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Jumlah PNS menurut Pendidikan Terakhir di Kota Malang, 2015-2016</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body chart-responsive">
              <div>
                <br>
                <div id="bar-charts"></div>
              </div>
          </div>
        </div></center><br>

<center>
    <div>
      <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 30px; font-size: 17px;" width="98%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="8">Jumlah Pegawai Negeri Sipil Menurut Dinas/Instansi Pemerintah dan Jenis Kelamin di Kota Malang Tahun 2015-2016
        </td>
      </tr>
      <tr>
        <td rowspan="2">No</td>
        <td rowspan="2">Dinas/Instansi Pemerintahan</td>
        <td colspan="3">2015</td>
        <td colspan="3">2016</td>
      </tr>
      <tr style="font-size: 10px;">
        <th>Laki-laki</th>
        <th>Perempuan</th>
        <th>Jumlah</th>
        <th>Laki-laki</th>
        <th>Perempuan</th>
        <th>Jumlah</th>
      </tr>
    </thead>
    <tbody>
 <!--    <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;

    $tot1 = 0;
    $tot2 = 0;
    foreach ($data_pns_dinas as $pns_dinas)
    {
      $laki1 = $pns_dinas->laki1;
      $laki2 = $pns_dinas->laki2;
      $prmp1 = $pns_dinas->prmp1;
      $prmp2 = $pns_dinas->prmp2;

      $tot1 = $pns_dinas->laki1 + $pns_dinas->prmp1;
      $tot2 = $pns_dinas->laki2 + $pns_dinas->prmp2;
      echo '<tr>
          <td>'.$pns_dinas->no.'</td>
          <td>'.$pns_dinas->dinas.'</td>
          <td><center>'.$laki1. '</center></td>
          <td><center>'.$prmp1. '</center></td>
          <td><center>'.$tot1.'</center></td>
          <td><center>'.$laki2.'</center></td>
          <td><center>'.$prmp2. '</center></td>
          <td><center>'.$tot2.'</center></td>

         
          </tr>';
      $total1 += $pns_dinas->laki1;
      $total3 += $pns_dinas->laki2;

      $total2 += $pns_dinas->prmp1;
      $total4 += $pns_dinas->prmp2;

      $total5 += $tot1;
      $total6 += $tot2;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="2">Jumlah</th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>

        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        
      </tr>
    </tfoot>
  </table>
            </div>
          </div></center>

<center><div>
            <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 30px;" width="98%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="7" style="font-size: 15px;">Jumlah Pegawai Negeri Sipil menurut Pendidikan Tertinggi yang Ditamatkan dan Jenis Kelamin di Kota Malang Tahun 2015-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Pendidikan Terakhir</td>
        <td colspan="2">Laki -laki</td>
        <td colspan="2">Perempuan</td>
        <td colspan="2">Jumlah</td>
      </tr>
      <tr>
        <th>2015</th>
        <th>2016</th>
        <th>2015</th>
        <th>2016</th>
        <th>2015</th>
        <th>2016</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;

    $tot1 = 0;
    $tot2 = 0;
    foreach ($data_pns_pdd as $pns_pdd)
    {
      $laki1 = $pns_pdd->laki1;
      $laki2 = $pns_pdd->laki2;
      $prmp1 = $pns_pdd->prmp1;
      $prmp2 = $pns_pdd->prmp2;

      $tot1 = $pns_pdd->laki1 + $pns_pdd->prmp1;
      $tot2 = $pns_pdd->laki2 + $pns_pdd->prmp2;
      echo '<tr>
          <td>'.$pns_pdd->pendidikan.'</td>
          <td><center>'.$laki1. '</center></td>
          <td><center>'.$laki2.'</center></td>
          <td><center>'.$prmp1. '</center></td>
          <td><center>'.$prmp2.'</center></td>
          <td><center>'.$tot1. '</center></td>
          <td><center>'.$tot2.'</center></td>

         
          </tr>';
      $total1 += $pns_pdd->laki1;
      $total2 += $pns_pdd->laki2;
      $total3 += $pns_pdd->prmp1;
      $total4 += $pns_pdd->prmp2;
      $total5 += $tot1;
      $total6 += $tot2;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1">Jumlah</th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        
      </tr>
    </tfoot>

  </table>
            </div>
          </div>
        </center>

<center><div>
            <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 30px;" width="96.7%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="7" style="font-size: 15px;">Jumlah Pegawai Negeri Sipil menurut Golongan Kepangkatan dan Jenis Kelamin di Kota Malang Tahun 2015-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Golongan Kepangkatan</td>
        <td colspan="2">Laki - laki</td>
        <td colspan="2">Perempuan</td>
        <td colspan="2">Jumlah</td>
      </tr>
      <tr>
        <th>2015</th>
        <th>2016</th>
        <th>2015</th>
        <th>2016</th>
        <th>2015</th>
        <th>2016</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;

    $tot1 = 0;
    $tot2 = 0;
    foreach ($data_pns_gol1 as $pns_gol1)
    {
      $laki1 = $pns_gol1->laki1;
      $laki2 = $pns_gol1->laki2;
      $prmp1 = $pns_gol1->prmp1;
      $prmp2 = $pns_gol1->prmp2;

      $tot1 = $pns_gol1->laki1 + $pns_gol1->prmp1;
      $tot2 = $pns_gol1->laki2 + $pns_gol1->prmp2;
      echo '<tr>
          <td width="306">'.$pns_gol1->golongan.'</td>
          <td width="108"><center>'.$laki1. '</center></td>
          <td width="108"><center>'.$laki2.'</center></td>
          <td width="108"><center>'.$prmp1. '</center></td>
          <td width="109"><center>'.$prmp2.'</center></td>
          <td><center>'.$tot1. '</center></td>
          <td><center>'.$tot2.'</center></td>

         
          </tr>';
      $total1 += $pns_gol1->laki1;
      $total2 += $pns_gol1->laki2;
      $total3 += $pns_gol1->prmp1;
      $total4 += $pns_gol1->prmp2;
      $total5 += $tot1;
      $total6 += $tot2;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1">Golongan I/Range I</th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        
      </tr>
    </tfoot>

  </table>
            </div>
          </div>
        </center>

<center><div style="width: 980px;">
            <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 80px;" width="98.5%">
    <caption class="title"></caption>
  
    <tbody>
<!--     <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;

    $tot1 = 0;
    $tot2 = 0;
    foreach ($data_pns_gol2 as $pns_gol2)
    {
      $laki1 = $pns_gol2->laki1;
      $laki2 = $pns_gol2->laki2;
      $prmp1 = $pns_gol2->prmp1;
      $prmp2 = $pns_gol2->prmp2;

      $tot1 = $pns_gol2->laki1 + $pns_gol2->prmp1;
      $tot2 = $pns_gol2->laki2 + $pns_gol2->prmp2;
      echo '<tr>
          <td width="306">'.$pns_gol2->golongan.'</td>
          <td width="108"><center>'.$laki1. '</center></td>
          <td width="108"><center>'.$laki2.'</center></td>
          <td width="108"><center>'.$prmp1. '</center></td>
          <td width="109"><center>'.$prmp2.'</center></td>
          <td><center>'.$tot1. '</center></td>
          <td><center>'.$tot2.'</center></td>

         
          </tr>';
      $total1 += $pns_gol2->laki1;
      $total2 += $pns_gol2->laki2;
      $total3 += $pns_gol2->prmp1;
      $total4 += $pns_gol2->prmp2;
      $total5 += $tot1;
      $total6 += $tot2;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1">Golongan II/Range II</th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        
      </tr>
    </tfoot>

  </table>
            </div>
          </div>
        </center>

<center><div style="width: 980px;">
            <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 130px;" width="98.5%">
    <caption class="title"></caption>
    
    <tbody>
   <!--  <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;

    $tot1 = 0;
    $tot2 = 0;
    foreach ($data_pns_gol3 as $pns_gol3)
    {
      $laki1 = $pns_gol3->laki1;
      $laki2 = $pns_gol3->laki2;
      $prmp1 = $pns_gol3->prmp1;
      $prmp2 = $pns_gol3->prmp2;

      $tot1 = $pns_gol3->laki1 + $pns_gol3->prmp1;
      $tot2 = $pns_gol3->laki2 + $pns_gol3->prmp2;
      echo '<tr>
          <td width="306">'.$pns_gol3->golongan.'</td>
          <td width="108"><center>'.$laki1. '</center></td>
          <td width="108"><center>'.$laki2.'</center></td>
          <td width="108"><center>'.$prmp1. '</center></td>
          <td width="109"><center>'.$prmp2.'</center></td>
          <td><center>'.$tot1. '</center></td>
          <td><center>'.$tot2.'</center></td>

         
          </tr>';
      $total1 += $pns_gol3->laki1;
      $total2 += $pns_gol3->laki2;
      $total3 += $pns_gol3->prmp1;
      $total4 += $pns_gol3->prmp2;
      $total5 += $tot1;
      $total6 += $tot2;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1">Golongan III/Range III</th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        
      </tr>
    </tfoot>

  </table>
            </div>
          </div>
        </center>

<center><div style="width: 980px;">
            <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 180px;" width="98.5%">
    <caption class="title"></caption>
    
    <tbody>
    <!-- <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;

    $tot1 = 0;
    $tot2 = 0;
    foreach ($data_pns_gol4 as $pns_gol4)
    {
      $laki1 = $pns_gol4->laki1;
      $laki2 = $pns_gol4->laki2;
      $prmp1 = $pns_gol4->prmp1;
      $prmp2 = $pns_gol4->prmp2;

      $tot1 = $pns_gol4->laki1 + $pns_gol4->prmp1;
      $tot2 = $pns_gol4->laki2 + $pns_gol4->prmp2;
      echo '<tr>
          <td width="306">'.$pns_gol4->golongan.'</td>
          <td width="108"><center>'.$laki1. '</center></td>
          <td width="108"><center>'.$laki2.'</center></td>
          <td width="108"><center>'.$prmp1. '</center></td>
          <td width="109"><center>'.$prmp2.'</center></td>
          <td><center>'.$tot1. '</center></td>
          <td><center>'.$tot2.'</center></td>

         
          </tr>';
      $total1 += $pns_gol4->laki1;
      $total2 += $pns_gol4->laki2;
      $total3 += $pns_gol4->prmp1;
      $total4 += $pns_gol4->prmp2;
      $total5 += $tot1;
      $total6 += $tot2;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1">Golongan IV/Range IV</th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        
      </tr>
    </tfoot>

  </table>
            </div>
          </div>
        </center>

      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url();?>/assets/charts3/view1.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Keuangan Daerah</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
    <h2 style="margin-left: 0.5em;">Realisasi Pendapatan Daerah</h2>
    <br>
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Berdasarkan data dari Badan Pengelolaan Keuangan dan Aset Daerah Kota Malang 
realisasi pendapatan Kota Malang tahun 2016 adalah sebanyak 1711.185.350,08 ribu rupiah, 
mengalami penurunan sebesar Rp.93.502.457,87 ribu rupiah atau turun 5,18 persen dibanding 
tahun 2015.  Penurunan pendapatan Kota Malang tahun 2016 disebabkan adanya penurunan 
dari item lain-lain pendapatan yang sah sebesar 283.283.112,14 ribu rupiah atau turun 59,29 
persen khususnya pada dana penyesuaian dan otonomi khusus serta bantuan keuangan dari 
provinsi atau pemerintah daerah lainnya.  
Realisasi pendapatan Kota Malang mengalami penurunan, akan tetapi dari sisi 
Pendapatan Asli Daerah (PAD) Kota Malang justru dari tahun 2014-2016 terus mengalami 
peningkatan, tercatat pada tahun 2016 PAD Kota Malang mengalami kenaikan sebesar 
22.393.900,31 ribu rupiah atau 5,27 persen meskipun kenaikan tersebut terbilang masih lebih 
kecil dibandingkan dengan kenaikan tahun 2015 yang mencapai 14,06 persen 
  </p>
</section>
      <center><div>
      <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 30px; height: 480px;" width="98%">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 20px;">
        <td colspan="7">Realisasi Pendapatan Daerah Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr style="font-size: 17px;">
        <th>No</th>
        <th><center>Jenis Pendapatan</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php
    
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    foreach ($data_realisasi_pendapatan as $realisasi_pendapatan)
    {

      $tahun1 = $realisasi_pendapatan->tahun1 == 0 ? '' : number_format($realisasi_pendapatan->tahun1, 2, ',', '.');
      $tahun2 = $realisasi_pendapatan->tahun2 == 0 ? '' : number_format($realisasi_pendapatan->tahun2, 2, ',', '.');
      $tahun3 = $realisasi_pendapatan->tahun3 == 0 ? '' : number_format($realisasi_pendapatan->tahun3, 2, ',', '.');
      echo '<tr style="font-size: 16px;">
          <td>'.$realisasi_pendapatan->no.'</td>
          <td>'.$realisasi_pendapatan->jenis.'</td>
          <td><center>'.$tahun1. '</center></td>
          <td><center>'.$tahun2. '</center></td>
          <td><center>'.$tahun3. '</center></td>
          </tr>';

      $total1 += $realisasi_pendapatan->tahun1;
      $total2 += $realisasi_pendapatan->tahun2;
      $total3 += $realisasi_pendapatan->tahun3;
    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="2"><center>Jumlah</center></th>
        <th><?=number_format($total1 * 0.5, 2, ',', '.')?></th>
        <th><?=number_format($total2 * 0.5, 2, ',', '.')?></th>
        <th><?=number_format($total3 * 0.5, 2, ',', '.')?></th>
      </tr>
    </tfoot>
  </table>
            </div>
        </div></center><br><br>

      </div>
      </div>
    </div> 
  </div>
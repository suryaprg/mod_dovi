 
 <div class="content-wrapper">
    <section class="content-header">
    </section>
     <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Kelembagaan RT dan RW</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Berdasarkan data dari seluruh Kantor Kecamatan yang ada di Kota Malang,  jumlah RW di 
Kota Malang pada tahun 2016 sebanyak 546, mengalami kenaikan sebanyak 2 (dua) RW atau 
0,37 persen jika dibandingkan dengan tahun 2015 sebanyak 544 RW, sedangkan jumlah RW 
tahun 2015 dibandingkan dengan tahun 2014 adalah tetap atau tidak mengalami perubahan. 
Selaras dengan Jumlah RW, jumlah RT di Kota Malang pada tahun 2016 juga mengalami kenaikan. 
Jumlah RT di Kota Malang pada tahun 2016 sebanyak  4.157, mengalami kenaikan sebanyak 32 
RT atau 0,78 persen jika dibandingkan tahun 2015 sebanyak 4.125 RT. Kenaikan jumlah RT pada 
tahun 2016 tidak sebanyak pada tahun 2015 yang kenaikannya mencapai 0,91 persen.  Jumlah 
RW dan RT paling banyak di Kota Malang terdapat pada Kecamatan Lowokwaru yaitu sebanyak 
127 RW dan 923 RT. Sedangkan jumlah RW dan RT paling sedikit terdapat pada Kecamatan Klojen 
yaitu sebanyak 89 RW dan 675 RT.
  </p>
</section><br>

<center><div class="box box-solid box-primary" style="width: 650px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Jumlah RW dan RT di Kota Malang Tahun 2014-2016</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div>
                <div id="containerer"></div>
                
              </div>
            </div>
        </div></center><br>

<center><div class="box box-solid box-primary" style="width: 650px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Jumlah RW dan RT menurut Kecamatan di Kota Malang Tahun 2014-2016</h3></center>
            </div>
            <div class="box-body chart-responsive">
              <div class="box-header">
                <?php $data_color = array("#5c9bd5", "#ed7e30", "#a5a5a5", "#ffc100", "#4472c5", "#70ad46")?>
                <!-- <i class="fa fa-square" style="color: #5c9bd5"></i> Kedungkandang
                &nbsp&nbsp&nbsp
                <i class="fa fa-square" style="color: #ed7e30"></i> Sukun
                &nbsp&nbsp&nbsp
                <i class="fa fa-square" style="color: #a5a5a5"></i> Klojen
                &nbsp&nbsp&nbsp
                <i class="fa fa-square" style="color: #ffc100"></i> Blimbing
                &nbsp&nbsp&nbsp
                <i class="fa fa-square" style="color: #4472c5"></i> Lowokwaru
                &nbsp&nbsp&nbsp
                <i class="fa fa-square" style="color: #70ad46"></i> Kota Malang -->
              </div>
              <div>
                <div id="containerer1" style="position: relative; right: 160px"></div>
                <div id="containerer2" style="position: absolute; left: 325px; bottom: 10px;"></div>
              </div>
            </div>
        </div></center><br>
       </div>
      </div>
    </div> 
  </div>
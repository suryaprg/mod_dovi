  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Jumlah Pengangguran Terbuka</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section><br>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Indikator utama ketenagakerjaan yang sering digunakan sebagai indikasi keberhasilan 
dalam menangani masalah pengangguran di suatu wilayah adalah jumlah pengangguran terbuka. 
Pengangguran Terbuka (Open Unemployment) adalah tenaga kerja yang benar-benar tidak 
memiliki pekerjaan. Pengganguran jenis yang seperti ini tergolong cukup banyak karena memang 
belum mendapat pekerjaan. Dari data yang disajikan pada gambar di bawah, dapat dilihat bahwa 
jumlah pengangguran terbuka di Kota Malang pada Tahun 2014 Kota Malang mencapai 6.920 
jiwa dan terus mengalami penurunan menjadi 6.257 pada Tahun 2015 serta 6.194 pada tahun 
2016. 
  </p>
</section>  

    <section class="content">
      <div class="row1">
        <div class="col-md-12">
          <center><div class="box box-solid box-primary" style="width: 850px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Jumlah Pengangguran Terbuka di Kota Malang Tahun 2014-2016</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body"><br>
              <div id="contamine" style="width: 810px; height: 400px; margin: 0 auto"></div>
            </div>
          </div></center>
        </div>
      </div>

    </section>
    </div>
  </div>
</div>
</div>
<!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
         <li class="header"><i class="fa fa-clipboard"></i>   KATEGORI</li>
        <li <?php if($this->uri->segment(2) == 'angkatan_kerja') echo "class='active'";?>>
         <a href="<?= base_url()."data/angkatan_kerja/". date("Y");?>">
             <i class="fa fa-circle"></i>Angkatan Kerja
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'pengangguran') echo "class='active'";?>>
          <a href="<?= base_url()."data/pengangguran/". date("Y");?>">
             <i class="fa fa-circle"></i>Tingkat Pengangguran
                     </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'upah_minimum') echo "class='active'";?>>
          <a href="<?= base_url()."data/upah_minimum/". date("Y");?>">
             <i class="fa fa-circle"></i>Upah Minimum Kerja
          </a>
        </li>
         <li class="header"><i class="fa fa-list"></i>   LAMPIRAN</li>
            <li <?php if($this->uri->segment(2) == 'disnaker') echo "class='active'";?>>
          <a href="<?= base_url()."data/disnaker/1/". date("Y");?>">
             <i class="fa fa-circle"></i>Lampiran Data Disnaker
          </a>
        </li>
    </ul>
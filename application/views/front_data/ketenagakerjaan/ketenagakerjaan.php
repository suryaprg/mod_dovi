  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Data Ketenagakerjaan</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="height: 2900px">
      <center><div>
      <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 30px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 20px;">
        <td colspan="7">Data Ketenagakerjaan Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr style="font-size: 16px;">
        <th>No</th>
        <th><center>Jenis Data</center></th>
        <th><center>Satuan</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
      <tr>
        <th style="background-color: #cbdaef; color: black;"><center>I.<br>1.</center></th>
        <th colspan="5" style="background-color: #cbdaef; color: black;">Ketenagakerjaan<br>Penduduk Usia 15 tahun ke atas</th>
      </tr>
    </thead>
    <tbody>
  <!--   <?php

    $tot1 = 0;
    foreach ($data_ketenagakerjaan_bgn1 as $ketenagakerjaan_bgn1)
    {

      echo '<tr style="font-size: 17px;">
          <td width="63"><center>'.$ketenagakerjaan_bgn1->no.'</center></td>
          <td width="358">'.$ketenagakerjaan_bgn1->jenis.'</td>
          <td width="108"><center>'.$ketenagakerjaan_bgn1->satuan.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn1->tahun1.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn1->tahun2.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn1->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table" style="position: relative; bottom: 60px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;"><center>2.</center></th>
        <th colspan="5" style="background-color: #cbdaef; color: black;">Angkatan Kerja<br>a. Bekerja</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_ketenagakerjaan_bgn2 as $ketenagakerjaan_bgn2)
    {

      echo '<tr style="font-size: 17px;">
          <td width="63"><center>'.$ketenagakerjaan_bgn2->no.'</center></td>
          <td width="358">'.$ketenagakerjaan_bgn2->jenis.'</td>
          <td width="108"><center>'.$ketenagakerjaan_bgn2->satuan.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn2->tahun1.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn2->tahun2.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn2->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table" style="position: relative; bottom: 90px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef;">&nbsp</th>
        <th colspan="5" style="background-color: #cbdaef; color: black;">b. Mencari Pekerjaan</th>
      </tr>
    </thead>
    <tbody>
 <!--    <?php

    $tot1 = 0;
    foreach ($data_ketenagakerjaan_bgn3 as $ketenagakerjaan_bgn3)
    {

      echo '<tr style="font-size: 17px;">
          <td width="63"><center>'.$ketenagakerjaan_bgn3->no.'</center></td>
          <td width="358">'.$ketenagakerjaan_bgn3->jenis.'</td>
          <td width="108"><center>'.$ketenagakerjaan_bgn3->satuan.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn3->tahun1.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn3->tahun2.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn3->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table" style="position: relative; bottom: 120px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;"><center>6.</center></th>
        <th colspan="5" style="background-color: #cbdaef; color: black;">Jumlah Pengangguran</th>
      </tr>
    </thead>
    <tbody>
  <!--   <?php

    $tot1 = 0;
    foreach ($data_ketenagakerjaan_bgn4 as $ketenagakerjaan_bgn4)
    {

      echo '<tr style="font-size: 17px;">
          <td width="63"><center>'.$ketenagakerjaan_bgn4->no.'</center></td>
          <td width="358">'.$ketenagakerjaan_bgn4->jenis.'</td>
          <td width="108"><center>'.$ketenagakerjaan_bgn4->satuan.'</center></td>
          <td width="121"><center>'.$ketenagakerjaan_bgn4->tahun1.'</center></td>
          <td width="122"><center>'.$ketenagakerjaan_bgn4->tahun2.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn4->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table" style="position: relative; bottom: 150px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;"><center>7.</center></th>
        <th colspan="5" style="background-color: #cbdaef; color: black;">Tenaga Kerja Dalam Negeri</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_ketenagakerjaan_bgn5 as $ketenagakerjaan_bgn5)
    {

      echo '<tr style="font-size: 17px;">
          <td width="63"><center>'.$ketenagakerjaan_bgn5->no.'</center></td>
          <td width="358">'.$ketenagakerjaan_bgn5->jenis.'</td>
          <td width="107"><center>'.$ketenagakerjaan_bgn5->satuan.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn5->tahun1.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn5->tahun2.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn5->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table" style="position: relative; bottom: 180px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;"><center>8.</center></th>
        <th colspan="5" style="background-color: #cbdaef; color: black;">TKI di Luar Negeri</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_ketenagakerjaan_bgn6 as $ketenagakerjaan_bgn6)
    {

      echo '<tr style="font-size: 17px;">
          <td width="63"><center>'.$ketenagakerjaan_bgn6->no.'</center></td>
          <td width="358">'.$ketenagakerjaan_bgn6->jenis.'</td>
          <td width="107"><center>'.$ketenagakerjaan_bgn6->satuan.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn6->tahun1.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn6->tahun2.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn6->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table" style="position: relative; bottom: 210px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;"><center>9.</center></th>
        <th colspan="5" style="background-color: #cbdaef; color: black;">PHK</th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php

    $tot1 = 0;
    foreach ($data_ketenagakerjaan_bgn7 as $ketenagakerjaan_bgn7)
    {

      $tahun1 = $ketenagakerjaan_bgn7->tahun1 == 0 ? '' : number_format($ketenagakerjaan_bgn7->tahun1, 0, ',', '.');
      $tahun2 = $ketenagakerjaan_bgn7->tahun2 == 0 ? '' : number_format($ketenagakerjaan_bgn7->tahun2, 0, ',', '.');
      $tahun3 = $ketenagakerjaan_bgn7->tahun3 == 0 ? '' : number_format($ketenagakerjaan_bgn7->tahun3, 0, ',', '.');
      echo '<tr style="font-size: 17px;">
          <td width="64"><center>'.$ketenagakerjaan_bgn7->no.'</center></td>
          <td width="400">'.$ketenagakerjaan_bgn7->jenis.'</td>
          <td width="110"><center>'.$ketenagakerjaan_bgn7->satuan.'</center></td>
          <td><center>'.$tahun1.'</center></td>
          <td><center>'.$tahun2.'</center></td>
          <td><center>'.$tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table" style="position: relative; bottom: 240px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;"><center>II.</center></th>
        <th colspan="5" style="background-color: #cbdaef; color: black;">Jumlah Penduduk Bekerja Menurut Lapangan Usaha</th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_ketenagakerjaan_bgn8 as $ketenagakerjaan_bgn8)
    {

      echo '<tr style="font-size: 17px;">
          <td width="63"><center>'.$ketenagakerjaan_bgn8->no.'</center></td>
          <td width="358">'.$ketenagakerjaan_bgn8->jenis.'</td>
          <td width="107"><center>'.$ketenagakerjaan_bgn8->satuan.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn8->tahun1.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn8->tahun2.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn8->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table" style="position: relative; bottom: 270px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      
      <tr>
        <th style="background-color: #cbdaef; color: black;"><center>III.</center></th>
        <th colspan="5" style="background-color: #cbdaef; color: black;">Pelayanan Ketenagakerjaan</th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php

    $tot1 = 0;
    foreach ($data_ketenagakerjaan_bgn9 as $ketenagakerjaan_bgn9)
    {

      echo '<tr style="font-size: 17px;">
          <td width="63"><center>'.$ketenagakerjaan_bgn9->no.'</center></td>
          <td width="358">'.$ketenagakerjaan_bgn9->jenis.'</td>
          <td width="107"><center>'.$ketenagakerjaan_bgn9->satuan.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn9->tahun1.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn9->tahun2.'</center></td>
          <td><center>'.$ketenagakerjaan_bgn9->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>
            </div>
        </div></center>

  <table class="data-table" style="position: relative; bottom: 250px;" width="90%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="7">Jumlah Unit, Tenaga Kerja, dan Nilai Investasi Menurut Bidang Usaha di Kota Malang, 2015-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Bidang Usaha</td>
        <td colspan="2">Jumlah Unit Usaha</td>
        <td colspan="2">Jumlah Tenaga Kerja (Orang)</td>
        <td colspan="2">Modal/Investasi</td>
      </tr>
      <tr>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;

    $tot1 = 0;
    foreach ($data_tenaga_kerja as $tenaga_kerja) 
    {
      $unit1 = $tenaga_kerja->unit1 == 0 ? '' : number_format($tenaga_kerja->unit1, 0, ',', '');
      $unit2 = $tenaga_kerja->unit2 == 0 ? '' : number_format($tenaga_kerja->unit2, 0, ',', '');
      $orang1 = $tenaga_kerja->orang1 == 0 ? '' : number_format($tenaga_kerja->orang1, 0, ',', '');
      $orang2 = $tenaga_kerja->orang2 == 0 ? '' : number_format($tenaga_kerja->orang2, 0, ',', '');
      $modal1 = $tenaga_kerja->modal1 == 0 ? '' : number_format($tenaga_kerja->modal1, 0, ',', '.');
      $modal2 = $tenaga_kerja->modal2 == 0 ? '' : number_format($tenaga_kerja->modal2, 0, ',', '.');

      echo '<tr>
          <td>'.$tenaga_kerja->bidang.'</td>
          <td><center>'.$unit1.'</center></td>
          <td><center>'.$unit2.'</center></td>
          <td><center>'.$orang1.'</center></td>
          <td><center>'.$orang2.'</center></td>
          <td><center>'.$modal1.'</center></td>
          <td><center>'.$modal2.'</center></td>

         
          </tr>';
      $total1 += $tenaga_kerja->unit1;
      $total2 += $tenaga_kerja->unit2;
      $total3 += $tenaga_kerja->orang1;
      $total4 += $tenaga_kerja->orang2;
      $total5 += $tenaga_kerja->modal1;
      $total6 += $tenaga_kerja->modal2;



    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1"><center>Jumlah</center></th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '.')?></th>
        <th><?=number_format($total6, 0, ',', '.')?></th>
        
      </tr>
    </tfoot>
  </table>

      </div>
      </div>
    </div> 
  </div>
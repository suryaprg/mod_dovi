<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Upah Minimum</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section><br>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Upah Minimum adalah upah bulanan terendah yang terdiri dari upah pokok termasuk 
tunjangan tetap, sedangkan Upah Minimum Kabupaten/Kota adalah Upah Minimum yang 
berlaku di Daerah Kabupaten/Kota. Selama 3 (tiga) tahun terakhir (2014-2016), UMK Kota 
Malang terus mengalami kenaikan yang cukup besar. Tahun 2014, UMK Kota Malang sebesar 
1.587.000 rupiah terus mengalami kenaikan menjadi 1.882.500 rupiah pada Tahun 2015 serta 
sebesar 2.099.000 pada Tahun 2016. Kenaikan UMK ini menunjukkan bahwa inflasi Kota Malang 
dalam 3 (tiga) tahun terakhir juga mengalami peningkatan. Tahun 2016, UMK Kota Malang 
menempati urutan terbesar ke-7 di Jawa Timur. 
  </p>
</section>  

    <section class="content">
      <div class="row1">
        <div class="col-md-12">
          <center><div class="box box-solid box-primary" style="width: 850px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Upah Minimum Kota Malang Tahun 2014-2016</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body"><br>
              <div id="contaminer" style="width: 810px; height: 400px; margin: 0 auto"></div>
            </div>
          </div></center>
        </div>
      </div>

    </section>
    </div>
  </div>
</div>
</div>
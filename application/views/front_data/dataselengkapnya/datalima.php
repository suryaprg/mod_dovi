<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Landscaper</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>web_new/img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>web_new/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/fonts/font-awesome/css/font-awesome.css">

<!-- Slider
    ================================================== -->
<link href="<?php echo base_url(); ?>web_new/css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url(); ?>web_new/css/owl.theme.css" rel="stylesheet" media="screen">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/nivo-lightbox/default.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
 
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand page-scroll" href="#about"><img src="<?php echo base_url(); ?>web_new/img/logo.png" alt=" " height="40" width="150"></a> </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url(); ?>web_home/web" class="page-scroll">Data</a></li>
        
        <li><a href="<?php echo base_url(); ?>web_home/web#testimonials" class="page-scroll">Gallery</a></li>
        <li><a href="#portfolio" class="page-scroll">Tentang</a></li>
        <li><a href="<?php echo base_url(); ?>web_home/web/struktur" class="page-scroll">Struktur Organisasi</a></li>
        <!-- <li><a href="#contact" class="page-scroll">Contact</a></li> -->
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>
<!-- Header -->
<header id="header">
  <div class="intro-1">
   
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 intro-text">
            <h1>Data Sektoral Kota Malang Tahun 2018</h1>
            <p><!--  Penjelasan singkat mengenai kantor <br> Dinas Komunikasi dan Informatika Kota Malang --></p>
            <a href="#portfolio" class="btn btn-primary btn-lg page-scroll">ENTER</a> </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- About Section -->

<!-- Gallery Section -->
<div id="portfolio">
  <div class="container">
    <div class="section-title text-center center">
    
      <div class="w3_wthree_agileits_icons main-grid-border">
    <div class="container">
    
         <div class="clearfix"> </div>
      </div>  
          <br>
          <br>
          <br>
     <div class="w3_wthree_agileits_icons main-grid-border">
    <div class="container">
      <div class="grid_3 grid_5 wthree">
        <div class="w3ls-heading">
        <h2 class="h-two">Buku Data Sektoral Kota Malang Tahun 2018</h2>
        <p class="sub two">Kominfo Kota Malang</p>
      </div>
      <br>  
      <center><embed src="<?php echo base_url(); ?>web_new/pdfFiles/2018.pdf" type="application/pdf" width="100%" height="1000px"></center></div>
         <div class="clearfix"> </div>
      </div>  
    
</div>
    
</div>
    </div>
    
</div>
</div>
<div id="footer">
<div class="footer w3ls">
  <div class="container">
    <div class="footer-main">
      <div class="footer-top">
        <div class="col-md-5 ftr-grid fg1">
          <h3><a href="index.html">Petunjuk Arah</a></h3>
          <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.6782028920125!2d112.6409980537108!3d-8.03207598675561!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd627eab4241a03%3A0x6763c56ee92a8ada!2sPerkantoran+Terpadu+Malang!5e0!3m2!1sid!2sid!4v1541818835217" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></p>
        </div>
        <div class="col-md-3 ftr-grid fg2 mid-gd">
          <h3>Hubungi Kami</h3>
          <div class="ftr-address">
            <div class="local">
              <i class="fa fa-map-marker" aria-hidden="true"></i>
            </div>
            <div class="ftr-text">
              <p>Perkantoran Terpadu Pemerintah Kota Malang, Gedung A Lantai 4.</p>
            </div>
            <div class="clearfix"> </div>
          </div>
          <div class="ftr-address">
            <div class="local">
              <i class="fa fa-phone" aria-hidden="true"></i>
            </div>
            <div class="ftr-text">
              <p>(0341)751550</p>
            </div>
            <div class="clearfix"> </div>
          </div>
          <div class="ftr-address">
            
            <div class="ftr-text">
              <div class="local">
              <i class="fa fa-envelope" aria-hidden="true"></i>
            </div> 
              <p><a href="mailto:kominfo.malangkota.go.id">kominfo@malangkota.go.id</a></p>
            </div>
            <br><br>
           <!--  <div class="w3l-social">
                    <ul>
                      <li><a href="https://www.facebook.com/mediacenter.malangkota"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="https://twitter.com/PemkotMalang"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="https://www.instagram.com/pemkotmalang/"><i class="fa fa-instagram"></i></a></li>
                      <li><a href="https://www.youtube.com/user/mediacentermalang"><i class="fa fa-youtube-play"></i></a></li>
                      <li><a href="https://plus.google.com/+PemerintahKotaMalang"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                  </div> -->
            <div class="clearfix"> </div>
          </div>
        </div>
        <div class="col-md-4 ftr-grid fg1">
          <h3>Tentang</h3>
              <p align="justify">Lahir sebagai cita cita Pemerintah Kota Malang untuk menyediakan satu basis data pembangunan yang akurat, terbuka, terpusat dan terintegrasi dan mewujudkan pelayanan publik yang prima</p>
            

          
        </div>
         <div class="clearfix"> </div>
      </div>
      
    </div>
  </div>
</div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/bootstrap.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/SmoothScroll.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/nivo-lightbox.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/jquery.isotope.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/owl.carousel.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/contact_me.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/main.js"></script>
</body>
</html>
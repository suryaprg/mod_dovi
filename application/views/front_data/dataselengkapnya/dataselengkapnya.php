<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Landscaper</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>web_new/img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="<?php echo base_url(); ?>web_newimg/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>web_new/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/fonts/font-awesome/css/font-awesome.css">

<!-- Slider
    ================================================== -->
<link href="<?php echo base_url(); ?>web_new/css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url(); ?>web_new/css/owl.theme.css" rel="stylesheet" media="screen">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/nivo-lightbox/default.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <div class="navbar-brand page-scroll" href="#page-top"><img src="img/logo.png" alt=" " height="40" width="150"></div> </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url(); ?>web_home/web" class="page-scroll">Data</a></li>
        
        <li><a href="<?php echo base_url(); ?>web_home/web#testimonials" class="page-scroll">Gallery</a></li>
        <li><a href="#portfolio" class="page-scroll">Tentang</a></li>
        <li><a href="<?php echo base_url(); ?>web_home/web/struktur" class="page-scroll">Struktur Organisasi</a></li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>
<!-- Header -->
<header id="header">
  <div class="intro-1">
   <!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Landscaper</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>web_new/img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>web_new/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/fonts/font-awesome/css/font-awesome.css">

<!-- Slider
    ================================================== -->
<link href="<?php echo base_url(); ?>web_new/css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url(); ?>web_new/css/owl.theme.css" rel="stylesheet" media="screen">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/nivo-lightbox/default.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
 <script src="<?php echo base_url(); ?>web_new/js/jssor.slider-27.5.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:800,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $SpacingX: 5,
                $SpacingY: 5
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }

        /*jssor slider arrow skin 106 css*/
        .jssora106 {display:block;position:absolute;cursor:pointer;}
        .jssora106 .c {fill:#fff;opacity:.3;}
        .jssora106 .a {fill:none;stroke:#000;stroke-width:350;stroke-miterlimit:10;}
        .jssora106:hover .c {opacity:.5;}
        .jssora106:hover .a {opacity:.8;}
        .jssora106.jssora106dn .c {opacity:.2;}
        .jssora106.jssora106dn .a {opacity:1;}
        .jssora106.jssora106ds {opacity:.3;pointer-events:none;}

        /*jssor slider thumbnail skin 101 css*/
        .jssort101 .p {position: absolute;top:0;left:0;box-sizing:border-box;background:#000;}
        .jssort101 .p .cv {position:relative;top:0;left:0;width:100%;height:100%;border:2px solid #000;box-sizing:border-box;z-index:1;}
        .jssort101 .a {fill:none;stroke:#fff;stroke-width:400;stroke-miterlimit:10;visibility:hidden;}
        .jssort101 .p:hover .cv, .jssort101 .p.pdn .cv {border:none;border-color:transparent;}
        .jssort101 .p:hover{padding:2px;}
        .jssort101 .p:hover .cv {background-color:rgba(0,0,0,6);opacity:.35;}
        .jssort101 .p:hover.pdn{padding:0;}
        .jssort101 .p:hover.pdn .cv {border:2px solid #fff;background:none;opacity:.35;}
        .jssort101 .pav .cv {border-color:#fff;opacity:.35;}
        .jssort101 .pav .a, .jssort101 .p:hover .a {visibility:visible;}
        .jssort101 .t {position:absolute;top:0;left:0;width:100%;height:100%;border:none;opacity:.6;}
        .jssort101 .pav .t, .jssort101 .p:hover .t{opacity:1;}
    </style>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand page-scroll" href="#about"><img src="<?php echo base_url(); ?>web_new/img/logo.png" alt=" " height="40" width="150"></a> </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url(); ?>web_home/web" class="page-scroll">Data</a></li>
        
        <li><a href="<?php echo base_url(); ?>web_home/web#testimonials" class="page-scroll">Gallery</a></li>
        <li><a href="<?php echo base_url(); ?>web_home/web/about" class="page-scroll">Tentang</a></li>
        <li><a href="<?php echo base_url(); ?>web_home/web/struktur" class="page-scroll">Struktur Organisasi</a></li>
        <!-- <li><a href="#contact" class="page-scroll">Contact</a></li> -->
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>
<!-- Header -->
<header id="header">
  <div class="intro-4">
   <!--  <div class="overlay"> -->
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 intro-text">
            <h1>Data Selengkapnya</h1>
            <p>                                                                                   <br></p>
            <a href="#about" class="btn btn-primary btn-lg">ENTER</a> </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- About Section -->
<div id="about">
  <div class="container">
    <div class="row">
      
            <div class="col-xs-12 col-md-3">
        <div class="about-media"><a href="<?=base_url();?>data/dataselengkapnya/datasatu"><img src="<?php echo base_url(); ?>web_new/img/buku_1.png" alt="" height="250" width="250" /></div></a>
        <div class="about-desc">
          <h3 align="center">Buku Statistik Sektoral 2017 Kota Malang</h3>
        </div>
      </div>
      <div class="col-xs-12 col-md-3">
        <div class="about-media"><a href="<?=base_url();?>data/dataselengkapnya/datadua"><img src="<?php echo base_url(); ?>web_new/img/buku_2.png" alt="" height="250" width="250" /></div></a>
        <div class="about-desc">
         <h3 align="center">Buku Profil SDA 2 Kota Malang</h3>
          
        </div>
      </div>

      <div class="col-xs-12 col-md-3">
       <div class="about-media"><a href="<?=base_url();?>data/dataselengkapnya/datatiga"><img src="<?php echo base_url(); ?>web_new/img/buku_3.png" alt="" height="250" width="250" /></div></a>
        <div class="about-desc">
          <h3 align="center">Buku SDM Kota Malang</h3>
          
        </div>
      </div>
      <div class="col-xs-12 col-md-3">
      <div class="about-media"><a href="<?=base_url();?>data/dataselengkapnya/dataempat"><img src="<?php echo base_url(); ?>web_new/img/buku_4.png" alt="" height="250" width="250" /></div></a>
        <div class="about-desc">
          <h3 align="center">Buku Profil Infrastruktur</h3>
         
        </div>
      </div>
    </div>
       <div class="row">
            <div class="col-xs-12 col-md-3">
        <div class="about-media"><a href="<?=base_url();?>data/dataselengkapnya/datalima"><img src="<?php echo base_url(); ?>web_new/img/buku_5.png" alt="" height="250" width="250" /></div></a>
        <div class="about-desc">
          <h3 align="center">Buku Data Sektoral Kota Malang Tahun 2018</h3>
          
        </div>
      </div>
      <div class="col-xs-12 col-md-3">
      <div class="about-media"><a href="<?=base_url();?>data/dataselengkapnya/dataenam"><img src="<?php echo base_url(); ?>web_new/img/buku_6.png" alt="" height="250" width="250" /></div></a>
        <div class="about-desc">
          <h3 align="center">Monitoring dan Evaluasi Data Sektoral Tahun 2018</h3>
        
        </div>
      </div>
      
     <!--  <div class="col-xs-12 col-md-3">
     <div class="about-media"><a href="<?=base_url();?>data/dataselengkapnya/datatujuh"><img src="<?php echo base_url(); ?>web_new/img/portfolio/pendidikan.jpg" alt="" height="250" width="250" /></div></a>
        <div class="about-desc">
          <h3 align="center">Data 7</h3>
          
        </div>
      </div>
      <div class="col-xs-12 col-md-3">
       <div class="about-media"><a href="<?=base_url();?>data/dataselengkapnya/datadelapan"><img src="<?php echo base_url(); ?>web_new/img/portfolio/perhubungan.jpg" alt="" height="250" width="250" /></div></a>
        <div class="about-desc">
          <h3 align="center">Data 8</h3>
          
        </div>
      </div>
    </div> -->
       
      </div>
  
    </div>
  </div>
</div>
<!-- Services Section -->


<!-- Footer Section -->
<div id="footer">
<div class="footer w3ls">
  <div class="container">
    <div class="footer-main">
      <div class="footer-top">
        <div class="col-md-5 ftr-grid fg1">
          <h3><a href="index.html">Petunjuk Arah</a></h3>
          <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.6782028920125!2d112.6409980537108!3d-8.03207598675561!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd627eab4241a03%3A0x6763c56ee92a8ada!2sPerkantoran+Terpadu+Malang!5e0!3m2!1sid!2sid!4v1541818835217" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></p>
        </div>
        <div class="col-md-3 ftr-grid fg2 mid-gd">
          <h3>Hubungi Kami</h3>
          <div class="ftr-address">
            <div class="local">
              <i class="fa fa-map-marker" aria-hidden="true"></i>
            </div>
            <div class="ftr-text">
              <p>Perkantoran Terpadu Pemerintah Kota Malang, Gedung A Lantai 4.</p>
            </div>
            <div class="clearfix"> </div>
          </div>
          <div class="ftr-address">
            <div class="local">
              <i class="fa fa-phone" aria-hidden="true"></i>
            </div>
            <div class="ftr-text">
              <p>(0341)751550</p>
            </div>
            <div class="clearfix"> </div>
          </div>
          <div class="ftr-address">
            
            <div class="ftr-text">
              <div class="local">
              <i class="fa fa-envelope" aria-hidden="true"></i>
            </div> 
              <p><a href="mailto:kominfo.malangkota.go.id">kominfo@malangkota.go.id</a></p>
            </div>
            <br><br>
           <!--  <div class="w3l-social">
                    <ul>
                      <li><a href="https://www.facebook.com/mediacenter.malangkota"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="https://twitter.com/PemkotMalang"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="https://www.instagram.com/pemkotmalang/"><i class="fa fa-instagram"></i></a></li>
                      <li><a href="https://www.youtube.com/user/mediacentermalang"><i class="fa fa-youtube-play"></i></a></li>
                      <li><a href="https://plus.google.com/+PemerintahKotaMalang"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                  </div> -->
            <div class="clearfix"> </div>
          </div>
        </div>
        <div class="col-md-4 ftr-grid fg1">
          <h3>Tentang</h3>
              <p align="justify">Lahir sebagai cita cita Pemerintah Kota Malang untuk menyediakan satu basis data pembangunan yang akurat, terbuka, terpusat dan terintegrasi dan mewujudkan pelayanan publik yang prima</p>
            

          
        </div>
         <div class="clearfix"> </div>
      </div>
      
    </div>
  </div>
</div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/bootstrap.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/SmoothScroll.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/nivo-lightbox.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/jquery.isotope.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/owl.carousel.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/contact_me.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/main.js"></script>
</body>
</html>
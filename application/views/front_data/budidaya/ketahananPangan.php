 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Ketahanan Pangan</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Komoditas Tanaman Pangan yang ditanam pada Kota Malang selama tahun 2014-2016 
adalah tanaman padi sawah, jagung, Kacang Tanah, ubi Kayu, dan ubi jalar dengan komoditas 
yang paling banyak produksinya di Kota Malang adalah tanaman padi sawah. Produksi padi sawah 
di Kota Malang dari tahun 2014 sampai 2016 terus mengalami peningkatan, tercatat pada tahun 
2016 jumlah produksi padi sawah di Kota Malang mencapai 14.285 ton. Kecamatan yang 
mempunyai potensi tanaman padi sawah terbesar adalah Kecamatan Sukun dengan produksi 
padi sawah sebanyak 4.658 ton. Sedangkan pada Kecamatan Klojen sama sekali tidak ada 
tanaman padi sawah. Komoditas tanaman pangan yang memiliki produksi terbesar kedua adalah 
tanaman ubi kayu dengan jumlah produksi pada tahun 2016 mencapai 3.858 ton.
  </p>
</section> 
    <br>
    <center><div class="box box-solid box-primary" style="width: 650px;" align="justify">
            <div class="box-header with-border">
              <center><h3 class="box-title">Produksi Komoditas Tanaman Pangan di Kota Malang Tahun 2014-2016</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div>
                <div id="bar-chart"></div>
              </div>
            </div>
          </div></center><br><br>

<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Tanaman buah-buahan tahunan adalah tanaman sumber vitamin, mineral, dan lain-lain 
yang dikonsumsi dari bagian tanaman berupa buah dan merupakan tanaman tahunan, umumnya 
dapat dikonsumsi tanpa dimasak terlebih dahulu (dikonsumsi segar). Berdasarkan data dari Dinas 
Pertanian dan Ketahanan Pangan Kota Malang, dapat diketahui bahwa pada tahun 2016 hampir 
semua jenis tanaman buah-buahan tahunan di Kota Malang sudah berproduksi kecuali tanaman 
manggis, nenas, jengkol, dan apel. Pada tahun 2016 buah mangga merupakan buah dengan 
produksi yang paling banyak di Kota Malang yaitu mencapai 1.013,70 kuintal. Buah Jeruk 
Siam/Keprok merupakan tanaman buah tahunan dengan produksi terbanyak kedua setelah buah 
mangga di Kota Malang pada tahun 2016 dengan jumlah produksi mencapai 5.429 ton, 
sedangkan buah yang produksinya paling sedikit di Kota Malang adalah buah duku/langsat 
dengan jumlah produksi hanya mencapai 5 kuintal.
  </p>
</section>
  <br>
    <center><div class="box box-solid box-primary" style="width: 930px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Produksi Tanaman Buah dan Sayuran Tahunan di Kota Malang Tahun 2016 (kuintal)</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div>
                <div id="bar-charts"></div>
              </div>
            </div>
          </div></center><br><br>

<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
Tanaman sayuran semusim adalah tanaman sumber vitamin, mineral, dan lain-lain yang 
dikonsumsi dari bagian tanaman yang berupa daun, bunga, buah, dan umbinya yang berumur 
kurang dari satu tahun. Sedangkan tanaman buah-buahan semusim adalah tanaman sumber 
vitamin, mineral, dan lain-lain yang dikonsumsi dari bagian tanaman berupa buah yang berumur 
kurang dari satu tahun. Berdasarkan data dari Dinas Pertanian dan Ketahanan Kota Malang 
diketahui bahwa tanaman sayuran dan buah-buahan semusim pada tahun 2016 di Kota Malang 
yang berproduksi hanya terdapat 9 (sembilan) jenis tanaman sayuran semusim, sedangkan untuk 
buah-buahan semusim tidak ada yang berproduksi. Tanaman sayuran semusim jamur merupakan tanaman dengan produksi yang paling banyak pada tahun 2016 di Kota Malang, dengan jumlah 
produksi mencapai 35.471,2 kuintal. Tanaman sayur semusim tomat merupakan tanaman 
dengan jumlah produksi terbanyak kedua di Kota Malang setelah tanaman jamur dengan 
produksi pada tahun 2016 mencapai 278 kuintal.
  </p>
</section>
  <br>
    <center><div class="box box-solid box-primary" style="width: 950px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Produksi Tanaman Sayuran dan Buah-Buahan Semusim di Kota Malang Tahun 2016 (kuintal)</h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div>
                <div id="bar-charter"></div>
              </div>
            </div>
          </div></center><br>

<table class="data-table table-responsive" style="position: relative; bottom: 30px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 17px;">
        <td colspan="7">Produksi ternak, unggas, telur,dan susu (TON) di Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr style="font-size: 15px;">
        <th><center>Jenis Ternak</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    $tot1 = 0;
    foreach ($data_peternakan_bgn1 as $peternakan_bgn1)
    {

      echo '<tr style="font-size: 15px;">
          <td>'.$peternakan_bgn1->jenis.'</td>
          <td><center>'.$peternakan_bgn1->tahun1.'</center></td>
          <td><center>'.$peternakan_bgn1->tahun2.'</center></td>
          <td><center>'.$peternakan_bgn1->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 60px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <th colspan="7" style="background-color: #cbdaef; color: black;">Daging Unggas</th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php

    $tot1 = 0;
    foreach ($data_peternakan_bgn2 as $peternakan_bgn2)
    {

      echo '<tr style="font-size: 15px;">
          <td width="379">'.$peternakan_bgn2->jenis.'</td>
          <td><center>'.$peternakan_bgn2->tahun1.'</center></td>
          <td><center>'.$peternakan_bgn2->tahun2.'</center></td>
          <td><center>'.$peternakan_bgn2->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 90px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <th colspan="7" style="background-color: #cbdaef; color: black;">Telur</th>
      </tr>
    </thead>
    <tbody>
 <!--    <?php

    $tot1 = 0;
    foreach ($data_peternakan_bgn3 as $peternakan_bgn3)
    {

      echo '<tr style="font-size: 15px;">
          <td width="379">'.$peternakan_bgn3->jenis.'</td>
          <td><center>'.$peternakan_bgn3->tahun1.'</center></td>
          <td><center>'.$peternakan_bgn3->tahun2.'</center></td>
          <td><center>'.$peternakan_bgn3->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

<table class="data-table table-responsive" style="position: relative; bottom: 75px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 17px;">
        <td colspan="7">Produksi Ikan menurut Jenis Ikan (kg) di Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr style="font-size: 15px;">
        <th><center>Jenis Ternak</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
      <tr>
        <th colspan="7" style="background-color: #cbdaef; color: black;">Budidaya Ikan Dalam Kolam</th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php

    $tot1 = 0;
    foreach ($data_perikanan_bgn1 as $perikanan_bgn1)
    {

      echo '<tr style="font-size: 15px;">
          <td>'.$perikanan_bgn1->jenis.'</td>
          <td><center>'.$perikanan_bgn1->tahun1.'</center></td>
          <td><center>'.$perikanan_bgn1->tahun2.'</center></td>
          <td><center>'.$perikanan_bgn1->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 105px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <th colspan="7" style="background-color: #cbdaef; color: black;">Budidaya Ikan Dalam Keramba</th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php

    $tot1 = 0;
    foreach ($data_perikanan_bgn2 as $perikanan_bgn2)
    {

      echo '<tr style="font-size: 15px;">
          <td width="316">'.$perikanan_bgn2->jenis.'</td>
          <td width="190"><center>'.$perikanan_bgn2->tahun1.'</center></td>
          <td width="191"><center>'.$perikanan_bgn2->tahun2.'</center></td>
          <td><center>'.$perikanan_bgn2->tahun3.'</center></td>
          </tr>';

    }?> -->
    </tbody>
  </table>

<table class="data-table table-responsive" style="position: relative; bottom: 90px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="10" style="font-size: 15px;">Luas Panen, Produksi, dan Produktivitas Padi Sawah Menurut Kecamatan di Kota Malang, 2014-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Kecamatan</td>
        <td colspan="3">Luas Panen (Ha)</td>
        <td colspan="3">Produksi (ton)</td>
        <td colspan="3">Produktivitas (Ku/Ha)</td>
      </tr>
      <tr>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;
    $total9 = 0;

    foreach ($data_panen_padi as $panen_padi)
    {
      $panen1 = $panen_padi->panen1;
      $panen2 = $panen_padi->panen2;
      $panen3 = $panen_padi->panen3;
      $produksi1 = $panen_padi->produksi1;
      $produksi2 = $panen_padi->produksi2;
      $produksi3 = $panen_padi->produksi3;
      $produktivitas1 = $panen_padi->produktivitas1;
      $produktivitas2 = $panen_padi->produktivitas2;
      $produktivitas3 = $panen_padi->produktivitas3;

      echo '<tr>
          <td><center>'.$panen_padi->kecamatan.'</center></td>
          <td><center>'.$panen1. '</center></td>
          <td><center>'.$panen2.'</center></td>
          <td><center>'.$panen3.'</center></td>
          <td><center>'.$produksi1. '</center></td>
          <td><center>'.$produksi2.'</center></td>
          <td><center>'.$produksi3.'</center></td>
          <td><center>'.$produktivitas1.'</center></td>
          <td><center>'.$produktivitas2.'</center></td>
          <td><center>'.$produktivitas3.'</center></td>

         
          </tr>';
      $total1 += $panen_padi->panen1;
      $total2 += $panen_padi->panen2;
      $total3 += $panen_padi->panen3;
      $total4 += $panen_padi->produksi1;
      $total5 += $panen_padi->produksi2;
      $total6 += $panen_padi->produksi3;
      $total7 += $panen_padi->produktivitas1;
      $total8 += $panen_padi->produktivitas2;
      $total9 += $panen_padi->produktivitas3;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1"><center>Jumlah</center></th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        <th><?=number_format($total7 / 4, 2, ',', '')?></th>
        <th><?=number_format($total8 / 4, 2, ',', '')?></th>
        <th><?=number_format($total9 / 4, 2, ',', '')?></th>
        
      </tr>
    </tfoot>
  </table>

<table class="data-table table-responsive" style="position: relative; bottom: 70px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="10" style="font-size: 15px;">Luas Panen, Produksi, dan Produktivitas Jagung Menurut Kecamatan di Kota Malang, 2014-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Kecamatan</td>
        <td colspan="3">Luas Panen (Ha)</td>
        <td colspan="3">Produksi (ton)</td>
        <td colspan="3">Produktivitas (Ku/Ha)</td>
      </tr>
      <tr>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;
    $total9 = 0;

    foreach ($data_panen_jagung as $panen_jagung)
    {
      $panen1 = $panen_jagung->panen1;
      $panen2 = $panen_jagung->panen2;
      $panen3 = $panen_jagung->panen3;
      $produksi1 = $panen_jagung->produksi1;
      $produksi2 = $panen_jagung->produksi2;
      $produksi3 = $panen_jagung->produksi3;
      $produktivitas1 = $panen_jagung->produktivitas1;
      $produktivitas2 = $panen_jagung->produktivitas2;
      $produktivitas3 = $panen_jagung->produktivitas3;

      echo '<tr>
          <td><center>'.$panen_jagung->kecamatan.'</center></td>
          <td><center>'.$panen1. '</center></td>
          <td><center>'.$panen2.'</center></td>
          <td><center>'.$panen3.'</center></td>
          <td><center>'.$produksi1. '</center></td>
          <td><center>'.$produksi2.'</center></td>
          <td><center>'.$produksi3.'</center></td>
          <td><center>'.$produktivitas1.'</center></td>
          <td><center>'.$produktivitas2.'</center></td>
          <td><center>'.$produktivitas3.'</center></td>

         
          </tr>';
      $total1 += $panen_jagung->panen1;
      $total2 += $panen_jagung->panen2;
      $total3 += $panen_jagung->panen3;
      $total4 += $panen_jagung->produksi1;
      $total5 += $panen_jagung->produksi2;
      $total6 += $panen_jagung->produksi3;
      $total7 += $panen_jagung->produktivitas1;
      $total8 += $panen_jagung->produktivitas2;
      $total9 += $panen_jagung->produktivitas3;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1"><center>Jumlah</center></th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        <th><?=number_format($total7 / 3, 2, ',', '')?></th>
        <th><?=number_format($total8 / 4, 2, ',', '')?></th>
        <th><?=number_format($total9 / 3, 2, ',', '')?></th>
        
      </tr>
    </tfoot>
  </table>

<table class="data-table table-responsive" style="position: relative; bottom: 50px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="10" style="font-size: 14px;">Luas Panen, Produksi, dan Produktivitas Kacang Tanah Menurut Kecamatan di Kota Malang, 2014-2016
</td>
      </tr>
      <tr>
        <td rowspan="2">Kecamatan</td>
        <td colspan="3">Luas Panen (Ha)</td>
        <td colspan="3">Produksi (ton)</td>
        <td colspan="3">Produktivitas (Ku/Ha)</td>
      </tr>
      <tr>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;
    $total9 = 0;

    foreach ($data_panen_kacang as $panen_kacang)
    {
      $panen1 = $panen_kacang->panen1;
      $panen2 = $panen_kacang->panen2;
      $panen3 = $panen_kacang->panen3;
      $produksi1 = $panen_kacang->produksi1;
      $produksi2 = $panen_kacang->produksi2;
      $produksi3 = $panen_kacang->produksi3;
      $produktivitas1 = $panen_kacang->produktivitas1;
      $produktivitas2 = $panen_kacang->produktivitas2;
      $produktivitas3 = $panen_kacang->produktivitas3;

      echo '<tr>
          <td><center>'.$panen_kacang->kecamatan.'</center></td>
          <td><center>'.$panen1. '</center></td>
          <td><center>'.$panen2.'</center></td>
          <td><center>'.$panen3.'</center></td>
          <td><center>'.$produksi1. '</center></td>
          <td><center>'.$produksi2.'</center></td>
          <td><center>'.$produksi3.'</center></td>
          <td><center>'.$produktivitas1.'</center></td>
          <td><center>'.$produktivitas2.'</center></td>
          <td><center>'.$produktivitas3.'</center></td>

         
          </tr>';
      $total1 += $panen_kacang->panen1;
      $total2 += $panen_kacang->panen2;
      $total3 += $panen_kacang->panen3;
      $total4 += $panen_kacang->produksi1;
      $total5 += $panen_kacang->produksi2;
      $total6 += $panen_kacang->produksi3;
      $total7 += $panen_kacang->produktivitas1;
      $total8 += $panen_kacang->produktivitas2;
      $total9 += $panen_kacang->produktivitas3;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1"><center>Jumlah</center></th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        <th><?=number_format($total7 / 2, 2, ',', '')?></th>
        <th><?=number_format($total8 / 3, 2, ',', '')?></th>
        <th><?=number_format($total9 / 2, 2, ',', '')?></th>
        
      </tr>
    </tfoot>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 30px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="10" style="font-size: 15px;">Luas Panen, Produksi, dan Produktivitas Ubi Kayu Menurut Kecamatan di Kota Malang, 2014-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Kecamatan</td>
        <td colspan="3">Luas Panen (Ha)</td>
        <td colspan="3">Produksi (ton)</td>
        <td colspan="3">Produktivitas (Ku/Ha)</td>
      </tr>
      <tr>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;
    $total9 = 0;

    foreach ($data_panen_ubikayu as $panen_ubikayu)
    {
      $panen1 = $panen_ubikayu->panen1;
      $panen2 = $panen_ubikayu->panen2;
      $panen3 = $panen_ubikayu->panen3;
      $produksi1 = $panen_ubikayu->produksi1;
      $produksi2 = $panen_ubikayu->produksi2;
      $produksi3 = $panen_ubikayu->produksi3;
      $produktivitas1 = $panen_ubikayu->produktivitas1;
      $produktivitas2 = $panen_ubikayu->produktivitas2;
      $produktivitas3 = $panen_ubikayu->produktivitas3;

      echo '<tr>
          <td><center>'.$panen_ubikayu->kecamatan.'</center></td>
          <td><center>'.$panen1. '</center></td>
          <td><center>'.$panen2.'</center></td>
          <td><center>'.$panen3.'</center></td>
          <td><center>'.$produksi1. '</center></td>
          <td><center>'.$produksi2.'</center></td>
          <td><center>'.$produksi3.'</center></td>
          <td><center>'.$produktivitas1.'</center></td>
          <td><center>'.$produktivitas2.'</center></td>
          <td><center>'.$produktivitas3.'</center></td>

         
          </tr>';
      $total1 += $panen_ubikayu->panen1;
      $total2 += $panen_ubikayu->panen2;
      $total3 += $panen_ubikayu->panen3;
      $total4 += $panen_ubikayu->produksi1;
      $total5 += $panen_ubikayu->produksi2;
      $total6 += $panen_ubikayu->produksi3;
      $total7 += $panen_ubikayu->produktivitas1;
      $total8 += $panen_ubikayu->produktivitas2;
      $total9 += $panen_ubikayu->produktivitas3;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1"><center>Jumlah</center></th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        <th><?=number_format($total7 / 3, 2, ',', '')?></th>
        <th><?=number_format($total8 / 3, 2, ',', '')?></th>
        <th><?=number_format($total9 / 3, 2, ',', '')?></th>
        
      </tr>
    </tfoot>
  </table>

  <table class="data-table table-responsive" style="position: relative; bottom: 10px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="10" style="font-size: 15px;">Luas Panen, Produksi, dan Produktivitas Ubi Jalar Menurut Kecamatan di Kota Malang, 2014-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Kecamatan</td>
        <td colspan="3">Luas Panen (Ha)</td>
        <td colspan="3">Produksi (ton)</td>
        <td colspan="3">Produktivitas (Ku/Ha)</td>
      </tr>
      <tr>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;
    $total9 = 0;

    foreach ($data_panen_ubijalar as $panen_ubijalar)
    {
      $panen1 = $panen_ubijalar->panen1;
      $panen2 = $panen_ubijalar->panen2;
      $panen3 = $panen_ubijalar->panen3;
      $produksi1 = $panen_ubijalar->produksi1;
      $produksi2 = $panen_ubijalar->produksi2;
      $produksi3 = $panen_ubijalar->produksi3;
      $produktivitas1 = $panen_ubijalar->produktivitas1;
      $produktivitas2 = $panen_ubijalar->produktivitas2;
      $produktivitas3 = $panen_ubijalar->produktivitas3;

      echo '<tr>
          <td><center>'.$panen_ubijalar->kecamatan.'</center></td>
          <td><center>'.$panen1. '</center></td>
          <td><center>'.$panen2.'</center></td>
          <td><center>'.$panen3.'</center></td>
          <td><center>'.$produksi1. '</center></td>
          <td><center>'.$produksi2.'</center></td>
          <td><center>'.$produksi3.'</center></td>
          <td><center>'.$produktivitas1.'</center></td>
          <td><center>'.$produktivitas2.'</center></td>
          <td><center>'.$produktivitas3.'</center></td>

         
          </tr>';
      $total1 += $panen_ubijalar->panen1;
      $total2 += $panen_ubijalar->panen2;
      $total3 += $panen_ubijalar->panen3;
      $total4 += $panen_ubijalar->produksi1;
      $total5 += $panen_ubijalar->produksi2;
      $total6 += $panen_ubijalar->produksi3;
      $total7 += $panen_ubijalar->produktivitas1;
      $total8 += $panen_ubijalar->produktivitas2;
      $total9 += $panen_ubijalar->produktivitas3;

    }?> -->
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1"><center>Jumlah</center></th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        <th><?=number_format($total7, 2, ',', '')?></th>
        <th><?=number_format($total8, 2, ',', '')?></th>
        <th><?=number_format($total9, 2, ',', '')?></th>
        
      </tr>
    </tfoot>
  </table><br>

  <table class="data-table table-responsive" style="position: relative;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="10" style="font-size: 15px;">Luas Panen, Produksi dan Produktivitas Tanaman Sayuran dan Buah-Buahan Semusim Menurut Jenis Komoditas di Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Komoditas</td>
        <td colspan="3">Luas Panen (Ha)</td>
        <td colspan="3">Produksi (ku)</td>
        <td colspan="3">Produktivitas (Ku/Ha)</td>
      </tr>
      <tr>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php

    foreach ($data_buah_sayur1 as $buah_sayur1)
    {
      $panen1 = $buah_sayur1->panen1;
      $panen2 = $buah_sayur1->panen2;
      $panen3 = $buah_sayur1->panen3;
      $produksi1 = $buah_sayur1->produksi1;
      $produksi2 = $buah_sayur1->produksi2;
      $produksi3 = $buah_sayur1->produksi3;
      $produktivitas1 = $buah_sayur1->produktivitas1;
      $produktivitas2 = $buah_sayur1->produktivitas2;
      $produktivitas3 = $buah_sayur1->produktivitas3;

      echo '<tr>
          <td><center>'.$buah_sayur1->komoditas.'</center></td>
          <td><center>'.$panen1. '</center></td>
          <td><center>'.$panen2.'</center></td>
          <td><center>'.$panen3.'</center></td>
          <td><center>'.$produksi1. '</center></td>
          <td><center>'.$produksi2.'</center></td>
          <td><center>'.$produksi3.'</center></td>
          <td><center>'.$produktivitas1.'</center></td>
          <td><center>'.$produktivitas2.'</center></td>
          <td><center>'.$produktivitas3.'</center></td>

          </tr>';

    }?> -->
    </tbody>
  </table><br><br>

  <table class="data-table table-responsive" style="position: relative; bottom: 10px;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="10" style="font-size: 15px;">Jumlah Tanaman Menghasilkan, Produksi, dan Produktivitas, Tanaman Buah-Buahan dan Sayuran Tahun Dirinci Menurut Jenis Komoditas di Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Komoditas</td>
        <td colspan="3">Tanaman Menghasilkan<br>(pohon/rumpun)</td>
        <td colspan="3">Produksi (ku)</td>
        <td colspan="3">Produktivitas (Ku/Pohon)</td>
      </tr>
      <tr>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
   <!--  <?php

    foreach ($data_buah_sayur2 as $buah_sayur2)
    {
      $penghasil1 = $buah_sayur2->penghasil1;
      $penghasil2 = $buah_sayur2->penghasil2;
      $penghasil3 = $buah_sayur2->penghasil3;
      $produksi1 = $buah_sayur2->produksi1;
      $produksi2 = $buah_sayur2->produksi2;
      $produksi3 = $buah_sayur2->produksi3;
      $produktivitas1 = $buah_sayur2->produktivitas1;
      $produktivitas2 = $buah_sayur2->produktivitas2;
      $produktivitas3 = $buah_sayur2->produktivitas3;

      echo '<tr>
          <td><center>'.$buah_sayur2->komoditas.'</center></td>
          <td><center>'.$penghasil1. '</center></td>
          <td><center>'.$penghasil2.'</center></td>
          <td><center>'.$penghasil3.'</center></td>
          <td><center>'.$produksi1. '</center></td>
          <td><center>'.$produksi2.'</center></td>
          <td><center>'.$produksi3.'</center></td>
          <td><center>'.$produktivitas1.'</center></td>
          <td><center>'.$produktivitas2.'</center></td>
          <td><center>'.$produktivitas3.'</center></td>

          </tr>';

    }?> -->
    </tbody>
  </table><br>

  <table class="data-table table-responsive" style="position: relative;" width="91.6%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="10" style="font-size: 15px;">Jumlah Luas Panen, Produksi, dan Produktivitas, Tanaman Biofarmaka Menurut Jenis Komoditas di Kota Malang Tahun 2014-2016</td>
      </tr>
      <tr>
        <td rowspan="2">Komoditas</td>
        <td colspan="3">Luas Panen (M<sup>2</sup>)</td>
        <td colspan="3">Produksi (Kg)</td>
        <td colspan="3">Produktivitas (M<sup>2</sup>/Kg)</td>
      </tr>
      <tr>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
    <!-- <?php

    foreach ($data_biofarma as $biofarma)
    {
      $panen1 = $biofarma->panen1;
      $panen2 = $biofarma->panen2;
      $panen3 = $biofarma->panen3;
      $produksi1 = $biofarma->produksi1;
      $produksi2 = $biofarma->produksi2;
      $produksi3 = $biofarma->produksi3;
      $produktivitas1 = $buah_sayur1->produktivitas1;
      $produktivitas2 = $buah_sayur1->produktivitas2;
      $produktivitas3 = $buah_sayur1->produktivitas3;

      echo '<tr>
          <td><center>'.$biofarma->komoditas.'</center></td>
          <td><center>'.$panen1. '</center></td>
          <td><center>'.$panen2.'</center></td>
          <td><center>'.$panen3.'</center></td>
          <td><center>'.$produksi1. '</center></td>
          <td><center>'.$produksi2.'</center></td>
          <td><center>'.$produksi3.'</center></td>
          <td><center>'.$produktivitas1.'</center></td>
          <td><center>'.$produktivitas2.'</center></td>
          <td><center>'.$produktivitas3.'</center></td>

          </tr>';

    }?> -->
    </tbody>
  </table><br><br>

      </div>
    </div>
  </div>
</div>

                <ul class="sidebar-menu">
                    <li class="header"><i class="fa fa-clipboard"></i>  KATEGORI </li>
                    <li <?php if($this->uri->segment(2) == 'web_pemerintah') echo "class='active'";?>>
                        <a href="<?php echo base_url()."data/web_pemerintah/". date("Y");?>">
                            <i class="fa fa-circle"></i>Website OPD
                        </a>
                    </li>
                    <li <?php if($this->uri->segment(2) == 'bts') echo "class='active'";?>>
                        <a href="<?php echo base_url()."data/bts/". date("Y");?>">
                            <i class="fa fa-circle"></i>Base Tranceiver Station
                        </a>
                    </li>
                    <li <?php if($this->uri->segment(2) == 'warnet') echo "class='active'";?>>
                        <a href="<?php echo base_url()."data/warnet/". date("Y");?>">
                           <i class="fa fa-circle"></i>Warung Internet
                        </a>
                    </li>
                     <li class="header"><i class="fa fa-align-justify"></i>   LAMPIRAN</li>

           
            <li <?php if($this->uri->segment(2) == 'lp_tehnologi') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_tehnologi/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Teknologi</a>
            </li>
               <li <?php if($this->uri->segment(2) == 'aplikasi') echo "class='active'";?>>
              <a href="<?= base_url()."data/aplikasi/"?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Aplikasi</a>
            </li>
               <li <?php if($this->uri->segment(2) == 'domain') echo "class='active'";?>>
              <a href="<?= base_url()."data/domain/"?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Domain</a>
            </li>
            
    
            <!--         <li <?php if($this->uri->segment(2) == 'dataSI') echo "class='active'";?>>
                        <a href="dataSI">
                            <span>Data Sistem Informasi Pemerintah Kota</span>
                        </a>
                    </li>
                    <li <?php if($this->uri->segment(2) == 'saranaP') echo "class='active'";?>>
                        <a href="saranaP">
                            <span>Sarana Pendukung Komunikasi dan <br>  Informasi</span>
                        </a>
                    </li>
                    <li <?php if($this->uri->segment(2) == 'ok') echo "class='active'";?>>
                        <a href="warnet">
                            <span>Daftar Website SKPD Pemkot Malang (Warnet)</span>
                        </a>
                    </li> -->
                </ul>
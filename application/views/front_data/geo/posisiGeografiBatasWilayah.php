      <link rel="stylesheet" href="<?php echo base_url();?>/assets/template_aw/charts1/views1.css">

 <div class="content-wrapper">
         <!-- Content Header (Page header) -->
         <section class="content-header">
         </section>
         <div class="content body" style="margin-right: 3em; margin-left: 3em;">
            <div class="box box-solid">
               <div class="box-header with-border">
                  <h1>Posisi Geografi dan Batas Wilayah</h1>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <h2 style="margin-left: 0.5em;">Geografi</h2>
                  <br>
                  <div class="row">
                     <div class="col-sm-6">
                        <p style="font-size: 17px;" align="justify">
                           &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                           <b>Kota Malang</b> merupakan salah satu kota tujuan wisata di Provinsi Jawa Timur.
                           Faktor pendukungnya antara lain potensi alam dan iklim.
                           Kota Malang secara geografis terletak berada pada posisi 112.060 - 112.070 Bujur Timur, 7.060 - 8.020 Lintang Selatan. Posisi Kota Malang berada di tengah tengah wilayah Kabupaten Malang, karena batas wilayah utara, timur, Selatan, dan Barat merupakan wilayah Kabupaten Malang. Berdasarkan Surat Keputusan walikota Malang No 146/054/428.41/90 tanggal 9 Januari 1990 diketahui bahwa luas wilayah kota Malang tahun 2016 sebesar yaitu 110,06 km2 yang terbagi dalam 5 (lima) kecamatan. Kecamatan Kedung kandang merupakan kecamatan terbesar dengan luas wilayah 36,24 persen dari wilayah Kota Malang, sedangkan kecamatan paling kecil adalah Kecamatan Klojen dengan luas wilayah hanya 8,02 persen. Luas wilayah Kota Malang tidak mengalami perubahan dari tahun 2014-2016.
                        </p>
                     </div>
                     <div class="col-md-6">
                        <div class="box box-solid box-primary">
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Letak Geografi Kota Malang</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                           <div class="box-body chart-responsive">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126438.28548094159!2d112.56174241362163!3d-7.978639465299347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd62822063dc2fb%3A0x78879446481a4da2!2sMalang%2C+Malang+City%2C+East+Java!5e0!3m2!1sen!2sid!4v1524811126876" width="100%" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>
                           </div>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Presentase Luas Wilayah Kota Malang menurut Kecamatan Tahun 2016</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                           <div class="box-body chart-responsive">
                              <div id="chartdiv_pie" style="width: 100%;max-height: 600px;height: 100vh;"></div>
                           </div>
                           <!-- /.box-body -->

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Jumlah Kelurahan menurut Kecamatan Tahun 2016 di Kota Malang</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                           <div class="box-body chart-responsive">
                              <div id="chartdiv" style="width: 100%;max-height: 600px;height: 100vh;"></div>
                           </div>
                        </div>
                     </div>
                  </div>
                   <h5 align="center">Sumber : Data luas wilayah Kota Malang berdasarkan Surat Keputusan Walikota Kota Malang Nomor
146/054/428.41/90 tanggal 9 Januari 1990</h5>
                  <section class="content">
                     <section class="content">
                        <div class="row">
                           <div class="col-md-6">
                              <div style="display: none;">
                                 <div class="chart" id="revenue-chart" style="height: 300px;"></div>
                              </div>
                              <div class="col-md-6">
                                 <!-- LINE CHART -->
                                 <div style="display: none;">
                                    <div class="chart" id="line-chart" style="height: 300px;"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </section>
               </div>
            </div>
         </div>
      </div>

      <script src="<?php echo base_url();?>/assets/template_aw/charts1/view1.min.js" type="text/javascript"></script>
      <!-- <script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script> -->
      <script>
         var chart;
         var graph;
         var categoryAxis;

         var chartData = 
         [
            <?php
               if($kecamatan){
                  foreach ($kecamatan as $r_kec => $v_kec) {
                     // print_r($v_kec);
                     echo "{
                              \"country\": \"".$v_kec["val_kec"]->nama_kec."\",
                              \"visits\": ".$v_kec["count_kel"].",
                              \"color\": \"#3c8dbc\"
                           },";
                  }
               }
            ?>
           
         ];


         AmCharts.ready(function () {
           chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = "country";
           chart.position = "left";
           chart.angle = 30;
            chart.depth3D = 15;
           chart.startDuration = 1;
           
           categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 0;
           categoryAxis.dashLength = 5; //
           categoryAxis.gridPosition = "start";
           categoryAxis.autoGridCount = false;
            categoryAxis.gridCount = chartData.length;
           
             
            graph = new AmCharts.AmGraph();
            graph.valueField = "visits";
            graph.type = "column";  
           graph.colorField = "color";
            graph.lineAlpha = 0;
           graph.fillAlphas = 0.8;
           graph.balloonText = "[[category]]: <b>[[value]]</b>";
           
           chart.addGraph(graph);
           
           chart.write('chartdiv');
         });



         // Reminder: you need to put https://www.google.com/jsapi in the head of your document or as an external resource on codepen //
      </script>
      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
      <script type="text/javascript">
         // Load google charts
         google.charts.load('current', {'packages':['corechart']});
         google.charts.setOnLoadCallback(drawChart);

         // Draw the chart and set the chart values
         function drawChart() {
           var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            <?php
               if($kecamatan){
                  foreach ($kecamatan as $r_kec => $v_kec) {
                     // print_r($v_kec);
                     echo "['".$v_kec["val_kec"]->nama_kec."',".$v_kec["val_kec"]->luas."],";
                  }
               }
            ?>
           
           // ['Work', 8],
           // ['Eat', 2],
           // ['TV', 4],
           // ['Gym', 2],
           // ['Sleep', 8]
         ]);

           // Optional; add a title and set the width and height of the chart
           var options = {'title':'', 'width':700, 'height':350};

           // Display the chart inside the <div> element with id="piechart"
           var chart = new google.visualization.PieChart(document.getElementById('piechart'));
           chart.draw(data, options);
         }
      </script>

      <script src="<?=base_url();?>assets/chart_main/main_amchart/core.js"></script>
      <script src="<?=base_url();?>assets/chart_main/main_amchart/charts.js"></script>
      <script src="<?=base_url();?>assets/chart_main/main_amchart/animated.js"></script>
      <script type="text/javascript">
        am4core.useTheme(am4themes_animated);

        var chart = am4core.create("chartdiv_pie", am4charts.PieChart);


        chart.data = [
          <?php
               if($kecamatan){
                  foreach ($kecamatan as $r_kec => $v_kec) {
                     // print_r($v_kec);
                     // echo "['".$v_kec["val_kec"]->nama_kec."',".$v_kec["val_kec"]->luas."],";

                     echo "{
                              \"country\": \"".$v_kec["val_kec"]->nama_kec."\",
                              \"litres\": ".$v_kec["val_kec"]->luas."
                          },";
                  }
               }
            ?>
        ];

        var series = chart.series.push(new am4charts.PieSeries());
        series.dataFields.value = "litres";
        series.dataFields.category = "country";

        // this creates initial animation
        series.hiddenState.properties.opacity = 1;
        series.hiddenState.properties.endAngle = -90;
        series.hiddenState.properties.startAngle = -90;

        chart.legend = new am4charts.Legend();
      </script>
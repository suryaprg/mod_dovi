      <link rel="stylesheet" href="<?php echo base_url();?>/assets/template_aw/charts1/views1.css">

 <div class="content-wrapper">
         <!-- Content Header (Page header) -->
         <section class="content-header">
         </section>
         <div class="content body" style="margin-right: 3em; margin-left: 3em;">
            <div class="box box-solid">
               <div class="box-header with-border">
                  <h1 align="center">Kecamatan Kedungkandang</h1>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                
                  <div class="row">
       
                     <div class="col-md-6">
                        <div class="box box-solid box-primary">
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Arjowinangun</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                                    <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/kedungkandang/1_big.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                     </div>
                      <div class="col-md-6">
                        <div class="box box-solid box-primary">
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Buring</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                                 <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/kedungkandang/BURING.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                     </div>
                  </div>
                 <br>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Cemorokandang</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/kedungkandang/CEMOROKANDANG.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Kedungkandang</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                           <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/kedungkandang/KEDUNGKANDANG.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>

                           <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Kotalama</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/kedungkandang/KOTALAMA.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Lesanpuro</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                         <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/kedungkandang/LESANPURO.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>

                           <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Madyopuro</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/kedungkandang/MADYOPURO.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Mergosono</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                            <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/kedungkandang/MERGOSONO.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>
                  
                     <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Sawojajar</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/kedungkandang/SAWOJAJAR.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Telogowaru</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                             <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/kedungkandang/TELOGOWARU.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>


                  <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Wonokoyo</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/kedungkandang/WONOKOYO.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
               
                  </div>
               </div>
            </div>
         </div>
</div>
          
 <script src="<?php echo base_url();?>/assets/template_aw/js/jquery-1.11.3.min.js"></script> 
   <!--  //  <link href="<?php echo base_url();?>/assets/template_aw/css/imageviewer.css"  rel="stylesheet" type="text/css" /> -->
      <script src="<?php echo base_url();?>/assets/template_aw/js/imageviewer.js"></script>
      <script type="text/javascript">
        $(function () {
    var viewer = ImageViewer();
    $('.gallery-items').click(function () {
        var imgSrc = this.src,
            highResolutionImage = $(this).data('high-res-img');
 
        viewer.show(imgSrc, highResolutionImage);
    });
});</script>
   </body>
</html>



      <link rel="stylesheet" href="<?php echo base_url();?>/assets/template_aw/charts1/views1.css">

 <div class="content-wrapper">
         <!-- Content Header (Page header) -->
         <section class="content-header">
         </section>
         <div class="content body" style="margin-right: 3em; margin-left: 3em;">
            <div class="box box-solid">
               <div class="box-header with-border">
                  <h1 align="center">Peta Bencana <br> Tahun 2015</h1>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                
                  <div class="row">
       
                     <div class="col-md-6">
                        <div class="box box-solid box-primary">
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Klojen</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                                    <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/peta/klojen.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                     </div>
                      <div class="col-md-6">
                        <div class="box box-solid box-primary">
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Blimbing</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                                 <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/peta/blimbing.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                     </div>
                  </div>
                 <br>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Lowokwaru</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/peta/lowokwaru.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Sukun</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                           <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/peta/sukun.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>

                           <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kedungkandang</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/peta/kedungkandang.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                    
                  </div>


         
               </div>
            </div>
         </div>
</div>
          
 <script src="<?php echo base_url();?>/assets/template_aw/js/jquery-1.11.3.min.js"></script> 
   <!--  //  <link href="<?php echo base_url();?>/assets/template_aw/css/imageviewer.css"  rel="stylesheet" type="text/css" /> -->
      <script src="<?php echo base_url();?>/assets/template_aw/js/imageviewer.js"></script>
      <script type="text/javascript">
        $(function () {
    var viewer = ImageViewer();
    $('.gallery-items').click(function () {
        var imgSrc = this.src,
            highResolutionImage = $(this).data('high-res-img');
 
        viewer.show(imgSrc, highResolutionImage);
    });
});</script>
   </body>
</html>



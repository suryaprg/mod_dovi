      <link rel="stylesheet" href="<?php echo base_url();?>/assets/template_aw/charts1/views1.css">

 <div class="content-wrapper">
         <!-- Content Header (Page header) -->
         <section class="content-header">
         </section>
         <div class="content body" style="margin-right: 3em; margin-left: 3em;">
            <div class="box box-solid">
               <div class="box-header with-border">
                  <h1 align="center">Kecamatan Sukun</h1>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                
                  <div class="row">
       
                     <div class="col-md-6">
                        <div class="box box-solid box-primary">
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Bakalankrajan</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                                    <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/sukun/BAKALANKRAJAN.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                     </div>
                      <div class="col-md-6">
                        <div class="box box-solid box-primary">
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Bandulan</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                                 <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/sukun/BANDULAN.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                     </div>
                  </div>
                 <br>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Bandungrejosari</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/sukun/BANDUNGREJOSARI.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Ciptomulyo</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                           <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/sukun/CIPTOMULYO.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>

                           <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Gadang</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/sukun/GADANG.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Karangbesuki</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                         <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/sukun/KARANGBESUKI.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>

                           <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Kebonsari</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/sukun/KEBONSARI.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Molyorejo</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                            <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/sukun/MOLYOREJO.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>
                  
                     <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Pisangcandi</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/sukun/Pisangcandi.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Sukun</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                             <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/sukun/SUKUN.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>


                  <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Tanjungrejo</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/sukun/TANJUNGREJO.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
               
                  </div>
               </div>
            </div>
         </div>
</div>
          
 <script src="<?php echo base_url();?>/assets/template_aw/js/jquery-1.11.3.min.js"></script> 
   <!--  //  <link href="<?php echo base_url();?>/assets/template_aw/css/imageviewer.css"  rel="stylesheet" type="text/css" /> -->
      <script src="<?php echo base_url();?>/assets/template_aw/js/imageviewer.js"></script>
      <script type="text/javascript">
        $(function () {
    var viewer = ImageViewer();
    $('.gallery-items').click(function () {
        var imgSrc = this.src,
            highResolutionImage = $(this).data('high-res-img');
 
        viewer.show(imgSrc, highResolutionImage);
    });
});</script>
   </body>
</html>



<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
        <div class="box box-solid">
            <div class="box-header with-border">
                      <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Laporan Pemanfaatan Hutan Kota dan Taman Kota</h1>
                <h2 align="center" ><?php print_r($th_st." - ".$th_fn); ?></h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
            </div>
            <!-- /.box-header -->
     <div class="box-body">
                <center>
                    <div class="box box-solid box-primary" style="width: 100%;">

                       <?php print_r($dropdown_kategori);?>
                    </div>
                </center> 
      
               
                <center>
                    <div>
                        <div class="box-body chart-responsive">
                            <table id="main_out_tbl" class="data-table" style="width: 100%;">
                                <?php print_r($str_tbl);?>
                            </table>
                                 <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/disperkim_all/1/".($th_fn));?>" id="btn_next" target="_blank" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
                        </div>
                    </div> 
                     <div class="col-sm-12"><br><br>
                        <div class="box box-solid box-primary" style="width: 100%;">
                            <div class="box-header with-border">
                                <center>
                                    <h3 class="box-title">Jumlah Bencana Tanah Longsor menurut Kecamatan di Kota Malang Tahun <?= $th_st ." - ". $th_fn; ?></h3></center>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <div class="box-body chart-responsive">
                                <div>
                                    <div id="chartdiv" style="width: 100%; height: 600px"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                </div>   
            </div>        
        </div>
    </div>
</div>





      

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>

<!-- Resources -->
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/core.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/charts.js"></script>
    <script src="<?= base_url();?>assets/chart_main/amchart_multi_line/animated.js"></script>

    <script type="text/javascript">
        var data_main = JSON.parse('<?php print_r("$data_graph");?>');
        var id_kategori_x = "<?php print_r($id_kategori);?>";

        console.log(data_main);
        $(document).ready(function(){
            console.log(data_main);
            $(".out_jenis").hide();
        });

        $(".jenis").click(function(){
            var id_jenis_html = $(this).attr("id");
            var id_jenis = id_jenis_html.split("_")[1];

            if($("#out_jenis_"+id_jenis).is(":visible")){
                $("#out_jenis_"+id_jenis).hide(1000);
            }else{
                $("#out_jenis_"+id_jenis).show(1000);
            }
            
        });

        $(".sub").click(function(){
            var id_jenis_html = $(this).attr("id");
            var id_jenis = id_jenis_html.split("_")[1];
            var sub_jenis = id_jenis_html.split("_")[2];
            // console.log(id_jenis);
            // console.log(sub_jenis);
            // console.log(data_main[id_jenis][sub_jenis]);
            create_graph(data_main[id_jenis][sub_jenis]);

            
        });

        
        $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/disperkim_all/1/".($th_fn+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/disperkim_all/1/".($th_fn-1));?>';
            window.location.href = prev_url;
        });

        $("#get_kate").change(function(){
            var id_kategori = $("#get_kate").val();
            var next_url = '<?php print_r(base_url());?>'+'data/disperkim_all/'+id_kategori+'/'+<?php print_r($th_fn);?>;
            window.location.href= next_url;
        });

        function create_graph(data_graph){  
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add percent sign to all numbers
        chart.numberFormatter.numberFormat = "#";

        // Add data
        chart.data = data_graph;

        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "year";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "Data Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor";
        valueAxis.title.fontWeight = 800;

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = "val";
        series.dataFields.categoryX = "year";
        series.clustered = false;
        series.tooltipText = "Jumlah pada Tahun {categoryX}: [bold]{valueY}[/]";


        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;
        }
    </script>

</body>
</html>
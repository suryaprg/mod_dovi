<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <div class="box box-solid">
            <div class="box-header with-border">
              <h1>Sawah dan Kebun</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
    <h2 style="margin-left: 0.5em;">Sawah</h2>
    <br>
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Sektor pertanian mempunyai peranan yang penting dan strategis dalam pembangunan 
daerah, peranan tersebut antara lain sebagai penyedia lapangan kerja, perolehan nilai tambah 
dan daya saing, dan pemenuhan kebutuhan konsumsi dan bahan baku industri serta optimalisasi 
pengelolaan sumber daya alam secara berkelanjutan. Sektor Pertanian walaupun di Kota Malang 
bukan merupakan sektor utama yang menunjang perekonomian, namun sektor ini penting untuk 
dikembangkan mengingat peranannya yang penting dan strategis. Berdasarkan data dari Dinas 
Pertanian dan Ketahanan Pangan Kota Malang, lahan sawah yang ada di Kota Malang pada tahun 
2016 hanya mencapai 10,38 persen dari luas wilayah Kota Malang, sisanya sebanyak 18,85 
persen merupakan luas lahan pertanian bukan sawah dan sisanya 70,77 persen adalah lahan 
bukan pertanian. 
  </p>
</section> 

<div class="content">
      <div class="row1">
        <div class="col-md-12">
          <div class="box box-solid box-primary" style="max-width: 67%; margin: inherit; left: 12em;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Persentase Luas Lahan menurut Jenis Lahan di Kota Malang Tahun 2016</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body chart-responsive">
              
              <br>
              <div class="charts chart-responsive" id="pieChart" style="max-width: 100%"></div>
              <div style="position: absolute; left: 320px; bottom: 130px; font-size: 20px;">
                <i class="fa fa-square" style="color: #fff100"></i> Lahan Sawah
                <br>
                <i class="fa fa-square" style="color: #2C3E50"></i> Lahan Pertanian Bukan Sawah
                <br>
                <i class="fa fa-square" style="color: #fe4400"></i> Lahan Bukan Petanian
              </div>
            </div>
          </div>
      <br>
      <br>
      <br>
        </div>
      </div>
    </div>
      
<section>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Lahan sawah yang ada di Kota Malang ternyata belum semuanya penggunaaannya untuk 
penanaman padi sawah hal ini dapat dilihat dari Lahan sawah di Kota Malang sebanyak 1.142 Ha  
yang ditanami padi sawah hanya sebanyak 844 Ha atau 73,91 persen. Sedangkan sebanyak 292 
Ha atau 25,57 persen lahan sawah yang ada ditanami tanaman lainnya seperti tanaman palawija 
ataupun tanaman hortikultura. Perlu menjadi perhatian bersama bahwa masih ada lahan 
sebanyak 6 Ha atau 0,53 persen dari luas lahan Kota Malang belum dimanfaatkan sama sekali 
atau tidak ditanami tanaman apapun padahal lahan seluas itu bisa ditanami baik komoditas 
tanaman pangan atau hortikutura sehingga bisa meningkatkan produksi.  
  </p>
</section>
        <br>
          <center><div class="box box-solid box-primary" style="width: 650px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Luas Lahan Berdasarkan Penggunaannya di Kota Malang Tahun 2016</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body chart-responsive">
              <div class="graph_container">
                <canvas id="Chart1"></canvas>
              </div>
            </div>
          </div></center>
          <br>
          <center><div style="height: 380px;">
            <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 30px;" width="86%">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="9">Luas Lahan (hektar) menurut Kecamatan dan Penggunaan Lahan di Kota Malang Tahun 2015 - 2016</td>
      </tr>
      <tr>
        <td rowspan="2">Kecamatan</td>
        <td colspan="2">Sawah</td>
        <td colspan="2">Pertanian <br> Bukan Sawah</td>
        <td colspan="2">Bukan <br> Pertanian</td>
        <td colspan="2">Total Luas <br> Lahan</td>
      </tr>
      <tr>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
      </tr>
    </thead>
    <tbody>
    <?php
    $total1 = 0;
    $total2 = 0;
    $total3 = 0;
    $total4 = 0;
    $total5 = 0;
    $total6 = 0;
    $total7 = 0;
    $total8 = 0;

    $tot1 = 0;
    foreach ($data_sawah as $sawah) {
      
      $sawah1 = $sawah->sawah1;
      $sawah2 = $sawah->sawah2;
      $pbs1 = $sawah->pbs1;
      $pbs2 = $sawah->pbs2;
      $bp1 = $sawah->bp1;
      $bp2 = $sawah->bp2;

      $tot1 = $sawah->sawah1 + $sawah->pbs1 + $sawah->bp1; 
      $tot2 = $sawah->sawah2 + $sawah->pbs2 + $sawah->bp2;
      echo '<tr>
          <td>'.$sawah->kec.'</td>
          <td>'.$sawah1. '</td>
          <td>'.$sawah2.'</td>
          <td>'.$pbs1. '</td>
          <td>'.$pbs2.'</td>
          <td>'.$bp1. '</td>
          <td>'.$bp2.'</td>

          <td><b>'.$tot1.'</b></td>
          <td><b>'.$tot2.'</b></td>
          </tr>';
      $total1 += $sawah->sawah1;
      $total2 += $sawah->sawah2;
      $total3 += $sawah->pbs1;
      $total4 += $sawah->pbs2;
      $total5 += $sawah->bp1;
      $total6 += $sawah->bp2;

      $total7 += $tot1;
      $total8 += $tot2;
    }
    
    ?>
    </tbody>
    <tfoot>
      <tr>
        <th colspan="1">Kota Malang</th>
        <th><?=number_format($total1, 0, ',', '')?></th>
        <th><?=number_format($total2, 0, ',', '')?></th>
        <th><?=number_format($total3, 0, ',', '')?></th>
        <th><?=number_format($total4, 0, ',', '')?></th>
        <th><?=number_format($total5, 0, ',', '')?></th>
        <th><?=number_format($total6, 0, ',', '')?></th>
        <th><?=number_format($total7, 0, ',', '')?></th>
        <th><?=number_format($total8, 0, ',', '')?></th>
        
      </tr>
    </tfoot>
  </table>
            </div></center>

      <h2 style="margin-left: 1em;">Kebun</h2>
    <br>
<section>
  <p style="font-size: 17px; margin-left: 2em; margin-right: 1em;">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
   Perkebunan mempunyai peranan penting dan strategis dalam pembangunan nasional 
maupun daerah, terutama dalam meningkatkan kemakmuran dan kesejahteraan rakyat, 
penyediaan lapangan kerja, perolehan nilai tambah dan daya saing, dan bahan baku industri. 
Beberapa komoditas perkebunan yang dipandang mempunyai nilai penting dan strategis di 
Indonesia adalah karet, kelapa sawit, kelapa, kopi, kakao, teh dan tebu. Dari 7 (tujuh) tanaman 
perkebunan unggulan dan strategis tanaman perkebunan di Indonesia, di Kota Malang hanya 
terdapat 2 (dua) tanaman perkebunan yaitu kelapa dan tebu. Produksi tanaman kelapa dari 
tahun 2014-2016 di Kota Malang cenderung mengalami kenaikan. Tercatat bahwa pada tahun 
2016 produksi kelapa di Kota Malang mencapai 925 ton, mengalami kenaikan sebanyak 118 ton 
Gambar 17. Produksi Tanaman Sayuran dan Buah-Buahan Semusim di Kota Malang Tahun 2016 (kuintal) 
atau 14,62 persen jika dibandingkan tahun 2015. Berbeda dengan tanaman kelapa, produksi 
tanaman tebu di Kota Malang dari tahun 2014-2016 justru cenderung mengalami penurunan. 
Produksi tanaman tebu pada tahun 2016 di Kota Malang sebanyak 53.142 ton, mengalami 
penurunan sebanyak 6.871,28 ton atau 11,45 persen jika dibandingkan dengan tahun 2015. 
  </p>
</section>
        <br>
          <center><div class="box box-solid box-primary" style="width: 650px;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Produksi Tanaman Kelapa dan Tebu di Kota Malang Tahun 2014-2016 (ton)</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body chart-responsive">
              <div class="graph_container">
                <div id="containering"></div>
              </div>
            </div>
          </div></center><br><br>

<table class="data-table" style="position: relative; bottom: 30px; width: 94%;">
    <caption class="title"></caption>
    <thead>
      <tr>
        <td colspan="11">Luas Panen, Produksi, dan Produktivitas, Tanaman Hias Menurut Jenis Komoditas di Kota Malang Tahun 2014-2016
</td>
      </tr>
      <tr>
        <td rowspan="2">Komoditas</td>
        <td colspan="3">Luas Panen (M<sup>2</sup>)</td>
        <td rowspan="2">Satuan <br> Produksi</td>
        <td colspan="3">Produksi (Kg)</td>
        <td colspan="3">Produktivitas (M<sup>2</sup>/Kg)</td>
      </tr>
      <tr>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        <th><center>2014</center></th>
        <th><center>2015</center></th>
        <th><center>2016</center></th>
        
      </tr>
    </thead>
    <tbody>
    <?php

    foreach ($data_kebun as $kebun) {

      echo '<tr>
          <td>'.$kebun->komoditas.'</td>
          <td>'.$kebun->panen1.'</td>
          <td>'.$kebun->panen2.'</td>
          <td>'.$kebun->panen3.'</td>
          <td>'.$kebun->satuan.'</td>
          <td>'.$kebun->produksi1.'</td>
          <td>'.$kebun->produksi2.'</td>
          <td>'.$kebun->produksi3.'</td>
          <td>'.$kebun->produktivitas1.'</td>
          <td>'.$kebun->produktivitas2.'</td>
          <td>'.$kebun->produktivitas3.'</td>

          </tr>';

    }
    
    ?>
    </tbody>
  </table>
          
            </div>
          </div>
        </div>
      </div>

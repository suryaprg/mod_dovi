      <link rel="stylesheet" href="<?php echo base_url();?>/assets/template_aw/charts1/views1.css">

 <div class="content-wrapper">
         <!-- Content Header (Page header) -->
         <section class="content-header">
         </section>
         <div class="content body" style="margin-right: 3em; margin-left: 3em;">
            <div class="box box-solid">
               <div class="box-header with-border">
                  <h1 align="center">Kecamatan Lowokwaru</h1>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                
                  <div class="row">
       
                     <div class="col-md-6">
                        <div class="box box-solid box-primary">
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Dinoyo</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                                    <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/lowokwaru/DINOYO.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                     </div>
                      <div class="col-md-6">
                        <div class="box box-solid box-primary">
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Jatimulyo</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                                 <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/lowokwaru/JATIMULYO.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                     </div>
                  </div>
                 <br>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Ketawanggede</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/lowokwaru/KETAWANGGEDE.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Lowokwaru</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                           <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/lowokwaru/LOWOKWARU.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>

                           <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Merjosari</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/lowokwaru/MERJOSARI.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Mojolangu</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                         <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/lowokwaru/MOJOLANGU.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>

                           <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Sumbersari</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/lowokwaru/SUMBERSARI.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Tanjungsekar</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                            <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/lowokwaru/TANJUNGSEKAR.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>
                  
                     <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Tasikmadu</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/lowokwaru/TASIKMADU.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Tlogomas</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                             <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/lowokwaru/TLOGOMAS.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>


                  <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Tulusrejo</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/lowokwaru/TULUSREJO.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Tunggulwulung</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/lowokwaru/TUNGGULWULUNG.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
               
                  </div>
               </div>
            </div>
         </div>
</div>
          
 <script src="<?php echo base_url();?>/assets/template_aw/js/jquery-1.11.3.min.js"></script> 
   <!--  //  <link href="<?php echo base_url();?>/assets/template_aw/css/imageviewer.css"  rel="stylesheet" type="text/css" /> -->
      <script src="<?php echo base_url();?>/assets/template_aw/js/imageviewer.js"></script>
      <script type="text/javascript">
        $(function () {
    var viewer = ImageViewer();
    $('.gallery-items').click(function () {
        var imgSrc = this.src,
            highResolutionImage = $(this).data('high-res-img');
 
        viewer.show(imgSrc, highResolutionImage);
    });
});</script>
   </body>
</html>



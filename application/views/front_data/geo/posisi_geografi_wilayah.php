 <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
        <div class="box-header with-border">
          <h1>Posisi Geografi dan Batas Wilayah</h1>
        </div>
            <!-- /.box-header -->
        <div class="box-body">
          <h2 style="margin-left: 0.5em;">Geografi</h2><br>
          <div class="row">
            <div class="col-sm-6">
              <p style="font-size: 17px;" align="justify">
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <b>Kota Malang</b> merupakan salah satu kota tujuan wisata di Provinsi Jawa Timur.
                Faktor pendukungnya antara lain potensi alam dan iklim.
                 Kota Malang secara geografis terletak berada pada posisi 112.060 - 112.070 Bujur Timur, 7.060 - 8.020 Lintang Selatan. Posisi Kota Malang berada di tengah tengah wilayah Kabupaten Malang, karena batas wilayah utara, timur, Selatan, dan Barat merupakan wilayah Kabupaten Malang. Berdasarkan Surat Keputusan walikota Malang No 146/054/428.41/90 tanggal 9 Januari 1990 diketahui bahwa luas wilayah kota Malang tahun 2016 sebesar yaitu 110,06 km2 yang terbagi dalam 5 (lima) kecamatan. Kecamatan Kedung kandang merupakan kecamatan terbesar dengan luas wilayah 36,24 persen dari wilayah Kota Malang, sedangkan kecamatan paling kecil adalah Kecamatan Klojen dengan luas wilayah hanya 8,02 persen. Luas wilayah Kota Malang tidak mengalami perubahan dari tahun 2014-2016.
              </p>
            </div>
            <div class="col-md-6">
              <div class="box box-solid box-primary">
                <div class="box-header with-border">
                  <center><h3 class="box-title">Letak Geografi Kota Malang</h3></center>
                  <div class="box-tools pull-right">
                  </div>
                </div>
                <div class="box-body chart-responsive">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126438.28548094159!2d112.56174241362163!3d-7.978639465299347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd62822063dc2fb%3A0x78879446481a4da2!2sMalang%2C+Malang+City%2C+East+Java!5e0!3m2!1sen!2sid!4v1524811126876" width="100%" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                      
              </div>
            </div>
          </div><br>
          <div class="row">
            <div class="col-sm-6">
              <div class="box box-solid box-primary" >
                <div class="box-header with-border">
                  <center><h3 class="box-title">Presentase Luas Wilayah Kota Malang menurut Kecamatan Tahun 2016</h3></center>
                  <div class="box-tools pull-right">
                  </div>
                </div>
                <div class="box-body chart-responsive">
                  <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
                </div>
                    <!-- /.box-body -->
                <div class="box-footer">
                      <?php 
                        $data_col = array("#3c8dbc", "#f56954", "#00a65a", "#f9f934", "#7a2bda");
                      ?>
                      <i class="fa fa-square" style="color: #3c8dbc"></i> Kedungkandang
                      &nbsp&nbsp&nbsp
                      <i class="fa fa-square" style="color: #f56954"></i> Sukun
                      &nbsp&nbsp&nbsp
                      <i class="fa fa-square" style="color: #00a65a"></i> Klojen
                      &nbsp&nbsp&nbsp
                      <i class="fa fa-square" style="color: #f9f934"></i> Blimbing
                      &nbsp&nbsp&nbsp
                      <i class="fa fa-square" style="color: #7a2bda"></i> Lowokwaru
                </div>
              </div>
              
              
            </div>
            <div class="col-sm-6">
              <div class="box box-solid box-primary" >
                <div class="box-header with-border">
                  <center><h3 class="box-title">Jumlah Kelurahan menurut Kecamatan Tahun 2016 di Kota Malang</h3></center>
                  <div class="box-tools pull-right">
                  </div>
                </div> 
                <div class="box-body chart-responsive">
                  <div id="chartdiv" style="height:338px;width:100%;"></div>
                </div>
              </div>
            </div>
          </div>
    
            
          <section class="content">  
            <section class="content">
              <div class="row">
                <div class="col-md-6">
                  <div style="display: none;">
                    <div class="chart" id="revenue-chart" style="height: 300px;"></div>
                  </div>
                  <div class="col-md-6">
                    <!-- LINE CHART -->
                    <div style="display: none;">
                      <div class="chart" id="line-chart" style="height: 300px;"></div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </section>
        </div>
      </div>
    </div>
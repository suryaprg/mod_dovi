<!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
               <li class="header"><i class="fa fa-clipboard"></i>  KATEGORI</li>

               <li <?php if($this->uri->segment(2) == 'geografi_wilayah') echo "class='active'";?>>
                  <a href="<?= base_url()."data/geografi_wilayah";?>">
                     <i class="fa fa-circle"></i> Posisi Geografi dan Batas Wilayah
                  </a>
               </li>

        <li <?php if($this->uri->segment(2) == 'kedungkandang') echo "class='treeview active'";?><?php if($this->uri->segment(2) == 'sukun') echo "class='treeview active'";?><?php if($this->uri->segment(2) == 'klojen') echo "class='treeview active'";?><?php if($this->uri->segment(2) == 'lowokwaru') echo "class='treeview active'";?><?php if($this->uri->segment(2) == 'blimbing') echo "class='treeview active'";?>>
          <a href="#">
             <i class="fa fa-circle"></i> Pembagian Wilayah Admin <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(2) == 'kedungkandang') echo "class='active'";?>><a href="<?=base_url()."data/kedungkandang";?>"><i class="fa fa-circle-o"></i>Kedungkandang</a></li>
            <li <?php if($this->uri->segment(2) == 'sukun') echo "class='active'";?>><a href="<?=base_url()."data/sukun";?>"><i class="fa fa-circle-o"></i>Sukun</a></li>
            <li <?php if($this->uri->segment(2) == 'klojen') echo "class='active'";?>><a href="<?=base_url()."data/klojen";?>"><i class="fa fa-circle-o"></i>Klojen</a></li>
            <li <?php if($this->uri->segment(2) == 'blimbing') echo "class='active'";?>><a href="<?=base_url()."data/blimbing";?>"><i class="fa fa-circle-o"></i>Blimbing</a></li>
            <li <?php if($this->uri->segment(2) == 'lowokwaru') echo "class='active'";?>><a href="<?=base_url()."data/lowokwaru";?>"><i class="fa fa-circle-o"></i>Lowokwaru</a></li>
          </ul>
        </li>

           <li <?php if($this->uri->segment(2) == 'peta_bencana') echo "class='active'";?>>
                  <a href="<?= base_url()."data/peta_bencana/"?>">
                    <i class="fa fa-circle"></i>Peta Bencana
                  </a>
               </li>

           
               <li <?php if($this->uri->segment(2) == 'iklim') echo "class='active'";?>>
                  <a href="<?= base_url()."data/iklim/". date("Y");?>">
                    <i class="fa fa-circle"></i>Iklim
                  </a>
               </li>
       
        <li class="header"><i class="fa fa-align-justify"></i>   LAMPIRAN</li>
           
            <li <?php if($this->uri->segment(2) == 'lp_tpa') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_tpa/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data TPA
              </a>
            </li>
             <li <?php if($this->uri->segment(2) == 'lp_bencana') echo "class='active'";?>>
              <a href="<?= base_url()."data/lp_bencana/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Bencana
              </a>
            </li>
               <li <?php if($this->uri->segment(2) == 'csr') echo "class='active'";?>>
              <a href="<?= base_url()."data/csr/";?>">
                <i class="fa fa-circle-o"></i>Lampiran Data CSR
              </a>
            </li>
               <li <?php if($this->uri->segment(2) == 'psu') echo "class='active'";?>>
              <a href="<?= base_url()."data/psu/";?>">
                <i class="fa fa-circle-o"></i>Lampiran Data PSU
              </a>
            </li>
               <li <?php if($this->uri->segment(2) == 'tanaman') echo "class='active'";?>>
              <a href="<?= base_url()."data/tanaman/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Penanaman Pohon
              </a>
            </li>
               <li <?php if($this->uri->segment(2) == 'taman') echo "class='active'";?>>
              <a href="<?= base_url()."data/taman/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Taman
              </a>
            </li>
               <li <?php if($this->uri->segment(2) == 'disperkim_all') echo "class='active'";?>>
              <a href="<?= base_url()."data/disperkim_all/1/".date("Y");?>">
                <i class="fa fa-circle-o"></i>Lampiran Data Disperkim
              </a>
            </li>
     
           
           
          </ul>

        </li>
            </ul>

         </section>
         <!-- /.sidebar -->
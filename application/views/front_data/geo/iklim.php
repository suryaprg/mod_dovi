  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <div class="box box-solid">
            <div class="box-header with-border">
            <div class="row">
            <div class="col-md-3">
                 <button type="button" id="btn_prev" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Tahun Sebelumnya
          </button>
            </div>
             <div class="col-md-6">
                <h1 align="center">Iklim</h1>
                <h2 align="center" >  <?php print_r($th_st." - ".$th_mulai); ?>    </h2>
            </div>
             <div class="col-md-3">
            <button type="button" id="btn_next" class="btn btn-primary pull-right" style="margin-right: 5px;">
            Tahun Berikutnya <i class="fa fa-arrow-right"></i> 
          </button>
            </div>
                    </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<section><br>
  <p style="font-size: 17px; margin-left: 1em; margin-right: 1em;" align="justify">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    Data iklim di Kota Malang didapat melalui Badan Meteorologi dan Geofisika Stasiun 
    Klimatologi Karangploso. Data yang tersedia untuk Kota Malang hanya jumlah curah hujan dan 
hari hujan, sedangkan untuk data kelembapan, kecepatan angin, dan suhu tidak tersedia. Jumlah 
stasiun pengukur curah hujan dan hari hujan yang ada di Kota Malang sebanyak 3 (tiga) tempat 
yaitu Stasiun Kedungkandang, Ciliwung dan Sukun. Berdasarkan pantauan dari 3 stasiun 
pengukur curah hujan yang ada di Kota Malang, tahun 2016 rata-rata curah hujan per bulan 
tertinggi terjadi pada bulan Februari yaitu sebanyak 582 mm sedangkan rata rata hari hujan 
terbanyak terjadi di bulan Februari sebanyak 25 hari. Pada bulan Agustus dan September rata
rata jumlah hari hujan sedikit hanya sebanyak 3 (tiga) hari dalam satu bulan, dengan jumlah hari hujan rata-rata bulan Agustus sebanyak 67 mm dan September 34 mm bisa diartikan bahwa pada bulan tersebut cuaca panas lebih mendominasi. 
  </p>
</section>  

        <br><center><div class="box box-solid box-primary" style="max-width: 93%;">
            <div class="box-header with-border">
              <center><h3 class="box-title">Rata - rata jumlah hari hujan dan curah hujan di Kota Malang Tahun 2016</h3></center>

              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="chart" id="chartContainer" style="height: 370px; max-width: 100%; margin: 0px auto;"></div>
              <img  height="27px" style="position: absolute; bottom: 10px; left: 10px;" src="<?php echo base_url();?>assets/template_aw/dist/img/mark.png" alt="mark1">
              <img  height="27px" style="position: absolute; bottom: 10px; left: 770px;" src="<?php echo base_url();?>assets/template_aw/dist/img/mark.png" alt="mark2">
            </div>
          </div></center><br>

<center><div>
      <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 30px; height: 480px;" width="91%">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 19px;">
        <td colspan="7">Jumlah Curah Hujan menurut Stasiun Klimatologi di Kota Malang Tahun 2016</td>
      </tr>
      <tr style="font-size: 14px;">
        <th><center>Bulan</center></th>
        <th><center>Stasiun <br> Ciliwung</center></th>
        <th><center>Stasiun <br> Kedungkandang</center></th>
        <th><center>Stasiun <br> Sukun</center></th>
      </tr>
    </thead>
    <tbody>
    <?php
      if($iklim){

        $count_st = count($st);
        $bln = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        $str_json_cura = "";

        for($periode = 1; $periode <= 12; $periode++){
          echo "<tr>";
            echo "<td>".$bln[$periode]."</td>";

            $t_cura = 0;
            foreach ($st as $r_st => $v_st) {
              if(!empty($iklim[$r_st]["cura_hujan"][$periode])){
                echo "<td>".$iklim[$r_st]["cura_hujan"][$periode]["cura"]."</td>";
                $t_cura += $iklim[$r_st]["cura_hujan"][$periode]["cura"];
                
              }else{
                echo "<td>-</td>";
                $t_cura += 0;
                // $str_json_cura .= "{x: new Date(".$th.", ".($periode-1).", ), y: 0 }, ";
              }
            }

            
            if($t_cura != 0){
              $str_json_cura .= "{x: new Date(".$th.", ".($periode-1).", ), y: ".($t_cura/$count_st)." }, ";
            }else {
              $str_json_cura .= "{x: new Date(".$th.", ".($periode-1).", ), y: 0 }, ";
            }
          echo "</tr>";
        }
      }
      ?>
    </tbody>
  </table>
    <br>
                        <center>
                            <a href="<?php print_r(base_url()."pdf/iklim/".($th));?>" id="btn_next" target="_blank" class="btn btn-success btn-lg" style="margin-right: 5px;">
            Cetak PDF <i class="fa fa-print"></i></a>
          </button>
          </center>
            </div>
        </div>
        </center>

<center><div>
      <div class="box-body chart-responsive">

  <table class="data-table" style="position: relative; bottom: 30px; height: 480px;" width="91%">
    <caption class="title"></caption>
    <thead>
      <tr style="font-size: 19px;">
        <td colspan="7">Jumlah Hari Hujan menurut Stasiun Klimatologi di Kota Malang Tahun 2016</td>
      </tr>
      <tr style="font-size: 14px;">
        <th><center>Bulan</center></th>
        <th><center>Stasiun <br> Ciliwung</center></th>
        <th><center>Stasiun <br> Kedungkandang</center></th>
        <th><center>Stasiun <br> Sukun</center></th>
      </tr>
    </thead>
    <tbody>
      <?php
      if($iklim){
        $count_st = count($st);

        $str_json_hari = "";

        $bln = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        for($periode = 1; $periode <= 12; $periode++){
          echo "<tr>";
            echo "<td>".$bln[$periode]."</td>";

            $t_hari = 0;
            foreach ($st as $r_st => $v_st) {
              if(!empty($iklim[$r_st]["cura_hujan"][$periode])){
                echo "<td>".$iklim[$r_st]["cura_hujan"][$periode]["hari"]."</td>";
                $t_hari += $iklim[$r_st]["cura_hujan"][$periode]["hari"];
                
              }else{
                echo "<td>-</td>";
                $t_hari += 0;
                // $str_json_hari .= "{x: new Date(".$th.", ".($periode-1).", ), y: 0 }, ";
              }
            }

            if($t_hari != 0){
              $str_json_hari .= "{x: new Date(".$th.", ".($periode-1).", ), y: ".($t_hari/$count_st)." }, ";
            }else {
              $str_json_hari .= "{x: new Date(".$th.", ".($periode-1).", ), y: 0 }, ";
            }
          echo "</tr>";
        }
      }
      ?>
   
    </tbody>
  </table>
            </div>
        </div>
      <h5 align="center">Sumber: Badan Meteorologi dan Geofisika Stasiun Klimatologi Karangploso</h5></center>


    </div>
  </div>
</div>
</div>


<!-- <script src="<?php echo base_url();?>assets/template_aw/charts1/view3.min.js"></script> -->
<script src="<?php echo base_url();?>assets/template_aw/charts1/view3.js"></script>
<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url();?>assets/template_aw/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template_aw/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template_aw/dist/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/js/loading.js"></script>
<script src="<?php echo base_url();?>assets/template_aw/charts5/view1.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/template_aw/charts5/view1.js"></script> -->
<script type="text/javascript">


  window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {

  title:{
    text: ""
  },
  axisX:{
    valueFormatString: "MMMM",
    crosshair: {
      enabled: true,
      snapToDataPoint: true
    }
  },
  axisY: {
    title: "",
    crosshair: {
      enabled: true
    }
  },
  toolTip:{
    shared:true
  },  
  
  data: [{
    type: "line",
    showInLegend: true,
    name: "Rata - rata Jumlah Curah Hujan",
    markerType: "square",
    xValueFormatString: "MMMM",
    color: "#F79124",
    dataPoints: [<?= $str_json_cura; ?>]
  },
  {
    type: "line",
    showInLegend: true,
    name: "Rata - rata Jumlah Hari Hujan",
    lineDashType: "dash",
    color: "#5e95ed",
    dataPoints: [<?= $str_json_hari; ?>]
  }]
});
chart.render();

function toogleDataSeries(e){
  if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  } else{
    e.dataSeries.visible = true;
  }
  chart.render();
}

}


</script>
<script type="text/javascript">
      $("#btn_next").click(function(){
            var next_url = '<?php print_r(base_url()."data/iklim/".($th_mulai+1));?>';
            window.location.href= next_url;
        });

        $("#btn_prev").click(function(){
            var prev_url = '<?php print_r(base_url()."data/iklim/".($th_mulai-1));?>';
            window.location.href = prev_url;
        });
</script>
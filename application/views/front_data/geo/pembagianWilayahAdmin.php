<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h1>Pembagian Wilayah Administrasi</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="height: 1070px">
                <br>

                <table class="data-table table-responsive" style="position: relative; bottom: 30px;" width="91.6%">
                    <caption class="title"></caption>
                    <thead>
                        <tr>
                            <td colspan="7" style="font-size: 17px;">Pembagian Wilayah Administrasi</td>
                        </tr>
                        <tr>
                            <td>Kecamatan</td>
                            <td>Kelurahan</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                          if($kecamatan){
                            foreach ($kecamatan as $r_kecamatan => $v_kecamatan) {
                              $data_kel = $v_kecamatan["val_kel"];
                              foreach ($data_kel as $r_data_kel => $v_data_kel) {
                                  if($r_data_kel == 0){
                                    echo "<tr>
                                            <td>".$v_data_kel->nama_kec."</td>
                                            <td>".$v_data_kel->nama_kel."</td>
                                          </tr>
                                        ";
                                  }else {
                                    echo "<tr>
                                            <td>&nbsp;</td>
                                            <td>".$v_data_kel->nama_kel."</td>
                                          </tr>
                                        ";
                                  }
                              }            
                            }
                          }
                        ?>

                    </tbody>

                </table>

            </div>
        </div>
    </div>
</div>
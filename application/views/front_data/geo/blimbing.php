      <link rel="stylesheet" href="<?php echo base_url();?>/assets/template_aw/charts1/views1.css">

 <div class="content-wrapper">
         <!-- Content Header (Page header) -->
         <section class="content-header">
         </section>
         <div class="content body" style="margin-right: 3em; margin-left: 3em;">
            <div class="box box-solid">
               <div class="box-header with-border">
                  <h1 align="center">Kecamatan Blimbing</h1>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                
                  <div class="row">
       
                     <div class="col-md-6">
                        <div class="box box-solid box-primary">
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Arjosari</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                                    <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/blimbing/ARJOSARI.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                     </div>
                      <div class="col-md-6">
                        <div class="box box-solid box-primary">
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Buring</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                                 <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/blimbing/BALEARJOSARI.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                     </div>
                  </div>
                 <br>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Blimbing</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/blimbing/BLIMBING.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Bunulrejo</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                           <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/blimbing/BUNULREJO.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>

                           <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Jodipan</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/blimbing/JODIPAN.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Kesatrian</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                         <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/blimbing/KESATRIAN.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>

                           <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Pandanwangi</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/blimbing/PANDANWANGI.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Polehan</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                            <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/blimbing/POLEHAN.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>
                  
                     <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Polowijen</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/blimbing/POLOWIJEN.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Purwantoro</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>

                             <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/blimbing/PURWANTORO.jpg" style="width: 300px" class="gallery-items">
</div>
                        </div>
                     </div>
                  </div>


                  <div class="row">
                     <div class="col-sm-6">
                        <div class="box box-solid box-primary" >
                           <div class="box-header with-border">
                              <center>
                                 <h3 class="box-title">Kelurahan Purwodadi</h3>
                              </center>
                              <div class="box-tools pull-right">
                              </div>
                           </div>
                       
                <div align="center">
    <img src="<?php echo base_url();?>/assets/template_aw/images/blimbing/PURWODADI.jpg" style="width: 300px" class="gallery-items">
</div>

                        </div>
                       
                     </div>
               
                  </div>
               </div>
            </div>
         </div>
</div>
          
 <script src="<?php echo base_url();?>/assets/template_aw/js/jquery-1.11.3.min.js"></script> 
   <!--  //  <link href="<?php echo base_url();?>/assets/template_aw/css/imageviewer.css"  rel="stylesheet" type="text/css" /> -->
      <script src="<?php echo base_url();?>/assets/template_aw/js/imageviewer.js"></script>
      <script type="text/javascript">
        $(function () {
    var viewer = ImageViewer();
    $('.gallery-items').click(function () {
        var imgSrc = this.src,
            highResolutionImage = $(this).data('high-res-img');
 
        viewer.show(imgSrc, highResolutionImage);
    });
});</script>
   </body>
</html>



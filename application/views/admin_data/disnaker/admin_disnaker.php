	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Disnaker</h3>
						</div>

						<div class="box-body">							
							<table class="table table-bordered table-striped display" id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 3%">Jenis</th>
										<th class="text-center" style="width: 3%">Sub Jenis</th>
										<th class="text-center" style="width: 1%">Tahun</th>
										<th class="text-center" style="width: 1%">Jumlah</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/admin_opd/disnaker/Admindisnaker/insert_disnaker/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">-- Pilih Jenis --</option>
													<?php foreach ($list_disnaker_jenis as $ltj) { ?>
														<option value="<?php echo $ltj->id_jenis ?>"><?php echo $ltj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td>
												<select name="id_sub_jenis" class="form-control" id="id_sub_jenis">
													<option value="">-- Pilih Sub Jenis --</option>
													<?php foreach ($list_disnaker_sub_jenis as $lts) { ?>
														<option value="<?php echo $lts->id_sub_jenis ?>"><?php echo $lts->nama_sub_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											<td><input type="number" name="th" class="form-control" id="th" placeholder="Tahun"></td>
											<td><input type="number" name="jml" class="form-control" id="jml" placeholder="Jumlah"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_disnaker as $lt) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lt->nama_jenis ?></td>
											<td align="left"><?php echo $lt->nama_sub_jenis ?></td>
											<td align="center"><?php echo $lt->th?></td>
											<td align="center"><?php echo $lt->jml ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_terminal(<?php echo $lt->id_main ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_terminal(<?php echo $lt->id_main ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-7">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Sub Jenis Disnaker</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped display" id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 4%">Jenis</th>
										<th class="text-center" style="width: 4%">Nama</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/admin_opd/disnaker/Admindisnaker/insert_disnaker_sub_jenis/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">-- Pilih Jenis --</option>
													<?php foreach ($list_disnaker_jenis as $ltj) { ?>
														<option value="<?php echo $ltj->id_jenis ?>"><?php echo $ltj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>											
											<td><input type="text" name="nama_sub_jenis" class="form-control" id="nama_sub_jenis" placeholder="Nama Sub Jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_disnaker_sub_jenis as $ltsj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $ltsj->nama_jenis ?></td>
											<td align="left"><?php echo $ltsj->nama_sub_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_terminal_sub(<?php echo $ltsj->id_sub_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_terminal_sub(<?php echo $ltsj->id_sub_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
				<div class="col-md-5">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Jenis Disnaker</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped " id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 5%">Nama</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/admin_opd/disnaker/Admindisnaker/insert_disnaker_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis" placeholder="Nama Jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"> Tambah</span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_disnaker_jenis as $ltj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $ltj->nama_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_terminal_jenis(<?php echo $ltj->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_terminal_jenis(<?php echo $ltj->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

		</section>
	</div>

	<script type="text/javascript">
//======================================================== LP TERMINAL ===========================================//
	  	function up_lp_terminal(id_main){
	    	clear_mod_up_lp_terminal();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_main', id_main);    
		      $.ajax({
		        url: "<?php echo base_url()."/admin_opd/disnaker/Admindisnaker/index_up_disnaker/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_terminal(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_terminal").modal('show');
	  	} 

	  	function clear_mod_up_lp_terminal(){
	      	$("#_id_jenis").val("");
	      	$("#_id_sub_jenis").val("");
	      	$("#_th").val("");
	      	$("#_jml").val("");
	  	}

	  	function res_update_lp_terminal(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_main").attr('readonly','readonly');                                        
	          	$("#_id_main").val(main_data.id_main);
	          	$("#_id_jenis").val(main_data.id_jenis);
	          	$("#_id_sub_jenis").val(main_data.id_sub_jenis);
	          	$("#_th").val(main_data.th);
	          	$("#_jml").val(main_data.jml);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_terminal(id_main){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_main+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_main+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."admin_opd/disnaker/Admindisnaker/delete_disnaker/";?>"+id_main;
	      	}else{

	      	}
	  	}      		
//======================================================== LP TERMINAL ===========================================//

//======================================================== LP TERMINAL JENIS =====================================//
	  	function up_lp_terminal_jenis(id_jenis){
	    	clear_mod_up_lp_terminal_jenis();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_jenis', id_jenis);    
		      $.ajax({
		        url: "<?php echo base_url()."/admin_opd/disnaker/Admindisnaker/index_up_disnaker_jenis/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_terminal_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_terminal_jenis").modal('show');
	  	} 

	  	function clear_mod_up_lp_terminal_jenis(){
	      	$("#_nama_jenis").val("");
	  	}

	  	function res_update_lp_terminal_jenis(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_jenis2").attr('readonly','readonly');                                        
	          	$("#_id_jenis2").val(main_data.id_jenis);
	          	$("#_nama_jenis").val(main_data.nama_jenis);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_terminal_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."admin_opd/disnaker/Admindisnaker/delete_disnaker_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}      		
//======================================================== LP TERMINAL JENIS =====================================//

//======================================================== LP TERMINAL SUB JENIS =================================//
	  	function up_lp_terminal_sub(id_sub_jenis){
	    	clear_mod_up_lp_terminal_sub();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_sub_jenis', id_sub_jenis);    
		      $.ajax({
		        url: "<?php echo base_url()."/admin_opd/disnaker/Admindisnaker/index_up_disnaker_sub_jenis/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_terminal_sub(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_terminal_sub").modal('show');
	  	} 

	  	function clear_mod_up_lp_terminal_sub(){
	      	$("#_id_jenis3").val("");
	      	$("#_nama_sub_jenis").val("");
	  	}

	  	function res_update_lp_terminal_sub(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_sub_jenis2").attr('readonly','readonly');                                        
	          	$("#_id_sub_jenis2").val(main_data.id_sub_jenis);
	          	$("#_id_jenis3").val(main_data.id_jenis);
	          	$("#_nama_sub_jenis").val(main_data.nama_sub_jenis);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_terminal_sub(id_sub_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_sub_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_sub_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."front_lp/lpterminal/delete_lp_terminal_sub_jenis/";?>"+id_sub_jenis;
	      	}else{

	      	}
	  	}      		
//======================================================== LP TERMINAL SUB JENIS =================================//		
	</script>

	<div class="modal fade" id="modal_up_lp_terminal">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data LP Terminal</h4>
	      	</div>
	      	<?php echo form_open("admin_opd/disnaker/Admindisnaker/update_disnaker"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID Terminal</label>
	                        	<input type="number" name="id_main" id="_id_main" class="form-control">
	                      	</div>                      
	                      
	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jenis</label>
								<select name="id_jenis" class="form-control" id="_id_jenis">
									<option value="">-- Pilih Jenis --</option>
									<?php foreach ($list_disnaker_jenis as $ltj) { ?>
										<option value="<?php echo $ltj->id_jenis ?>"><?php echo $ltj->nama_jenis ?></option>
									<?php } ?>
								</select>
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Sub Jenis</label>
								<select name="id_sub_jenis" class="form-control" id="_id_sub_jenis">
									<option value="">-- Pilih Sub Jenis --</option>
									<?php foreach ($list_disnaker_sub_jenis as $lts) { ?>
										<option value="<?php echo $lts->id_sub_jenis ?>"><?php echo $lts->nama_sub_jenis ?></option>
									<?php } ?>
								</select>	                      	
	                      	</div>	                      	


	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Tahun</label>
	                        	<input type="number" name="th" class="form-control" id="_th" placeholder="Tahun">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jumlah</label>
	                        	<input type="text" name="jml" class="form-control" id="_jml" placeholder="Jumlah">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>

	<div class="modal fade" id="modal_up_lp_terminal_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data LP Jenis Terminal</h4>
	      	</div>
	      	<?php echo form_open("admin_opd/disnaker/Admindisnaker/update_disnaker_jenis"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID Terminal</label>
	                        	<input type="number" name="id_jenis" id="_id_jenis2" class="form-control">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Nama</label>
	                        	<input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Nama">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>

	<div class="modal fade" id="modal_up_lp_terminal_sub">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data LP Sub Jenis Terminal</h4>
	      	</div>
	      	<?php echo form_open("admin_opd/disnaker/Admindisnaker/update_disnaker_sub_jenis"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID Sub</label>
	                        	<input type="number" name="id_sub_jenis" id="_id_sub_jenis2" class="form-control">
	                      	</div>                      
	                      
	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jenis</label>
								<select name="id_jenis" class="form-control" id="_id_jenis3">
									<option value="">-- Pilih Jenis --</option>
									<?php foreach ($list_disnaker_jenis as $ltj) { ?>
										<option value="<?php echo $ltj->id_jenis ?>"><?php echo $ltj->nama_jenis ?></option>
									<?php } ?>
								</select>
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Nama</label>
	                        	<input type="text" name="nama_sub_jenis" class="form-control" id="_nama_sub_jenis" placeholder="Nama">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>	
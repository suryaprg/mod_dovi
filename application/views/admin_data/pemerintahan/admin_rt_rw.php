 <!-- okContent Header (Page header) -->
    <section class="content-header">

    </section>

  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data RT dan RW </h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/pemerintahan/Adminrtrw/insert_rtrw"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Kecamatan</label>
                      <select name="id_kec" id="id_kec" class="form-control">

                        <?php
                          if($list_kecamatan){
                          $no = 1;
                          foreach ($list_kecamatan as $r_list_kecamatan => $v_list_kecamatan) {
                            echo "<option value=\"".$v_list_kecamatan->id_kec."\">".$v_list_kecamatan->nama_kec."</option>";
                            $no++;
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="text" class="form-control" id="th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah RT</label>
                      <input type="number" class="form-control" id="jml_rt" name="jml_rt" placeholder="Jumlah RT" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah RW</label>
                      <input type="text" class="form-control" id="jml_rw" name="jml_rw" placeholder="Jumlah RW" required="">
                    </div>
                  </div>

                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->

      
      <!-- <div class="row"> -->
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data RT RW</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kecamatan</th>
                  <th>Periode (Tahun)</th>
                  <th>Jumlah RT</th>
                  <th>Jumlah RW</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_rt_rw){
                      $no = 1;
                      foreach ($list_rt_rw as $r_list_rt_rw => $v_list_rt_rw) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_rt_rw->nama_kec."</td>
                                <td>".$v_list_rt_rw->th."</td>
                                <td>".$v_list_rt_rw->jml_rt."</td>
                                <td>".$v_list_rt_rw->jml_rw."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_rt_rw('".$v_list_rt_rw->id_rt_rw."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_rt_rw('".$v_list_rt_rw->id_rt_rw."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
  </div>

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------RT RW---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

        <div class="modal fade" id="modal_up_rt_rw">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data RT RW</h4>
              </div>
              <?php echo form_open("admin_opd/pemerintahan/Adminrtrw/up_rt_rw"); ?>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Kecamatan</label>
                      <select name="id_kec" id="_id_kec" class="form-control">

                        <?php
                          if($list_kecamatan){
                          $no = 1;
                          foreach ($list_kecamatan as $r_list_kecamatan => $v_list_kecamatan) {
                            echo "<option value=\"".$v_list_kecamatan->id_kec."\">".$v_list_kecamatan->nama_kec."</option>";
                            $no++;
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="text" class="form-control" id="_th" name="th" placeholder="Jumlah RW" required="">
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah RT</label>
                      <input type="number" class="form-control" id="_jml_rt" name="jml_rt" placeholder="Jumlah RT" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah RW</label>
                      <input type="text" class="form-control" id="_jml_rw" name="jml_rw" placeholder="Jumlah RW" required="">
                    </div>
                  </div>

                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">ID rtrw</label>
                      <input type="text" class="form-control" id="_id_rtrw" name="id_rtrw" placeholder="Tahun" required="">
                    </div>
                  </div>
                </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>

        
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------RT RW---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    
    

    <script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Stasiun---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_rt_rw(id_rtrw){
        clear_mod_up_rt_rw();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_rtrw', id_rtrw);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/pemerintahan/Adminrtrw/index_up_rt_rw";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_rt_rw(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_rt_rw").modal('show');
      }

      function res_update_rt_rw(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_kec").val(main_data.id_kec);
              $("#_th").val(main_data.th);
              $("#_jml_rt").val(main_data.jml_rt);
              $("#_jml_rw").val(main_data.jml_rw);
              $("#_id_rtrw").val(main_data.id_rt_rw);
              
          }else{
              clear_mod_up_st();
          }
      }

      function clear_mod_up_rt_rw(){
          $("#_id_kec").val("");
          $("#_th").val("");
          $("#_jml_rt").val("");
          $("#_jml_rw").val("");
          $("#_id_rtrw").val("");
          
      }

      // var admin_upx = "";
      function del_data_rt_rw(id_rtrw){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_rtrw+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_rtrw+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/pemerintahan/Adminrtrw/delete_rt_rw/";?>"+id_rtrw;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Stasiun---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


      

    </script>
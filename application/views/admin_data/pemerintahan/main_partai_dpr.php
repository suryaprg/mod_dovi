 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Fraksi DPR</h3>
              <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_dpr"><i class="fa fa-plus"></i>&nbsp;&nbsp; Tambah Data Fraksi DPR</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Fraksi Partai</th>
                  <th>Jumlah Anggota</th>
                  <th>Jenis Kelamin</th>
                  <th>Periode</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_dpr){
                      $no = 1;
                      foreach ($list_dpr as $r_list_dpr => $v_list_dpr) {

                        $jk = "Laki-Laki";
                        if($v_list_dpr->jk){
                          $jk = "Perempuan";
                        }
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_dpr->nama_partai."</td>
                                <td>".$v_list_dpr->jml."</td>
                                <td>".$jk."</td>
                                <td>".$v_list_dpr->th."</td>
                               
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_dpr('".$v_list_dpr->id_pem_dpr."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_dpr('".$v_list_dpr->id_pem_dpr."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->

        <div class="col-md-4">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Partai</h3>
              <button type="button" class="btn btn-success pull-right" id="add_partai" data-toggle="modal" data-target="#modal_in_partai"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data Partai</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Partai</th>
                  <th>Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                  <?php

                  if($list_partai){
                      $no = 1;
                      foreach ($list_partai as $r_list_partai => $v_list_partai) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_partai->nama_partai."</td>
                               
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_partai('".$v_list_partai->id_partai."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_partai('".$v_list_partai->id_partai."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                    
                  ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

      </div>
      <!-- /.row -->      
    </div>

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Partai---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

        <div class="modal fade" id="modal_in_partai">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Data Partai</h4>
              </div>
              <?php echo form_open("admin_super/mainpartai/insert_partai"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          
                          <div class="form-group">
                            <label for="exampleInputPassword1">Nama Partai</label>
                            <input type="text" class="form-control" id="nama_partai" name="nama_partai" placeholder="Nama Partai" required="">
                          </div>

                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="modal_up_partai">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Partai</h4>
              </div>
              <?php echo form_open("admin_super/mainpartai/up_partai"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          
                          <div class="form-group">
                            <label for="exampleInputPassword1">Nama Partai</label>
                            <input type="text" class="form-control" id="up_nama_partai" name="nama_partai" placeholder="Nama Partai" required="">
                          </div>

                          <div class="form-group" hidden="">
                            <label for="exampleInputPassword1">Id Partai</label>
                            <input type="text" class="form-control" id="up_id_partai" name="id_partai" placeholder="Luas Wilayah (km2)" required="">
                          </div>

                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Partai---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------DPR---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        <div class="modal fade" id="modal_in_dpr">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Data DPR</h4>
              </div>
              <?php echo form_open("admin_super/mainpartai/insert_dpr"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          
                          <div class="form-group">
                            <label for="exampleInputPassword1">Fraksi DPR</label>

                            <select class="form-control" id="id_partai" name="id_partai">
                              <?php
                                if($list_partai){
                                    foreach ($list_partai as $r_list_partai => $v_list_partai) {
                                      echo "<option value=\"".$v_list_partai->id_partai."\">".$v_list_partai->nama_partai."</option>";
                                    }
                                }
                              ?>
                            </select>
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Periode</label>
                            <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Jenis Kelamin</label>
                            <select class="form-control" id="jk" name="jk">
                              <option value="0">Laki-Laki</option>
                              <option value="1">Perempuan</option>
                            </select>
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Jumlah Anggota</label>
                            <input type="number" class="form-control" id="jml" name="jml" placeholder="Luas Wilayah (km2)" required="">
                          </div>

                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="modal_up_dpr">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data DPR</h4>
              </div>
              <?php echo form_open("admin_super/mainpartai/up_dpr"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          
                          <div class="form-group">
                            <label for="exampleInputPassword1">Fraksi DPR</label>

                            <select class="form-control" id="up_id_partai_dpr" name="id_partai">
                              <?php
                                if($list_partai){
                                    foreach ($list_partai as $r_list_partai => $v_list_partai) {
                                      echo "<option value=\"".$v_list_partai->id_partai."\">".$v_list_partai->nama_partai."</option>";
                                    }
                                }
                              ?>
                            </select>
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Periode</label>
                            <input type="text" class="form-control" id="up_periode" name="periode" placeholder="Periode" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Jenis Kelamin</label>
                            <select class="form-control" id="up_jk" name="jk">
                              <option value="0">Laki-Laki</option>
                              <option value="1">Perempuan</option>
                            </select>
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Jumlah Anggota</label>
                            <input type="number" class="form-control" id="up_jml" name="jml" placeholder="Luas Wilayah (km2)" required="">
                          </div>

                          <div class="form-group" hidden="">
                            <label for="exampleInputPassword1">Id DPR</label>
                            <input type="text" class="form-control" id="up_id_dpr" name="id_dpr" placeholder="Periode" required="">
                          </div>

                        </div>

                      </div>
                      <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------DPR---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    


    <script type="text/javascript">


      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Partai---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_partai(id_partai){
        clear_mod_up_partai();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_partai', id_partai);    
          $.ajax({
            url: "<?php echo base_url()."/admin_super/mainpartai/index_up_partai/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_partai(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_partai").modal('show');
      }

      function res_update_partai(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#up_nama_partai").val(main_data.nama_partai);
              $("#up_id_partai").val(main_data.id_partai);    
          }else{
              clear_mod_partai();
          }
      }

      function clear_mod_up_partai(){
          $("#up_nama_partai").val("");
          $("#up_id_partai").val("");

      }

      // var admin_upx = "";
      function del_data_partai(id_partai){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_partai+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_partai+" akan terhapus semua...");
        if(conf){
          window.location.href = "<?= base_url()."admin_super/mainpartai/delete_partai/";?>"+id_partai;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Partai---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------DPR---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
      function up_data_dpr(id_dpr){
        clear_mod_up_dpr();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_dpr', id_dpr);    
          $.ajax({
            url: "<?php echo base_url()."admin_super/mainpartai/index_up_dpr/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_dpr(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_dpr").modal('show');
      }

      function res_update_dpr(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              // console.log(main_data);
              // console.log(main_data.id_partai);
              $("#up_id_partai_dpr").val(main_data.id_partai);
              $("#up_periode").val(main_data.th);
              $("#up_jk").val(main_data.jk);
              $("#up_jml").val(main_data.jml);
              $("#up_id_dpr").val(main_data.id_pem_dpr);   
          }else{
              clear_mod_up_dpr();
          }
      }

      function clear_mod_up_dpr(){
          
          $("#up_periode").val("");
          
          $("#up_jml").val("");
          $("#up_id_dpr").val("");
      }

      // var admin_upx = "";
      function del_data_dpr(id_dpr){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_dpr+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_dpr+" akan terhapus semau.. !!!!!!! ");
        if(conf){
          window.location.href = "<?= base_url()."admin_super/mainpartai/delete_dpr/";?>"+id_dpr;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------DPR---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    
      

    </script>
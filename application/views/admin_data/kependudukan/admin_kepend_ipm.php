 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data Index Pembangunan Manusia</h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/kependudukan/Adminkependudukanipm/insert_ipm"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Index Pembangunan Manusia</label>
                      <input type="text" class="form-control" id="rasio" name="rasio" placeholder="Jumlah IPM" required="">
                    </div>
                  </div>

                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Input Data Index Pembangunan Manusia</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Periode (Tahun)</th>
                  <th>Jumlah Index Pembangunan Manusia</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_kependudukan){
                      $no = 1;
                      foreach ($list_kependudukan as $r_list_kependudukan => $v_list_kependudukan) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_kependudukan->th."</td>
                                <td>".$v_list_kependudukan->ipm."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_kepend('".$v_list_kependudukan->id_ipm."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_kepend('".$v_list_kependudukan->id_ipm."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->       
      </div>
      <!-- /.row -->      
    </div>

    

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Kependudukan---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        

        <div class="modal fade" id="modal_up_kepend">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Index Pembangunan Manusia</h4>
              </div>
              <?php echo form_open("admin_opd/kependudukan/Adminkependudukanipm/up_ipm"); ?>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="_th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Index Pembangunan Manusia</label>
                      <input type="text" class="form-control" id="_rasio" name="rasio" placeholder="Jumlah IPM" required="">
                    </div>
                  </div>

                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_rasio</label>
                      <input type="text" class="form-control" id="_id_ipm" name="id_ipm" placeholder="Jumlah Rasio Ketergantungan" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Kependudukan---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

    <script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Kependudukan---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_kepend(id_ipm){
        clear_mod_up_kepend();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_ipm', id_ipm);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/kependudukan/Adminkependudukanipm/index_up_ipm/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_kepend(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_kepend").modal('show');
      }

      function res_update_kepend(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_ipm").val(main_data.id_ipm);
              $("#_th").val(main_data.th);
              $("#_rasio").val(main_data.ipm);
              
          }else{
              clear_mod_up_kepend();
          }
      }

      function clear_mod_up_kepend(){
          $("#_id_ipm").val("");
          $("#_th").val("");
          $("#_rasio").val("");
      }

      // var admin_upx = "";
      function del_data_kepend(id_ipm){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_ipm+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_ipm+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/kependudukan/Adminkependudukanipm/delete_ipm/";?>"+id_ipm;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Kependudukan---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


      

    </script>
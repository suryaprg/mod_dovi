 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data Index Kesehatan, Daya Beli dan Pendidikan</h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/kependudukan/Adminkependudukanipmbidang/insert_ipm"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Perkembangan Index Kesehatan</label>
                      <input type="text" class="form-control" id="in_kes" name="in_kes" placeholder="Jumlah Index Kesehatan" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Perkembangan Index Daya Beli</label>
                      <input type="text" class="form-control" id="in_daya" name="in_daya" placeholder="Jumlah Index Daya Beli" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Perkembangan Index Pendidikan</label>
                      <input type="text" class="form-control" id="in_pend" name="in_pend" placeholder="Jumlah Index Pendidikan" required="">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Index Kesehatan, Daya Beli dan Pendidikan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Periode (Tahun)</th>
                  <th>Perkembangan Index Kesehatan</th>
                  <th>Perkembangan Index Daya Beli</th>
                  <th>Perkembangan Index Pendidikan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_kependudukan){
                      $no = 1;
                      foreach ($list_kependudukan as $r_list_kependudukan => $v_list_kependudukan) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_kependudukan->th."</td>
                                <td>".$v_list_kependudukan->in_kes."</td>
                                <td>".$v_list_kependudukan->in_daya."</td>
                                <td>".$v_list_kependudukan->in_pend."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_kepend('".$v_list_kependudukan->id_index."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_kepend('".$v_list_kependudukan->id_index."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->       
      </div>
      <!-- /.row -->      
    </div>

    

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Kependudukan---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        

        <div class="modal fade" id="modal_up_kepend">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Index Kesehatan, Daya Beli dan Pendidikan</h4>
              </div>
              <?php echo form_open("admin_opd/kependudukan/Adminkependudukanipmbidang/up_ipm"); ?>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="_th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Perkembangan Index Kesehatan</label>
                      <input type="text" class="form-control" id="_in_kes" name="in_kes" placeholder="Jumlah Rasio Ketergantungan" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Perkembangan Index Daya Beli</label>
                      <input type="text" class="form-control" id="_in_daya" name="in_daya" placeholder="Jumlah Rasio Ketergantungan" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Perkembangan Index Pendidikan</label>
                      <input type="text" class="form-control" id="_in_pend" name="in_pend" placeholder="Jumlah Rasio Ketergantungan" required="">
                    </div>
                  </div>
                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_index</label>
                      <input type="text" class="form-control" id="_id_index" name="id_index" placeholder="Jumlah Rasio Ketergantungan" required="" readonly="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Kependudukan---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

<script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Kependudukan---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_kepend(id_index){
        clear_mod_up_kepend();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_index', id_index);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/kependudukan/Adminkependudukanipmbidang/index_up_ipm/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_kepend(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_kepend").modal('show');
      }

      function res_update_kepend(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_index").val(main_data.id_index);
              $("#_th").val(main_data.th);
              $("#_in_kes").val(main_data.in_kes);
              $("#_in_daya").val(main_data.in_daya);
              $("#_in_pend").val(main_data.in_pend);
              
          }else{
              clear_mod_up_kepend();
          }
      }

      function clear_mod_up_kepend(){
          $("#_id_index").val("");
          $("#_th").val("");
          $("#_in_kes").val("");
          $("#_in_daya").val("");
          $("#_in_pend").val("");
      }

      // var admin_upx = "";
      function del_data_kepend(id_index){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_index+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_index+" akan terhapus semua.. ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/kependudukan/Adminkependudukanipmbidang/delete_ipm/";?>"+id_index;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Kependudukan---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


      

    </script>
 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Kependudukan Berdasarkan Kelompok Umur </h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/kependudukan/Adminkependudukanumur/insert_kepend"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Kelompok Umur</label>
                      <select class="form-control" id="kel_umur" name="kel_umur">
                        <option value="0 - 14">Umur 0 - 14</option>
                        <option value="15 - 64">Umur 15 - 64</option>
                        <option value="> 65">Umur > 65</option>
                      </select>
                                            
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Penduduk</label>
                      <input type="number" class="form-control" id="jml_pend" name="jml_pend" placeholder="Jumlah Penduduk" required="">
                    </div>
                  </div>

                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Kependudukan Berdasarkan Kelompok Umur</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kelompok Umur</th>
                  <th>Periode (Tahun)</th>
                  <th>Jumlah Penduduk</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_kependudukan){
                      $no = 1;
                      foreach ($list_kependudukan as $r_list_kependudukan => $v_list_kependudukan) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_kependudukan->kelompok."</td>
                                <td>".$v_list_kependudukan->periode."</td>
                                <td>".$v_list_kependudukan->jml_penduduk."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_kepend('".$v_list_kependudukan->id_kel_umur."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_kepend('".$v_list_kependudukan->id_kel_umur."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->       
      </div>
      <!-- /.row -->      
    </div>

    

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Kependudukan---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        

        <div class="modal fade" id="modal_up_kepend">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Kependudukan Berdasarkan Kelompok Umur</h4>
              </div>
              <?php echo form_open("admin_opd/kependudukan/Adminkependudukanumur/up_kepend"); ?>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Kelompok Umur</label>
                      <select class="form-control" id="_kel_umur" name="kel_umur">
                        <option value="0 - 14">Umur 0 - 14</option>
                        <option value="15 - 64">Umur 15 - 64</option>
                        <option value="> 65">Umur > 65</option>
                      </select>
                                            
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="_th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Penduduk</label>
                      <input type="number" class="form-control" id="_jml_pend" name="jml_pend" placeholder="Jumlah Penduduk" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_kel_umur</label>
                      <input type="number" class="form-control" id="_id_kel_umur" name="id_kel_umur" placeholder="id_kel_umur" required="" readonly="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Kependudukan---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

    <script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Kependudukan---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_kepend(id_kel_umur){
        clear_mod_up_kepend();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_kel_umur', id_kel_umur);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/kependudukan/Adminkependudukanumur/index_up_kepend/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_kepend(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_kepend").modal('show');
      }

      function res_update_kepend(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_kel_umur").val(main_data.id_kel_umur);
              $("#_jml_pend").val(main_data.jml_penduduk);
              $("#_kel_umur").val(main_data.kelompok);
              $("#_th").val(main_data.periode);
              
          }else{
              clear_mod_up_kepend();
          }
      }

      function clear_mod_up_kepend(){
          $("#_id_kel_umur").val("");
          $("#_jml_pend").val("");
          $("#_kel_umur").val("");
          $("#_th").val("");
      }

      // var admin_upx = "";
      function del_data_kepend(id_kel_umur){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_kel_umur+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kel_umur+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/kependudukan/Adminkependudukanumur/delete_kepend/";?>"+id_kel_umur;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Kependudukan---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


      

    </script>
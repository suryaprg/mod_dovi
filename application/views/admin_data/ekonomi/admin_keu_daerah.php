 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->

        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Keuangan Daerah </h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/ekonomi/Adminkeudaerah/insert_keu"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Keuangan</label>
                      <select name="id_jenis" id="id_jenis" class="form-control">

                        <?php
                          if($list_jenis_keu){
                            $no = 1;
                            foreach ($list_jenis_keu as $r_list_jenis_keu => $v_list_jenis_keu) {
                              echo "<option value=\"".$v_list_jenis_keu->id_jenis."\">".$v_list_jenis_keu->ket."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nominal</label>
                      <input type="text" class="form-control" id="nominal" name="nominal" placeholder="Nominal" required="">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        <!-- </div> -->
        <!--/.col (left) -->

        

      <!-- </div> -->
      
      <!-- <div class="row"> -->
        <!-- left column -->
        <!-- <div class="col-md-8"> -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Keuangan Daerah</h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kec"><i class="fa fa-plus"></i>&nbsp;&nbsp; Tambah Data Keuangan Daerah</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Keterangan</th>
                  <th>Tahun</th>
                  <th>Nominal</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  if($list_data_keu){
                      $no = 1;
                      foreach ($list_data_keu as $r_list_data_keu => $v_list_data_keu) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_data_keu->ket."</td>
                                <td>".$v_list_data_keu->th."</td>
                                <td>".$v_list_data_keu->jml."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_keu('".$v_list_data_keu->id_jml_keu."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_keu('".$v_list_data_keu->id_jml_keu."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                    $no++;
                    }

                  }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <div class="col-md-4">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Jenis Keuangan Daerah</h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kel"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data Jenis Keuangan Daerah</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Anggaran</th>
                  <th>Kategori Anggaran</th>
                  <th>Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php echo form_open("admin_opd/ekonomi/Adminkeudaerah/insert_jenis"); ?>
                      <td colspan="2"><input type="text" class="form-control" id="jenis" name="jenis"></td>
                      <td colspan=""><div class="col-md-12">
                            <div class="form-group">
                              <select class="form-control" id="kategori" name="kategori">
                                <option value="0">Realisasi Pendapatan</option>
                                <option value="1">Realisasi Belanaja</option>
                              </select>
                              <!-- <input type="text" class="form-control" id="_ket" name="jenis" placeholder="Keterangan" required=""> -->
                            </div>
                          </div></td>
                      <td><button type="submit" class="btn btn-success pull-right" id="add_jenis_keu" name="add_jenis_keu"><i class="fa fa-plus"></i>&nbsp;&nbsp; Simpan</button></td>
                    </form>
                  </tr>
                  <?php
                    if($list_jenis_keu){
                      $no = 1;
                      foreach ($list_jenis_keu as $r_list_jenis_keu => $v_list_jenis_keu) {
                        $status_kategori = "Realisasi Pendapatan";
                        if($v_list_jenis_keu->kategori == "1"){
                          $status_kategori = "Realisasi Belanaja";
                        }
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_jenis_keu->ket."</td>
                                <td>".$status_kategori."</td>

                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_jenis('".$v_list_jenis_keu->id_jenis."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_jenis('".$v_list_jenis_keu->id_jenis."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                    
                  ?>
                  
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row --> 
      
    </div>

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


        <div class="modal fade" id="modal_up_jenis">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Jenis Keuangan</h4>
              </div>
              <?php echo form_open("admin_opd/ekonomi/Adminkeudaerah/up_jenis"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Keterangan</label>
                              <input type="text" class="form-control" id="_ket" name="jenis" placeholder="Keterangan" required="">
                            </div>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Kategori</label>
                              <select class="form-control" id="_kategori" name="kategori">
                                <option value="0">Realisasi Pendapatan</option>
                                <option value="1">Realisasi Belanaja</option>
                              </select>
                              <!-- <input type="text" class="form-control" id="_ket" name="jenis" placeholder="Keterangan" required=""> -->
                            </div>
                          </div>
                          <div class="col-md-12" hidden="">
                            <div class="form-group">
                              <label for="exampleInputPassword1">id jenis</label>
                              <input type="text" class="form-control" id="_id_jenis" name="id_jenis" placeholder="Jumlah RW" required="" readonly="">
                            </div>
                          </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Keuangan Daerah---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        <div class="modal fade" id="modal_up_keu">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Keuangan Daerah</h4>
              </div>
              <?php echo form_open("admin_opd/ekonomi/adminkeudaerah/up_keu/"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Jenis Keuangan</label>
                                <select name="id_jenis" id="_id_jenis" class="form-control">

                                  <?php
                                    if($list_jenis_keu){
                                      $no = 1;
                                      foreach ($list_jenis_keu as $r_list_jenis_keu => $v_list_jenis_keu) {
                                        echo "<option value=\"".$v_list_jenis_keu->id_jenis."\">".$v_list_jenis_keu->ket."</option>";
                                        $no++;
                                      }
                                    }
                                    
                                  ?>
                                </select>
                              </div>
                            </div>
                            
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Tahun</label>
                                <input type="number" class="form-control" id="_th" name="th" placeholder="Jumlah RT" required="">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Nominal</label>
                                <input type="text" class="form-control" id="_nominal" name="nominal" placeholder="Jumlah RW" required="">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="exampleInputPassword1">id_keu</label>
                                <input type="text" class="form-control" id="_id_keu" name="id_keu" placeholder="Jumlah RW" required="" readonly="">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Keuangan Daerah---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    


    <script type="text/javascript">


      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_jenis(id_jenis){
        clear_mod_up_jenis();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_jenis', id_jenis);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/ekonomi/adminkeudaerah/index_up_jenis";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_jenis(res);
            }
        });
        
        $("#modal_up_jenis").modal('show');
      }

      function res_update_jenis(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_ket").val(main_data.ket);
              $("#_id_jenis").val(main_data.id_jenis);
              $("#_kategori").val(main_data.kategori);
              
              
          }else{
              clear_mod_up_jenis();
          }
      }

      function clear_mod_up_jenis(){
          $("#_ket").val("");
          $("#_id_jenis").val("");
          $("#_kategori").val("");
         
      }

      // var admin_upx = "";
      function del_data_jenis(id_jenis){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/ekonomi/Adminkeudaerah/delete_jenis/";?>"+id_jenis;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Keuangan Daerah---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_keu(id_jml_keu){
        clear_mod_up_keu();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_jml_keu', id_jml_keu);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/ekonomi/Adminkeudaerah/index_up_keu/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_keu(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_keu").modal('show');
      }

      function res_update_keu(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_keu").val(main_data.id_jml_keu);
              $("#_id_jenis").val(main_data.id_jenis);

              // console.log(main_data.id_kec);
              $("#_th").val(main_data.th);
              $("#_nominal").val(main_data.jml);
              
          }else{
              clear_mod_up_keu();
          }
      }

      function clear_mod_up_keu(){
          $("#_id_keu").val("");
          $("#_id_jenis").val("");
          $("#_th").val("");
          $("#_nominal").val("");
      }

      // var admin_upx = "";
      function del_data_keu(id_jml_keu){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_jml_keu+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jml_keu+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/ekonomi/adminkeudaerah/delete_keu/";?>"+id_jml_keu;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Keuangan Daerah---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    
      

    </script>
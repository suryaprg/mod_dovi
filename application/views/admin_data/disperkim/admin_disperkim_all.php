 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->

    <!--     <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Perumahan, Kawasan dan Permukiman </h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/disperkim/Admindisperkim/insert"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Disperkim </label>
                      <select name="id_jenis" id="id_jenis" class="form-control">

                        <?php
                          if($list_disperkim_all_jenis){
                            $no = 1;
                            foreach ($list_disperkim_all_jenis as $r_data => $v_data) {
                              echo "<option value=\"".$v_data->id_jenis."\">".$v_data->nama_jenis."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Sub Jenis</label>
                      <select name="id_sub_jenis" id="id_sub_jenis" class="form-control">

                        <?php
                          if($list_disperkim_all_sub_jenis){
                            $no = 1;
                            foreach ($list_disperkim_all_sub_jenis as $r_data => $v_data) {
                              echo "<option value=\"".$v_data->id_sub_jenis."\">".$v_data->nama_sub_jenis."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                                 
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Satuan</label>
                      <input type="text" class="form-control" id="satuan" name="satuan" placeholder="Satuan" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah</label>
                      <input type="number" class="form-control" id="jml" name="jml" placeholder="Jumlah" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>
                  
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan Data</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        <!-- </div> -->
        <!--/.col (left) -->


        <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kec"><i class="fa fa-plus"></i>&nbsp;&nbsp; Tambah Data Jumlah Panen Komoditi (Ton)</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-striped display" id="example1" >
                <thead>
                <tr>
                  <th>No</th>
                
                  <th>Nama Jenis</th>
                  <th>Nama Sub Jenis</th>
                  <th>Satuan</th>
                  <th>Jumlah</th>
                  <th>Tahun</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  if($list_disperkim_all){
                      $no = 1;

                      foreach ($list_disperkim_all as $r_data => $v_data) {
                        
                        echo "<tr>
                                <td>".$no."</td>
                                
                                <td>".$v_data->nama_jenis."</td>
                                <td>".$v_data->nama_sub_jenis."</td>
                                <td>".$v_data->satuan."</td>
                                <td>".$v_data->jml."</td>
                                <td>".$v_data->th."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_disperkim('".$v_data->id_disperkim."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_disperkim('".$v_data->id_disperkim."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                    $no++;
                    }

                  }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->


      <!-------------------------------------------------------------->

      <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Kategori</h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kel"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data Jenis Keuangan Daerah</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table  class="table table-bordered table-striped display" id="example2">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Kategori</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php echo form_open("admin_opd/disperkim/Admindisperkim/insert_kategori"); ?>
                      <td colspan="2">
                        <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" placeholder="Nama Kategori"></td>                  
                        <td><button type="submit" class="btn btn-success pull-right" id="add_disperkim_all_sub_jenis" name="add_disperkim_all_sub_jenis"><i class="fa fa-plus"></i>&nbsp;&nbsp; Simpan</button>
                      </td>
                    </form>
                  </tr>
                  <?php
                    if($list_disperkim_all_kategori){
                      $no = 1;
                      foreach ($list_disperkim_all_kategori as $r_data => $v_data) {
                        
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_data->nama_kategori."</td>
                                
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_kategori('".$v_data->id_kategori."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_kategori('".$v_data->id_kategori."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                    
                  ?>
                  
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
         <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Jenis</h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kel"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data Jenis Keuangan Daerah</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Kategori</th>
                  <th>Nama Jenis</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php echo form_open("admin_opd/disperkim/Admindisperkimjenis/insert"); ?>
                      <td colspan="2">
                        <select name="id_kategori" class="form-control" id="id_kategori">
                          <option value="">-- Pilih Kategori --</option>
                          <?php foreach ($list_disperkim_all_kategori as $dak) { ?>
                            <option value="<?php echo $dak->id_kategori ?>"><?php echo $dak->nama_kategori ?></option>
                          <?php } ?>
                        </select>

                      <td colspan="1">
                        <input type="text" class="form-control" id="nama_jenis" name="nama_jenis">
                      </td>                      
                        <td><button type="submit" class="btn btn-success pull-right" id="add_disperkim_all_sub_jenis" name="add_disperkim_all_sub_jenis"><i class="fa fa-plus"></i>&nbsp;&nbsp; Simpan</button>
                      </td>

                       
                    </form>
                  </tr>
                  <?php
                    if($list_disperkim_all_jenis){
                      $no = 1;
                      foreach ($list_disperkim_all_jenis as $r_data => $v_data) {
                        
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_data->nama_kategori."</td>
                                <td>".$v_data->nama_jenis."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_jenis('".$v_data->id_jenis."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_jenis('".$v_data->id_jenis."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                    
                  ?>
                  
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->

        </div>
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Sub Jenis</h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kel"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data Jenis Keuangan Daerah</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example4" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Jenis</th>
                  <th>Nama Sub Jenis</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php echo form_open("admin_opd/disperkim/Admindisperkimjenis/insert"); ?>
                      <td colspan="2">
                        <select name="id_jenis" class="form-control" id="id_jenis">
                          <option value="">-- Pilih Jenis --</option>
                          <?php foreach ($list_disperkim_all_jenis as $daj) { ?>
                            <option value="<?php echo $daj->id_jenis ?>"><?php echo $daj->nama_jenis ?></option>
                          <?php } ?>
                        </select>

                      <td colspan="1">
                        <input type="text" class="form-control" id="nama_jenis" name="nama_jenis">
                      </td>                      
                        <td><button type="submit" class="btn btn-success pull-right" id="add_disperkim_all_sub_jenis" name="add_disperkim_all_sub_jenis"><i class="fa fa-plus"></i>&nbsp;&nbsp; Simpan</button>
                      </td>

                       
                    </form>
                  </tr>
                  <?php
                    if($list_disperkim_all_sub_jenis){
                      $no = 1;
                      foreach ($list_disperkim_all_sub_jenis as $r_data => $v_data) {
                        
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_data->nama_jenis."</td>
                                <td>".$v_data->nama_sub_jenis."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_jenis('".$v_data->id_jenis."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_jenis('".$v_data->id_jenis."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                    
                  ?>
                  
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
</div>
</div>
</section>
</div>
        

      <!-- </div> -->
      
      <!-- <div class="row"> -->
        <!-- left column -->
        <!-- <div class="col-md-8"> -->
  

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------- Komoditi ---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
       <div class="modal fade" id="modal_up_disperkim">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data</h4>
              </div>
              <?php echo form_open("admin_opd/disperkim/Admindisperkim/update/"); ?>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis</label>
                      <select name="id_disperkim" id="_id_disperkim" class="form-control">

                       <?php
                                    if($list_disperkim_all){
                                      $no = 1;
                                      foreach ($list_disperkim_all as $r_list_disperkim_all => $v_list_disperkim_all) {
                                        echo "<option value=\"".$v_list_disperkim_all->id_disperkim."\">".$v_list_disperkim_all->nama_jenis."</option>";     
                                      $no++;
                                      }
                                    }
                                  ?>
                      </select>
                    </div>
                  </div>
                   <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Sub Jenis</label>
                      <select name="id_disperkim" id="_id_disperkim" class="form-control">

                       <?php
                                    if($list_disperkim_all){
                                      $no = 1;
                                      foreach ($list_disperkim_all as $r_list_disperkim_all => $v_list_disperkim_all) {
                                        echo "<option value=\"".$v_list_disperkim_all->id_disperkim."\">".$v_list_disperkim_all->nama_sub_jenis."</option>";     
                                      $no++;
                                      }
                                    }
                                  ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Satuan</label>
                      <input type="text" class="form-control" id="_satuan" name="satuan" placeholder="Tanggal" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah</label>
                      <input type="text" class="form-control" id="_jml" name="jml" placeholder="Jumlah" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="text" class="form-control" id="_th" name="th" placeholder="Lokasi" required="">
                    </div>
                  </div>
                 <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_dis</label>
                      <input type="text" class="form-control" id="_id_disperkim" name="id_disperkim" readonly="" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------- Komoditi ---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    <div class="modal fade" id="modal_up_kategori">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data</h4>
              </div>
              <?php echo form_open("admin_opd/disperkim/Admindisperkim/update_kategori/"); ?>
              <div class="modal-body">
                <div class="row">    
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama Kategori</label>
                      <input type="text" class="form-control" id="_nama_kategori" name="nama_kategori" placeholder="Lokasi" required="">
                    </div>
                  </div>
                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_disperkim</label>
                      <input type="text" class="form-control" id="_id_kategori" name="id_kategori" readonly="" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- /.modal -->


        <!-- -------------------------------------------------------Jenis---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    <div class="modal fade" id="modal_up_jenis">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data</h4>
              </div>
              <?php echo form_open("admin_opd/disperkim/Admindisperkim/update_jenis/"); ?>
              <div class="modal-body">
                <div class="row">
                 <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Kategori</label>
                      <select name="id_jenis" id="_id_jenis" class="form-control">
                                  <?php
                                    if($list_disperkim_all_jenis){
                                      $no = 1;
                                      foreach ($list_disperkim_all_jenis as $r_list_disperkim_all_jenis => $v_list_disperkim_all_jenis) {
                                        echo "<option value=\"".$v_list_disperkim_all_jenis->id_jenis."\">".$v_list_disperkim_all_jenis->nama_kategori."</option>";     
                                      $no++;
                                      }
                                    }
                                  ?>
                      </select>
                    </div>
                  </div>
                    <div class="col-md-12">
                     <div class="form-group">
                          <label for="exampleInputPassword1">Nama Jenis</label>
                          <input type="text" class="form-control" id="_nama_jenis" name="nama_jenis" placeholder="Keterangan" required="">
                           </div>
                     </div>

                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_jenis</label>
                      <input type="text" class="form-control" id="_id_jenis" name="id_jenis" readonly="" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<!-------------------------------------------------sub jenis------------------------------------------>

        <div class="modal fade" id="modal_up_sub_jenis">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data</h4>
              </div>
              <?php echo form_open("admin_opd/disperkim/Admindisperkim/update_sub_jenis/"); ?>
              <div class="modal-body">
                <div class="row">
                 <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Kategori</label>
                      <select name="id_sub_jenis" id="_id_sub_jenis" class="form-control">
                                  <?php
                                    if($list_disperkim_all_sub_jenis){
                                      $no = 1;
                                      foreach ($list_disperkim_all_sub_jenis as $r_list_disperkim_all_sub_jenis => $v_list_disperkim_all_sub_jenis) {
                                        echo "<option value=\"".$v_list_disperkim_all_sub_jenis->id_sub_jenis."\">".$v_list_disperkim_all_sub_jenis->nama_jenis."</option>";     
                                      $no++;
                                      }
                                    }
                                  ?>
                      </select>
                    </div>
                  </div>
                    <div class="col-md-12">
                     <div class="form-group">
                          <label for="exampleInputPassword1">Nama Sub Jenis</label>
                          <input type="text" class="form-control" id="_nama_sub_jenis" name="nama_sub_jenis" placeholder="Keterangan" required="">
                           </div>
                     </div>

                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_jenis</label>
                      <input type="text" class="form-control" id="_id_sub_jenis" name="id_sub_jenis" readonly="" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


    

    


    <script type="text/javascript">



    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Komoditi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_disperkim(id_disperkim){
        clear_mod_up_disperkim();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_disperkim', id_disperkim);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/disperkim/Admindisperkim/index_up/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_disperkim(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_disperkim").modal('show');
      }

      function res_update_disperkim(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
            
              $("#_id_disperkim").val(main_data.id_disperkim);
              $("#_id_jenis").val(main_data.nama_jenis);
              $("#_id_sub_jenis").val(main_data.nama_sub_jenis);
              $("#_satuan").val(main_data.satuan);
              $("#_jml").val(main_data.jml);
              $("#_th").val(main_data.th);
              
              
          }else{
              clear_mod_up_disperkim();
          }
      }

      function clear_mod_up_disperkim(){
        
          $("#_id_jenis").val("");
          $("#_id_sub_jenis").val("");
          $("#_satuan").val("");
          $("#_jml").val("");
          $("#_th").val("");
         
      }

      // var admin_upx = "";
      function del_data_disperkim(id_disperkim){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_disperkim+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_disperkim+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/disperkim/Admindisperkim/delete/";?>"+id_disperkim;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Komoditi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    function up_data_jenis(id_sub_jenis){
        clear_mod_up_sub_jenis();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_sub_jenis', id_sub_jenis);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/disperkim/Admindisperkimjenis/index_up_sub_jenis/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_sub_jenis(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_sub_jenis").modal('show');
      }

      function res_update_sub_jenis(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
              
              $("#_id_jenis").val(main_data.nama_jenis);
              $("#_id_sub_jenis").val(main_data.nama_sub_jenis);
              
              
          }else{
              clear_mod_up_sub_jenis();
          }
      }

      function clear_mod_up_sub_jenis(){
         
          $("#_id_jenis").val("");
           $("#_id_sub_jenis").val("");
         
         
      }

      // var admin_upx = "";
      function del_data_jenis(id_sub_jenis){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_sub_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_sub_jenis+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/disperkim/Admindisperkimjenis/delete_sub_jenis/";?>"+id_sub_jenis;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Komoditi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_kategori(id_kategori){
        clear_mod_up_kategori();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_kategori', id_kategori);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/disperkim/Admindisperkim/index_up_kategori/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_kategori(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_kategori").modal('show');
      }

      function res_update_kategori(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
            
              $("#_id_kategori").val(main_data.id_kategori);
              $("#_nama_kategori").val(main_data.nama_kategori);
              
              
          }else{
              clear_mod_up_kategori();
          }
      }

      function clear_mod_up_kategori(){
        
          $("#_id_kategori").val("");
          $("#_nama_kategori").val("");
         
      }

      // var admin_upx = "";
      function del_data_kategori(id_kategori){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_kategori+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kategori+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/disperkim/Admindisperkim/delete_kategori/";?>"+id_kategori;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Komoditi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    //<!-- -------------------------------------------------------------Komoditi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_jenis(id_jenis){
        clear_mod_up_jenis();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_jenis', id_jenis);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/disperkim/Admindisperkim/index_up_jenis/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_jenis(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_jenis").modal('show');
      }

      function res_update_jenis(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_jenis").val(main_data.id_jenis);
              $("#_nama_jenis").val(main_data.nama_jenis);
              $("#_id_kategori").val(main_data.id_kategori);
             
              
              
          }else{
              clear_mod_up_jenis();
          }
      }

      function clear_mod_up_jenis(){
          $("#_id_jenis").val("");
          $("#_nama_jenis").val("");
           $("#_id_kategori").val("");
         
      }

      // var admin_upx = "";
      function del_data_jenis(id_jenis){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/disperkim/Admindisperkim/delete_jenis/";?>"+id_jenis;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Komoditi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

      

    </script>
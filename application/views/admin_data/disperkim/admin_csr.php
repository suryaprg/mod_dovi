 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data CSR </h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/disperkim/admincsr/insert_csr"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Pemberi CSR</label>
                     <input type="text" class="form-control" id="pemberi_csr" name="pemberi_csr" placeholder="Pemberi CSR" required="">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tanggal NPHD</label>
                      <input type="date" class="form-control" id="tgl_nphd" name="tgl_nphd" placeholder="Tanggal" required="">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">No NPHD</label>
                      <input type="text" class="form-control" id="no_nphd" name="no_nphd" placeholder="No NPHD" required="">
                    </div>
                  </div>
                   <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">RAB</label>
                      <input type="text" class="form-control" id="rab" name="rab" placeholder="Terlampir / Belum Terlampir" required="">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tanggal Bash</label>
                      <input type="date" class="form-control" id="tgl_bash" name="tgl_bash" placeholder="Tanggal" required="">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">No Bash</label>
                      <input type="text" class="form-control" id="no_bash" name="no_bash" placeholder="No Bash" required="">
                    </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Objek Pembangunan</label>
                      <textarea class="form-control" id="obj_pembangunan" name="obj_pembangunan" placeholder="Keterangan" required=""></textarea> 
                    </div>
                  </div>
                 

                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data csr</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Pemberi CSR</th>
                  <th>Tanggal NHPD</th>
                  <th>No NHPD</th>
                  <th>Tanggal Bash</th>
                  <th>No Bash</th>
                  <th>Obj Pembangunan</th>
                  <th>Rab</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_csr){
                      $no = 1;
                      foreach ($list_csr as $r_list_csr => $v_list_csr) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_csr->pemberi_csr."</td>
                                <td>".$v_list_csr->tgl_nphd."</td>
                                <td>".$v_list_csr->no_nphd."</td>
                                <td>".$v_list_csr->tgl_bash."</td>
                                <td>".$v_list_csr->no_bash."</td>
                                <td>".$v_list_csr->obj_pembangunan."</td>
                                <td>".$v_list_csr->rab."</td>
                               
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_csr('".$v_list_csr->id_csr."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_csr('".$v_list_csr->id_csr."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->

        

      </div>
      <!-- /.row -->      
    </div>

 

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Iklim---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        

        <div class="modal fade" id="modal_up_csr">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data csr</h4>
              </div>
              <?php echo form_open("admin_opd/disperkim/admincsr/up_csr"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">csr</label>
                                <input type="text" class="form-control" id="_pemberi_csr" name="pemberi_csr" placeholder="csr" required="">
                              </div>
                            </div>
                            
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Tgl NPHD</label>
                                <input type="text" class="form-control" id="_tgl_nphd" name="tgl_nphd" placeholder="Tanggal NPHD" required="">
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">No NPHD</label>
                                <input type="text" class="form-control" id="_no_nphd" name="no_nphd" placeholder="Keterangan" required="">
                              </div>
                            </div>


                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Tgl Bash</label>
                                <input type="text" class="form-control" id="_tgl_bash" name="tgl_bash" placeholder="csr" required="">
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">No Bash</label>
                                <input type="text" class="form-control" id="_no_bash" name="no_bash" placeholder="csr" required="">
                                </div>
                            </div>
                                <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Objek Pembangunan</label>
                                <input type="text" class="form-control" id="_obj_pembangunan" name="obj_pembangunan" placeholder="csr" required="">
                              </div>
                            </div>
                              <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">RAB</label>
                                <input type="text" class="form-control" id="_rab" name="rab" placeholder="csr" required="">
                              </div>
                            </div>
                            <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_csr</label>
                      <input type="text" class="form-control" id="_id_csr" name="id_csr" readonly="" required="">
                    </div>
                  </div>
                            </div>
                              </div>
                            </div>
                          </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Iklim---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

    <script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Stasiun---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_csr(id_csr){
        clear_mod_up_csr();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_csr', id_csr);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/disperkim/Admincsr/index_up_csr/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_csr(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_csr").modal('show');
      }

      function res_update_csr(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_pemberi_csr").val(main_data.pemberi_csr);
              $("#_tgl_nphd").val(main_data.tgl_nphd);
              $("#_no_nphd").val(main_data.no_nphd);
              $("#_tgl_bash").val(main_data.tgl_bash);
              $("#_no_bash").val(main_data.no_bash);
              $("#_obj_pembangunan").val(main_data.obj_pembangunan);
              $("#_rab").val(main_data.rab);  
              $("#_id_csr").val(main_data.id_csr);
              
          }else{
              clear_mod_up_csr();
          }
      }

      function clear_mod_up_csr(){
         $("#up_pemberi_csr").val("");
         $("#up_tgl_nphd").val("");
         $("#up_no_nphd").val("");
         $("#up_tgl_bash").val("");
         $("#up_no_bash").val("");
         $("#up_obj_pembangunan").val("");
         $("#up_rab").val("");
         $("#up_id_csr").val("");
         
          
      }

      // var admin_upx = "";
      function del_data_csr(id_csr){

        var conf = confirm("Apakah anda yakin untuk menghapus "+id_csr+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_csr+" akan terhapus semua.. !!!!!!! ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/disperkim/Admincsr/delete_csr/";?>"+id_csr;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Stasiun---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- --

    </script>
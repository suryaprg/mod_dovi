  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <section class="content">
      <div class="row">

        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Pohon</h3>
            </div>

            <div class="box-body">              
              <table class="table table-bordered table-striped display" id="">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 1%">No</th>
                    <th class="text-center" style="width: 3%">Jenis</th>
                    <th class="text-center" style="width: 3%">Tanggal</th>
                    <th class="text-center" style="width: 1%">Jumlah</th>
                    <th class="text-center" style="width: 1%">Lokasi</th>
                     <th class="text-center" style="width: 1%">Keterangan</th>
                    <th class="text-center" style="width: 1%">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <form action="<?php echo base_url()."admin_opd/disperkim/Adminpohon/insert_pohon/" ?>" method="post">
                      <td>
                        <select name="id_jenis" class="form-control" id="id_jenis">
                          <option value="">-- Pilih Jenis --</option>
                          <?php foreach ($list_pohon_jenis as $ltj) { ?>
                            <option value="<?php echo $ltj->id_jenis ?>"><?php echo $ltj->nama_jenis ?></option>
                          <?php } ?>
                        </select>
                      </td>
                      <td><input type="date" name="tgl" class="form-control" id="tgl"></td>
                      <td><input type="number" name="jml" class="form-control" id="jml"></td>
                      <td><input type="text" name="lokasi" class="form-control" id="lokasi"></td>
                      <td><input type="text" name="keterangan" class="form-control" id="keterangan"></td>
                      <td align="center">
                        <button type="submit" class="btn btn-success">
                          <span class="glyphicon glyphicon-plus"></span>
                        </button>
                      </td>
                    </form>
                  </tr>
                  <?php $no = 1; foreach ($list_pohon as $lt) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td align="left"><?php echo $lt->nama_jenis ?></td>
                      <td align="left"><?php echo $lt->tgl ?></td>
                      <td align="center"><?php echo $lt->jml ?></td>
                      <td align="center"><?php echo $lt->lokasi?></td>
                      <td align="center"><?php echo $lt->keterangan ?></td>
                      <td align="center">
                        <a href="#" onclick="up_lp_terminal(<?php echo $lt->id_pohon ?>)">
                          <button class="btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span>
                          </button>
                        </a>
                        <a href="#" onclick="del_lp_terminal(<?php echo $lt->id_pohon ?>)">
                          <button class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-trash"></span>
                          </button>
                        </a>                        
                      </td>
                    </tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>

      <div class="row">

        <div class="col-md-7">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Jenis Pohon</h3>
            </div>

            <div class="box-body">
              <table class="table table-bordered table-striped display" id="">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 1%">No</th>
                    <th class="text-center" style="width: 4%">Nama</th>
                    <th class="text-center" style="width: 1%">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <form action="<?php echo base_url()."/admin_opd/disperkim/Adminpohon/insert_pohon_jenis/" ?>" method="post">
                                          
                      <td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis"></td>
                      <td align="center">
                        <button type="submit" class="btn btn-success">
                          <span class="glyphicon glyphicon-plus"></span>
                        </button>
                      </td>
                    </form>
                  </tr>
                  <?php $no = 1; foreach ($list_pohon_jenis as $ltsj) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td align="left"><?php echo $ltsj->nama_jenis ?></td>
                      
                      <td align="center">
                        <a href="#" onclick="up_lp_terminal_jenis(<?php echo $ltsj->id_jenis ?>)">
                          <button class="btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span>
                          </button>
                        </a>
                        <a href="#" onclick="del_lp_terminal_jenis(<?php echo $ltsj->id_jenis ?>)">
                          <button class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-trash"></span>
                          </button>
                        </a>                        
                      </td>
                    </tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>

    </section>
  </div>

  <script type="text/javascript">
//======================================================== LP TERMINAL ===========================================//
      function up_lp_terminal(id_pohon){
        clear_mod_up_lp_terminal();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_pohon', id_pohon);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/disperkim/Adminpohon/index_up_pohon/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_lp_terminal(res);
                // $("#out_up_mhs").html(res);
            }
        });
      
        $("#modal_up_lp_terminal").modal('show');
      } 

      function clear_mod_up_lp_terminal(){
          $("#_id_jenis").val("");
          $("#_tgl").val("");
          $("#_jml").val("");
          $("#_lokasi").val("");
          $("#_keterangan").val("");
      }

      function res_update_lp_terminal(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
            
              $("#_id_pohon").attr('readonly','readonly');                                        
              $("#_id_pohon").val(main_data.id_pohon);
              $("#_id_jenis").val(main_data.id_jenis);
              $("#_tgl").val(main_data.tgl);
              $("#_jml").val(main_data.jml);
              $("#_lokasi").val(main_data.lokasi);
              $("#_keterangan").val(main_data.keterangan);
                        
          }else{
              clear_mod_up_ik();
          }
      }       

<<<<<<< HEAD
      function clear_mod_up_pohon(){
          $("#_id_pohon").val("");
          $("#_tgl").val("");
          $("#_id_jenis").val("");
          $("#_jml").val("");
          $("#_lokasi").val("");
          $("#_keterangan").val("");
      }

      // var admin_upx = "";
      function del_data_pohon(id_pohon){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_pohon+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_pohon+" akan terhapus semua...");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/disperkim/Adminpohon/delete_pohon/";?>"+id_pohon;
        }else{

        }
      }
      
      function del_lp_terminal(id_pohon){
          var conf = confirm("Apakah anda yakin untuk menghapus "+id_pohon+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_pohon+" akan terhapus semua.. !!!!!!! ");
          if(conf){
            window.location.href = "<?= base_url()."admin_opd/disperkim/Adminpohon/delete_pohon/";?>"+id_pohon;
          }else{

          }
      }         
//======================================================== LP TERMINAL ===========================================//

//======================================================== LP TERMINAL JENIS =====================================//
      function up_lp_terminal_jenis(id_jenis){
        clear_mod_up_lp_terminal_jenis();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_jenis', id_jenis);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/disperkim/Adminpohon/index_up_pohon_jenis/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_lp_terminal_jenis(res);
                // $("#out_up_mhs").html(res);
            }
        });
      
        $("#modal_up_lp_terminal_jenis").modal('show');
      } 

      function clear_mod_up_lp_terminal_jenis(){
          $("#_nama_jenis").val("");
      }

      function res_update_lp_terminal_jenis(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
            
              $("#_id_jenis2").attr('readonly','readonly');                                        
              $("#_id_jenis2").val(main_data.id_jenis);
              $("#_nama_jenis").val(main_data.nama_jenis);
            
          }else{
              clear_mod_up_ik();
          }
      }       

      function del_lp_terminal_jenis(id_jenis){
          var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
          if(conf){
            window.location.href = "<?= base_url()."admin_opd/disperkim/Adminpohon/delete_pohon_jenis/";?>"+id_jenis;
          }else{
      }

      // var admin_upx = "";
      function del_pohon_jenis(id_jenis){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua...");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/disperkim/Adminpohonjenis/delete_pohon_jenis/";?>"+id_jenis;
        }else{
        }
      }         
//======================================================== LP TERMINAL JENIS =====================================//


  </script>

  <div class="modal fade" id="modal_up_lp_terminal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Ubah Data Pohon</h4>
          </div>
          <?php echo form_open("admin_opd/disperkim/Adminpohon/update_pohon"); ?>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-12">

                          <div class="form-group">
                            <label for="exampleInputPassword1">ID Pohon</label>
                            <input type="number" name="id_pohon" id="_id_pohon" class="form-control">
                          </div>                      
                        
                          <div class="form-group">
                            <label for="exampleInputPassword1">Jenis</label>
                <select name="id_jenis" class="form-control" id="_id_jenis">
                  <option value="">-- Pilih Jenis --</option>
                  <?php foreach ($list_pohon_jenis as $ltj) { ?>
                    <option value="<?php echo $ltj->id_jenis ?>"><?php echo $ltj->nama_jenis ?></option>
                  <?php } ?>
                </select>
                          </div>                      

                          <div class="form-group">
                            <label for="exampleInputPassword1">Tanggal</label>
                            <input type="date" name="tgl" class="form-control" id="_tgl" placeholder="Tahun">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Jumlah</label>
                            <input type="number" name="jml" class="form-control" id="_jml" placeholder="Jumlah">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Lokasi</label>
                            <input type="text" name="lokasi" class="form-control" id="_lokasi" placeholder="Jumlah">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Keterangan</label>
                            <input type="text" name="keterangan" class="form-control" id="_keterangan" placeholder="Jumlah">
                          </div>

                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Ubah Data</button>
          </div>

          </form>
      </div>
    </div>
  </div>
</div>

  <div class="modal fade" id="modal_up_lp_terminal_jenis">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Ubah Data Jenis Pohon</h4>
          </div>
          <?php echo form_open("admin_opd/disperkim/Adminpohon/update_pohon_jenis"); ?>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-12">

                          <div class="form-group">
                            <label for="exampleInputPassword1">ID Jenis</label>
                            <input type="number" name="id_jenis" id="_id_jenis2" class="form-control">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Nama</label>
                            <input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Nama">
                          </div>

                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Ubah Data</button>
          </div>

          </form>
      </div>
    </div>
  </div>
</div>

</div>  
  <div class="content body" style="margin-right: 3em; margin-left: 3em;">
    <section class="content">
      <div class="row">

        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data taman</h3>
            </div>

            <div class="box-body">              
              <table class="table table-bordered table-striped display" id="">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 1%">No</th>
                    <th class="text-center" style="width: 3%">Jenis Taman</th>
                    <th class="text-center" style="width: 3%">Tahun</th>
                    <th class="text-center" style="width: 1%">Jumlah</th>
                    <th class="text-center" style="width: 1%">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <form action="<?php echo base_url()."admin_opd/disperkim/Admintaman/insert_taman/" ?>" method="post">
                      <td>
                        <select name="id_jenis" class="form-control" id="id_jenis">
                          <option value="">-- Pilih Jenis --</option>
                          <?php foreach ($list_taman_jenis as $ltj) { ?>
                            <option value="<?php echo $ltj->id_jenis ?>"><?php echo $ltj->nama_jenis ?></option>
                          <?php } ?>
                        </select>
                      </td>
                      <td><input type="number" name="th" class="form-control" id="th"></td>
                      <td><input type="number" name="jml" class="form-control" id="jml"></td>
                      
                      <td align="center">
                        <button type="submit" class="btn btn-success">
                          <span class="glyphicon glyphicon-plus"></span>
                        </button>
                      </td>
                    </form>
                  </tr>
                  <?php $no = 1; foreach ($list_taman as $lt) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td align="left"><?php echo $lt->nama_jenis ?></td>
                      <td align="left"><?php echo $lt->th ?></td>
                      <td align="center"><?php echo $lt->jml ?></td>
                      
                      <td align="center">
                        <a href="#" onclick="up_lp_terminal(<?php echo $lt->id_taman ?>)">
                          <button class="btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span>
                          </button>
                        </a>
                        <a href="#" onclick="del_lp_terminal(<?php echo $lt->id_taman ?>)">
                          <button class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-trash"></span>
                          </button>
                        </a>                        
                      </td>
                    </tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>

      <div class="row">

        <div class="col-md-7">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Jenis taman</h3>
            </div>

            <div class="box-body">
              <table class="table table-bordered table-striped display" id="">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 1%">No</th>
                    <th class="text-center" style="width: 4%">Nama</th>
                    <th class="text-center" style="width: 1%">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
<<<<<<< HEAD
                    <?php echo form_open("admin_opd/disperkim/Admintamanjenis/insert_taman_jenis"); ?>
                      <td colspan="2">
                      	<input type="text" class="form-control" id="nama_jenis" name="nama_jenis" placeholder="Nama Jenis">
=======
                    <td></td>
                    <form action="<?php echo base_url()."/admin_opd/disperkim/Admintaman/insert_taman_jenis/" ?>" method="post">
                                          
                      <td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis"></td>
                      <td align="center">
                        <button type="submit" class="btn btn-success">
                          <span class="glyphicon glyphicon-plus"></span>
                        </button>
>>>>>>> 066cf9f399d14670c154c4f5b19e69b0584f1a45
                      </td>
                    </form>
                  </tr>
                  <?php $no = 1; foreach ($list_taman_jenis as $ltsj) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td align="left"><?php echo $ltsj->nama_jenis ?></td>
                      
                      <td align="center">
                        <a href="#" onclick="up_lp_terminal_jenis(<?php echo $ltsj->id_jenis ?>)">
                          <button class="btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span>
                          </button>
                        </a>
                        <a href="#" onclick="del_lp_terminal_jenis(<?php echo $ltsj->id_jenis ?>)">
                          <button class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-trash"></span>
                          </button>
                        </a>                        
                      </td>
                    </tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>

    </section>
  </div>

  <script type="text/javascript">
//======================================================== LP TERMINAL ===========================================//
      function up_lp_terminal(id_taman){
        clear_mod_up_lp_terminal();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_taman', id_taman);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/disperkim/Admintaman/index_up_taman/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_lp_terminal(res);
                // $("#out_up_mhs").html(res);
            }
        });
      
        $("#modal_up_lp_terminal").modal('show');
      } 

      function clear_mod_up_lp_terminal(){
          $("#_id_jenis").val("");
          $("#_th").val("");
          $("#_jml").val("");
        
      }

      function res_update_lp_terminal(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
            
              $("#_id_taman").attr('readonly','readonly');                                        
              $("#_id_taman").val(main_data.id_taman);
              $("#_id_jenis").val(main_data.id_jenis);
              $("#_th").val(main_data.th);
              $("#_jml").val(main_data.jml);
              
                        
          }else{
              clear_mod_up_ik();
          }
      }       

<<<<<<< HEAD
      // var admin_upx = "";
      function delete_taman_jenis(id_jenis){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/disperkim/Admintamanjenis/delete_taman_jenis/";?>"+id_jenis;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

      function del_lp_terminal(id_taman){
          var conf = confirm("Apakah anda yakin untuk menghapus "+id_taman+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_taman+" akan terhapus semua.. !!!!!!! ");
          if(conf){
            window.location.href = "<?= base_url()."admin_opd/disperkim/Admintaman/delete_taman/";?>"+id_taman;
          }else{

          }
      }         
//======================================================== LP TERMINAL ===========================================//

//======================================================== LP TERMINAL JENIS =====================================//
      function up_lp_terminal_jenis(id_jenis){
        clear_mod_up_lp_terminal_jenis();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_jenis', id_jenis);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/disperkim/Admintaman/index_up_taman_jenis/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_lp_terminal_jenis(res);
                // $("#out_up_mhs").html(res);
            }
        });
      
        $("#modal_up_lp_terminal_jenis").modal('show');
      } 

      function clear_mod_up_lp_terminal_jenis(){
          $("#_nama_jenis").val("");
      }

      function res_update_lp_terminal_jenis(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
            
              $("#_id_jenis2").attr('readonly','readonly');                                        
              $("#_id_jenis2").val(main_data.id_jenis);
              $("#_nama_jenis").val(main_data.nama_jenis);
            
          }else{
              clear_mod_up_ik();
          }
      }       

      function del_lp_terminal_jenis(id_jenis){
          var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
          if(conf){
            window.location.href = "<?= base_url()."admin_opd/disperkim/Admintaman/delete_taman_jenis/";?>"+id_jenis;
          }else{}
        }

      // var admin_upx = "";
      function del_data_taman(id_taman){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_taman+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_taman+" akan terhapus semua...");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/disperkim/Admintaman/delete_taman/";?>"+id_taman;
        }else{
          }
      }         
//======================================================== LP TERMINAL JENIS =====================================//


  </script>

  <div class="modal fade" id="modal_up_lp_terminal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Ubah Data taman</h4>
          </div>
          <?php echo form_open("admin_opd/disperkim/Admintaman/update_taman"); ?>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-12">

                          <div class="form-group">
                            <label for="exampleInputPassword1">ID taman</label>
                            <input type="number" name="id_taman" id="_id_taman" class="form-control">
                          </div>                      
                        
                          <div class="form-group">
                            <label for="exampleInputPassword1">Jenis</label>
                <select name="id_jenis" class="form-control" id="_id_jenis">
                  <option value="">-- Pilih Jenis --</option>
                  <?php foreach ($list_taman_jenis as $ltj) { ?>
                    <option value="<?php echo $ltj->id_jenis ?>"><?php echo $ltj->nama_jenis ?></option>
                  <?php } ?>
                </select>
                          </div>                      

                          <div class="form-group">
                            <label for="exampleInputPassword1">Tanggal</label>
                            <input type="number" name="th" class="form-control" id="_th" placeholder="Tahun">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Jumlah</label>
                            <input type="number" name="jml" class="form-control" id="_jml" placeholder="Jumlah">
                          </div>

                         

                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Ubah Data</button>
          </div>

          </form>
      </div>
    </div>
  </div>
</div>

  <div class="modal fade" id="modal_up_lp_terminal_jenis">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Ubah Data Jenis taman</h4>
          </div>
          <?php echo form_open("admin_opd/disperkim/Admintaman/update_taman_jenis"); ?>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-12">

                          <div class="form-group">
                            <label for="exampleInputPassword1">ID Jenis</label>
                            <input type="number" name="id_jenis" id="_id_jenis2" class="form-control">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Nama</label>
                            <input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Nama">
                          </div>

                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Ubah Data</button>
          </div>

          </form>
      </div>
    </div>
  </div>
</div>

</div>  
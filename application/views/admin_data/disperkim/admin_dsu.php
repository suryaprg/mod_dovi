 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data DSU </h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/disperkim/admindsu/insert_dsu"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Kawasan</label>
                     <input type="text" class="form-control" id="nama_kawasan" name="nama_kawasan" placeholder="Admin DSU" required="">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Lokasi</label>
                      <input type="text" class="form-control" id="lokasi" name="lokasi" placeholder="Lokasi" required="">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Pengembang</label>
                      <input type="text" class="form-control" id="pengembang" name="pengembang" placeholder="Pengembang" required="">
                    </div>
                  </div>
                   <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tanggal</label>
                      <input type="date" class="form-control" id="tgl" name="tgl" placeholder="Tanggal" required="">
                    </div>
                  </div>

                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data DSU</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Kawasan</th>
                  <th>Lokasi</th>
                  <th>Pengembang</th>
                  <th>Tanggal</th>
                  
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_dsu){
                      $no = 1;
                      foreach ($list_dsu as $r_list_dsu => $v_list_dsu) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_dsu->nama_kawasan."</td>
                                <td>".$v_list_dsu->lokasi."</td>
                                <td>".$v_list_dsu->pengembang."</td>
                                <td>".$v_list_dsu->tgl."</td>
                                
                               
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_dsu('".$v_list_dsu->id_dsu."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_dsu('".$v_list_dsu->id_dsu."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->

        

      </div>
      <!-- /.row -->      
    </div>

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->





    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Iklim---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        

        <div class="modal fade" id="modal_up_dsu">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data DSU</h4>
              </div>
              <?php echo form_open("admin_opd/disperkim/admindsu/up_dsu"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Nama Kawasan</label>
                                <input type="text" class="form-control" id="_nama_kawasan" name="nama_kawasan" placeholder="Nama Kawasan" required="">
                              </div>
                            </div>
                            
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Lokasi</label>
                                <input type="text" class="form-control" id="_lokasi" name="lokasi" placeholder="Lokasi" required="">
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Pengembang</label>
                                <input type="text" class="form-control" id="_pengembang" name="pengembang" placeholder="Keterangan" required="">
                              </div>
                            </div>


                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Tanggal</label>
                                <input type="date" class="form-control" id="_tgl" name="tgl" placeholder="Tanggal" required="">
                              </div>
                            </div>
                            <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_dsu</label>
                      <input type="text" class="form-control" id="_id_dsu" name="id_dsu" readonly="" required="">
                    </div>
                  </div>
                
                            </div>
                              </div>
                            </div>
                          </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Iklim---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

    <script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Stasiun---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_dsu(id_dsu){
        clear_mod_up_dsu();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_dsu', id_dsu);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/disperkim/Admindsu/index_up_dsu/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_dsu(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_dsu").modal('show');
      }

      function res_update_dsu(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_nama_kawasan").val(main_data.nama_kawasan);
              $("#_lokasi").val(main_data.lokasi);
              $("#_pengembang").val(main_data.pengembang);
              $("#_tgl").val(main_data.tgl);
              $("#_id_dsu").val(main_data.id_dsu);
              
          }else{
              clear_mod_up_dsu();
          }
      }

      function clear_mod_up_dsu(){
         $("#_nama_kawasan").val("");
         $("#_lokasi").val("");
         $("#_pengembang").val("");
         $("#_tgl").val("");
         $("#_id_dsu").val("");
         
          
      }

      // var admin_upx = "";
      function del_data_dsu(id_dsu){

        var conf = confirm("Apakah anda yakin untuk menghapus "+id_dsu+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_dsu+" akan terhapus semua.. !!!!!!! ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/disperkim/Admindsu/delete_dsu/";?>"+id_dsu;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Stasiun---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    
    
      

    </script>
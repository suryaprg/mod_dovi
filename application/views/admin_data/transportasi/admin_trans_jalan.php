 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data Panjang Jalan Menurut Kondisi Jalan</h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/transportasi/Admintransjalan/insert"); ?>
              <div class="box-body">
                <div class="row">

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Keterangan Jalan</label>
                      <select id="kategori_jln" name="kategori_jln" class="form-control">
                        <option value="Kondisi Jalan Baik">Kondisi Jalan Baik</option>
                        <option value="Kondisi Jalan Sedang">Kondisi Jalan Sedang</option>
                        <option value="Kondisi Jalan Rusak">Kondisi Jalan Rusak</option>
                        <option value="Kondisi Jalan Rusak Berat">Kondisi Jalan Rusak Berat</option>   
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Panjang Jalan Negara</label>
                      <input type="text" class="form-control" id="jln_negara" name="jln_negara" placeholder="Panjang Jalan Negara" required="">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Panjang Jalan Provinsi</label>
                      <input type="text" class="form-control" id="jln_prov" name="jln_prov" placeholder="Panjang Jalan Provinsi" required="">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Panjang Jalan Kota</label>
                      <input type="text" class="form-control" id="jln_kota" name="jln_kota" placeholder="Panjang Jalan Kota" required="">
                    </div>
                  </div>
                  
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Jumlah Panjang Jalan Menurut Kondisi Jalan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Keterangan Jalan</th>
                  <th>Periode (Tahun)</th>
                  <th>Panjang Jalan Negara</th>
                  <th>Panjang Jalan Provinsi</th>
                  <th>Panjang Jalan Kota</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_jalan){
                      $no = 1;
                      foreach ($list_jalan as $r_list_jalan => $v_list_jalan) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_jalan->kategori_jln."</td>
                                <td>".$v_list_jalan->th."</td>
                                <td>".$v_list_jalan->jln_negara."</td>
                                <td>".$v_list_jalan->jln_prov."</td>
                                <td>".$v_list_jalan->jln_kota."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data('".$v_list_jalan->id_jln."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data('".$v_list_jalan->id_jln."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->       
      </div>
      <!-- /.row -->      
    </div>

    

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        

        <div class="modal fade" id="modal_up">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Panjang Jalan Menurut Kondisi Jalan</h4>
              </div>
              <?php echo form_open("admin_opd/transportasi/Admintransjalan/update"); ?>
              <div class="modal-body">
                <div class="row">
                  
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Keterangan Jalan</label>
                      <select id="_kategori_jln" name="kategori_jln" class="form-control">
                        <option value="Kondisi Jalan Baik">Kondisi Jalan Baik</option>
                        <option value="Kondisi Jalan Sedang">Kondisi Jalan Sedang</option>
                        <option value="Kondisi Jalan Rusak">Kondisi Jalan Rusak</option>
                        <option value="Kondisi Jalan Rusak Berat">Kondisi Jalan Rusak Berat</option>   
                      </select>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="_th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Panjang Jalan Negara</label>
                      <input type="text" class="form-control" id="_jln_negara" name="jln_negara" placeholder="Panjang Jalan Negara" required="">
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Panjang Jalan Provinsi</label>
                      <input type="text" class="form-control" id="_jln_prov" name="jln_prov" placeholder="Panjang Jalan Provinsi" required="">
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Panjang Jalan Kota</label>
                      <input type="text" class="form-control" id="_jln_kota" name="jln_kota" placeholder="Panjang Jalan Kota" required="">
                    </div>
                  </div>
                  
                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_jln</label>
                      <input type="text" class="form-control" id="_id_jln" name="id_jln" readonly="" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

<script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data(id_jln){
        clear_mod_up();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_jln', id_jln);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/transportasi/Admintransjalan/index_up";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up").modal('show');
      }

      function res_update(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_jln").val(main_data.id_jln);
              $("#_kategori_jln").val(main_data.kategori_jln);
              $("#_th").val(main_data.th);
              $("#_jln_negara").val(main_data.jln_negara);
              $("#_jln_prov").val(main_data.jln_prov);
              $("#_jln_kota").val(main_data.jln_kota);
              
          }else{
              clear_mod_up();
          }
      }

      function clear_mod_up(){
          $("#_id_jln").val("");
          $("#_kategori_jln").val("");
          $("#_th").val("");
          $("#_jln_negara").val("");
          $("#_jln_prov").val("");
          $("#_jln_kota").val("");
         
      }

      // var admin_upx = "";
      function del_data(id_jln){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_jln+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jln+" akan terhapus semau.. !!!!!!! ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/transportasi/Admintransjalan/delete/";?>"+id_jln;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


      

    </script>
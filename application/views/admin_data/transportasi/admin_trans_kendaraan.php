 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->

        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Jumlah Kendaraan </h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/transportasi/Admintranskendaraan/insert"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Kendaraan</label>
                      <select name="id_jenis_kendaraan" id="id_jenis_kendaraan" class="form-control">

                        <?php
                          if($list_kendaraan_jenis){
                            $no = 1;
                            foreach ($list_kendaraan_jenis as $r_list_kendaraan_jenis => $v_list_kendaraan_jenis) {
                              echo "<option value=\"".$v_list_kendaraan_jenis->id_jenis_kendaraan."\">".$v_list_kendaraan_jenis->keterangan."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Total Kendaraan</label>
                      <input type="number" class="form-control" id="jml" name="jml" placeholder="Total Kendaraan" required="">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        <!-- </div> -->
        <!--/.col (left) -->

        

      <!-- </div> -->
      
      <!-- <div class="row"> -->
        <!-- left column -->
        <!-- <div class="col-md-8"> -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kec"><i class="fa fa-plus"></i>&nbsp;&nbsp; Tambah Data Keuangan Daerah</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped display">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Kendaraan</th>
                  <th>Tahun</th>
                  <th>Total Kendaraan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  if($list_jml_kendaraan){
                      $no = 1;
                      foreach ($list_jml_kendaraan as $r_list_jml_kendaraan => $v_list_jml_kendaraan) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_jml_kendaraan->keterangan."</td>
                                <td>".$v_list_jml_kendaraan->th."</td>
                                <td>".$v_list_jml_kendaraan->jml."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_ken('".$v_list_jml_kendaraan->id_jml_kendaraan."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_ken('".$v_list_jml_kendaraan->id_jml_kendaraan."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                    $no++;
                    }

                  }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <div class="col-md-4">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Jenis Kendaraan</h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kel"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data Jenis Keuangan Daerah</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Kendaraan</th>
                  <th>Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php echo form_open("admin_opd/transportasi/Admintranskendaraanjenis/insert"); ?>
                      <td colspan="2"><input type="text" class="form-control" id="keterangan" name="keterangan"></td>
                      <td><button type="submit" class="btn btn-success pull-right" id="add_jenis_keu" name="add_jenis_keu"><i class="fa fa-plus"></i>&nbsp;&nbsp; Simpan</button></td>
                    </form>
                  </tr>
                  <?php
                    if($list_kendaraan_jenis){
                      $no = 1;
                      foreach ($list_kendaraan_jenis as $r_list_kendaraan_jenis => $v_list_kendaraan_jenis) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_kendaraan_jenis->keterangan."</td>
                               
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_jenis('".$v_list_kendaraan_jenis->id_jenis_kendaraan."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_jenis('".$v_list_kendaraan_jenis->id_jenis_kendaraan."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                    
                  ?>
                  
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row --> 
      
    </div>

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


        <div class="modal fade" id="modal_up_jenis">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Jenis Keuangan</h4>
              </div>
              <?php echo form_open("admin_opd/transportasi/Admintranskendaraanjenis/update"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Keterangan</label>
                              <input type="text" class="form-control" id="_keterangan" name="keterangan" placeholder="Keterangan" required="">
                            </div>
                          </div>
                          <div class="col-md-12" hidden="">
                            <div class="form-group">
                              <label for="exampleInputPassword1">id jenis</label>
                              <input type="text" class="form-control" id="_id_jenis_kendaraan" name="id_jenis_kendaraan" placeholder="Jumlah RW" required="" readonly="">
                            </div>
                          </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Keuangan Daerah---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        <div class="modal fade" id="modal_up_ken">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Keuangan Daerah</h4>
              </div>
              <?php echo form_open("admin_opd/transportasi/Admintranskendaraan/update/"); ?>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Kendaraan</label>
                      <select name="id_jenis_kendaraan" id="_id_jenis_kendaraan_ex" class="form-control">

                        <?php
                          if($list_kendaraan_jenis){
                            $no = 1;
                            foreach ($list_kendaraan_jenis as $r_list_kendaraan_jenis => $v_list_kendaraan_jenis) {
                              echo "<option value=\"".$v_list_kendaraan_jenis->id_jenis_kendaraan."\">".$v_list_kendaraan_jenis->keterangan."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="number" class="form-control" id="_th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Total Kendaraan</label>
                      <input type="number" class="form-control" id="_jml" name="jml" placeholder="Total Kendaraan" required="">
                    </div>
                  </div>

                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_jml_kendaraan</label>
                      <input type="number" class="form-control" id="_id_jml_kendaraan" name="id_jml_kendaraan" readonly="" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Keuangan Daerah---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    


    <script type="text/javascript">


      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_jenis(id_jenis_kendaraan){
        clear_mod_up_jenis();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_jenis_kendaraan', id_jenis_kendaraan);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/transportasi/Admintranskendaraanjenis/index_up";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_jenis(res);
            }
        });
        
        $("#modal_up_jenis").modal('show');
      }

      function res_update_jenis(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_keterangan").val(main_data.keterangan);
              $("#_id_jenis_kendaraan").val(main_data.id_jenis_kendaraan);
              
              
          }else{
              clear_mod_up_jenis();
          }
      }

      function clear_mod_up_jenis(){
          $("#_keterangan").val("");
          $("#_id_jenis_kendaraan").val("");
         
      }

      // var admin_upx = "";
      function del_data_jenis(id_jenis_kendaraan){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis_kendaraan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis_kendaraan+" akan terhapus semau.. !!!!!!! ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/transportasi/Admintranskendaraanjenis/delete/";?>"+id_jenis_kendaraan;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Keuangan Daerah---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_ken(id_jml_kendaraan){
        clear_mod_up_ken();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_jml_kendaraan', id_jml_kendaraan);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/transportasi/Admintranskendaraan/index_up/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_ken(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_ken").modal('show');
      }

      function res_update_ken(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_jml_kendaraan").val(main_data.id_jml_kendaraan);
              $("#_id_jenis_kendaraan_ex").val(main_data.id_jenis_kendaraan);
              $("#_th").val(main_data.th);
              $("#_jml").val(main_data.jml);
              
          }else{
              clear_mod_up_ken();
          }
      }

      function clear_mod_up_ken(){
          $("#_id_jml_kendaraan").val("");
          $("#_id_jenis_kendaraan_ex").val("");
          $("#_th").val("");
          $("#_jml").val("");
      }

      // var admin_upx = "";
      function del_data_ken(id_jml_kendaraan){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_jml_kendaraan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jml_kendaraan+" akan terhapus semau.. !!!!!!! ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/transportasi/Admintranskendaraan/delete/";?>"+id_jml_kendaraan;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Keuangan Daerah---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    
      

    </script>
 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->

        <div class="col-md-7">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Perdagangan</h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/Perdagangan/Adminperdagangan/insert"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Perdagangan</label>
                      <select name="id_jenis" id="id_jenis" class="form-control">

                        <?php
                          if($list_perdagangan_jenis){
                            $no = 1;
                            foreach ($list_perdagangan_jenis as $r_data => $v_data) {
                              echo "<option value=\"".$v_data->id_jenis."\">".$v_data->nama_jenis."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Sub Jenis Perdagangan</label>
                      <select name="id_sub_jenis" id="id_sub_jenis" class="form-control">

                        <?php
                          if($list_perdagangan_sub_jenis){
                            $no = 1;
                            foreach ($list_perdagangan_sub_jenis as $r_data => $v_data) {
                              echo "<option value=\"".$v_data->id_sub_jenis."\">".$v_data->nama_sub_jenis."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah</label>
                      <input type="text" class="form-control" id="jml" name="jml" placeholder="Jumlah" required="">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        <!-- </div> -->
        <!--/.col (left) -->

        

      <!-- </div> -->
      
      <!-- <div class="row"> -->
        <!-- left column -->
        <!-- <div class="col-md-8"> -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kec"><i class="fa fa-plus"></i>&nbsp;&nbsp; Tambah Data Jumlah Panen Komoditi (Ton)</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Perdagangan</th>
                  <th>Sub Jenis Perdagangan</th>
                  <th>Tahun</th>
                  <th>Jumlah</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  if($list_perdagangan){
                      $no = 1;

                      foreach ($list_perdagangan as $r_data => $v_data) {
                      	
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_data->nama_jenis."</td>
                                <td>".$v_data->id_sub_jenis."</td>
                                <td>".$v_data->th."</td>
                                <td>".$v_data->jml."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_perdagangan('".$v_data->id_perdagangan."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_perdagangan('".$v_data->id_perdagangan."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                    $no++;
                    }

                  }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <div class="col-md-5">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Jenis Perdagangan</h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kel"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data Jenis Keuangan Daerah</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Jenis</th>
                  <th>Unit</th> 
                  <th>Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php echo form_open("admin_opd/perdagangan/Adminperdaganganjenis/insert"); ?>
                      <td colspan="2">
                      	<input type="text" class="form-control" id="nama_jenis" name="nama_jenis" placeholder="Nama Jenis">
                      </td>
                       <td colspan="1">
                        <input type="text" class="form-control" id="unit" name="unit" placeholder="Unit">
                      </td>
                      
                      <td><button type="submit" class="btn btn-success pull-right" id="add_jenis_per" name="add_jenis_per"><i class="fa fa-plus"></i>&nbsp;&nbsp; Simpan</button></td>
                    </form>
                  </tr>
                  <?php
                    if($list_perdagangan_jenis){
                      $no = 1;
                      foreach ($list_perdagangan_jenis as $r_data => $v_data) {
                      	
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_data->nama_jenis."</td>
                                <td>".$v_data->unit."</td>
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_jenis('".$v_data->id_jenis."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_jenis('".$v_data->id_jenis."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                    
                  ?>
                  
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-5">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Sub Jenis Perdagangan</h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kel"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data Jenis Keuangan Daerah</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Sub Jenis</th> 
                  <th>Nama Jenis</th>
                  <th>Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php echo form_open("admin_opd/perdagangan/Adminperdagangansubjenis/insert"); ?>
                      <td colspan="2">
                        <input type="text" class="form-control" id="nama_sub_jenis" name="nama_sub_jenis">
                      </td>
                      <td colspan="1">
                        <input type="text" class="form-control" id="id_jenis" name="id_jenis">
                      </td>
                      
                      <td><button type="submit" class="btn btn-success pull-right" id="add_jenis_per" name="add_jenis_per"><i class="fa fa-plus"></i>&nbsp;&nbsp; Simpan</button></td>
                    </form>
                  </tr>
                  <?php
                    if($list_perdagangan_sub_jenis){
                      $no = 1;
                      foreach ($list_perdagangan_sub_jenis as $r_data => $v_data) {
                        
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_data->nama_sub_jenis."</td>
                                 <td>".$v_data->nama_jenis."</td>
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_jenis('".$v_data->id_jenis."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_jenis('".$v_data->id_jenis."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                    
                  ?>
                  
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row --> 
      
      
    </div>
      </div>
      <!-- /.row --> 

      
    </div>

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->



        <div class="modal fade" id="modal_up_jenis">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Jenis Perdagangan</h4>
              </div>
              <?php echo form_open("admin_opd/perdagangan/Adminperdaganganjenis/update"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Nama Jenis</label>
                              <input type="text" class="form-control" id="_nama_jenis" name="nama_jenis" placeholder="Nama Jenis" required="">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="exampleInputPassword1">unit</label>
                              <input type="text" class="form-control" id="_unit" name="unit" placeholder="Unit" required="">
                            </div>
                          </div>
                          
                          <div class="col-md-12" hidden="">
                            <div class="form-group">
                              <label for="exampleInputPassword1">id jenis</label>
                              <input type="text" class="form-control" id="_id_jenis" name="id_jenis" placeholder="ID Jenis" required="" readonly="">
                            </div>
                          </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------- Komoditi ---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        <div class="modal fade" id="modal_up_perdagangan">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Perdagangan</h4>
              </div>
              <?php echo form_open("admin_opd/perdagangan/Adminperdagangan/update/"); ?>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Perdagangan</label>
                      <select name="id_jenis" id="_id_jenis" class="form-control">

                        <?php
                          if($list_perdagangan_jenis){
                            $no = 1;
                            foreach ($list_perdagangan_jenis as $r_data => $v_data) {
                              echo "<option value=\"".$v_data->id_jenis."\">".$v_data->nama_jenis."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Sub Jenis Perdagangan</label>
                      <select name="id_sub_jenis" id="_id_sub_jenis" class="form-control">

                        <?php
                          if($list_perdagangan_sub_jenis){
                            $no = 1;
                            foreach ($list_perdagangan_sub_jenis as $r_data => $v_data) {
                              echo "<option value=\"".$v_data->id_sub_jenis."\">".$v_data->nama_sub_jenis."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="number" class="form-control" id="_th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah</label>
                      <input type="text" class="form-control" id="_jml" name="jml" placeholder="Jumlah" required="">
                    </div>
                  </div>

                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_perdagangan</label>
                      <input type="text" class="form-control" id="_id_perdagangan" name="id_perdagangan" readonly="" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------- Komoditi ---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    


    <script type="text/javascript">


      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_jenis(id_jenis){
        clear_mod_up_jenis();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_jenis', id_jenis);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/perdagangan/Adminperdaganganjenis/index_up";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_jenis(res);
            }
        });
        
        $("#modal_up_jenis").modal('show');
      }

      function res_update_jenis(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_jenis").val(main_data.id_jenis);
              $("#_nama_jenis").val(main_data.nama_jenis);
              $("#_unit").val(main_data.unit);
              
              
          }else{
              clear_mod_up_jenis();
          }
      }

      function clear_mod_up_jenis(){
          $("#_id_jenis").val("");
          $("#_nama_jenis").val("");
          $("#_unit").val("");
         
      }

      // var admin_upx = "";
      function del_data_jenis(id_jenis){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/perdagangan/Adminperdaganganjenis/delete/";?>"+id_jenis;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Komoditi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_perdagangan(id_perdagangan){
        clear_mod_up_perdagangan();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_perdagangan', id_perdagangan);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/perdagangan/Adminperdagangan/index_up/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_perdagangan(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_perdagangan").modal('show');
      }

      function res_update_perdagangan(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);

              $("#_id_jenis").val(main_data.id_jenis);
              $("#_id_sub_jenis").val(main_data.id_sub_jenis);
              $("#_th").val(main_data.th);
              $("#_jml").val(main_data.jml);
              $("#_id_perdagangan").val(main_data.id_perdagangan);
              
          }else{
              clear_mod_up_perdagangan();
          }
      }

      function clear_mod_up_perdagangan(){
          
          $("#_id_jenis").val("");
          $("#_id_sub_jenis").val("");
          $("#_th").val("");
          $("#_jml").val("");
          $("#_id_perdagangan").val("");
      }

      // var admin_upx = "";
      function del_data_perdagangan(id_perdagangan){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_perdagangan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_perdagangan+" akan terhapus semua...");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/perdagangan/Adminperdagangan/delete/";?>"+id_perdagangan;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Komoditi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    
      

    </script>
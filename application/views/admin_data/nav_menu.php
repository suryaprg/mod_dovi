<!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">KATEGORI</li>

        <li class="treeview">
          <a href="#">
             <span>Admin</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."super/admin";?>"><i class="fa fa-circle-o"></i>Data Admin</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
             <span>Partai</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."super/partai";?>"><i class="fa fa-circle-o"></i>Data Partai</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
             <span>Kecamatan Kelurahan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."super/kelkec";?>"><i class="fa fa-circle-o"></i>Data Kecamatan dan Kelurahan</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
             <span>Geografi</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/iklim";?>"><i class="fa fa-circle-o"></i>Data Iklim dan Curah Hujan</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Pemerintah</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/rt_rw";?>"><i class="fa fa-circle-o"></i>RT dan RW</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Ekonomi</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/keuangan_daerah";?>"><i class="fa fa-circle-o"></i>keuangan_daerah</a></li>
            <li><a href="<?=base_url()."admin/pendapatan_regional";?>"><i class="fa fa-circle-o"></i>pendapatan_regional</a></li>
            <li><a href="<?=base_url()."admin/kegiatan_usaha";?>"><i class="fa fa-circle-o"></i>kegiatan_usaha</a></li>
            <li><a href="<?=base_url()."admin/pengeluaran_penduduk";?>"><i class="fa fa-circle-o"></i>pengeluaran_penduduk</a></li>
            <li><a href="<?=base_url()."admin/pengeluaran_rumah_tangga";?>"><i class="fa fa-circle-o"></i>pengeluaran_rumah_tangga</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Kependudukan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/kependudukan_jk";?>"><i class="fa fa-circle-o"></i>Berdasarkan Jenis Kelamin</a></li>
            <li><a href="<?=base_url()."admin/kependudukan_umur";?>"><i class="fa fa-circle-o"></i>Berdasarkan Umur</a></li>
            <li><a href="<?=base_url()."admin/kependudukan_ketergantungan";?>"><i class="fa fa-circle-o"></i>Index Ketergantungan</a></li>
            <li><a href="<?=base_url()."admin/kependudukan_ipm";?>"><i class="fa fa-circle-o"></i>IPM</a></li>
            <li><a href="<?=base_url()."admin/kependudukan_ipm_bid";?>"><i class="fa fa-circle-o"></i>Pengembangan IPM</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Ketenaga Kerjaan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/kerja_angkatan";?>"><i class="fa fa-circle-o"></i>Angkatan Kerja</a></li>
            <li><a href="<?=base_url()."admin/kerja_pengangguran";?>"><i class="fa fa-circle-o"></i>Pengangguran</a></li>
            <li><a href="<?=base_url()."admin/kerja_ump";?>"><i class="fa fa-circle-o"></i>Upah</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Tehnologi</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/tehno_bts";?>"><i class="fa fa-circle-o"></i>BTS</a></li>
            <li><a href="<?=base_url()."admin/tehno_warnet";?>"><i class="fa fa-circle-o"></i>Warnet</a></li>
            <li><a href="<?=base_url()."admin/tehno_web";?>"><i class="fa fa-circle-o"></i>Website OPD</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Kesehatan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/kesehatan_gizi";?>"><i class="fa fa-circle-o"></i>Gizi Balita</a></li>
            <li><a href="<?=base_url()."admin/kesehatan_penyakit";?>"><i class="fa fa-circle-o"></i>Penyakit</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Transportasi</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/trans_kendaraan";?>"><i class="fa fa-circle-o"></i>Kendaraan</a></li>
            <li><a href="<?=base_url()."admin/trans_jalan";?>"><i class="fa fa-circle-o"></i>Jalan</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Pertanian</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/pertanian_lahan";?>"><i class="fa fa-circle-o"></i>Lahan Berdasarkan Jenis</a></li>
            <li><a href="<?=base_url()."admin/pertanian_lahan_penggunaan";?>"><i class="fa fa-circle-o"></i>Lahan Berdasarkan Penggunaan</a></li>
            <li><a href="<?=base_url()."admin/kelapa_tebu";?>"><i class="fa fa-circle-o"></i>Kelapa dan Tebu</a></li>
            <li><a href="<?=base_url()."admin/pertanian_komoditi";?>"><i class="fa fa-circle-o"></i>Komoditi</a></li>
            <li><a href="<?=base_url()."admin/pertanian_hortikultura";?>"><i class="fa fa-circle-o"></i>Hortikultura</a></li>
            <li><a href="<?=base_url()."admin/peternakan";?>"><i class="fa fa-circle-o"></i>Peternakan</a></li>
            <li><a href="<?=base_url()."admin/perikanan";?>"><i class="fa fa-circle-o"></i>Perikanan</a></li>

          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Pendidikan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/pendidikan_baca_tulis";?>"><i class="fa fa-circle-o"></i>Baca dan Tulis</a></li>
            <li><a href="<?=base_url()."admin/pendidikan_partisipasi_sklh";?>"><i class="fa fa-circle-o"></i>Partisipasi Sekolah</a></li>
            <li><a href="<?=base_url()."admin/pendidikan_penduduk";?>"><i class="fa fa-circle-o"></i>Pendidikan Penduduk</a></li>
            <li><a href="<?=base_url()."admin/jumlah_sekolah";?>"><i class="fa fa-circle-o"></i>Jumlah Sekolah</a></li>
            <li><a href="<?=base_url()."admin/rasio_guru_murid";?>"><i class="fa fa-circle-o"></i>Rasio Guru dan Murid</a></li>

          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Kemiskinan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/penduduk_miskin";?>"><i class="fa fa-circle-o"></i>Jumlah Penduduk Miskin</a></li>
            <li><a href="<?=base_url()."admin/garis_miskin";?>"><i class="fa fa-circle-o"></i>Garis Kemiskinan</a></li>
          </ul>
        </li>
      
        
    </ul>

<!-- 
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li>
        <li>
          <a href="pages/widgets.html">
            <i class="fa fa-th"></i> <span>Widgets</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Charts</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
            <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>UI Elements</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
            <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
            <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
            <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
            <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
            <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Forms</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
            <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
            <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Tables</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
          </ul>
        </li>
        <li>
          <a href="pages/calendar.html">
            <i class="fa fa-calendar"></i> <span>Calendar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>
        <li>
          <a href="pages/mailbox/mailbox.html">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Examples</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
            <li><a href="pages/examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
            <li><a href="pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
            <li><a href="pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
            <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
            <li><a href="pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
            <li><a href="pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
            <li><a href="pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
            <li><a href="pages/examples/pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>
        <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul> -->
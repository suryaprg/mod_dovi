 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data Aplikasi </h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/diskominfo/Adminaplikasi/insert_aplikasi"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Aplikasi</label>
                     <input type="text" class="form-control" id="aplikasi" name="aplikasi" placeholder="Aplikasi" required="">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Fungsi</label>
                      <input type="text" class="form-control" id="funsgi" name="fungsi" placeholder="Fungsi" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Keterangan</label>
                      <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" required="">
                    </div>
                  </div>

                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan Data</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Aplikasi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Aplikasi</th>
                  <th>Fungsi</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_aplikasi){
                      $no = 1;
                      foreach ($list_aplikasi as $r_list_aplikasi => $v_list_aplikasi) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_aplikasi->aplikasi."</td>
                                <td>".$v_list_aplikasi->fungsi."</td>
                                <td>".$v_list_aplikasi->keterangan."</td>
                               
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_aplikasi('".$v_list_aplikasi->id_aplikasi."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_aplikasi('".$v_list_aplikasi->id_aplikasi."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->

        

      </div>
      <!-- /.row -->      
    </div>

    

        <div class="modal fade" id="modal_up_aplikasi">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Aplikasi</h4>
              </div>
              <?php echo form_open("/admin_opd/diskominfo/Adminaplikasi/up_aplikasi"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Aplikasi</label>
                                <input type="text" class="form-control" id="_aplikasi" name="aplikasi" placeholder="Aplikasi" required="">
                              </div>
                            </div>
                            
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Fungsi</label>
                                <input type="text" class="form-control" id="_fungsi" name="fungsi" placeholder="Fungsi" required="">
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Keterangan</label>
                                <input type="text" class="form-control" id="_keterangan" name="keterangan" placeholder="Keterangan" required="">
                              </div>
                            </div>


                           <!--  <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Id</label>
                                <input type="text" class="form-control" id="_id_aplikasi" name="id_aplikasi" placeholder="Aplikasi" required="">
                              </div>
                            </div> -->
                          </div>

                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Iklim---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

    <script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Stasiun---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_aplikasi(id_aplikasi){
        clear_mod_up_aplikasi();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_aplikasi', id_aplikasi);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/diskominfo/Adminaplikasi/index_up_aplikasi/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_st(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_aplikasi").modal('show');
      }

      function res_update_aplikasi(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#up_aplikasi").val(main_data.aplikasi);
              $("#up_fungsi").val(main_data.fungsi);
              $("#up_keterangan").val(main_data.keterangan);
              $("#up_id_aplikasi").val(main_data.id_aplikasi);
              
          }else{
              clear_mod_up_aplikasi();
          }
      }

      function clear_mod_up_aplikasi(){
          $("#up_aplikasi").val("");
          $("#up_fungsi").val("");
          $("#up_keterangan").val("");
          $("#up_id_aplikasi").val("");
          
      }

      // var admin_upx = "";
      function del_data_aplikasi(id_aplikasi){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_aplikasi+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_aplikasi+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/diskominfo/Adminaplikasi/delete_aplikasi/";?>"+id_aplikasi;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Stasiun---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Iklim---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_aplikasi(id_aplikasi){
        clear_mod_up_aplikasi();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_aplikasi', id_aplikasi);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/diskominfo/Adminaplikasi/index_up_aplikasi/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_ik(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_aplikasi").modal('show');
      }

      function res_update_aplikasi(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_aplikasi").val(main_data.id_aplikasi);
              $("#_aplikasi").val(main_data.aplikasi);
              $("#_fungsi").val(main_data.fungsi);
              $("#_keterangan").val(main_data.keterangan);

              // console.log(main_data.id_kec);
              $("#_id_aplikasi").val(main_data.id_aplikasi);
              
          }else{
              clear_mod_up_ik();
          }
      }

      function clear_mod_up_aplikasi(){
          $("#_aplikasi").val("");
          $("#_fungsi").val("");
          $("#_keterangan").val("");
          $("#_id_aplikasi").val("");
      }

      // var admin_upx = "";
      function del_data_aplikasi(id_aplikasi){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_aplikasi+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_aplikasi+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/diskominfo/Adminaplikasi/delete_aplikasi/";?>"+id_aplikasi;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Iklim---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    
      

    </script>
 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data Domain</h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/diskominfo/Admindomain/insert_domain"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama SKPD</label>
                       <input type="text" class="form-control" id="nama_skpd" name="nama_skpd" placeholder="Nama SKPD" required="">
                    </div>
                  </div>
                 
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Sub Domain</label>
                      <input type="text" class="form-control" id="subdomain" name="subdomain" placeholder="Sub Domain" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>

                  
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan Data</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Domain</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama SKPD</th> 
                  <th>Sub Domain</th>
                  <th>Tahun</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_domain){
                      $no = 1;
                      foreach ($list_domain as $r_list_domain => $v_list_domain) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_domain->nama_skpd."</td>
                                <td>".$v_list_domain->subdomain."</td>
                                <td>".$v_list_domain->th."</td>
                               
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_domain('".$v_list_domain->id_aplikasi."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_domain('".$v_list_domain->id_aplikasi."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->

      </div>
      <!-- /.row -->      
    </div>

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
  

        <div class="modal fade" id="modal_up_domain">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Domain</h4>
              </div>
              <?php echo form_open("admin_opd/diskominfo/Admindomain/up_domain"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          
                          <div class="box-body">
                          <div class="form-group">
                            <label for="exampleInputPassword1">Nama SKPD</label>
                            <input type="text" class="form-control" id="_nama_skpd" name="nama_skpd" placeholder="Nama SKPD" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Sub Domain</label>
                            <input type="text" class="form-control" id="_subdomain" name="subdomain" placeholder="Sub Domain" required="">
                          </div>
                        </div>


                          <div class="form-group">
                            <label for="exampleInputPassword1">Tahun</label>
                            <input type="text" class="form-control" id="_th" name="th" placeholder="Tahun" required="">
                          </div>
                        </div>

                          <div class="form-group" hidden="">
                            <label for="exampleInputPassword1">Id aplikasi</label>
                            <input type="text" class="form-control" id="_id_aplikasi" name="id_aplikasi" readonly="" required="">
                          </div>

                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </div>
              
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------stasiun---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

    <script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Stasiun---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_domain(id_aplikasi){
        clear_mod_up_domain();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_aplikasi', id_aplikasi);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/diskominfo/Admindomain/index_up_domain/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_domain(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_domain").modal('show');
      }

      function res_update_domain(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_nama_skpd").val(main_data.nama_skpd);
              $("#_subdomain").val(main_data.subdomain);
              $("#_th").val(main_data.th);
              $("#_id_aplikasi").val(main_data.id_aplikasi);
              
          }else{
              clear_mod_up_domain();
          }
      }

      function clear_mod_up_domain(){
          $("#_nama_skpd").val("");
          $("#_subdomain").val("");
          $("#_th").val("");
            
      }

      // var admin_upx = "";
    function del_data_domain(id_aplikasi){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_aplikasi+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_aplikasi+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/diskominfo/Admindomain/delete_domain/";?>"+id_aplikasi;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Stasiun---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->  
      
    </script>
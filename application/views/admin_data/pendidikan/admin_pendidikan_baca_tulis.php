 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->

        <div class="col-md-7">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Penduduk Usia 15 Tahun dengan Kemampuan Baca Tulis</h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/pendidikan/Adminpendidikanbacatulis/insert"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Kemampuan</label>
                      <select name="id_jenis" id="id_jenis" class="form-control">

                        <?php
                          if($list_pendidikan_jenis){
                            $no = 1;
                            foreach ($list_pendidikan_jenis as $r_data => $v_data) {
                              echo "<option value=\"".$v_data->id_jenis."\">".$v_data->nama_jenis."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Total Prosentase Kemampuan</label>
                      <input type="text" class="form-control" id="prosentase" name="prosentase" placeholder="Total Prosentase Kemampuan" required="">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        <!-- </div> -->
        <!--/.col (left) -->

        

      <!-- </div> -->
      
      <!-- <div class="row"> -->
        <!-- left column -->
        <!-- <div class="col-md-8"> -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kec"><i class="fa fa-plus"></i>&nbsp;&nbsp; Tambah Data Jumlah Panen Komoditi (Ton)</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Tahun</th>
                  <th>Jenis Kemampuan</th>
                  <th>Total Prosentase Kemampuan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  if($list_pendidikan){
                      $no = 1;

                      foreach ($list_pendidikan as $r_data => $v_data) {
                      	
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_data->th."</td>
                                <td>".$v_data->nama_jenis."</td>
                                <td>".$v_data->prosentase."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_ken('".$v_data->id_baca_tulis."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_ken('".$v_data->id_baca_tulis."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                    $no++;
                    }

                  }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <div class="col-md-5">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Jenis Kemampuan</h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kel"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data Jenis Keuangan Daerah</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Kemampuan</th>
                  <th>Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php echo form_open("admin_opd/pendidikan/Adminpendidikanbacatulisjenis/insert"); ?>
                      <td colspan="2">
                      	<input type="text" class="form-control" id="nama_jenis" name="nama_jenis">
                      </td>
                      <td><button type="submit" class="btn btn-success pull-right" id="add_jenis_keu" name="add_jenis_keu"><i class="fa fa-plus"></i>&nbsp;&nbsp; Simpan</button></td>
                    </form>
                  </tr>
                  <?php
                    if($list_pendidikan_jenis){
                      $no = 1;
                      foreach ($list_pendidikan_jenis as $r_data => $v_data) {
                      	
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_data->nama_jenis."</td>
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_jenis('".$v_data->id_jenis."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_jenis('".$v_data->id_jenis."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                    
                  ?>
                  
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row --> 
      
    </div>

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


        <div class="modal fade" id="modal_up_jenis">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Jenis Kemampuan</h4>
              </div>
              <?php echo form_open("admin_opd/pendidikan/Adminpendidikanbacatulisjenis/update"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Keterangan</label>
                              <input type="text" class="form-control" id="_nama_jenis" name="nama_jenis" placeholder="Keterangan" required="">
                            </div>
                          </div>
                          
                          <div class="col-md-12" hidden="">
                            <div class="form-group">
                              <label for="exampleInputPassword1">id jenis</label>
                              <input type="text" class="form-control" id="_id_jenis" name="id_jenis" placeholder="Jumlah RW" required="" readonly="">
                            </div>
                          </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------- Komoditi ---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        <div class="modal fade" id="modal_up_ken">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Penduduk Usia 15 Tahun dengan Kemampuan Baca Tulis</h4>
              </div>
              <?php echo form_open("admin_opd/pendidikan/Adminpendidikanbacatulis/update/"); ?>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Kemampuan</label>
                      <select name="id_jenis" id="_id_jenis_ex" class="form-control">

                        <?php
                          if($list_pendidikan_jenis){
                            $no = 1;
                            foreach ($list_pendidikan_jenis as $r_data => $v_data) {
                              echo "<option value=\"".$v_data->id_jenis."\">".$v_data->nama_jenis."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="number" class="form-control" id="_th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Total Prosentase Kemampuan</label>
                      <input type="text" class="form-control" id="_prosentase" name="prosentase" placeholder="Total Prosentase Kemampuan" required="">
                    </div>
                  </div>

                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_baca_tulis</label>
                      <input type="text" class="form-control" id="_id_baca_tulis" name="id_baca_tulis" readonly="" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------- Komoditi ---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    


    <script type="text/javascript">


      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_jenis(id_jenis){
        clear_mod_up_jenis();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_jenis', id_jenis);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/pendidikan/Adminpendidikanbacatulisjenis/index_up";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_jenis(res);
            }
        });
        
        $("#modal_up_jenis").modal('show');
      }

      function res_update_jenis(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_jenis").val(main_data.id_jenis);
              $("#_nama_jenis").val(main_data.nama_jenis);
              
              
          }else{
              clear_mod_up_jenis();
          }
      }

      function clear_mod_up_jenis(){
          $("#_id_jenis").val("");
          $("#_nama_jenis").val("");
         
      }

      // var admin_upx = "";
      function del_data_jenis(id_jenis){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua...");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/pendidikan/Adminpendidikanbacatulisjenis/delete/";?>"+id_jenis;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Komoditi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_ken(id_baca_tulis){
        clear_mod_up_ken();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_baca_tulis', id_baca_tulis);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/pendidikan/Adminpendidikanbacatulis/index_up/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_ken(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_ken").modal('show');
      }

      function res_update_ken(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_baca_tulis").val(main_data.id_baca_tulis);
              $("#_id_jenis_ex").val(main_data.id_jenis);
              $("#_th").val(main_data.th);
              $("#_prosentase").val(main_data.prosentase);
              
          }else{
              clear_mod_up_ken();
          }
      }

      function clear_mod_up_ken(){
          $("#_id_baca_tulis").val("");
          $("#_id_jenis_ex").val("");
          $("#_th").val("");
          $("#_prosentase").val("");
      }

      // var admin_upx = "";
      function del_data_ken(id_baca_tulis){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_baca_tulis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_baca_tulis+" akan terhapus semau.. !!!!!!! ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/pendidikan/Adminpendidikanbacatulis/delete/";?>"+id_baca_tulis;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Komoditi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    
      

    </script>
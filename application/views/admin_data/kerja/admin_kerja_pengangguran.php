 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data Pengangguran</h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/kerja/Adminkerjapengangguran/insert_kerja"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Pengangguran</label>
                      <input type="text" class="form-control" id="jml" name="jml" placeholder="Jumlah Pengangguran" required="">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Pengangguran</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Periode (Tahun)</th>
                  <th>Jumlah Pengangguran</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_kerja){
                      $no = 1;
                      foreach ($list_kerja as $r_list_kerja => $v_list_kerja) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_kerja->th."</td>
                                <td>".$v_list_kerja->jml."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data('".$v_list_kerja->id_pengangguran."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data('".$v_list_kerja->id_pengangguran."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->       
      </div>
      <!-- /.row -->      
    </div>

    

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Kerja---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        

        <div class="modal fade" id="modal_up">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Pengangguran</h4>
              </div>
              <?php echo form_open("admin_opd/kerja/Adminkerjapengangguran/up_kerja"); ?>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="_th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Pengangguran</label>
                      <input type="text" class="form-control" id="_jml" name="jml" placeholder="Jumlah Penduduk yang Bekeraja" required="">
                    </div>
                  </div>
                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_pengangguran</label>
                      <input type="text" class="form-control" id="_id_pengangguran" name="id_pengangguran" readonly="" required="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Kerja---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

<script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Kerja---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data(id_pengangguran){
        clear_mod_up();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_pengangguran', id_pengangguran);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/kerja/Adminkerjapengangguran/index_up_kerja/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up").modal('show');
      }

      function res_update(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_pengangguran").val(main_data.id_pengangguran);
              $("#_th").val(main_data.th);
              $("#_jml").val(main_data.jml);
              
          }else{
              clear_mod_up();
          }
      }

      function clear_mod_up(){
          $("#_id_pengangguran").val("");
          $("#_th").val("");
          $("#_jml").val("");
      }

      // var admin_upx = "";
      function del_data(id_pengangguran){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_pengangguran+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_pengangguran+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/kerja/Adminkerjapengangguran/delete_kerja/";?>"+id_pengangguran;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Kerja---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


      

    </script>
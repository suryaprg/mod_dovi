 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Iklim </h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/geo/adminiklim/insert_ik"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Stasiun Klimatologi</label>
                      <select name="nama_st" id="nama_st" class="form-control">
                        <?php
                          if($list_stasiun){
                            $no = 1;
                            foreach ($list_stasiun as $r_list_stasiun => $v_list_stasiun) {
                              echo "<option value=\"".$v_list_stasiun->id_st."\">".$v_list_stasiun->ket_st."</option>";     
                            $no++;
                            }
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Bulan)</label>
                      <select class="form-control" id="periode" name="periode">
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                      </select>
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Hujan (Hari)</label>
                      <input type="text" class="form-control" id="hari" name="hari" placeholder="Jumlah Hujan (Hari)" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Curah Hujan</label>
                      <input type="text" class="form-control" id="cura" name="cura" placeholder="Jumlah Curah Hujan" required="">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Stasiun Klimatologi</h3>
              <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_st"><i class="fa fa-plus"></i>&nbsp;&nbsp; Tambah Data Stasiun Klimatologi</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Stasiun</th>
                  <th>Keterangan Stasiun</th>
                  <th>Aksi</th>                  
                </tr>
                </thead>
                <tbody>
                  <?php

                  if($list_stasiun){
                      $no = 1;
                      foreach ($list_stasiun as $r_list_stasiun => $v_list_stasiun) {
                        echo "<tr>
                                <td>".$no."</td>
                                
                                <td>".$v_list_stasiun->ket_st."</td>
                                <td>".$v_list_stasiun->alamat_st."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_st('".$v_list_stasiun->id_st."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_st('".$v_list_stasiun->id_st."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                    $no++;
                    }

                  }
                    
                  ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Curah Hujan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped display">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Stasiun</th>
                  
                  <th>Tahun</th>
                  <th>Periode</th>
                  <th>Jumlah Hari Hujan</th>
                  <th>Jumlah Curah Hujan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_hujan){
                      $no = 1;
                      foreach ($list_hujan as $r_list_hujan => $v_list_hujan) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_hujan->ket_st."</td>
                                <td>".$v_list_hujan->th."</td>
                                <td>".$v_list_hujan->periode."</td>
                                <td>".$v_list_hujan->jml_hari."</td>
                                <td>".$v_list_hujan->jml_cura."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_ik('".$v_list_hujan->id_hujan."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_ik('".$v_list_hujan->id_hujan."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->

        

      </div>
      <!-- /.row -->      
    </div>

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Stasiun Klimatologi---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

        <div class="modal fade" id="modal_in_st">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Data Stasiun Klimatologi</h4>
              </div>
              <?php echo form_open("admin_opd/geo/adminiklim/insert_st"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          <div class="form-group">
                            <label for="exampleInputPassword1">Nama Stasiun</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Stasiun" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Alamat Stasiun</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat" required="">
                          </div>
                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="modal_up_st">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Stasiun</h4>
              </div>
              <?php echo form_open("admin_opd/geo/adminiklim/up_st"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          
                          <div class="box-body">
                          <div class="form-group">
                            <label for="exampleInputPassword1">Nama Stasiun</label>
                            <input type="text" class="form-control" id="up_nama" name="nama" placeholder="Nama Stasiun" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Alamat Stasiun</label>
                            <input type="text" class="form-control" id="up_alamat" name="alamat" placeholder="Alamat Stasiun" required="">
                          </div>
                        </div>

                          <div class="form-group" hidden="">
                            <label for="exampleInputPassword1">Id Stasiun</label>
                            <input type="text" class="form-control" id="up_id_st" name="id_st" readonly="" required="">
                          </div>

                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------stasiun---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Iklim---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        

        <div class="modal fade" id="modal_up_iklim">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Kelurahan</h4>
              </div>
              <?php echo form_open("admin_opd/geo/Adminiklim/up_ik"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Nama Stasiun Klimatologi</label>
                                <select name="nama_st" id="_nama_st" class="form-control">
                                  <?php
                                    if($list_stasiun){
                                      $no = 1;
                                      foreach ($list_stasiun as $r_list_stasiun => $v_list_stasiun) {
                                        echo "<option value=\"".$v_list_stasiun->id_st."\">".$v_list_stasiun->ket_st."</option>";     
                                      $no++;
                                      }
                                    }
                                  ?>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Periode (Bulan)</label>
                                <select class="form-control" id="_periode" name="periode">
                                  <option value="1">Januari</option>
                                  <option value="2">Februari</option>
                                  <option value="3">Maret</option>
                                  <option value="4">April</option>
                                  <option value="5">Mei</option>
                                  <option value="6">Juni</option>
                                  <option value="7">Juli</option>
                                  <option value="8">Agustus</option>
                                  <option value="9">September</option>
                                  <option value="10">Oktober</option>
                                  <option value="11">November</option>
                                  <option value="12">Desember</option>
                                </select>
                                <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Tahun</label>
                                <input type="number" class="form-control" id="_th" name="th" placeholder="Tahun" required="">
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Jumlah Hujan (Hari)</label>
                                <input type="text" class="form-control" id="_hari" name="hari" placeholder="Jumlah Hujan (Hari)" required="">
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Jumlah Cura Hujan</label>
                                <input type="text" class="form-control" id="_cura" name="cura" placeholder="Jumlah Cura Hujan" required="">
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Id</label>
                                <input type="text" class="form-control" id="_id_hujan" name="id_hujan" placeholder="Jumlah Cura Hujan" required="">
                              </div>
                            </div>
                          </div>

                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Iklim---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

    <script type="text/javascript">      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Stasiun---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_st(id_st){
        clear_mod_up_st();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_st', id_st);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/geo/Adminiklim/index_up_st/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_st(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_st").modal('show');
      }

      function res_update_st(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#up_nama").val(main_data.ket_st);
              $("#up_alamat").val(main_data.alamat_st);
              $("#up_id_st").val(main_data.id_st);
              
          }else{
              clear_mod_up_st();
          }
      }

      function clear_mod_up_st(){
          $("#up_nama").val("");
          $("#up_alamat").val("");
          $("#up_id_st").val("");
          
      }

      // var admin_upx = "";
      function del_data_st(id_st){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_st+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_st+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/geo/Adminiklim/delete_st/";?>"+id_st;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Stasiun---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Iklim---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_ik(id_ik){
        clear_mod_up_ik();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_ik', id_ik);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/geo/Adminiklim/index_up_ik/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_ik(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_iklim").modal('show');
      }

      function res_update_ik(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_nama_st").val(main_data.id_st);
              $("#_periode").val(main_data.periode);
              $("#_th").val(main_data.th);
              $("#_hari").val(main_data.jml_hari);

              // console.log(main_data.id_kec);
              $("#_cura").val(main_data.jml_cura);
              $("#_id_hujan").val(main_data.id_hujan);
              
          }else{
              clear_mod_up_ik();
          }
      }

      function clear_mod_up_ik(){
          $("#_nama_st").val("");
          $("#_periode").val("");
          $("#_th").val("");
          $("#_hari").val("");
          $("#_cura").val("");
          $("#_id_hujan").val("");
      }

      // var admin_upx = "";
      function del_data_ik(id_ik){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_ik+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_ik+" akan terhapus semau.. !!!!!!! ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/geo/adminiklim/delete_ik/";?>"+id_ik;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Iklim---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    
      

    </script>
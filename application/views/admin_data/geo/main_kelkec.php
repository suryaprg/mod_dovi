 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Kecamatan</h3>
              <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kec"><i class="fa fa-plus"></i>&nbsp;&nbsp; Tambah Data Kecamatan</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Kecamatan</th>
                  <th>Luas Wilayah</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_kacamatan){
                      $no = 1;
                      foreach ($list_kacamatan as $r_list_kacamatan => $v_list_kacamatan) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_kacamatan->nama_kec."</td>
                                <td>".$v_list_kacamatan->luas."</td>
                                
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_kec('".$v_list_kacamatan->id_kec."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_kec('".$v_list_kacamatan->id_kec."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->

        <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Kelurahan</h3>
              <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kel"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data Kelurahan</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Kelurahan</th>
                  <th>Wilayah Kecamatan</th>
                  <th>Luas</th>
                  <th>Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                  <?php

                  if($list_kelurahan){
                      $no = 1;
                      foreach ($list_kelurahan as $r_list_kelurahan => $v_list_kelurahan) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_kelurahan->nama_kel."</td>
                                <td>".$v_list_kelurahan->nama_kec."</td>
                                <td>".$v_list_kelurahan->luas_kel."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_kel('".$v_list_kelurahan->id_kel."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_kel('".$v_list_kelurahan->id_kel."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                    $no++;
                    }

                  }
                    
                  ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

      </div>
      <!-- /.row -->      
    </div>

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Kecamatan---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

        <div class="modal fade" id="modal_in_kec">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Data Kecamatan</h4>
              </div>
              <?php echo form_open("admin_super/mainkelkec/insert_kecamatan"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          
                          <div class="form-group">
                            <label for="exampleInputPassword1">Nama Kecamatan</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Kecamatan" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Posisi Wilayah (Latitude dan Longitude)</label>
                            <input type="text" class="form-control" id="latlng" name="latlng" placeholder="Posisi Wilayah (Latitude dan Longitud)" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Luas Wilayah (km2)</label>
                            <input type="text" class="form-control" id="luas" name="luas" placeholder="Luas Wilayah (km2)" required="">
                          </div>

                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="modal_up_kec">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Kecamatan</h4>
              </div>
              <?php echo form_open("admin_super/mainkelkec/up_kecmatan"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          
                          <div class="form-group">
                            <label for="exampleInputPassword1">Nama Kecamatan</label>
                            <input type="text" class="form-control" id="up_nama" name="nama" placeholder="Nama Kecamatan" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Posisi Wilayah (Latitude dan Longitude)</label>
                            <input type="text" class="form-control" id="up_latlng" name="latlng" placeholder="Posisi Wilayah (Latitude dan Longitud)" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Luas Wilayah (km2)</label>
                            <input type="text" class="form-control" id="up_luas" name="luas" placeholder="Luas Wilayah (km2)" required="">
                          </div>

                          <div class="form-group" hidden="">
                            <label for="exampleInputPassword1">Id Kecamatan</label>
                            <input type="text" class="form-control" id="up_id_kec" name="id_kec" placeholder="Luas Wilayah (km2)" required="">
                          </div>

                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Kecamatan---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Kelurahan---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        <div class="modal fade" id="modal_in_kel">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Data Kelurahan</h4>
              </div>
              <?php echo form_open("admin_super/mainkelkec/insert_kelurahan"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          
                          <div class="form-group">
                            <label for="exampleInputPassword1">Nama Kelurahan</label>
                            <input type="text" class="form-control" id="nama_kel" name="nama_kel" placeholder="Nama Kelurahan" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Wilayah Kecamatan</label>

                            <select class="form-control" id="id_kec" name="id_kec">
                              <?php
                                if($list_kacamatan){
                                    foreach ($list_kacamatan as $r_list_kacamatan => $v_list_kacamatan) {
                                      echo "<option value=\"".$v_list_kacamatan->id_kec."\">".$v_list_kacamatan->nama_kec."</option>";
                                    }
                                }
                              ?>
                            </select>

                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Posisi Wilayah (Latitude dan Longitude)</label>
                            <input type="text" class="form-control" id="latlng_kel" name="latlng_kel" placeholder="Posisi Wilayah (Latitude dan Longitud)" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Luas Wilayah (km2)</label>
                            <input type="text" class="form-control" id="luas_kel" name="luas_kel" placeholder="Luas Wilayah (km2)" required="">
                          </div>

                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="modal_up_kel">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Kelurahan</h4>
              </div>
              <?php echo form_open("admin_super/mainkelkec/up_kelurahan"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          
                          <div class="form-group">
                            <label for="exampleInputPassword1">Nama Kelurahan</label>
                            <input type="text" class="form-control" id="up_nama_kel" name="nama_kel" placeholder="Nama Kelurahan" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Wilayah Kecamatan</label>

                            <select class="form-control" id="up_id_kec_kel" name="id_kec">
                              <?php
                                if($list_kacamatan){
                                    foreach ($list_kacamatan as $r_list_kacamatan => $v_list_kacamatan) {
                                      echo "<option value=\"".$v_list_kacamatan->id_kec."\">".$v_list_kacamatan->nama_kec."</option>";
                                    }
                                }
                              ?>
                            </select>

                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Posisi Wilayah (Latitude dan Longitude)</label>
                            <input type="text" class="form-control" id="up_latlng_kel" name="latlng_kel" placeholder="Posisi Wilayah (Latitude dan Longitud)" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Luas Wilayah (km2)</label>
                            <input type="text" class="form-control" id="up_luas_kel" name="luas_kel" placeholder="Luas Wilayah (km2)" required="">
                          </div>

                          <div class="form-group" hidden="">
                            <label for="exampleInputPassword1">Id Kelurahan</label>
                            <input type="text" class="form-control" id="up_id_kel" name="up_id_kel" placeholder="Nama Kelurahan" required="">
                          </div>

                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Kelurahan---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    


    <script type="text/javascript">


      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Kecamatan---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_kec(id_kec){
        clear_mod_up_kec();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_kec', id_kec);    
          $.ajax({
            url: "<?php echo base_url()."/admin_super/mainkelkec/index_up_kecmatan/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_kec(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_kec").modal('show');
      }

      function res_update_kec(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#up_nama").val(main_data.nama_kec);
              $("#up_latlng").val(main_data.latlng);
              $("#up_luas").val(main_data.luas);
              $("#up_id_kec").val(main_data.id_kec);
              
          }else{
              clear_mod_up();
          }
      }

      function clear_mod_up_kec(){
          $("#up_nama").val("");
          $("#up_latlng").val("");
          $("#up_luas").val("");
          $("#up_id_kec").val("");
      }

      // var admin_upx = "";
      function del_data_kec(id_kec){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_kec+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kec+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_super/mainkelkec/delete_kecmatan/";?>"+id_kec;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Kecamatan---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Kelurahan---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_kel(id_kel){
        clear_mod_up_kel();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_kel', id_kel);    
          $.ajax({
            url: "<?php echo base_url()."/admin_super/mainkelkec/index_up_kelurahan/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_kel(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_kel").modal('show');
      }

      function res_update_kel(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#up_nama_kel").val(main_data.nama_kel);
              $("#up_id_kec_kel").val(main_data.id_kec);

              // console.log(main_data.id_kec);
              $("#up_latlng_kel").val(main_data.latlng);
              $("#up_luas_kel").val(main_data.luas_kel);
              $("#up_id_kel").val(main_data.id_kel);
              
          }else{
              clear_mod_up_kel();
          }
      }

      function clear_mod_up_kel(){
          $("#up_nama_kel").val("");
          $("#up_latlng_kel").val("");
          $("#up_luas_kel").val("");
          $("#up_id_kel").val("");
      }

      // var admin_upx = "";
      function del_data_kel(id_kel){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_kel+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_kel+" akan terhapus semau.. !!!!!!! ");
        if(conf){
          window.location.href = "<?= base_url()."admin_super/mainkelkec/delete_kelurahan/";?>"+id_kel;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Kelurahan---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    
      

    </script>
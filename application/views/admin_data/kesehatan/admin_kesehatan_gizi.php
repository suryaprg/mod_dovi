 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data Jumlah Balita Kurang Gizi dan Gizi Buruk</h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/kesehatan/Adminkesehatangizi/insert"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Balita Kurang Gizi</label>
                      <input type="text" class="form-control" id="jmh_kurang_gizi" name="jmh_kurang_gizi" placeholder="Jumlah Balita Kurang Gizi" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Balita Gizi Buruk</label>
                      <input type="text" class="form-control" id="jml_gizi_buruk" name="jml_gizi_buruk" placeholder="Jumlah Balita Gizi Buruk" required="">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Jumlah Balita Kurang Gizi dan Gizi Buruk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Periode (Tahun)</th>
                  <th>Jumlah Balita Kurang Gizi</th>
                  <th>Jumlah Balita Gizi Buruk</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_kesehatan){
                      $no = 1;
                      foreach ($list_kesehatan as $r_list_kesehatan => $v_list_kesehatan) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_kesehatan->th."</td>
                                <td>".$v_list_kesehatan->jmh_kurang_gizi."</td>
                                <td>".$v_list_kesehatan->jml_gizi_buruk."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data('".$v_list_kesehatan->id_gizi_balita."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data('".$v_list_kesehatan->id_gizi_balita."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->       
      </div>
      <!-- /.row -->      
    </div>

    

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        

        <div class="modal fade" id="modal_up">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Jumlah Balita Kurang Gizi dan Gizi Buruk</h4>
              </div>
              <?php echo form_open("admin_opd/kesehatan/Adminkesehatangizi/update"); ?>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="_th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Balita Kurang Gizi</label>
                      <input type="text" class="form-control" id="_jmh_kurang_gizi" name="jmh_kurang_gizi" placeholder="Jumlah Balita Kurang Gizi" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Balita Gizi Buruk</label>
                      <input type="text" class="form-control" id="_jml_gizi_buruk" name="jml_gizi_buruk" placeholder="Jumlah Balita Gizi Buruk" required="">
                    </div>
                  </div>

                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_gizi_balita</label>
                      <input type="text" class="form-control" id="_id_gizi_balita" name="id_gizi_balita" readonly="" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

<script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data(id_gizi_balita){
        clear_mod_up();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_gizi_balita', id_gizi_balita);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/kesehatan/Adminkesehatangizi/index_up";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up").modal('show');
      }

      function res_update(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_gizi_balita").val(main_data.id_gizi_balita);
              $("#_th").val(main_data.th);
              $("#_jmh_kurang_gizi").val(main_data.jmh_kurang_gizi);
              $("#_jml_gizi_buruk").val(main_data.jml_gizi_buruk);
              
          }else{
              clear_mod_up();
          }
      }

      function clear_mod_up(){
          $("#_id_gizi_balita").val("");
          $("#_th").val("");
          $("#_jmh_kurang_gizi").val("");
          $("#_jml_gizi_buruk").val("");
      }

      // var admin_upx = "";
      function del_data(id_gizi_balita){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_gizi_balita+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_gizi_balita+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/kesehatan/Adminkesehatangizi/delete/";?>"+id_gizi_balita;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


      

    </script>
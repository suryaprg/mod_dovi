	<div class="content body" style="margin-right: 3em; margin-left: 3em;">
		<section class="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data dpmptsp</h3>
						</div>

						<div class="box-body" style="overflow-x:auto;">							
							<table class="table table-bordered table-striped display" id="example1" style="width: 100px">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 3%">Jenis</th>
										<th class="text-center" style="width: 1%">Tahun</th>
										<th class="text-center" style="width: 1%">Terbit</th>
										<th class="text-center" style="width: 1%">Investasi</th>
										<th class="text-center" style="width: 1%">Tk</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/admin_opd/dpmptsp/Admindpmptsp/insert_dpmptsp/" ?>" method="post">
											<td>
												<select name="id_jenis" class="form-control" id="id_jenis">
													<option value="">-- Pilih Jenis --</option>
													<?php foreach ($list_dpmptsp_jenis as $ltj) { ?>
														<option value="<?php echo $ltj->id_jenis ?>"><?php echo $ltj->nama_jenis ?></option>
													<?php } ?>
												</select>
											</td>
											
											<td><input type="number" name="th" class="form-control" id="th" placeholder="Tahun"></td>
											<td><input type="number" name="terbit" class="form-control" id="terbit" placeholder="Terbit"></td>
											<td><input type="number" name="investasi" class="form-control" id="investasi" placeholder="Investasi"></td>
											<td><input type="number" name="tk" class="form-control" id="tk" placeholder="TK"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_dpmptsp as $lt) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $lt->nama_jenis ?></td>
											<td align="center"><?php echo $lt->th?></td>
											<td align="center"><?php echo $lt->terbit?></td>
											<td align="center"><?php echo $lt->investasi?></td>
											<td align="center"><?php echo $lt->tk ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_terminal(<?php echo $lt->id_main ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_terminal(<?php echo $lt->id_main ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-md-5">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Data Jenis dpmptsp</h3>
						</div>

						<div class="box-body">
							<table class="table table-bordered table-striped " id="">
								<thead>
									<tr>
										<th class="text-center" style="width: 1%">No</th>
										<th class="text-center" style="width: 5%">Nama</th>
										<th class="text-center" style="width: 1%">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<form action="<?php echo base_url()."/admin_opd/dpmptsp/Admindpmptsp/insert_dpmptsp_jenis/" ?>" method="post">
											<td><input type="text" name="nama_jenis" class="form-control" id="nama_jenis" placeholder="Nama Jenis"></td>
											<td align="center">
												<button type="submit" class="btn btn-success">
													<span class="glyphicon glyphicon-plus"></span>
												</button>
											</td>
										</form>
									</tr>
									<?php $no = 1; foreach ($list_dpmptsp_jenis as $ltj) { ?>
										<tr>
											<td align="center"><?php echo $no ?></td>
											<td align="left"><?php echo $ltj->nama_jenis ?></td>
											<td align="center">
												<a href="#" onclick="up_lp_terminal_jenis(<?php echo $ltj->id_jenis ?>)">
													<button class="btn btn-primary btn-xs">
														<span class="glyphicon glyphicon-pencil"></span>
													</button>
												</a>
												<a href="#" onclick="del_lp_terminal_jenis(<?php echo $ltj->id_jenis ?>)">
													<button class="btn btn-danger btn-xs">
														<span class="glyphicon glyphicon-trash"></span>
													</button>
												</a>												
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

		</section>
	</div>

	<script type="text/javascript">
//======================================================== LP TERMINAL ===========================================//
	  	function up_lp_terminal(id_main){
	    	clear_mod_up_lp_terminal();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_main', id_main);    
		      $.ajax({
		        url: "<?php echo base_url()."/admin_opd/dpmptsp/Admindpmptsp/index_up_dpmptsp/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_terminal(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_terminal").modal('show');
	  	} 

	  	function clear_mod_up_lp_terminal(){
	      	$("#_id_jenis").val("");
	        $("#_th").val("");
	        $("#_terbit").val("");
	        $("#_investasi").val("");
	      	$("#_tk").val("");
	  	}

	  	function res_update_lp_terminal(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_main").attr('readonly','readonly');                                        
	          	$("#_id_main").val(main_data.id_main);
	          	$("#_id_jenis").val(main_data.id_jenis);
	         	$("#_th").val(main_data.th);
	         	$("#_terbit").val(main_data.terbit);
	         	$("#_investasi").val(main_data.investasi);
	          	$("#_tk").val(main_data.tk);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_terminal(id_main){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_main+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_main+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."admin_opd/dpmptsp/Admindpmptsp/delete_dpmptsp/";?>"+id_main;
	      	}else{

	      	}
	  	}      		
//======================================================== LP TERMINAL ===========================================//

//======================================================== LP TERMINAL JENIS =====================================//
	  	function up_lp_terminal_jenis(id_jenis){
	    	clear_mod_up_lp_terminal_jenis();
	    	// console.lo

		    var data_main =  new FormData();
		    data_main.append('id_jenis', id_jenis);    
		      $.ajax({
		        url: "<?php echo base_url()."/admin_opd/dpmptsp/Admindpmptsp/index_up_dpmptsp_jenis/";?>", // point to serv
		        dataType: 'html',  // what to expect back from the PHP script, if anything
		        cache: false,
		        contentType: false,
		        processData: false,
		        data: data_main,                         
		        type: 'post',
		        success: function(res){
		            console.log(res);
		            res_update_lp_terminal_jenis(res);
		            // $("#out_up_mhs").html(res);
		        }
	    	});
	    
		    $("#modal_up_lp_terminal_jenis").modal('show');
	  	} 

	  	function clear_mod_up_lp_terminal_jenis(){
	      	$("#_nama_jenis").val("");
	  	}

	  	function res_update_lp_terminal_jenis(res){
	      	var data = JSON.parse(res);

	      	if(data.status){
	          	var main_data = data.val;
	          	console.log(main_data);
	          
	          	$("#_id_jenis2").attr('readonly','readonly');                                        
	          	$("#_id_jenis2").val(main_data.id_jenis);
	          	$("#_nama_jenis").val(main_data.nama_jenis);
	          
	      	}else{
	          	clear_mod_up_ik();
	      	}
	  	}		    

	  	function del_lp_terminal_jenis(id_jenis){
	      	var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua.. !!!!!!! ");
	      	if(conf){
	        	window.location.href = "<?= base_url()."admin_opd/dpmptsp/Admindpmptsp/delete_dpmptsp_jenis/";?>"+id_jenis;
	      	}else{

	      	}
	  	}      		
//======================================================== LP TERMINAL JENIS =====================================//	
	</script>

	<div class="modal fade" id="modal_up_lp_terminal">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data</h4>
	      	</div>
	      	<?php echo form_open("admin_opd/dpmptsp/Admindpmptsp/update_dpmptsp"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID Terminal</label>
	                        	<input type="number" name="id_main" id="_id_main" class="form-control">
	                      	</div>                      
	                      
	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Jenis</label>
								<select name="id_jenis" class="form-control" id="_id_jenis">
									<option value="">-- Pilih Jenis --</option>
									<?php foreach ($list_dpmptsp_jenis as $ltj) { ?>
										<option value="<?php echo $ltj->id_jenis ?>"><?php echo $ltj->nama_jenis ?></option>
									<?php } ?>
								</select>
	                      	</div>                     	


	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Tahun</label>
	                        	<input type="number" name="th" class="form-control" id="_th" placeholder="Tahun">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Terbit</label>
	                        	<input type="number" name="terbit" class="form-control" id="_terbit" placeholder="Terbit">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Investasi</label>
	                        	<input type="number" name="investasi" class="form-control" id="_investasi" placeholder="Investasi">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">TK</label>
	                        	<input type="number" name="tk" class="form-control" id="_tk" placeholder="TK">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>

	<div class="modal fade" id="modal_up_lp_terminal_jenis">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Ubah Data Jenis</h4>
	      	</div>
	      	<?php echo form_open("admin_opd/dpmptsp/Admindpmptsp/update_dpmptsp_jenis"); ?>
	      	<div class="modal-body">
	        	<div class="row">
	          	<div class="col-md-12">
	            	<div class="box box-primary">
	                	<div class="box-body">
	                  	<div class="row">
	                    	<div class="col-md-12">

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">ID Jenis</label>
	                        	<input type="number" name="id_jenis" id="_id_jenis2" class="form-control">
	                      	</div>

	                      	<div class="form-group">
	                        	<label for="exampleInputPassword1">Nama</label>
	                        	<input type="text" name="nama_jenis" class="form-control" id="_nama_jenis" placeholder="Nama">
	                      	</div>

	                  	</div>
	                	</div>
	            	</div>
	          	</div>
	        	</div>
	      	</div>

	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	        	<button type="submit" class="btn btn-primary">Ubah Data</button>
	      	</div>

	      	</form>
	    </div>
	  </div>
	</div>
</div>
</div>	
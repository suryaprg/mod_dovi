<!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">KATEGORI</li>

        <li class="treeview">
          <a href="#">
             <span>Admin</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."super/admin";?>"><i class="fa fa-circle-o"></i>Data Admin</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
             <span>Partai</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."super/partai";?>"><i class="fa fa-circle-o"></i>Data Partai</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
             <span>Kecamatan Kelurahan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."super/kelkec";?>"><i class="fa fa-circle-o"></i>Data Kecamatan dan Kelurahan</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
             <span>Geografi</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/iklim";?>"><i class="fa fa-circle-o"></i>Data Iklim dan Curah Hujan</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Pemerintah</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/rt_rw";?>"><i class="fa fa-circle-o"></i>RT dan RW</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Ekonomi</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/keuangan_daerah";?>"><i class="fa fa-circle-o"></i>keuangan_daerah</a></li>
            <li><a href="<?=base_url()."admin/pendapatan_regional";?>"><i class="fa fa-circle-o"></i>pendapatan_regional</a></li>
            <li><a href="<?=base_url()."admin/kegiatan_usaha";?>"><i class="fa fa-circle-o"></i>kegiatan_usaha</a></li>
            <li><a href="<?=base_url()."admin/pengeluaran_penduduk";?>"><i class="fa fa-circle-o"></i>pengeluaran_penduduk</a></li>
            <li><a href="<?=base_url()."admin/pengeluaran_rumah_tangga";?>"><i class="fa fa-circle-o"></i>pengeluaran_rumah_tangga</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Kependudukan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/kependudukan_jk";?>"><i class="fa fa-circle-o"></i>Berdasarkan Jenis Kelamin</a></li>
            <li><a href="<?=base_url()."admin/kependudukan_umur";?>"><i class="fa fa-circle-o"></i>Berdasarkan Umur</a></li>
            <li><a href="<?=base_url()."admin/kependudukan_ketergantungan";?>"><i class="fa fa-circle-o"></i>Index Ketergantungan</a></li>
            <li><a href="<?=base_url()."admin/kependudukan_ipm";?>"><i class="fa fa-circle-o"></i>IPM</a></li>
            <li><a href="<?=base_url()."admin/kependudukan_ipm_bid";?>"><i class="fa fa-circle-o"></i>Pengembangan IPM</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Ketenaga Kerjaan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/kerja_angkatan";?>"><i class="fa fa-circle-o"></i>Angkatan Kerja</a></li>
            <li><a href="<?=base_url()."admin/kerja_pengangguran";?>"><i class="fa fa-circle-o"></i>Pengangguran</a></li>
            <li><a href="<?=base_url()."admin/kerja_ump";?>"><i class="fa fa-circle-o"></i>Upah</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Tehnologi</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/tehno_bts";?>"><i class="fa fa-circle-o"></i>BTS</a></li>
            <li><a href="<?=base_url()."admin/tehno_warnet";?>"><i class="fa fa-circle-o"></i>Warnet</a></li>
            <li><a href="<?=base_url()."admin/tehno_web";?>"><i class="fa fa-circle-o"></i>Website OPD</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Kesehatan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/kesehatan_gizi";?>"><i class="fa fa-circle-o"></i>Gizi Balita</a></li>
            <li><a href="<?=base_url()."admin/kesehatan_penyakit";?>"><i class="fa fa-circle-o"></i>Penyakit</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Transportasi</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/trans_kendaraan";?>"><i class="fa fa-circle-o"></i>Kendaraan</a></li>
            <li><a href="<?=base_url()."admin/trans_jalan";?>"><i class="fa fa-circle-o"></i>Jalan</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Pertanian</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/pertanian_lahan";?>"><i class="fa fa-circle-o"></i>Lahan Berdasarkan Jenis</a></li>
            <li><a href="<?=base_url()."admin/pertanian_lahan_penggunaan";?>"><i class="fa fa-circle-o"></i>Lahan Berdasarkan Penggunaan</a></li>
            <li><a href="<?=base_url()."admin/kelapa_tebu";?>"><i class="fa fa-circle-o"></i>Kelapa dan Tebu</a></li>
            <li><a href="<?=base_url()."admin/pertanian_komoditi";?>"><i class="fa fa-circle-o"></i>Komoditi</a></li>
            <li><a href="<?=base_url()."admin/pertanian_hortikultura";?>"><i class="fa fa-circle-o"></i>Hortikultura</a></li>
            <li><a href="<?=base_url()."admin/peternakan";?>"><i class="fa fa-circle-o"></i>Peternakan</a></li>
            <li><a href="<?=base_url()."admin/perikanan";?>"><i class="fa fa-circle-o"></i>Perikanan</a></li>

          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Pendidikan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/pendidikan_baca_tulis";?>"><i class="fa fa-circle-o"></i>Baca dan Tulis</a></li>
            <li><a href="<?=base_url()."admin/pendidikan_partisipasi_sklh";?>"><i class="fa fa-circle-o"></i>Partisipasi Sekolah</a></li>
            <li><a href="<?=base_url()."admin/pendidikan_penduduk";?>"><i class="fa fa-circle-o"></i>Pendidikan Penduduk</a></li>
            <li><a href="<?=base_url()."admin/jumlah_sekolah";?>"><i class="fa fa-circle-o"></i>Jumlah Sekolah</a></li>
            <li><a href="<?=base_url()."admin/rasio_guru_murid";?>"><i class="fa fa-circle-o"></i>Rasio Guru dan Murid</a></li>

          </ul>
        </li>

        <li class="treeview">
          <a href="#">
             <span>Kemiskinan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/penduduk_miskin";?>"><i class="fa fa-circle-o"></i>Jumlah Penduduk Miskin</a></li>
            <li><a href="<?=base_url()."admin/garis_miskin";?>"><i class="fa fa-circle-o"></i>Garis Kemiskinan</a></li>
          </ul>
        </li>
        

        <!-- <li <?php if($this->uri->segment(2) == 'index_pembangunan_manusia') echo "class='active'";?>>
          <a href="<?= base_url()."data/index_pembangunan_manusia/". date("Y");?>">
            <span>Index Pembangunan Manusia</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'index') echo "class='active'";?>>
          <a href="#">
            <span>Index Pendidikan, Kesehatan dan Daya Beli</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'jumlah_penduduk_miskin') echo "class='active'";?>>
          <a href="<?= base_url()."data/jumlah_penduduk_miskin/". date("Y");?>">
            <span>Jumlah Penduduk Miskin</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'pengeluaran_perkapita') echo "class='active'";?>>
          <a href="<?= base_url()."data/pengeluaran_perkapita/". date("Y");?>">
            <span>Pengeluaran Perkapita dan Garis Kemiskinan</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'kepandudukan_jk') echo "class='active'";?>>
          <a href="<?= base_url()."data/kepandudukan_jk/". date("Y");?>">
            <span>Jumlah Penduduk menurut Jenis Kelamin</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'kependudukan_kel_umur') echo "class='active'";?>>
          <a href="<?= base_url()."data/kependudukan_kel_umur/". date("Y");?>">
            <span>Jumlah Penduduk Kota Malang menurut Kelompok Umur</span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2) == 'rasio_ketergantungan') echo "class='active'";?>>
          <a href="<?= base_url()."data/rasio_ketergantungan/". date("Y");?>">
            <span>Rasio Jenis Kelamin Penduduk</span>
          </a>
        </li> -->
        
        
    </ul>
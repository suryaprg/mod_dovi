 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->

        <div class="col-md-7">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Jumlah Hasil Peterakan</h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/pertanian/Adminpertanianternakjml/insert"); ?>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Hasil Peternakan</label>
                      <select name="id_jenis" id="id_jenis" class="form-control">

                        <?php
                          if($list_pertanian_jenis){
                            $no = 1;
                            foreach ($list_pertanian_jenis as $r_data => $v_data) {
                              echo "<option value=\"".$v_data->id_jenis."\">".$v_data->nama_jenis."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Total Hasil Peternakan (Ton)</label>
                      <input type="text" class="form-control" id="jml" name="jml" placeholder="Total Hasil Peternakan (Ton)" required="">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        <!-- </div> -->
        <!--/.col (left) -->

        

      <!-- </div> -->
      
      <!-- <div class="row"> -->
        <!-- left column -->
        <!-- <div class="col-md-8"> -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kec"><i class="fa fa-plus"></i>&nbsp;&nbsp; Tambah Data Jumlah Panen Komoditi (Ton)</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Tahun</th>
                  <th>Jenis Komoditi</th>
                  <th>Kategori</th>
                  <th>Total Hasil Peternakan (Ton)</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  if($list_pertanian){
                      $no = 1;

                      foreach ($list_pertanian as $r_data => $v_data) {
                      	$kategori = "Telur";
                        if($v_data->kategori == 0){
                          $kategori = "Daging Non Unggas";
                        }elseif ($v_data->kategori == 1){
                          $kategori = "Daging Unggas";
                        }
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_data->th."</td>
                                <td>".$v_data->nama_jenis."</td>
                                <td>".$kategori."</td>
                                <td>".$v_data->jml."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_ken('".$v_data->id_ternak."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_ken('".$v_data->id_ternak."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                    $no++;
                    }

                  }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <div class="col-md-5">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Jenis Hasil Peternakan</h3>
              <!-- <button type="button" class="btn btn-success pull-right" id="add_kec" data-toggle="modal" data-target="#modal_in_kel"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data Jenis Keuangan Daerah</button> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Ternak</th>
                  <th>Kategori</th>
                  <th>Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php echo form_open("admin_opd/pertanian/Adminpertanianternakjenis/insert"); ?>
                      <td colspan="2">
                      	<input type="text" class="form-control" id="nama_jenis" name="nama_jenis">
                      </td>
                      <td>
                      	<select class="form-control" id="kategori" name="kategori">
                      		<option value="0">Daging Non Unggas</option>
                          <option value="1">Daging Unggas</option>
                      		<option value="2">Telur</option>
                      	</select>
                      </td>
                      <td><button type="submit" class="btn btn-success pull-right" id="add_jenis_keu" name="add_jenis_keu"><i class="fa fa-plus"></i>&nbsp;&nbsp; Simpan</button></td>
                    </form>
                  </tr>
                  <?php
                    if($list_pertanian_jenis){
                      $no = 1;
                      foreach ($list_pertanian_jenis as $r_list_pertanian_jenis => $v_list_pertanian_jenis) {
                      	$kategori = "Telur";
                      	if($v_list_pertanian_jenis->kategori == 0){
                      		$kategori = "Daging Non Unggas";
                      	}elseif ($v_list_pertanian_jenis->kategori == 1){
                          $kategori = "Daging Unggas";
                        }
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_pertanian_jenis->nama_jenis."</td>
                                <td>".$kategori."</td>
                               
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data_jenis('".$v_list_pertanian_jenis->id_jenis."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data_jenis('".$v_list_pertanian_jenis->id_jenis."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                    
                  ?>
                  
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row --> 
      
    </div>

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


        <div class="modal fade" id="modal_up_jenis">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Jenis Hasil Peternakan</h4>
              </div>
              <?php echo form_open("admin_opd/pertanian/Adminpertanianternakjenis/update"); ?>
              <div class="modal-body">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Keterangan</label>
                              <input type="text" class="form-control" id="_nama_jenis" name="nama_jenis" placeholder="Keterangan" required="">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Kategori</label>
                              <select class="form-control" id="_kategori" name="kategori">
                                <option value="0">Daging Non Unggas</option>
                                <option value="1">Daging Unggas</option>
                                <option value="2">Telur</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-12" hidden="">
                            <div class="form-group">
                              <label for="exampleInputPassword1">id jenis</label>
                              <input type="text" class="form-control" id="_id_jenis" name="id_jenis" placeholder="Jumlah RW" required="" readonly="">
                            </div>
                          </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------- Komoditi ---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        <div class="modal fade" id="modal_up_ken">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Jumlah Hasil Peterakan (Ton)</h4>
              </div>
              <?php echo form_open("admin_opd/pertanian/Adminpertanianternakjml/update/"); ?>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Hasil Peternakan</label>
                      <select name="id_jenis" id="_id_jenis_ex" class="form-control">

                        <?php
                          if($list_pertanian_jenis){
                            $no = 1;
                            foreach ($list_pertanian_jenis as $r_data => $v_data) {
                              echo "<option value=\"".$v_data->id_jenis."\">".$v_data->nama_jenis."</option>";
                              $no++;
                            }
                          }
                          
                        ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tahun</label>
                      <input type="number" class="form-control" id="_th" name="th" placeholder="Tahun" required="">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Total Hasil Peterakan (Ton)</label>
                      <input type="text" class="form-control" id="_jml" name="jml" placeholder="Total Hasil Peterakan (Ton)" required="">
                    </div>
                  </div>

                  <div class="col-md-6" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_ternak</label>
                      <input type="text" class="form-control" id="_id_ternak" name="id_ternak" readonly="" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- ------------------------------------------------------------- Komoditi ---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    


    <script type="text/javascript">


      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_jenis(id_jenis){
        clear_mod_up_jenis();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_jenis', id_jenis);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/pertanian/Adminpertanianternakjenis/index_up";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update_jenis(res);
            }
        });
        
        $("#modal_up_jenis").modal('show');
      }

      function res_update_jenis(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_jenis").val(main_data.id_jenis);
              $("#_kategori").val(main_data.kategori);
              $("#_nama_jenis").val(main_data.nama_jenis);
              
              
          }else{
              clear_mod_up_jenis();
          }
      }

      function clear_mod_up_jenis(){
          $("#_id_jenis").val("");
          $("#_kategori").val("");
          $("#_nama_jenis").val("");
         
      }

      // var admin_upx = "";
      function del_data_jenis(id_jenis){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_jenis+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_jenis+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/pertanian/Adminpertanianternakjenis/delete/";?>"+id_jenis;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Jenis---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Komoditi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data_ken(id_ternak){
        clear_mod_up_ken();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_ternak', id_ternak);    
          $.ajax({
            url: "<?php echo base_url()."/admin_opd/pertanian/Adminpertanianternakjml/index_up/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                res_update_ken(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up_ken").modal('show');
      }

      function res_update_ken(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_ternak").val(main_data.id_ternak);
              $("#_id_jenis_ex").val(main_data.id_jenis);
              $("#_th").val(main_data.th);
              $("#_jml").val(main_data.jml);
              
          }else{
              clear_mod_up_ken();
          }
      }

      function clear_mod_up_ken(){
          $("#_id_ternak").val("");
          $("#_id_jenis_ex").val("");
          $("#_th").val("");
          $("#_jml").val("");
      }

      // var admin_upx = "";
      function del_data_ken(id_ternak){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_ternak+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_ternak+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/pertanian/Adminpertanianternakjml/delete/";?>"+id_ternak;
        }else{

        }
      }

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Komoditi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
    
      

    </script>
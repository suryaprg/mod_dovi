 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data Produksi Pertanian Kelapa dan Tebu</h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/pertanian/Adminpertaniankelapatebu/insert"); ?>
              <div class="box-body">
                <div class="row">

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>

                   <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Hasil Panen Kelapa (TON)</label>
                      <input type="text" class="form-control" id="jml_kelapa" name="jml_kelapa" placeholder="Jumlah Hasil Panen Kelapa (TON)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Hasil Panen Tebu (TON)</label>
                      <input type="text" class="form-control" id="jml_tebu" name="jml_tebu" placeholder="Jumlah Hasil Panen Tebu (TON)" required="">
                    </div>
                  </div>

                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Produksi Pertanian Kelapa dan Tebu</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Periode (Tahun)</th>
                  <th>Jumlah Hasil Panen Kelapa (TON)</th>
                  <th>Jumlah Hasil Panen Tebu (TON)</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_pertanian){
                      $no = 1;
                      foreach ($list_pertanian as $R_list_pertanian => $v_list_pertanian) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_pertanian->th."</td>
                                <td>".$v_list_pertanian->jml_kelapa."</td>
                                <td>".$v_list_pertanian->jml_tebu."</td>
                                
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data('".$v_list_pertanian->id_pertanian."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data('".$v_list_pertanian->id_pertanian."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->       
      </div>
      <!-- /.row -->      
    </div>

    

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        

        <div class="modal fade" id="modal_up">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Produksi Pertanian Kelapa dan Tebu</h4>
              </div>
              <?php echo form_open("admin_opd/pertanian/Adminpertaniankelapatebu/update"); ?>
              <div class="modal-body">
                <div class="row">
                  
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="_th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>

                   <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Hasil Panen Kelapa (TON)</label>
                      <input type="text" class="form-control" id="_jml_kelapa" name="jml_kelapa" placeholder="Jumlah Hasil Panen Kelapa (TON)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jumlah Hasil Panen Tebu (TON)</label>
                      <input type="text" class="form-control" id="_jml_tebu" name="jml_tebu" placeholder="Jumlah Hasil Panen Tebu (TON)" required="">
                    </div>
                  </div>

                  <div class="col-md-12" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_peng</label>
                      <input type="text" class="form-control" id="_id_pertanian" name="id_pertanian" readonly="" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

<script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data(id_pertanian){
        clear_mod_up();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_pertanian', id_pertanian);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/pertanian/Adminpertaniankelapatebu/index_up";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up").modal('show');
      }

      function res_update(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_pertanian").val(main_data.id_pertanian);
              $("#_th").val(main_data.th);
              $("#_jml_kelapa").val(main_data.jml_kelapa);
              $("#_jml_tebu").val(main_data.jml_tebu);
              
          }else{
              clear_mod_up();
          }
      }

      function clear_mod_up(){
          $("#_id_pertanian").val("");
          $("#_th").val("");
          $("#_jml_kelapa").val("");
          $("#_jml_tebu").val("");         
      }

      // var admin_upx = "";
      function del_data(id_pertanian){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_pertanian+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_pertanian+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/pertanian/Adminpertaniankelapatebu/delete/";?>"+id_pertanian;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


      

    </script>
 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Data Lahan Pertanian</h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_opd/pertanian/Adminpertanianlahan/insert"); ?>
              <div class="box-body">
                <div class="row">

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jenis Lahan</label>
                      <select id="jenis_lahan" name="jenis_lahan" class="form-control">
                        <option value="Lahan Pertanian Sawah">Lahan Pertanian Sawah</option>
                        <option value="Lahan Pertanian Bukan Sawah">Lahan Pertanian Bukan Sawah</option>
                        <option value="Lahan Bukan Pertanian">Lahan Bukan Pertanian</option> 
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Prosentase Lahan (%)</label>
                      <input type="text" class="form-control" id="luas_lahan" name="luas_lahan" placeholder="Prosentase Lahan" required="">
                    </div>
                  </div>

                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
      
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Lahan Pertanian</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Lahan</th>
                  <th>Periode (Tahun)</th>
                  <th>Prosentase Lahan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_pertanian){
                      $no = 1;
                      foreach ($list_pertanian as $R_list_pertanian => $v_list_pertanian) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_pertanian->jenis_lahan."</td>
                                <td>".$v_list_pertanian->th."</td>
                                <td>".$v_list_pertanian->luas_lahan."</td>
                                
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data('".$v_list_pertanian->id_lahan."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data('".$v_list_pertanian->id_lahan."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->       
      </div>
      <!-- /.row -->      
    </div>

    

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
        

        <div class="modal fade" id="modal_up">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah Data Lahan Pertanian</h4>
              </div>
              <?php echo form_open("admin_opd/pertanian/Adminpertanianlahan/update"); ?>
              <div class="modal-body">
                <div class="row">
                  
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Jenis Lahan</label>
                      <select id="_jenis_lahan" name="jenis_lahan" class="form-control">
                        <option value="Lahan Pertanian Sawah">Lahan Pertanian Sawah</option>
                        <option value="Lahan Pertanian Bukan Sawah">Lahan Pertanian Bukan Sawah</option>
                        <option value="Lahan Bukan Pertanian">Lahan Bukan Pertanian</option> 
                      </select>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Periode (Tahun)</label>
                      <input type="number" class="form-control" id="_th" name="th" placeholder="Periode (Tahun)" required="">
                      <!-- <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode (Bulan)" required=""> -->
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Prosentase Lahan (%)</label>
                      <input type="text" class="form-control" id="_luas_lahan" name="luas_lahan" placeholder="Prosentase Lahan" required="">
                    </div>
                  </div>

                  <div class="col-md-12" hidden="">
                    <div class="form-group">
                      <label for="exampleInputPassword1">id_lahan</label>
                      <input type="text" class="form-control" id="_id_lahan" name="id_lahan" readonly="" required="">
                    </div>
                  </div>
                </div>
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    

<script type="text/javascript">
      

    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    
      function up_data(id_lahan){
        clear_mod_up();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_lahan', id_lahan);    
          $.ajax({
            url: "<?php echo base_url()."admin_opd/pertanian/Adminpertanianlahan/index_up";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up").modal('show');
      }

      function res_update(res){
          var data = JSON.parse(res);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              $("#_id_lahan").val(main_data.id_lahan);
              $("#_jenis_lahan").val(main_data.jenis_lahan);
              $("#_th").val(main_data.th);
              $("#_luas_lahan").val(main_data.luas_lahan);
              
          }else{
              clear_mod_up();
          }
      }

      function clear_mod_up(){
          $("#_id_lahan").val("");
          $("#_jenis_lahan").val("");
          $("#_th").val("");
          $("#_luas_lahan").val("");         
      }

      // var admin_upx = "";
      function del_data(id_lahan){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_lahan+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_lahan+" akan terhapus semua... ");
        if(conf){
          window.location.href = "<?= base_url()."admin_opd/pertanian/Adminpertanianlahan/delete/";?>"+id_lahan;
        }else{

        }
      }


    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------Tehnologi---------------------------------------------------------------- -->
    //<!-- -------------------------------------------------------------------------------------------------------------------------------------- -->


      

    </script>
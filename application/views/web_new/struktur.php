<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Malang Open Data</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>web_new/img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>web_new/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/fonts/font-awesome/css/font-awesome.css">

<!-- Slider
    ================================================== -->
<link href="<?php echo base_url(); ?>web_new/css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url(); ?>web_new/css/owl.theme.css" rel="stylesheet" media="screen">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/nivo-lightbox/default.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
 <!-- <script src="<?php echo base_url(); ?>web_new/js/jssor.slider-27.5.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:800,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $SpacingX: 5,
                $SpacingY: 5
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script> -->
   <!--  <style>
        /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }

        /*jssor slider arrow skin 106 css*/
        .jssora106 {display:block;position:absolute;cursor:pointer;}
        .jssora106 .c {fill:#fff;opacity:.3;}
        .jssora106 .a {fill:none;stroke:#000;stroke-width:350;stroke-miterlimit:10;}
        .jssora106:hover .c {opacity:.5;}
        .jssora106:hover .a {opacity:.8;}
        .jssora106.jssora106dn .c {opacity:.2;}
        .jssora106.jssora106dn .a {opacity:1;}
        .jssora106.jssora106ds {opacity:.3;pointer-events:none;}

        /*jssor slider thumbnail skin 101 css*/
        .jssort101 .p {position: absolute;top:0;left:0;box-sizing:border-box;background:#000;}
        .jssort101 .p .cv {position:relative;top:0;left:0;width:100%;height:100%;border:2px solid #000;box-sizing:border-box;z-index:1;}
        .jssort101 .a {fill:none;stroke:#fff;stroke-width:400;stroke-miterlimit:10;visibility:hidden;}
        .jssort101 .p:hover .cv, .jssort101 .p.pdn .cv {border:none;border-color:transparent;}
        .jssort101 .p:hover{padding:2px;}
        .jssort101 .p:hover .cv {background-color:rgba(0,0,0,6);opacity:.35;}
        .jssort101 .p:hover.pdn{padding:0;}
        .jssort101 .p:hover.pdn .cv {border:2px solid #fff;background:none;opacity:.35;}
        .jssort101 .pav .cv {border-color:#fff;opacity:.35;}
        .jssort101 .pav .a, .jssort101 .p:hover .a {visibility:visible;}
        .jssort101 .t {position:absolute;top:0;left:0;width:100%;height:100%;border:none;opacity:.6;}
        .jssort101 .pav .t, .jssort101 .p:hover .t{opacity:1;}
    </style>
HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]--> 
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand page-scroll" href="#about"><img src="<?php echo base_url(); ?>web_new/img/logo.png" alt=" " height="40" width="150"></a> </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url(); ?>beranda" class="page-scroll">Data</a></li>
        
        <li><a href="<?php echo base_url(); ?>beranda#testimonials" class="page-scroll">Gallery</a></li>
        <li><a href="<?php echo base_url(); ?>beranda/tentang" class="page-scroll">Tentang</a></li>
        <li><a href="#services" class="page-scroll">Struktur Organisasi</a></li>
        <!-- <li><a href="#contact" class="page-scroll">Contact</a></li> -->
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>
<!-- Header -->
<!-- Header -->
<header id="header">
  <div class="intro-2">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 intro-text">
            <h1>Struktur Organisasi</h1>
            <p> Penjelasan singkat mengenai organisasi kantor <br> Dinas Komunikasi dan Informatika Kota Malang</p>
            <a href="#services" class="btn btn-primary btn-lg page-scroll">ENTER</a> </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- About Section -->

<!-- Gallery Section -->
<div id="services">
  <div class="container">
    <div class="col-md-10 col-md-offset-1 section-title text-center">
      <h2>Kepala Dinas</h2>
      <hr>
      
    </div>
    <div class="row">
      <div class="text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/kepala.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>ZULKIFLI AMRIZAL, S.SOS, M.SI</h3>
          <p>Kepala Dinas Komunikasi dan Informatika Kota Malang</p>
        </div>
      </div>
      <br>
    <br>
      <div class="col-md-10 col-md-offset-1 section-title text-center">
      <h2>Sekretaris</h2>
      <hr>
    </div>
      <div class="text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/yayuk.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>IR. YAYUK HERMIATI, MH</h3>
          <p>SEKRETARIS</p>
        </div>
      </div>
    </div>
        <div class="row">
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/seksuhartini.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>DRA. EC. SRI SUHARTINI</h3>
          <p>Sub Bagian Penyusunan Program</p>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/sekprihatini.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>PRIHATINI SETYANINGSIH, SE, MM</h3>
          <p>Sub Bagian Keuangan</p>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/seksrimulyani.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>SRI MULYANI, SH</h3>
          <p>Bag. Umum</p>
        </div>
      </div>
    </div>
    <br>
    <br>
      <div class="col-md-10 col-md-offset-1 section-title text-center">
      <h2>Bidang Statistik</h2>
      <hr>
    </div>
     <div class="row">
      <div class="text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/stasukaryo.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>DRS. SUKARYONO</h3>
          <p>Kepala Bidang Statistik</p>
        </div>
      </div>
    </div>
        <div class="row">
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/statitin.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>BASTIAN PADMA H., SE, MM</h3>
          <p>Seksi Pengelolaan Data Statistik</p>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/staerna.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>DRA. ERNA WIYATI</h3>
          <p>Seksi Layanan Data dan Informasi Statistik</p>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/stanur.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>DRA. NUR CHAMIDAH</h3>
          <p>Seksi Evaluasi dan Pelaporan</p>
        </div>
      </div>
    </div>
      <br>
    <br>
      <div class="col-md-10 col-md-offset-1 section-title text-center">
      <h2>Bidang Aplikasi Informatika</h2>
      <hr>
    </div>
     <div class="row">
      <div class="text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/titis.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>IR. TITIS ANDAYANI, MM</h3>
          <p>Bidang Aplikasi Informatika</p>
        </div>
      </div>
    </div>
        <div class="row">
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/ita.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>ITA NURDIYAH,S.KOM</h3>
          <p>Seksi Pengelolaan E-Gov</p>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/zulkifli.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>ZULKIFLI EKO PRIONO,BSC</h3>
          <p>Seksi Pemberdayaan Teknologi Informasi dan Komunikasi</p>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/didik.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>DIDIK SUPRIYADI</h3>
          <p>Seksi Sarana Prasarana Informatika</p>
        </div>
      </div>
    </div>
      <br>
    <br>
      <div class="col-md-10 col-md-offset-1 section-title text-center">
      <h2>Bidang Komunikasi Publik dan Persandian</h2>
      <hr>
     
    </div>
     <div class="row">
      <div class="text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/agus.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>IR. AGUS TRI PUTRANTO, MT</h3>
          <p>Bidang Komunikasi Publik dan Persandian</p>
        </div>
      </div>
    </div>
        <div class="row">
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/ridwan.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>MOHAMAD RIDWAN, STP,MM</h3>
          <p>Seksi Sarana Prasarana dan Komunikasi Publik</p>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/febrian.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>FEBRIAN RETNOSARI,S.SOS,M.SI</h3>
          <p>Seksi Pengelolaan dan Kemitraan Komunikasi Publik</p>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/ardianti.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>DRA. MULYO ARDIANTI</h3>
          <p>Seksi Persandian</p>
        </div>
      </div>
    </div>
      <br>
    <br>
      <div class="col-md-10 col-md-offset-1 section-title text-center">
      <h2>Bidang Informasi Publik</h2>
      <hr>
    </div>
     <div class="row">
      <div class="text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/ismintarti.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>ISMINTARI, S.P</h3>
          <p>Bidang Informasi Publik</p>
        </div>
      </div>
    </div>
        <div class="row">
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/laode.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>LAODE KB AL FITRA, SP,MM</h3>
          <p>Seksi Pengelolaan Informasi Publik</p>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/dedy.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>DEDY SURFIANTO, SE</h3>
          <p>Seksi Layanan Informasi Publik</p>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/djoko.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>DJOKO PURNOMO</h3>
          <p>Seksi Pemberdayaan dan Kemitraan Informasi</p>
        </div>
      </div>
    </div>
    <br>
    <br>
    <div class="col-md-10 col-md-offset-1 section-title text-center">
      <h2>UPT</h2>
      <hr>
    </div>
    <div class="row">
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/redy.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>REDY DJANANDJAJA SUSELOHADI,SH,MM</h3>
          <p>Kepala UPT LPSE</p>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/erkan.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>ERKAN WIKANTO,S.SOS, M.SI</h3>
          <p>Kasubbag Tata Usaha</p>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="service-media"> <img src="<?php echo base_url(); ?>web_new/img/foto/wahyu.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>M. WAHYU HIDAYAT, S.KOM,MM</h3>
          <p>Kepala UPT Command Center</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="footer">
<div class="footer w3ls">
  <div class="container">
    <div class="footer-main">
      <div class="footer-top">
        <div class="col-md-5 ftr-grid fg1">
          <h3><a href="index.html">Petunjuk Arah</a></h3>
          <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.6782028920125!2d112.6409980537108!3d-8.03207598675561!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd627eab4241a03%3A0x6763c56ee92a8ada!2sPerkantoran+Terpadu+Malang!5e0!3m2!1sid!2sid!4v1541818835217" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></p>
        </div>
        <div class="col-md-3 ftr-grid fg2 mid-gd">
          <h3>Hubungi Kami</h3>
          <div class="ftr-address">
            <div class="local">
              <i class="fa fa-map-marker" aria-hidden="true"></i>
            </div>
            <div class="ftr-text">
              <p>Perkantoran Terpadu Pemerintah Kota Malang, Gedung A Lantai 4.</p>
            </div>
            <div class="clearfix"> </div>
          </div>
          <div class="ftr-address">
            <div class="local">
              <i class="fa fa-phone" aria-hidden="true"></i>
            </div>
            <div class="ftr-text">
              <p>(0341)751550</p>
            </div>
            <div class="clearfix"> </div>
          </div>
          <div class="ftr-address">
            
            <div class="ftr-text">
              <div class="local">
              <i class="fa fa-envelope" aria-hidden="true"></i>
            </div> 
              <p><a href="mailto:kominfo.malangkota.go.id">kominfo@malangkota.go.id</a></p>
            </div>
            <br><br>
           <!--  <div class="w3l-social">
                    <ul>
                      <li><a href="https://www.facebook.com/mediacenter.malangkota"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="https://twitter.com/PemkotMalang"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="https://www.instagram.com/pemkotmalang/"><i class="fa fa-instagram"></i></a></li>
                      <li><a href="https://www.youtube.com/user/mediacentermalang"><i class="fa fa-youtube-play"></i></a></li>
                      <li><a href="https://plus.google.com/+PemerintahKotaMalang"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                  </div> -->
            <div class="clearfix"> </div>
          </div>
        </div>
        <div class="col-md-4 ftr-grid fg1">
          <h3>Tentang</h3>
              <p align="justify">Lahir sebagai cita cita Pemerintah Kota Malang untuk menyediakan satu basis data pembangunan yang akurat, terbuka, terpusat dan terintegrasi dan mewujudkan pelayanan publik yang prima</p>
            

          
        </div>
         <div class="clearfix"> </div>
      </div>
      
    </div>
  </div>
</div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/bootstrap.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/SmoothScroll.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/nivo-lightbox.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/jquery.isotope.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/owl.carousel.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/contact_me.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>web_new/js/main.js"></script>
</body>
</html>
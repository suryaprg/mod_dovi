
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">KATEGORI</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."super/admin";?>"><i class="fa fa-circle-o"></i>Data Admin</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Partai</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."super/partai";?>"><i class="fa fa-circle-o"></i>Data Partai</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kecamatan Kelurahan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."super/kelkec";?>"><i class="fa fa-circle-o"></i>Data Kecamatan dan Kelurahan</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Geografi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/iklim";?>"><i class="fa fa-circle-o"></i>Data Iklim dan Curah Hujan</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Pemerintah</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/rt_rw";?>"><i class="fa fa-circle-o"></i>RT dan RW</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Ekonomi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/keuangan_daerah";?>"><i class="fa fa-circle-o"></i>keuangan_daerah</a></li>
            <li><a href="<?=base_url()."admin/pendapatan_regional";?>"><i class="fa fa-circle-o"></i>pendapatan_regional</a></li>
            <li><a href="<?=base_url()."admin/kegiatan_usaha";?>"><i class="fa fa-circle-o"></i>kegiatan_usaha</a></li>
            <li><a href="<?=base_url()."admin/pengeluaran_penduduk";?>"><i class="fa fa-circle-o"></i>pengeluaran_penduduk</a></li>
            <li><a href="<?=base_url()."admin/pengeluaran_rumah_tangga";?>"><i class="fa fa-circle-o"></i>pengeluaran_rumah_tangga</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kependudukan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/kependudukan_jk";?>"><i class="fa fa-circle-o"></i>Berdasarkan Jenis Kelamin</a></li>
            <li><a href="<?=base_url()."admin/kependudukan_umur";?>"><i class="fa fa-circle-o"></i>Berdasarkan Umur</a></li>
            <li><a href="<?=base_url()."admin/kependudukan_ketergantungan";?>"><i class="fa fa-circle-o"></i>Index Ketergantungan</a></li>
            <li><a href="<?=base_url()."admin/kependudukan_ipm";?>"><i class="fa fa-circle-o"></i>IPM</a></li>
            <li><a href="<?=base_url()."admin/kependudukan_ipm_bid";?>"><i class="fa fa-circle-o"></i>Pengembangan IPM</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Ketenaga Kerjaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/kerja_angkatan";?>"><i class="fa fa-circle-o"></i>Angkatan Kerja</a></li>
            <li><a href="<?=base_url()."admin/kerja_pengangguran";?>"><i class="fa fa-circle-o"></i>Pengangguran</a></li>
            <li><a href="<?=base_url()."admin/kerja_ump";?>"><i class="fa fa-circle-o"></i>Upah</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Tehnologi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/tehno_bts";?>"><i class="fa fa-circle-o"></i>BTS</a></li>
            <li><a href="<?=base_url()."admin/tehno_warnet";?>"><i class="fa fa-circle-o"></i>Warnet</a></li>
            <li><a href="<?=base_url()."admin/tehno_web";?>"><i class="fa fa-circle-o"></i>Website OPD</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kesehatan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/kesehatan_gizi";?>"><i class="fa fa-circle-o"></i>Gizi Balita</a></li>
            <li><a href="<?=base_url()."admin/kesehatan_penyakit";?>"><i class="fa fa-circle-o"></i>Penyakit</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Transportasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/trans_kendaraan";?>"><i class="fa fa-circle-o"></i>Kendaraan</a></li>
            <li><a href="<?=base_url()."admin/trans_jalan";?>"><i class="fa fa-circle-o"></i>Jalan</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Pertanian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/pertanian_lahan";?>"><i class="fa fa-circle-o"></i>Lahan Berdasarkan Jenis</a></li>
            <li><a href="<?=base_url()."admin/pertanian_lahan_penggunaan";?>"><i class="fa fa-circle-o"></i>Lahan Berdasarkan Penggunaan</a></li>
            <li><a href="<?=base_url()."admin/kelapa_tebu";?>"><i class="fa fa-circle-o"></i>Kelapa dan Tebu</a></li>
            <li><a href="<?=base_url()."admin/pertanian_komoditi";?>"><i class="fa fa-circle-o"></i>Komoditi</a></li>
            <li><a href="<?=base_url()."admin/pertanian_hortikultura";?>"><i class="fa fa-circle-o"></i>Hortikultura</a></li>
            <li><a href="<?=base_url()."admin/peternakan";?>"><i class="fa fa-circle-o"></i>Peternakan</a></li>
            <li><a href="<?=base_url()."admin/perikanan";?>"><i class="fa fa-circle-o"></i>Perikanan</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Pendidikan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/pendidikan_baca_tulis";?>"><i class="fa fa-circle-o"></i>Baca dan Tulis</a></li>
            <li><a href="<?=base_url()."admin/pendidikan_partisipasi_sklh";?>"><i class="fa fa-circle-o"></i>Partisipasi Sekolah</a></li>
            <li><a href="<?=base_url()."admin/pendidikan_penduduk";?>"><i class="fa fa-circle-o"></i>Pendidikan Penduduk</a></li>
            <li><a href="<?=base_url()."admin/jumlah_sekolah";?>"><i class="fa fa-circle-o"></i>Jumlah Sekolah</a></li>
            <li><a href="<?=base_url()."admin/rasio_guru_murid";?>"><i class="fa fa-circle-o"></i>Rasio Guru dan Murid</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kemiskinan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/penduduk_miskin";?>"><i class="fa fa-circle-o"></i>Jumlah Penduduk Miskin</a></li>
            <li><a href="<?=base_url()."admin/garis_miskin";?>"><i class="fa fa-circle-o"></i>Garis Kemiskinan</a></li>
          </ul>
        </li>
        
      </ul>

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">LAMPIRAN</li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Aparat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/aparat";?>"><i class="fa fa-circle-o"></i>Data LP Aparat</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Bencana</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/bencana";?>"><i class="fa fa-circle-o"></i>Data LP Bencana</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Industri</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/industri";?>"><i class="fa fa-circle-o"></i>Data Pendapatan Industri</a></li>
          </ul>
        </li>        

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kajian Penelitian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/kajian_penelitian";?>"><i class="fa fa-circle-o"></i>Data LP Kajian Penelitian</a></li>
          </ul>
        </li>  

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kendaraan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/kendaraan";?>"><i class="fa fa-circle-o"></i>Data Kendaraan Kecamatan</a></li>
            <li><a href="<?=base_url()."admin/kendaraan/plat";?>"><i class="fa fa-circle-o"></i>Data Kendaraan Plat</a></li>
            <li><a href="<?=base_url()."admin/kendaraan/umum";?>"><i class="fa fa-circle-o"></i>Data Kendaraan Umum</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kesehatan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/kesehatan";?>"><i class="fa fa-circle-o"></i>Data Kesehatan</a></li>
            <li><a href="<?=base_url()."admin/kesehatan/jenis";?>"><i class="fa fa-circle-o"></i>Data Kesehatan Jenis</a></li>
            <li><a href="<?=base_url()."admin/kesehatan/sub";?>"><i class="fa fa-circle-o"></i>Data Kesehatan Sub Jenis</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Koperasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/koperasi";?>"><i class="fa fa-circle-o"></i>Data Koperasi</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Keuangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/keuangan";?>"><i class="fa fa-circle-o"></i>Data Keuangan</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Pendidikan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/pendidikan";?>"><i class="fa fa-circle-o"></i>Data Pendidikan</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Perpustakaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/perpustakaan";?>"><i class="fa fa-circle-o"></i>Data Perpustakaan</a></li>
          </ul>
        </li>                      

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Umur</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/umur";?>"><i class="fa fa-circle-o"></i>Data LP Kel Umur</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Tenaga</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/tenaga";?>"><i class="fa fa-circle-o"></i>Data LP Tenaga</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Terminal</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/terminal";?>"><i class="fa fa-circle-o"></i>Data LP Terminal</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>TPA</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()."admin/tpa";?>"><i class="fa fa-circle-o"></i>Data LP TPA</a></li>
          </ul>
        </li>                        
      </ul>
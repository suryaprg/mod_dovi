 <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <div class="content body" style="margin-right: 3em; margin-left: 3em;">
      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Admin Super</h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("admin_super/mainadmin/insert_admin"); ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Level admin</label>
                  <select name="admin_lv" id="admin_lv" class="form-control">
                  <?php
                    if($list_admin_lv){
                      foreach ($list_admin_lv as $r_list_admin_lv => $v_list_admin_lv) {
                        echo "<option value=\"".$v_list_admin_lv->id_lv."\">".$v_list_admin_lv->ket."</option>";
                      }
                    }
                  ?>
                    
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Dinas</label>
                  <select name="id_dinas" id="id_dinas" class="form-control">
                  <?php
                    if($list_dinas){
                      foreach ($list_dinas as $r_list_dinas => $v_list_dinas) {
                        echo "<option value=\"".$v_list_dinas->id_dinas."\">".$v_list_dinas->nama_dinas."</option>";
                      }
                    }
                  ?>
                    
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Username</label>
                  <input type="text" class="form-control" id="user" name="user" placeholder="Username" required="">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Nama</label>
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required="">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" id="pass" name="pass" placeholder="Password" required="">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Ulangi Password</label>
                  <input type="password" class="form-control" id="re_pass" name="re_pass" placeholder="Ulangi Password" required="">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">NIP</label>
                  <input type="text" class="form-control" id="nip" name="nip" placeholder="NIP" required="">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Jabatan</label>
                  <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Jabatan" required="">
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Admin</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>lv. Admin</th>
                  <th>User</th>
                  <th>Nama</th>
                  <th>Nip</th>
                  <th>Jabatan</th>
                  <th>Dinas</th>
                  <th>Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                  <?php
                    if($list_admin){
                      $no = 1;
                      foreach ($list_admin as $r_list_admin => $v_list_admin) {
                        echo "<tr>
                                <td>".$no."</td>
                                <td>".$v_list_admin->ket."</td>
                                <td>".$v_list_admin->email."</td>
                                <td>".$v_list_admin->nama."</td>
                                <td>".$v_list_admin->nip."</td>
                                <td>".$v_list_admin->jabatan."</td>
                                <td>".$v_list_admin->nama_dinas."</td>
                                
                                <td align=\"center\">
                                    <a href=\"#\" class=\"btn btn-warning\" onclick=\"up_data('".$v_list_admin->id_admin."');\"><i class=\"fa fa-pencil\"></i></a>
                                    <a href=\"#\" class=\"btn btn-danger\" onclick=\"del_data('".$v_list_admin->id_admin."');\"><i class=\"fa fa-trash\"></i></a>
                                </td>
                              </tr>";
                          $no++;
                      }
                    }
                  ?>
                </tbody>
                <!-- <tfoot>
                <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
                </tr>
                </tfoot> -->
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->      
    </div>

        <div class="modal fade" id="modal_up">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update Admin Super</h4>
              </div>
              <?php echo form_open("admin_super/mainadmin/up_admin"); ?>
              <div class="modal-body">
                
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <!-- /.box-header -->
                      <!-- form start -->
                      <!-- <form role="form"> -->
                        <div class="box-body">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Level admin</label>
                            <select name="admin_lv" id="up_admin_lv" class="form-control">
                            <?php
                              if($list_admin_lv){
                                foreach ($list_admin_lv as $r_list_admin_lv => $v_list_admin_lv) {
                                  echo "<option value=\"".$v_list_admin_lv->id_lv."\">".$v_list_admin_lv->ket."</option>";
                                }
                              }
                            ?>
                              
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Dinas</label>
                            <select name="id_dinas" id="up_id_dinas" class="form-control">
                            <?php
                              if($list_dinas){
                                foreach ($list_dinas as $r_list_dinas => $v_list_dinas) {
                                  echo "<option value=\"".$v_list_dinas->id_dinas."\">".$v_list_dinas->nama_dinas."</option>";
                                }
                              }
                            ?>
                              
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">User Name</label>
                            <input type="text" class="form-control" id="up_user" name="user" placeholder="User Name" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="up_pass" name="pass" placeholder="Password" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Ulangi Password</label>
                            <input type="password" class="form-control" id="up_re_pass" name="re_pass" placeholder="Ulangi Password" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Nama</label>
                            <input type="text" class="form-control" id="up_nama" name="nama" placeholder="Name" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">NIP</label>
                            <input type="text" class="form-control" id="up_nip" name="nip" placeholder="NIP" required="">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Jabatan</label>
                            <input type="text" class="form-control" id="up_jabatan" name="jabatan" placeholder="Jabatan" required="">
                          </div>

                          <div class="form-group" hidden="">
                            <label for="exampleInputPassword1">Id Admin</label>
                            <input type="text" class="form-control" id="up_id_admin" name="id_admin" placeholder="Jabatan" required="" readonly="">
                          </div>
                          
                        </div>
                        <!-- /.box-body -->
                      
                      
                    </div>
                    <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
                </div>
                <!-- /.row -->
              <!-- </form> -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>

            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


    <script type="text/javascript">
      // var admin_upx = "";
      function del_data(id_admin){
        var conf = confirm("Apakah anda yakin untuk menghapus "+id_admin+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_admin+" akan terhapus semau.. !!!!!!! ");
        if(conf){
          window.location.href = "<?= base_url()."admin_super/mainadmin/delete_admin/";?>"+id_admin;
        }else{

        }
      }

      function up_data(id_admin){
        clear_mod_up();
        // console.lo

        var data_main =  new FormData();
        data_main.append('id_admin', id_admin);    
          $.ajax({
            url: "<?php echo base_url()."/admin_super/mainadmin/index_up_admin/";?>", // point to serv
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                // console.log(res);
                res_update(res);
                // $("#out_up_mhs").html(res);
            }
        });
        
        $("#modal_up").modal('show');
      }

      function res_update(res){
          var data = JSON.parse(res);
                                        // console.log(data);

          if(data.status){
              var main_data = data.val;
              console.log(main_data);
                                            
              
              $("#up_admin_lv").val(main_data.id_lv);
              $("#up_id_dinas").val(main_data.id_dinas);
              $("#up_user").val(main_data.email);
              $("#up_nama").val(main_data.nama);
              $("#up_nip").val(main_data.nip);
              $("#up_jabatan").val(main_data.jabatan);
              $("#up_id_admin").val(main_data.id_admin);

              $("#up_pass").val("");
              $("#up_re_pass").val("");
              
          }else{
              clear_mod_up();
          }
      }

      function clear_mod_up(){
          $("#up_admin_lv").val("");
          $("#up_id_dinas").val("");
          $("#up_user").val("");
          $("#up_nama").val("");
          $("#up_nip").val("");
          $("#up_jabatan").val("");
          $("#up_id_admin").val("");

          $("#up_pass").val("");
          $("#up_re_pass").val("");
      }

    </script>
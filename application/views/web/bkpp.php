<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Malang Open Data</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Auditing Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="<?php echo base_url(); ?>web/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url(); ?>web/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="<?php echo base_url(); ?>web/js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- font-awesome icons -->
<link href="<?php echo base_url(); ?>web/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
<body>
<!-- banner -->	
<!-- navigation -->
<div class="nav-links">	
	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>                        
				</button>
				<a class="navbar-brand" href="index.html"><img src="<?php echo base_url(); ?>web/images/logo.png"></a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav link-effect">
					<li><a href="<?php echo base_url(); ?>web_home/web/index">Home</a></li>
					<li><a href="<?php echo base_url(); ?>web_home/web/data">Data</a></li>
					<li><a href="<?php echo base_url(); ?>web_home/web/visualisasi">Visualisasi</a></li>
					<li class="active" class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span data-hover="Short Codes">Pejabat Struktural</span> <b class="caret"></b></a>
							<ul class="dropdown-menu agile_short_dropdown">
								<li><a href="<?=base_url();?>web_home/web/kepaladinas">Kepala Dinas</a></li>
								<li><a href="<?=base_url();?>web_home/web/sekretaris">Sekretaris</a></li>
								<li><a href="<?=base_url();?>web_home/web/bidangstatistik">Bidang Statistika</a></li>
								<li><a href="<?=base_url();?>web_home/web/aptika">Bidang Aplikasi Informatika</a></li>
								<li><a href="<?=base_url();?>web_home/web/bkpp">Bidang Komunikasi Publik</a></li>
								<li><a href="<?=base_url();?>web_home/web/bip">Bidang Informasi Publik</a></li>
								<li><a href="<?=base_url();?>web_home/web/upt">UPT</a></li>
							</ul>
					</li>
					<li><a href="<?=base_url();?>web_home/web/dokumentasi">Dokumentasi</a></li>
					<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span data-hover="Short Codes">Tentang</span> <b class="caret"></b></a>
							<ul class="dropdown-menu agile_short_dropdown">
								<li><a href="<?=base_url();?>web_home/web/visi">Visi dan Misi</a></li>
								<li><a href="<?=base_url();?>web_home/web/struktur">Struktur Organisasi</a></li>
								<li><a href="<?=base_url();?>web_home/web/tupoksi">Tugas Pokok dan Fungsi</a></li>
							</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</div>
<!-- /navigation -->
		<div class="w3layouts-banner-5">
      </div>
      <div class="team">
			<div class="container">
				<div class="w3ls-heading">
					<h3 class="h-two">Bidang Komunikasi Publik dan Persandian</h3>
				</div>
				<div class="agile-team-grids">
					<div class="col-sm-3 col-sm-offset-4 team-grid">
						<div class="flip-container">
							<div class="flipper">
								<div class="front">
									<img src="<?=base_url();?>web/images/foto/agus.jpg" />
								</div>
								<div class="back">
									<h4>IR. Agus Tri Putranto, MT</h4>
									<p>Bidang Komunikasi Publik dan Persandian</p>
									<div class="w3l-social">
										<ul>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-instagram"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>

	</div>
</div>
		<div class="team">
			<div class="container">
				<div class="w3ls-heading">
					<h3 class="h-two">Kepala Sub Bagian</h3>
				</div>
				<div class="agile-team-grids">
					<div class="col-sm-3 col-sm-offset-2 team-grid">
						<div class="flip-container">
							<div class="flipper">
								<div class="front">
									<img src="<?=base_url();?>web/images/foto/ridwan.jpg" alt="" />
								</div>
								<div class="back">
									<h4>Mohamad Ridwan, STP,MM</h4>
									<p>Seksi Sarana Prasarana dan Komunikasi Publik</p>
									<div class="w3l-social">
										<ul>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-instagram"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3 team-grid">
						<div class="flip-container">
							<div class="flipper">
								<div class="front">
									<img src="<?=base_url();?>web/images/foto/febrian.jpg" alt="" />
								</div>
								<div class="back">
									<h4>Febrian Retnosari,S.SOS,M.SI</h4>
									<p>Seksi Pengelolaan dan Kemitraan Komunikasi Publik</p>
									<div class="w3l-social">
										<ul>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-instagram"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3 team-grid">
						<div class="flip-container">
							<div class="flipper">
								<div class="front">
									<img src="<?=base_url();?>web/images/foto/ardianti.jpg" alt="" />
								</div>
								<div class="back">
									<h4>Dra. Mulyo Ardianti</h4>
									<p>Seksi Persandian</p>
									<div class="w3l-social">
										<ul>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-instagram"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
	</div>
	</div>

	
<!-- //projects -->
<!-- Footer -->
<div class="footer w3ls">
	<div class="container">
		<div class="footer-main">
			<div class="footer-top">
				<div class="col-md-5 ftr-grid fg1">
					<h3><a href="index.html">Petunjuk Arah</a></h3>
					<p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.6782028920125!2d112.6409980537108!3d-8.03207598675561!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd627eab4241a03%3A0x6763c56ee92a8ada!2sPerkantoran+Terpadu+Malang!5e0!3m2!1sid!2sid!4v1541818835217" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></p>
				</div>
				<div class="col-md-3 ftr-grid fg2 mid-gd">
					<h3>Hubungi Kami</h3>
					<div class="ftr-address">
						<div class="local">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
						</div>
						<div class="ftr-text">
							<p>Perkantoran Terpadu Pemerintah Kota Malang, Gedung A Lantai 4.</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="ftr-address">
						<div class="local">
							<i class="fa fa-phone" aria-hidden="true"></i>
						</div>
						<div class="ftr-text">
							<p>(0341)751550</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="ftr-address">
						<div class="local">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</div>
						<div class="ftr-text">
							<p><a href="mailto:kominfo.malangkota.go.id">kominfo@malangkota.go.id</a></p>
						</div>
						<br><br>
						<div class="w3l-social">
										<ul>
											<li><a href="https://www.facebook.com/mediacenter.malangkota"><i class="fa fa-facebook"></i></a></li>
											<li><a href="https://twitter.com/PemkotMalang"><i class="fa fa-twitter"></i></a></li>
											<li><a href="https://www.instagram.com/pemkotmalang/"><i class="fa fa-instagram"></i></a></li>
											<li><a href="https://www.youtube.com/user/mediacentermalang"><i class="fa fa-youtube-play"></i></a></li>
											<li><a href="https://plus.google.com/+PemerintahKotaMalang"><i class="fa fa-google-plus"></i></a></li>
										</ul>
									</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-4 ftr-grid fg1">
					<h3>Tentang</h3>
							<p align="justify">Lahir sebagai cita cita Pemerintah Kota Malang untuk menyediakan satu basis data pembangunan yang akurat, terbuka, terpusat dan terintegrasi dan mewujudkan pelayanan publik yang prima</p>
						

					
				</div>
			   <div class="clearfix"> </div>
			</div>
			
		</div>
	</div>
</div>
	

<!-- Footer -->	

	<!-- start-smoth-scrolling -->
	
<!-- js -->
		<!--//pop-up-box -->
<script type="text/javascript" src="<?=base_url();?>web/js/jquery-2.1.4.min.js"></script>

<!-- //js -->
<script type="text/javascript" src="<?=base_url();?>web/js/move-top.js"></script>
<script type="text/javascript" src="<?=base_url();?>web/js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<script type="text/javascript" src="<?=base_url();?>web/js/bootstrap.js"></script>
</body>
</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index(){
		redirect(base_url()."beranda");
	}

	public function index_no(){
		$this->load->view("index_no");
	}

	public function cek(){
		if(file_exists("./assets/pdfs")){
			print_r("ada");
		}else {
			print_r("tak ada");
		}
	}

	public function laporan_pdf(){
	    $data = array(
	        "dataku" => array(
	            "nama" => "Petani Kode",
	            "url" => "http://petanikode.com"
	        )
	    );

	    $this->load->library('pdf');
	    $this->pdf->setPaper('A4', 'potrait');
	    $this->pdf->filename = "laporan-petanikode.pdf";
	    $this->pdf->load_view('welcome_message', $data);


	}
}

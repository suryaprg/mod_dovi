<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminrtrw extends CI_Controller {
//ok
	function __construct(){
		parent::__construct();
		$this->load->model("admin_super/main_kecamatan", "mk");
		$this->load->model("admin_opd/Admin_rt_rw", "atw");

		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "rt_rw";
		$data["list_kecamatan"] = $this->mk->get_kec();
		$data["list_rt_rw"] = $this->atw->get_rt_rw();
		$this->load->view('admin_data/pemerintahan/main_admin_pemerintahan', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------rt_rw----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	
	private function validation_ins_rt_rw(){
		$config_val_input = array(
            array(
                'field'=>'id_kec',
                'label'=>'Kecamatan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'jml_rt',
                'label'=>'Jumlah RT',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_rw',
                'label'=>'Jumlah RW',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_rtrw(){
		if($this->validation_ins_rt_rw()){
			$id_kec = $this->input->post("id_kec");
			$jml_rt = $this->input->post("jml_rt");
			$jml_rw = $this->input->post("jml_rw");
			$th = $this->input->post("th");
			
			$data_insert = array(
							"id_rt_rw"=>"",
							"id_kec"=>$id_kec,
							"th"=>$th,
							"jml_rt"=>$jml_rt,
							"jml_rw"=>$jml_rw
						);
			$insert = $this->atw->insert_rt_rw($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/rt_rw");
	}

	public function index_up_rt_rw(){
		$id_rtrw = $this->input->post("id_rtrw");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->atw->get_rt_rw_where(array("id_rt_rw"=>$id_rtrw))->row_array(); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function up_rt_rw(){
		if($this->validation_ins_rt_rw()){
			$id_rtrw = $this->input->post("id_rtrw");

			$id_kec = $this->input->post("id_kec");
			$jml_rt = $this->input->post("jml_rt");
			$jml_rw = $this->input->post("jml_rw");
			$th = $this->input->post("th");
			
			$data_insert = array(
							"id_kec"=>$id_kec,
							"th"=>$th,
							"jml_rt"=>$jml_rt,
							"jml_rw"=>$jml_rw
						);

			$data_where = array(
							"id_rt_rw"=>$id_rtrw
						);

			$insert = $this->atw->update_rt_rw($data_insert, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/rt_rw");
	}

	public function delete_rt_rw($id_rtrw){
		$delete = $this->atw->delete_rt_rw(array("id_rt_rw"=>$id_rtrw));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/rt_rw");
	}

}

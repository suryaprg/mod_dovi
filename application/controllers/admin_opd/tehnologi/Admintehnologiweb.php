<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admintehnologiweb extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_tehnologi_web", "atwb");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "tehno_web";
		$data["list_tehno"] = $this->atwb->get_tehnologi();
		$this->load->view('admin_data/tehnologi/main_admin_tehnologi', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------tehnologi--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'jml',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_tehnologi(){
		if($this->validation_ins()){

			$th = $this->input->post("th");
			$jml_bts = $this->input->post("jml");

			$data = array(
						"id_web_opd"=>"",
						"th"=>$th,
						"jml"=>$jml_bts
					);
			
			$insert = $this->atwb->insert_tehnologi($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/tehno_web");
	}

	public function index_up_tehnologi(){
		$id_web_opd = $this->input->post("id_web_opd");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->atwb->get_tehnologi_where(array("id_web_opd"=>$id_web_opd))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function up_tehnologi(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$jml_bts = $this->input->post("jml");
			$id_web_opd = $this->input->post("id_web_opd");

			$where = array(
							"id_web_opd"=>$id_web_opd
						);
			
			$data = array(
						"th"=>$th,
						"jml"=>$jml_bts
					);

			$update = $this->atwb->update_tehnologi($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/tehno_web");
	}

	public function delete_tehnologi($id_web_opd){
		$delete = $this->atwb->delete_tehnologi(array("id_web_opd"=>$id_web_opd));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/tehno_web");
	}

	
}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admindisnaker extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/admin_disnaker", "ad");
		$this->load->library("response_message");

	}

	public function index(){
		$data["page"] 						= "disnaker";
		$data["list_disnaker"] 				= $this->ad->get_disnaker();
	//	$data["list_disnaker_kategori"] 	= $this->ad->get_disnaker_kategori();
		$data["list_disnaker_jenis"] 		= $this->ad->get_disnaker_jenis();
		$data["list_disnaker_sub_jenis"] 	= $this->ad->get_disnaker_sub_jenis();
		$this->load->view('admin_data/disnaker/main_admin_disnaker', $data);
	}

//================================================ LP disnaker ====================================================//
	private function validation_ins_disnaker(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'id_sub_jenis',
                'label'=>'Sub Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_disnaker(){
		if($this->validation_ins_disnaker()){
			$id_jenis 		= $this->input->post("id_jenis");
			$id_sub_jenis 	= $this->input->post("id_sub_jenis");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");
			
			$data_insert = array(
							"id_main"=>"",
							"id_jenis"=>$id_jenis,
							"id_sub_jenis"=>$id_sub_jenis,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->ad->insert_disnaker($data_insert);
				if($insert){
					redirect('admin/disnaker');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_disnaker(){
		$id_main 	= $this->input->post("id_main");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->ad->get_disnaker_where(array("id_main"=>$id_main)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_disnaker(){
		if($this->validation_ins_disnaker()){
			$id_main 	= $this->input->post("id_main");

			$id_jenis 		= $this->input->post("id_jenis");
			$id_sub_jenis 	= $this->input->post("id_sub_jenis");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"id_sub_jenis"=>$id_sub_jenis,
						"th"=>$th,
						"jml"=>$jml
					);

			$data_where = array(
							"id_main"=>$id_main,
						);

			$insert = $this->ad->update_disnaker($data, $data_where);
				if($insert){
					redirect('admin/disnaker');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_disnaker($id_main){
		$delete = $this->ad->delete_disnaker(array("id_main"=>$id_main));
		if($delete){
			echo "del";
			redirect('admin/disnaker');
		}else {
			echo "fail";
		}
	}
//================================================ LP disnaker ====================================================//

//================================================ LP disnaker JENIS ==============================================//
	private function validation_ins_disnaker_jenis(){
		$config_val_input = array(
			 array(
                'field'=>'id_kategori',
                'label'=>'id_kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_disnaker_jenis(){
		$id_jenis 		= $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->ad->get_disnaker_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_disnaker_jenis(){
		if($this->validation_ins_disnaker_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis

						);
			$insert = $this->ad->insert_disnaker_jenis($data_insert);
				if($insert){
					redirect('admin/disnaker');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_disnaker_jenis(){
		if($this->validation_ins_disnaker_jenis()){
			$id_jenis 	= $this->input->post("id_jenis");
			$id_kategori 	= $this->input->post("id_kategori");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"id_kategori"=>$id_kategori,
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->ad->update_disnaker_jenis($data, $data_where);
				if($insert){
					redirect('admin/disnaker');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_disnaker_jenis($id_jenis){
		$delete = $this->ad->delete_disnaker_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/disnaker');
		}else {
			echo "fail";
		}
	}	
//================================================ LP disnaker JENIS ==============================================//

//================================================ LP disnaker SUB JENIS ==========================================//
	private function validation_ins_disnaker_sub_jenis(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'ID Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            ),
            array(
                'field'=>'nama_sub_jenis',
                'label'=>'Nama Sub Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_disnaker_sub_jenis(){
		$id_sub_jenis 	= $this->input->post("id_sub_jenis");
		$data["status"] = false;
		$data["val"]	 = null;
		
		$data_main =$this->ad->get_disnaker_sub_jenis_where(array("id_sub_jenis"=>$id_sub_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_disnaker_sub_jenis(){
		if($this->validation_ins_disnaker_sub_jenis()){
			$nama_sub_jenis = $this->input->post("nama_sub_jenis");
			$id_jenis 		= $this->input->post("id_jenis");
			
			$data_insert = array(
							"id_sub_jenis"=>"",
							"id_jenis"=>$id_jenis,
							"nama_sub_jenis"=>$nama_sub_jenis

						);
			$insert = $this->ad->insert_disnaker_sub_jenis($data_insert);
				if($insert){
					redirect('admin/disnaker');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_disnaker_sub_jenis(){
		if($this->validation_ins_disnaker_sub_jenis()){
			$id_sub_jenis 	= $this->input->post("id_sub_jenis");
			$id_jenis 		= $this->input->post("id_jenis");

			$nama_sub_jenis = $this->input->post("nama_sub_jenis");

			$data = array(
						
						"nama_sub_jenis"=>$nama_sub_jenis,
						"id_jenis"=>$id_jenis						
					);

			$data_where = array(
							"id_sub_jenis"=>$id_sub_jenis,
						);

			$insert = $this->ad->update_disnaker_sub_jenis($data, $data_where);
				if($insert){
					redirect('admin/disnaker');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_disnaker_sub_jenis($id_sub_jenis){
		$delete = $this->ad->delete_disnaker_sub_jenis(array("id_sub_jenis"=>$id_sub_jenis));
		if($delete){
			echo "del";
			redirect('admin/disnaker');
		}else {
			echo "fail";
		}
	}	
//================================================ LP disnaker SUB JENIS ==========================================//
}
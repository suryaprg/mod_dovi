<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Disbudpardts extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/disbudpar_dts", "la");
		$this->load->library("response_message");

	}

	public function index(){
		$data["page"] = "dts";
		$data["list_dts"] = $this->la->get_dts();
		$data["list_dtw_jenis"] = $this->la->get_dtw_jenis();
		$data["list_dtw_main"] = $this->la->get_dtw_main();
		$this->load->view('front_lp/budpar/main_admin_budpar', $data);
	}

//==============================================disbudpar dts===========================================//
	private function validation_ins_disbudpar_dts(){
		$config_val_input = array(
            array(
                'field'=>'nama',
                'label'=>'Nama',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_disbudpar_dts(){
		$id_main = $this->input->post("id_main");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_dts_where(array("id_main"=>$id_main)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_disbudpar_dts(){
		if($this->validation_ins_disbudpar_dts()){
			$nama = $this->input->post("nama");
			
			$data_insert = array(
							"id_main"=>"",
							"nama"=>$nama
						);
			$insert = $this->la->insert_dts($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/dts');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_disbudpar_dts(){
		if($this->validation_ins_disbudpar_dts()){
			$id_main = $this->input->post("id_main");

			$nama = $this->input->post("nama");

			$data = array(
						
						"nama"=>$nama
					);

			$data_where = array(
							"id_main"=>$id_main,
						);

			$insert = $this->la->update_dts($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/dts');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_disbudpar_dts($id_main){
		$delete = $this->la->delete_dts(array("id_main"=>$id_main));
		if($delete){
			echo "del";
			redirect('admin/dts');
		}else {
			echo "fail";
		}
	}

//================================================ LP DISBUDPART ====================================================//	

//================================================ LP DISBUDPAR Jenis ====================================================//
	private function validation_ins_disbudpar_dtw_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_disbudpar_dtw_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_dtw_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_disbudpar_dtw_jenis(){
		if($this->validation_ins_disbudpar_dtw_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->la->insert_dtw_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/dts');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_disbudpar_dtw_jenis(){
		if($this->validation_ins_disbudpar_dtw_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->la->update_dtw_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/dts');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_disbudpar_dtw_jenis($id_jenis){
		$delete = $this->la->delete_dtw_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/dts');
		}else {
			echo "fail";
		}
	}

//================================================ LP pertanian subJenis ====================================================//
private function validation_ins_disbudpar_dtw_main(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'nsj',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )

                ),

             array(
                'field'=>'wisman',
                'label'=>'nsj',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )

                ),
                array(
                'field'=>'wisnus',
                'label'=>'sn',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                )
               
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_disbudpar_dtw_main(){
		if($this->validation_ins_disbudpar_dtw_main()){
			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$wisman = $this->input->post("wisman");
			$wisnus = $this->input->post("wisnus");
			
			$data_insert = array(

							"id_main"=>"",
							"id_jenis"=>$id_jenis,
							"th"=>$th,
							"wisman"=>$wisman,
							"wisnus"=>$wisnus
						);
			$insert = $this->la->insert_dtw_main($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/dts');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_disbudpar_dtw_main(){
		$id_main = $this->input->post("id_main");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_dtw_main_where(array("id_sub_jenis"=>$id_sub_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_disbudpar_dtw_main(){
		if($this->validation_ins_disbudpar_dtw_main()){

			$id_main = $this->input->post("id_main");
			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$wisman= $this->input->post("wisman");
			$wisnus = $this->input->post("wisnus");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"wisman"=>$wisman,
						"wisnus"=>$wisnus
						
					);

			$data_where = array(
							"id_main"=>$id_main,
						);

			$insert = $this->la->update_dtw_main($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/dts');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_disbudpar_dtw_main($id_main){
		$delete = $this->la->delete_dtw_main(array("id_main"=>$id_main));
		if($delete){
			echo "del";
			redirect('admin/dts');
		}else {
			echo "fail";
		}
	}				
}

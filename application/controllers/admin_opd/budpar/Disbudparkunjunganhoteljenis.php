<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Disbudparkunjunganhoteljenis extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/disbudpar_kunjunganhotel", "la");
		$this->load->library("response_message");

	}

	public function index(){
		$data["page"] = "dth";
		$data["list_disbudpar_kunjungan_hotel_jenis"] = $this->la->get_disbudpar_kunjungan_hotel_jenis();
		$data["list_disbudpar_kunjungan_hotel_kate"] = $this->la->get_disbudpar_kunjungan_hotel_kate();
		$data["list_disbudpar_kunjungan_hotel_main"] = $this->la->get_disbudpar_kunjungan_hotel_main();
		$this->load->view('front_lp/budpar/main_admin_budpar', $data);
	}

//==================================================================================================//
	private function validation_ins_disbudpar_kunjungan_hotel_jenis(){
		$config_val_input = array(
            array(
                'field'=>'id_kategori',
                'label'=>'Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                 )
                       
            ),
            array(
                'field'=>'alamat',
                'label'=>'Alamat',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                 )
                       
            ),
            array(
                'field'=>'jml_kmr',
                'label'=>'Jumlah Kamar',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_disbudpar_kunjungan_hotel_jenis(){
		if($this->validation_ins_disbudpar_kunjungan_hotel_jenis()){
			$id_kategori = $this->input->post("id_kategori");
			$nama_jenis = $this->input->post("nama_jenis");
			$alamat = $this->input->post("alamat");
			$jml_kmr = $this->input->post("jml_kmr");
			
			$data_insert = array(
							"id_jenis"=>"",
							"id_kategori"=>$id_kategori,
							"nama_jenis"=>$nama_jenis,
							"alamat"=>$alamat,
							"jml_kmr"=>$jml_kmr
						);
			$insert = $this->la->insert_disbudpar_kunjungan_hotel_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin_opd/budpar/Disbudparkunjunganhoteljenis','refresh');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_disbudpar_kunjungan_hotel_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_disbudparkunjunganhoteljenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_disbudpar_kunjungan_hotel_jenis(){
		if($this->validation_ins_disbudpar_kunjungan_hotel_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$id_kategori = $this->input->post("id_kategori");
			$nama_jenis = $this->input->post("nama_jenis");
			$alamat = $this->input->post("alamat");
			$jml_kmr = $this->input->post("jml_kmr");

			$data = array(
						
						"id_kategori"=>$id_kategori,
						"nama_jenis"=>$nama_jenis,
						"alamat"=>$alamat,
						"jml_kmr"=>$jml_kmr
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->la->update_disbudpar_kunjungan_hotel_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin_opd/budpar/Disbudparkunjunganhoteljenis','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_disbudpar_kunjungan_hotel_jenis($id_jenis){
		$delete = $this->la->delete_disbudpar_kunjungan_hotel_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin_opd/budpar/Disbudparkunjunganhoteljenis','refresh');
		}else {
			echo "fail";
		}
	}
//=================disbudpar_kunjungan_hotel_kate=======================================================//
	private function validation_ins_disbudpar_kunjungan_hotel_kate(){
		$config_val_input = array(
            array(
                'field'=>'nama_kategori',
                'label'=>'Nama Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_disbudpar_kunjungan_hotel_kate(){
		$id_kategori = $this->input->post("id_kategori");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_disbudpar_kunjungan_hotel_kate_where(array("id_kategori"=>$id_kategori)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_disbudpar_kunjungan_hotel_kate(){
		if($this->validation_ins_disbudpar_kunjungan_hotel_kate()){
			$nama_kategori = $this->input->post("nama_kategori");
			
			$data_insert = array(
							"id_kategori"=>"",
							"nama_kategori"=>$nama_kategori
						);
			$insert = $this->la->insert_disbudpar_kunjungan_hotel_kate($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/dth');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_disbudpar_kunjungan_hotel_kate(){
		if($this->validation_ins_disbudpar_kunjungan_hotel_kate()){
			$id_kategori = $this->input->post("id_kategori");

			$nama_kategori = $this->input->post("nama_kategori");

			$data = array(
						
						"nama_kategori"=>$nama_kategori
					);

			$data_where = array(
							"id_kategori"=>$id_kategori,
						);

			$insert = $this->la->update_disbudpar_kunjungan_hotel_kate($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/dth');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_disbudpar_kunjungan_hotel_kate($id_kategori){
		$delete = $this->la->delete_disbudpar_kunjungan_hotel_kate(array("id_kategori"=>$id_kategori));
		if($delete){
			echo "del";
			redirect('admin/dth');
		}else {
			echo "fail";
		}
	}
//=================disbudpar_kunjungan_hotel_main =======================================================//			
	private function validation_ins_disbudpar_kunjungan_hotel_main(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'wisman',
                'label'=>'Wisman',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'wisnus',
                'label'=>'Wisnus',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_disbudpar_kunjungan_hotel_main(){
		if($this->validation_ins_disbudpar_kunjungan_hotel_main()){
			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$wisman = $this->input->post("wisman");
			$wisnus = $this->input->post("wisnus");
			
			$data_insert = array(
							"id_main"=>$id_main,
							"id_jenis"=>$id_jenis,
							"th"=>$th,
							"wisman"=>$wisman,
							"wisnus"=>$wisnus
						);
			$insert = $this->la->insert_disbudpar_kunjungan_hotel_main($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/dth');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_disbudpar_kunjungan_hotel_main(){
		$id_main = $this->input->post("id_main");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_disbudpar_kunjungan_hotel_main_where(array("id_main"=>$id_main)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_disbudpar_kunjungan_hotel_main(){
		if($this->validation_ins_disbudpar_kunjungan_hotel_main()){
			$id_main = $this->input->post("id_main");
			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$wisman = $this->input->post("wisman");
			$wisnus = $this->input->post("wisnus");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"wisman"=>$wisman,
						"wisnus"=>$wisnus
					);

			$data_where = array(
							"id_main"=>$id_main,
						);

			$insert = $this->la->update_disbudpar_kunjungan_hotel_main($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/dth');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_disbudpar_kunjungan_hotel_main($id_main){
		$delete = $this->la->delete_disbudpar_kunjungan_hotel_main(array("id_main"=>$id_main));
		if($delete){
			echo "del";
			redirect('admin/dth');
		}else {
			echo "fail";
		}
	}
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Obj_main extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Obj", "obj");
		$this->load->library("response_message");

	}

	public function index(){
		$data["page"] 				= "obj";
		$data["list_obj_main"] 		= $this->obj->get_obj();
		$data["list_obj_kategori"] 	= $this->obj->get_obj_kategori();
		$this->load->view('front_lp/budpar/main_admin_budpar', $data);
	}

//================================================ OBJ ====================================================//
	private function validation_ins_obj(){
		$config_val_input = array(
            array(
                'field'=>'id_kategori',
                'label'=>'Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )
                       
            ),
        
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),

            array(
                'field'=>'nama_obj',
                'label'=>'Nama Obj',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),            

            array(
                'field'=>'lokasi',
                'label'=>'lokasi',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")

                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_obj(){
		if($this->validation_ins_obj()){
			$id_kategori = $this->input->post("id_kategori");
			$nama_obj = $this->input->post("nama_obj");
			$th = $this->input->post("th");
			$lokasi = $this->input->post("lokasi");
			
			$data_insert = array(
							"id_main"=>"",
							"id_kategori"=>$id_kategori,
							"nama_obj"=>$nama_obj,
							"th"=>$th,
							"lokasi"=>$lokasi
						);
			$insert = $this->obj->insert_obj($data_insert);
				if($insert){
					redirect('admin/obj');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_obj(){
		$id_main = $this->input->post("id_main");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->obj->get_obj_where(array("id_main"=>$id_main)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_obj(){
		if($this->validation_ins_obj()){
			$id_main = $this->input->post("id_main");

			$id_kategori = $this->input->post("id_kategori");
			$nama_obj = $this->input->post("nama_obj");
			$th = $this->input->post("th");
			$lokasi = $this->input->post("lokasi");

			$data = array(
						
						"id_kategori"=>$id_kategori,
						"nama_obj"=>$nama_obj,
						"th"=>$th,
						"lokasi"=>$lokasi
					);

			$data_where = array(
							"id_main"=>$id_main,
						);

			$insert = $this->obj->update_obj($data, $data_where);
				if($insert){
					redirect('admin/obj');
					echo "yes";
				}else {
					echo "no";
				}

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_obj($id_main){
		$delete = $this->obj->delete_obj(array("id_main"=>$id_main));
		if($delete){
			echo "del";
			redirect('admin/obj');
		}else {
			echo "fail";
		}
	}
//================================================ OBJ ====================================================//	

//================================================ OBJ KTG ================================================//
	private function validation_ins_obj_kategori(){
		$config_val_input = array(
            
            array(
                'field'=>'nama_kategori',
                'label'=>'Nama Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")

                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_obj_kategori(){
		if($this->validation_ins_obj_kategori()){

			$nama_kategori = $this->input->post("nama_kategori");
			
			$data_insert = array(
							"id_kategori"=>"",
							"nama_kategori"=>$nama_kategori
						);
			$insert = $this->obj->insert_obj_kategori($data_insert);
				if($insert){
					redirect('admin/obj');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_obj_kategori(){
		$id_kategori = $this->input->post("id_kategori");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->obj->get_obj_kategori_where(array("id_kategori"=>$id_kategori)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_obj_kategori(){
		if($this->validation_ins_obj_kategori()){
			$id_kategori = $this->input->post("id_kategori");

			$nama_kategori = $this->input->post("nama_kategori");

			$data = array(
						
						"nama_kategori"=>$nama_kategori
					);

			$data_where = array(
							"id_kategori"=>$id_kategori,
						);

			$insert = $this->obj->update_obj_kategori($data, $data_where);
				if($insert){
					redirect('admin/obj');
					echo "yes";
				}else {
					echo "no";
				}

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_obj_kategori($id_kategori){
		$delete = $this->obj->delete_obj_kategori(array("id_kategori"=>$id_kategori));
		if($delete){
			redirect('admin/obj');			
			echo "del";
		}else {
			echo "fail";
		}
	}
//================================================ OBJ KTG ================================================//
}
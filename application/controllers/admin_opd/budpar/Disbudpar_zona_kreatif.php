<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Disbudpar_zona_kreatif extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/disbudpar", "dp");
		$this->load->library("response_message");
	}

	public function index(){
		$data["page"] 		 	= "zona_kreatif";
		$data["list_zona"] 		= $this->dp->get_zona();
		$this->load->view('front_lp/budpar/main_admin_budpar', $data);
	}

//================================================ ZONA KREATIF =========================================//
	private function validation_ins_zona(){
		$config_val_input = array(
      array(
                'field'=>'nama_zona',
                'label'=>'Nama',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'alamat',
                'label'=>'Alamat',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_disbudpar_zona(){
		if($this->validation_ins_zona()){
			$nama_zona	 = $this->input->post("nama_zona");
			$alamat	 = $this->input->post("alamat");
			$data_insert = array(
							"id_zona"	=> "",
							"nama_zona"	=> $nama_zona,
							"alamat"    => $alamat
						);
			$insert = $this->dp->insert_zona($data_insert);
				if($insert){
					redirect('admin/zona_kreatif');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_disbudpar_zona(){
		$id_zona 		= $this->input->post("id_zona");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->dp->get_zona_where(array("id_zona" => $id_zona)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_disbudpar_zona(){
		if($this->validation_ins_zona()){
			$id_zona	= $this->input->post("id_zona");

			$nama_zona	= $this->input->post("nama_zona");
					$alamat	= $this->input->post("alamat");

			$data = array(
						
						"nama_zona"	=> $nama_zona,
						"alamat"    => $alamat
					);

			$data_where = array(
							"id_zona" => $id_zona,
						);

			$insert = $this->dp->update_zona($data, $data_where);
				if($insert){
					redirect('admin/zona_kreatif');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}
	}

	public function delete_disbudpar_zona($id_zona){
		$delete = $this->dp->delete_zona(array("id_zona" => $id_zona));
		if($delete){
			redirect('admin/zona_kreatif');
			echo "del";
		}else {
			echo "fail";
		}
	}
//================================================ ZONA KREATIF ==========================================//		
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminkesehatangizi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_gizi", "ag");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kesehatan_gizi";
		$data["list_kesehatan"] = $this->ag->get();
		$this->load->view('admin_data/kesehatan/main_admin_kesehatan', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------tehnologi--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jmh_kurang_gizi',
                'label'=>'jmh_kurang_gizi',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_gizi_buruk',
                'label'=>'jml_gizi_buruk',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){

			$th = $this->input->post("th");
			$jmh_kurang_gizi = $this->input->post("jmh_kurang_gizi");
			$jml_gizi_buruk = $this->input->post("jml_gizi_buruk");

			$data = array(
						"id_gizi_balita"=>"",
						"th"=>$th,
						"jmh_kurang_gizi"=>$jmh_kurang_gizi,
						"jml_gizi_buruk"=>$jml_gizi_buruk
					);
			
			$insert = $this->ag->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kesehatan_gizi");
	}

	public function index_up(){
		$id_gizi_balita = $this->input->post("id_gizi_balita");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->ag->get_where(array("id_gizi_balita"=>$id_gizi_balita))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$jmh_kurang_gizi = $this->input->post("jmh_kurang_gizi");
			$jml_gizi_buruk = $this->input->post("jml_gizi_buruk");

			$id_gizi_balita = $this->input->post("id_gizi_balita");
			
			$where = array(
							"id_gizi_balita"=>$id_gizi_balita
						);
			
			$data = array(
						
						"th"=>$th,
						"jmh_kurang_gizi"=>$jmh_kurang_gizi,
						"jml_gizi_buruk"=>$jml_gizi_buruk
					);

			$update = $this->ag->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kesehatan_gizi");
	}

	public function delete($id_gizi_balita){
		$delete = $this->ag->delete(array("id_gizi_balita"=>$id_gizi_balita));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/kesehatan_gizi");
	}

	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminkesehatanpenyakit extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_kesehatan_penyakit", "akp");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kesehatan_penyakit";
		$data["list_kesehatan"] = $this->akp->get();
		$this->load->view('admin_data/kesehatan/main_admin_kesehatan', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------tehnologi--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'keterangan',
                'label'=>'keterangan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'jml',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){

			$th = $this->input->post("th");
			$keterangan = $this->input->post("keterangan");
			$jml = $this->input->post("jml");

			$data = array(
						"id_jenis_penyakit"=>"",
						"keterangan"=>$keterangan,
						"jml"=>$jml,
						"th"=>$th
					);
			
			$insert = $this->akp->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kesehatan_penyakit");
	}

	public function index_up(){
		$id_jenis_penyakit = $this->input->post("id_jenis_penyakit");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->akp->get_where(array("id_jenis_penyakit"=>$id_jenis_penyakit))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$keterangan = $this->input->post("keterangan");
			$jml = $this->input->post("jml");

			$id_jenis_penyakit = $this->input->post("id_jenis_penyakit");
			
			$where = array(
							"id_jenis_penyakit"=>$id_jenis_penyakit
						);
			
			$data = array(
						"keterangan"=>$keterangan,
						"jml"=>$jml,
						"th"=>$th
					);

			$update = $this->akp->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kesehatan_penyakit");
	}

	public function delete($id_jenis_penyakit){
		$delete = $this->akp->delete(array("id_jenis_penyakit"=>$id_jenis_penyakit));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/kesehatan_penyakit");
	}

	
}

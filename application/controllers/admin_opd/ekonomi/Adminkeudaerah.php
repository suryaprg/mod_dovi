<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminkeudaerah extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model("admin_opd/admin_keu", "ak");

		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "keuangan_daerah";
		$data["list_jenis_keu"] = $this->ak->get_jenis_keu();
		$data["list_data_keu"] = $this->ak->get_keu();
		$this->load->view('admin_data/ekonomi/main_admin_keu', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------Jenis Keuangan----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	
	private function validation_ins_jenis(){
		$config_val_input = array(
            array(
                'field'=>'jenis',
                'label'=>'Keterangan Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'kategori',
                'label'=>'kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_jenis(){
		if($this->validation_ins_jenis()){
			$keterangan = $this->input->post("jenis");
			$kategori =  $this->input->post("kategori");

			$data_insert = array(
							"id_jenis"=>"",
							"ket"=>$keterangan,
							"kategori"=>$kategori
						);

			$insert = $this->ak->insert_jenis_keu($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/keuangan_daerah");
	}

	public function index_up_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		// $id_jenis = "1";
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->ak->get_jenis_keu_where(array("id_jenis"=>$id_jenis))->row_array(); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function up_jenis(){
		if($this->validation_ins_jenis()){
			$id_jenis = $this->input->post("id_jenis");
			$keterangan = $this->input->post("jenis");
			$kategori =  $this->input->post("kategori");

			$data_insert = array(
							"ket"=>$keterangan,
							"kategori"=>$kategori
						);

			$data_where = array(
							"id_jenis"=>$id_jenis
						);

			$insert = $this->ak->update_jenis_keu($data_insert, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/keuangan_daerah");
	}

	public function delete_jenis($id_jenis){
		$delete = $this->ak->delete_jenis_keu(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/keuangan_daerah");
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Data Keuangan--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins_keu(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis Keuangan',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Periode (Tahun)',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'nominal',
                'label'=>'Nominal',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_keu(){
		if($this->validation_ins_keu()){

			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$nominal = $this->input->post("nominal");

			$data = array(
						"id_jml_keu"=>"",
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"jml"=>$nominal
					);
			
			$insert = $this->ak->insert_keu($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/keuangan_daerah");
	}

	public function index_up_keu(){
		$id_jml_keu = $this->input->post("id_jml_keu");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->ak->get_keu_where(array("id_jml_keu"=>$id_jml_keu))->row_array(); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function up_keu(){
		if($this->validation_ins_keu()){
			$id_jml_keu = $this->input->post("id_keu");

			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$nominal = $this->input->post("nominal");

			$data = array(
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"jml"=>$nominal
					);

			$data_where = array(
							"id_jml_keu"=>$id_jml_keu,
						);

			$insert = $this->ak->update_keu($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/keuangan_daerah");
	}

	public function delete_keu($id_jml_keu){
		$delete = $this->ak->delete_keu(array("id_jml_keu"=>$id_jml_keu));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/keuangan_daerah");
	}

}



<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpengeluaranrmttgg extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_pengeluaran_rmt_tgg", "aprt");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "pengeluaran_rumah_tangga";
		$data["list_ekonomi"] = $this->aprt->get();
		$this->load->view('admin_data/ekonomi/main_admin_keu', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_pertanian_lahan----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'jml_mkn',
                'label'=>'jml_mkn',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'jml_non',
                'label'=>'jml_non',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$jml_mkn = $this->input->post("jml_mkn");
			$jml_non = $this->input->post("jml_non");

			$data = array(
						"id_rmt_tgg"=>"",
						"th"=>$th,
						"jml_mkn"=>$jml_mkn,
						"jml_non"=>$jml_non
					);
			
			$insert = $this->aprt->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pengeluaran_rumah_tangga");
	}

	public function index_up(){
		$id_rmt_tgg = $this->input->post("id_rmt_tgg");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->aprt->get_where(array("id_rmt_tgg"=>$id_rmt_tgg))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$jml_mkn = $this->input->post("jml_mkn");
			$jml_non = $this->input->post("jml_non");

			$id_rmt_tgg = $this->input->post("id_rmt_tgg");
			
			$where = array(
							"id_rmt_tgg"=>$id_rmt_tgg
						);
			
			$data = array(
						"th"=>$th,
						"jml_mkn"=>$jml_mkn,
						"jml_non"=>$jml_non,
					);
			

			$update = $this->aprt->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pengeluaran_rumah_tangga");
	}

	public function delete($id_rmt_tgg){
		$delete = $this->aprt->delete(array("id_rmt_tgg"=>$id_rmt_tgg));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/pengeluaran_rumah_tangga");
	}

	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpendapatanpdo extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_pendapatan_regional", "apr");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "pendapatan_regional";
		$data["list_ekonomi"] = $this->apr->get();
		$this->load->view('admin_data/ekonomi/main_admin_keu', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_pertanian_lahan----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'th_pendapatan',
                'label'=>'th_pendapatan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'jml_pendapatan',
                'label'=>'jml_pendapatan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$th_pendapatan = $this->input->post("th_pendapatan");
			$jml_pendapatan = $this->input->post("jml_pendapatan");

			$data = array(
						"id_pendapatan"=>"",
						"th_pendapatan"=>$th_pendapatan,
						"jml_pendapatan"=>$jml_pendapatan,
					);
			
			$insert = $this->apr->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pendapatan_regional");
	}

	public function index_up(){
		$id_pendapatan = $this->input->post("id_pendapatan");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->apr->get_where(array("id_pendapatan"=>$id_pendapatan))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$th_pendapatan = $this->input->post("th_pendapatan");
			$jml_pendapatan = $this->input->post("jml_pendapatan");

			$id_pendapatan = $this->input->post("id_pendapatan");
			
			$where = array(
							"id_pendapatan"=>$id_pendapatan
						);
			
			$data = array(
						"th_pendapatan"=>$th_pendapatan,
						"jml_pendapatan"=>$jml_pendapatan
					);
			

			$update = $this->apr->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pendapatan_regional");
	}

	public function delete($id_pendapatan){
		$delete = $this->apr->delete(array("id_pendapatan"=>$id_pendapatan));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/pendapatan_regional");
	}

	
}

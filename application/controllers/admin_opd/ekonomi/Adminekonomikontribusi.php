<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminekonomikontribusi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_ekonomi_kontribusi", "aek");
		$this->load->model("admin_opd/Admin_ekonomi_kontribusi_jenis", "aekj");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kegiatan_usaha";
		$data["list_ekonomi"] = $this->aek->get();
		$data["list_ekonomi_jenis"] = $this->aekj->get();
		$this->load->view('admin_data/ekonomi/main_admin_keu', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_pertanian_lahan----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'id_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'jml',
                'label'=>'jml',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			$id_jenis = $this->input->post("id_jenis");

			$data = array(
						"id_lap_usaha"=>"",
						"th"=>$th,
						"jml"=>$jml,
						"id_jenis"=>$id_jenis
					);
			
			$insert = $this->aek->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kegiatan_usaha");
	}

	public function index_up(){
		$id_lap_usaha = $this->input->post("id_lap_usaha");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->aek->get_where(array("id_lap_usaha"=>$id_lap_usaha))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			$id_jenis = $this->input->post("id_jenis");

			$id_lap_usaha = $this->input->post("id_lap_usaha");
			
			$where = array(
							"id_lap_usaha"=>$id_lap_usaha
						);
			
			$data = array(
						"th"=>$th,
						"jml"=>$jml,
						"id_jenis"=>$id_jenis
					);
			

			$update = $this->aek->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kegiatan_usaha");
	}

	public function delete($id_lap_usaha){
		$delete = $this->aek->delete(array("id_lap_usaha"=>$id_lap_usaha));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/kegiatan_usaha");
	}

	
}

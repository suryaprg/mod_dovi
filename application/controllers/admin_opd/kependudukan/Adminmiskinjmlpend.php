<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminmiskinjmlpend extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_miskin_jml_pend", "amjp");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "penduduk_miskin";
		$data["list_kependudukan"] = $this->amjp->get();
		$this->load->view('admin_data/kependudukan/main_admin_kepend', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_miskin_jml_pend----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'agregat',
                'label'=>'agregat',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'prosentase',
                'label'=>'prosentase',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$agregat = $this->input->post("agregat");
			$prosentase = $this->input->post("prosentase");

			$data = array(
						"id_pend_miskin"=>"",
						"th"=>$th,
						"agregat"=>$agregat,
						"prosentase"=>$prosentase
					);
			
			$insert = $this->amjp->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/penduduk_miskin");
	}

	public function index_up(){
		$id_pend_miskin = $this->input->post("id_pend_miskin");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->amjp->get_where(array("id_pend_miskin"=>$id_pend_miskin))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$agregat = $this->input->post("agregat");
			$prosentase = $this->input->post("prosentase");

			$id_pend_miskin = $this->input->post("id_pend_miskin");
			
			$where = array(
							"id_pend_miskin"=>$id_pend_miskin
						);
			
			$data = array(
						"th"=>$th,
						"agregat"=>$agregat,
						"prosentase"=>$prosentase,
					);
			

			$update = $this->amjp->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/penduduk_miskin");
	}

	public function delete($id_pend_miskin){
		$delete = $this->amjp->delete(array("id_pend_miskin"=>$id_pend_miskin));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/penduduk_miskin");
	}

	
}

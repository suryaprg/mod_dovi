<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminkependudukanketer extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_kependudukan_ketergantungan", "akk");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kependudukan_keter";
		$data["list_kependudukan"] = $this->akk->get_kependudukan();
		$this->load->view('admin_data/kependudukan/main_admin_kepend', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Kependudukan--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins_kepend(){
		$config_val_input = array(
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'rasio',
                'label'=>'rasio',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_kepend(){
		if($this->validation_ins_kepend()){

			$th = $this->input->post("th");
			$rasio = $this->input->post("rasio");

			$data = array(
						"id_rasio"=>"",
						"th_rasio"=>$th,
						"rasio_keter"=>$rasio
					);
			
			$insert = $this->akk->insert_kependudukan($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kependudukan_ketergantungan");
	}

	public function index_up_kepend(){
		$id_rasio = $this->input->post("id_rasio");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->akk->get_kependudukan_where(array("id_rasio"=>$id_rasio))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function up_kepend(){
		if($this->validation_ins_kepend()){
			$th = $this->input->post("th");
			$rasio = $this->input->post("rasio");

			$id_rasio = $this->input->post("id_rasio");
			
			$where = array(
							"id_rasio"=>$id_rasio
						);
			
			$data = array(
						"th_rasio"=>$th,
						"rasio_keter"=>$rasio
					);
			

			$update = $this->akk->update_kependudukan($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kependudukan_ketergantungan");
	}

	public function delete_kepend($id_rasio){
		$delete = $this->akk->delete_kependudukan(array("id_rasio"=>$id_rasio));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/kependudukan_ketergantungan");
	}

	
}

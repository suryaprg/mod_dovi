<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminkependudukanipmbidang extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_kependudukan_ipm_bidang", "akib");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kependudukan_ipm_bid";
		$data["list_kependudukan"] = $this->akib->get_kependudukan();
		$this->load->view('admin_data/kependudukan/main_admin_kepend', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Kependudukan--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins_ipm(){
		$config_val_input = array(
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'in_kes',
                'label'=>'in_kes',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'in_daya',
                'label'=>'in_daya',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'in_pend',
                'label'=>'in_pend',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_ipm(){
		if($this->validation_ins_ipm()){

			$th = $this->input->post("th");
			$in_kes = $this->input->post("in_kes");
			$in_daya = $this->input->post("in_daya");
			$in_pend = $this->input->post("in_pend");

			$data = array(
						"id_index"=>"",
						"th"=>$th,
						"in_kes"=>$in_kes,
						"in_daya"=>$in_daya,
						"in_pend"=>$in_pend
					);
			
			$insert = $this->akib->insert_kependudukan($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kependudukan_ipm_bid");
	}

	public function index_up_ipm(){
		$id_index = $this->input->post("id_index");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->akib->get_kependudukan_where(array("id_index"=>$id_index))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function up_ipm(){
		if($this->validation_ins_ipm()){
			$th = $this->input->post("th");
			$in_kes = $this->input->post("in_kes");
			$in_daya = $this->input->post("in_daya");
			$in_pend = $this->input->post("in_pend");

			$id_index = $this->input->post("id_index");
			
			$where = array(
							"id_index"=>$id_index
						);
			
			$data = array(
						"th"=>$th,
						"in_kes"=>$in_kes,
						"in_daya"=>$in_daya,
						"in_pend"=>$in_pend
					);
			
			

			$update = $this->akib->update_kependudukan($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kependudukan_ipm_bid");
	}

	public function delete_ipm($id_index){
		$delete = $this->akib->delete_kependudukan(array("id_index"=>$id_index));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/kependudukan_ipm_bid");
	}

	
}

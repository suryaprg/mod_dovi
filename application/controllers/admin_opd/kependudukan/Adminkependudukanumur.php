<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminkependudukanumur extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_kependudukan_umur", "aku");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kependudukan_umur";
		$data["list_kependudukan"] = $this->aku->get_kependudukan();
		$this->load->view('admin_data/kependudukan/main_admin_kepend', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Kependudukan--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins_kepend(){
		$config_val_input = array(
            array(
                'field'=>'kel_umur',
                'label'=>'kel_umur',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_pend',
                'label'=>'jml_pend',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_kepend(){
		if($this->validation_ins_kepend()){

			$kel_umur = $this->input->post("kel_umur");
			$th = $this->input->post("th");
			$jml_pend = $this->input->post("jml_pend");

			$data = array(
						"id_kel_umur"=>"",
						"jml_penduduk"=>$jml_pend,
						"kelompok"=>$kel_umur,
						"periode"=>$th
					);
			
			$insert = $this->aku->insert_kependudukan($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kependudukan_umur");
	}

	public function index_up_kepend(){
		$id_kel_umur = $this->input->post("id_kel_umur");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->aku->get_kependudukan_where(array("id_kel_umur"=>$id_kel_umur))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function up_kepend(){
		if($this->validation_ins_kepend()){
			$kel_umur = $this->input->post("kel_umur");
			$th = $this->input->post("th");
			$jml_pend = $this->input->post("jml_pend");

			$id_kel_umur = $this->input->post("id_kel_umur");
			
			$where = array(
							"id_kel_umur"=>$id_kel_umur
						);
			
			$data = array(
						"jml_penduduk"=>$jml_pend,
						"kelompok"=>$kel_umur,
						"periode"=>$th
					);

			$update = $this->aku->update_kependudukan($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kependudukan_umur");
	}

	public function delete_kepend($id_kel_umur){

		$delete = $this->aku->delete_kependudukan(array("id_kel_umur"=>$id_kel_umur));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/kependudukan_umur");
	}

	
}

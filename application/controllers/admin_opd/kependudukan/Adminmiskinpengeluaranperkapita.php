<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminmiskinpengeluaranperkapita extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_miskin_pengeluaran_perkapita", "ampp");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "garis_miskin";
		$data["list_kependudukan"] = $this->ampp->get();
		$this->load->view('admin_data/kependudukan/main_admin_kepend', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_miskin_pengeluaran_perkapita----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'garis_kemiskinan',
                'label'=>'garis_kemiskinan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'pengeluaran_perkapita',
                'label'=>'pengeluaran_perkapita',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$garis_kemiskinan = $this->input->post("garis_kemiskinan");
			$pengeluaran_perkapita = $this->input->post("pengeluaran_perkapita");

			$data = array(
						"id_perkapita"=>"",
						"th"=>$th,
						"garis_kemiskinan"=>$garis_kemiskinan,
						"pengeluaran_perkapita"=>$pengeluaran_perkapita
					);
			
			$insert = $this->ampp->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/garis_miskin");
	}

	public function index_up(){
		$id_perkapita = $this->input->post("id_perkapita");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->ampp->get_where(array("id_perkapita"=>$id_perkapita))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$garis_kemiskinan = $this->input->post("garis_kemiskinan");
			$pengeluaran_perkapita = $this->input->post("pengeluaran_perkapita");

			$id_perkapita = $this->input->post("id_perkapita");
			
			$where = array(
							"id_perkapita"=>$id_perkapita
						);
			
			$data = array(
						"th"=>$th,
						"garis_kemiskinan"=>$garis_kemiskinan,
						"pengeluaran_perkapita"=>$pengeluaran_perkapita,
					);
			

			$update = $this->ampp->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/garis_miskin");
	}

	public function delete($id_perkapita){
		$delete = $this->ampp->delete(array("id_perkapita"=>$id_perkapita));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/garis_miskin");
	}

	
}

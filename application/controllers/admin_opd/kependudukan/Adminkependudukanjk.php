<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminkependudukanjk extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_kependudukan_jk", "akj");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kependudukan_jk";
		$data["list_kependudukan"] = $this->akj->get_kependudukan();
		$this->load->view('admin_data/kependudukan/main_admin_kepend', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------kependudukan_jk--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins_kepend(){
		$config_val_input = array(
            array(
                'field'=>'rasio_jk',
                'label'=>'Rasio Jenis Kelamin (Prosentase %)',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Periode (Tahun)',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_laki',
                'label'=>'Jumlah Penduduk Laki - Laki',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_pr',
                'label'=>'Jumlah Penduduk Perempuan',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_kepend(){
		if($this->validation_ins_kepend()){

			$rasio_jk = $this->input->post("rasio_jk");
			$th = $this->input->post("th");
			$jml_laki = $this->input->post("jml_laki");
			$jml_pr = $this->input->post("jml_pr");

			$data = array(
						"id_kepend_jk"=>"",
						"rasio_jk"=>$rasio_jk,
						"th"=>$th,
						"t_cowo"=>$jml_laki,
						"t_cewe"=>$jml_pr
					);
			
			$insert = $this->akj->insert_kependudukan($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kependudukan_jk");
	}

	public function index_up_kepend(){
		$id_kepend_jk = $this->input->post("id_kepend_jk");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->akj->get_kependudukan_where(array("id_kepend_jk"=>$id_kepend_jk))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function up_kepend(){
		if($this->validation_ins_kepend()){
			$rasio_jk = $this->input->post("rasio_jk");
			$th = $this->input->post("th");
			$jml_laki = $this->input->post("jml_laki");
			$jml_pr = $this->input->post("jml_pr");

			$id_kepend_jk = $this->input->post("id_kepend_jk");

			
			$where = array(
							"id_kepend_jk"=>$id_kepend_jk
						);
			
			$data = array(
						"rasio_jk"=>$rasio_jk,
						"th"=>$th,
						"t_cowo"=>$jml_laki,
						"t_cewe"=>$jml_pr,
					);
			$update = $this->akj->update_kependudukan($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kependudukan_jk");
	}

	public function delete_kepend($id_kepend_jk){

		$delete = $this->akj->delete_kependudukan(array("id_kepend_jk"=>$id_kepend_jk));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/kependudukan_jk");
	}

	
}

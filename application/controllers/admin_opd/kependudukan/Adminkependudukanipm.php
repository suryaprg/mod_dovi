<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminkependudukanipm extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_kependudukan_ipm", "aki");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kependudukan_ipm";
		$data["list_kependudukan"] = $this->aki->get_kependudukan();
		$this->load->view('admin_data/kependudukan/main_admin_kepend', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Kependudukan--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins_ipm(){
		$config_val_input = array(
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'rasio',
                'label'=>'rasio',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_ipm(){
		if($this->validation_ins_ipm()){

			$th = $this->input->post("th");
			$rasio = $this->input->post("rasio");

			$data = array(
						"id_ipm"=>"",
						"th"=>$th,
						"ipm"=>$rasio
					);
			
			$insert = $this->aki->insert_kependudukan($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kependudukan_ipm");
	}

	public function index_up_ipm(){
		$id_ipm = $this->input->post("id_ipm");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->aki->get_kependudukan_where(array("id_ipm"=>$id_ipm))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function up_ipm(){
		if($this->validation_ins_ipm()){
			$th = $this->input->post("th");
			$rasio = $this->input->post("rasio");

			$id_rasio = $this->input->post("id_ipm");
			
			$where = array(
							"id_ipm"=>$id_rasio
						);
			
			$data = array(
						"th"=>$th,
						"ipm"=>$rasio
					);
			

			$update = $this->aki->update_kependudukan($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kependudukan_ipm");
	}

	public function delete_ipm($id_ipm){
		$delete = $this->aki->delete_kependudukan(array("id_ipm"=>$id_ipm));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/kependudukan_ipm");
	}

	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminiklim extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/admin_iklim", "ai");
		$this->load->library("response_message");
		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }

	}

	public function index(){
		$data["page"] = "iklim";
		$data["list_hujan"] = $this->ai->get_iklim();
		$data["list_stasiun"] = $this->ai->get_st();
		$this->load->view('admin_data/geo/main_admin_geo', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------Stasiun----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	
	private function validation_ins_stasiun(){
		$config_val_input = array(
            array(
                'field'=>'nama',
                'label'=>'Nama Stasiun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'alamat',
                'label'=>'Alamat Stasiun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_st(){
		if($this->validation_ins_stasiun()){
			$nama = $this->input->post("nama");
			$alamat = $this->input->post("alamat");
			
			$data_insert = array(
							"id_st"=>"",
							"ket_st"=>$nama,
							"alamat_st"=>$alamat
						);
			$insert = $this->ai->insert_st($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/iklim");
	}

	public function index_up_st(){
		$id_st = $this->input->post("id_st");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->ai->get_st_where(array("id_st"=>$id_st)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function up_st(){
		if($this->validation_ins_stasiun()){
			$id_st = $this->input->post("id_st");

			$nama = $this->input->post("nama");
			$alamat = $this->input->post("alamat");
			
			$data_insert = array(
							"ket_st"=>$nama,
							"alamat_st"=>$alamat
						);

			$data_where = array(
							"id_st"=>$id_st
						);

			$insert = $this->ai->update_st($data_insert, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/iklim");
	}

	public function delete_st($id_st){
		$delete = $this->ai->delete_st(array("id_st"=>$id_st));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/iklim");
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Iklim--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins_ik(){
		$config_val_input = array(
            array(
                'field'=>'nama_st',
                'label'=>'Nama Stasiun',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'periode',
                'label'=>'Periode (Bulan)',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'hari',
                'label'=>'Jumlah Hujan (Hari)',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'cura',
                'label'=>'Jumlah Cura Hujan',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_ik(){
		if($this->validation_ins_ik()){

			$nama_st = $this->input->post("nama_st");
			$periode = $this->input->post("periode");
			$th = $this->input->post("th");
			$hari = $this->input->post("hari");
			$cura = $this->input->post("cura");

			$data = array(
						"id_hujan"=>"",
						"id_st"=>$nama_st,
						"periode"=>$periode,
						"th"=>$th,
						"jml_hari"=>$hari,
						"jml_cura"=>$cura
					);
			
			$insert = $this->ai->insert_iklim($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/iklim");
	}

	public function index_up_ik(){
		$id_ik = $this->input->post("id_ik");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->ai->get_iklim_where(array("id_hujan"=>$id_ik)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function up_ik(){
		if($this->validation_ins_ik()){
			$id_hujan = $this->input->post("id_hujan");

			$nama_st = $this->input->post("nama_st");
			$periode = $this->input->post("periode");
			$th = $this->input->post("th");
			$hari = $this->input->post("hari");
			$cura = $this->input->post("cura");

			$data = array(
						
						"id_st"=>$nama_st,
						"periode"=>$periode,
						"th"=>$th,
						"jml_hari"=>$hari,
						"jml_cura"=>$cura
					);

			$data_where = array(
							"id_hujan"=>$id_hujan,
						);

			$insert = $this->ai->update_iklim($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/iklim");
	}

	public function delete_ik($id_ik){
		$delete = $this->ai->delete_iklim(array("id_hujan"=>$id_ik));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/iklim");
	}

	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminkerjaangkatan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_kerja_angkatan", "aka");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kerja_angkatan";
		$data["list_kerja"] = $this->aka->get_kerja();
		$this->load->view('admin_data/kerja/main_admin_kerja', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Kependudukan--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins_kerja(){
		$config_val_input = array(
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_kerja',
                'label'=>'jml_kerja',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'jml_no_kerja',
                'label'=>'jml_no_kerja',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_kerja(){
		if($this->validation_ins_kerja()){

			$th = $this->input->post("th");
			$jml_kerja = $this->input->post("jml_kerja");
			$jml_no_kerja = $this->input->post("jml_no_kerja");

			$data = array(
						"id_angkatan"=>"",
						"th"=>$th,
						"jml_kerja"=>$jml_kerja,
						"jml_no_kerja"=>$jml_no_kerja
					);
			
			$insert = $this->aka->insert_kerja($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kerja_angkatan");
	}

	public function index_up_kerja(){
		$id_angkatan = $this->input->post("id_angkatan");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->aka->get_kerja_where(array("id_angkatan"=>$id_angkatan))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function up_kerja(){
		if($this->validation_ins_kerja()){
			$th = $this->input->post("th");
			$jml_kerja = $this->input->post("jml_kerja");
			$jml_no_kerja = $this->input->post("jml_no_kerja");

			$id_angkatan = $this->input->post("id_angkatan");

			$where = array(
							"id_angkatan"=>$id_angkatan
						);
			
			$data = array(
						"th"=>$th,
						"jml_kerja"=>$jml_kerja,
						"jml_no_kerja"=>$jml_no_kerja
					);			
			

			$update = $this->aka->update_kerja($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kerja_angkatan");
	}

	public function delete_kerja($id_angkatan){
		$delete = $this->aka->delete_kerja(array("id_angkatan"=>$id_angkatan));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/kerja_angkatan");
	}

	
}

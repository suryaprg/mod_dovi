<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminkerjapengangguran extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_kerja_pengangguran", "akp");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kerja_pengangguran";
		$data["list_kerja"] = $this->akp->get_kerja();
		$this->load->view('admin_data/kerja/main_admin_kerja', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Pengangguran--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins_kerja(){
		$config_val_input = array(
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'jml',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_kerja(){
		if($this->validation_ins_kerja()){

			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			
			$data = array(
						"id_pengangguran"=>"",
						"th"=>$th,
						"jml"=>$jml,
					);
			
			$insert = $this->akp->insert_kerja($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kerja_pengangguran");
	}

	public function index_up_kerja(){
		$id_pengangguran = $this->input->post("id_pengangguran");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->akp->get_kerja_where(array("id_pengangguran"=>$id_pengangguran))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function up_kerja(){
		if($this->validation_ins_kerja()){
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			
			$id_pengangguran = $this->input->post("id_pengangguran");

			$where = array(
							"id_pengangguran"=>$id_pengangguran
						);
			
			$data = array(
						"th"=>$th,
						"jml"=>$jml,
					);


			$update = $this->akp->update_kerja($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kerja_pengangguran");
	}

	public function delete_kerja($id_pengangguran){
		$delete = $this->akp->delete_kerja(array("id_pengangguran"=>$id_pengangguran));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/kerja_pengangguran");
	}

	
}

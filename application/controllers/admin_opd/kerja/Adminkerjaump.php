<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminkerjaump extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_kerja_ump", "aku");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kerja_ump";
		$data["list_kerja"] = $this->aku->get_kerja();
		$this->load->view('admin_data/kerja/main_admin_kerja', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Pengangguran--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins_kerja(){
		$config_val_input = array(
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'jml',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_kerja(){
		if($this->validation_ins_kerja()){

			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			
			$data = array(
						"id_ump"=>"",
						"th"=>$th,
						"jml"=>$jml,
					);
			
			$insert = $this->aku->insert_kerja($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kerja_ump");
	}

	public function index_up_kerja(){
		$id_ump = $this->input->post("id_ump");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->aku->get_kerja_where(array("id_ump"=>$id_ump))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function up_kerja(){
		if($this->validation_ins_kerja()){
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			
			$id_ump = $this->input->post("id_ump");

			$where = array(
							"id_ump"=>$id_ump
						);
			
			$data = array(
						"th"=>$th,
						"jml"=>$jml,
					);


			$update = $this->aku->update_kerja($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kerja_ump");
	}

	public function delete_kerja($id_ump){
		$delete = $this->aku->delete_kerja(array("id_ump"=>$id_ump));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/kerja_ump");
	}

	
}

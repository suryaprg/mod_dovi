<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminperdagangansubjenis extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_perdagangan_sub_jenis", "dpj");
	
		
		$this->load->library("response_message");
		
		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "perdagangan";
		$data["list_perdagangan_sub_jenis"] = $this->dpj->get();
		$this->load->view('admin_data/perdagangan/main_admin_perdagangan', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_pertanian_lahan----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(

			array(
                'field'=>'id_jenis',
                'label'=>'id_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )            

             ),array(
                'field'=>'nama_sub_jenis',
                'label'=>'nama_sub_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'unit',
                'label'=>'unit',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
		
			$id_jenis			= $this->input->post("id_jenis");
			$nama_sub_jenis     = $this->input->post("nama_sub_jenis");
			$unit 				= $this->input->post("unit");
		
			
			

			$data = array(
						"id_sub_jenis" => "",
						"id_jenis" => $id_jenis,
						"nama_sub_jenis"=>$nama_sub_jenis,
						"unit"=>$unit
						
						
						
					);
			
			$insert = $this->dpj->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/perdagangan");
	}

	public function index_up(){
		$id_sub_jenis = $this->input->post("id_sub_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->dp->get_where(array("id_sub_jenis"=>$id_jenis))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
		
			$id_jenis 		= $this->input->post("id_jenis");
			$nama_sub_jenis 		= $this->input->post("nama_sub_jenis");
			$unit 			= $this->input->post("unit");
			
			
			
			$where = array(
							"id_sub_jenis"=>$id_sub_jenis
						);
			
			$data = array(
					
						"id_jenis"=>$id_jenis,
						"nama_sub_jenis"=>$nama_sub_jenis,
						"unit"=>$unit
						
						
						
					);
			

			$update = $this->dpj->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/perdagangan");
	}

	public function delete($id_sub_jenis){
		$delete = $this->dpj->delete(array("id_sub_jenis"=>$id_sub_jenis));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/perdagangan");
	}

	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admindsu extends CI_Controller {
//ok
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_dsu", "ad");

		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "dsu";
		$data["list_dsu"] = $this->ad->get_dsu();
		$this->load->view('admin_data/disperkim/main_admin_disperkim', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------rt_rw----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	
	private function validation_ins_dsu(){
		$config_val_input = array(
         array(
                'field'=>'nama_kawasan',
                'label'=>'nama_kawasan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            
            array(
                'field'=>'lokasi',
                'label'=>'lokasi',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
             array(
                'field'=>'pengembang',
                'label'=>'pengembang',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	
                 )
                       
            ),
              array(
                'field'=>'tgl',
                'label'=>'tgl',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                	
                 )
                       
                       
            )
            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_dsu(){
		if($this->validation_ins_dsu()){
			
			$nama_kawasan 	= $this->input->post("nama_kawasan");
			$lokasi		= $this->input->post("lokasi");
			$pengembang		= $this->input->post("pengembang");
			$tgl		= $this->input->post("tgl");
			


			
			$data_insert = array(
						
							"id_dsu"=>"",
							"nama_kawasan"=>$nama_kawasan,
							"lokasi"=>$lokasi,
							"pengembang"=>$pengembang,
							"tgl"=>$tgl,
							
							
						);
			$insert = $this->ad->insert_dsu($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/dsu");
	}

	public function index_up_dsu(){
		$id_dsu = $this->input->post("id_dsu");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->ad->get_dsu_where(array("id_dsu"=>$id_dsu))->row_array(); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function up_dsu(){
		if($this->validation_ins_dsu()){
			$id_dsu 		= $this->input->post("id_dsu");
			$nama_kawasan 	= $this->input->post("nama_kawasan");
			$lokasi 		= $this->input->post("lokasi");
			$pengembang		= $this->input->post("pengembang");
			$tgl		= $this->input->post("tgl");
			
			
			
			$data_insert = array(
							
							"nama_kawasan"=>$nama_kawasan,
							"lokasi"=>$lokasi,
							"pengembang"=>$pengembang,
							"tgl"=>$tgl
							
							
						);

			$data_where = array(
							"id_dsu"=>$id_dsu
						);

			$insert = $this->ad->update_dsu($data_insert, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/dsu");
	}

	public function delete_dsu($id_dsu){
		$delete = $this->ad->delete_dsu(array("id_dsu"=>$id_dsu));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/dsu");
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admintamanjenis extends CI_Controller {
//ok
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_taman_jenis", "atj");

		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "taman";
		$data["list_taman_jenis"] = $this->atj->get_taman_jenis();
		$this->load->view('admin_data/disperkim/main_admin_disperkim', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------rt_rw----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	
	private function validation_ins_taman(){
		$config_val_input = array(
           array(
                'field'=>'nama_jenis',
                'label'=>'nama_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                                           
                     
            )
            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_taman_jenis(){
		if($this->validation_ins_taman()){
			
			
			$nama_jenis			= $this->input->post("nama_jenis");
			
			


			
			$data_insert = array(
						
						
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						
							
							
						);
			$insert = $this->atj->insert_taman_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/taman");
	}

	public function index_up_taman_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->atj->get_taman_jenis_where(array("id_jenis"=>$id_jenis))->row_array(); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function up_taman_jenis(){
		if($this->validation_ins_taman()){
			$id_jenis 				= $this->input->post("id_jenis");
			$nama_jenis 			= $this->input->post("nama_jenis");
			
			
			
			
			$data_insert = array(
							
							
							"nama_jenis"=>$nama_jenis
							
							
						);

			$data_where = array(
							"id_jenis"=>$id_jenis
						);

			$insert = $this->atj->update_taman_jenis($data_insert, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/taman");
	}

	public function delete_taman_jenis($id_jenis){
		$delete = $this->atj->delete_taman_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/taman");
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admindisperkimjenis extends CI_Controller {
//ok
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_disperkim_all_jenis", "adaj");
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "all";
		$data["list_disperkim_all_jenis"] 	  = $this->adaj->get_jenis();
		$this->load->view('admin_data/disperkim/main_admin_disperkim', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------rt_rw----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	
	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'id_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'nama_jenis',
                'label'=>'nama_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            
            array(
                'field'=>'id_kategori',
                'label'=>'id_kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                )    
              
            )
            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			
			$id_jenis			= $this->input->post("id_jenis");
			$nama_jenis			= $this->input->post("nama_jenis");
			$id_kategori		= $this->input->post("id_kategori");
						
			$data_insert = array(
						
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis,
							"id_kategori"=>$id_kategori
							
							
						);
			$insert = $this->adaj->insert($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/disperkim/all");
	}

	public function index_up(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->adaj->get_where(array("id_jenis"=>$id_jenis))->row_array(); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$id_jenis		= $this->input->post("id_jenis");
			$nama_jenis 			= $this->input->post("nama_jenis");
			$id_kategori 		= $this->input->post("id_kategori");
		
			
			
			
			$data_insert = array(
							
							"nama_jenis"=>$nama_jenis,
							"id_kategori"=>$id_kategori
							
						);

			$data_where = array(
							"id_jenis"=>$id_jenis
						);

			$insert = $this->adaj->update($data_insert, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/disperkim/all");
	}

	public function delete($id_jenis){
		$delete = $this->adaj->delete(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/disperkim/all");
	}

}

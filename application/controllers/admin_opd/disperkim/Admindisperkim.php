<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admindisperkim extends CI_Controller {
//ok
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_disperkim_all", "ada");
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "all";
		$data["list_disperkim_all"] 	  		  = $this->ada->get_disperkim_all();
		$data["list_disperkim_all_jenis"] 	  	  = $this->ada->get_disperkim_all_jenis();
		$data["list_disperkim_all_kategori"] 	  = $this->ada->get_disperkim_all_kategori();
		$data["list_disperkim_all_sub_jenis"] 	  = $this->ada->get_disperkim_all_sub_jenis();
		$this->load->view('admin_data/disperkim/main_admin_disperkim', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------rt_rw----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	
	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'id_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            
            array(
                'field'=>'id_sub_jenis',
                'label'=>'id_sub_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )

                ), 
            array(
                'field'=>'satuan',
                'label'=>'satuan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )
            ), array(
                'field'=>'jml',
                'label'=>'jml',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )
                  ),
                   array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )    
               
                     
            )
            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			
			$id_jenis			= $this->input->post("id_jenis");
			$id_sub_jenis		= $this->input->post("id_sub_jenis");
			$satuan				= $this->input->post("satuan");
			$jml				= $this->input->post("jml");
			$th					= $this->input->post("th");

			


			
			$data_insert = array(
						
							"id_disperkim"=>"",
							"id_jenis"=>$id_jenis,
							"id_sub_jenis"=>$id_sub_jenis,
							"satuan"=>$satuan,
							"jml"=>$jml,
							"th"=>$th
							
							
						);
			$insert = $this->ada->insert_disperkim_all($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/disperkim/all");
	}

	public function index_up(){
		$id_disperkim = $this->input->post("id_disperkim");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->ada->get_disperkim_all_where(array("id_disperkim"=>$id_disperkim)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$id_disperkim 	= $this->input->post("id_disperkim");

			$id_jenis 			= $this->input->post("nama_jenis");
			$id_sub_jenis 		= $this->input->post("nama_sub_jenis");
			$satuan				= $this->input->post("satuan");
			$jml				= $this->input->post("jml");
			$th 				= $this->input->post("th");
			
			
			
			$data_insert = array(
							
							
							"id_jenis"=>$id_jenis,
							"id_sub_jenis"=>$id_sub_jenis,
							"satuan"=>$satuan,
							"jml"=>$jml,
							"th"=>$th
							
						);

			$data_where = array(
							"id_disperkim"=>$id_disperkim
						);

			$update = $this->ada->update_disperkim_all($data_insert, $data_where);
				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/disperkim/all");
	}

	public function delete($id_disperkim){
		$delete = $this->ada->delete_disperkim_all(array("id_disperkim"=>$id_disperkim));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/disperkim/all");
	}


#---------------------------kategori
	private function validation_ins_kategori(){
		$config_val_input = array(
            array(
                'field'=>'nama_kategori',
                'label'=>'nama_kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
               )           
                     
            )
            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_kategori(){
		if($this->validation_ins_kategori()){
			
			$nama_kategori		= $this->input->post("nama_kategori");
		
		
			$data_insert = array(
						
							"id_kategori"=>"",
							"nama_kategori"=>$nama_kategori
							
							
						);
			$insert = $this->ada->insert_disperkim_all_kategori($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/disperkim/all");
	}

	public function index_up_kategori(){
		$id_kategori = $this->input->post("id_kategori");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->ada->get_disperkim_all_kategori_where(array("id_kategori"=>$id_kategori)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function update_kategori(){
		if($this->validation_ins_kategori()){
		
			$nama_kategori 				= $this->input->post("nama_kategori");

			$id_kategori 			    = $this->input->post("id_kategori");
			
			
			
			$data = array(
						
							"nama_kategori"=>$nama_kategori
							
						);

			$where = array(

							"id_kategori"=>$id_kategori
						);

			$update = $this->ada->update_disperkim_all_kategori($data, $where);
				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/disperkim/all");
	}

	public function delete_kategori($id_kategori){
		$delete = $this->ada->delete_disperkim_all_kategori(array("id_kategori"=>$id_kategori));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/disperkim/all");
	}

	#---------------------------Jenis
	private function validation_ins_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'nama_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
               ),
               array(
                'field'=>'id_kategori',
                'label'=>'id_kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
               )           
                     
            )
            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_jenis(){
		if($this->validation_ins_jenis()){
			

			$nama_kategori		= $this->input->post("nama_jenis");
			$id_kategori 		= $this->input->post("id_kategori");
		
			$data_insert = array(
						
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis,
							"id_kategori"=>$id_kategori
							
							
							
						);
			$insert = $this->ada->insert_disperkim_all_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/disperkim/all");
	}

	public function index_up_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->ada->get_disperkim_all_jenis_where(array("id_jenis"=>$id_jenis))->row_array(); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function update_jenis(){
		if($this->validation_ins_jenis()){
		

			$nama_jenis			= $this->input->post("nama_jenis");
			$id_kategori 		= $this->input->post("id_kategori");

			$id_jenis			= $this->input->post("id_jenis");
			
			
			
			$data = array(
						
							"nama_jenis"=>$nama_jenis,
							"id_kategori"=>$id_kategori

							
						);

			$where = array(

							"id_jenis"=>$id_jenis
						);

			$update = $this->ada->update_disperkim_all_jenis($data, $where);
				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/disperkim/all");
	}

	public function delete_jenis($id_jenis){
		$delete = $this->ada->delete_disperkim_all_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/disperkim/all");
	}

#--------------------------- Sub Jenis

	private function validation_ins_sub_jenis(){
		$config_val_input = array(
			array(
                'field'=>'id_jenis',
                'label'=>'id_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
               ),
            array(
                'field'=>'nama_sub_jenis',
                'label'=>'nama_sub_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
               )           
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_sub_jenis(){
		if($this->validation_ins_sub_jenis()){
			

			$id_jenis		= $this->input->post("id_jenis");
			$nama_sub_jenis 		= $this->input->post("nama_sub_jenis");
		
			$data_insert = array(
						
							"id_sub_jenis"=>"",
							"id_jenis"=>$id_jenis,
							"nama_sub_jenis"=>$nama_sub_jenis
							
							
							
							
						);
			$insert = $this->ada->insert_disperkim_all_sub_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/disperkim/all");
	}

	public function index_up_sub_jenis(){
		$id_sub_jenis = $this->input->post("id_sub_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->ada->get_disperkim_all_sub_jenis_where(array("id_sub_jenis"=>$id_sub_jenis))->row_array(); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function update_sub_jenis(){
		if($this->validation_ins_sub_jenis()){
		

			$nama_sub_jenis			= $this->input->post("nama_sub_jenis");
			$id_jenis 		= $this->input->post("id_jenis");

			$id_sub_jenis			= $this->input->post("id_sub_jenis");
			
			
			
			$data = array(
						
							"id_kategori"=>$id_kategori,
							"nama_sub_jenis"=>$nama_sub_jenis
							

							
						);

			$where = array(

							"id_sub_jenis"=>$id_sub_jenis
						);

			$update = $this->ada->update_disperkim_all_sub_jenis($data, $where);
				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/disperkim/all");
	}

	public function delete_sub_jenis($id_sub_jenis){
		$delete = $this->ada->delete_disperkim_all_sub_jenis(array("id_sub_jenis"=>$id_sub_jenis));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/disperkim/all");
	}


}

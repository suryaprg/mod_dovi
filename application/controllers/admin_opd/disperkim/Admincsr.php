<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admincsr extends CI_Controller {
//ok
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_csr", "ac");

		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "csr";
		$data["list_csr"] = $this->ac->get_csr();
		$this->load->view('admin_data/disperkim/main_admin_disperkim', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------rt_rw----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	
	private function validation_ins_csr(){
		$config_val_input = array(
           array(
                'field'=>'pemberi_csr',
                'label'=>'pemberi_csr',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            
            array(
                'field'=>'tgl_nphd',
                'label'=>'tgl_nphd',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
             array(
                'field'=>'no_nphd',
                'label'=>'no_nphd',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	
                 )
                       
            ),
              array(
                'field'=>'tgl_bash',
                'label'=>'tgl_bash',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	
                 )
                       
            ),
               array(
                'field'=>'no_bash',
                'label'=>'no_bash',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )
                       
            ),
                array(
                'field'=>'obj_pembangunan',
                'label'=>'obj_pembangunan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )
                       
            ),
                 array(
                'field'=>'rab',
                'label'=>'rab',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	
                 )
                       
            )
            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_csr(){
		if($this->validation_ins_csr()){
			
			$pemberi_csr 	= $this->input->post("pemberi_csr");
			$tgl_nphd 		= $this->input->post("tgl_nphd");
			$no_nphd		= $this->input->post("no_nphd");
			$tgl_bash		= $this->input->post("tgl_bash");
			$no_bash		= $this->input->post("no_bash");
			$obj_pembangunan= $this->input->post("obj_pembangunan");
			$rab			= $this->input->post("rab");


			
			$data_insert = array(
						
							"id_csr"=>"",
							"pemberi_csr"=>$pemberi_csr,
							"tgl_nphd"=>$tgl_nphd,
							"no_nphd"=>$no_nphd,
							"tgl_bash"=>$tgl_bash,
							"no_bash"=>$no_bash,
							"obj_pembangunan"=>$obj_pembangunan,
							"rab"=>$rab
							
						);
			$insert = $this->ac->insert_csr($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/csr");
	}

	public function index_up_csr(){
		$id_csr = $this->input->post("id_csr");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->ac->get_csr_where(array("id_csr"=>$id_csr))->row_array(); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function up_csr(){
		if($this->validation_ins_csr()){
			$id_csr 		= $this->input->post("id_csr");
			$pemberi_csr 	= $this->input->post("pemberi_csr");
			$tgl_nphd 		= $this->input->post("tgl_nphd");
			$no_nphd		= $this->input->post("no_nphd");
			$tgl_bash		= $this->input->post("tgl_bash");
			$no_bash		= $this->input->post("no_bash");
			$obj_pembangunan= $this->input->post("obj_pembangunan");
			$rab			= $this->input->post("rab");
			
			
			$data_insert = array(

							
							"pemberi_csr"=>$pemberi_csr,
							"tgl_nphd"=>$tgl_nphd,
							"no_nphd"=>$no_nphd,
							"tgl_bash"=>$tgl_bash,
							"no_bash"=>$no_bash,
							"obj_pembangunan"=>$obj_pembangunan,
							"rab"=>$rab
							
						);

			$data_where = array(
							"id_csr"=>$id_csr
						);

			$insert = $this->ac->update_csr($data_insert, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/csr");
	}

	public function delete_csr($id_csr){
		$delete = $this->ac->delete_csr(array("id_csr"=>$id_csr));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/csr");
	}

}

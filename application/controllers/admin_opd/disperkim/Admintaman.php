<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admintaman extends CI_Controller {
//ok
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_taman", "at");
		

		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "taman";
		$data["list_taman"] = $this->at->get_taman();
		$data["list_taman_jenis"] = $this->at->get_taman_jenis();
		$this->load->view('admin_data/disperkim/main_admin_disperkim', $data);
	}

//================================================ LP taman ====================================================//
	private function validation_ins_taman(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )                      
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_taman(){
		if($this->validation_ins_taman()){
			$id_jenis 		= $this->input->post("id_jenis");
			$tgl	= $this->input->post("tgl");
			$jml 			= $this->input->post("jml");
			$lokasi 			= $this->input->post("lokasi");
			$keterangan			= $this->input->post("keterangan");

			$data_insert = array(
							"id_taman"=>"",
							"id_jenis"=>$id_jenis,
							"th"=>$th,
							"jml"=>$jml
							

							
						);
			$insert = $this->at->insert_taman($data_insert);
				if($insert){
					redirect('admin/taman');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_taman(){
		$id_taman 	= $this->input->post("id_taman");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->at->get_taman_where(array("id_taman"=>$id_taman)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_taman(){
		if($this->validation_ins_taman()){
			$id_taman 	= $this->input->post("id_taman");

			$id_jenis 		= $this->input->post("id_jenis");
			$th 	= $this->input->post("th");
			$jml 			= $this->input->post("jml");
			

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"jml"=>$jml
						
						
					);

			$data_where = array(
							"id_taman"=>$id_taman,
						);

			$insert = $this->at->update($data, $data_where);
				if($insert){
					redirect('admin/taman');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_taman($id_taman){
		$delete = $this->at->delete_taman(array("id_taman"=>$id_taman));
		if($delete){
			echo "del";
			redirect('admin/taman');
		}else {
			echo "fail";
		}
	}
//================================================ LP taman ====================================================//
	//================================================ LP taman JENIS ==============================================//
	private function validation_ins_taman_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_taman_jenis(){
		$id_jenis 		= $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->at->get_taman_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_taman_jenis(){
		if($this->validation_ins_taman_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis

						);
			$insert = $this->at->insert_taman_jenis($data_insert);
				if($insert){
					redirect('admin/taman');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_taman_jenis(){
		if($this->validation_ins_taman_jenis()){
			$id_jenis 	= $this->input->post("id_jenis");
		

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->at->update_taman_jenis($data, $data_where);
				if($insert){
					redirect('admin/taman');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_taman_jenis($id_jenis){
		$delete = $this->at->delete_taman_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/taman');
		}else {
			echo "fail";
		}
	}	
//================================================ LP taman JENIS ==============================================//

}
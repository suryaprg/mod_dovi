<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpohon extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/admin_pohon", "ap");
		$this->load->library("response_message");

	}

	public function index(){
		$data["page"] 						= "pohon";
		$data["list_pohon"] 				= $this->ap->get_pohon();
		$data["list_pohon_jenis"] 			= $this->ap->get_pohon_jenis();
	//	$data["list_pohon_sub_jenis"] 	= $this->ad->get_pohon_sub_jenis();
		$this->load->view('admin_data/disperkim/main_admin_disperkim', $data);
	}

//================================================ LP pohon ====================================================//
	private function validation_ins_pohon(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'tgl',
                'label'=>'Tanggal',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'lokasi',
                'label'=>'Lokasi',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'keterangan',
                'label'=>'keterangan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_pohon(){
		if($this->validation_ins_pohon()){
			$id_jenis 		= $this->input->post("id_jenis");
			$tgl	= $this->input->post("tgl");
			$jml 			= $this->input->post("jml");
			$lokasi 			= $this->input->post("lokasi");
			$keterangan			= $this->input->post("keterangan");

			$data_insert = array(
							"id_pohon"=>"",
							"id_jenis"=>$id_jenis,
							"tgl"=>$tgl,
							"jml"=>$jml,
							"lokasi"=>$lokasi,
							"keterangan"=>$keterangan

							
						);
			$insert = $this->ap->insert_pohon($data_insert);
				if($insert){
					redirect('admin/pohon');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_pohon(){
		$id_pohon 	= $this->input->post("id_pohon");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->ap->get_pohon_where(array("id_pohon"=>$id_pohon)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_pohon(){
		if($this->validation_ins_pohon()){
			$id_pohon 	= $this->input->post("id_pohon");

			$id_jenis 		= $this->input->post("id_jenis");
			$tgl 	= $this->input->post("tgl");
			$jml 			= $this->input->post("jml");
			$lokasi 			= $this->input->post("lokasi");
			$keterangan 			= $this->input->post("keterangan");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"tgl"=>$tgl,
						"jml"=>$jml,
						"lokasi"=>$lokasi,
						"keterangan"=>$keterangan
						
					);

			$data_where = array(
							"id_pohon"=>$id_pohon,
						);

			$insert = $this->ap->update($data, $data_where);
				if($insert){
					redirect('admin/pohon');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_pohon($id_pohon){
		$delete = $this->ap->delete_pohon(array("id_pohon"=>$id_pohon));
		if($delete){
			echo "del";
			redirect('admin/pohon');
		}else {
			echo "fail";
		}
	}
//================================================ LP pohon ====================================================//
	//================================================ LP pohon JENIS ==============================================//
	private function validation_ins_pohon_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_pohon_jenis(){
		$id_jenis 		= $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->ap->get_pohon_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_pohon_jenis(){
		if($this->validation_ins_pohon_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis

						);
			$insert = $this->ap->insert_pohon_jenis($data_insert);
				if($insert){
					redirect('admin/pohon');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_pohon_jenis(){
		if($this->validation_ins_pohon_jenis()){
			$id_jenis 	= $this->input->post("id_jenis");
		

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->ap->update_pohon_jenis($data, $data_where);
				if($insert){
					redirect('admin/pohon');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_pohon_jenis($id_jenis){
		$delete = $this->ap->delete_pohon_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/pohon');
		}else {
			echo "fail";
		}
	}	
//================================================ LP pohon JENIS ==============================================//

}
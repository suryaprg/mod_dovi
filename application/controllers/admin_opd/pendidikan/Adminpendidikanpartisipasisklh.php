<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpendidikanpartisipasisklh extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_pendidikan_partisipasi_sklh", "apps");
		$this->load->model("admin_opd/Admin_pendidikan_partisipasi_sklh_jenis", "appsj");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "pendidikan_partisipasi_sklh";
		$data["list_pendidikan"] = $this->apps->get();
		$data["list_pendidikan_jenis"] = $this->appsj->get();
		$this->load->view('admin_data/pendidikan/main_admin_pendidikan', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_pertanian_lahan----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'id_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'prosentase',
                'label'=>'prosentase',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$prosentase = $this->input->post("prosentase");
			$id_jenis = $this->input->post("id_jenis");

			$data = array(
						"id_partisipasi_sklh"=>"",
						"th"=>$th,
						"prosentase"=>$prosentase,
						"id_jenis"=>$id_jenis
					);
			
			$insert = $this->apps->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pendidikan_partisipasi_sklh");
	}

	public function index_up(){
		$id_partisipasi_sklh = $this->input->post("id_partisipasi_sklh");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->apps->get_where(array("id_partisipasi_sklh"=>$id_partisipasi_sklh))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$prosentase = $this->input->post("prosentase");
			$id_jenis = $this->input->post("id_jenis");

			$id_partisipasi_sklh = $this->input->post("id_partisipasi_sklh");
			
			$where = array(
							"id_partisipasi_sklh"=>$id_partisipasi_sklh
						);
			
			$data = array(
						"th"=>$th,
						"prosentase"=>$prosentase,
						"id_jenis"=>$id_jenis
					);
			

			$update = $this->apps->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pendidikan_partisipasi_sklh");
	}

	public function delete($id_partisipasi_sklh){
		$delete = $this->apps->delete(array("id_partisipasi_sklh"=>$id_partisipasi_sklh));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/pendidikan_partisipasi_sklh");
	}

	
}

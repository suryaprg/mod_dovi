<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpendidikanpenduduk extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_pendidikan_penduduk", "app");
		$this->load->model("admin_opd/Admin_pendidikan_penduduk_jenis", "appj");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "pendidikan_penduduk";
		$data["list_pendidikan"] = $this->app->get();
		$data["list_pendidikan_jenis"] = $this->appj->get();
		$this->load->view('admin_data/pendidikan/main_admin_pendidikan', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_pertanian_lahan----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'id_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'prosentase',
                'label'=>'prosentase',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$prosentase = $this->input->post("prosentase");
			$id_jenis = $this->input->post("id_jenis");

			$data = array(
						"id_pendidikan"=>"",
						"th"=>$th,
						"prosentase"=>$prosentase,
						"id_jenis"=>$id_jenis
					);
			
			$insert = $this->app->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pendidikan_penduduk");
	}

	public function index_up(){
		$id_pendidikan = $this->input->post("id_pendidikan");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->app->get_where(array("id_pendidikan"=>$id_pendidikan))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$prosentase = $this->input->post("prosentase");
			$id_jenis = $this->input->post("id_jenis");

			$id_pendidikan = $this->input->post("id_pendidikan");
			
			$where = array(
							"id_pendidikan"=>$id_pendidikan
						);
			
			$data = array(
						"th"=>$th,
						"prosentase"=>$prosentase,
						"id_jenis"=>$id_jenis
					);
			

			$update = $this->app->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pendidikan_penduduk");
	}

	public function delete($id_pendidikan){
		$delete = $this->app->delete(array("id_pendidikan"=>$id_pendidikan));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/pendidikan_penduduk");
	}

	
}

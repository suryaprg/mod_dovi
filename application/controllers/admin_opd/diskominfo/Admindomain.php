<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admindomain extends CI_Controller {
//ok
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_domain", "ad");

		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "domain";
		$data["list_domain"] = $this->ad->get_domain();
		$this->load->view('admin_data/diskominfo/main_admin_kominfo', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------rt_rw----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	
	private function validation_ins_domain(){
		$config_val_input = array(
           array(
                'field'=>'nama_skpd',
                'label'=>'Nama',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'subdomain',
                'label'=>'Sub Domain',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_domain(){
		if($this->validation_ins_domain()){
			
			$nama_skpd 		= $this->input->post("nama_skpd");
			$subdomain 		= $this->input->post("subdomain");
			$th 			= $this->input->post("th");
			
			$data_insert = array(
							"id_aplikasi"=>"",
							"nama_skpd"=>$nama_skpd,
							"subdomain"=>$subdomain,
							"th"=>$th,
							
						);
			$insert = $this->ad->insert_domain($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/domain");
	}

	public function index_up_domain(){
		$id_aplikasi = $this->input->post("id_aplikasi");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->ad->get_domain_where(array("id_aplikasi"=>$id_aplikasi)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function up_domain(){
		if($this->validation_ins_domain()){
			$id_aplikasi = $this->input->post("id_aplikasi");

			
			$nama_skpd = $this->input->post("nama_skpd");
			$subdomain = $this->input->post("subdomain");
			$th = $this->input->post("th");
			
			
			$data_insert = array(
							
							"nama_skpd"=>$nama_skpd,
							"subdomain"=>$subdomain,
							"th"=>$th
							
						);

			$data_where = array(
							"id_aplikasi"=>$id_aplikasi
						);

			$insert = $this->ad->update_domain($data_insert, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/domain");
	}

	public function delete_domain($id_aplikasi){
		$delete = $this->ad->delete_domain(array("id_aplikasi"=>$id_aplikasi));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/domain");
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminaplikasi extends CI_Controller {
//ok
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_aplikasi", "aa");

		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "aplikasi";
		$data["list_aplikasi"] = $this->aa->get_aplikasi();
		$this->load->view('admin_data/diskominfo/main_admin_kominfo', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------rt_rw----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	
	private function validation_ins_aplikasi(){
		$config_val_input = array(
            array(
                'field'=>'id_aplikasi',
                'label'=>'id_aplikasi',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'aplikasi',
                'label'=>'Aplikasi',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'fungsi',
                'label'=>'Fungsi',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )
                       
            ),
            array(
                'field'=>'keterangan',
                'label'=>'Keterangan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_aplikasi(){
		if($this->validation_ins_aplikasi()){
			
			$aplikasi 		= $this->input->post("aplikasi");
			$fungsi 		= $this->input->post("fungsi");
			$keterangan		= $this->input->post("keterangan");
			
			$data_insert = array(
						
							"id_aplikasi"=>"",
							"aplikasi"=>$aplikasi,
							"fungsi"=>$fungsi,
							"keterangan"=>$keterangan
							
						);
			$insert = $this->aa->insert_aplikasi($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/aplikasi");
	}

	public function index_up_aplikasi(){
		$id_aplikasi = $this->input->post("id_aplikasi");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->atw->get_aplikasi_where(array("id_aplikasi"=>$id_aplikasi))->row_array(); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}


	public function up_aplikasi(){
		if($this->validation_ins_aplikasi()){
			$id_aplikasi = $this->input->post("id_aplikasi");
			$aplikasi = $this->input->post("aplikasi");
			$fungsi = $this->input->post("fungsi");
			$keterangan = $this->input->post("keterangan");
			
			
			$data_insert = array(
							
							"aplikasi"=>$aplikasi,
							"fungsi"=>$fungsi,
							"keterangan"=>$keterangan
							
						);

			$data_where = array(
							"id_aplikasi"=>$id_aplikasi
						);

			$insert = $this->aa->update_aplikasi($data_insert, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/aplikasi");
	}

	public function delete_aplikasi($id_aplikasi){
		$delete = $this->aa->delete_aplikasi(array("id_aplikasi"=>$id_aplikasi));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/aplikasi");
	}

}

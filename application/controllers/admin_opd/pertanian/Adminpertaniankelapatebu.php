<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpertaniankelapatebu extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_pertanian_kelapa_tebu", "apkt");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "pertanian_kelapa_tebu";
		$data["list_pertanian"] = $this->apkt->get();
		$this->load->view('admin_data/pertanian/main_admin_pertanian', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_pertanian_lahan----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'jml_tebu',
                'label'=>'jml_tebu',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'jml_kelapa',
                'label'=>'jml_kelapa',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$jml_kelapa = $this->input->post("jml_kelapa");
			$jml_tebu = $this->input->post("jml_tebu");

			$data = array(
						"id_pertanian"=>"",
						"th"=>$th,
						"jml_kelapa"=>$jml_kelapa,
						"jml_tebu"=>$jml_tebu
					);
			
			$insert = $this->apkt->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kelapa_tebu");
	}

	public function index_up(){
		$id_pertanian = $this->input->post("id_pertanian");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->apkt->get_where(array("id_pertanian"=>$id_pertanian))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$jml_kelapa = $this->input->post("jml_kelapa");
			$jml_tebu = $this->input->post("jml_tebu");

			$id_pertanian = $this->input->post("id_pertanian");
			
			$where = array(
							"id_pertanian"=>$id_pertanian
						);
			
			$data = array(
						"th"=>$th,
						"jml_kelapa"=>$jml_kelapa,
						"jml_tebu"=>$jml_tebu
					);

			$update = $this->apkt->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/kelapa_tebu");
	}

	public function delete($id_pertanian){
		$delete = $this->apkt->delete(array("id_pertanian"=>$id_pertanian));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/kelapa_tebu");
	}

	
}

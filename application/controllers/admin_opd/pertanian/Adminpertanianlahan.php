<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpertanianlahan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_pertanian_lahan", "apl");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "pertanian_lahan";
		$data["list_pertanian"] = $this->apl->get();
		$this->load->view('admin_data/pertanian/main_admin_pertanian', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_pertanian_lahan----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'jenis_lahan',
                'label'=>'jenis_lahan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'luas_lahan',
                'label'=>'luas_lahan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$jenis_lahan = $this->input->post("jenis_lahan");
			$th = $this->input->post("th");
			$luas_lahan = $this->input->post("luas_lahan");

			$data = array(
						"id_lahan"=>"",
						"jenis_lahan"=>$jenis_lahan,
						"th"=>$th,
						"luas_lahan"=>$luas_lahan
					);
			
			$insert = $this->apl->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pertanian_lahan");
	}

	public function index_up(){
		$id_lahan = $this->input->post("id_lahan");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->apl->get_where(array("id_lahan"=>$id_lahan))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$id_lahan =  $this->input->post("id_lahan");
			$jenis_lahan = $this->input->post("jenis_lahan");
			$th = $this->input->post("th");
			$luas_lahan = $this->input->post("luas_lahan");


			$where = array(
							"id_lahan"=>$id_lahan
						);
			
			$data = array(
						"jenis_lahan"=>$jenis_lahan,
						"th"=>$th,
						"luas_lahan"=>$luas_lahan
					);
			$update = $this->apl->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pertanian_lahan");
	}

	public function delete($id_lahan){
		$delete = $this->apl->delete(array("id_lahan"=>$id_lahan));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/pertanian_lahan");
	}

	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpertanianlahanpenggunaan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_pertanian_lahan_penggunaan", "aplp");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "pertanian_lahan_penggunaan";
		$data["list_pertanian"] = $this->aplp->get();
		$this->load->view('admin_data/pertanian/main_admin_pertanian', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_pertanian_lahan_penggunaan---------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'jenis_peng',
                'label'=>'jenis_peng',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'luas_pend',
                'label'=>'luas_pend',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$jenis_peng = $this->input->post("jenis_peng");
			$th = $this->input->post("th");
			$luas_pend = $this->input->post("luas_pend");

			$data = array(
						"id_peng"=>"",
						"jenis_peng"=>$jenis_peng,
						"th"=>$th,
						"luas_pend"=>$luas_pend
					);
			
			$insert = $this->aplp->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pertanian_lahan_penggunaan");
	}

	public function index_up(){
		$id_peng = $this->input->post("id_peng");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->aplp->get_where(array("id_peng"=>$id_peng))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$jenis_peng = $this->input->post("jenis_peng");
			$th = $this->input->post("th");
			$luas_pend = $this->input->post("luas_pend");

			$id_peng = $this->input->post("id_peng");

			$where = array(
							"id_peng"=>$id_peng
						);
			
			$data = array(
						"jenis_peng"=>$jenis_peng,
						"th"=>$th,
						"luas_pend"=>$luas_pend
					);

			$update = $this->aplp->update($data, $where);
				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pertanian_lahan_penggunaan");
	}

	public function delete($id_peng){
		$delete = $this->aplp->delete(array("id_peng"=>$id_peng));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/pertanian_lahan_penggunaan");
	}

	
}
	
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpertanianholtijml extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_pertanian_holti_jml", "aph");
		$this->load->model("admin_opd/Admin_pertanian_holti_jenis", "aphj");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "pertanian_hortikultura";
		$data["list_pertanian"] = $this->aph->get();
		$data["list_pertanian_jenis"] = $this->aphj->get();
		$this->load->view('admin_data/pertanian/main_admin_pertanian', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_pertanian_lahan----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'id_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'jml',
                'label'=>'jml',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			$id_jenis = $this->input->post("id_jenis");

			$data = array(
						"id_holti"=>"",
						"th"=>$th,
						"jml"=>$jml,
						"id_jenis"=>$id_jenis
					);
			
			$insert = $this->aph->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pertanian_hortikultura");
	}

	public function index_up(){
		$id_holti = $this->input->post("id_holti");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->aph->get_where(array("id_holti"=>$id_holti))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			$id_jenis = $this->input->post("id_jenis");

			$id_holti = $this->input->post("id_holti");
			
			$where = array(
							"id_holti"=>$id_holti
						);
			
			$data = array(
						"th"=>$th,
						"jml"=>$jml,
						"id_jenis"=>$id_jenis
					);
			

			$update = $this->aph->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pertanian_hortikultura");
	}

	public function delete($id_holti){
		$delete = $this->aph->delete(array("id_holti"=>$id_holti));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/pertanian_hortikultura");
	}

	
}

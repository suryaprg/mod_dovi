<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpertanianholtijenis extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_pertanian_holti_jenis", "aphj");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}


#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_pertanian_lahan----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'nama_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'kategori',
                'label'=>'kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$nama_jenis = $this->input->post("nama_jenis");
			$kategori = $this->input->post("kategori");

			$data = array(
						"id_jenis"=>"",
						"nama_jenis"=>$nama_jenis,
						"kategori"=>$kategori
					);
			
			$insert = $this->aphj->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pertanian_hortikultura");
	}

	public function index_up(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->aphj->get_where(array("id_jenis"=>$id_jenis))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$nama_jenis = $this->input->post("nama_jenis");
			$kategori = $this->input->post("kategori");


			$id_jenis = $this->input->post("id_jenis");
			
			$where = array(
							"id_jenis"=>$id_jenis
						);
			
			$data = array(
						"nama_jenis"=>$nama_jenis,
						"kategori"=>$kategori
					);

			$update = $this->aphj->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pertanian_hortikultura");
	}

	public function delete($id_jenis){
		$delete = $this->aphj->delete(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/pertanian_hortikultura");
	}

	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpertaniankomoditi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_pertanian_komoditi", "apk");
		$this->load->model("admin_opd/Admin_pertanian_komoditas_jenis", "apkj");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "pertanian_komoditi";
		$data["list_pertanian"] = $this->apk->get();
		$data["list_pertanian_jenis"] = $this->apkj->get();
		$this->load->view('admin_data/pertanian/main_admin_pertanian', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin_pertanian_lahan----------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'id_jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'jml',
                'label'=>'jml',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			$id_jenis = $this->input->post("id_jenis");

			$data = array(
						"id_komoditas"=>"",
						"th"=>$th,
						"jml"=>$jml,
						"id_jenis"=>$id_jenis
					);
			
			$insert = $this->apk->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pertanian_komoditi");
	}

	public function index_up(){
		$id_komoditas = $this->input->post("id_komoditas");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->apk->get_where(array("id_komoditas"=>$id_komoditas))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			$id_jenis = $this->input->post("id_jenis");

			$id_komoditas = $this->input->post("id_komoditas");
			
			$where = array(
							"id_komoditas"=>$id_komoditas
						);
			
			$data = array(
						"th"=>$th,
						"jml"=>$jml,
						"id_jenis"=>$id_jenis
					);
			

			$update = $this->apk->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/pertanian_komoditi");
	}

	public function delete($id_komoditas){
		$delete = $this->apk->delete(array("id_komoditas"=>$id_komoditas));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/pertanian_komoditi");
	}

	
}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admindpmptsp extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/admin_dpmptsp", "ad");
		$this->load->library("response_message");

	}

	public function index(){
		$data["page"] 						= "dpmptsp";
		$data["list_dpmptsp"] 				= $this->ad->get_dpmptsp();
		$data["list_dpmptsp_jenis"] 		= $this->ad->get_dpmptsp_jenis();
		
		$this->load->view('admin_data/dpmptsp/main_admin_dpmptsp', $data);
	}

//================================================ LP dpmptsp ====================================================//
	private function validation_ins_dpmptsp(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
          
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
             array(
                'field'=>'terbit',
                'label'=>'Terbit',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
              array(
                'field'=>'investasi',
                'label'=>'Investasi',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'tk',
                'label'=>'tk',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_dpmptsp(){
		if($this->validation_ins_dpmptsp()){
			$id_jenis 		= $this->input->post("id_jenis");
			$th 			= $this->input->post("th");
			$terbit 			= $this->input->post("terbit");
			$investasi 			= $this->input->post("investasi");
			$tk			= $this->input->post("tk");
			
			$data_insert = array(
							"id_main"=>"",
							"id_jenis"=>$id_jenis,
							"th"=>$th,
							"terbit"=>$terbit,
							"investasi"=>$investasi,
							"tk"=>$tk
						);
			$insert = $this->ad->insert_dpmptsp($data_insert);
				if($insert){
					redirect('admin/dpmptsp');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_dpmptsp(){
		$id_main 	= $this->input->post("id_main");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->ad->get_dpmptsp_where(array("id_main"=>$id_main)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_dpmptsp(){
		if($this->validation_ins_dpmptsp()){
			$id_main 	= $this->input->post("id_main");

			$id_jenis 		= $this->input->post("id_jenis");
			$th 			= $this->input->post("th");
			$terbit 			= $this->input->post("terbit");
			$investasi			= $this->input->post("investasi");
			$tk 			= $this->input->post("tk");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"terbit"=>$terbit,
						"investasi"=>$investasi,
						"tk"=>$tk
					);

			$data_where = array(
							"id_main"=>$id_main,
						);

			$insert = $this->ad->update_dpmptsp($data, $data_where);
				if($insert){
					redirect('admin/dpmptsp');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_dpmptsp($id_main){
		$delete = $this->ad->delete_dpmptsp(array("id_main"=>$id_main));
		if($delete){
			echo "del";
			redirect('admin/dpmptsp');
		}else {
			echo "fail";
		}
	}
//================================================ LP dpmptsp ====================================================//

//================================================ LP dpmptsp JENIS ==============================================//
	private function validation_ins_dpmptsp_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_dpmptsp_jenis(){
		$id_jenis 		= $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->ad->get_dpmptsp_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_dpmptsp_jenis(){
		if($this->validation_ins_dpmptsp_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis

						);
			$insert = $this->ad->insert_dpmptsp_jenis($data_insert);
				if($insert){
					redirect('admin/dpmptsp');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_dpmptsp_jenis(){
		if($this->validation_ins_dpmptsp_jenis()){
			$id_jenis 	= $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->ad->update_dpmptsp_jenis($data, $data_where);
				if($insert){
					redirect('admin/dpmptsp');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_dpmptsp_jenis($id_jenis){
		$delete = $this->ad->delete_dpmptsp_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/dpmptsp');
		}else {
			echo "fail";
		}
	}	
//================================================ LP dpmptsp JENIS ==============================================//


}
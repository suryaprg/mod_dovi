<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admintransjalan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_trans_jalan", "atj");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "trans_jalan";
		
		$data["list_jalan"] = $this->atj->get();

		$this->load->view('admin_data/transportasi/main_admin_trans', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Jenis Kendaraan--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'kategori_jln',
                'label'=>'kategori_jln',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),array(
                'field'=>'jln_negara',
                'label'=>'jln_negara',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'jln_prov',
                'label'=>'jln_prov',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),array(
                'field'=>'jln_kota',
                'label'=>'jln_kota',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){
			$kategori_jln = $this->input->post("kategori_jln");
			$th = $this->input->post("th");
			$jln_negara = $this->input->post("jln_negara");
			$jln_prov = $this->input->post("jln_prov");
			$jln_kota = $this->input->post("jln_kota");

			$data = array(
						"id_jln"=>"",
						"kategori_jln"=>$kategori_jln,
						"th"=>$th,
						"jln_negara"=>$jln_negara,
						"jln_prov"=>$jln_prov,
						"jln_kota"=>$jln_kota
					);
			
			$insert = $this->atj->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/trans_jalan");
	}

	public function index_up(){
		$id_jln = $this->input->post("id_jln");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->atj->get_where(array("id_jln"=>$id_jln))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$kategori_jln = $this->input->post("kategori_jln");
			$th = $this->input->post("th");
			$jln_negara = $this->input->post("jln_negara");
			$jln_prov = $this->input->post("jln_prov");
			$jln_kota = $this->input->post("jln_kota");

			$id_jln = $this->input->post("id_jln");

			$where = array(
							"id_jln"=>$id_jln
						);
			
			
			$data = array(
						"kategori_jln"=>$kategori_jln,
						"th"=>$th,
						"jln_negara"=>$jln_negara,
						"jln_prov"=>$jln_prov,
						"jln_kota"=>$jln_kota
					);

			$update = $this->atj->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/trans_jalan");
	}

	public function delete($id_jln){
		$delete = $this->atj->delete(array("id_jln"=>$id_jln));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/trans_jalan");
	}

	
}

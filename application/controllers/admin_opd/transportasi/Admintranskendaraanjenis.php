<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admintranskendaraanjenis extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_trans_jenis_kendaraan", "atjk");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}


#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Jenis Kendaraan--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'keterangan',
                'label'=>'keterangan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){

			$keterangan = $this->input->post("keterangan");

			$data = array(
						"id_jenis_kendaraan"=>"",
						"keterangan"=>$keterangan
					);
			
			$insert = $this->atjk->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/trans_kendaraan");
	}

	public function index_up(){
		$id_jenis_kendaraan = $this->input->post("id_jenis_kendaraan");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->atjk->get_where(array("id_jenis_kendaraan"=>$id_jenis_kendaraan))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$keterangan = $this->input->post("keterangan");
			$id_jenis_kendaraan = $this->input->post("id_jenis_kendaraan");

			$where = array(
							"id_jenis_kendaraan"=>$id_jenis_kendaraan
						);
			
			$data = array(
						"keterangan"=>$keterangan
					);

			$update = $this->atjk->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/trans_kendaraan");
	}

	public function delete($id_jenis_kendaraan){
		$delete = $this->atjk->delete(array("id_jenis_kendaraan"=>$id_jenis_kendaraan));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/trans_kendaraan");
	}

	
}

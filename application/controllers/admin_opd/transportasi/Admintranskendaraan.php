<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admintranskendaraan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/Admin_trans_jenis_kendaraan", "atjk");
		$this->load->model("admin_opd/Admin_trans_jml_kendaraan", "atk");
		
		$this->load->library("response_message");

		if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "trans_kendaraan";
		
		$data["list_kendaraan_jenis"] = $this->atjk->get();
		$data["list_jml_kendaraan"] = $this->atk->get();

		$this->load->view('admin_data/transportasi/main_admin_trans', $data);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Jenis Kendaraan--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis_kendaraan',
                'label'=>'id_jenis_kendaraan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'jml',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert(){
		if($this->validation_ins()){

			$id_jenis_kendaraan = $this->input->post("id_jenis_kendaraan");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");

			$data = array(
						"id_jml_kendaraan"=>"",
						"id_jenis_kendaraan"=>$id_jenis_kendaraan,
						"th"=>$th,
						"jml"=>$jml
					);
			
			$insert = $this->atk->insert($data);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/trans_kendaraan");
	}

	public function index_up(){
		$id_jml_kendaraan = $this->input->post("id_jml_kendaraan");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_send =$this->atk->get_where(array("id_jml_kendaraan"=>$id_jml_kendaraan))->row_array(); 
		if(!empty($data_send)){
			$data["status"] = true;
			$data["val"] = $data_send;
		}

		print_r(json_encode($data));
	}


	public function update(){
		if($this->validation_ins()){
			$id_jenis_kendaraan = $this->input->post("id_jenis_kendaraan");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");

			$id_jml_kendaraan = $this->input->post("id_jml_kendaraan");
			
			$where = array(
							"id_jml_kendaraan"=>$id_jml_kendaraan
						);
			
			$data = array(
						"id_jenis_kendaraan"=>$id_jenis_kendaraan,
						"th"=>$th,
						"jml"=>$jml
					);


			$update = $this->atk->update($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."admin/trans_kendaraan");
	}

	public function delete($id_jml_kendaraan){
		$delete = $this->atk->delete(array("id_jml_kendaraan"=>$id_jml_kendaraan));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."admin/trans_kendaraan");
	}

	
}

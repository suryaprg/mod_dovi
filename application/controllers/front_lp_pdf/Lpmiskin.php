<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpmiskin extends CI_Controller{

    public function __construct(){
        parent::__construct();  
        $this->load->model('front_lp/Lp_miskin', 'lm');
        
        $this->load->library("response_message");

    }

#===========================================================================================================================
#------------------------------------------------------------Miskin-------------------------------------------------------
#===========================================================================================================================

    public function miskin($th_fn){
        $jenis = $this->lm->get_lp_jenis();

        $th_st = $th_fn-2;

        $str_title_data = "Data Kemiskinan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

        //------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <th>No</th>
                                <th>Keterangan</th>";

        $t_col = 2;

        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";

            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";
        
        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;


            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td align=\"center\">".($row_jenis+1)."</td>
                            <td>".$v_data->nama_jenis."</td>";
            
            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lm->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;
                $str_tbl .= "<td align=\"right\">".number_format($val_data["jml"],2,".",",")."</td>";

                $row_th++;
            }

            $str_tbl .= "</tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Data_Kemiskinan";    
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");  
        
        print_r("<pre>");
        // print_r($data["data_graph"]);
        print_r("<table border=\"1\">".$str_tbl."</table>");
        // print_r("<table>".$str_tbl."</table>");

        // $this->load->view("front_lp/miskin/miskin",$data);

    }

#===========================================================================================================================
#------------------------------------------------------------Miskin-------------------------------------------------------
#===========================================================================================================================
}
?>
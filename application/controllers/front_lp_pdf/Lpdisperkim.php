<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpdisperkim extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_disperkim', 'lp');
        $this->load->model('front_lp/Lp_disperkim_pohon', 'lpp');
        $this->load->model('front_lp/Lp_disperkim_taman', 'lpt');

        $this->load->model('front_lp/Lp_disperkim_all', 'lpa');

	}
    
    public function get_dsu(){
        $val_data = $this->lp->get_dsu();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data PSU yang di Serahkan Kepada Pemerintah Kota Malang";

        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"23%\">Lokasi</th>
                                <th width=\"23%\">Kawasan</th>
                                <th width=\"23%\">Pengembang</th>
                                <th width=\"23%\">Tanggal</th>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"5\">".$str_title."</th>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td align=\"center\">".($row_data++)."</td>
                            <td>".$v_data->lokasi."</td>
                            <td>".$v_data->nama_kawasan."</td>
                            <td>".$v_data->pengembang."</td>
                            <td align=\"center\">".$v_data->tgl."</td>
                        </tr>";

        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title;
        $data["kategori"] = "Dianas_pemrukiman";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }

    public function get_csr(){
        $val_data = $this->lp->get_csr();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Pemberi CSR di Kota Malang";

        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"19%\">Tanggal dan No. Bash</th>
                                <th width=\"19%\">Tanggal dan No. NPHD</th>
                                <th width=\"19%\">Objek Pembangunan</th>
                                <th width=\"19%\">Pemberi CSR</th>
                                <th width=\"19%\">RAB</th>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"6\">".$str_title."</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td align=\"center\">".($row_data+1)."</td>
                            <td>tgl : ".$v_data->tgl_bash." <br><br> No.Bash: ".$v_data->no_bash."</td>
                            <td>tgl : ".$v_data->tgl_nphd." <br><br> No.NPHD: ".$v_data->no_nphd."</td>
                            <td>".$v_data->obj_pembangunan."</td>
                            <td>".$v_data->pemberi_csr."</td>
                            <td>".$v_data->rab."</td>
                        </tr>";

        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title;
        $data["kategori"] = "Dianas_pemrukiman";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }

    public function get_pohon($th_fn){
        $val_data = $this->lpp->get_lp($th_fn);
        $val_jenis = $this->lpp->get_lp_jenis();

        $list_jenis = array();
        foreach ($val_jenis as $key => $value) {
            $list_jenis[0] = "-";
            $list_jenis[$value->id_jenis] = $value->nama_jenis;
        }

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Penanaman Pohon di Kota Malang";

        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"13%\">Tanggal</th>
                                <th width=\"20%\">Jenis Tanaman</th>
                                <th width=\"7%\">Jumlah Bibit</th>
                                <th width=\"30%\">Lokasi</th>
                                <th width=\"25%\">Keterangan</th>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"6\">".$str_title."</th>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";

        $data_graph = array();

       
            $row_data = 0;
            foreach ($val_data as $r_data => $v_data) {

                 $str_tbl .= "<tr>
                                <td align=\"center\">".($row_data+1)."</td>
                                <td align=\"center\">".$v_data->tgl."</td>
                                <td>".$list_jenis[$v_data->id_jenis]."</td>
                                <td align=\"center\">".$v_data->jml."</td>
                                <td>".$v_data->lokasi."</td>
                                <td>".$v_data->keterangan."</td>
                            </tr>";
                $row_data++;
            }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title;
        $data["kategori"] = "Dianas_pemrukiman";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");

        // print_r("<pre>");
        // print_r($val_jenis);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }

    public function get_taman($th_fn){
        // chart using Stacked Bar Chart

        $jenis = $this->lpt->get_lp_jenis();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Pemanfaatan Taman dan Hutan Kota dan Pedestrian di Kota Malang Tahun ".$th_st." - ".$th_fn;

        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"35%\">Keterangan</th>";

        $t_col = 2;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\">".$i."</th>";
            $t_col += 1;
        }
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title."</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";

        $row_jenis = 0;

        $series = "";
        foreach ($jenis as $r_jenis => $v_jenis) {
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;  

            $str_tbl .= " <tr>
                                <td align=\"center\">".($row_jenis+1)."</td>
                                <td>".$v_jenis->nama_jenis."</td>"; 
            
            $row_th= 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) { 
                $val_data = $this->lpt->get_lp(array("lp.id_jenis"=>$v_jenis->id_jenis));
                $data["list_data"][$row_jenis]["detail"][$i] = $val_data["jml"];

                $str_tbl .= "<td align=\"right\">".$val_data["jml"]."</td>";
                $row_th++;
            }
            $row_jenis++;

            $str_tbl .= "</tr>";
        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title;
        $data["kategori"] = "Dianas_pemrukiman";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'potrait');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");
        // print_r("<pre>");
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }
    
    public function get_disperkim_all($id_kategori,$th_fn){
        $kategori = $this->lpa->get_lp_kategori(array("id_kategori"=>$id_kategori));
        $th_st = $th_fn-1;

        //------------------------------------------------header-------------------------------------------------

        $str_title = "Data Disperkim di Kota Malang Tahun ".$th_st." - ".$th_fn;

        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"35%\">Keterangan</th>";
        

        $t_col = 2;
        for ($i=$th_fn-1; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"25%\">".$i."</th>";
            
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">th. </th>
                                <th>Kategori</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";
        $row_kate = 0;
        $list_data = array();

        foreach ($kategori as $r_kate => $v_kate) {
            $data["list_data"][$row_kate]["main"]["id_kategori"] = $v_kate->id_kategori;
            $data["list_data"][$row_kate]["main"]["nama_kategori"] = $v_kate->nama_kategori;  

            $str_tbl .= "<table border='1' class=\"data-table\" style=\"width: 100%; border-collapse: collapse;\">
                         <thead>
                          <tr>
                            <th colspan=\"2\">Data ".$v_kate->nama_kategori." di Kota Malang Tahun ".$th_st." - ".$th_fn."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Jenis Lahan</th>
                          </tr>
                         </thead>";          

            $jenis = $this->lpa->get_lp_jenis(array("id_kategori"=>$v_kate->id_kategori));
            $row_jenis = 0;
            foreach ($jenis as $r_jenis => $v_jenis) {
                $data["list_data"][$row_kate]["jenis"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
                $data["list_data"][$row_kate]["jenis"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;

                $str_tbl .=    "<tr class=\"jenis\" id=\"jenis_".$v_jenis->id_jenis."\">
                                    <td>".($row_jenis+1)."</a></td>
                                    <td>".$v_jenis->nama_jenis."</a></td>
                                </tr>
                                <tr class=\"out_jenis\" id=\"out_jenis_".$v_jenis->id_jenis."\">
                                    <td>&nbsp;</td>
                                    <td>
                                         <table border='1' class=\"data-table\" style=\"width: 100%; border-collapse: collapse;\">".
                                         $str_header_mod; 
                
                $sub_jenis = $this->lpa->get_lp_sub_jenis(array("id_jenis"=>$v_jenis->id_jenis));

                if($sub_jenis){
                    $row_sub = 0;
                    foreach ($sub_jenis as $r_sub => $v_sub) {
                        $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][$row_sub]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                        $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][$row_sub]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis; 

                        $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_jenis->id_jenis."_".$v_sub->id_sub_jenis."\">
                                        <td align=\"center\"><a href=\"#\">".($row_sub+1)."</a></td>
                                        <td>".$v_sub->nama_sub_jenis."</a></td>";

                        $row_th = 0;
                        for ($i=$th_st; $i <= $th_fn; $i++) { 
                            $val_data = $this->lpa->get_lp(array("id_jenis"=>$v_jenis->id_jenis, "id_sub_jenis"=>$v_sub->id_sub_jenis, "th"=>$i));
                            $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][$row_sub]["detail"][$row_th] = $val_data;

                            // print_r("<pre>");
                            // print_r($val_data);

                            if(!empty($val_data) or is_numeric($val_data["jml"])){
                                $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"],0,".",",")."</td>";
                            }else{
                                $str_tbl .= "<td align=\"center\">-</td>";
                            }
                            
                            $row_th++;
                        }

                        // print_r("-----");
                        $str_tbl .= "</tr>";
                        $row_sub++;
                    }
                }else{

                    $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][0]["main"]["id_sub_jenis"] = $v_jenis->id_jenis;
                    $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][0]["main"]["nama_sub_jenis"] = $v_jenis->nama_jenis; 

                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_jenis->id_jenis."_0\">
                                        <td align=\"center\">1</a></td>
                                        <td>".$v_jenis->nama_jenis."</a></td>";

                    $row_th = 0;
                    for ($i=$th_st; $i <= $th_fn; $i++) { 
                        $val_data = $this->lpa->get_lp(array("id_jenis"=>$v_jenis->id_jenis, "id_sub_jenis"=>"0", "th"=>$i));

                        if(!empty($val_data)){
                            $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"],0,".",",")."</td>";
                            // print_r("ok");
                        }else{
                            $str_tbl .= "<td align=\"center\">-</td>";
                            // print_r("no");
                        }
                        // print_r("<pre>");
                        // print_r($val_data);

                        $row_th++;
                    }

                    // print_r("-----");
                    $str_tbl .= "</tr>";
                }
                
                $str_tbl .= "           
                                    </table>
                                </td>
                            </tr>";
                $row_jenis++;
            }
            $str_tbl .= "</table>
                        <br><br>";
            $row_kate++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;
        $data["kategori"] = "Dianas_pemrukiman";

        // $this->load->view('front_data_pdf/transportasi/model_2', $data);
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/model_2', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        print_r("<table border='1'>".$str_tbl."</table>");
        // print_r($data["dropdown_kategori"]);
        
    }

   
    

}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppertanian extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_lahan', 'll');
        $this->load->model('front_lp/Lp_panen', 'lp');

        $this->load->library("response_message");

	}

#===========================================================================================================================
#-----------------------------------------------------Lahan Kecamatan--------------------------------------------------
#===========================================================================================================================
    public function lahan_kec($th_fn){
        $jenis = $this->ll->get_lp_jenis();
        $kecamatan = $this->ll->get_kec();

        $th_st = $th_fn-2;
        
        //------------------------------------------------header-------------------------------------------------
        $str_title_data = "Data Luas Lahan (hektar) menurut Kecamatan dan Penggunaan Lahan ".($th_st)." - ".$th_fn;
        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"35%\">Keterangan</th>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\">".$i."</th>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Jenis Lahan</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 0;
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $t_data = array();

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td>".($row_jenis+1)."</a></td>
                            <td>".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"border-collapse: collapse; width: 100%;\">".
                                 $str_header_mod;

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;
                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                <td align=\"center\">".($r_kec+1)."</a></td>
                                <td>".$v_sub->nama_kec."</a></td>";
                
                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->ll->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data){
                        $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"],0 , ".", ",")."</td>";
                    }else{
                        $str_tbl .= "<td align=\"center\">-</td>";
                    }
                    $row_th++;
                }

                $str_tbl .= "</tr>";
                
                $r_kec++;
            }
            $row_jenis++;
            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Pertanian";
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        // $this->load->view("front_lp/pertanian/lp_pertanian_lahan",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Lahan Kecamatan--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Luas Panen Kecamatan--------------------------------------------------
#===========================================================================================================================
    public function panen_kec($th_fn){
        $jenis = $this->lp->get_lp_jenis();
        $kecamatan = $this->lp->get_kec();

        //------------------------------------------------header-------------------------------------------------
        $str_title_data = "Data Hasil Panen Berdasarkan Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"35%\">Keterangan</th>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\">".$i."</th>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Jenis Lahan</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td align=\"center\"><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"border-collapse: collapse; width: 100%;\">".
                                 $str_header_mod;            

            $t_data = array();

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                <td align=\"center\">".($r_kec+1)."</a></td>
                                <td>".$v_sub->nama_kec."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $array_val;

                    if($val_data["jml_panen"]){
                        $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_panen"], 0, ".", ",")."</td>";
                        $jml_panen = $val_data["jml_panen"];
                    }else{
                        $str_tbl .= "<td align=\"center\">-</td>";
                        $jml_panen = 0;
                    }
                    
                    $row_th++;

                }
                
                $r_kec++;
                $str_tbl .= "</tr>";
            }
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Pertanian";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        // $this->load->view("front_lp/pertanian/lp_pertanian_panen",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Luas Penen Kecamatan--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Hasil Panen Kecamatan--------------------------------------------------
#===========================================================================================================================
    public function hasil_panen_kec($th_fn){
        $jenis = $this->lp->get_lp_jenis();
        $kecamatan = $this->lp->get_kec();

        //------------------------------------------------header-------------------------------------------------
        $str_title_data = "Luas Penggunaan Lahan Sawah (hektar) menurut Kecamatan di Kota Malang Tahun ".($th_st)." - ".$th_fn;
        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"35%\">Keterangan</th>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\">".$i."</th>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Jenis Lahan</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-2;
        $row_jenis = 0;
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td align=\"center\">".($row_jenis+1)."</a></td>
                            <td>".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"border-collapse: collapse; width: 100%;\">".
                                 $str_header_mod;            

            $t_data = array();

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                <td align=\"center\">".($r_kec+1)."</a></td>
                                <td>".$v_sub->nama_kec."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $array_val;
                    
                    if($val_data["luas_panen"]){
                        $str_tbl .= "<td align=\"center\">".number_format($val_data["luas_panen"], 0, ".", ",")."</td>";
                        $luas_panen = $val_data["luas_panen"];
                    }else{
                        $str_tbl .= "<td align=\"center\">-</td>";
                        $luas_panen = 0;
                    }

                    $row_th++;
                }
                
                $r_kec++;
                $str_tbl .= "</tr>";
            }
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Pertanian";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        // $this->load->view("front_lp/pertanian/lp_pertanian_panen_lahan",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Hasil Penen Kecamatan-------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Produktivitas Panen Kecamatan-----------------------------------------
#===========================================================================================================================
    public function prod_panen_kec($th_fn){
        $jenis = $this->lp->get_lp_jenis();
        $kecamatan = $this->lp->get_kec();
        
        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------
        $str_title_data = "Luas Produktifitas Lahan Pertanian (hektar) menurut Kecamatan di Kota Malang Tahun ".($th_st)." - ".$th_fn;
        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"35%\">Keterangan</th>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\">".$i."</th>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Jenis Lahan</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td align=\"center\">".($row_jenis+1)."</a></td>
                            <td>".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"border-collapse: collapse; width: 100%;\">".
                                 $str_header_mod;            

            $t_data = array();

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                <td align=\"center\">".($r_kec+1)."</a></td>
                                <td>".$v_sub->nama_kec."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $array_val;

                    if($val_data["jml_panen"]){
                        // $str_tbl .= "<td>".$val_data["jml_panen"]."</td>";
                        $jml_panen = $val_data["jml_panen"];
                    }else{
                        // $str_tbl .= "<td>-</td>";
                        $jml_panen = 0;
                    }

                    
                    if($val_data["luas_panen"]){
                        // $str_tbl .= "<td>".$val_data["luas_panen"]."</td>";
                        $luas_panen = $val_data["luas_panen"];
                    }else{
                        // $str_tbl .= "<td>-</td>";
                        $luas_panen = 0;
                    }                 
                    
                    $prob_panen = 0;
                    if($jml_panen == 0 or $luas_panen == 0){
                        $str_tbl .= "<td>-</td>";
                    }else{
                        $prob_panen = (($jml_panen*10)/$luas_panen)*100;
                        $str_tbl .= "<td align=\"center\">".number_format($prob_panen, 2, ".", ",")."</td>";
                    }

                    $row_th++;

                }
                
                $r_kec++;
                $str_tbl .= "</tr>";
            }
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Pertanian";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_lp/pertanian/lp_pertanian_panen_prob",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Produktivitas Penen Kecamatan--------------------------------------------------
#===========================================================================================================================
}
?>
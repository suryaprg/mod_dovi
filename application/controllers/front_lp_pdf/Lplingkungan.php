<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lplingkungan extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_tpa', 'lp');
        
        $this->load->library("response_message");

	}
    
    public function index($th_fn){        
    }

    public function tpa($th_fn){
        $kec = $this->lp->get_lp_kec();

        $th_st = $th_fn-2;

        $str_title_data = "Data Tempat Pembuangan Akhir Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th>No</th>
                                <th>Kecamatan</th>";
        
        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";
            
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_kec = 0;
        $str_tbl = "";
        foreach ($kec as $r_data => $v_data) {
            $data["list_data"][$row_kec]["main"]["kecamatan"] = $v_data->nama_kec;
            $data["list_data"][$row_kec]["main"]["id_kec"] = $v_data->id_kec;

            $str_tbl .= " <tr>
                                <td align=\"center\">".($row_kec+1)."</td>
                                <td>".$v_data->nama_kec."</td>"; 
            
            $row_th = 0;
            for ($i=$th_fn-2; $i <= $th_fn ; $i++) {
                $val_data = $this->lp->get_lp(array("lpt.id_kec"=>$v_data->id_kec, "th"=>$i));                 
    
                $data["list_data"][$row_kec]["value"][$i] = $val_data["jml"];
                $str_tbl .= "<td align=\"right\">".number_format($val_data["jml"],0,".",",")."</td>";
                $row_th++;
            }

            $str_tbl .= "</tr>";
            $row_kec++;
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Lingkungan";

        // print_r("<pre>");
        // print_r("<table border=\"1\">".$data["str_header"].$data["str_tbl"]."</table>");

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");

        $this->load->view("front_lp/lingkungan/tpa",$data);

    }

    public function keuangan_get($id_jenis,$th_fn){
        
    }
    
}
?>
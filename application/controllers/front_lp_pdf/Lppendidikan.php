<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppendidikan extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_perpustakaan', 'lp');
        
        $this->load->library("response_message");

	}
    
    public function index($th_fn){
        
    }

    public function perpustakaan($th_fn){
        $jenis = $this->lp->get_lp_perpustakaan_jenis();

        $th_st = $th_fn-2;

        $str_title_data = "Data Perpustakaan Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th>No</th>
                                <th>Keterangan</th>";
        
        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";
            
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $no = 0;

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$r_data]["jenis"] = $v_data;
            
            $str_tbl .= " <tr>
                                <td align=\"center\">".($no+1)."</td>
                                <td>".$v_data->nama_jenis."</td>"; 

            $no_row = 0;
            for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                $data_main = $this->lp->get_lp_perpustakaan(array("lp.id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$r_data]["value"][$i] = $data_main["jml"];

                $str_tbl .= "<td align=\"center\">".number_format($data_main["jml"],0,".",",")."</td>";

                $no_row++;
            }

            $str_tbl .= "</tr>";

            $no++;
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Perpustakaan";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        $this->load->view("front_lp/pendidikan/lp_pendidikan",$data);

    }

    public function keuangan_get($id_jenis,$th_fn){
        
    }
    
}
?>
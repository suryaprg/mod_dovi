<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpdisnakerall extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_disnaker_all', 'lp');

        $this->load->library("response_message");

	}

#===========================================================================================================================
#-----------------------------------------------------Perdagangan--------------------------------------------------
#===========================================================================================================================
    public function disnaker_all($th_fn){
        $kategori_filter = $this->lp->get_lp_kategori();
        $kategori_all = $this->lp->get_lp_kategori();

        //------------------------------------------------header-------------------------------------------------

        $str_title = "Data Dinas Ketenaga Kerjaan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

        // $str_header_mod = "<tr>
        //                         <td width=\"5%\">No</td>
        //                         <td width=\"45%\">Keterangan</td>";

        $str_header_mod = "";

        $t_col = 2;
        for ($i=$th_fn-1; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"25%\">".$i."</th>";
            
            $t_col += 1;
        }

        $str_header_mod .= "";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title."</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-1;
        $str_tbl = "";

        $row_kategori = 0;

        $str_dropdown_kategori = "";

        $no = 1;

        foreach ($kategori_filter as $r_kate => $v_kate) {
            // $str_tbl = "";

            $data["list_data"][$v_kate->id_kategori]["kategori"]["main"]["id_kategori"] = $v_kate->id_kategori;
            $data["list_data"][$v_kate->id_kategori]["kategori"]["main"]["nama_kategori"] = $v_kate->nama_kategori;

            $str_tbl .= "<table border=\"1\" class=\"data-table\" style=\"width: 100%;border-collapse: collapse;\">
                         <thead>
                          <tr>
                            <th colspan=\"".$t_col."\">Data ".$v_kate->nama_kategori." di Kota Malang Tahun ".($th_st)." - ".$th_fn."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Kategori</th>
                                ".$str_header_mod."
                          </tr>
                         </thead>";

            // $data_graph[$v_kate->id_kategori]["main"]

            $jenis = $this->lp->get_lp_jenis(array("id_kategori"=>$v_kate->id_kategori));

            $row_jenis = 0;
            foreach ($jenis as $r_data => $v_data) {
                $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["main"]["id_jenis"] = $v_data->id_jenis;
                $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["main"]["nama_jenis"] = $v_data->nama_jenis;

                $str_tbl .=    "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                                    <td colspan=\"".$t_col."\">".($no).". ".$v_data->nama_jenis."</td>
                                </tr>";     


                $r_kec = 0;

                $array_jml = array();
                $sub = $this->lp->get_lp_sub_jenis(array("id_jenis"=>$v_data->id_jenis));

                if($sub){
                    foreach ($sub as $r_sub => $v_sub) {
                        $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["sub_jenis"][$r_kec]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                        $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["sub_jenis"][$r_kec]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;
                        
                        $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_sub_jenis."\">
                                        <td align=\"center\">".($r_kec+1)."</td>
                                        <td>".$v_sub->nama_sub_jenis."</td>";


                        $row_th = 0;
                        for ($i=$th_st; $i <= $th_fn ; $i++) {
                            $val_data = $this->lp->get_lp(array(
                                                            "id_jenis"=>$v_data->id_jenis,
                                                            "id_sub_jenis"=>$v_sub->id_sub_jenis,
                                                            "th"=>$i));
                            $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["sub_jenis"][$r_kec]["value"][$i][0] = $val_data["jml"];
                            $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"],3,".",",")."</td>";

                            $row_th++;

                        }
                        
                        $r_kec++;
                        $str_tbl .= "</tr>";
                    }
                }else{
                    $r_kec = 0;
                    $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["sub_jenis"][$r_kec]["main"]["id_sub_jenis"]   = $v_data->id_jenis;
                    $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["sub_jenis"][$r_kec]["main"]["nama_sub_jenis"] = $v_data->nama_jenis;
                        
                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_0\">
                                        <td align=\"center\">".($r_kec+1)."</td>
                                        <td>".$v_data->nama_jenis."</td>";

                     $row_th = 0;
                        for ($i=$th_st; $i <= $th_fn ; $i++) {
                            $val_data = $this->lp->get_lp(array(
                                                            "id_jenis"=>$v_data->id_jenis,
                                                            "id_sub_jenis"=>0,
                                                            "th"=>$i));
                            $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["sub_jenis"][$r_kec]["value"][$i][0] = $val_data["jml"];
                            $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"],3,".",",")."</td>";

                            $row_th++;

                        }
                        
                        $r_kec++;
                        $str_tbl .= "</tr>";
                }
                
                $row_jenis++;

                // $str_tbl .= "</tr>";
                $row_jenis++;
                $no++;
            }

            $str_tbl .= "</table>
                        <br><br>";

            // $data["str_tbl"][$v_kate->id_kategori] = (string)$str_tbl;

            $row_kategori++;   
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;
        $data["kategori"] = "Dinas_Tenaga_Kerja";

        // $this->load->view('front_data_pdf/transportasi/model_2', $data);
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/model_2', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        
        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".."</table>");
        // print_r($str_tbl);

        // $this->load->view("front_lp/Pendidikan/lp_pendidikan_all",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Perdagangan--------------------------------------------------
#===========================================================================================================================
}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lptransportasi extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_terminal', 'lt');

        $this->load->model('front_lp/Lp_kendaraan_kec', 'lkk');
        $this->load->model('front_lp/Lp_kendaraan_plat', 'lkp');
        $this->load->model('front_lp/Lp_kendaraan_umum', 'lku');

        $this->load->library("response_message");

	}


#===========================================================================================================================
#------------------------------------------------------------Terminal-------------------------------------------------------
#===========================================================================================================================
    
    public function terminal($th_fn){
        $jenis = $this->lt->get_lp_jenis();
        $th_st = $th_fn-2;
        $str_tbl = "";
        
        $str_title_data = "Jumlah Terminal, Uji KIR, Lama Pengujian KIR, Fasilitas Perlengkapan Jalan, dan Trayek di Kota Malang Tahun ".($th_st)." - ".$th_fn;
        $str_header_mod = "<tr>
                            <th width=\"10%\">No</th>
                            <th width=\"30%\">Keterangan</th>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\">".$i."</th>";
            $t_col++;
        }
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                                <th colspan=\"2\">".$str_title_data."</th>
                           </tr>
                           <tr>
                                <th width=\"7%\">No</th>
                                <th>Kategori</th>
                           </tr>";
                                        
        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td align=\"center\">".($row_jenis+1)."</a></td>
                            <td>".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"border-collapse: collapse; width: 100%;\">";

            $str_tbl .= $str_header_mod;
            
            $sub_jenis = $this->lt->get_lp_sub_jenis(array("ltsj.id_jenis"=>$v_data->id_jenis));

            if(count($sub_jenis) != 0){
                foreach ($sub_jenis as $r_sub => $v_sub) {
                    $data["list_data"][$v_data->id_jenis][$v_sub->id_sub_jenis]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;
                    $data["list_data"][$v_data->id_jenis][$v_sub->id_sub_jenis]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                    
                    $str_tbl .= "      
                                        <tr>
                                            <td align=\"center\">-</td>
                                            <td>".$v_sub->nama_sub_jenis."</td>";
                    $row_th = 0;
                    for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                        $val_data = $this->lt->get_lp(array("lt.id_jenis"=>$v_data->id_jenis,
                                                            "lt.id_sub_jenis"=>$v_sub->id_sub_jenis,
                                                            "th"=>$i));
                        $data["list_data"][$v_data->id_jenis][$v_sub->id_sub_jenis]["value"][$i] = $val_data["jml"];

                        $row_th++;
                        $str_tbl .= "       <td align=\"center\">".number_format($val_data["jml"], 2, ".", ",")."</td>";
                    
                    }
                }    
            }else{    
                    $data["list_data"][$v_data->id_jenis][0]["main"]["nama_sub_jenis"] = $v_data->nama_jenis;
                    $data["list_data"][$v_data->id_jenis][0]["main"]["id_sub_jenis"] = $v_data->id_jenis;
                    $data_graph[$v_data->id_jenis][0]["caption"] = $v_data->nama_jenis;

                    $str_tbl .= "      
                                        <tr>
                                            <td align=\"center\">-</td>
                                            <td>".$v_data->nama_jenis."</a></td>";
                    $row_th = 0;
                    for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                        $val_data = $this->lt->get_lp(array("lt.id_jenis"=>$v_data->id_jenis,
                                                            "lt.id_sub_jenis"=>0,
                                                            "th"=>$i));
                        $data["list_data"][$v_data->id_jenis][0]["value"][$i] = $val_data["jml"];

                        $row_th++;
                        $str_tbl .= "       <td align=\"center\">".number_format($val_data["jml"], 2, ".", ",")."</td>";
                    
                    }
                
                
            }
            $row_jenis++;
            $str_tbl .= "               </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $data["str_tbl"] = $str_tbl;
        $data["str_header"] = $str_header_top;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Transportasi";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        print_r("<table>".$data["str_header"].$str_tbl."</table>");

        // $this->load->view("front_lp/transportasi/Lp_terminal",$data);
    }
#===========================================================================================================================
#------------------------------------------------------------Terminal-------------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Kecamatan--------------------------------------------------
#===========================================================================================================================
    public function kendaraan_kec($th_fn){
        $jenis = $this->lkk->get_lp_jenis();
        $kecamatan = $this->lkk->get_kec();

        $th_st = $th_fn-2;
        //------------------------------------------------header-------------------------------------------------
        $str_title_data = "Jumlah Kendaraan Menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"35%\">Keterangan</th>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\">".$i."</th>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Jenis Kendaraan</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------
        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis_kendaraan;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->keterangan;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis_kendaraan."\">
                            <td align=\"center\">".($row_jenis+1)."</a></td>
                            <td>".$v_data->keterangan."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis_kendaraan."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border='1' class=\"data-table\" style=\"border-collapse: collapse; width: 100%;\">";
            $str_tbl .= $str_header_mod;

            $t_data = array();

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis_kendaraan."_".$v_sub->id_kec."\">
                                <td align=\"center\">".($r_kec+1)."</a></td>
                                <td>".$v_sub->nama_kec."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lkk->get_lp(array("id_jenis_kendaraan"=>$v_data->id_jenis_kendaraan, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data["jml"]){
                        $str_tbl .= "<td align=\"right\">".number_format($val_data["jml"], 0, ".", ",")."</td>";
                        // $jml_panen = $val_data["jml_panen"];
                    }else{
                        $str_tbl .= "<td align=\"right\">-</td>";
                        // $jml_panen = 0;
                    }

                    $row_th++;
                }
                
                $str_tbl .= "</tr>";
                $r_kec++;
            }

            $str_tbl .= "           
                                </table>
                            </td>
                        </tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Transportasi";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        print_r("<table>".$data["str_header"].$str_tbl."</table>");

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");

        // $this->load->view("front_lp/transportasi/Lp_kendaraan_kec",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Kecamatan--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Plat--------------------------------------------------
#===========================================================================================================================
    public function kendaraan_plat($th_fn){
        $jenis = $this->lkp->get_lp_jenis();

        $th_st = $th_fn-2;
        //------------------------------------------------header-------------------------------------------------
        $str_title_data ="Jumlah Kendaraan Berdasarkan Jenis Plat di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        $str_header_mod = "<tr>
                                <th width=\"5%\" rowspan=\"2\">No</th>
                                <th width=\"35%\" rowspan=\"2\">Keterangan</th>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\" colspan='3'>".$i."</th>";

            $str_header_bot .= "<th>Hitam</th><th>Kuning</th><th>Merah</th>";
            $t_col += 3;
        }
        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------       
        
        $row_jenis = 0;
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td align=\"center\">".($row_jenis+1)."</a></td>
                            <td>".$v_data->nama_jenis."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) { 
                $val_data = $this->lkp->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;
                    if($val_data){
                        $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_hitam"], 0, ".", ",")." unit</td>";
                        $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_kuning"], 0, ".", ",")." unit</td>";
                        $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_merah"], 0, ".", ",")." unit</td>";
                    }else{
                        $str_tbl .= "<td align=\"center\">-</td>";
                        $str_tbl .= "<td align=\"center\">-</td>";
                        $str_tbl .= "<td align=\"center\">-</td>";
                    }

                $row_th++;
            }

            $str_tbl .= "</tr>";            
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Transportasi";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
        // $data["str_series"] = $str_series;
        // $data["jml_th"] = $row_th;

        // print_r("<pre>");
        // print_r($data);
        print_r("<table border='1'>".$data["str_header"]. $data["str_tbl"]."</table>");

        // $this->load->view("front_lp/transportasi/lp_kendaraan_plat",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Plat--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Umum--------------------------------------------------
#===========================================================================================================================
    public function kendaraan_umum($th_fn){
        $jenis = $this->lku->get_lp_jenis();
        $kategori = $this->lku->get_lp_kategori();

        //------------------------------------------------header-------------------------------------------------
        $str_title_data = "Jumlah Kendaraan Berdasarkan Jenis di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"35%\">Keterangan</th>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\">".$i."</th>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Kategori</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $th_st = $th_fn-2;
        $row_jenis = 0;
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->id_jenis_kendaraan;
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->keterangan;

            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis_kendaraan;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->keterangan;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis_kendaraan."\">
                            <td align=\"center\">".($row_jenis+1)."</a></td>
                            <td>".$v_data->keterangan."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis_kendaraan."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border='1' class=\"data-table\" style=\"border-collapse: collapse; width: 100%;\">";
            $str_tbl .= $str_header_mod;

            $r_kec = 0;
            foreach ($kategori as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["id_kategori"] = $v_sub->id_jenis;
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["nama_kategori"] = $v_sub->nama_jenis;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis_kendaraan."_".$v_sub->id_jenis."\">
                                <td align=\"center\">".($r_kec+1)."</a></td>
                                <td>".$v_sub->nama_jenis."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lku->get_lp(array("id_jenis_kendaraan"=>$v_data->id_jenis_kendaraan, "id_jenis"=> $v_sub->id_jenis, "th"=>$i));
                    $data["list_data"][$row_jenis]["kategori"][$r_kec]["value"][$i] = $val_data["jml"];

                    if(is_numeric($val_data["jml"])){
                        $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"], 0, ".", ",")."</td>";
                    }else{
                        $str_tbl .= "<td align=\"center\">-</td>";
                    }

                    $row_th++;
                }
                
                $str_tbl .= "</tr>";
                $r_kec++;
            }

            $str_tbl .= "           
                                </table>
                            </td>
                        </tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Transportasi";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");

        // $this->load->view("front_lp/keuangan/tenaga",$data);
        // $this->load->view("front_lp/transportasi/Lp_kendaraan_umum",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Umum--------------------------------------------------
#===========================================================================================================================


}
?>
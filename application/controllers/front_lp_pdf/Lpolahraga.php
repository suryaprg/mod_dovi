<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpolahraga extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_olahraga', 'lp');
        
        $this->load->library("response_message");

	}
    
    public function index($th_fn){
        
    }

    public function olahraga($th_fn){
        $olahraga = $this->lp->get_lp_olahraga();
        $th_st = $th_fn-2;
        
        $str_title_data = "Data Kegiatan dan Sarana Olahraga di Kota Malang Tahun ".($th_st)." - ".$th_fn;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th width=\"5%\">No.</th>
                            <th width=\"45%\">Keterangan</th>";

        $t_col=2;
        for ($i=$th_st; $i<=$th_fn ; $i++) {
            $str_header_mod .= "<th width=\"15%\">".$i."</th>";
            $t_col++;
        }
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $row_kec = 0;
        foreach ($olahraga as $r_data => $v_data) {
            $data["list_data"][$row_kec]["main"]["olahraga"] = $v_data->nama_jenis;
            $data["list_data"][$row_kec]["main"]["id_jenis"] = $v_data->id_jenis;
            
            $str_tbl .= "<tr>
                            <td align=\"center\">".($row_kec+1)."</td>
                            <td>".$v_data->nama_jenis."</td>";

            $row_th = 0;
            for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                $val_data = $this->lp->get_lp(array("lpt.id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_kec]["value"][$i] = $val_data["jml"];
                $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"], 0, ".", ",")."</td>";
                $row_th++;
            }

            $str_tbl .= "</tr>";
            $row_kec++;
        }
        
        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Dinas_olaharaga";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");   

        print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>"); 

        // print_r("<pre>");
        // print_r($data);
        // $this->load->view("front_lp/olahraga/lp_olahraga",$data);

    }

    public function keuangan_get($id_jenis,$th_fn){
        
    }
    
}
?>
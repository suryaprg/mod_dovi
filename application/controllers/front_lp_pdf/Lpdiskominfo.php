<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpdiskominfo extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_diskominfo', 'lp');

	}
    
    public function get_aplikasi(){  
        $val_data = $this->lp->get_aplikasi();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Aplikasi yang Digunakan di Pemerintah Kota Malang";

        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"25%\">Perangkat Daerah</th>
                                <th width=\"40%\">Aplikasi</th>
                                <th width=\"40%\">Fungsi</th>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"4\">".$str_title."</th>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td align=\"center\">".($row_data+1)."</td>
                            <td>".$v_data->aplikasi."</td>
                            <td>".$v_data->fungsi."</td>
                            <td>".$v_data->keterangan."</td>
                        </tr>";
            $row_data++;
        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title;
        $data["kategori"] = "Dinas_kominfo";    
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        print_r("<pre>");
        // print_r($data["data_graph"]);
        print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");

    }

    public function get_domain(){  
        $val_data = $this->lp->get_domain();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Domain yang Digunakan di Pemerintah Kota Malang";

        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"40%\">Sub Domain</th>
                                <th width=\"40%\">Nama SKPD</th>
                                <th width=\"25%\">Tahun</th>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"4\">".$str_title."</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td align=\"center\">".($row_data+1)."</td>
                            <td>".$v_data->subdomain."</td>
                            <td>".$v_data->nama_skpd."</td>
                            <td align=\"center\">".$v_data->th."</td>
                        </tr>";
            $row_data++;

        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title;
        $data["kategori"] = "Dinas_kominfo";    
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
    }
    
}
?>
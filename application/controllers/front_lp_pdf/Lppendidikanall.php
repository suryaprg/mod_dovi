<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppendidikanall extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_pendidikan_all', 'lp');

        $this->load->library("response_message");
        //Load the library
        $this->load->library('html2pdf');

	}

#===========================================================================================================================
#-----------------------------------------------------Pendidikan All--------------------------------------------------
#===========================================================================================================================
    public function pendidikan_all($th_fn){
        $jenis = $this->lp->get_lp_jenis();
        $kategori = $this->lp->get_lp_kategori();
        $kecamatan = $this->lp->get_kec();

        $kategori_all = $this->lp->get_lp_kategori();

        //------------------------------------------------header-------------------------------------------------

        $str_header_mod = "<tr>
                                <th width=\"5%\" rowspan=\"2\">No</th>
                                <th width=\"35%\" rowspan=\"2\">Kecamatan</th>";
        $str_header_mod_sec ="<tr>";

        $t_col = 2;
        for ($i=$th_fn-1; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th  colspan=\"3\">".$i."</th>";

            $str_header_mod_sec .= "<th width=\"10%\">Negeri</th>
                                    <th width=\"10%\">Swasta</th>
                                    <th width=\"10%\">Jumlah Sekolah</th>";
            $t_col += 1;
        }

        $str_header_mod_sec .="</tr>";

        $str_header_mod .= "</tr>";

        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-1;
        $str_tbl = "";

        $row_kategori = 0;

        $str_dropdown_kategori = "";
        $str_tbl = "";

        foreach ($kategori as $r_kate => $v_kate) {
            $str_tbl .= "<table border=\"1\" class=\"data-table\" style=\"border-collapse: collapse; width: 100%; align-content: center;\">
                            <thead>
                              <tr>
                                <th colspan=\"2\">Data ".$v_kate->nama_kategori." di Kota Malang Tahun ".($th_st)." - ".$th_fn."</th>
                              </tr>
                              <tr>
                                    <th width=\"5%\">No. </th>
                                    <th>Kategori</th>
                              </tr>
                            </thead>";

            $row_jenis = 0;
            foreach ($jenis as $r_data => $v_data) {
                $str_tbl .=    "<tr>
                                    <td align=\"center\">".($row_jenis+1)."</td>
                                    <td>".$v_data->nama_jenis."</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <table border=\"1\" style=\"border-collapse: collapse; width: 100%;\">".
                                        $str_header_mod.$str_header_mod_sec;     

                $r_kec = 0;
                $array_jml = array();
                foreach ($kecamatan as $r_sub => $v_sub) {
                    
                    $str_tbl .= "<tr>
                                    <td align=\"center\">".($r_kec+1)."</td>
                                    <td>".$v_sub->nama_kec."</td>";


                    $row_th = 0;
                    for ($i=$th_st; $i <= $th_fn ; $i++) {
                        $val_data = $this->lp->get_lp(array("id_kategori"=>$v_kate->id_kategori,
                                                        "id_jenis"=>$v_data->id_jenis,
                                                        "id_kec"=>$v_sub->id_kec,
                                                        "th"=>$i));
                        $jml_negri = 0;
                        if($val_data["jml_negeri"]){
                            $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_negeri"],0,".",",")."</td>";
                            $jml_negri = $val_data["jml_negeri"];
                        }else{
                            $str_tbl .= "<td align=\"center\">-</td>";
                            $jml_negri = 0;
                        }

                        $jml_swasta = 0;
                        if($val_data["jml_swasta"]){
                            $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_swasta"],0,".",",")."</td>";
                            $jml_swasta = $val_data["jml_swasta"];
                        }else{
                            $str_tbl .= "<td align=\"center\">-</td>";
                            $jml_swasta = 0;
                        }

                        $str_tbl .= "<td align=\"center\">".number_format(($jml_swasta+$jml_negri),0,".",",")."</td>";

                        if(isset($array_jml[$i]["jml_swasta"])){
                            $array_jml[$i]["jml_swasta"] += $jml_swasta;
                        }else{
                            $array_jml[$i]["jml_swasta"] = $jml_swasta;
                        }

                        if(isset($array_jml[$i]["jml_negri"])){
                            $array_jml[$i]["jml_negri"] += $jml_negri;
                        }else{
                            $array_jml[$i]["jml_negri"] = $jml_negri;
                        }

                        if(isset($array_jml[$i]["jml_all"])){
                            $array_jml[$i]["jml_all"] += ($jml_swasta+$jml_negri);
                        }else{
                            $array_jml[$i]["jml_all"] = ($jml_swasta+$jml_negri);
                        }

                        $row_th++;

                    }
                    
                    $r_kec++;
                    $str_tbl .= "</tr>";
                }

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_00\">
                                    <td>-</td>
                                    <td>Jumlah</td>";

                $row_jml = 0;
                foreach ($array_jml as $r_jml => $v_jml) {
                    $str_tbl .= "<td align=\"center\">".number_format($v_jml["jml_negri"],0,".",",")."</td>
                                <td align=\"center\">".number_format($v_jml["jml_swasta"],0,".",",")."</td>
                                <td align=\"center\">".number_format($v_jml["jml_all"],0,".",",")."</td>";

                    $row_jml++;
                }

                $str_tbl .= "</tr>";
                
                $str_tbl .= "           </table>
                                    </td>
                                </tr>";

                $row_jenis++;
            }

            $str_tbl .= "</table>";
            $str_tbl .= "<br><br><br>";
            $row_kategori++;   
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_tbl"] = $str_tbl;

        $data["kategori"] = "Pendidikan"; 

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/model_2', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
        // print_r("<table border='1'>".$str_tbl."</table>");

        $this->load->view('front_data_pdf/transportasi/model_2', $data);
        // $this->load->view("front_lp/Pendidikan/lp_pendidikan_all",$data);
    }
    
    
    
#===========================================================================================================================
#-----------------------------------------------------Pendidikan All--------------------------------------------------
#===========================================================================================================================
}
?>
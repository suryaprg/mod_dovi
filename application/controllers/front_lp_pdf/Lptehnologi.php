<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lptehnologi extends CI_Controller{

    public function __construct(){
        parent::__construct();  
        $this->load->model('front_lp/Lp_tehnologi', 'lt');
        
        $this->load->library("response_message");

    }

#===========================================================================================================================
#------------------------------------------------------------Tehnologi------------------------------------------------------
#===========================================================================================================================
    public function tehnologi($th_fn){
        $jenis = $this->lt->get_lp_jenis();
        $sub_jenis = $this->lt->get_lp_sub();

        $th_st = $th_fn-2;
        $row_jenis = 0;
        $str_tbl = "";

        $str_title_data = "Data Tehnologi di Kota Malang Tahun ".($th_st)." - ".$th_fn;
        
        $str_header_mod = "     <tr>
                                <th width=\"5%\">No</th>
                                <th width=\"30%\">Keterangan</th>";
        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\">".$i."</th>";
            $t_col+=1;
        }

        $str_header_mod .= "    </tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td>-</td>
                            <td colspan=\"".($t_col-1)."\"><b>".$v_data->nama_jenis."</b></td>";
            $str_tbl .= "</tr>";

            $t_data = array();

            $r_kec = 0;
            foreach ($sub_jenis as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["sub_jenis"][$r_kec]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                $data["list_data"][$row_jenis]["sub_jenis"][$r_kec]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_sub->id_sub_jenis."\">
                                <td align=\"center\">".($r_kec+1)."</td>
                                <td>".$v_sub->nama_sub_jenis."</td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lt->get_lp(array("id_sub_jenis"=> $v_sub->id_sub_jenis, "th"=>$i));
                    $data["list_data"][$row_jenis]["sub_jenis"][$r_kec]["value"][$i] = $val_data["jml"];

                    $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"],0 ,".",",")."</td>";
                    $row_th++;
                }
                $str_tbl .= "</tr>";
                $r_kec++;
            }
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_tbl"] = $str_tbl;
        $data["str_header"] = $str_header_top.$str_header_mod;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Data_Pemanfaatan_Tehnologi";    
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");  
        
        print_r("<table>".$data["str_header"].$data["str_tbl"]."</table>");

        // $this->load->view("front_lp/keuangan/tenaga",$data);
        // $this->load->view("front_lp/tehnologi/tehnologi",$data);
    }
#===========================================================================================================================
#----------------------------------------------------------Tehnologi--------------------------------------------------------
#===========================================================================================================================

}
?>
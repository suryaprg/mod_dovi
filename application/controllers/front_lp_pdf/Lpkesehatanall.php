<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkesehatanall extends CI_Controller{

    public function __construct(){
        parent::__construct();  
        $this->load->model('front_lp/Lp_kesehatan_all', 'lp');

        $this->load->library("response_message");

    }

#===========================================================================================================================
#-----------------------------------------------------Pendidikan All--------------------------------------------------------
#===========================================================================================================================
    public function kesehatan_all($th_fn){
        // $kategori_filter = $this->lp->get_lp_kategori_filter(array("id_kategori"=> $id_kategori));
        $kategori_all = $this->lp->get_lp_kategori();        
        
        $th_st = $th_fn-2;
        $data_graph = array();
        $str_tbl = "";
        $data["list_data"] = array();

        //------------------------------------------------header-------------------------------------------------

        $str_title = "Data Kesehatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"25%\">Keterangan</th>";

        $t_col = 3;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";
            
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </td>
                                <th>Jenis Kategori</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 0;

        $str_dropdown_kategori = "";

        $row_kate = 0;
        foreach ($kategori_all as $r_kate => $v_kate) {
            $jenis = $this->lp->get_lp_jenis_filter(array("id_kategori"=>$v_kate->id_kategori));
            $data["list_data"][$v_kate->id_kategori]["kategori"]["main"]["id_jenis"] = $v_kate->id_kategori;
            $data["list_data"][$v_kate->id_kategori]["kategori"]["main"]["nama_kategori"] = $v_kate->nama_kategori;

            $str_tbl .= "<table border=\"1\" style=\"border-collapse: collapse; width: 100%;\">
                         <thead>
                          <tr>
                            <th colspan=\"2\">Data ".$v_kate->nama_kategori." di Kota Malang Tahun ".$th_st." - ".$th_fn."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Jenis Kategori</th>
                          </tr>
                         </thead>";

            $row_jenis = 0;
            foreach ($jenis as $r_jenis => $v_jenis) {
                $sub_jenis = $this->lp->get_lp_sub(array("id_jenis"=>$v_jenis->id_jenis));
                $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_jenis->id_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
                $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_jenis->id_jenis]["main"]["nama_kategori"] = $v_jenis->nama_jenis;

                $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_jenis->id_jenis."\">
                                <td align=\"center\">".($row_jenis+1)."</td>
                                <td>".$v_jenis->nama_jenis."</td>
                            </tr>
                            <tr class=\"out_jenis\" id=\"out_jenis_".$v_jenis->id_jenis."\">
                                <td>&nbsp;</td>
                                <td>
                                     <table border=\"1\" style=\"border-collapse: collapse; width: 100%;\">".
                                     $str_header_mod;


                $row_sub = 0;
                foreach ($sub_jenis as $r_sub => $v_sub) {
                    $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_jenis->id_jenis]["sub_jenis"][$v_sub->id_sub_jenis]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                    $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_jenis->id_jenis]["sub_jenis"][$v_sub->id_sub_jenis]["main"]["nama_kategori"] = $v_sub->nama_sub_jenis;

                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_jenis->id_jenis."_".$v_sub->id_sub_jenis."\">
                                    <td align=\"center\">".($row_sub+1)."</td>
                                    <td>".$v_sub->nama_sub_jenis."</td>";
                    
                    $row_th = 0;
                    for($i=$th_st; $i <= $th_fn ; $i++){
                        $val_data = $this->lp->get_lp(array("id_jenis"=>$v_jenis->id_jenis, 
                                                            "id_sub_jenis"=>$v_sub->id_sub_jenis,
                                                            "th"=>$i));
                        $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_jenis->id_jenis]["sub_jenis"][$v_sub->id_sub_jenis]["detail"][$i] = $val_data;

                        if($val_data){
                            $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"],0,".",",")."</td>";
                        }else{
                            $str_tbl .= "<td align=\"center\">0</td>";
                        }

                        $row_th++;
                    }

                    $str_tbl .= "</tr>";

                    $row_sub++;
                }

                $str_tbl .= "           
                                    </table>
                                </td>
                            </tr>";
                $row_jenis++;
            }
            $str_tbl .= "</table>";
            $str_tbl .= "<br><br>";
            // $data["str_tbl"][$v_kate->id_kategori] = (string)$str_tbl;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        $data["kategori"] = "Kesehatan"; 

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/model_2', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }
        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        // print_r();
        print_r("<table border='1'>".$data["str_tbl"]."</table>");

        // $this->load->view("front_lp/Pendidikan/lp_pendidikan_all",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Pendidikan All--------------------------------------------------------
#===========================================================================================================================
}
?>
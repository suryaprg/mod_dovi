<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkeuangan extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_keuangan', 'lk');
        $this->load->model('front_lp/Lp_koperasi', 'lkop');

        $this->load->model('front_lp/Lp_tenaga', 'lt');

        $this->load->model('front_lp/Lp_pend_ind', 'lpi');
        
        $this->load->library("response_message");

	}


#===========================================================================================================================
#------------------------------------------------------------Keuangan-------------------------------------------------------
#===========================================================================================================================
    
    public function index($th_fn){
        $th_st = $th_fn-2;

        $str_title_data = "Realisasi Pendapatan dan Realisasi Belanja Pemerintah Kota Malang Menurut Jenis Pendapatan (ribu rupiah) Kota Malang Tahun ".($th_st)." - ".$th_fn;
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th width=\"7%\">No</th>
                                <th width=\"33%\">Kecamatan</th>";

        $t_col = 2;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\">".$i."</th>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                          </tr>
                          <tr>
                            <th width=\"7%\">No</th>
                            <th>Kategori</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $keuangan_jenis = $this->lk->get_lp_keu_jenis();
        $data["keuangan"] = array();
        $str_tbl = "";
        $row_jenis = 0;
        foreach ($keuangan_jenis as $r_data => $v_data) {
            $data["keuangan"][$v_data->id_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["keuangan"][$v_data->id_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .=    "<tr>
                                <td align=\"center\">".($row_jenis+1)."</td>
                                <td>".$v_data->nama_jenis."</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <table border=\"1\" style=\"border-collapse: collapse; width: 100%;\">".
                                    $str_header_mod; 

            $data_sub_jenis = $this->lk->get_lp_keu_sub_jenis(array("lksj.id_jenis"=>$v_data->id_jenis));

            $row_sub = 0;
            foreach ($data_sub_jenis as $r_data_sub => $v_data_sub) {
                $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["nama_sub_jenis"] = $v_data_sub->nama_sub_jenis;

                $str_tbl .= "<tr>
                                    <td align=\"center\">".($row_sub+1)."</td>
                                    <td>".$v_data_sub->nama_sub_jenis."</td>";

                $row_graph = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) { 
                    $data_keu = $this->lk->get_lp_keu(array("lk.id_jenis"=>$v_data->id_jenis,
                                                                    "lk.id_sub_jenis"=>$v_data_sub->id_sub_jenis,
                                                                    "lk.th"=>$i)
                                                    );
                    $jml = $data_keu["jml"];
                    $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["value"][$i] = $jml;

                    $str_tbl .= "   <td align=\"right\">".number_format($jml,0,".",",")."</td>";

                    $row_graph++;
                }

                $str_tbl .= "</tr>";
                $row_sub++;
            }

            $str_tbl .= "          </table>
                                </td>
                            </tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Keuangan";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        print_r("<pre>");
        print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
        
        // $this->load->view("front_lp/Lp_keuangan",$data);
    }

#===========================================================================================================================
#------------------------------------------------------------Keuangan-------------------------------------------------------
#===========================================================================================================================


#===========================================================================================================================
#------------------------------------------------------------Koperasi-------------------------------------------------------
#===========================================================================================================================

    public function koperasi($th_fn){
        $jenis = $this->lkop->get_lp_jenis()->result();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"35%\">Keterangan</th>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\">".$i."</th>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\"> Data Koperasi di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Jenis Koperasi</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 0;
        $str_tbl = "";

        $data_graph = array();
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_data->id_jenis;
            
            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td>".($row_jenis+1)."</td>
                            <td>".$v_data->nama_jenis."</td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border='1' class=\"data-table\" width=\"100%\">";
            $str_tbl .= $str_header_mod;

            $sub_jenis = $this->lkop->get_lp_jenis_where(array("id_jenis"=>$v_data->id_jenis))->result();
            if(count($sub_jenis)){
                
                $row_sub = 0;
                foreach ($sub_jenis as $r_sub => $v_sub) {
                    $data["list_data"][$row_jenis]["main_sub"][$row_sub]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;
                    $data["list_data"][$row_jenis]["main_sub"][$row_sub]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;

                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_sub_jenis."\">
                                <td>".($row_sub+1)."</td>
                                <td>".$v_sub->nama_sub_jenis."</td>";
                    
                    $row_th = 0;
                    for ($i=$th_fn-2; $i <= $th_fn ; $i++) {
                        $val_data = $this->lkop->get_lp(array("id_sub_jenis"=>$v_sub->id_sub_jenis, "th"=>$i));
                        $data["list_data"][$row_jenis]["main_sub"][$row_sub]["value"][$i] = $val_data["jml"];

                        $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"], 2, ".", ",")."</td>";
                        $row_th++;
                    }
                    // $data["list_data"][$row_jenis]["value"];

                    $str_tbl .= "</tr>";

                    $row_sub++;
                }
            }else{
                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_0\">
                                <td>1</a></td>
                                <td>".$v_data->nama_jenis."</a></td>";

                $row_th = 0;
                for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                    $val_data = $this->lkop->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                    $data["list_data"][$row_jenis]["value"][$i] = $val_data["jml"];

                    $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"], 2, ".", ",")."</td>";

                    $row_th++;
                }

                $str_tbl .= "</tr>";
            }

            $str_tbl .= "           
                                </table>
                            </td>
                        </tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;
        
        $data["title"] = $str_title_data;
        $data["kategori"] = "Keuangan";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r($data_graph);

        // print_r("<table border='1'>".$str_tbl."</table>");
        $this->load->view("front_lp/keuangan/koperasi",$data);
    }
#===========================================================================================================================
#------------------------------------------------------------Koperasi-------------------------------------------------------
#===========================================================================================================================


#===========================================================================================================================
#------------------------------------------------------------Tenaga-------------------------------------------------------
#===========================================================================================================================

    public function tenaga($th_fn){
        $jenis = $this->lt->get_lp_jenis();
        $kategori = $this->lt->get_lp_kategori();
        $th_st = $th_fn-2;

        $str_title_data = "Jumlah Unit, Tenaga Kerja, dan Nilai Investasi Menurut Bidang Usaha di Kota Malang Tahun ".$th_st." - ".$th_fn;
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th rowspan=\"2\" width=\"5%\">No</th>
                                <th rowspan=\"2\" width=\"15%\">Keterangan</th>";

        $str_header_bot = "<tr>";
        
        $t_col = 2;

        foreach ($kategori as $r_kate => $v_kate) {
            $t_ty = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) { 
                $str_header_bot .= "<th>".$i."</th>";
                $t_col += 1;
                $t_ty++;
            }
            $str_header_mod .= "<th colspan=\"".$t_ty."\" width=\"25%\">".$v_kate->nama_kategori."</th>";
        }
        

        $str_header_mod .= "</tr>";
        $str_header_bot .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 0;
        $str_tbl = "";
        
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td align=\"center\">".($row_jenis+1)."</td>
                            <td>".$v_data->nama_jenis."</a></td>";

            $row_kategori = 0;
            foreach ($kategori as $r_kate => $v_kate) {
                $data["list_data"][$row_jenis]["kategori"][$row_kategori]["main"]["nama_kategori"] = $v_kate->nama_kategori;
                $data["list_data"][$row_jenis]["kategori"][$row_kategori]["main"]["id_kategori"] = $v_kate->id_kategori;

                $str_series = "";
                $row_th = 0;
                for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 

                    $val_data = $this->lt->get_lp_tenaga(array("id_jenis"=>$v_data->id_jenis, "id_kategori"=> $v_kate->id_kategori, "th"=>$i));
                    $data["list_data"][$row_jenis]["kategori"][$row_kategori]["value"][$i] = $val_data["jml"];

                    if($val_data){
                        // $str_tbl .= "<td>".$val_data["jml"]."</td>";
                        $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"],2,".",",")."</td>";
                    }else{
                        $str_tbl .= "<td align=\"center\">-</td>";
                    }


                    $row_th++;
                }

                $row_kategori++;
            }

            $row_jenis++;
            $str_tbl .= "</tr>";
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;
        
        $data["title"] = $str_title_data;
        $data["kategori"] = "Ekonomi";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        // $this->load->view("front_lp/pendidikan/lp_pendidikan",$data);

        print_r("<table border=\"1\">".$data["str_header"].$str_tbl."</table>");

        // $this->load->view("front_lp/keuangan/tenaga",$data);
    }
#===========================================================================================================================
#------------------------------------------------------------Tenaga-------------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#--------------------------------------------------------Pendapatan Industri------------------------------------------------
#===========================================================================================================================
    public function pendapatan_ind($th_fn){
        $jenis = $this->lpi->get_lp_jenis();
        $kategori = $this->lpi->get_lp_kategori();

        //------------------------------------------------header-------------------------------------------------
        $str_title_data = "Rekapan Pendataan Industri Menurut Jenis Industri di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"35%\">Keterangan</th>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th width=\"20%\">".$i."</th>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Jenis Industri</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td align=\"center\">".($row_jenis+1)."</td>
                            <td>".$v_data->nama_jenis."</td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table class=\"data-table\" style=\"border-collapse: collapse; width: 100%;\">";
            $str_tbl .= $str_header_mod;     

            $t_data = array();

            $r_kec = 0;
            foreach ($kategori as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["id_kategori"] = $v_sub->id_kategori;
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["nama_kategori"] = $v_sub->nama_kategori;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kategori."\">
                                <td align=\"center\">".($r_kec+1)."</td>
                                <td>".$v_sub->nama_kategori."</td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lpi->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kategori"=> $v_sub->id_kategori, "th"=>$i));
                    $data["list_data"][$row_jenis]["kategori"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data["jml"]){
                        $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"], 0, ".", ",")."</td>";
                        // $jml_panen = $val_data["jml_panen"];
                    }else{
                        $str_tbl .= "<td align=\"center\">-</td>";
                        // $jml_panen = 0;
                    }

                    $row_th++;
                }
                $str_tbl .= "</tr>";
                
                $r_kec++;
            }
            $str_tbl .= "           
                                </table>
                            </td>
                        </tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Ekonomi";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
        
        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_lp/keuangan/pend_industri",$data);
    }
#===========================================================================================================================
#--------------------------------------------------------Pendapatan Industri------------------------------------------------
#===========================================================================================================================

}
?>
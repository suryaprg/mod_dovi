<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lphukum extends CI_Controller{

    public function __construct(){
        parent::__construct();  
        $this->load->model('front_lp/Lp_pidana', 'lp');
        $this->load->model('front_lp/Lp_pidana_kec', 'lpk');
        
        $this->load->library("response_message");

    }

#===========================================================================================================================
#------------------------------------------------------------Pidana---------------------------------------------------------
#===========================================================================================================================

    public function pidana($th_fn){
        $jenis = $this->lp->get_lp_jenis();

        $th_st = $th_fn-2;
        $str_tbl = "";

        //------------------------------------------------header-------------------------------------------------
        $str_title_data = "Data Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        $str_header_mod = "<tr>
                                <th rowspan=\"2\">No</th>
                                <th rowspan=\"2\">Keterangan</th>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th colspan=\"2\">".$i."</th>";
            $str_header_bot .= "<th>Lapor</th><th>Selesai</th>";
            $t_col += 2;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";


        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td align=\"center\">".($row_jenis+1)."</a></td>
                            <td>".$v_data->nama_jenis."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["lapor"]){
                    $str_tbl .= "<td align=\"center\">".number_format($val_data["lapor"],0,".",",")."</td>";
                }else{
                    $str_tbl .= "<td align=\"center\">-</td>";
                }

                if($val_data["selesai"]){
                    $str_tbl .= "<td align=\"center\">".number_format($val_data["selesai"],0,".",",")."</td>";
                }else{
                    $str_tbl .= "<td align=\"center\">-</td>";
                }

                $row_th++;
            }

            $str_tbl .= "</tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Hukum"; 

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$str_header_top.$str_header_mod.$str_header_bot.$str_tbl."</table>");

        $this->load->view("front_lp/hukum/pidana", $data);
    }

#===========================================================================================================================
#------------------------------------------------------------Pidana---------------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#------------------------------------------------------------Pidana_Kec---------------------------------------------------------
#===========================================================================================================================

    public function pidana_kec($th_fn){
        $kecamatan = $this->lpk->get_lp_jenis();

        $th_st = $th_fn-2;

        $str_tbl = "";
        //------------------------------------------------header-------------------------------------------------
        $str_title_data = "Jumlah Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        $str_header_mod = "<tr>
                                <th rowspan=\"2\">No</th>
                                <th rowspan=\"2\">Kecamatan</th>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th colspan=\"2\">".$i."</th>";
            $str_header_bot .= "<th>Lapor</th><th>Selesai</th>";
            $t_col += 2;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";


        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 0;
        foreach ($kecamatan as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_kec"] = $v_data->id_kec;
            $data["list_data"][$row_jenis]["jenis"]["nama_kec"] = $v_data->nama_kec;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_kec."\">
                            <td align=\"center\">".($row_jenis+1)."</a></td>
                            <td>".$v_data->nama_kec."</a></td>";

            $row_th = 0;

            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpk->get_lp(array("id_kec"=>$v_data->id_kec, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_lapor"]){
                    $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_lapor"],0,".",",")."</td>";
                }else{
                    $str_tbl .= "<td align=\"center\">-</td>";
                }

                if($val_data["jml_lapor"]){
                    $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_selesai"],0,".",",")."</td>";
                }else{
                    $str_tbl .= "<td align=\"center\">-</td>";
                }

                $row_th++;
            }

            $row_jenis++;

            $str_tbl .= "</tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Hukum"; 

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_lp/hukum/pidana_kecamatan", $data);
    }

#===========================================================================================================================
#------------------------------------------------------------Pidana_Kec---------------------------------------------------------
#===========================================================================================================================
}
?>
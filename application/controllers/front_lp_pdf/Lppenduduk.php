<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppenduduk extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_penduduk_jk', 'lpj');
        $this->load->model('front_lp/Lp_penduduk_umur', 'lpu');
        
        $this->load->library("response_message");

	}

#===========================================================================================================================
#------------------------------------------------------------Penduduk JK-------------------------------------------------------
#===========================================================================================================================
    public function penduduk_jk($th_fn){
        $kecamatan = $this->lpj->get_kec();
        $th_st = $th_fn-2;

        $str_title_data ="Jumlah Penduduk Jenis Kelamin menurut Kecamatan Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th rowspan=\"2\">No</th>
                                <th rowspan=\"2\">Kecamatan</th>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th colspan=\"2\">".$i."</th>";
            $str_header_bot .= "<th>Laki-laki</th>
                                <th>Perempuan</th>";
            $t_col += 2;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";


        $row_jenis = 0;
        foreach ($kecamatan as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_kec;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_kec;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_kec."\">
                            <td align=\"center\">".($row_jenis+1)."</td>
                            <td>".$v_data->nama_kec."</td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpj->get_lp(array("id_kec"=>$v_data->id_kec, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_l"]){
                    $jml_l = $val_data["jml_l"];
                    $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_l"],0, ".", ",")." jiwa</td>";
                }else{
                    $str_tbl .= "<td align=\"center\">-</td>";
                    $jml_l = 0;
                }

                if($val_data["jml_p"]){
                    $jml_p = $val_data["jml_p"];
                    $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_p"],0, ".", ",")." jiwa</td>";
                }else{
                    $jml_p = 0;
                    $str_tbl .= "<td align=\"center\">-</td>";
                }

                $row_th++;
            }

            $str_tbl .= "</tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Data_Kependudukan";    
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");  


        // print_r("<pre>");
        // print_r($data);

        // $this->load->view("front_lp/penduduk/lp_penduduk_jk",$data);
        // $this->load->view("front_lp/pmks/pmks", $data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }

#===========================================================================================================================
#------------------------------------------------------------Penduduk JK----------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------------Penduduk Rasio--------------------------------------------------
#===========================================================================================================================

    public function penduduk_rasio($th_fn){
        $kecamatan = $this->lpj->get_kec();
        $th_st = $th_fn-2;

        $str_title_data = "Jumlah Jenis Kelamin menurut Kecamatan Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th rowspan=\"2\">No</th>
                                <th rowspan=\"2\">Kecamatan</th>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th colspan=\"2\">".$i."</th>";
            $str_header_bot .= "<th>Jumlah Penduduk</th>
                                <th>Rasio</th>";
            $t_col += 2;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";
        $row_jenis = 0;
        foreach ($kecamatan as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_kec;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_kec;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_kec."\">
                            <td align=\"center\">".($row_jenis+1)."</td>
                            <td>".$v_data->nama_kec."</td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpj->get_lp(array("id_kec"=>$v_data->id_kec, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_l"]){
                    $jml_l = $val_data["jml_l"];
                }else{
                    $jml_l = 0;
                }

                if($val_data["jml_p"]){
                    $jml_p = $val_data["jml_p"];
                }else{
                    $jml_p = 0;
                }

                $str_tbl .= "<td align=\"center\">".number_format(($jml_l+$jml_p), 0, ".",",")." jiwa</td>";
                $str_tbl .= "<td align=\"center\">".number_format($val_data["rasio"], 2, ".",",")." %</td>";

                $row_th++;
            }

            $str_tbl .= "</tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;
        
        $data["title"] = $str_title_data;
        $data["kategori"] = "Data_Kependudukan";    
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");  

        $this->load->view("front_lp/penduduk/lp_penduduk_rasio", $data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }

#===========================================================================================================================
#-----------------------------------------------------------Penduduk Rasio--------------------------------------------------
#===========================================================================================================================


#===========================================================================================================================
#-----------------------------------------------------------Penduduk Umur--------------------------------------------------
#===========================================================================================================================

    public function penduduk_umur($th_fn){
        $jenis = $this->lpu->get_lp_jenis();

        $th_st = $th_fn-2;

        $str_title_data = "Jumlah Penduduk menurut Kelompok Umur dan Jenis Kelamin Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        //------------------------------------------------header-------------------------------------------------
          $str_header_mod = "<tr>
                                <td rowspan=\"2\">No</td>
                                <td rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"3\">".$i."</td>";
            $str_header_bot .= "<th>Laki-laki</th>
                                <th>Perempuan</th>
                                <th>Rasio</th>";
            $t_col += 3;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";

        $data_graph = array();
        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td align=\"center\">".($row_jenis+1)."</td>
                            <td align=\"center\">".$v_data->nama_jenis."</td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpu->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_l"]){
                    $jml_l = $val_data["jml_l"];
                    $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_l"], 0, ".",",")." jiwa</td>";
                }else{
                    $str_tbl .= "<td align=\"center\">-</td>";
                    $jml_l = 0;
                }

                if($val_data["jml_p"]){
                    $jml_p = $val_data["jml_p"];
                    $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_p"], 0, ".",",")." jiwa</td>";
                }else{
                    $jml_p = 0;
                    $str_tbl .= "<td align=\"center\">-</td>";
                }

                $str_tbl .= "<td align=\"center\">".number_format(($jml_l+$jml_p), 0, ".",",")." jiwa</td>";

                $row_th++;
            }

            $str_tbl .= "</tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Data_Kependudukan";    
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");  

        // $this->load->view("front_lp/pmks/pmks", $data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
        $this->load->view("front_lp/penduduk/lp_penduduk_umur",$data);
    }

#===========================================================================================================================
#-----------------------------------------------------------Penduduk Umur--------------------------------------------------
#===========================================================================================================================
}
?>
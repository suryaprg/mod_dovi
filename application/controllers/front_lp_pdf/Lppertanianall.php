<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppertanianall extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_pertanian_all', 'lp');

        // $this->load->library("response_message");

	}

#===========================================================================================================================
#-----------------------------------------------------Pertanian All--------------------------------------------------
#===========================================================================================================================
    public function pertanian_all($th_fn){
        $jenis_all = $this->lp->get_lp_jenis();
        $jenis =  $this->lp->get_lp_jenis();
       
        $th_st = $th_fn-1;
        
        //------------------------------------------------header-------------------------------------------------

        $str_title_data = "Data Pertanian Keseluruhan di Kota Malang Tahun ".($th_st)." - ".$th_fn;

        $str_header_mod = "<tr>
                                <th width=\"5%\" rowspan=\"2\">No</th>
                                <th width=\"25%\" rowspan=\"2\">Keterangan</th>
                                <th width=\"10%\" rowspan=\"2\">Satuan</th>";
        $str_header_mod_sec ="<tr>";

        $t_col = 3;
        for ($i=$th_fn-1; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th  colspan=\"3\">".$i."</th>";
            $str_header_mod_sec .= "<th width=\"10%\">Jumlah Panen</th>
                                    <th width=\"10%\">Luas Lahan</th>
                                    <th width=\"10%\">Produktivitas</th>";
            $t_col += 3;
        }

        $str_header_mod_sec .="</tr>";

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Jenis Lahan</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------
       
        $row_jenis = 0;
        $str_tbl = "";

        foreach ($jenis as $r_jenis => $v_jenis) {
            $data["list_data"][$v_jenis->id_jenis]["jenis"]["main"]["id_jenis"] = $v_jenis->id_jenis;
            $data["list_data"][$v_jenis->id_jenis]["jenis"]["main"]["nama_kategori"] = $v_jenis->nama_jenis;

            $str_tbl .= "<table border=\"1\" style=\"border-collapse: collapse; width: 100%;\">
                         <thead>
                          <tr>
                            <th colspan=\"".$t_col."\">Data ". $v_jenis->nama_jenis." di Kota Malang Tahun ".($th_fn-1)." - ".$th_fn."</th>
                          </tr>
                          ".$str_header_mod.$str_header_mod_sec."
                         </thead>";

            $sub_jenis = $this->lp->get_lp_sub(array("id_jenis"=>$v_jenis->id_jenis));

            $row_sub = 0;
            foreach ($sub_jenis as $r_sub => $v_sub) {
                    $data["list_data"][$v_jenis->id_jenis]["jenis"]["sub_jenis"][$row_sub]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                    $data["list_data"][$v_jenis->id_jenis]["jenis"]["sub_jenis"][$row_sub]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;
                    
                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_jenis->id_jenis."_".$v_sub->id_sub_jenis."\">
                                    <td align=\"center\">".($row_sub+1)."</a></td>
                                    <td>".$v_sub->nama_sub_jenis."</a></td>
                                    <td>".$v_sub->satuan."</td>";
                    $row_th = 0;
                    for ($i=$th_st; $i <= $th_fn ; $i++) {
                        $val_data = $this->lp->get_lp(array("id_sub_jenis"=>$v_sub->id_sub_jenis,
                                                        "th"=>$i));

                        $luas = 0;
                        if($val_data["luas"]){
                            $luas = $val_data["luas"];
                            $str_tbl .= "<td align=\"center\">".number_format($luas, 0, ".", ",")."</td>";
                        }else{
                            $luas = 0;
                            $str_tbl .= "<td align=\"center\">-</td>";
                        }

                        $produksi = 0;
                        if($val_data["jml"]){
                            $produksi = $val_data["jml"];
                            $str_tbl .= "<td align=\"center\">".number_format($produksi, 0, ".", ",")."</td>";
                        }else{
                            $produksi = 0;
                            $str_tbl .= "<td align=\"center\">-</td>";
                        }

                        if($produksi != 0){
                            $produktivitas = $luas/$produksi;
                            $str_tbl .= "<td align=\"center\">".number_format($produktivitas, 3, ".", ",")."</td>";
                        }else{
                            $str_tbl .= "<td align=\"center\">0</td>";
                        }

                        $row_th++;
                    }

                    $str_tbl .= "</tr>";
                $row_sub++;
            }            

            $str_tbl .= "</table><br><br><br>";
            $row_jenis++;   
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Pertanian";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/model_2', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");

        print_r("<table border='1'>".$str_tbl."</table>");

        // $this->load->view("front_lp/pertanian/Lp_pertanian_all",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Pendidikan All--------------------------------------------------
#===========================================================================================================================
}
?>
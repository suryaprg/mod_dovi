<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Bpkad extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		
        $this->load->library("response_message");

	}

	public function get_json_file($kategori,$th_fn){
		$dir = "./assets/json_bpkad/";
   		$filename = $th_fn.".json";
   		$th_title = substr($th_fn, 0, 4);

   		// print_r("<pre>");
   		$data_array = array();
   		$data_table_array = array("golongan"=>"",
   								"jenis_kelamin"=>"",
   								"agama"=>"",
   								"status"=>""
   								);
   		$str_table_fix = "";
   		$data_array_fix = "";

   		if(isset($data_table_array[$kategori])){
   			// echo "matul";
   			if(is_dir($dir)){
	   			if(file_exists($dir.$filename)){
	   				$get_data_content = file_get_contents($dir.$filename);
	   				if($get_data_content){
	   					$data_json = json_decode($get_data_content);

	   					$no = 1;
	   					foreach ($data_json as $r_data_json => $v_data_json) {
	   						$data_array["golongan"][$r_data_json]["unit_kerja"] = $v_data_json->unit_kerja;
	   						$data_array["golongan"][$r_data_json]["jumlah_golongan_I"] = $v_data_json->jumlah_golongan_I;
	   						$data_array["golongan"][$r_data_json]["jumlah_golongan_II"] = $v_data_json->jumlah_golongan_II;
	   						$data_array["golongan"][$r_data_json]["jumlah_golongan_III"] = $v_data_json->jumlah_golongan_III;
	   						$data_array["golongan"][$r_data_json]["jumlah_golongan_IV"] = $v_data_json->jumlah_golongan_IV;

	   						$data_table_array["golongan"] = $data_table_array["golongan"]."<tr>
	   															<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->jumlah_golongan_I."</td>
																<td align=\"right\">".$v_data_json->jumlah_golongan_II."</td>
																<td align=\"right\">".$v_data_json->jumlah_golongan_III."</td>
																<td align=\"right\">".$v_data_json->jumlah_golongan_IV."</td>
															</tr>";

	   						$data_array["jenis_kelamin"][$r_data_json]["unit_kerja"] = $v_data_json->unit_kerja;
	   						$data_array["jenis_kelamin"][$r_data_json]["laki"] = $v_data_json->laki;
	   						$data_array["jenis_kelamin"][$r_data_json]["perempuan"] = $v_data_json->perempuan;

	   						$data_table_array["jenis_kelamin"] = $data_table_array["jenis_kelamin"]."<tr>
																<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->laki."</td>
																<td align=\"right\">".$v_data_json->perempuan."</td>
															</tr>";

	   						$data_array["agama"][$r_data_json]["unit_kerja"] = $v_data_json->unit_kerja;
	   						$data_array["agama"][$r_data_json]["agama_islam"] = $v_data_json->agama_islam;
	   						$data_array["agama"][$r_data_json]["agama_kristen"] = $v_data_json->agama_kristen;
	   						$data_array["agama"][$r_data_json]["agama_katholik"] = $v_data_json->agama_katholik;
	   						$data_array["agama"][$r_data_json]["agama_hindu"] = $v_data_json->agama_hindu;
	   						$data_array["agama"][$r_data_json]["agama_budha"] = $v_data_json->agama_budha;

	   						$data_table_array["agama"] = $data_table_array["agama"]."<tr>
	   															<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->agama_islam."</td>
																<td align=\"right\">".$v_data_json->agama_kristen."</td>
																<td align=\"right\">".$v_data_json->agama_katholik."</td>
																<td align=\"right\">".$v_data_json->agama_hindu."</td>
																<td align=\"right\">".$v_data_json->agama_budha."</td>
															</tr>";

	   						$data_array["status"][$r_data_json]["unit_kerja"] = $v_data_json->unit_kerja;
	   						$data_array["status"][$r_data_json]["status_kawin"] = $v_data_json->status_kawin;
	   						$data_array["status"][$r_data_json]["status_belum_kawin"] = $v_data_json->status_belum_kawin;
	   						
	   						$data_table_array["status"] = $data_table_array["status"]."<tr>
	   															<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->status_kawin."</td>
																<td align=\"right\">".$v_data_json->status_belum_kawin."</td>
															</tr>";

							$no++;
	   					}
	   				}
	   			}
	   		}
	   		//------------------------------------------------header-------------------------------------------------
	        $str_title = "Data Jumlah Pegawai Negeri Sipil di Kota Malang Tahun ".$th_title.", berdasarkan ".$kategori;

	        $str_header_mod = "<tr>
	                                <th width=\"5%\">No</th>";

	        $t_wi = 95/count($data_array[$kategori][0]);
	        $t_col = 1;
	        foreach ($data_array[$kategori][0] as $r_data => $v_data) {
	        	$str_header_mod .="<th width=\"".$t_wi."%\">".$r_data."</th>";
	        	$t_col += 1;
	        }
	        $str_header_mod .= "</tr>";

	        $str_header_top = "<tr>
	                            <th colspan=\"".$t_col."\">".$str_title."</th>
	                          </tr>";
	        //------------------------------------------------header-------------------------------------------------

	        $str_table_fix = "<table border=\"1\" style=\"border-collapse: collapse; width: 100%;\">
   								<thead>".$str_header_top.$str_header_mod."</thead>
								<tbody>".$data_table_array[$kategori]."</tbody>
   							</table>";
   		}
   		
   		$data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

   		$data["str_tbl"] = $str_table_fix;
   		$data["kategori"] = "Kepegawaian";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/model_2', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");
   		print_r($data["str_tbl"]);


   		// print_r($data["data_graph"]);
	}

	public function con(){
		$connected = @fsockopen("www.google.com", 80); 
		if ($connected){
			fclose($connected);
			return true;
			// print_r("connected");
		}
		return false;
	}

}
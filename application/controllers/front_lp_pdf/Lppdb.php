<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppdb extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_pdb', 'lp');

        $this->load->library("response_message");

	}

#===========================================================================================================================
#-----------------------------------------------------Produk Domestik Bruto--------------------------------------------------
#===========================================================================================================================
    public function pdb($th_fn){
        $jenis = $this->lp->get_lp_jenis();
        $kategori = $this->lp->get_lp_kategori();

        $str_title ="Jumlah Pendapatan Domestik Bruto di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th>No</th>
                                <th>Jenis Pendapatan</th>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title."</th>
                          </tr>
                          <tr>
                                <th width=\"5%\">No. </th>
                                <th>Kategori</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td align=\"center\">".($row_jenis+1)."</td>
                            <td>".$v_data->nama_jenis."</td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"width: 100%;border-collapse: collapse;\">".
                                 $str_header_mod;            

            $t_data = array();

            $r_kec = 0;
            foreach ($kategori as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["id_kategori"] = $v_sub->id_kategori;
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["nama_kategori"] = $v_sub->nama_kategori;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kategori."\">
                                <td align=\"center\">".($r_kec+1)."</td>
                                <td>".$v_sub->nama_kategori."</td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kategori"=> $v_sub->id_kategori, "th"=>$i));
                    // $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                    $data["list_data"][$row_jenis]["kategori"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data["jml"]){
                        $str_tbl .= "<td align=\"center\">".number_format($val_data["jml"],0, ".", ",")."</td>";
                        $jml = $val_data["jml"];
                    }else{
                        $str_tbl .= "<td align=\"center\">-</td>";
                        $jml = 0;
                    }
                    
                    $row_th++;

                }
                
                $r_kec++;
                $str_tbl .= "</tr>";
            }
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title;
        $data["kategori"] = "Pendapatan_Domestik_Bruto";    
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        // $this->load->view("front_lp/pdb/lp_pdb",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Produk Domestik Bruto--------------------------------------------------
#===========================================================================================================================
}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpbencana extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_bencana', 'lp');
        
        $this->load->library("response_message");

	}
    
    public function index($th_fn){
        
    }

    public function bencana($th_fn){
        $kecamatan = $this->lp->get_lp_kecamatan();

        $th_st = $th_fn-2;

        $str_title = " Jumlah Bencana menurut Jenis Bencana dan Kecamatan Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th>No</th>
                                <th>Kecamatan</th>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";
            $str_header_bot .= "<th>Laki-laki</th>
                                <th>Perempuan</th>";
            $t_col += 1;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";


        $row_jenis = 0;
        foreach ($kecamatan as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_kec;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_kec;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_kec."\">
                            <td align=\"center\"><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_kec."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lp->get_lp(array("lpt.id_kec"=>$v_data->id_kec, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml"]){
                    $jml = $val_data["jml"];
                    $str_tbl .= "<td align=\"right\">".number_format($val_data["jml"],0,".",",")."</td>";
                }else{
                    $str_tbl .= "<td align=\"right\">-</td>";
                    $jml = 0;
                }

                $row_th++;
                // $str_tbl .= "<td>".($jml_l+$jml_p)."</td>";
            }

            $str_tbl .= "</tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title;
        $data["kategori"] = "Bencana";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

        $this->load->view("front_lp/bencana/lp_bencana",$data);
        // $this->load->view("front_lp/pmks/pmks", $data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

    }

    public function keuangan_get($id_jenis,$th_fn){
        
    }
    
}
?>
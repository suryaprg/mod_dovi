<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppmks extends CI_Controller{

    public function __construct(){
        parent::__construct();  
        $this->load->model('front_lp/Lp_pmks', 'lp');
        
        $this->load->library("response_message");

    }

#===========================================================================================================================
#------------------------------------------------------------PMKS-------------------------------------------------------
#===========================================================================================================================

    public function pmks($th_fn){
        $jenis = $this->lp->get_lp_jenis();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th rowspan=\"2\">No</th>
                                <th rowspan=\"2\">Keterangan</th>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th colspan=\"3\">".$i."</th>";
            $str_header_bot .= "<th>Laki-laki</th>
                                <th>Perempuan</th>
                                <th>Jumlah</th>";
            $t_col += 3;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">Data Penyandang Masalah Kesejahteraan Sosial Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</th>
                        </tr>";


        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";
        // $series = "";

        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td>".($row_jenis+1)."</a></td>
                            <td>".$v_data->nama_jenis."</a></td>";

            // $series = "";
            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {

                $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_l"]){
                    $jml_l = $val_data["jml_l"];
                    $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_l"],0,".",",")."</td>";
                }else{
                    $str_tbl .= "<td align=\"right\">-</td>";
                    $jml_l = 0;
                }

                if($val_data["jml_p"]){
                    $jml_p = $val_data["jml_p"];
                    $str_tbl .= "<td align=\"center\">".number_format($val_data["jml_p"],0,".",",")."</td>";
                }else{
                    $jml_p = 0;
                    $str_tbl .= "<td align=\"right\">-</td>";
                }

                $str_tbl .= "<td align=\"center\">".($jml_l+$jml_p)."</td>";

                $row_th++;
            }

            $str_tbl .= "</tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Data_Tindakan_Pidana";    
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");  

        // $this->load->view("front_lp/pmks/pmks", $data);
    }

#===========================================================================================================================
#------------------------------------------------------------PMKS-------------------------------------------------------
#===========================================================================================================================
}
?>
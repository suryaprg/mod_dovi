<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpdpmptsp extends CI_Controller{

    public function __construct(){
        parent::__construct();  
        $this->load->model('front_lp/Lp_dpmptsp', 'lpa');
    }

    public function lp_dpmptsp_investasi($th_fn){
        $jenis = $this->lpa->get_lp_jenis();
        $array_of_month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        $array_of_num = array("01","02","03","04","05","06","07","08","09","10","11","12");
        $str_tbl = "";
        $data_graph = array();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Perijinan di Pemerintah Kota Malang Tahun ". $th_fn;

        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"20%\">Jenis</th>";
        $str_header_mod_sec = "<tr>";
        $t_col = 2;
        foreach ($array_of_month as $key => $value) {
            $str_header_mod .= "<th width=\"5%\">".$value."</th>";
            $str_header_mod_sec .= "<th>Terbit</th>
                                    <th>Investasi</th>
                                    <th>TK</th>";
            $t_col+=1;
        }
        $str_header_mod .= "</tr>";
        $str_header_mod_sec .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title."</th>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 1;
        foreach ($jenis as $r_jenis => $v_jenis) {
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;  

            $str_tbl .= " <tr>
                            <td align=\"center\">".$row_jenis."</td>
                            <td>".$v_jenis->nama_jenis."</td>"; 

            $row_th= 0;
            $array_count = array();
            foreach ($array_of_num as $r_num => $v_num) {
                $val_data = $this->lpa->get_lp(array("lpt.id_jenis"=>$v_jenis->id_jenis, "th"=>$th_fn.$v_num));
                if(is_numeric($val_data["investasi"])){
                    $str_tbl .= "<td align=\"right\">".number_format($val_data["investasi"],0,".",",")."</td>";
                    // $data["list_data"][$row_jenis]["detail"][$row_th]["tk"] = $val_data["tk"];
                    $data["list_data"][$row_jenis]["detail"][$row_th]["investasi"] = $val_data["investasi"];
                    // $data["list_data"][$row_jenis]["detail"][$row_th]["terbit"] = $val_data["terbit"];
                }else{
                    $str_tbl .= "<td align=\"right\">0</td>";

                    // $data["list_data"][$row_jenis]["detail"][$row_th]["tk"] = "-";
                    $data["list_data"][$row_jenis]["detail"][$row_th]["investasi"] = "-";
                    // $data["list_data"][$row_jenis]["detail"][$row_th]["terbit"] = "-"; 
                }
                            
            }
            $row_jenis++;
            $str_tbl .= "</tr>";
        }

        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title;
        $data["kategori"] = "Perijinan";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");

        

    }

    public function lp_dpmptsp_terbit($th_fn){
        $jenis = $this->lpa->get_lp_jenis();
        $array_of_month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        $array_of_num = array("01","02","03","04","05","06","07","08","09","10","11","12");
        $str_tbl = "";
        $data_graph = array();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Perijinan di Pemerintah Kota Malang Tahun ". $th_fn;

        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"20%\">Jenis</th>";
        $str_header_mod_sec = "<tr>";
        $t_col = 2;
        foreach ($array_of_month as $key => $value) {
            $str_header_mod .= "<th>".$value."</th>";
            $str_header_mod_sec .= "<th>Terbit</th>
                                    <th>Investasi</th>
                                    <th>TK</th>";
            $t_col+=1;
        }
        $str_header_mod .= "</tr>";
        $str_header_mod_sec .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title."</th>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 1;
        foreach ($jenis as $r_jenis => $v_jenis) {
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;  

            $str_tbl .= " <tr>
                            <td align=\"center\">".$row_jenis."</td>
                            <td>".$v_jenis->nama_jenis."</td>"; 

            $row_th= 0;
            $array_count = array();
            foreach ($array_of_num as $r_num => $v_num) {
                $val_data = $this->lpa->get_lp(array("lpt.id_jenis"=>$v_jenis->id_jenis, "th"=>$th_fn.$v_num));
                if(is_numeric($val_data["terbit"])){
                    $str_tbl .= "<td align=\"right\">".number_format($val_data["terbit"],0,".",",")."</td>";
                    // $data["list_data"][$row_jenis]["detail"][$row_th]["tk"] = $val_data["tk"];
                    // $data["list_data"][$row_jenis]["detail"][$row_th]["investasi"] = $val_data["investasi"];
                    $data["list_data"][$row_jenis]["detail"][$row_th]["terbit"] = $val_data["terbit"];
                }else{
                    $str_tbl .= "<td align=\"right\">0</td>";

                    // $data["list_data"][$row_jenis]["detail"][$row_th]["tk"] = "-";
                    // $data["list_data"][$row_jenis]["detail"][$row_th]["investasi"] = "-";
                    $data["list_data"][$row_jenis]["detail"][$row_th]["terbit"] = "-"; 
                }
                            
            }
            $row_jenis++;
            $str_tbl .= "</tr>";
        }

        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title;
        $data["kategori"] = "Perijinan";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");
        
    }

    public function lp_dpmptsp_tk($th_fn){
        $jenis = $this->lpa->get_lp_jenis();
        $array_of_month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        $array_of_num = array("01","02","03","04","05","06","07","08","09","10","11","12");
        $str_tbl = "";
        $data_graph = array();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Perijinan di Pemerintah Kota Malang Tahun ". $th_fn;

        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"20%\">Jenis</th>";
        $str_header_mod_sec = "<tr>";
        $t_col = 2;
        foreach ($array_of_month as $key => $value) {
            $str_header_mod .= "<th>".$value."</th>";
            $str_header_mod_sec .= "<th>Terbit</th>
                                    <th>Investasi</th>
                                    <th>TK</th>";
            $t_col+=1;
        }
        $str_header_mod .= "</tr>";
        $str_header_mod_sec .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title."</th>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 1;
        foreach ($jenis as $r_jenis => $v_jenis) {
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;  

            $str_tbl .= " <tr>
                            <td align=\"center\">".$row_jenis."</td>
                            <td>".$v_jenis->nama_jenis."</td>"; 

            $row_th= 0;
            $array_count = array();
            foreach ($array_of_num as $r_num => $v_num) {
                $val_data = $this->lpa->get_lp(array("lpt.id_jenis"=>$v_jenis->id_jenis, "th"=>$th_fn.$v_num));
                if(is_numeric($val_data["tk"])){
                    $str_tbl .= "<td align=\"right\">".number_format($val_data["tk"],0,".",",")."</td>";
                    $data["list_data"][$row_jenis]["detail"][$row_th]["tk"] = $val_data["tk"];
                    // $data["list_data"][$row_jenis]["detail"][$row_th]["investasi"] = $val_data["investasi"];
                    // $data["list_data"][$row_jenis]["detail"][$row_th]["terbit"] = $val_data["terbit"];
                }else{
                    $str_tbl .= "<td align=\"right\">0</td>";

                    $data["list_data"][$row_jenis]["detail"][$row_th]["tk"] = "-";
                    // $data["list_data"][$row_jenis]["detail"][$row_th]["investasi"] = "-";
                    // $data["list_data"][$row_jenis]["detail"][$row_th]["terbit"] = "-"; 
                }
                            
            }
            $row_jenis++;
            $str_tbl .= "</tr>";
        }

        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title;
        $data["kategori"] = "Perijinan";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");
        
    }
}
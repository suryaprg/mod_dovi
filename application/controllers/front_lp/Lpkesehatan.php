<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkesehatan extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_kesehatan", "lk");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kesehatan";
		$data["list_kesehatan"] = $this->lk->get_kesehatan();
		$data["list_kesehatan_jenis"] = $this->lk->get_kesehatan_jenis();
		$data["list_kesehatan_sub_jenis"] = $this->lk->get_kesehatan_sub_jenis();
		$this->load->view('front_lp/penduduk/main_admin_penduduk', $data);
	}

//================================================ LP Kesehatan ====================================================//
	private function validation_ins_lp_kesehatan(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'id_sub_jenis',
                'label'=>'Sub Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_kesehatan(){
		if($this->validation_ins_lp_kesehatan()){
			$id_jenis 		= $this->input->post("id_jenis");
			$id_sub_jenis 	= $this->input->post("id_sub_jenis");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");
			
			$data_insert = array(
							"id_kesehatan" 	=>"",
							"id_jenis"		=>$id_jenis,
							"id_sub_jenis"	=>$id_sub_jenis,
							"th"			=>$th,
							"jml"			=>$jml
						);
			$insert = $this->lk->insert_kesehatan($data_insert);
				if($insert){
					redirect('admin/kesehatan');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_kesehatan(){
		$id_kesehatan 	= $this->input->post("id_kesehatan");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main =$this->lk->get_kesehatan_where(array("id_kesehatan"=>$id_kesehatan)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_kesehatan(){
		if($this->validation_ins_lp_kesehatan()){
			$id_kesehatan = $this->input->post("id_kesehatan");

			$id_jenis 		= $this->input->post("id_jenis");
			$id_sub_jenis 	= $this->input->post("id_sub_jenis");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");

			$data = array(
						
						"id_jenis"		=> $id_jenis,
						"id_sub_jenis"	=> $id_sub_jenis,
						"th"			=> $th,
						"jml"			=> $jml
					);

			$data_where = array(
							"id_kesehatan" => $id_kesehatan,
						);

			$insert = $this->lk->update_kesehatan($data, $data_where);
				if($insert){
					redirect('admin/kesehatan');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_kesehatan($id_kesehatan){
		$delete = $this->lk->delete_kesehatan(array("id_kesehatan" => $id_kesehatan));
		if($delete){
			echo "del";
			redirect('admin/kesehatan');
		}else {
			echo "fail";
		}
	}
//================================================ LP Kesehatan ====================================================//	
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppendidikanall extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_pendidikan_all', 'lp');

        $this->load->library("response_message");

	}

#===========================================================================================================================
#-----------------------------------------------------Pendidikan All--------------------------------------------------
#===========================================================================================================================
    public function pendidikan_all($id_kategori,$th_fn){
        $jenis = $this->lp->get_lp_jenis();
        $kategori = $this->lp->get_lp_kategori_filter(array("id_kategori"=>$id_kategori));
        $kecamatan = $this->lp->get_kec();

        $kategori_all = $this->lp->get_lp_kategori();

        //------------------------------------------------header-------------------------------------------------

        $str_title = "Data Pendidikan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

        $str_header_mod = "<tr>
                                <td width=\"5%\" rowspan=\"2\">No</td>
                                <td width=\"35%\" rowspan=\"2\">Keterangan</td>";
        $str_header_mod_sec ="";

        $t_col = 2;
        for ($i=$th_fn-1; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td  colspan=\"3\">".$i."</td>";
            $str_header_mod_sec .= "<td width=\"10%\">Negeri</td>
                                    <td width=\"10%\">Swasta</td>
                                    <td width=\"10%\">Jumlah Sekolah</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"2\">".$str_title."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-1;
        $data_graph = array();
        $str_tbl = "";

        $row_kategori = 0;

        $str_dropdown_kategori = "";

        foreach ($kategori as $r_kate => $v_kate) {
            $str_tbl = "";

            $data["list_data"][$v_kate->id_kategori]["kategori"]["main"]["id_kategori"] = $v_kate->id_kategori;
            $data["list_data"][$v_kate->id_kategori]["kategori"]["main"]["nama_kategori"] = $v_kate->nama_kategori;

            $str_tbl .= "<table class=\"data-table\" style=\"width: 100%;\">
                         <thead>
                          <tr>
                            <td colspan=\"2\">Data ".$v_kate->nama_kategori." di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>
                         </thead>";

            // $data_graph[$v_kate->id_kategori]["main"]

            $row_jenis = 0;
            foreach ($jenis as $r_data => $v_data) {
                $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["main"]["id_jenis"] = $v_data->id_jenis;
                $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["main"]["nama_jenis"] = $v_data->nama_jenis;

                $str_tbl .=    "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                                <td><a href=\"#\">".($row_jenis+1)."</a></td>
                                <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                            </tr>
                            <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                                <td>&nbsp;</td>
                                <td>
                                     <table class=\"data-table\" style=\"width: 100%;\">".
                                     $str_header_mod.$str_header_mod_sec;     


                $r_kec = 0;

                $array_jml = array();
                foreach ($kecamatan as $r_sub => $v_sub) {
                    $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                    $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;
                    
                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                    <td><a href=\"#\">".($r_kec+1)."</a></td>
                                    <td><a href=\"#\">".$v_sub->nama_kec."</a></td>";


                    $row_th = 0;
                    for ($i=$th_st; $i <= $th_fn ; $i++) {
                        $val_data = $this->lp->get_lp(array("id_kategori"=>$v_kate->id_kategori,
                                                        "id_jenis"=>$v_data->id_jenis,
                                                        "id_kec"=>$v_sub->id_kec,
                                                        "th"=>$i));
                        // $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                        $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["kecamatan"][$r_kec]["value"][$i][0] = $val_data["jml_negeri"];
                        $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["kecamatan"][$r_kec]["value"][$i][1] = $val_data["jml_swasta"];

                        $jml_negri = 0;
                        if($val_data["jml_negeri"]){
                            $str_tbl .= "<td>".$val_data["jml_negeri"]."</td>";
                            $jml_negri = $val_data["jml_negeri"];
                        }else{
                            $str_tbl .= "<td>-</td>";
                            $jml_negri = 0;
                        }

                        $jml_swasta = 0;
                        if($val_data["jml_swasta"]){
                            $str_tbl .= "<td>".$val_data["jml_swasta"]."</td>";
                            $jml_swasta = $val_data["jml_swasta"];
                        }else{
                            $str_tbl .= "<td>-</td>";
                            $jml_swasta = 0;
                        }

                        $str_tbl .= "<td>".($jml_swasta+$jml_negri)."</td>";

                        $data_graph[$v_kate->id_kategori][$v_data->id_jenis][$v_sub->id_kec][$row_th]["year"] = (int)$i;
                        $data_graph[$v_kate->id_kategori][$v_data->id_jenis][$v_sub->id_kec][$row_th]["jml_negeri"] = (int)$val_data["jml_negeri"];
                        $data_graph[$v_kate->id_kategori][$v_data->id_jenis][$v_sub->id_kec][$row_th]["jml_swasta"] = (int)$val_data["jml_swasta"];
                        $data_graph[$v_kate->id_kategori][$v_data->id_jenis][$v_sub->id_kec][$row_th]["jml_all"] = (int)$jml_swasta+$jml_negri;

                        if(isset($array_jml[$i]["jml_swasta"])){
                            $array_jml[$i]["jml_swasta"] += $jml_swasta;
                        }else{
                            $array_jml[$i]["jml_swasta"] = $jml_swasta;
                        }

                        if(isset($array_jml[$i]["jml_negri"])){
                            $array_jml[$i]["jml_negri"] += $jml_negri;
                        }else{
                            $array_jml[$i]["jml_negri"] = $jml_negri;
                        }

                        if(isset($array_jml[$i]["jml_all"])){
                            $array_jml[$i]["jml_all"] += ($jml_swasta+$jml_negri);
                        }else{
                            $array_jml[$i]["jml_all"] = ($jml_swasta+$jml_negri);
                        }

                        $row_th++;

                    }
                    
                    $r_kec++;
                    $str_tbl .= "</tr>";
                }

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_00\">
                                    <td><a href=\"#\">-</a></td>
                                    <td><a href=\"#\">Jumlah</a></td>";

                $row_jml = 0;
                foreach ($array_jml as $r_jml => $v_jml) {
                    $str_tbl .= "<td>".$v_jml["jml_swasta"]."</td>
                                <td>".$v_jml["jml_negri"]."</td>
                                <td>".$v_jml["jml_all"]."</td>";

                    $data_graph[$v_kate->id_kategori][$v_data->id_jenis]["00"][$row_jml]["year"] = (int)$r_jml;
                    $data_graph[$v_kate->id_kategori][$v_data->id_jenis]["00"][$row_jml]["jml_negeri"] = (int)$v_jml["jml_negri"];
                    $data_graph[$v_kate->id_kategori][$v_data->id_jenis]["00"][$row_jml]["jml_swasta"] = (int)$v_jml["jml_swasta"];
                    $data_graph[$v_kate->id_kategori][$v_data->id_jenis]["00"][$row_jml]["jml_all"] = (int)$v_jml["jml_all"];

                    $row_jml++;
                }

                $str_tbl .= "</tr>";

                $row_jenis++;

                $str_tbl .= "           
                                    </table>
                                </td>
                            </tr>";
                $row_jenis++;
            }

            $str_tbl .= "</table>";

            $data["str_tbl"][$v_kate->id_kategori] = (string)$str_tbl;

            $row_kategori++;   
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        $data["move_tbl"] = json_encode($data["str_tbl"]);
        // $data["str_series"] = $str_series;
        $data["kategori"] = $kategori;
        $data["str_header"] = $str_header_top;
        $data["title"] = $str_title;
        $data["id_kategori"] = $id_kategori;


        foreach ($kategori_all as $r_kate_all => $v_kate_all) {
            $str_dropdown_kategori .= "<option id=\"val_".$v_kate_all->id_kategori."\" value=\"".$v_kate_all->id_kategori."\">".$v_kate_all->nama_kategori."</option>";    
        }
        
        $data["dropdown_kategori"] = "<select name=\"get_kate\" id=\"get_kate\">".$str_dropdown_kategori."</select>";

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_lp/Pendidikan/lp_pendidikan_all",$data);
    }

    
#===========================================================================================================================
#-----------------------------------------------------Pendidikan All--------------------------------------------------
#===========================================================================================================================
}
?>
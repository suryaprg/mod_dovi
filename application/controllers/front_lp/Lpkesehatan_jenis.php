<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkesehatan_jenis extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_kesehatan", "lk");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kesehatan_jenis";
		$data["list_kesehatan_jenis"] = $this->lk->get_kesehatan_jenis();
		$data["list_kesehatan_kategori"] = $this->lk->get_kesehatan_kategori();
		$this->load->view('front_lp/penduduk/main_admin_penduduk', $data);
	}

//================================================== LP KESEHATAN JENIS ========================================//
	private function validation_ins_lp_kesehatan_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'id_kategori',
                'label'=>'Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_kesehatan_jenis(){
		$id_jenis 		= $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main =$this->lk->get_kesehatan_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_kesehatan_jenis(){
		if($this->validation_ins_lp_kesehatan_jenis()){
			$nama_jenis 	= $this->input->post("nama_jenis");
			$id_kategori 	= $this->input->post("id_kategori");
			
			$data_insert = array(
							"id_jenis"		=> "",
							"nama_jenis"	=> $nama_jenis,
							"id_kategori"	=> $id_kategori
						);
			$insert = $this->lk->insert_kesehatan_jenis($data_insert);
				if($insert){
				redirect('admin/kesehatan/jenis');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_kesehatan_jenis(){
		if($this->validation_ins_lp_kesehatan_jenis()){
			$id_jenis 		= $this->input->post("id_jenis");

			$nama_jenis		= $this->input->post("nama_jenis");
			$id_kategori	= $this->input->post("id_kategori");

			$data = array(
						
						"nama_jenis" => $nama_jenis,
						"id_kategori"=> $id_kategori
					);

			$data_where = array(
							"id_jenis" => $id_jenis,
						);

			$insert = $this->lk->update_kesehatan_jenis($data, $data_where);
				if($insert){
					redirect('admin/kesehatan/jenis');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_kesehatan_jenis($id_jenis){
		$delete = $this->lk->delete_kesehatan_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/kesehatan/jenis');
		}else {
			echo "fail";
		}
	}
//================================================== LP KESEHATAN JENIS ========================================//

//================================================== LP KESEHATAN KATEGORI ========================================//
	private function validation_ins_lp_kesehatan_kategori(){
		$config_val_input = array(
            array(
                'field'=>'nama_kategori',
                'label'=>'Nama Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_kesehatan_kategori(){
		$id_kategori 	= $this->input->post("id_kategori");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->lk->get_kesehatan_kategori_where(array("id_kategori"=>$id_kategori)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_kesehatan_kategori(){
		if($this->validation_ins_lp_kesehatan_kategori()){
			$nama_kategori = $this->input->post("nama_kategori");
			
			$data_insert = array(
							"id_kategori"	=> "",
							"nama_kategori"	=> $nama_kategori
						);
			$insert = $this->lk->insert_kesehatan_kategori($data_insert);
				if($insert){
					redirect('admin/kesehatan/jenis');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_kesehatan_kategori(){
		if($this->validation_ins_lp_kesehatan_kategori()){
			$id_kategori 	= $this->input->post("id_kategori");

			$nama_kategori 	= $this->input->post("nama_kategori");

			$data = array(
						
						"nama_kategori" => $nama_kategori
					);

			$data_where = array(
							"id_kategori" => $id_kategori,
						);

			$insert = $this->lk->update_kesehatan_kategori($data, $data_where);
				if($insert){
					redirect('admin/kesehatan/jenis');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_kesehatan_kategori($id_kategori){
		$delete = $this->lk->delete_kesehatan_kategori(array("id_kategori"=>$id_kategori));
		if($delete){
			echo "del";
			redirect('admin/kesehatan/jenis');
		}else {
			echo "fail";
		}
	}
//================================================== LP KESEHATAN KATEGORI ========================================//				
}
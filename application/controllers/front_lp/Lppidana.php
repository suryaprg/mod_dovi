<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppidana extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_pidana", "pd");
		$this->load->library("response_message");
				if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "pidana";
		$data["list_pidana"] = $this->pd->get_pidana();
		$data["list_pidana_kec"] = $this->pd->get_pidana_kec();
		$data["list_pidana_jenis"] = $this->pd->get_pidana_jenis();
		$this->load->view('front_lp/hukum/main_admin_hukum', $data);
	}

//================================================ LP pidana====================================================//
	private function validation_ins_lp_pidana(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                         
            ),
            array(
                'field'=>'lapor',
                'label'=>'Lapor',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )    
                       
            ),
            array(
                'field'=>'selesai',
                'label'=>'Selesai',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pidana(){
		if($this->validation_ins_lp_pidana()){
			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$lapor = $this->input->post("lapor");
			$selesai = $this->input->post("selesai");
			
			$data_insert = array(
							"id_pidana"=>"",
							"id_jenis"=>$id_jenis,
							"th"=>$th,
							"lapor"=>$lapor,
							"selesai"=>$selesai
						);
			$insert = $this->pd->insert_pidana($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/pidana');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pidana(){
		$id_pidana = $this->input->post("id_pidana");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->pd->get_pidana_where(array("id_pidana"=>$id_pidana)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pidana(){
		if($this->validation_ins_lp_pidana()){
			$id_pidana = $this->input->post("id_pidana");

			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$lapor = $this->input->post("lapor");
			$selesai = $this->input->post("selesai");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"lapor"=>$lapor,
						"selesai"=>$selesai
					);

			$data_where = array(
							"id_pidana"=>$id_pidana,
						);

			$insert = $this->pd->update_pidana($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin_opd/geo/Lppidana','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pidana($id_pidana){
		$delete = $this->pd->delete_pidana(array("id_pidana"=>$id_pidana));
		if($delete){
			echo "del";
			redirect('admin_opd/geo/Lppidana','refresh');
		}else {
			echo "fail";
		}
	}

//================================================ LP pidana jenis====================================================//
private function validation_ins_lp_pidana_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_pidana_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->pd->get_pidana_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_pidana_jenis(){
		if($this->validation_ins_lp_pidana_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->pd->insert_pidana_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/pidana');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_pidana_jenis(){
		if($this->validation_ins_lp_pidana_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->pd->update_pidana_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/pidana');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pidana_jenis($id_jenis){
		$delete = $this->pd->delete_pidana_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/pidana');
		}else {
			echo "fail";
		}
	}

//================================================ LP pidana Kec====================================================//
private function validation_ins_lp_pidana_kec(){
		$config_val_input = array(
            array(
                'field'=>'id_kec',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                         
            ),
            array(
                'field'=>'jml_lapor',
                'label'=>'Jumlah Lapor',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )    
                       
            ),
            array(
                'field'=>'jml_selesai',
                'label'=>'Selesai',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pidana_kec(){
		if($this->validation_ins_lp_pidana_kec()){
			$id_kec = $this->input->post("id_kec");
			$th = $this->input->post("th");
			$jml_lapor = $this->input->post("jml_lapor");
			$jml_selesai = $this->input->post("jml_selesai");
			
			$data_insert = array(
							"id_pidana"=>"",
							"id_kec"=>$id_kec,
							"th"=>$th,
							"jml_lapor"=>$jml_lapor,
							"jml_selesai"=>$jml_selesai
						);
			$insert = $this->pd->insert_pidana_kec($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/pidana');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pidana_kec(){
		$id_pidana = $this->input->post("id_pidana");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->pd->get_pidana_kec_where(array("id_pidana"=>$id_pidana)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pidana_kec(){
		if($this->validation_ins_lp_pidana_kec()){
			$id_pidana = $this->input->post("id_pidana");

			$id_kec = $this->input->post("id_kec");
			$th = $this->input->post("th");
			$jml_lapor = $this->input->post("jml_lapor");
			$jml_selesai = $this->input->post("jml_selesai");

			$data = array(
						
						"id_kec"=>$id_kec,
						"th"=>$th,
						"jml_lapor"=>$jml_lapor,
						"jml_selesai"=>$jml_selesai
					);

			$data_where = array(
							"id_pidana"=>$id_pidana,
						);

			$insert = $this->pd->update_pidana_kec($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/pidana');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pidana_kec($id_pidana){
		$delete = $this->pd->delete_pidana_kec(array("id_pidana"=>$id_pidana));
		if($delete){
			echo "del";
			redirect('admin/pidana');
		}else {
			echo "fail";
		}
	}
}



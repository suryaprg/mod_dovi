<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lptehnologi extends CI_Controller{

    public function __construct(){
        parent::__construct();  
        $this->load->model('front_lp/Lp_tehnologi', 'lt');
        
        $this->load->library("response_message");

    }

#===========================================================================================================================
#------------------------------------------------------------Tehnologi------------------------------------------------------
#===========================================================================================================================
    public function tehnologi($th_fn){
        $jenis = $this->lt->get_lp_jenis();
        $sub_jenis = $this->lt->get_lp_sub();

        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        $str_header = "     <tr>
                                <th width=\"10%\">No</th>
                                <th width=\"30%\">Keterangan</th>
                                    ";
        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header .= "<th width=\"20%\">".$i."</th>";
            $t_col++;
        }

        $str_header .= "    </tr>";
        $data_graph = array();

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td>-</td>
                            <td colspan=\"".$t_col."\"><b>".$v_data->nama_jenis."</b></td>";
            $str_tbl .= "</tr>";

            $t_data = array();

            $r_kec = 0;
            foreach ($sub_jenis as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["sub_jenis"][$r_kec]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                $data["list_data"][$row_jenis]["sub_jenis"][$r_kec]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_sub->id_sub_jenis."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_sub_jenis."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lt->get_lp(array("id_sub_jenis"=> $v_sub->id_sub_jenis, "th"=>$i));
                    $data["list_data"][$row_jenis]["sub_jenis"][$r_kec]["value"][$i] = $val_data["jml"];

                    $str_tbl .= "<td>".$val_data["jml"]."</td>";

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_sub->id_sub_jenis][$row_th] = $main_data;
                    $row_th++;
                }
                $str_tbl .= "</tr>";
                $r_kec++;
            }
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;
        $data["str_tbl"] = $str_tbl;
        $data["str_header"] = $str_header;
        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["sub_jenis"] = $sub_jenis;
        // $data["jml_th"] = $row_th;

        // print_r("<pre>");
        // print_r($data["graph_data"]);
        // print_r("<table>".$str_header."</table>");

        // $this->load->view("front_lp/keuangan/tenaga",$data);
        $this->load->view("front_lp/tehnologi/tehnologi",$data);
    }
#===========================================================================================================================
#----------------------------------------------------------Tehnologi--------------------------------------------------------
#===========================================================================================================================

}
?>
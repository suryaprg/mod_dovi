<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpdpmptsp extends CI_Controller{

	public function __construct(){
		parent::__construct();	
        $this->load->model('front_lp/Lp_dpmptsp', 'lpa');
	}

	public function lp_dpmptsp_investasi($th_fn){
            $data["page"] = "investasi";
		$array_of_color = array("#FF0F00","#FF6600","#FF9E01","#FCD202","#F8FF01","#B0DE09","#04D215","#0D8ECF","#0D52D1","#2A0CD0","#8A0CCF","#CD0D74");
        $jenis = $this->lpa->get_lp_jenis();
        $array_of_month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        $array_of_num = array("01","02","03","04","05","06","07","08","09","10","11","12");
        $str_tbl = "";
        $data_graph = array();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Perijinan di Pemerintah Kota Malang Tahun ". $th_fn;

        $str_header_mod = "<tr>
                                <th width=\"5%\" rowspan=\"2\">No</th>
                                <th width=\"20%\" rowspan=\"2\">Jenis</th>";
        $str_header_mod_sec = "<tr>";
        $t_col = 2;
        foreach ($array_of_month as $key => $value) {
            $str_header_mod .= "<th>".$value."</th>";
            $str_header_mod_sec .= "<th>Terbit</th>
                                    <th>Investasi</th>
                                    <th>TK</th>";
            $t_col+=1;
        }
        $str_header_mod .= "</tr>";
        $str_header_mod_sec .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title."</th>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 1;
        foreach ($jenis as $r_jenis => $v_jenis) {
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;  

            $str_tbl .= " <tr class=\"jenis\" id=\"jenis_".$v_jenis->id_jenis."\">
                            <td align=\"center\">".$row_jenis."</td>
                            <td>".$v_jenis->nama_jenis."</td>"; 

            $row_th= 0;
            $array_count = array();
            foreach ($array_of_num as $r_num => $v_num) {
                $val_data = $this->lpa->get_lp(array("lpt.id_jenis"=>$v_jenis->id_jenis, "th"=>$th_fn.$v_num));
                $data_graph[$v_jenis->id_jenis][$row_th]["bulan"] = $array_of_month[$r_num];

                if(is_numeric($val_data["investasi"])){
                    $str_tbl .= "<td align=\"right\">".number_format($val_data["investasi"],0,".",",")."</td>";

                    $data_graph[$v_jenis->id_jenis][$row_th]["val_data"] = (double)$val_data["investasi"];

                    $data["list_data"][$row_jenis]["detail"][$row_th]["investasi"] = $val_data["investasi"];
                }else{
                    $str_tbl .= "<td align=\"right\">0</td>";

                    $data_graph[$v_jenis->id_jenis][$row_th]["val_data"] = 0;

                    $data["list_data"][$row_jenis]["detail"][$row_th]["investasi"] = "-";
                }

                $data_graph[$v_jenis->id_jenis][$row_th]["color"] = $array_of_color[$r_num];
                
                $row_th++;              
            }
            $row_jenis++;
            $str_tbl .= "</tr>";
        }

        $data["title"] = $str_title;
        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;
        $data["data_graph"] = json_encode($data_graph);

        $data["th_fn"] = $th_fn; 
    //     print_r("<table border=\"1\">
				// 	<thead>".$data["str_header"]."</thead>
				// 	<tbody>".$str_tbl."</tbody>	
				// </table>");
        // print_r($data["data_graph"]);
        $this->load->view('front_data/sosial/masterSospolkem', $data);
	}

    public function lp_dpmptsp_terbit($th_fn){
            $data["page"] = "terbit";
            $array_of_color = array("#FF0F00","#FF6600","#FF9E01","#FCD202","#F8FF01","#B0DE09","#04D215","#0D8ECF","#0D52D1","#2A0CD0","#8A0CCF","#CD0D74");
        $jenis = $this->lpa->get_lp_jenis();
        $array_of_month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        $array_of_num = array("01","02","03","04","05","06","07","08","09","10","11","12");
        $str_tbl = "";
        $data_graph = array();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Perijinan di Pemerintah Kota Malang Tahun ". $th_fn;

        $str_header_mod = "<tr>
                                <th width=\"5%\" rowspan=\"2\">No</th>
                                <th width=\"20%\" rowspan=\"2\">Jenis</th>";
        $str_header_mod_sec = "<tr>";
        $t_col = 2;
        foreach ($array_of_month as $key => $value) {
            $str_header_mod .= "<th>".$value."</th>";
            $str_header_mod_sec .= "<th>Terbit</th>
                                    <th>Investasi</th>
                                    <th>TK</th>";
            $t_col+=1;
        }
        $str_header_mod .= "</tr>";
        $str_header_mod_sec .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title."</th>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        // $row_jenis = 1;
        // foreach ($jenis as $r_jenis => $v_jenis) {
        //     $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
        //     $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;  

        //     $str_tbl .= " <tr>
        //                     <td align=\"center\">".$row_jenis."</td>
        //                     <td>".$v_jenis->nama_jenis."</td>"; 

        //     $row_th= 0;
        //     $array_count = array();
        //     foreach ($array_of_num as $r_num => $v_num) {
        //         $val_data = $this->lpa->get_lp(array("lpt.id_jenis"=>$v_jenis->id_jenis, "th"=>$th_fn.$v_num));
        //         if(is_numeric($val_data["terbit"])){
        //             $str_tbl .= "<td align=\"right\">".number_format($val_data["terbit"],0,".",",")."</td>";
        //             $data["list_data"][$row_jenis]["detail"][$row_th]["terbit"] = $val_data["terbit"];
        //         }else{
        //             $str_tbl .= "<td align=\"right\">0</td>";

        //             $data["list_data"][$row_jenis]["detail"][$row_th]["terbit"] = "-"; 
        //         }
                            
        //     }
        //     $row_jenis++;
        //     $str_tbl .= "</tr>";
        // }


        $row_jenis = 1;
        foreach ($jenis as $r_jenis => $v_jenis) {
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;  

            $str_tbl .= " <tr class=\"jenis\" id=\"jenis_".$v_jenis->id_jenis."\">
                            <td align=\"center\">".$row_jenis."</td>
                            <td>".$v_jenis->nama_jenis."</td>"; 

            $row_th= 0;
            $array_count = array();
            foreach ($array_of_num as $r_num => $v_num) {
                $val_data = $this->lpa->get_lp(array("lpt.id_jenis"=>$v_jenis->id_jenis, "th"=>$th_fn.$v_num));
                $data_graph[$v_jenis->id_jenis][$row_th]["bulan"] = $array_of_month[$r_num];

                if(is_numeric($val_data["terbit"])){
                    $str_tbl .= "<td align=\"right\">".number_format($val_data["terbit"],0,".",",")."</td>";

                    $data_graph[$v_jenis->id_jenis][$row_th]["val_data"] = (double)$val_data["terbit"];

                    $data["list_data"][$row_jenis]["detail"][$row_th]["investasi"] = $val_data["terbit"];
                }else{
                    $str_tbl .= "<td align=\"right\">0</td>";

                    $data_graph[$v_jenis->id_jenis][$row_th]["val_data"] = 0;

                    $data["list_data"][$row_jenis]["detail"][$row_th]["terbit"] = "-";
                }

                $data_graph[$v_jenis->id_jenis][$row_th]["color"] = $array_of_color[$r_num];
                
                $row_th++;              
            }
            $row_jenis++;
            $str_tbl .= "</tr>";
        }

       $data["title"] = $str_title;
        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;
        $data["data_graph"] = json_encode($data_graph);

        $data["th_fn"] = $th_fn;   
        // print_r("<table border=\"1\">
        //             <thead>".$data["str_header"]."</thead>
        //             <tbody>".$str_tbl."</tbody> 
        //         </table>");

          $this->load->view('front_data/sosial/masterSospolkem', $data);
    }

    public function lp_dpmptsp_tk($th_fn){
            $data["page"] = "tk";
            $array_of_color = array("#FF0F00","#FF6600","#FF9E01","#FCD202","#F8FF01","#B0DE09","#04D215","#0D8ECF","#0D52D1","#2A0CD0","#8A0CCF","#CD0D74");
        $jenis = $this->lpa->get_lp_jenis();
        $array_of_month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        $array_of_num = array("01","02","03","04","05","06","07","08","09","10","11","12");
        $str_tbl = "";
        $data_graph = array();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Perijinan di Pemerintah Kota Malang Tahun ". $th_fn;

        $str_header_mod = "<tr>
                                <th width=\"5%\" rowspan=\"2\">No</th>
                                <th width=\"20%\" rowspan=\"2\">Jenis</th>";
        $str_header_mod_sec = "<tr>";
        $t_col = 2;
        foreach ($array_of_month as $key => $value) {
            $str_header_mod .= "<th>".$value."</th>";
            $str_header_mod_sec .= "<th>Terbit</th>
                                    <th>Investasi</th>
                                    <th>TK</th>";
            $t_col+=1;
        }
        $str_header_mod .= "</tr>";
        $str_header_mod_sec .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title."</th>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 1;
        foreach ($jenis as $r_jenis => $v_jenis) {
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;  

            $str_tbl .= " <tr class=\"jenis\" id=\"jenis_".$v_jenis->id_jenis."\">
                            <td align=\"center\">".$row_jenis."</td>
                            <td>".$v_jenis->nama_jenis."</td>"; 

            $row_th= 0;
            $array_count = array();
            foreach ($array_of_num as $r_num => $v_num) {
                $val_data = $this->lpa->get_lp(array("lpt.id_jenis"=>$v_jenis->id_jenis, "th"=>$th_fn.$v_num));
                $data_graph[$v_jenis->id_jenis][$row_th]["bulan"] = $array_of_month[$r_num];

                if(is_numeric($val_data["tk"])){
                    $str_tbl .= "<td align=\"right\">".number_format($val_data["tk"],0,".",",")."</td>";

                    $data_graph[$v_jenis->id_jenis][$row_th]["val_data"] = (double)$val_data["tk"];

                    $data["list_data"][$row_jenis]["detail"][$row_th]["investasi"] = $val_data["tk"];
                }else{
                    $str_tbl .= "<td align=\"right\">0</td>";

                    $data_graph[$v_jenis->id_jenis][$row_th]["val_data"] = 0;

                    $data["list_data"][$row_jenis]["detail"][$row_th]["tk"] = "-";
                }

                $data_graph[$v_jenis->id_jenis][$row_th]["color"] = $array_of_color[$r_num];
                
                $row_th++;              
            }
            $row_jenis++;
            $str_tbl .= "</tr>";
        }

       $data["title"] = $str_title;
        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;
        $data["data_graph"] = json_encode($data_graph);

        $data["th_fn"] = $th_fn;    
        // print_r("<table border=\"1\">
        //             <thead>".$data["str_header"]."</thead>
        //             <tbody>".$str_tbl."</tbody> 
        //         </table>");

          $this->load->view('front_data/sosial/masterSospolkem', $data);
    }
}
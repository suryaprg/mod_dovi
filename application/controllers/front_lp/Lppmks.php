<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppmks extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_pmks", "la");
		$this->load->library("response_message");
				if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "pmks";
		$data["list_pmks"] = $this->la->get_pmks();
		$data["list_pmks_jenis"] = $this->la->get_pmks_jenis();
		$this->load->view('front_lp/penduduk/main_admin_penduduk', $data);
	}

//================================================ LP PMKS ====================================================//
	private function validation_ins_lp_pmks(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_l',
                'label'=>'Jumlahl',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
             array(
                'field'=>'jml_p',
                'label'=>'Jumlahp',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pmks(){
		if($this->validation_ins_lp_pmks()){
			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$jmll = $this->input->post("jml_l");
			$jmlp = $this->input->post("jml_p");
			
			$data_insert = array(
							"id_pmks"=>"",
							"id_jenis"=>$id_jenis,
							"th"=>$th,
							"jml_l"=>$jmll,
							"jml_p"=>$jmlp,
						);
			$insert = $this->la->insert_pmks($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin_opd/geo/Lppmks','refresh');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pmks(){
		$id = $this->input->post("id_pmks");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_pmks_where(array("id_pmks"=>$id)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pmks(){
		if($this->validation_ins_lp_pmks()){
			$id_pmks = $this->input->post("id_pmks");

			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$jmll = $this->input->post("jml_l");
			$jmlp = $this->input->post("jml_p");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"jml_l"=>$jml_l,
						"jml_p"=>$jml_p,
					);

			$data_where = array(
							"id_pmks"=>$id_pmks,
						);

			$insert = $this->la->update_pmks($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin_opd/geo/Lppmks','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pmks($id_pmks){
		$delete = $this->la->delete_pmks(array("id_pmks"=>$id_pmks));
		if($delete){
			echo "del";
			redirect('admin_opd/geo/Lppmks','refresh');
		}else {
			echo "fail";
		}
	}
//================================================ LP PMKS JENIS ====================================================//


	private function validation_ins_lp_pmks_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_pmks_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_pmks_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_pmks_jenis(){
		if($this->validation_ins_lp_pmks_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->la->insert_pmks_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/pmks');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_pmks_jenis(){
		if($this->validation_ins_lp_pmks_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->la->update_pmks_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/pmks');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pmks_jenis($id_jenis){
		$delete = $this->la->delete_pmks_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/pmks');
		}else {
			echo "fail";
		}
	}			
}
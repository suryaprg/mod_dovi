<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkendaraan_umum extends CI_Controller
{

	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_kendaraan", "lk");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] 						= "kendaraan_umum";
		$data["list_kendaraan_umum"] 		= $this->lk->get_kendaraan_umum();
		$data["list_kendaraan_umum_jenis"] 	= $this->lk->get_kendaraan_umum_jenis();
		$data["main_jenis_kendaraan"] 		= $this->lk->get_kendaraan_jenis();
		$this->load->view('front_lp/transportasi/main_admin_transportasi', $data);
	}		

//================================================== LP KENDARAAN UMUM ============================================//
	private function validation_ins_lp_kendaraan_umum(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'ID Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'id_jenis_kendaraan',
                'label'=>'ID Jenis Kendaraan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),            
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}	

	public function insert_lp_kendaraan_umum(){
		if($this->validation_ins_lp_kendaraan_umum()){
			$id_jenis 			= $this->input->post("id_jenis");
			$id_jenis_kendaraan = $this->input->post("id_jenis_kendaraan");
			$th					= $this->input->post("th");
			$jml				= $this->input->post("jml");
			
			$data_insert = array(
							"id_kendaraan"		 => "",
							"id_jenis"			 => $id_jenis,
							"id_jenis_kendaraan" => $id_jenis_kendaraan,
							"th"				 => $th,
							"jml"				 => $jml
						);

			$insert = $this->lk->insert_kendaraan_umum($data_insert);
				if($insert){
					redirect('admin/kendaraan/umum');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}	

	public function index_up_lp_kendaraan_umum(){
		$id_kendaraan = $this->input->post("id_kendaraan");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->lk->get_kendaraan_umum_where(array("id_kendaraan"=>$id_kendaraan)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_kendaraan_umum(){
		if($this->validation_ins_lp_kendaraan_umum()){
			$id_kendaraan 		= $this->input->post("id_kendaraan");

			$id_jenis 			= $this->input->post("id_jenis");
			$id_jenis_kendaraan	= $this->input->post("id_jenis_kendaraan");
			$th 				= $this->input->post("th");
			$jml 				= $this->input->post("jml");

			$data = array(						
						"id_jenis"			 => $id_jenis,
						"id_jenis_kendaraan" => $id_jenis_kendaraan,
						"th"				 => $th,
						"jml"				 => $jml
					);

			$data_where = array(
							"id_kendaraan"	=> $id_kendaraan,
						);

			$insert = $this->lk->update_kendaraan_umum($data, $data_where);
				if($insert){
					redirect('admin/kendaraan/umum');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_kendaraan_umum($id_kendaraan){
		$delete = $this->lk->delete_kendaraan_umum(array("id_kendaraan"=>$id_kendaraan));
		if($delete){
			redirect('admin/kendaraan/umum');
			echo "del";
		}else {
			echo "fail";
		}
	}	
//================================================== LP KENDARAAN UMUM ============================================//

//================================================== LP KENDARAAN UMUM JENIS ============================================//
	private function validation_ins_lp_kendaraan_umum_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}	

	public function insert_lp_kendaraan_umum_jenis(){
		if($this->validation_ins_lp_kendaraan_umum_jenis()){
			$id_jenis 			= $this->input->post("id_jenis");
			$nama_jenis 		= $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"	 => "",
							"nama_jenis" => $nama_jenis
						);

			$insert = $this->lk->insert_kendaraan_umum_jenis($data_insert);
				if($insert){
					redirect('admin/kendaraan/umum');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}	

	public function index_up_lp_kendaraan_umum_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->lk->get_kendaraan_umum_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_kendaraan_umum_jenis(){
		if($this->validation_ins_lp_kendaraan_umum_jenis()){
			$id_jenis 		= $this->input->post("id_jenis");

			$nama_jenis		= $this->input->post("nama_jenis");

			$data = array(						
						"nama_jenis" => $nama_jenis
					);

			$data_where = array(
							"id_jenis"	=> $id_jenis,
						);

			$insert = $this->lk->update_kendaraan_umum_jenis($data, $data_where);
				if($insert){
					redirect('admin/kendaraan/umum');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_kendaraan_umum_jenis($id_jenis){
		$delete = $this->lk->delete_kendaraan_umum_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			redirect('admin/kendaraan/umum');
			echo "del";
		}else {
			echo "fail";
		}
	}	
//================================================== LP KENDARAAN PLAT UMUM ============================================//	
}
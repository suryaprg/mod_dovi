<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppertanian extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_lahan', 'll');
        $this->load->model('front_lp/Lp_panen', 'lp');

        $this->load->library("response_message");

	}

#===========================================================================================================================
#-----------------------------------------------------Lahan Kecamatan--------------------------------------------------
#===========================================================================================================================
    public function lahan_kec($th_fn){
        $jenis = $this->ll->get_lp_jenis();
        $kecamatan = $this->ll->get_kec();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Data Luas Lahan (hektar) menurut Kecamatan dan Penggunaan Lahan ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $t_data = array();

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"bottom: 30px;\" width=\"100%\">".
                                 $str_header_mod;

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;
                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kec."</a></td>";
                
                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->ll->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data){
                        $str_tbl .= "<td>".$val_data["jml"]."</td>";
                    }else{
                        $str_tbl .= "<td>-</td>";
                    }
                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_data->id_jenis][$v_sub->id_kec][$row_th] = $main_data;
                    $row_th++;
                }

                $str_tbl .= "</tr>";
                
                $r_kec++;
            }
            $row_jenis++;
            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;
        $data["str_tbl"] = $str_tbl;
        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kecamatan"] = $kecamatan;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;
        // $data["jml_th"] = $row_th;

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_lp/pertanian/lp_pertanian_lahan",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Lahan Kecamatan--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Luas Panen Kecamatan--------------------------------------------------
#===========================================================================================================================
    public function panen_kec($th_fn){
        $jenis = $this->lp->get_lp_jenis();
        $kecamatan = $this->lp->get_kec();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Luas Penggunaan Lahan Sawah (hektar) menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"bottom: 30px;\" width=\"100%\">".
                                 $str_header_mod;            

            $t_data = array();

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kec."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $array_val;

                    if($val_data["jml_panen"]){
                        $str_tbl .= "<td>".$val_data["jml_panen"]."</td>";
                        $jml_panen = $val_data["jml_panen"];
                    }else{
                        $str_tbl .= "<td>-</td>";
                        $jml_panen = 0;
                    }

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml_panen"]);
                    $data_graph[$v_data->id_jenis][$v_sub->id_kec][$row_th] = $main_data;
                    $row_th++;

                    // if($val_data["luas_panen"]){
                    //     $str_tbl .= "<td>".$val_data["luas_panen"]."</td>";
                    //     $luas_panen = $val_data["luas_panen"];
                    // }else{
                    //     $str_tbl .= "<td>-</td>";
                    //     $luas_panen = 0;
                    // }

                    
                    // if($jml_panen == 0 or $luas_panen == 0){
                    //     $str_tbl .= "<td>-</td>";
                    // }else{
                    //     $prob_panen = $luas_panen/($jml_panen*10);
                    //     $str_tbl .= "<td>".$prob_panen."</td>";
                    // }
                }
                
                $r_kec++;
                $str_tbl .= "</tr>";
            }
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kecamatan"] = $kecamatan;
        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_lp/pertanian/lp_pertanian_panen",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Luas Penen Kecamatan--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Hasil Panen Kecamatan--------------------------------------------------
#===========================================================================================================================
    public function hasil_panen_kec($th_fn){
        $jenis = $this->lp->get_lp_jenis();
        $kecamatan = $this->lp->get_kec();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Luas Penggunaan Lahan Sawah (hektar) menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"bottom: 30px;\" width=\"100%\">".
                                 $str_header_mod;            

            $t_data = array();

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kec."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $array_val;

                    // if($val_data["jml_panen"]){
                    //     $str_tbl .= "<td>".$val_data["jml_panen"]."</td>";
                    //     $jml_panen = $val_data["jml_panen"];
                    // }else{
                    //     $str_tbl .= "<td>-</td>";
                    //     $jml_panen = 0;
                    // }

                    
                    if($val_data["luas_panen"]){
                        $str_tbl .= "<td>".$val_data["luas_panen"]."</td>";
                        $luas_panen = $val_data["luas_panen"];
                    }else{
                        $str_tbl .= "<td>-</td>";
                        $luas_panen = 0;
                    }

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["luas_panen"]);
                    $data_graph[$v_data->id_jenis][$v_sub->id_kec][$row_th] = $main_data;
                    $row_th++;

                    
                    // if($jml_panen == 0 or $luas_panen == 0){
                    //     $str_tbl .= "<td>-</td>";
                    // }else{
                    //     $prob_panen = $luas_panen/($jml_panen*10);
                    //     $str_tbl .= "<td>".$prob_panen."</td>";
                    // }
                }
                
                $r_kec++;
                $str_tbl .= "</tr>";
            }
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kecamatan"] = $kecamatan;
        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_lp/pertanian/lp_pertanian_panen_lahan",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Hasil Penen Kecamatan--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Produktivitas Panen Kecamatan--------------------------------------------------
#===========================================================================================================================
    public function prod_panen_kec($th_fn){
        $jenis = $this->lp->get_lp_jenis();
        $kecamatan = $this->lp->get_kec();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td>No</td>
                                <td>Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td>".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Luas Penggunaan Lahan Sawah (hektar) menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" width=\"100%\">".
                                 $str_header_mod;            

            $t_data = array();

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kec."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $array_val;

                    if($val_data["jml_panen"]){
                        // $str_tbl .= "<td>".$val_data["jml_panen"]."</td>";
                        $jml_panen = $val_data["jml_panen"];
                    }else{
                        // $str_tbl .= "<td>-</td>";
                        $jml_panen = 0;
                    }

                    
                    if($val_data["luas_panen"]){
                        // $str_tbl .= "<td>".$val_data["luas_panen"]."</td>";
                        $luas_panen = $val_data["luas_panen"];
                    }else{
                        // $str_tbl .= "<td>-</td>";
                        $luas_panen = 0;
                    }                 
                    
                    $prob_panen = 0;
                    if($jml_panen == 0 or $luas_panen == 0){
                        $str_tbl .= "<td>-</td>";
                    }else{
                        $prob_panen = (($jml_panen*10)/$luas_panen)*100;
                        $str_tbl .= "<td>".number_format($prob_panen, 2, ".", ",")."</td>";
                    }

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$prob_panen);
                    $data_graph[$v_data->id_jenis][$v_sub->id_kec][$row_th] = $main_data;
                    $row_th++;

                }
                
                $r_kec++;
                $str_tbl .= "</tr>";
            }
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kecamatan"] = $kecamatan;
        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_lp/pertanian/lp_pertanian_panen_prob",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Produktivitas Penen Kecamatan--------------------------------------------------
#===========================================================================================================================
}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpmiskin extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("front_lp/lp_miskin", "la");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "miskin";
		$data["list_miskin"] = $this->la->get_miskin();
		$data["list_miskin_jenis"] = $this->la->get_miskin_jenis();
		$this->load->view('front_lp/miskin/main_admin_miskin', $data);
	}

//================================================ LP miskin ====================================================//
	private function validation_ins_lp_miskin(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_miskin(){
		if($this->validation_ins_lp_miskin()){
			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			
			$data_insert = array(
							"id_miskin"=>"",
							"id_jenis"=>$id_jenis,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->la->insert_miskin($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('/front_lp/Lpmiskin','refresh');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_miskin(){
		$id = $this->input->post("id_miskin");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_miskin_where(array("id_miskin"=>$id)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_miskin(){
		if($this->validation_ins_lp_miskin()){
			$id_miskin = $this->input->post("id_miskin");

			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"jml"=>$jml
					);

			$data_where = array(
							"id_miskin"=>$id_miskin,
						);

			$insert = $this->la->update_miskin($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('/front_lp/Lpmiskin','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_miskin($id_miskin){
		$delete = $this->la->delete_miskin(array("id_miskin"=>$id_miskin));
		if($delete){
			echo "del";
			redirect('/front_lp/Lpmiskin','refresh');
		}else {
			echo "fail";
		}
	}	

//================================================ LP miskin Jenis ====================================================//
	private function validation_ins_lp_miskin_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_miskin_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_miskin_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_miskin_jenis(){
		if($this->validation_ins_lp_miskin_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->la->insert_miskin_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/miskin');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_miskin_jenis(){
		if($this->validation_ins_lp_miskin_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->la->update_miskin_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/miskin');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_miskin_jenis($id_jenis){
		$delete = $this->la->delete_miskin_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/miskin');
		}else {
			echo "fail";
		}
	}			
}
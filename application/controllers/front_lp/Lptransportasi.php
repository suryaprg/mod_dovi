<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lptransportasi extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_terminal', 'lt');

        $this->load->model('front_lp/Lp_kendaraan_kec', 'lkk');
        $this->load->model('front_lp/Lp_kendaraan_plat', 'lkp');
        $this->load->model('front_lp/Lp_kendaraan_umum', 'lku');

        $this->load->library("response_message");

	}


#===========================================================================================================================
#------------------------------------------------------------Terminal-------------------------------------------------------
#===========================================================================================================================
    
    public function terminal($th_fn){
        $jenis = $this->lt->get_lp_jenis();
        $row_jenis = 0;

        $data_graph = array();
        $str_tbl = "";
        $str_series = "";
        

        
        $str_header = "<thead>
                            <tr>
                                <th width=\"10%\">No</th>
                                <th width=\"30%\">Keterangan</th>
                                    ";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header .= "<th width=\"20%\">".$i."</th>";
            $t_col++;
        }
        $str_header .= "</tr></thead>";
                                        

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;

            // print_r("<pre>");
            // print_r($v_data);
            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"bottom: 30px;\" width=\"100%\">";

            $str_tbl .= $str_header;
            
            $sub_jenis = $this->lt->get_lp_sub_jenis(array("ltsj.id_jenis"=>$v_data->id_jenis));

            if(count($sub_jenis) != 0){
                foreach ($sub_jenis as $r_sub => $v_sub) {
                    $data["list_data"][$v_data->id_jenis][$v_sub->id_sub_jenis]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;
                    $data["list_data"][$v_data->id_jenis][$v_sub->id_sub_jenis]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                    
                    $data_graph[$v_data->id_jenis][$v_sub->id_sub_jenis]["caption"] = $v_sub->nama_sub_jenis;

                    $str_tbl .= "      
                                        <tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_sub_jenis."\">
                                            <td>-</td>
                                            <td><a href=\"#\">".$v_sub->nama_sub_jenis."</a></td>";
                    $row_th = 0;
                    for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                        $val_data = $this->lt->get_lp(array("lt.id_jenis"=>$v_data->id_jenis,
                                                            "lt.id_sub_jenis"=>$v_sub->id_sub_jenis,
                                                            "th"=>$i));
                        $data["list_data"][$v_data->id_jenis][$v_sub->id_sub_jenis]["value"][$i] = $val_data["jml"];

                        $data_graph[$v_data->id_jenis][$v_sub->id_sub_jenis]["val_data"][$row_th]["year"] = $i;
                        $data_graph[$v_data->id_jenis][$v_sub->id_sub_jenis]["val_data"][$row_th]["value"] = (float)$val_data["jml"];

                        $row_th++;
                        $str_tbl .= "           <td>".$val_data["jml"]."</td>";
                    
                    }
                }    
            }else{    
                    $data["list_data"][$v_data->id_jenis][0]["main"]["nama_sub_jenis"] = $v_data->nama_jenis;
                    $data["list_data"][$v_data->id_jenis][0]["main"]["id_sub_jenis"] = $v_data->id_jenis;
                    $data_graph[$v_data->id_jenis][0]["caption"] = $v_data->nama_jenis;

                    $str_tbl .= "      
                                        <tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_0\">
                                            <td>-</td>
                                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";
                    $row_th = 0;
                    for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                        $val_data = $this->lt->get_lp(array("lt.id_jenis"=>$v_data->id_jenis,
                                                            "lt.id_sub_jenis"=>0,
                                                            "th"=>$i));
                        $data["list_data"][$v_data->id_jenis][0]["value"][$i] = $val_data["jml"];

                        $data_graph[$v_data->id_jenis][0]["val_data"][$row_th]["year"] = $i;
                        $data_graph[$v_data->id_jenis][0]["val_data"][$row_th]["value"] = (float)$val_data["jml"];

                        $row_th++;
                        $str_tbl .= "           <td>".$val_data["jml"]."</td>";
                    
                    }
                
                
            }
            $row_jenis++;
            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $data["str_tbl"] = $str_tbl;
        $data["data_graph"] = json_encode($data_graph);
        $data["str_series"] = $str_series;

        $data["t_col"] = $t_col;

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table>".$str_tbl."</table>");

        $this->load->view("front_lp/transportasi/Lp_terminal",$data);
    }
#===========================================================================================================================
#------------------------------------------------------------Terminal-------------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Kecamatan--------------------------------------------------
#===========================================================================================================================
    public function kendaraan_kec($th_fn){
        $jenis = $this->lkk->get_lp_jenis();
        $kecamatan = $this->lkk->get_kec();
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Luas Penggunaan Lahan Sawah (hektar) menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------
        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis_kendaraan;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->keterangan;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis_kendaraan."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->keterangan."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis_kendaraan."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border='1' class=\"data-table\" width=\"100%\">";
            $str_tbl .= $str_header_mod;

            $t_data = array();

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis_kendaraan."_".$v_sub->id_kec."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kec."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lkk->get_lp(array("id_jenis_kendaraan"=>$v_data->id_jenis_kendaraan, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data["jml"]){
                        $str_tbl .= "<td>".$val_data["jml"]."</td>";
                        // $jml_panen = $val_data["jml_panen"];
                    }else{
                        $str_tbl .= "<td>-</td>";
                        // $jml_panen = 0;
                    }

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_data->id_jenis_kendaraan][$v_sub->id_kec][$row_th] = $main_data;
                    $row_th++;
                }
                
                $str_tbl .= "</tr>";
                $r_kec++;
            }

            $str_tbl .= "           
                                </table>
                            </td>
                        </tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kecamatan"] = $kecamatan;
        // $data["jml_th"] = $row_th;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");

        $this->load->view("front_lp/transportasi/Lp_kendaraan_kec",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Kecamatan--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Plat--------------------------------------------------
#===========================================================================================================================
    public function kendaraan_plat($th_fn){
        $jenis = $this->lkp->get_lp_jenis();
        
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\" rowspan=\"2\">No</td>
                                <td width=\"35%\" rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-1; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\" colspan='3'>".$i."</td>";

            $str_header_bot .= "<td>Hitam</td><td>Kuning</td><td>Merah</td>";
            $t_col += 3;
        }
        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Luas Penggunaan Lahan Sawah (hektar) menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------       
        
        $th_st = $th_fn-1;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) { 
                $val_data = $this->lkp->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;
                    if($val_data){
                        $str_tbl .= "<td>".$val_data["jml_hitam"]."</td>";
                        $str_tbl .= "<td>".$val_data["jml_kuning"]."</td>";
                        $str_tbl .= "<td>".$val_data["jml_merah"]."</td>";
                    }else{
                        $str_tbl .= "<td>-</td>";
                        $str_tbl .= "<td>-</td>";
                        $str_tbl .= "<td>-</td>";
                    }
                $main_data = array("year"=>(string)$i, "jml_hitam"=>(float)$val_data["jml_hitam"], "jml_kuning"=>(float)$val_data["jml_kuning"], "jml_merah"=>(float)$val_data["jml_merah"]);
                $data_graph[$v_data->id_jenis][$row_th] = $main_data;

                $row_th++;
            }

            $t_data = array();

            $str_tbl .= "</tr>";            
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;
        
        $data["data_graph"] = json_encode($data_graph);

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;
        // $data["str_series"] = $str_series;
        // $data["jml_th"] = $row_th;

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$data["str_header"]. $data["str_tbl"]."</table>");

        $this->load->view("front_lp/transportasi/lp_kendaraan_plat",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Plat--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Umum--------------------------------------------------
#===========================================================================================================================
    public function kendaraan_umum($th_fn){
        $jenis = $this->lku->get_lp_jenis();
        $kategori = $this->lku->get_lp_kategori();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Luas Penggunaan Lahan Sawah (hektar) menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->id_jenis_kendaraan;
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->keterangan;

            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis_kendaraan;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->keterangan;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis_kendaraan."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->keterangan."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis_kendaraan."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border='1' class=\"data-table\" width=\"100%\">";
            $str_tbl .= $str_header_mod;

            $t_data = array();

            $r_kec = 0;
            foreach ($kategori as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["id_kategori"] = $v_sub->id_jenis;
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["nama_kategori"] = $v_sub->nama_jenis;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis_kendaraan."_".$v_sub->id_jenis."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_jenis."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lku->get_lp(array("id_jenis_kendaraan"=>$v_data->id_jenis_kendaraan, "id_jenis"=> $v_sub->id_jenis, "th"=>$i));
                    $data["list_data"][$row_jenis]["kategori"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data["jml"]){
                        $str_tbl .= "<td>".$val_data["jml"]."</td>";
                        // $jml_panen = $val_data["jml_panen"];
                    }else{
                        $str_tbl .= "<td>-</td>";
                        // $jml_panen = 0;
                    }

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_data->id_jenis_kendaraan][$v_sub->id_jenis][$row_th] = $main_data;
                    $row_th++;
                }
                
                $str_tbl .= "</tr>";
                $r_kec++;
            }

            $str_tbl .= "           
                                </table>
                            </td>
                        </tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;
        $data["str_tbl"] = $str_tbl;
        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kategori"] = $kategori;
        // $data["jml_th"] = $row_th;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");

        // $this->load->view("front_lp/keuangan/tenaga",$data);
        $this->load->view("front_lp/transportasi/Lp_kendaraan_umum",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Umum--------------------------------------------------
#===========================================================================================================================


}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Rasio extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("admin_opd/Rasio_m", "rg");
		$this->load->library("response_message");
	}

	public function index(){
		$data["page"] 				= "rasio";
		$data["list_rasio"]			= $this->rg->get_rasio();
		$data["list_kecamatan"]		= $this->rg->get_kecamatan();

		// $data["list_pdb_jenis"] 	= $this->pd->get_pdb_jenis();
		$this->load->view('front_lp/penduduk/main_admin_penduduk', $data);
	}

	private function validation_ins_rasio(){
		$config_val_input = array(

			array(
                'field'=>'id_kec',
                'label'=>'id_kec',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),

			array(
                'field'=>'th',
                'label'=>'th',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
      
            array(
                'field'=>'jml_l',
                'label'=>'jml_l',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_p',
                'label'=>'jml_p',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'rasio',
                'label'=>'rasio',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_rasio(){
		if($this->validation_ins_rasio()){
			$id_kec 		= $this->input->post("id_kec");
			$th 		= $this->input->post("th");
			$jml_l 		= $this->input->post("jml_l");
			$jml_p 		= $this->input->post("jml_p");
			$rasio 		= $this->input->post("rasio");
			
			
			$data_insert = array(
							"id_penduduk"=>"",
							"id_kec"=>$id_kec,
							"th"=>$th,
							"jml_l"=>$jml_l,
							"jml_p"=>$jml_p,
							"rasio"=>$rasio
							
						);
			$insert = $this->rg->insert_rasio($data_insert);
				if($insert){
					redirect('admin/rasio');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_rasio(){
		$id_penduduk 	= $this->input->post("id_penduduk");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->rg->get_rasio_where(array("id_penduduk"=>$id_penduduk)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}
	public function update_lp_rasio(){
		if($this->validation_ins_rasio()){
			$id_penduduk 	= $this->input->post("id_penduduk");

			$id_kec		= $this->input->post("id_kec");
			$th		= $this->input->post("th");
			$jml_l		= $this->input->post("jml_l");
			$jml_p 	= $this->input->post("jml_p");
			$rasio 			= $this->input->post("rasio");
			

			$data = array(
						
						"id_kec"=>$id_kec,
						"th"=>$th,
						"jml_l"=>$jml_l,
						"jml_p"=>$jml_p,
						"rasio"=>$rasio
						
					);

			$data_where = array(
							"id_penduduk"=>$id_penduduk
						);

			$insert = $this->rg->update_rasio($data, $data_where);
				if($insert){
					redirect('admin/rasio');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_rasio($id_penduduk){
		$delete = $this->rg->delete_rasio(array("id_penduduk"=>$id_penduduk));
		if($delete){
			echo "del";
			redirect('admin/rasio');
		}else {
			echo "fail";
		}
	}	
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lphukum extends CI_Controller{

    public function __construct(){
        parent::__construct();  
        $this->load->model('front_lp/Lp_pidana', 'lp');
        $this->load->model('front_lp/Lp_pidana_kec', 'lpk');
        
        $this->load->library("response_message");

    }

#===========================================================================================================================
#------------------------------------------------------------Pidana---------------------------------------------------------
#===========================================================================================================================

    public function pidana($th_fn){
        $jenis = $this->lp->get_lp_jenis();

        $th_st = $th_fn-2;
        $str_tbl = "";

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td rowspan=\"2\">No</td>
                                <td rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"2\">".$i."</td>";
            $str_header_bot .= "<th>Lapor</th><th>Selesai</th>";
            $t_col += 2;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Data Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                        </tr>";


        //------------------------------------------------header-------------------------------------------------
        // $str_header .= "    </tr>";

        $data_graph = array();

        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";
            
            // $data_graph[$v_data->id_jenis]["caption"] = $v_data->nama_jenis;

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["lapor"]){
                    $str_tbl .= "<td>".$val_data["lapor"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                }

                if($val_data["selesai"]){
                    $str_tbl .= "<td>".$val_data["selesai"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                }

                $main_data = array("year"=>$i, "lapor"=>(int)$val_data["lapor"], "selesai"=>(int)$val_data["selesai"]);
                $data_graph[$v_data->id_jenis][$row_th] = $main_data;

                $row_th++;
                // $str_tbl .= "<td>".$val_data["selesai"]."</td>";
            }

            $str_tbl .= "</tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$str_header_top.$str_header_mod.$str_header_bot.$str_tbl."</table>");

        $this->load->view("front_lp/hukum/pidana", $data);
    }

#===========================================================================================================================
#------------------------------------------------------------Pidana---------------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#------------------------------------------------------------Pidana_Kec---------------------------------------------------------
#===========================================================================================================================

    public function pidana_kec($th_fn){
        $kecamatan = $this->lpk->get_lp_jenis();

        $th_st = $th_fn-2;

        $str_tbl = "";
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td rowspan=\"2\">No</td>
                                <td rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"2\">".$i."</td>";
            $str_header_bot .= "<th>Lapor</th><th>Selesai</th>";
            $t_col += 2;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Jumlah Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                        </tr>";


        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 0;
        foreach ($kecamatan as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_kec"] = $v_data->id_kec;
            $data["list_data"][$row_jenis]["jenis"]["nama_kec"] = $v_data->nama_kec;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_kec."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_kec."</a></td>";

            $row_th = 0;

            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpk->get_lp(array("id_kec"=>$v_data->id_kec, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_lapor"]){
                    $str_tbl .= "<td>".$val_data["jml_lapor"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                }

                if($val_data["jml_lapor"]){
                    $str_tbl .= "<td>".$val_data["jml_selesai"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                }

                $main_data = array("year"=>$i, "lapor"=>(int)$val_data["jml_lapor"], "selesai"=>(int)$val_data["jml_selesai"]);
                $data_graph[$v_data->id_kec][$row_th] = $main_data;

                $row_th++;
            }

            $row_jenis++;

            $str_tbl .= "</tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_lp/hukum/pidana_kecamatan", $data);
    }

#===========================================================================================================================
#------------------------------------------------------------Pidana_Kec---------------------------------------------------------
#===========================================================================================================================
}
?>
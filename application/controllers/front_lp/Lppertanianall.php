<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppertanianall extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_pertanian_all', 'lp');

        // $this->load->library("response_message");

	}

#===========================================================================================================================
#-----------------------------------------------------Pertanian All--------------------------------------------------
#===========================================================================================================================
    public function pertanian_all($id_jenis,$th_fn){
        $jenis_all = $this->lp->get_lp_jenis();


        $jenis =  $this->lp->get_lp_jenis();
        // $jenis = $this->lp->get_lp_jenis_filter(array("id_jenis"=>$id_jenis));
        $sub_jenis = $this->lp->get_lp_sub(array("id_jenis"=>$id_jenis));
        
        //------------------------------------------------header-------------------------------------------------

        $str_title = "Data Pendidikan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

        $str_header_mod = "<tr>
                                <td width=\"5%\" rowspan=\"2\">No</td>
                                <td width=\"25%\" rowspan=\"2\">Keterangan</td>
                                <td width=\"10%\" rowspan=\"2\">Satuan</td>";
        $str_header_mod_sec ="";

        $t_col = 3;
        for ($i=$th_fn-1; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td  colspan=\"3\">".$i."</td>";
            $str_header_mod_sec .= "<td width=\"10%\">Jumlah Panen</td>
                                    <td width=\"10%\">Luas Lahan</td>
                                    <td width=\"10%\">Produktivitas</td>";
            $t_col += 3;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-1;
        $data_graph = array();
        $str_tbl = "";

        $row_jenis = 0;

        $str_dropdown_ = "";

        foreach ($jenis as $r_jenis => $v_jenis) {
            $str_tbl = "";

            $data["list_data"][$v_jenis->id_jenis]["jenis"]["main"]["id_jenis"] = $v_jenis->id_jenis;
            $data["list_data"][$v_jenis->id_jenis]["jenis"]["main"]["nama_kategori"] = $v_jenis->nama_jenis;

            $str_tbl .= "<table class=\"data-table\" style=\"width: 100%;\">
                         <thead>
                          <tr>
                            <td colspan=\"".$t_col."\">Data ". $v_jenis->nama_jenis." di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          ".$str_header_mod.$str_header_mod_sec."
                         </thead>";

            // $data_graph[$v_kate->id_jenis]["main"]

            $row_sub = 0;
            foreach ($sub_jenis as $r_sub => $v_sub) {
                    $data["list_data"][$v_jenis->id_jenis]["jenis"]["sub_jenis"][$row_sub]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                    $data["list_data"][$v_jenis->id_jenis]["jenis"]["sub_jenis"][$row_sub]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;
                    
                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_jenis->id_jenis."_".$v_sub->id_sub_jenis."\">
                                    <td><a href=\"#\">".($row_sub+1)."</a></td>
                                    <td><a href=\"#\">".$v_sub->nama_sub_jenis."</a></td>
                                    <td>".$v_sub->satuan."</td>";
                    $row_th = 0;
                    for ($i=$th_st; $i <= $th_fn ; $i++) {
                        $val_data = $this->lp->get_lp(array("id_sub_jenis"=>$v_sub->id_sub_jenis,
                                                        "th"=>$i));

                        $luas = 0;
                        if($val_data["luas"]){
                            $luas = $val_data["luas"];
                            $str_tbl .= "<td>".$luas."</td>";
                        }else{
                            $luas = 0;
                            $str_tbl .= "<td>-</td>";
                        }

                        $produksi = 0;
                        if($val_data["jml"]){
                            $produksi = $val_data["jml"];
                            $str_tbl .= "<td>".$produksi."</td>";
                        }else{
                            $produksi = 0;
                            $str_tbl .= "<td>-</td>";
                        }

                        if($produksi != 0){
                            $produktivitas = $luas/$produksi;
                            $str_tbl .= "<td>".number_format($produktivitas, 5, ".", ",")."</td>";
                        }else{
                            $str_tbl .= "<td>0</td>";
                        }

                        $data_graph[$v_jenis->id_jenis][$v_sub->id_sub_jenis][$row_th]["year"] = (string)$i;
                        $data_graph[$v_jenis->id_jenis][$v_sub->id_sub_jenis][$row_th]["luas"] = (double)$luas;
                        $data_graph[$v_jenis->id_jenis][$v_sub->id_sub_jenis][$row_th]["produksi"] = (double)$produksi;
                        $data_graph[$v_jenis->id_jenis][$v_sub->id_sub_jenis][$row_th]["produktivitas"] = (double)$produktivitas;
                        
                        $row_th++;
                    }

                    $str_tbl .= "</tr>";
                $row_sub++;
            }

            $str_tbl .= "</table>";

            // $data["str_tbl"][$v_jenis->id_jenis] = (string)$str_tbl;

            $row_jenis++;   
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        
        // $data["str_series"] = $str_series;
        $data["jenis_all"] = $jenis_all;
        $data["str_header"] = $str_header_top;
        $data["title"] = $str_title;
        $data["id_jenis"] = $id_jenis;

        $data["str_tbl"] = $str_tbl;

        foreach ($jenis_all as $r_jenis_all => $v_jenis_all) {
            $str_dropdown_ .= "<option id=\"val_".$v_jenis_all->id_jenis."\" value=\"".$v_jenis_all->id_jenis."\">".$v_jenis_all->nama_jenis."</option>";    
        }
        
        $data["dropdown_"] = "<select name=\"get_kate\" id=\"get_kate\">".$str_dropdown_."</select>";

        // print_r("<pre>");
        // print_r($data);
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$str_tbl."</table>");

        $this->load->view("front_lp/pertanian/Lp_pertanian_all",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Pendidikan All--------------------------------------------------
#===========================================================================================================================
}
?>
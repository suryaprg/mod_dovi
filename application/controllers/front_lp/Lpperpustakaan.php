<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpperpustakaan extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_perpustakaan", "lp");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] 				= "perpustakaan";
		$data["list_perpus"] 		= $this->lp->get_perpus();
		$data["list_perpus_jenis"] 	= $this->lp->get_perpus_jenis();
		$this->load->view('front_lp/pendidikan/main_admin_pendidikan', $data);
	}

//================================================ LP PERPUS ====================================================//
	private function validation_ins_lp_perpus(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_perpus(){
		if($this->validation_ins_lp_perpus()){
			$id_jenis 	= $this->input->post("id_jenis");
			$th 		= $this->input->post("th");
			$jml 		= $this->input->post("jml");
			
			$data_insert = array(
							"id_perpustakaan"=>"",
							"id_jenis"=>$id_jenis,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->lp->insert_perpus($data_insert);
				if($insert){
					redirect('admin/perpustakaan');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_perpus(){
		$id_perpustakaan = $this->input->post("id_perpustakaan");
		$data["status"]  = false;
		$data["val"] 	 = null;
		
		$data_main = $this->lp->get_perpus_where(array("id_perpustakaan"=>$id_perpustakaan)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_perpus(){
		if($this->validation_ins_lp_perpus()){
			$id_perpustakaan	= $this->input->post("id_perpustakaan");

			$id_jenis 			= $this->input->post("id_jenis");
			$th 				= $this->input->post("th");
			$jml 				= $this->input->post("jml");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"jml"=>$jml
					);

			$data_where = array(
							"id_perpustakaan"=>$id_perpustakaan,
						);

			$insert = $this->lp->update_perpus($data, $data_where);
				if($insert){
					redirect('admin/perpustakaan');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}
	}

	public function delete_lp_perpus($id_perpustakaan){
		$delete = $this->lp->delete_perpus(array("id_perpustakaan"=>$id_perpustakaan));
		if($delete){
			redirect('admin/perpustakaan');
			echo "del";
		}else {
			echo "fail";
		}
	}
//================================================ LP PERPUS ====================================================//

//================================================ LP PERPUS JENIS ==============================================//
	private function validation_ins_lp_perpus_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_perpus_jenis(){
		if($this->validation_ins_lp_perpus_jenis()){
			$nama_jenis  = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->lp->insert_perpus_jenis($data_insert);
				if($insert){
					redirect('admin/perpustakaan');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_perpus_jenis(){
		$id_jenis 		= $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->lp->get_perpus_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_perpus_jenis(){
		if($this->validation_ins_lp_perpus_jenis()){
			$id_jenis	= $this->input->post("id_jenis");

			$nama_jenis	= $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->lp->update_perpus_jenis($data, $data_where);
				if($insert){
					redirect('admin/perpustakaan');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}
	}

	public function delete_lp_perpus_jenis($id_jenis){
		$delete = $this->lp->delete_perpus_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			redirect('admin/perpustakaan');
			echo "del";
		}else {
			echo "fail";
		}
	}
//================================================ LP PERPUS JENIS ==============================================//			
}
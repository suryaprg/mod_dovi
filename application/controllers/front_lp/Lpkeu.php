<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkeu extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_keu", "lu");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }

	}

	public function index(){
		$data["page"] 				= "keuangan";
		$data["list_keu"] 			= $this->lu->get_keu();
		$data["list_keu_jenis"] 	= $this->lu->get_keu_jenis();
		$data["list_keu_sub_jenis"] = $this->lu->get_keu_sub_jenis();
		$this->load->view('front_lp/keuangan/main_admin_keuangan', $data);
	}

//================================================ LP KEUANGAN ==================================================//
	private function validation_ins_lp_keu(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'id_sub_jenis',
                'label'=>'Sub Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_keu(){
		if($this->validation_ins_lp_keu()){
			$id_jenis 		= $this->input->post("id_jenis");
			$id_sub_jenis 	= $this->input->post("id_sub_jenis");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");
			
			$data_insert = array(
							"id_keu"=>"",
							"id_jenis"=>$id_jenis,
							"id_sub_jenis"=>$id_sub_jenis,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->lu->insert_keu($data_insert);
				if($insert){
					redirect('admin/keuangan');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_keu(){
		$id_keu 		= $this->input->post("id_keu");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main =$this->lu->get_keu_where(array("id_keu"=>$id_keu)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_keu(){
		if($this->validation_ins_lp_keu()){
			$id_keu 		= $this->input->post("id_keu");

			$id_jenis 		= $this->input->post("id_jenis");
			$id_sub_jenis 	= $this->input->post("id_sub_jenis");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"id_sub_jenis"=>$id_sub_jenis,
						"th"=>$th,
						"jml"=>$jml
					);

			$data_where = array(
							"id_keu"=>$id_keu,
						);

			$insert = $this->lu->update_keu($data, $data_where);
				if($insert){
					redirect('admin/keuangan');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_keu($id_keu){
		$delete = $this->lu->delete_keu(array("id_keu"=>$id_keu));
		if($delete){
			echo "del";
			redirect('admin/keuangan');
		}else {
			echo "fail";
		}
	}
//================================================ LP KEUANGAN ==================================================//

//================================================ LP KEUANGAN JENIS ==================================================//
	private function validation_ins_lp_keu_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            ),    
            array(
                'field'=>'kategori',
                'label'=>'Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )    
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_keu_jenis(){
		$id_jenis 		= $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main =$this->lu->get_keu_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_keu_jenis(){
		if($this->validation_ins_lp_keu_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			$kategori 	= $this->input->post("kategori");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis,
							"kategori"=>$kategori

						);
			$insert = $this->lu->insert_keu_jenis($data_insert);
				if($insert){
					redirect('admin/keuangan');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_keu_jenis(){
		if($this->validation_ins_lp_keu_jenis()){
			$id_jenis 	= $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");
			$kategori 	= $this->input->post("kategori");

			$data = array(
						
						"nama_jenis"=>$nama_jenis,
						"kategori"=>$kategori
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->lu->update_keu_jenis($data, $data_where);
				if($insert){
					redirect('admin/keuangan');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_keu_jenis($id_jenis){
		$delete = $this->lu->delete_keu_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/keuangan');
		}else {
			echo "fail";
		}
	}	
//================================================ LP KEUANGAN JENIS ==================================================//		
//================================================ LP KEUANGAN SUB JENIS ===============================================//
	private function validation_ins_lp_keu_sub_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_sub_jenis',
                'label'=>'Nama Sub Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            ),
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_keu_sub_jenis(){
		$id_sub_jenis 	= $this->input->post("id_sub_jenis");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main =$this->lu->get_keu_sub_jenis_where(array("id_sub_jenis"=>$id_sub_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_keu_sub_jenis(){
		if($this->validation_ins_lp_keu_sub_jenis()){
			$nama_sub_jenis = $this->input->post("nama_sub_jenis");
			$id_jenis 		= $this->input->post("id_jenis");

			$data_insert = array(
							"id_sub_jenis"=>"",
							"id_jenis"=>$id_jenis,
							"nama_sub_jenis"=>$nama_sub_jenis

						);
			$insert = $this->lu->insert_keu_sub_jenis($data_insert);
				if($insert){
					redirect('admin/keuangan');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_keu_sub_jenis(){
		if($this->validation_ins_lp_keu_sub_jenis()){
			$id_sub_jenis 	= $this->input->post("id_sub_jenis");
			$id_jenis 		= $this->input->post("id_jenis");
			$nama_sub_jenis = $this->input->post("nama_sub_jenis");

			$data = array(
						
						"nama_sub_jenis"=>$nama_sub_jenis,
						"id_jenis"=>$id_jenis
					);

			$data_where = array(
							"id_sub_jenis"=>$id_sub_jenis,
						);

			$insert = $this->lu->update_keu_sub_jenis($data, $data_where);
				if($insert){
					redirect('admin/keuangan');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_keu_sub_jenis($id_sub_jenis){
		$delete = $this->lu->delete_keu_sub_jenis(array("id_sub_jenis"=>$id_sub_jenis));
		if($delete){
			echo "del";
			redirect('admin/keuangan');
		}else {
			echo "fail";
		}
	}		
//================================================ LP KEUANGAN SUB JENIS ===============================================//	
}
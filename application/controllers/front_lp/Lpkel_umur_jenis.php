<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkel_umur_jenis extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_kel_umur", "lku");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		// $data["page"] = "iklim";
		// $data["list_umur"] = $this->lku->get_umur();
		$data["list_umur_jenis"] = $this->lku->get_umur_jenis();
		$this->load->view('admin_data/geo/v_lpkel_umur_jenis', $data);
	}

	private function validation_ins_lp_umur_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}	

	public function insert_lp_umur_jenis(){
		if($this->validation_ins_lp_umur_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->lku->insert_umur_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin_opd/geo/Lpkel_umur_jenis','refresh');
		}else {
			print_r(validation_errors());
		}
	}	

	public function index_up_lp_umur_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->lku->get_umur_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_umur_jenis(){
		if($this->validation_ins_lp_umur_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->lku->update_umur_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin_opd/geo/Lpkel_umur_jenis','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_umur_jenis($id_jenis){
		$delete = $this->lku->delete_umur_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin_opd/geo/Lpkel_umur_jenis','refresh');
		}else {
			echo "fail";
		}
	}
}
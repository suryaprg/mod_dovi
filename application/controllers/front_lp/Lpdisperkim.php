<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpdisperkim extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_disperkim', 'lp');
        $this->load->model('front_lp/Lp_disperkim_pohon', 'lpp');
        $this->load->model('front_lp/Lp_disperkim_taman', 'lpt');

        $this->load->model('front_lp/Lp_disperkim_all', 'lpa');

	}
    
    public function get_psu(){
        $val_data = $this->lp->get_dsu();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data PSU yang di Serahkan Kepada Pemerintah Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"23%\">Lokasi</td>
                                <td width=\"23%\">Kawasan</td>
                                <td width=\"23%\">Pengembang</td>
                                <td width=\"23%\">Tanggal</td>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title."</td>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td>".($row_data+1)."</td>
                            <td>".$v_data->lokasi."</td>
                            <td>".$v_data->nama_kawasan."</td>
                            <td>".$v_data->pengembang."</td>
                            <td>".$v_data->tgl."</td>
                        </tr>";

        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;
    }

    public function get_csr(){
        $val_data = $this->lp->get_csr();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Pemberi CSR di Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"19%\">Tanggal dan No. Bash</td>
                                <td width=\"19%\">Tanggal dan No. NPHD</td>
                                <td width=\"19%\">Objek Pembangunan</td>
                                <td width=\"19%\">Pemberi CSR</td>
                                <td width=\"19%\">RAB</td>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"6\">".$str_title."</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td>".($row_data+1)."</td>
                            <td>tgl : ".$v_data->tgl_bash." <br><br> No.Bash: ".$v_data->no_bash."</td>
                            <td>tgl : ".$v_data->tgl_nphd." <br><br> No.NPHD: ".$v_data->no_nphd."</td>
                            <td>".$v_data->obj_pembangunan."</td>
                            <td>".$v_data->pemberi_csr."</td>
                            <td>".$v_data->rab."</td>
                        </tr>";

        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;

//        print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_lp/disperkim/csr", $data);
    }

    public function get_pohon(){
        
        $val_data = $this->lpp->get_lp();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Penanaman Pohon di Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"19%\">Tanggal</td>
                                <td width=\"19%\">Jenis Tanaman</td>
                                <td width=\"19%\">Jumlah Bibit</td>
                                <td width=\"19%\">Lokasi</td>
                                <td width=\"19%\">Keterangan</td>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"6\">".$str_title."</td>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";

        $data_graph = array();
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td>".($row_data+1)."</td>
                            <td>".$v_data->tgl."</td>
                            <td>".$v_data->nama_jenis."</td>
                            <td>".$v_data->jml."</td>
                            <td>".$v_data->lokasi."</td>
                            <td>".$v_data->keterangan."</td>
                        </tr>";

        }

        $data["data_graph"] = json_encode($data_graph);

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;
    }

    public function get_taman(){
        
        $val_data = $this->lpt->get_lp();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Penanaman Pohon di Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"19%\">Tanggal</td>
                                <td width=\"19%\">Jenis Tanaman</td>
                                <td width=\"19%\">Jumlah Bibit</td>
                                <td width=\"19%\">Lokasi</td>
                                <td width=\"19%\">Keterangan</td>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"6\">".$str_title."</td>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";

        $data_graph = array();
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td>".($row_data+1)."</td>
                            <td>".$v_data->tgl."</td>
                            <td>".$v_data->nama_jenis."</td>
                            <td>".$v_data->jml."</td>
                            <td>".$v_data->lokasi."</td>
                            <td>".$v_data->keterangan."</td>
                        </tr>";

        }

        $data["data_graph"] = json_encode($data_graph);

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;
    }
    
    public function get_disperkim_all($th_fn){
        $kategori = $this->lpa->get_lp_kategori();
        $th_st = $th_fn-1;

        $row_kate = 0;
        
        $list_data = array();
        foreach ($kategori as $r_kate => $v_kate) {
            $data["list_data"][$row_kate]["main"]["id_kategori"] = $v_kate->id_kategori;
            $data["list_data"][$row_kate]["main"]["nama_kategori"] = $v_kate->nama_kategori;            

            $jenis = $this->lpa->get_lp_jenis(array("id_kategori"=>$v_kate->id_kategori));
            $row_jenis = 0;
            foreach ($jenis as $r_jenis => $v_jenis) {
                $data["list_data"][$row_kate]["jenis"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
                $data["list_data"][$row_kate]["jenis"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis; 
                
                $sub_jenis = $this->lpa->get_lp_sub_jenis(array("id_jenis"=>$v_jenis->id_jenis));
                $row_sub = 0;
                foreach ($sub_jenis as $r_sub => $v_sub) {
                    $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][$row_sub]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                    $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][$row_sub]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis; 

                    $row_th = 0;
                    for ($i=$th_st; $i < $th_fn; $i++) { 
                        $val_data = $this->lpa->get_lp(array("id_jenis"=>$v_jenis->id_jenis, "id_sub_jenis"=>$v_sub->id_sub_jenis, "th"=>$i));

                        $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][$row_sub]["detail"][$row_th] = $val_data;

                        $row_th++;
                    }

                    $row_sub++;
                }

                $row_jenis++;
            }

            $row_kate++;
        }

        print_r("<pre>");
        print_r($data);

    }
    

}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkeuangan extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_keuangan', 'lk');
        $this->load->model('front_lp/Lp_koperasi', 'lkop');

        $this->load->model('front_lp/Lp_tenaga', 'lt');

        $this->load->model('front_lp/Lp_pend_ind', 'lpi');
        
        $this->load->library("response_message");

	}


#===========================================================================================================================
#------------------------------------------------------------Keuangan-------------------------------------------------------
#===========================================================================================================================
    
    public function index($th_fn){
        $th_st = $th_fn - 2;

        $keuangan_jenis = $this->lk->get_lp_keu_jenis();
        $data["keuangan"] = array();
        foreach ($keuangan_jenis as $r_data => $v_data) {
            $data["keuangan"][$v_data->id_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["keuangan"][$v_data->id_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $data_sub_jenis = $this->lk->get_lp_keu_sub_jenis(array("lksj.id_jenis"=>$v_data->id_jenis));

            foreach ($data_sub_jenis as $r_data_sub => $v_data_sub) {
                $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["nama_sub_jenis"] = $v_data_sub->nama_sub_jenis;

                $row_graph = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) { 
                    $data_keu = $this->lk->get_lp_keu(array("lk.id_jenis"=>$v_data->id_jenis,
                                                                    "lk.id_sub_jenis"=>$v_data_sub->id_sub_jenis,
                                                                    "lk.th"=>$i
                                                            )
                    
                                                    );
                    $jml = $data_keu["jml"];
                    $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["value"][$i] = $jml;

                    $tmp_array = array("year"=>$i, "value"=>0);

                    if($jml){
                        $tmp_array["value"] = (int)$jml;
                    }
                    $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["data_graph"][$row_graph] = $tmp_array;
                    
                    $row_graph++;
                }
            }
        }

        // print_r("<pre>");
        // print_r(json_encode($data));
        $data["json_data"] = json_encode($data["keuangan"]);
        $this->load->view("front_lp/Lp_keuangan",$data);
    }

    public function index_ne($th_fn){
        $th_st = $th_fn - 2;

        $keuangan_jenis = $this->lk->get_lp_keu_jenis();
        $data["keuangan"] = array();
        foreach ($keuangan_jenis as $r_data => $v_data) {
            // print_r($v_data);
            $data["keuangan"][$v_data->id_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["keuangan"][$v_data->id_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;
            $data["keuangan"][$v_data->id_jenis]["jenis"]["kategori"] = $v_data->kategori;

            $data_sub_jenis = $this->lk->get_lp_keu_sub_jenis(array("lksj.id_jenis"=>$v_data->id_jenis));

            foreach ($data_sub_jenis as $r_data_sub => $v_data_sub) {
                $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["nama_sub_jenis"] = $v_data_sub->nama_sub_jenis;
                $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["id_sub_jenis"] = $v_data_sub->id_sub_jenis;

                $row_graph = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) { 
                    $data_keu = $this->lk->get_lp_keu(array("lk.id_jenis"=>$v_data->id_jenis,
                                                                    "lk.id_sub_jenis"=>$v_data_sub->id_sub_jenis,
                                                                    "lk.th"=>$i
                                                            )
                    
                                                    );
                    $jml = $data_keu["jml"];
                    $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["value"][$i] = $jml;

                    $tmp_array = array("year"=>$i, "val_jml"=>0);

                    if($jml){
                        $tmp_array["val_jml"] = (int)$jml;
                    }
                    $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["data_graph"][$row_graph] = $tmp_array;
                    
                    $row_graph++;
                }
            }
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $data["json_data"] = json_encode($data["keuangan"]);

        // print_r("<pre>");
        // print_r($data);

        
        $this->load->view("front_lp/Lp_keuangan_e",$data);
    }

    public function keuangan_get($id_jenis,$th_fn){
        $th_st = $th_fn - 2;
        $data = array();

        $str_tb = "";

        $keuangan_jenis = $this->lk->get_lp_keu_jenis_where(array("id_jenis"=>$id_jenis));
        foreach ($keuangan_jenis as $r_data => $v_data) {

            $data_sub_jenis = $this->lk->get_lp_keu_sub_jenis(array("lksj.id_jenis"=>$v_data->id_jenis));

            $no_item = 0;
            foreach ($data_sub_jenis as $r_data_sub => $v_data_sub) {
                $data["sub_jenis"][$no_item]["sub_jenis"]["nama"] = $v_data_sub->nama_sub_jenis;
                $data["sub_jenis"][$no_item]["sub_jenis"]["id_sub_jenis"] = $v_data_sub->id_sub_jenis;

                $str_tb .= "<tr class=\"sub_jenis\" id=\"sub_".$v_data_sub->id_sub_jenis."\">
                                <td width=\"5%\">".($no_item+1)."</td>
                                <td width=\"35%\">".$v_data_sub->nama_sub_jenis."</td>";

                $row_graph = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) { 
                    $data_keu = $this->lk->get_lp_keu(array("lk.id_jenis"=>$v_data->id_jenis,
                                                                    "lk.id_sub_jenis"=>$v_data_sub->id_sub_jenis,
                                                                    "lk.th"=>$i
                                                            )
                                                    );

                    $jml = 0;
                    if($data_keu["jml"]){
                        $jml = (int)$data_keu["jml"];
                    }

                    $data["sub_jenis"][$no_item]["value"][$i] = $jml;

                    $tmp_array = array("year"=>$i, "value"=>$jml);
                    $data["sub_jenis"][$no_item]["data_graph"][$row_graph] = $tmp_array;
                    

                    $str_tb .= "<td width=\"20%\" align=\"right\">".number_format($jml, 2, ".", ",")."</td>";
                    $row_graph++;
                }

                $str_tb .= "</tr>";
                $no_item++;
            }
        }


        $data["str_tb"] = $str_tb;
        // print_r("<pre>");
        // print_r($str_tb);
        print_r(json_encode($data));
        // $data["json_data"] = json_encode($data["keuangan"]);
        // $this->load->view("front_lp/Lp_keuangan",$data);
    }
#===========================================================================================================================
#------------------------------------------------------------Keuangan-------------------------------------------------------
#===========================================================================================================================


#===========================================================================================================================
#------------------------------------------------------------Koperasi-------------------------------------------------------
#===========================================================================================================================

    public function koperasi($th_fn){
        $jenis = $this->lkop->get_lp_jenis()->result();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Luas Penggunaan Lahan Sawah (hektar) menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 0;
        $str_tbl = "";

        $data_graph = array();
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_data->id_jenis;
            
            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border='1' class=\"data-table\" width=\"100%\">";
            $str_tbl .= $str_header_mod;

            $sub_jenis = $this->lkop->get_lp_jenis_where(array("id_jenis"=>$v_data->id_jenis))->result();
            if(count($sub_jenis)){
                
                $row_sub = 0;
                foreach ($sub_jenis as $r_sub => $v_sub) {
                    $data["list_data"][$row_jenis]["main_sub"][$row_sub]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;
                    $data["list_data"][$row_jenis]["main_sub"][$row_sub]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;

                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_sub_jenis."\">
                                <td><a href=\"#\">".($row_sub+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_sub_jenis."</a></td>";
                    
                    $row_th = 0;
                    for ($i=$th_fn-2; $i <= $th_fn ; $i++) {
                        $val_data = $this->lkop->get_lp(array("id_sub_jenis"=>$v_sub->id_sub_jenis, "th"=>$i));
                        $data["list_data"][$row_jenis]["main_sub"][$row_sub]["value"][$i] = $val_data["jml"];

                        $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                        $data_graph[$v_data->id_jenis][$v_sub->id_sub_jenis][$row_th] = $main_data;

                        $str_tbl .= "<td align=\"right\">".number_format($val_data["jml"], 2, ".", ",")."</td>";
                        $row_th++;
                    }
                    // $data["list_data"][$row_jenis]["value"];

                    $str_tbl .= "</tr>";

                    $row_sub++;
                }
            }else{
                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_0\">
                                <td><a href=\"#\">1</a></td>
                                <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

                $row_th = 0;
                for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                    $val_data = $this->lkop->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                    $data["list_data"][$row_jenis]["value"][$i] = $val_data["jml"];

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_data->id_jenis][0][$row_th] = $main_data;

                    $str_tbl .= "<td align=\"right\">".number_format($val_data["jml"], 2, ".", ",")."</td>";

                    $row_th++;
                }

                $str_tbl .= "</tr>";
            }

            $str_tbl .= "           
                                </table>
                            </td>
                        </tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;
        
        $data["data_graph"] = json_encode($data_graph);

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r($data_graph);

        // print_r("<table border='1'>".$str_tbl."</table>");
        $this->load->view("front_lp/keuangan/koperasi",$data);
    }
#===========================================================================================================================
#------------------------------------------------------------Koperasi-------------------------------------------------------
#===========================================================================================================================


#===========================================================================================================================
#------------------------------------------------------------Tenaga-------------------------------------------------------
#===========================================================================================================================

    public function tenaga($th_fn){
        $jenis = $this->lt->get_lp_jenis();
        $kategori = $this->lt->get_lp_kategori();

        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";
        
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

            $row_kategori = 0;
            foreach ($kategori as $r_kate => $v_kate) {
                $data["list_data"][$row_jenis]["kategori"][$row_kategori]["main"]["nama_kategori"] = $v_kate->nama_kategori;
                $data["list_data"][$row_jenis]["kategori"][$row_kategori]["main"]["id_kategori"] = $v_kate->id_kategori;

                $str_series = "";
                $row_th = 0;
                for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 

                    $str_series.= "createSeries(\"val_".$row_th."\", \"".$i."\");";

                    $val_data = $this->lt->get_lp_tenaga(array("id_jenis"=>$v_data->id_jenis, "id_kategori"=> $v_kate->id_kategori, "th"=>$i));
                    $data["list_data"][$row_jenis]["kategori"][$row_kategori]["value"][$i] = $val_data["jml"];

                    if($val_data){
                        // $str_tbl .= "<td>".$val_data["jml"]."</td>";
                        $str_tbl .= "<td>".number_format($val_data["jml"],2,".",",")."</td>";
                    }else{
                        $str_tbl .= "<td>-</td>";
                    }

                    $data_graph[$v_data->id_jenis][$row_kategori]["year"] = $v_kate->nama_kategori;
                    $data_graph[$v_data->id_jenis][$row_kategori]["val_".$row_th] = (float)$val_data["jml"];
                    

                    $row_th++;

                    

                }

                $row_kategori++;
            }

            $row_jenis++;
            $str_tbl .= "</tr>";
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;
        $data["str_tbl"] = $str_tbl;
        $data["graph_data"] = json_encode($data_graph);
        $data["str_series"] = $str_series;
        $data["kategori"] = $kategori;
        $data["jml_th"] = $row_th;

        // print_r("<pre>");
        // print_r($data["graph_data"]);
        // print_r("<table>".$str_tbl."</table>");

        $this->load->view("front_lp/keuangan/tenaga",$data);
    }
#===========================================================================================================================
#------------------------------------------------------------Tenaga-------------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#--------------------------------------------------------Pendapatan Industri------------------------------------------------
#===========================================================================================================================
    public function pendapatan_ind($th_fn){
        $jenis = $this->lpi->get_lp_jenis();
        $kategori = $this->lpi->get_lp_kategori();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Rekapan Pendataan Industri menurut Jenis Industri di Kota Malang di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td colspan=\"".($t_col-1)."\">Jenis Industri</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table class=\"data-table\" width=\"100%\">";
            $str_tbl .= $str_header_mod;     

            $t_data = array();

            $r_kec = 0;
            foreach ($kategori as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["id_kategori"] = $v_sub->id_kategori;
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["nama_kategori"] = $v_sub->nama_kategori;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kategori."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kategori."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lpi->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kategori"=> $v_sub->id_kategori, "th"=>$i));
                    $data["list_data"][$row_jenis]["kategori"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data["jml"]){
                        $str_tbl .= "<td>".$val_data["jml"]."</td>";
                        // $jml_panen = $val_data["jml_panen"];
                    }else{
                        $str_tbl .= "<td>-</td>";
                        // $jml_panen = 0;
                    }

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_data->id_jenis][$v_sub->id_kategori][$row_th] = $main_data;
                    $row_th++;
                }
                $str_tbl .= "</tr>";
                
                $r_kec++;
            }
            $str_tbl .= "           
                                </table>
                            </td>
                        </tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kategori"] = $kategori;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;
        
        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_lp/keuangan/pend_industri",$data);
    }
#===========================================================================================================================
#--------------------------------------------------------Pendapatan Industri------------------------------------------------
#===========================================================================================================================

}
?>
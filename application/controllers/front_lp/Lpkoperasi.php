<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkoperasi extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_koperasi", "lp");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }

	}

	public function index(){
		$data["page"] = "koperasi";
		$data["list_koperasi"] 			 = $this->lp->get_koperasi();
		$data["list_koperasi_jenis"] 	 = $this->lp->get_koperasi_jenis();
		$data["list_koperasi_sub_jenis"] = $this->lp->get_koperasi_sub_jenis();
		$this->load->view('front_lp/keuangan/main_admin_keuangan', $data);
	}

//================================================ LP KOPERASI ===============================================//
	private function validation_ins_lp_koperasi(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'id_sub_jenis',
                'label'=>'Sub Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_koperasi(){
		if($this->validation_ins_lp_koperasi()){
			$id_jenis 		= $this->input->post("id_jenis");
			$id_sub_jenis 	= $this->input->post("id_sub_jenis");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");
			
			$data_insert = array(
							"id_koperasi"=>"",
							"id_jenis"=>$id_jenis,
							"id_sub_jenis"=>$id_sub_jenis,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->lp->insert_koperasi($data_insert);
				if($insert){
					redirect('admin/koperasi');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_koperasi(){
		$id_koperasi 	= $this->input->post("id_koperasi");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main =$this->lp->get_koperasi_where(array("id_koperasi"=>$id_koperasi)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_koperasi(){
		if($this->validation_ins_lp_koperasi()){
			$id_koperasi 	= $this->input->post("id_koperasi");

			$id_jenis 		= $this->input->post("id_jenis");
			$id_sub_jenis 	= $this->input->post("id_sub_jenis");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"id_sub_jenis"=>$id_sub_jenis,
						"th"=>$th,
						"jml"=>$jml
					);

			$data_where = array(
							"id_koperasi"=>$id_koperasi,
						);

			$insert = $this->lp->update_koperasi($data, $data_where);
				if($insert){
					redirect('admin/koperasi');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_koperasi($id_koperasi){
		$delete = $this->lp->delete_koperasi(array("id_koperasi"=>$id_koperasi));
		if($delete){
			echo "del";
			redirect('admin/koperasi');
		}else {
			echo "fail";
		}
	}
//================================================ LP KOPERASI ===============================================//

//================================================ LP KOPERASI JENIS =========================================//
	private function validation_ins_lp_koperasi_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_koperasi_jenis(){
		$id_jenis 		= $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main =$this->lp->get_koperasi_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_koperasi_jenis(){
		if($this->validation_ins_lp_koperasi_jenis()){
			$nama_jenis  = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis

						);
			$insert = $this->lp->insert_koperasi_jenis($data_insert);
				if($insert){
					redirect('admin/koperasi');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_koperasi_jenis(){
		if($this->validation_ins_lp_koperasi_jenis()){
			$id_jenis 	= $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->lp->update_koperasi_jenis($data, $data_where);
				if($insert){
					redirect('admin/koperasi');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_koperasi_jenis($id_jenis){
		$delete = $this->lp->delete_koperasi_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/koperasi');
		}else {
			echo "fail";
		}
	}
//================================================ LP KOPERASI JENIS =========================================//

//================================================ LP KOPERASI SUB JENIS =====================================//
	private function validation_ins_lp_koperasi_sub_jenis(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            ),
            array(
                'field'=>'nama_sub_jenis',
                'label'=>'Nama Sub Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_koperasi_sub_jenis(){
		$id_sub_jenis 	= $this->input->post("id_sub_jenis");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main =$this->lp->get_koperasi_sub_jenis_where(array("id_sub_jenis"=>$id_sub_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_koperasi_sub_jenis(){
		if($this->validation_ins_lp_koperasi_sub_jenis()){
			$id_jenis 		= $this->input->post("id_jenis");
			$nama_sub_jenis = $this->input->post("nama_sub_jenis");
			
			$data_insert = array(
							"id_sub_jenis"=>"",
							"id_jenis"=>$id_jenis,
							"nama_sub_jenis"=>$nama_sub_jenis

						);
			$insert = $this->lp->insert_koperasi_sub_jenis($data_insert);
				if($insert){
					redirect('admin/koperasi');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_koperasi_sub_jenis(){
		if($this->validation_ins_lp_koperasi_sub_jenis()){
			$id_sub_jenis 	= $this->input->post("id_sub_jenis");

			$id_jenis 		= $this->input->post("id_jenis");
			$nama_sub_jenis = $this->input->post("nama_sub_jenis");

			$data = array(
						
						"id_jenis"=>$id_jenis,						
						"nama_sub_jenis"=>$nama_sub_jenis
					);

			$data_where = array(
							"id_sub_jenis"=>$id_sub_jenis,
						);

			$insert = $this->lp->update_koperasi_sub_jenis($data, $data_where);
				if($insert){
					redirect('admin/koperasi');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_koperasi_sub_jenis($id_sub_jenis){
		$delete = $this->lp->delete_koperasi_sub_jenis(array("id_sub_jenis"=>$id_sub_jenis));
		if($delete){
			echo "del";
			redirect('admin/koperasi');
		}else {
			echo "fail";
		}
	}	
//================================================ LP KOPERASI SUB JENIS =====================================//		
}
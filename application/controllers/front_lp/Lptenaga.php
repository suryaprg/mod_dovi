<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lptenaga extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_tenaga", "lpt");
		$this->load->library("response_message");
				if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"]				 	= "tenaga";
		$data["list_tenaga"] 			= $this->lpt->get_tenaga();
		$data["list_tenaga_jenis"] 		= $this->lpt->get_tenaga_jenis();
		$data["list_tenaga_kategori"] 	= $this->lpt->get_tenaga_kategori();
		$this->load->view('front_lp/keuangan/main_admin_keuangan', $data);
	}

//================================================ LP TENAGA ====================================================//
	private function validation_ins_lp_tenaga(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'id_kategori',
                'label'=>'Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_tenaga(){
		if($this->validation_ins_lp_tenaga()){
			$id_jenis	 	= $this->input->post("id_jenis");
			$id_kategori	= $this->input->post("id_kategori");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");
			
			$data_insert = array(
							"id_tenaga"=>"",
							"id_jenis"=>$id_jenis,
							"id_kategori"=>$id_kategori,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->lpt->insert_tenaga($data_insert);
				if($insert){
					redirect('admin/tenaga');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_tenaga(){
		$id_tenaga 			= $this->input->post("id_tenaga");
		$data["status"] 	= false;
		$data["val"] 		= null;
		
		$data_main 			= $this->lpt->get_tenaga_where(array("id_tenaga"=>$id_tenaga)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_tenaga(){
		if($this->validation_ins_lp_tenaga()){
			$id_tenaga 		= $this->input->post("id_tenaga");

			$id_jenis 		= $this->input->post("id_jenis");
			$id_kategori 	= $this->input->post("id_kategori");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");

			$data = array(
						
						"id_jenis"		=> $id_jenis,
						"id_kategori"	=> $id_kategori,
						"th"			=> $th,
						"jml"			=> $jml
					);

			$data_where = array(
							"id_tenaga"	=>$id_tenaga,
						);

			$insert = $this->lpt->update_tenaga($data, $data_where);
				if($insert){
					redirect('admin/tenaga');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_tenaga($id_tenaga){
		$delete = $this->lpt->delete_tenaga(array("id_tenaga"=>$id_tenaga));
		if($delete){
			echo "del";
			redirect('admin/tenaga');
		}else {
			echo "fail";
		}
	}
//================================================ LP TENAGA ====================================================//

//================================================ LP TENAGA JENIS ==============================================//
	private function validation_ins_lp_tenaga_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_tenaga_jenis(){
		$id_jenis 				= $this->input->post("id_jenis");
		$data["status"] 		= false;
		$data["val"] 			= null;
		
		$data_main			 	= $this->lpt->get_tenaga_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] 	= true;
			$data["val"] 		= $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_tenaga_jenis(){
		if($this->validation_ins_lp_tenaga_jenis()){
			$nama_jenis 	= $this->input->post("nama_jenis");
			
			$data_insert 	= array(
								"id_jenis"	=>"",
								"nama_jenis"=>$nama_jenis

						);
			$insert = $this->lpt->insert_tenaga_jenis($data_insert);
				if($insert){
					redirect('admin/tenaga');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_tenaga_jenis(){
		if($this->validation_ins_lp_tenaga_jenis()){
			$id_jenis 	= $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data 		= array(
						
						"nama_jenis"	=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"	=>$id_jenis,
						);

			$insert = $this->lpt->update_tenaga_jenis($data, $data_where);
				if($insert){
					redirect('admin/tenaga');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_tenaga_jenis($id_jenis){
		$delete = $this->lpt->delete_tenaga_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/tenaga');
		}else {
			echo "fail";
		}
	}	
//================================================ LP TENAGA JENIS ==============================================//

//================================================ LP TENAGA KATEGORI ==============================================//
	private function validation_ins_lp_tenaga_kategori(){
		$config_val_input = array(
            array(
                'field'=>'nama_kategori',
                'label'=>'Nama Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_tenaga_kategori(){
		$id_kategori		= $this->input->post("id_kategori");
		$data["status"] 	= false;
		$data["val"] 		= null;
		
		$data_main 			=$this->lpt->get_tenaga_kategori_where(array("id_kategori"=>$id_kategori)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_tenaga_kategori(){
		if($this->validation_ins_lp_tenaga_kategori()){
			$nama_kategori	= $this->input->post("nama_kategori");
			
			$data_insert	= array(
								"id_kategori"	=>"",
								"nama_kategori"	=>$nama_kategori

						);
			$insert = $this->lpt->insert_tenaga_kategori($data_insert);
				if($insert){
					redirect('admin/tenaga');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_tenaga_kategori(){
		if($this->validation_ins_lp_tenaga_kategori()){
			$id_kategori 	= $this->input->post("id_kategori");

			$nama_kategori 	= $this->input->post("nama_kategori");

			$data 			= array(
						
							"nama_kategori"	=>$nama_kategori
					);

			$data_where = array(
							"id_kategori"	=>$id_kategori,
						);

			$insert = $this->lpt->update_tenaga_kategori($data, $data_where);
				if($insert){
					redirect('admin/tenaga');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_tenaga_kategori($id_kategori){
		$delete = $this->lpt->delete_tenaga_kategori(array("id_kategori"=>$id_kategori));
		if($delete){
			echo "del";
			redirect('admin/tenaga');
		}else {
			echo "fail";
		}
	}
//================================================ LP TENAGA KATEGORI ==============================================//				
}
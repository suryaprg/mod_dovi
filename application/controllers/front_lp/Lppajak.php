<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppajak extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_pajak", "la");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }

	}

	public function index(){
		$data["page"] = "pajak";
		$data["list_pajak"] = $this->la->get_pajak();
		$data["list_pajak_jenis"] = $this->la->get_pajak_jenis();
		$this->load->view('front_lp/keuangan/main_admin_keuangan', $data);
	}

//================================================ LP PAJAK ====================================================//
	private function validation_ins_lp_pajak(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pajak(){
		if($this->validation_ins_lp_pajak()){
			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			
			$data_insert = array(
							"id_pajak"=>"",
							"id_jenis"=>$id_jenis,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->la->insert_pajak($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('front_lp/Lppajak','refresh');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pajak(){
		$id_pajak = $this->input->post("id_pajak");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_pajak_where(array("id_pajak"=>$id_pajak)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pajak(){
		if($this->validation_ins_lp_pajak()){
			$id_pajak = $this->input->post("id_pajak");
			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"jml"=>$jml
					);

			$data_where = array(
							"id_pajak"=>$id_pajak,
						);

			$insert = $this->la->update_pajak($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('front_lp/Lppajak','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pajak($id_pajak){
		$delete = $this->la->delete_pajak(array("id_pajak"=>$id_pajak));
		if($delete){
			echo "del";
			redirect('front_lp/Lppajak','refresh');
		}else {
			echo "fail";
		}
	}
//================================================ LP PAJAK JENIS ====================================================//

private function validation_ins_lp_pajak_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_pajak_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_pajak_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_pajak_jenis(){
		if($this->validation_ins_lp_pajak_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->la->insert_pajak_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/pajak');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_pajak_jenis(){
		if($this->validation_ins_lp_pajak_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->la->update_pajak_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/pajak');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pajak_jenis($id_jenis){
		$delete = $this->la->delete_pajak_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/pajak');
		}else {
			echo "fail";
		}
	}			
}
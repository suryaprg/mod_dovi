<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lptehnologi extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_tehnologi", "la");
		$this->load->library("response_message");
				if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "tehnologi";
		$data["list_tehnologi"] = $this->la->get_tehnologi();
		$data["list_tehnologi_jenis"] = $this->la->get_tehnologi_jenis();
		$data["list_tehnologi_sub_jenis"] = $this->la->get_tehnologi_sub_jenis();
		$this->load->view('front_lp/tehnologi/main_admin_tehnologi', $data);
	}

//================================================ LP APARAT ====================================================//
	private function validation_ins_lp_tehnologi(){
		$config_val_input = array(
            array(
                'field'=>'id_sub_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )   
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_tehnologi(){
		if($this->validation_ins_lp_tehnologi()){
			$id_sub_jenis = $this->input->post("id_sub_jenis");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			
			$data_insert = array(
							"id_tehnologi"=>"",
							"id_sub_jenis"=>$id_sub_jenis,
							"th"=>$th,
							"jml"=>$jml
						); 
			$insert = $this->la->insert_tehnologi($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/tehnologi');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_tehnologi(){
		$id_tehnologi = $this->input->post("id_tehnologi");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_tehnologi_where(array("id_tehnologi"=>$id_tehnologi)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_tehnologi(){
		if($this->validation_ins_lp_tehnologi()){
			$id_tehnologi = $this->input->post("id_tehnologi");

			$id_sub_jenis = $this->input->post("id_sub_jenis");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");

			$data = array(
						
						"id_sub_jenis"=>$id_sub_jenis,
						"th"=>$th,
						"jml"=>$jml
					);

			$data_where = array(
							"id_tehnologi"=>$id_tehnologi,
						);

			$insert = $this->la->update_tehnologi($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('/front_lp/Lptehnologi','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_tehnologi($id_tehnologi){
		$delete = $this->la->delete_tehnologi(array("id_tehnologi"=>$id_tehnologi));
		if($delete){
			echo "del";
			redirect('/front_lp/Lptehnologi','refresh');
		}else {
			echo "fail";
		}
	}
//================================================ LP PERTANIAN ====================================================//	

//================================================ LP pertanian Jenis ====================================================//
	private function validation_ins_lp_tehnologi_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_tehnologi_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_tehnologi_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_tehnologi_jenis(){
		if($this->validation_ins_lp_tehnologi_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->la->insert_tehnologi_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/tehnologi');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_tehnologi_jenis(){
		if($this->validation_ins_lp_tehnologi_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->la->update_tehnologi_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/tehnologi');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_tehnologi_jenis($id_jenis){
		$delete = $this->la->delete_tehnologi_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/tehnologi');
		}else {
			echo "fail";
		}
	}

//================================================ LP pertanian subJenis ====================================================//
private function validation_ins_lp_tehnologi_sub_jenis(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'nama_sub_jenis',
                'label'=>'nsj',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )

                ),
                array(
                'field'=>'satuan',
                'label'=>'sn',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                )
               
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_tehnologi_sub_jenis(){
		if($this->validation_ins_lp_tehnologi_sub_jenis()){
			$id_jenis = $this->input->post("id_jenis");
			$nama_sub_jenis = $this->input->post("nama_sub_jenis");
			$satuan = $this->input->post("satuan");
			
			$data_insert = array(

							"id_sub_jenis"=>"",
							"id_jenis"=>$id_jenis,
							"nama_sub_jenis"=>$nama_sub_jenis,
							"satuan"=>$satuan
						);
			$insert = $this->la->insert_tehnologi_sub_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/tehnologi');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_tehnologi_sub_jenis(){
		$id_sub_jenis = $this->input->post("id_sub_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_tehnologi_sub_jenis_where(array("id_sub_jenis"=>$id_sub_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_tehnologi_sub_jenis(){
		if($this->validation_ins_lp_tehnologi_sub_jenis()){

			$id_sub_jenis = $this->input->post("id_sub_jenis");
			$id_jenis = $this->input->post("id_jenis");
			$nama_sub_jenis = $this->input->post("nama_sub_jenis");
			$satuan = $this->input->post("satuan");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"nama_sub_jenis"=>$nama_sub_jenis,
						"satuan"=>$satuan
						
					);

			$data_where = array(
							"id_sub_jenis"=>$id_sub_jenis,
						);

			$insert = $this->la->update_tehnologi_sub_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/tehnologi');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_tehnologi_sub_jenis($id_sub_jenis){
		$delete = $this->la->delete_tehnologi_sub_jenis(array("id_sub_jenis"=>$id_sub_jenis));
		if($delete){
			echo "del";
			redirect('admin/tehnologi');
		}else {
			echo "fail";
		}
	}				
}

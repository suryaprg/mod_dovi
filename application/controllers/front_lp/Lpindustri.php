<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpindustri extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_pendindustri", "li");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] 				= "industri";
		$data["list_pend_industri"] = $this->li->get_pend_industri();
		$data["list_pend_jenis"] 	= $this->li->get_pend_industri_jenis();
		$data["list_pend_kategori"] = $this->li->get_pend_industri_kategori();
		$this->load->view('front_lp/keuangan/main_admin_keuangan', $data);
	}

//================================================ LP INDUSTRI ====================================================//
	private function validation_ins_lp_pendindustri(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'id_kategori',
                'label'=>'Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )
                       
            ),

            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),

            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")

                 )         
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_pend_industri(){
		if($this->validation_ins_lp_pendindustri()){
			$id_jenis 		= $this->input->post("id_jenis");
			$id_kategori 	= $this->input->post("id_kategori");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");
			
			$data_insert = array(
							"id_pend"=>"",
							"id_jenis"=>$id_jenis,
							"id_kategori"=>$id_kategori,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->li->insert_pend_industri($data_insert);
				if($insert){
					redirect('admin/industri');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pend_industri(){
		$id_pend 		= $this->input->post("id_pend");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->li->get_pend_industri_where(array("id_pend"=>$id_pend)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pend_industri(){
		if($this->validation_ins_lp_pendindustri()){
			$id_pend  	 = $this->input->post("id_pend");

			$id_jenis 	 = $this->input->post("id_jenis");
			$id_kategori = $this->input->post("id_kategori");
			$th 		 = $this->input->post("th");
			$jml 		 = $this->input->post("jml");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"id_kategori"=>$id_kategori,
						"th"=>$th,
						"jml"=>$jml
					);

			$data_where = array(
							"id_pend"=>$id_pend,
						);

			$insert = $this->li->update_pend_industri($data, $data_where);
				if($insert){
					redirect('admin/industri');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pend_industri($id_pend){
		$delete = $this->li->delete_pend_industri(array("id_pend"=>$id_pend));
		if($delete){
			echo "del";
			redirect('admin/industri');
		}else {
			echo "fail";
		}
	}
//================================================ LP INDUSTRI ====================================================//	

//================================================ LP INDUSTRI JENIS ==============================================//
	private function validation_ins_lp_pend_industri_jenis(){
		$config_val_input = array(

            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")

                 )         
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pend_industri_jenis(){
		if($this->validation_ins_lp_pend_industri_jenis()){
	
			$nama_jenis  = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->li->insert_pendindustri_jenis($data_insert);
				if($insert){
					redirect('admin/industri');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pendindustri_jenis(){
		$id_jenis 		= $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main =$this->li->get_pendindustri_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pend_industri_jenis(){
		if($this->validation_ins_lp_pend_industri_jenis()){
			$id_jenis   = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->li->update_pendindustri_jenis($data, $data_where);
				if($insert){
					redirect('admin/industri');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pend_industri_jenis($id_jenis){
		$delete = $this->li->delete_pendindustri_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/industri');
		}else {
			echo "fail";
		}
	}
//================================================ LP INDUSTRI JENIS ==============================================//	

//================================================ LP INDUSTRI KATEGORI ==========================================//
	private function validation_ins_lp_pendindustri_kategori(){
		$config_val_input = array(

            array(
                'field'=>'nama_kategori',
                'label'=>'Nama kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")

                 )         
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pendindustri_kategori(){
		if($this->validation_ins_lp_pendindustri_kategori()){
	
			$nama_kategori = $this->input->post("nama_kategori");
			
			$data_insert = array(
							"id_kategori"=>"",
							"nama_kategori"=>$nama_kategori
						);
			$insert = $this->li->insert_pendindustri_kategori($data_insert);
				if($insert){
					redirect('admin/industri');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pendindustri_kategori(){
		$id_kategori 	= $this->input->post("id_kategori");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main =$this->li->get_pendindustri_kategori_where(array("id_kategori"=>$id_kategori)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pendindustri_kategori(){
		if($this->validation_ins_lp_pendindustri_kategori()){
			$id_kategori 	= $this->input->post("id_kategori");

			$nama_kategori 	= $this->input->post("nama_kategori");

			$data = array(
						
						"nama_kategori"=>$nama_kategori
					);

			$data_where = array(
							"id_kategori"=>$id_kategori,
						);

			$insert = $this->li->update_pendindustri_kategori($data, $data_where);
				if($insert){
					redirect('admin/industri');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pendindustri_kategori($id_kategori){
		$delete = $this->li->delete_pendindustri_kategori(array("id_kategori"=>$id_kategori));
		if($delete){
			echo "del";
			redirect('admin/industri');
		}else {
			echo "fail";
		}
	}
//================================================ LP INDUSTRI KATEGORI ==========================================//
}
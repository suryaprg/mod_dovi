<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpaparatjenis extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_aparat", "la");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }

	}

	public function index(){
		$data["page"] = "iklim";
		$data["list_aparat_jenis"] = $this->la->get_aparat_jenis();
		$this->load->view('front_lp/aparat/v_lpaparatjenis', $data);
	}

	private function validation_ins_lp_aparat_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_aparat_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_aparat_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_aparat_jenis(){
		if($this->validation_ins_lp_aparat_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->la->insert_aparat_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('front_lp/Lpaparatjenis','refresh');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_aparat_jenis(){
		if($this->validation_ins_lp_aparat_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->la->update_aparat_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('front_lp/Lpaparatjenis','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_aparat_jenis($id_jenis){
		$delete = $this->la->delete_aparat_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('front_lp/Lpaparatjenis','refresh');
		}else {
			echo "fail";
		}
	}			
}
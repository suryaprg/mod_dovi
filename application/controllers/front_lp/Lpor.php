<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpor extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("front_lp/lp_or", "la");
		$this->load->library("response_message");
				if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "or";
		$data["list_or"] = $this->la->get_or();
		$data["list_or_jenis"] = $this->la->get_or_jenis();
		$this->load->view('front_lp/olahraga/main_admin_olahraga', $data);
	}

//================================================ LP or ====================================================//
	private function validation_ins_lp_or(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_or(){
		if($this->validation_ins_lp_or()){
			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			
			$data_insert = array(
							"id_or"=>"",
							"id_jenis"=>$id_jenis,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->la->insert_or($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('/front_lp/Lpor','refresh');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_or(){
		$id_or = $this->input->post("id_or");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_or_where(array("id_or"=>$id_or)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_or(){
		if($this->validation_ins_lp_or()){
			$id_or = $this->input->post("id_or");
			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"jml"=>$jml
					);

			$data_where = array(
							"id_or"=>$id_or,
						);

			$insert = $this->la->update_or($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('/front_lp/Lpor','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_or($id_or){
		$delete = $this->la->delete_or(array("id_or"=>$id_or));
		if($delete){
			echo "del";
			redirect('/front_lp/Lpor','refresh');
		}else {
			echo "fail";
		}
	}

//================================================ LP or JENIS ====================================================//

	private function validation_ins_lp_or_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_or_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_or_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_or_jenis(){
		if($this->validation_ins_lp_or_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->la->insert_or_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/or');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_or_jenis(){
		if($this->validation_ins_lp_or_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->la->update_or_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/or');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_or_jenis($id_jenis){
		$delete = $this->la->delete_or_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/or');
		}else {
			echo "fail";
		}
	}			
}
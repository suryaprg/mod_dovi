<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppertanian extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_pertanian", "la");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }

	}

	public function index(){
		$data["page"] = "pertanian";
		$data["list_pertanian"] = $this->la->get_pertanian();
		$data["list_pertanian_jenis"] = $this->la->get_pertanian_jenis();
		$data["list_pertanian_sub_jenis"] = $this->la->get_pertanian_sub_jenis();
		$this->load->view('front_lp/pertanian/main_admin_pertanian', $data);
	}

//================================================ LP APARAT ====================================================//
	private function validation_ins_lp_pertanian(){
		$config_val_input = array(
            array(
                'field'=>'id_sub_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                         
            ),
            array(
                'field'=>'luas',
                'label'=>'las',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )    
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pertanian(){
		if($this->validation_ins_lp_pertanian()){
			$id_sub_jenis = $this->input->post("id_sub_jenis");
			$luas = $this->input->post("luas");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			
			$data_insert = array(
							"id_pertanian"=>"",
							"id_sub_jenis"=>$id_sub_jenis,
							"th"=>$th,
							"luas"=>$luas,
							"jml"=>$jml
						);
			$insert = $this->la->insert_pertanian($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/pertanian');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pertanian(){
		$id_pertanian = $this->input->post("id_pertanian");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_pertanian_where(array("id_pertanian"=>$id_pertanian)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pertanian(){
		if($this->validation_ins_lp_pertanian()){
			$id_pertanian = $this->input->post("id_pertanian");

			$id_sub_jenis = $this->input->post("id_sub_jenis");
			$th = $this->input->post("th");
			$luas = $this->input->post("luas");
			$jml = $this->input->post("jml");

			$data = array(
						
						"id_sub_jenis"=>$id_sub_jenis,
						"th"=>$th,
						"luas"=>$luas,
						"jml"=>$jml
					);

			$data_where = array(
							"id_pertanian"=>$id_pertanian,
						);

			$insert = $this->la->update_pertanian($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin_opd/geo/Lppertanian','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pertanian($id_pertanian){
		$delete = $this->la->delete_pertanian(array("id_pertanian"=>$id_pertanian));
		if($delete){
			echo "del";
			redirect('admin_opd/geo/Lppertanian','refresh');
		}else {
			echo "fail";
		}
	}
//================================================ LP PERTANIAN ====================================================//	

//================================================ LP pertanian Jenis ====================================================//
	private function validation_ins_lp_pertanian_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_pertanian_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_pertanian_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_pertanian_jenis(){
		if($this->validation_ins_lp_pertanian_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->la->insert_pertanian_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/pertanian');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_pertanian_jenis(){
		if($this->validation_ins_lp_pertanian_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->la->update_pertanian_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/pertanian');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pertanian_jenis($id_jenis){
		$delete = $this->la->delete_pertanian_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/pertanian');
		}else {
			echo "fail";
		}
	}

//================================================ LP pertanian subJenis ====================================================//
private function validation_ins_lp_pertanian_sub_jenis(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'nama_sub_jenis',
                'label'=>'nsj',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )

                ),
                array(
                'field'=>'satuan',
                'label'=>'sn',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                )
               
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pertanian_sub_jenis(){
		if($this->validation_ins_lp_pertanian_sub_jenis()){
			$id_jenis = $this->input->post("id_jenis");
			$nama_sub_jenis = $this->input->post("nama_sub_jenis");
			$satuan = $this->input->post("satuan");
			
			$data_insert = array(

							"id_sub_jenis"=>"",
							"id_jenis"=>$id_jenis,
							"nama_sub_jenis"=>$nama_sub_jenis,
							"satuan"=>$satuan
						);
			$insert = $this->la->insert_pertanian_sub_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/pertanian');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pertanian_sub_jenis(){
		$id_sub_jenis = $this->input->post("id_sub_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_pertanian_sub_jenis_where(array("id_sub_jenis"=>$id_sub_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pertanian_sub_jenis(){
		if($this->validation_ins_lp_pertanian_sub_jenis()){

			$id_sub_jenis = $this->input->post("id_sub_jenis");
			$id_jenis = $this->input->post("id_jenis");
			$nama_sub_jenis = $this->input->post("nama_sub_jenis");
			$satuan = $this->input->post("satuan");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"nama_sub_jenis"=>$nama_sub_jenis,
						"satuan"=>$satuan
						
					);

			$data_where = array(
							"id_sub_jenis"=>$id_sub_jenis,
						);

			$insert = $this->la->update_pertanian_sub_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/pertanian');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pertanian_sub_jenis($id_sub_jenis){
		$delete = $this->la->delete_pertanian_sub_jenis(array("id_sub_jenis"=>$id_sub_jenis));
		if($delete){
			echo "del";
			redirect('admin/pertanian');
		}else {
			echo "fail";
		}
	}				
}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppenduduk extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_penduduk_jk', 'lpj');
        $this->load->model('front_lp/Lp_penduduk_umur', 'lpu');
        
        $this->load->library("response_message");

	}

#===========================================================================================================================
#------------------------------------------------------------Penduduk JK-------------------------------------------------------
#===========================================================================================================================
    public function penduduk_jk($th_fn){
        $kecamatan = $this->lpj->get_kec();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td rowspan=\"2\">No</td>
                                <td rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"2\">".$i."</td>";
            $str_header_bot .= "<th>Laki-laki</th>
                                <th>Perempuan</th>";
            $t_col += 3;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Data Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";


        $row_jenis = 0;
        foreach ($kecamatan as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_kec;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_kec;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_kec."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_kec."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpj->get_lp(array("id_kec"=>$v_data->id_kec, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_l"]){
                    $jml_l = $val_data["jml_l"];
                    $str_tbl .= "<td>".$val_data["jml_l"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                    $jml_l = 0;
                }

                if($val_data["jml_p"]){
                    $jml_p = $val_data["jml_p"];
                    $str_tbl .= "<td>".$val_data["jml_p"]."</td>";
                }else{
                    $jml_p = 0;
                    $str_tbl .= "<td>-</td>";
                }

                $main_data = array("year"=>(string)$i, "val_l"=>(float)$val_data["jml_l"], "val_p"=>(float)$val_data["jml_p"], "val_jml"=>(float)($jml_l+$jml_p));
                $data_graph[$v_data->id_kec][$row_th] = $main_data;
                $row_th++;
                // $str_tbl .= "<td>".($jml_l+$jml_p)."</td>";
            }

            $str_tbl .= "</tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);
        

        // print_r("<pre>");
        // print_r($data);

        $this->load->view("front_lp/penduduk/lp_penduduk_jk",$data);
        // $this->load->view("front_lp/pmks/pmks", $data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }

#===========================================================================================================================
#------------------------------------------------------------Penduduk JK----------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------------Penduduk Rasio--------------------------------------------------
#===========================================================================================================================

    public function penduduk_rasio($th_fn){
        $kecamatan = $this->lpj->get_kec();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td rowspan=\"2\">No</td>
                                <td rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"2\">".$i."</td>";
            $str_header_bot .= "<th>Jumlah Penduduk</th>
                                <th>Rasio</th>";
            $t_col += 3;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Data Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";


        $row_jenis = 0;
        foreach ($kecamatan as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_kec;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_kec;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_kec."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_kec."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpj->get_lp(array("id_kec"=>$v_data->id_kec, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_l"]){
                    $jml_l = $val_data["jml_l"];
                    // $str_tbl .= "<td>".$val_data["jml_l"]."</td>";
                }else{
                    // $str_tbl .= "<td>-</td>";
                    $jml_l = 0;
                }

                if($val_data["jml_p"]){
                    $jml_p = $val_data["jml_p"];
                    // $str_tbl .= "<td>".$val_data["jml_p"]."</td>";
                }else{
                    $jml_p = 0;
                    // $str_tbl .= "<td>-</td>";
                }

                $str_tbl .= "<td>".($jml_l+$jml_p)."</td>";
                $str_tbl .= "<td>".$val_data["rasio"]."</td>";

                $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["rasio"]);
                $data_graph[$v_data->id_kec][$row_th] = $main_data;
                $row_th++;
            }

            $str_tbl .= "</tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;
        $data["data_graph"] = json_encode($data_graph);

        // print_r("<pre>");
        // print_r($data);

        $this->load->view("front_lp/penduduk/lp_penduduk_rasio", $data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }

#===========================================================================================================================
#-----------------------------------------------------------Penduduk Rasio--------------------------------------------------
#===========================================================================================================================


#===========================================================================================================================
#-----------------------------------------------------------Penduduk Umur--------------------------------------------------
#===========================================================================================================================

    public function penduduk_umur($th_fn){
        $jenis = $this->lpu->get_lp_jenis();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td rowspan=\"2\">No</td>
                                <td rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"3\">".$i."</td>";
            $str_header_bot .= "<th>Laki-laki</th>
                                <th>Perempuan</th>
                                <th>Rasio</th>";
            $t_col += 3;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Data Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";

        $data_graph = array();
        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpu->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_l"]){
                    $jml_l = $val_data["jml_l"];
                    $str_tbl .= "<td>".$val_data["jml_l"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                    $jml_l = 0;
                }

                if($val_data["jml_p"]){
                    $jml_p = $val_data["jml_p"];
                    $str_tbl .= "<td>".$val_data["jml_p"]."</td>";
                }else{
                    $jml_p = 0;
                    $str_tbl .= "<td>-</td>";
                }

                $str_tbl .= "<td>".($jml_l+$jml_p)."</td>";
                // $str_tbl .= "<td>".$val_data["rasio"]."</td>";
                $main_data = array("year"=>(string)$i, "val_l"=>(float)$val_data["jml_l"], "val_p"=>(float)$val_data["jml_p"], "val_jml"=>(float)($jml_l+$jml_p));
                $data_graph[$v_data->id_jenis][$row_th] = $main_data;
                $row_th++;
            }

            $str_tbl .= "</tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);

        // print_r("<pre>");
        // print_r($data);

        // $this->load->view("front_lp/pmks/pmks", $data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
        $this->load->view("front_lp/penduduk/lp_penduduk_umur",$data);
    }

#===========================================================================================================================
#-----------------------------------------------------------Penduduk Umur--------------------------------------------------
#===========================================================================================================================
}
?>
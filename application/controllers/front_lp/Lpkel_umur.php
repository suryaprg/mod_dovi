<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkel_umur extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_kel_umur", "lku");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "umur";
		$data["list_umur"] = $this->lku->get_umur();
		$data["list_umur_jenis"] = $this->lku->get_umur_jenis();
		$this->load->view('front_lp/penduduk/main_admin_penduduk', $data);
	}

//==================================================== LP KEL UMUR ===============================================//	

	private function validation_ins_lp_umur(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'ID Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),            
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_l',
                'label'=>'Jumlah L',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_p',
                'label'=>'Jumlah P',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}	

	public function insert_lp_umur(){
		if($this->validation_ins_lp_umur()){
			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$jml_l = $this->input->post("jml_l");
			$jml_p = $this->input->post("jml_p");
			
			$data_insert = array(
							"id_kel_umur"=>"",
							"id_jenis"=>$id_jenis,
							"th"=>$th,
							"jml_l"=>$jml_l,
							"jml_p"=>$jml_p
						);
			$insert = $this->lku->insert_umur($data_insert);
				if($insert){
					redirect('admin/umur');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}	

	public function index_up_lp_umur(){
		$id_kel_umur = $this->input->post("id_kel_umur");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->lku->get_umur_where(array("id_kel_umur"=>$id_kel_umur)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_umur(){
		if($this->validation_ins_lp_umur()){
			$id_kel_umur = $this->input->post("id_kel_umur");

			$id_jenis = $this->input->post("id_jenis");
			$th = $this->input->post("th");
			$jml_l = $this->input->post("jml_l");
			$jml_p = $this->input->post("jml_p");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"th"=>$th,
						"jml_l"=>$jml_l,
						"jml_p"=>$jml_p
					);

			$data_where = array(
							"id_kel_umur"=>$id_kel_umur,
						);

			$insert = $this->lku->update_umur($data, $data_where);
				if($insert){
					redirect('admin/umur');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_umur($id_kel_umur){
		$delete = $this->lku->delete_umur(array("id_kel_umur"=>$id_kel_umur));
		if($delete){
			redirect('admin/umur');
			echo "del";
		}else {
			echo "fail";
		}
	}	
//==================================================== LP KEL UMUR ===============================================//	

//==================================================== LP KEL UMUR JENIS =========================================//
	private function validation_ins_lp_umur_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}	

	public function insert_lp_umur_jenis(){
		if($this->validation_ins_lp_umur_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->lku->insert_umur_jenis($data_insert);
				if($insert){
					redirect('admin/umur');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}	

	public function index_up_lp_umur_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->lku->get_umur_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_umur_jenis(){
		if($this->validation_ins_lp_umur_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->lku->update_umur_jenis($data, $data_where);
				if($insert){
					redirect('admin/umur');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_umur_jenis($id_jenis){
		$delete = $this->lku->delete_umur_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/umur');
		}else {
			echo "fail";
		}
	}	
//==================================================== LP KEL UMUR JENIS =========================================//	

}
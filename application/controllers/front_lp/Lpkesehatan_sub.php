<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkesehatan_sub extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_kesehatan", "lk");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kesehatan_sub";
		$data["list_kesehatan_jenis"] = $this->lk->get_kesehatan_jenis();
		$data["list_kesehatan_sub_jenis"] = $this->lk->get_kesehatan_sub_jenis();
		$data["list_kesehatan_sub3_jenis"] = $this->lk->get_kesehatan_sub3_jenis();
		$this->load->view('front_lp/penduduk/main_admin_penduduk', $data);
	}

//================================================= LP KESEHATAN SUB JENIS =========================================//
	private function validation_ins_lp_kesehatan_sub(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'nama_sub_jenis',
                'label'=>'Sub Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),            
            array(
                'field'=>'satuan',
                'label'=>'Satuan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_kesehatan_sub(){
		$id_sub_jenis = $this->input->post("id_sub_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->lk->get_kesehatan_sub_jenis_where(array("id_sub_jenis"=>$id_sub_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_kesehatan_sub(){
		if($this->validation_ins_lp_kesehatan_sub()){
			$nama_sub_jenis = $this->input->post("nama_sub_jenis");
			$id_jenis = $this->input->post("id_jenis");
			$satuan = $this->input->post("satuan");

			$data_insert = array(
							"id_sub_jenis"=>"",
							"id_jenis"=>$id_jenis,
							"nama_sub_jenis"=>$nama_sub_jenis,
							"satuan"=>$satuan
						);
			$insert = $this->lk->insert_kesehatan_sub_jenis($data_insert);
				if($insert){
					redirect('admin/kesehatan/sub');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_kesehatan_sub(){
		if($this->validation_ins_lp_kesehatan_sub()){
			$id_sub_jenis 	= $this->input->post("id_sub_jenis");
			$id_jenis 		= $this->input->post("id_jenis");
			$nama_sub_jenis = $this->input->post("nama_sub_jenis");
			$satuan 		= $this->input->post("satuan");

			$data = array(
						
						"nama_sub_jenis"=>$nama_sub_jenis,
						"id_jenis"=>$id_jenis,
						"satuan"=>$satuan
					);

			$data_where = array(
							"id_sub_jenis"=>$id_sub_jenis,
						);

			$insert = $this->lk->update_kesehatan_sub_jenis($data, $data_where);
				if($insert){
					redirect('admin/kesehatan/sub');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_kesehatan_sub($id_sub_jenis){
		$delete = $this->lk->delete_kesehatan_sub_jenis(array("id_sub_jenis"=>$id_sub_jenis));
		if($delete){
			echo "del";
			redirect('admin/kesehatan/sub');
		}else {
			echo "fail";
		}
	}
//================================================= LP KESEHATAN SUB JENIS =========================================//

//================================================= LP KESEHATAN SUB JENIS =========================================//
	private function validation_ins_lp_kesehatan_sub3(){
		$config_val_input = array(
            array(
                'field'=>'id_sub_jenis',
                'label'=>'Sub Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'nama_3sub_jenis',
                'label'=>'Sub 3 Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),            
            array(
                'field'=>'satuan',
                'label'=>'Satuan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_kesehatan_sub3(){
		$id_3sub_jenis 	= $this->input->post("id_3sub_jenis");
		$data["status"] = false;
		$data["val"]	= null;
		
		$data_main =$this->lk->get_kesehatan_sub3_jenis_where(array("id_3sub_jenis"=>$id_3sub_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_kesehatan_sub3(){
		if($this->validation_ins_lp_kesehatan_sub3()){
			$id_sub_jenis 	 = $this->input->post("id_sub_jenis");
			$nama_3sub_jenis = $this->input->post("nama_3sub_jenis");
			$satuan 		 = $this->input->post("satuan");

			$data_insert = array(
							"id_3sub_jenis"=>"",
							"id_sub_jenis"=>$id_sub_jenis,
							"nama_3sub_jenis"=>$nama_3sub_jenis,
							"satuan"=>$satuan
						);
			$insert = $this->lk->insert_kesehatan_sub3_jenis($data_insert);
				if($insert){
					redirect('admin/kesehatan/sub');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_kesehatan_sub3(){
		if($this->validation_ins_lp_kesehatan_sub3()){
			$id_3sub_jenis 	 = $this->input->post("id_3sub_jenis");
			$id_sub_jenis 	 = $this->input->post("id_sub_jenis");
			$nama_3sub_jenis = $this->input->post("nama_3sub_jenis");
			$satuan 		 = $this->input->post("satuan");

			$data = array(
						
						"id_sub_jenis"=>$id_sub_jenis,
						"nama_3sub_jenis"=>$nama_3sub_jenis,
						"satuan"=>$satuan
					);

			$data_where = array(
							"id_3sub_jenis"=>$id_3sub_jenis,
						);

			$insert = $this->lk->update_kesehatan_sub3_jenis($data, $data_where);
				if($insert){
					redirect('admin/kesehatan/sub');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_kesehatan_sub3($id_3sub_jenis){
		$delete = $this->lk->delete_kesehatan_sub3_jenis(array("id_3sub_jenis"=>$id_3sub_jenis));
		if($delete){
			echo "del";
			redirect('admin/kesehatan/sub');
		}else {
			echo "fail";
		}
	}	
//================================================= LP KESEHATAN SUB JENIS =========================================//
}

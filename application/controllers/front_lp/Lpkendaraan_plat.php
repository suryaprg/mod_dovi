<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkendaraan_plat extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_kendaraan", "lk");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }

	}

	public function index(){
		$data["page"] 						= "kendaraan_plat";
		$data["list_kendaraan_plat"] 		= $this->lk->get_kendaraan_plat();
		$data["list_kendaraan_plat_jenis"] 	= $this->lk->get_kendaraan_plat_jenis();
		$this->load->view('front_lp/transportasi/main_admin_transportasi', $data);
	}

//================================================== LP KENDARAAN PLAT ============================================//
	private function validation_ins_lp_kendaraan_plat(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'ID Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),            
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_merah',
                'label'=>'Jumlah Merah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_kuning',
                'label'=>'Jumlah Kuning',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_hitam',
                'label'=>'Jumlah Hitam',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}	

	public function insert_lp_kendaraan_plat(){
		if($this->validation_ins_lp_kendaraan_plat()){
			$id_jenis 	= $this->input->post("id_jenis");
			$th			= $this->input->post("th");
			$jml_merah	= $this->input->post("jml_merah");
			$jml_kuning	= $this->input->post("jml_kuning");
			$jml_hitam	= $this->input->post("jml_hitam");
			
			$data_insert = array(
							"id_kendaraan"	=> "",
							"id_jenis"		=> $id_jenis,
							"th"			=> $th,
							"jml_merah"		=> $jml_merah,
							"jml_kuning"	=> $jml_kuning,
							"jml_hitam"		=> $jml_hitam
						);

			$insert = $this->lk->insert_kendaraan_plat($data_insert);
				if($insert){
					redirect('admin/kendaraan/plat');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}	

	public function index_up_lp_kendaraan_plat(){
		$id_kendaraan = $this->input->post("id_kendaraan");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->lk->get_kendaraan_plat_where(array("id_kendaraan"=>$id_kendaraan)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_kendaraan_plat(){
		if($this->validation_ins_lp_kendaraan_plat()){
			$id_kendaraan 		= $this->input->post("id_kendaraan");

			$id_jenis 			= $this->input->post("id_jenis");
			$th 				= $this->input->post("th");
			$jml_merah 			= $this->input->post("jml_merah");
			$jml_kuning			= $this->input->post("jml_kuning");
			$jml_hitam 			= $this->input->post("jml_hitam");

			$data = array(						
						"id_jenis"		=> $id_jenis,
						"th"			=> $th,
						"jml_merah"		=> $jml_merah,
						"jml_kuning"	=> $jml_kuning,
						"jml_hitam"		=> $jml_hitam
					);

			$data_where = array(
							"id_kendaraan"	=> $id_kendaraan,
						);

			$insert = $this->lk->update_kendaraan_plat($data, $data_where);
				if($insert){
					redirect('admin/kendaraan/plat');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_kendaraan_plat($id_kendaraan){
		$delete = $this->lk->delete_kendaraan_plat(array("id_kendaraan"=>$id_kendaraan));
		if($delete){
			redirect('admin/kendaraan/plat');
			echo "del";
		}else {
			echo "fail";
		}
	}	
//================================================== LP KENDARAAN PLAT ============================================//		

//================================================== LP KENDARAAN PLAT JENIS ============================================//
	private function validation_ins_lp_kendaraan_plat_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )            
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}	

	public function insert_lp_kendaraan_plat_jenis(){
		if($this->validation_ins_lp_kendaraan_plat_jenis()){
			$nama_jenis	= $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"		=> "",
							"nama_jenis"	=> $nama_jenis
						);

			$insert = $this->lk->insert_kendaraan_plat_jenis($data_insert);
				if($insert){
					redirect('admin/kendaraan/plat');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}	

	public function index_up_lp_kendaraan_plat_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->lk->get_kendaraan_plat_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_kendaraan_plat_jenis(){
		if($this->validation_ins_lp_kendaraan_plat_jenis()){
			$id_jenis 		= $this->input->post("id_jenis");

			$nama_jenis 	= $this->input->post("nama_jenis");

			$data = array(						
						"nama_jenis"	=> $nama_jenis
					);

			$data_where = array(
							"id_jenis"	=> $id_jenis,
						);

			$insert = $this->lk->update_kendaraan_plat_jenis($data, $data_where);
				if($insert){
					redirect('admin/kendaraan/plat');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_kendaraan_plat_jenis($id_jenis){
		$delete = $this->lk->delete_kendaraan_plat_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			redirect('admin/kendaraan/plat');
			echo "del";
		}else {
			echo "fail";
		}
	}	
//================================================== LP KENDARAAN PLAT JENIS ============================================//	
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpdiskominfo extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_diskominfo', 'lp');

	}
    
    public function get_aplikasi(){  
        $val_data = $this->lp->get_aplikasi();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Aplikasi yang Digunakan di Pemerintah Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"25%\">Aplikasi</td>
                                <td width=\"40%\">Keterangan</td>
                                <td width=\"40%\">Fungsi</td>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title."</td>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td>".($row_data+1)."</td>
                            <td>".$v_data->aplikasi."</td>
                            <td>".$v_data->fungsi."</td>
                            <td>".$v_data->keterangan."</td>
                        </tr>";

        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;

        print_r("<pre>");
        // print_r($data["data_graph"]);
        print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");

    }

    public function get_domain(){  
        $val_data = $this->lp->get_domain();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Domain yang Digunakan di Pemerintah Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"40%\">Sub Domain</td>
                                <td width=\"40%\">Nama SKPD</td>
                                <td width=\"25%\">th</td>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title."</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td>".($row_data+1)."</td>
                            <td>".$v_data->subdomain."</td>
                            <td>".$v_data->nama_skpd."</td>
                            <td>".$v_data->th."</td>
                        </tr>";

        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;

        print_r("<pre>");
        // print_r($data["data_graph"]);
        print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");
        
    }
    
}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lptpa extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_tpa", "ltp");
		$this->load->library("response_message");
				if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"]				= "tpa";
		$data["list_tpa"] 			= $this->ltp->get_tpa();
		$data["list_kecamatan"] 	= $this->ltp->get_kecamatan();
		$this->load->view('front_lp/lingkungan/main_admin_lingkungan', $data);
	}

//================================================ LP TPA ====================================================//
	private function validation_ins_lp_tpa(){
		$config_val_input = array(
            array(
                'field'=>'id_kec',
                'label'=>'Kecamatan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),

            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_tpa(){
		if($this->validation_ins_lp_tpa()){
			$id_kec		 	= $this->input->post("id_kec");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");
			
			$data_insert = array(
							"id_tpa"=>"",
							"id_kec"=>$id_kec,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->ltp->insert_tpa($data_insert);
				if($insert){
					redirect('admin/tpa');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_tpa(){
		$id_tpa 			= $this->input->post("id_tpa");
		$data["status"] 	= false;
		$data["val"] 		= null;
		
		$data_main 			=$this->ltp->get_tpa_where(array("id_tpa"=>$id_tpa)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_tpa(){
		if($this->validation_ins_lp_tpa()){
			$id_tpa 		= $this->input->post("id_tpa");

			$id_kec 		= $this->input->post("id_kec");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");

			$data = array(
						
						"id_kec"		=>$id_kec,
						"th"			=>$th,
						"jml"			=>$jml
					);

			$data_where = array(
							"id_tpa"	=>$id_tpa,
						);

			$insert = $this->ltp->update_tpa($data, $data_where);
				if($insert){
					redirect('admin/tpa');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_tpa($id_tpa){
		$delete = $this->ltp->delete_tpa(array("id_tpa"=>$id_tpa));
		if($delete){
			echo "del";
			redirect('admin/tpa');
		}else {
			echo "fail";
		}
	}
//================================================ LP TPA ====================================================//	
}
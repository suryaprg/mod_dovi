<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppendidikan extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_pendidikan", "lp");
		$this->load->library("response_message");
				if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "pendidikan";
		$data["list_kecamatan"] 		  = $this->lp->get_kecamatan();
		$data["list_pendidikan"] 		  = $this->lp->get_pendidikan();
		$data["list_pendidikan_jenis"] 	  = $this->lp->get_pendidikan_jenis();
		$data["list_pendidikan_kategori"] = $this->lp->get_pendidikan_kategori();
		$this->load->view('front_lp/pendidikan/main_admin_pendidikan', $data);
	}

//================================================ LP PENDIDIKAN ===============================================//
	private function validation_ins_lp_pendidikan(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'id_kategori',
                'label'=>'Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )
                       
            ),
            array(
                'field'=>'id_kec',
                'label'=>'Kecamatan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),

            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),

            array(
                'field'=>'jml_negeri',
                'label'=>'Jumlah Sekolah Negeri',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")

                 )
                       
            ),

            array(
                'field'=>'jml_swasta',
                'label'=>'Jumalah Sekolah Swasta',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")

                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pendidikan(){
		if($this->validation_ins_lp_pendidikan()){
			$id_jenis 	 = $this->input->post("id_jenis");
			$id_kategori = $this->input->post("id_kategori");
			$id_kec 	 = $this->input->post("id_kec");
			$th 		 = $this->input->post("th");
			$jml_negeri  = $this->input->post("jml_negeri");
			$jml_swasta  = $this->input->post("jml_swasta");
			
			$data_insert = array(
							"id_pendidikan"=>"",
							"id_jenis"=>$id_jenis,
							"id_kategori"=>$id_kategori,
							"id_kec"=>$id_kec,
							"th"=>$th,
							"jml_negeri"=>$jml_negeri,
							"jml_swasta"=>$jml_swasta
						);
			$insert = $this->lp->insert_pendidikan($data_insert);
				if($insert){
					redirect('admin/pendidikan');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pendidikan(){
		$id_pendidikan  = $this->input->post("id_pendidikan");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main =$this->lp->get_pendidikan_where(array("id_pendidikan"=>$id_pendidikan)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pendidikan(){
		if($this->validation_ins_lp_pendidikan()){
			$id_pendidikan  = $this->input->post("id_pendidikan");

			$id_jenis 		= $this->input->post("id_jenis");
			$id_kategori 	= $this->input->post("id_kategori");
			$id_kec 		= $this->input->post("id_kec");
			$th 			= $this->input->post("th");
			$jml_negeri 	= $this->input->post("jml_negeri");
			$jml_swasta 	= $this->input->post("jml_swasta");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"id_kategori"=>$id_kategori,
						"id_kec"=>$id_kec,
						"th"=>$th,
						"jml_negeri"=>$jml_negeri,
						"jml_swasta"=>$jml_swasta
					);

			$data_where = array(
							"id_pendidikan"=>$id_pendidikan,
						);

			$insert = $this->lp->update_pendidikan($data, $data_where);
				if($insert){
					redirect('admin/pendidikan');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pendidikan($id_pendidikan){
		$delete = $this->lp->delete_pendidikan(array("id_pendidikan"=>$id_pendidikan));
		if($delete){
			echo "del";
			redirect('admin/pendidikan');
		}else {
			echo "fail";
		}
	}
//================================================ LP PENDIDIKAN ===============================================//	

//================================================ LP PENDIDIKAN JENIS =========================================//
	private function validation_ins_lp_pendidikan_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pendidikan_jenis(){
		if($this->validation_ins_lp_pendidikan_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis

						);
			$insert = $this->lp->insert_pendidikan_jenis($data_insert);
				if($insert){
					redirect('admin/pendidikan');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pendidikan_jenis(){
		$id_jenis 		= $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main = $this->lp->get_pendidikan_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pendidikan_jenis(){
		if($this->validation_ins_lp_pendidikan_jenis()){
			$id_jenis 	= $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->lp->update_pendidikan_jenis($data, $data_where);
				if($insert){
					redirect('admin/pendidikan');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pendidikan_jenis($id_jenis){
		$delete = $this->lp->delete_pendidikan_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/pendidikan');
		}else {
			echo "fail";
		}
	}
//================================================ LP PENDIDIKAN JENIS ========================================//

//================================================ LP PENDIDIKAN KATEGORI =====================================//
	private function validation_ins_lp_pendidikan_kategori(){
		$config_val_input = array(
            array(
                'field'=>'nama_kategori',
                'label'=>'Nama Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pendidikan_kategori(){
		if($this->validation_ins_lp_pendidikan_kategori()){
			$nama_kategori 	= $this->input->post("nama_kategori");
			
			$data_insert = array(
							"id_kategori"=>"",
							"nama_kategori"=>$nama_kategori

						);
			$insert = $this->lp->insert_pendidikan_kategori($data_insert);
				if($insert){
					redirect('admin/pendidikan');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pendidikan_kategori(){
		$id_kategori 	= $this->input->post("id_kategori");
		$data["status"] = false;
		$data["val"] 	= null;
		
		$data_main =$this->lp->get_pendidikan_kategori_where(array("id_kategori"=>$id_kategori));//bagian edit terdapat data
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] 	= $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pendidikan_kategori(){
		if($this->validation_ins_lp_pendidikan_kategori()){
			$id_kategori 	= $this->input->post("id_kategori");

			$nama_kategori 	= $this->input->post("nama_kategori");

			$data = array(
							"nama_kategori"=>$nama_kategori,
					);

			$data_where = array(
							"id_kategori"=>$id_kategori,
						);

			$insert = $this->lp->update_pendidikan_kategori($data, $data_where);
				if($insert){
					redirect('admin/pendidikan');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_pendidikan_kategori($id_kategori){
		$delete = $this->lp->delete_pendidikan_kategori(array("id_kategori"=>$id_kategori));
		if($delete){
			echo "del";
			redirect('admin/pendidikan');
		}else {
			echo "fail";
		}
	}
//================================================ LP PENDIDIKAN KATEGORI =====================================//	
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppanen extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_panen", "la");
		$this->load->library("response_message");
				if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "panen";
		$data["list_panen"] = $this->la->get_panen();
		$data["list_panen_jenis"] = $this->la->get_panen_jenis();
		$data["list_main_kecamatan"] = $this->la->get_main_kecamatan();
		$this->load->view('front_lp/pertanian/main_admin_pertanian', $data);
	}

//================================================ LP PANEN ====================================================//
	private function validation_ins_lp_panen(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
             array(
                'field'=>'id_kec',
                'label'=>'Id kec',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'luas_panen',
                'label'=>'Luas panen',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml_panen',
                'label'=>'Jumlah Panen',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_panen(){
		if($this->validation_ins_lp_panen()){
			$id_jenis = $this->input->post("id_jenis");
			$id_kec = $this->input->post("id_kec");
			$th = $this->input->post("th");
			$luas_panen = $this->input->post("luas_panen");
			$jml_panen = $this->input->post("jml_panen");
			
			$data_insert = array(
							"id_panen"=>"",
							"id_jenis"=>$id_jenis,
							"id_kec"=>$id_kec,
							"th"=>$th,
							"luas_panen"=>$luas_panen,
							"jml_panen"=>$jml_panen
						);
			$insert = $this->la->insert_panen($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('front_lp/Lppanen','refresh');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_panen(){
		$id_panen = $this->input->post("id_panen");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_panen_where(array("id_panen"=>$id_panen)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_panen(){
		if($this->validation_ins_lp_panen()){
			$id_panen = $this->input->post("id_panen");
			$id_jenis = $this->input->post("id_jenis");
			$id_kec = $this->input->post("id_kec");
			$th = $this->input->post("th");
			$luas_panen = $this->input->post("luas_panen");
			$jml_panen = $this->input->post("jml_panen");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"id_kec"=>$id_kec,
						"th"=>$th,
						"luas_panen"=>$luas_panen,
						"jml_panen"=>$jml_panen
					);

			$data_where = array(
							"id_panen"=>$id_panen,
						);

			$insert = $this->la->update_panen($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('front_lp/Lppanen','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_panen($id_panen){
		$delete = $this->la->delete_panen(array("id_panen"=>$id_panen));
		if($delete){
			echo "del";
			redirect('front_lp/Lppanen','refresh');
		}else {
			echo "fail";
		}
	}
//================================================ LP PANEN JENIS ====================================================//
private function validation_ins_lp_panen_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_panen_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_panen_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_panen_jenis(){
		if($this->validation_ins_lp_panen_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->la->insert_panen_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/panen');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_panen_jenis(){
		if($this->validation_ins_lp_panen_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->la->update_panen_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/panen');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_panen_jenis($id_jenis){
		$delete = $this->la->delete_panen_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/panen');
		}else {
			echo "fail";
		}
	}			
}
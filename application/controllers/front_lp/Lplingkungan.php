<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lplingkungan extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_tpa', 'lp');
        
        $this->load->library("response_message");

	}
    
    public function index($th_fn){
        
    }

    public function tpa($th_fn){
        $kec = $this->lp->get_lp_kec();

        $row_kec = 0;

        $data_graph = array();
        $series = "";
        foreach ($kec as $r_data => $v_data) {
            $data["list_data"][$row_kec]["main"]["kecamatan"] = $v_data->nama_kec;
            $data["list_data"][$row_kec]["main"]["id_kec"] = $v_data->id_kec;
            
            $row_th = 0;
            for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 

                $val_data = $this->lp->get_lp(array("lpt.id_kec"=>$v_data->id_kec, "th"=>$i));

                if(!isset($data_graph[$row_th]["year"])){
                    $data_graph[$row_th]["year"] = "$i";
                }

                if(!isset($data_graph[$row_th]["jml"])){
                    $data_graph[$row_th]["jml"] = (int)$val_data["jml"];
                }else {
                    $data_graph[$row_th]["jml"] += (int)$val_data["jml"];
                }

                $data_graph[$row_th][$v_data->id_kec] =  (int)$val_data["jml"];

                
                $data["list_data"][$row_kec]["value"][$i] = $val_data["jml"];

                $row_th++;
            }

            $series .= "createSeries(\"".$v_data->id_kec."\", \"".$v_data->nama_kec."\");";

            $row_kec++;
        }
        $series .= "createSeries(\"jml\", \"Total TPA Kota Malang\");";

        $data["graph_data"] = json_encode($data_graph);
        $data["series_data"] = $series;

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;


        // print_r("<pre>");
        // print_r($data);
        // print_r($data_graph);


        $this->load->view("front_lp/lingkungan/tpa",$data);

    }

    public function keuangan_get($id_jenis,$th_fn){
        
    }
    
}
?>
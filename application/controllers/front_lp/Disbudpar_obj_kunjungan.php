<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Disbudpar_obj_kunjungan extends CI_Model{
    
    public function get_lp_kategori(){
        $data = $this->db->get("disbudpar_obj_pemajuan_bud_kate")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("disbudpar_obj_pemajuan_bud_main", $where)->result();
        return $data;
    }
    
}
?>

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lplahan extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_lahan", "la");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }

	}

	public function index(){
		$data["page"] = "lahan";
		$data["list_lahan"] = $this->la->get_lahan();
		$data["list_lahan_jenis"] = $this->la->get_lahan_jenis();
		$data["list_main_kecamatan"] = $this->la->get_main_kecamatan();
		$this->load->view('front_lp/pertanian/main_admin_pertanian', $data);
	}

//================================================ LP lahan ====================================================//
	private function validation_ins_lp_lahan(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
             array(
                'field'=>'id_kec',
                'label'=>'Id kec',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_lahan(){
		if($this->validation_ins_lp_lahan()){
			$id_jenis = $this->input->post("id_jenis");
			$id_kec = $this->input->post("id_kec");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			
			$data_insert = array(
							"id_lahan"=>"",
							"id_jenis"=>$id_jenis,
							"id_kec"=>$id_kec,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->la->insert_lahan($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('/front_lp/Lplahan','refresh');
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_lahan(){
		$id_lahan = $this->input->post("id_lahan");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_lahan_where(array("id_lahan"=>$id_lahan)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_lahan(){
		if($this->validation_ins_lp_lahan()){
			$id_lahan = $this->input->post("id_lahan");
			$id_jenis = $this->input->post("id_jenis");
			$id_kec = $this->input->post("id_kec");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"id_kec"=>$id_kec,
						"th"=>$th,
						"jml"=>$jml
					);

			$data_where = array(
							"id_lahan"=>$id_lahan,
						);

			$insert = $this->la->update_lahan($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('/front_lp/Lplahan','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_lahan($id_lahan){
		$delete = $this->la->delete_lahan(array("id_lahan"=>$id_lahan));
		if($delete){
			echo "del";
			redirect('/front_lp/Lplahan','refresh');
		}else {
			echo "fail";
		}
	}
//================================================ LP lahan jenis ====================================================//
private function validation_ins_lp_lahan_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function index_up_lp_lahan_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->la->get_lahan_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}	

	public function insert_lp_lahan_jenis(){
		if($this->validation_ins_lp_lahan_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->la->insert_lahan_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin/lahan','refresh');
		}else {
			print_r(validation_errors());
		}
	}

	public function update_lp_lahan_jenis(){
		if($this->validation_ins_lp_lahan_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->la->update_lahan_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin/lahan','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_lahan_jenis($id_jenis){
		$delete = $this->la->delete_lahan_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/lahan','refresh');
		}else {
			echo "fail";
		}
	}			
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpbencana extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_bencana", "lb");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }

	}

	public function index(){
		$data["page"] = "bencana";
		$data["list_bencana"] = $this->lb->get_bencana();
		$data["list_bencana_jenis"] = $this->lb->get_bencana_jenis();
		$data["list_main_kec"] = $this->lb->get_main_kec();
		$this->load->view('front_lp/bencana/main_admin_bencana', $data);
	}

//================================================== LP BENCANA ==========================================//
	private function validation_ins_lp_bencana(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'id_kec',
                'label'=>'ID Kec',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),            
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}	

	public function insert_lp_bencana(){
		if($this->validation_ins_lp_bencana()){
			$id_jenis = $this->input->post("id_jenis");
			$id_kec = $this->input->post("id_kec");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");
			
			$data_insert = array(
							"id_bencana"=>"",
							"id_jenis"=>$id_jenis,
							"id_kec"=>$id_kec,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->lb->insert_bencana($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
		redirect('admin/bencana');
	}	

	public function index_up_lp_bencana(){
		$id_bencana = $this->input->post("id_bencana");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->lb->get_bencana_where(array("id_bencana"=>$id_bencana)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_bencana(){
		if($this->validation_ins_lp_bencana()){
			$id_bencana = $this->input->post("id_bencana");

			$id_jenis = $this->input->post("id_jenis");
			$id_kec = $this->input->post("id_kec");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"id_kec"=>$id_kec,
						"th"=>$th,
						"jml"=>$jml
					);

			$data_where = array(
							"id_bencana"=>$id_bencana,
						);

			$insert = $this->lb->update_bencana($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

		redirect('admin/bencana');
	}

	public function delete_lp_bencana($id_bencana){
		$delete = $this->lb->delete_bencana(array("id_bencana"=>$id_bencana));
		if($delete){
			echo "del";
			redirect('admin/bencana');
		}else {
			echo "fail";
		}
	}	
//================================================== LP BENCANA ==========================================//

//================================================== LP BENCANA JENIS ==========================================//
	private function validation_ins_lp_bencana_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_bencana_jenis(){
		if($this->validation_ins_lp_bencana_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->lb->insert_bencana_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
		redirect('admin/bencana');
	}	

	public function index_up_lp_bencana_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->lb->get_bencana_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_bencana_jenis(){
		if($this->validation_ins_lp_bencana_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->lb->update_bencana_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

		redirect('admin/bencana');
	}

	public function delete_lp_bencana_jenis($id_jenis){
		$delete = $this->lb->delete_bencana_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/bencana');
		}else {
			echo "fail";
		}
	}	
//================================================== LP BENCANA JENIS ==========================================//
}
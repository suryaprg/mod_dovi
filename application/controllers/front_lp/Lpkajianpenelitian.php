<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpkajianpenelitian extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_kajian_penelitian", "lkp");
		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

	public function index(){
		$data["page"] = "kajian";
		$data["list_kajian"] = $this->lkp->get_kajian();
		// $data["list_bencana_jenis"] = $this->lb->get_bencana_jenis();
		$this->load->view('front_lp/pendidikan/main_admin_pendidikan', $data);
	}

	private function validation_ins_lp_kajian(){
		$config_val_input = array(
            array(
                'field'=>'nama_kajian',
                'label'=>'Nama Kajian',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),            
            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}	

	public function insert_lp_kajian(){
		if($this->validation_ins_lp_kajian()){
			$nama_kajian = $this->input->post("nama_kajian");
			$th = $this->input->post("th");
			
			$data_insert = array(
							"id_kajian"=>"",
							"nama_kajian"=>$nama_kajian,
							"th"=>$th
						);
			$insert = $this->lkp->insert_kajian($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
		redirect('admin/kajian_penelitian');
	}	

	public function index_up_lp_kajian(){
		$id_kajian = $this->input->post("id_kajian");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->lkp->get_kajian_where(array("id_kajian"=>$id_kajian)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_kajian(){
		if($this->validation_ins_lp_kajian()){
			$id_kajian = $this->input->post("id_kajian");

			$nama_kajian = $this->input->post("nama_kajian");
			$th = $this->input->post("th");

			$data = array(
						
						"nama_kajian"=>$nama_kajian,
						"th"=>$th
					);

			$data_where = array(
							"id_kajian"=>$id_kajian,
						);

			$insert = $this->lkp->update_kajian($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}
		redirect('admin/kajian_penelitian');
	}

	public function delete_lp_kajian($id_kajian){
		$delete = $this->lkp->delete_kajian(array("id_kajian"=>$id_kajian));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect('admin/kajian_penelitian');
	}	
}

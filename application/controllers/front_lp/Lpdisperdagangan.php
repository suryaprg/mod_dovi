<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpdisperdagangan extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Lp_dis_perdagangan', 'lp');

        $this->load->library("response_message");

	}

#===========================================================================================================================
#-----------------------------------------------------Perdagangan--------------------------------------------------
#===========================================================================================================================
    public function perdagangan($th_fn){
        $jenis = $this->lp->get_lp_jenis();
        
        $str_title = "Data Dinas Perdagangan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"45%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"15%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" width=\"100%\">".
                                 $str_header_mod;            

            $t_data = array();
            $kategori = $this->lp->get_lp_kategori(array("id_jenis"=>$v_data->id_jenis));

            if($kategori){
                $r_sub = 0;
                foreach ($kategori as $r_sub => $v_sub) {
                    $data["list_data"][$row_jenis]["sub_jenis"][$r_sub]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                    $data["list_data"][$row_jenis]["sub_jenis"][$r_sub]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;

                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_sub_jenis."\">
                                    <td><a href=\"#\">".($r_sub+1)."</a></td>
                                    <td><a href=\"#\">".$v_sub->nama_sub_jenis."</a></td>";

                    $row_th = 0;
                    for ($i=$th_st; $i <= $th_fn ; $i++) {
                        $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_sub_jenis"=> $v_sub->id_sub_jenis, "th"=>$i));
                        // $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                        $data["list_data"][$row_jenis]["sub_jenis"][$r_sub]["value"][$i] = $val_data["jml"];

                        if($val_data["jml"]){
                            $str_tbl .= "<td>".$val_data["jml"]."</td>";
                            $jml = $val_data["jml"];
                        }else{
                            $str_tbl .= "<td>-</td>";
                            $jml = 0;
                        }
                        
                        $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                        $data_graph[$v_data->id_jenis][$v_sub->id_sub_jenis][$row_th] = $main_data;
                        $row_th++;

                    }
                    
                    $r_sub++;
                    $str_tbl .= "</tr>";
                }
            }else{
                    $data["list_data"][$row_jenis]["sub_jenis"][$r_sub]["main"]["id_sub_jenis"] = $v_data->id_jenis;
                    $data["list_data"][$row_jenis]["sub_jenis"][$r_sub]["main"]["nama_sub_jenis"] = $v_data->nama_jenis;

                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_0\">
                                    <td><a href=\"#\">1</a></td>
                                    <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

                    $row_th = 0;
                    for ($i=$th_st; $i <= $th_fn ; $i++) {
                        $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_sub_jenis"=> 0, "th"=>$i));
                        // $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                        $data["list_data"][$row_jenis]["sub_jenis"][0]["value"][$i] = $val_data["jml"];

                        if($val_data["jml"]){
                            $str_tbl .= "<td>".$val_data["jml"]."</td>";
                            $jml = $val_data["jml"];
                        }else{
                            $str_tbl .= "<td>-</td>";
                            $jml = 0;
                        }
                        
                        $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                        $data_graph[$v_data->id_jenis][0][$row_th] = $main_data;
                        $row_th++;

                    }
                    
                    
                    $str_tbl .= "</tr>";
            }
            
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        // $data["kategori"] = $kategori;
        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        print_r("<pre>");
        // print_r($data["data_graph"]);
        print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        // $this->load->view("front_lp/pdb/lp_pdb",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Perdagangan--------------------------------------------------
#===========================================================================================================================
}
?>
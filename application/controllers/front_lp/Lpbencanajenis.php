<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lpbencanajenis extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model("admin_opd/lp_bencana", "lb");
		$this->load->library("response_message");

			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }

	}

	public function index(){
		// $data["page"] = "iklim";
		// $data["list_bencana"] = $this->lb->get_bencana();
		$data["list_bencana_jenis"] = $this->lb->get_bencana_jenis();
		$this->load->view('admin_data/geo/v_lpbencanajenis', $data);
	}
	
	private function validation_ins_lp_bencana_jenis(){
		$config_val_input = array(
            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_bencana_jenis(){
		if($this->validation_ins_lp_bencana_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis
						);
			$insert = $this->lb->insert_bencana_jenis($data_insert);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			
			redirect('admin_opd/geo/Lpbencanajenis','refresh');
		}else {
			print_r(validation_errors());
		}
	}	

	public function index_up_lp_bencana_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->lb->get_bencana_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_bencana_jenis(){
		if($this->validation_ins_lp_bencana_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");

			$data = array(
						
						"nama_jenis"=>$nama_jenis
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->lb->update_bencana_jenis($data, $data_where);
				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
						
			redirect('admin_opd/geo/Lpbencanajenis','refresh');

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_lp_bencana_jenis($id_jenis){
		$delete = $this->lb->delete_bencana_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin_opd/geo/Lpbencanajenis','refresh');
		}else {
			echo "fail";
		}
	}					
}

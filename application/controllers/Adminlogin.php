<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminlogin extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('admin_super/main_admin', 'ma');
        
        $this->load->library("response_message");

        if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"]){
                redirect(base_url()."admin/keuangan_daerah");
            }
        }

	}
    
    public function index(){
        $this->load->view('login');
    }
    
    
    public function auth(){
        if($this->val_form_log()){
            $email = $this->input->post('email');
    		$password = $this->input->post('password');
    		$where = array(
    			'email' => $email,
    			'password' => md5($password),
                'status_active' => "1",
                'is_delete' => "0"
    			);
    		
            $cek = $this->ma->get_admin_where($where);
    		if(!empty($cek)){
                $cek["is_log"] = 1;
    			
                $this->session->set_userdata("admin_lv_1",$cek);
               
                $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("LOG_SUC"));
                $msg_array = $this->response_message->default_mgs($main_msg,null);
                $this->session->set_flashdata("response_login", $msg_array);         
                
    		}else{
                //echo "gagal";
                print_r($_SESSION);
                
                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
                $msg_array = $this->response_message->default_mgs($main_msg,null);
                $this->session->set_flashdata("response_login", $msg_array);         
                
    			// redirect(base_url("admin/login"));
    		}     
        }else{
            $msg_detail = array(
                                "email" => form_error("email"),
                                "password" => form_error("password")  
                            );
                            
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
            $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
            $this->session->set_flashdata("response_login", $msg_array);
            // redirect(base_url("admin/login"));
        }
        // print_r("<pre>");
        // print_r($_SESSION);

        redirect(base_url()."admin/keuangan_daerah");
    }
    
    private function val_form_log(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainfront extends CI_Controller {

	public function index(){
		$this->load->view('front/index');
	}

	public function profil(){
		$this->load->view('front/profile');
	}

	public function tentang(){
		$this->load->view('front/about');
	}

	public function unit_kerja(){
		$this->load->view('front/unit_kerja');
	}

	public function kepegawaian(){
		$this->load->view('front/kepegawaian');
	}

}

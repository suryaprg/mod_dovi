<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainsosial extends CI_Controller {


	function __construct(){
		parent::__construct();	
		$this->load->helper(array('form','url', 'text_helper','date'));
		$this->load->database();	
		// $this->load->model('m_cpm');
		// $this->load->model('Users_model');
		$this->load->library(array('Pagination','user_agent','session','form_validation','upload'));
	
	} 

	public function index(){
		$data['page'] = "Mainsosial/index";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('sospolkem/masterSospolkem',$data);
	
	}

	public function organisasiAgama(){
		$data['page'] = "Mainsosial/organisasiAgama";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('sospolkem/masterSospolkem',$data);
	}


	public function parpol(){
		$data['page'] = "Mainsosial/parpol";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('sospolkem/masterSospolkem',$data);
	}

	public function parlementeria(){
		$data['page'] = "Mainsosial/parlementeria";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('sospolkem/masterSospolkem',$data);
	}

	public function aparatKeamanan(){
		$data['page'] = "Mainsosial/aparatKeamanan";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('sospolkem/masterSospolkem',$data);
	}

	public function pidanaPenyelesaian(){
		$data['page'] = "Mainsosial/pidanaPenyelesaian";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('sospolkem/masterSospolkem',$data);
	}



}

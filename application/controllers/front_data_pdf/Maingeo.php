<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maingeo extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_super/Main_kecamatan", "mk");
		$this->load->model("admin_opd/Admin_iklim", "ai");
	}
	
	public function index(){
		$data['page'] = "geografi_wilayah";

		$data["kecamatan"] = array();
		$kecamatan = $this->mk->get_kec();
		foreach ($kecamatan as $r_kec => $v_kec) {
			$data["kecamatan"][$r_kec]["val_kec"] = $v_kec;
			$data["kecamatan"][$r_kec]["count_kel"] = count($this->mk->get_kel_all_where(array("mk.id_kec"=>$v_kec->id_kec)));
		}

		print_r("<pre>");
		print_r($data);
		// $this->load->view('geo/masterGeografi',$data);
	
	}

	public function geografi_wilayah(){
		$data['page'] = "geografi_wilayah";
		
		$data["kecamatan"] = array();
		$kecamatan = $this->mk->get_kec();
		
		foreach ($kecamatan as $r_kec => $v_kec) {
			$data["kecamatan"][$r_kec]["val_kec"] = $v_kec;
			$data["kecamatan"][$r_kec]["count_kel"] = count($this->mk->get_kel_all_where(array("mk.id_kec"=>$v_kec->id_kec)));
		}

		// print_r("<pre>");
		// print_r($data);

		$this->load->view('front_data/geo/masterGeografi',$data);
	
	}

	public function wilayahAdmin(){
		$data['page'] = "wilayah_admin";

		$str_title_data = "Pembagian Wilayah Administrasi di Kota Malang";

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<th width=\"5%\">Kecamatan</th>
                            <th>Kelurahan</th>";
        
        $t_col = 2;
  
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

		$str_tbl = "";
		$data["list_data"] = array();
		$row_data = 0;

		$kecamatan = $this->mk->get_kec();
		foreach ($kecamatan as $r_kec => $v_kec) {
			$val_all_kec = $this->mk->get_kel_all_where(array("mk.id_kec"=>$v_kec->id_kec));
			$data["list_data"][$r_kec]["val_kec"] = $v_kec;
			$data["list_data"][$r_kec]["val_kel"] = $val_all_kec;

			foreach ($val_all_kec as $r_kel => $v_kel) {
				if($r_kel == 0){
                    $str_tbl .= "<tr>
                        <td>".$v_kel->nama_kec."</td>
                        <td>".$v_kel->nama_kel."</td>
                    </tr>";
                }else {
                    $str_tbl .= "<tr>
	                        <td>&nbsp;</td>
	                        <td>".$v_kel->nama_kel."</td>
                        </tr>";
                }
			}

		}

		$data["str_tbl"] = $str_tbl;
		$data["str_header"] = $str_header_top.$str_header_mod;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Wilayah";

		//Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");
	}


	public function iklim($th_fn){
		$data['page'] = "iklim";
		$data_station = $this->ai->get_st();
		$bln = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

		$th_st = $th_fn;

		$str_title_data = "Rata - rata jumlah hari hujan dan curah hujan di Kota Malang Tahun ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<th rowspan=\"2\">No</th>";

       	$str_header_bot = "<tr>";

        $t_col = 1;
        foreach ($data_station as $key => $value) {
        	$str_header_mod .= "<th colspan=\"2\"> Stasiun ".$value->ket_st."</th>";
        	$str_header_bot .= "<th>Jumlah Hari</th>
        						<th>Jumlah Curah Hujan</th>";
        	$t_col+=2;
        }
        
        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";
		$row_data = 0;

		foreach ($bln as $r_bln => $v_bln) {
			$str_tbl .= "<tr>
							<td>".$v_bln."</td>";
			foreach ($data_station as $r_data_station => $v_data_station) {
				$val_data = $this->ai->get_iklim_where(array("periode"=>$r_bln+1, "id_st"=>$v_data_station->id_st));
				$str_tbl .= "<td align=\"right\">".$val_data["jml_hari"]."</td>
							<td align=\"right\">".$val_data["jml_cura"]."</td>";
			}
			$str_tbl .= "</tr>";
		}
		
		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;

		$data["str_tbl"] = $str_tbl;
		$data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Wilayah";

		//Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");
		
		// print_r("<table>".$data["str_header"].$str_tbl."</table>");
		// $this->load->view('front_data/geo/masterGeografi',$data);
	}

	public function sawahKebun(){
		$data['page'] = "Maingeo/sawahKebun";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('front_data/geo/masterGeografi',$data);
	}

	public function sungai(){
		$data['page'] = "Maingeo/sungai";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('front_data/geo/masterGeografi',$data);
	}

	public function hutan(){
		$data['page'] = "Maingeo/hutan";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('front_data/geo/masterGeografi',$data);
	}

	public function tamanRTH(){
		$data['page'] = "Maingeo/tamanRTH";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('front_data/geo/masterGeografi',$data);
	}
}

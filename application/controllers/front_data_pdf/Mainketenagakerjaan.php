<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainketenagakerjaan extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_ketenagakerjaan", "mk");
		
		$this->load->library("response_message");
	}

	public function index(){
		$this->kerja_angkatan(date("Y"));
	}

	public function kerja_angkatan($th_fn){
		$th_st = $th_fn-2;

		$str_title_data = "Persentase Penduduk menurut Angkatan Kerja di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Jumlah Yang Bekerja</th>
        					<th>Jumlah Yang Tidak Bekerja</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"3\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		$data["list_data"] = array();
		for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
			$val_data = $this->mk->get_kerja_angkatan(array("th"=>$i))->row_array();
			$data["list_data"][$i] = $val_data;

			$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>";

			
			if($val_data){
				$str_tbl .= "<td align=\"center\">".number_format($val_data["jml_kerja"], 0, ".", ",")." Jiwa</td>
                            <td align=\"center\">".number_format($val_data["jml_no_kerja"], 0, ".", ",")." Jiwa</td>";
			}else{
				$str_tbl .= "<td align=\"center\">0</td>
                            <td align=\"center\">0</td>";
			}

            $str_tbl .= "</tr>";

            $row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Ketenagakerjaan";	

       
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");


		// print_r("<pre>");
		// print_r($data);

		// $this->load->view("front_data/ketenagakerjaan/ketenagakerjaan_angkatan", $data);
	}

	public function kerja_pengangguran($th_fn){
		$th_st = $th_fn-2;
		
		$str_title_data = "Persentase Pengangguran di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Jumlah Pengangguran</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;
		for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
			$val_data = $this->mk->get_kerja_pengangguran(array("th"=>$i))->row_array();
			$data["list_data"][$i] = $val_data;

			$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>";
			
			if($val_data){
				$str_tbl .= "<td align=\"center\">".number_format($val_data["jml"], 0, ".", ",")." Jiwa</td>";
	            
			}else{
				$str_tbl .= "<td align=\"center\">0</td>";
			}

            $str_tbl .= "</tr>";

            $row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Ketenagakerjaan";	

       
        // //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

		// print_r("<pre>");
		// print_r($data);

		// $this->load->view('front_data_pdf/transportasi/jalan', $data);
		// $this->load->view("front_data/ketenagakerjaan/ketenagakerjaan_pengangguran", $data);
	}

	public function kerja_ump($th_fn){
		$th_st = $th_fn-2;

		$str_title_data = "Upah Minimum Provinsi di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Jumlah Upah Minimum Provinsi</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
			$val_data = $this->mk->get_kerja_ump(array("th"=>$i))->row_array();
			$data["list_data"][$i] = $val_data;

			$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>";

			
			if($val_data){
				$str_tbl .= "<td align=\"center\">Rp. ".number_format($val_data["jml"], 0, ".", ",")."</td>";
	            
			}else{
				$str_tbl .= "<td align=\"center\">0</td>";

			}

            $str_tbl .= "</tr>";

            $row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Ketenagakerjaan";	

       
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

		// print_r("<pre>");
		// print_r($data);

		// $this->load->view("front_data/ketenagakerjaan/ketenagakerjaan_ump", $data);
	}

}

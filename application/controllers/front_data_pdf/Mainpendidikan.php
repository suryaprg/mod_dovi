<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpendidikan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_pendidikan", "mp");
		
		$this->load->library("response_message");
	}

	public function index(){
		$this->pendidikan_baca_tulis(date("Y"));
	}

	public function pendidikan_baca_tulis($th_fn){
		$data["kategori"] = "";

		$th_st = $th_fn-2;
		$str_title_data = "Persentase Penduduk Usia 15 tahun ke Atas menurut Kemampuan Baca Tulis di Kota Malang Tahun ".($th_st)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<th>Tahun Data</th>";

       
        $str_header_mod .= "<th width=\"40%\">Huruf Latin dan  atau Lainnya</th>
        					<th width=\"40%\">Buta Huruf</th>";
       	$colspan=3;
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$colspan."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		$row_th = 0;
		for ($i=$th_st; $i<=$th_fn ; $i++) {
			$for_data = $this->mp->get_pendidikan_baca_tulis(array("th"=>$i))->result();

			if ($for_data) {
				$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>";
				$row_data = 0;
				
				foreach ($for_data as $key => $value) {
					$data["list_data"][$i][$row_data] = $value;

					if($value){
						$str_tbl .= "<td align=\"center\">".$value->prosentase." %</td>";
					}else{
						$str_tbl .= "<td align=\"center\">0</td>";
					}
					

					$row_data++;
				}

				$str_tbl .= "</tr>";
				$row_th++;
			}
			
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Pertanian";	

        
        // print_r("<pre>");
        // print_r($data);
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

		// print_r("<pre>");
		// print_r($data);
		// print_r($data["data_graph"]);
		// print_r("<table>".$data["str_header"].$str_tbl."</table>");

		// $this->load->view("front_data/pendidikan/pendidikan_baca_tulis", $data);
	}

	public function pendidikan_partisipasi_sklh($th_fn){
		$data["kategori"] = "";

		$th_st = $th_fn-2;
		

		$str_title_data = "Angka Partisipasi Sekolah (APS) Penduduk di Kota Malang Tahun ".($th_st)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<th>Tahun Data</th>";

       
        $str_header_mod .= "<th width=\"25%\">APS SD (7-12 tahun)</th>
        					<th width=\"25%\">APS SMP (13-15 tahun)</th>
        					<th width=\"25%\">APS SMA (16-18 tahun)</th>";
       	$colspan=4;
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$colspan."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		$row_th = 0;

		for ($i=$th_st; $i<=$th_fn ; $i++) {
			$for_data = $this->mp->get_pendidikan_partisipasi_sklh(array("th"=>$i))->result();
			if($for_data){
				$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>";
				$row_data = 0;
				$series = "";
				foreach ($for_data as $key => $value) {
					$data["list_data"][$i][$row_data] = $value;
					$str_tbl .= "<td align=\"center\">".$value->prosentase." %</td>";
					
					$row_data++;
				}

				$str_tbl .= "</tr>";
			}
			
			$row_th++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Pertanian";	

        
        // print_r("<pre>");
        // print_r($data);
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");		

		// print_r("<pre>");
		// print_r($data["data_graph"]);
		// print_r("<table>".$data["str_header"].$str_tbl."</table>");

		// $this->load->view("front_data/pendidikan/pendidikan_partisipasi_sklh", $data);
	}

	public function pendidikan_penduduk($th_fn){
		$data["kategori"] = "";
		
		$th_st = $th_fn-2;
		
		$str_title_data = "Angka Partisipasi Sekolah (APS) Penduduk di Kota Malang Tahun ".($th_st)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<th>Tahun Data</th>";

       
        $str_header_mod .= "<th width=\"17%\">Tidak Punya Ijazah SD</th>
        					<th width=\"17%\">SD / Sederajat</th>
        					<th width=\"17%\">SMP / Sederajat</th>
        					<th width=\"17%\">SMA / Sederajat</th>
        					<th width=\"17%\">PT / Universitas</th>";
       	$colspan=6;
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$colspan."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		$row_th = 0;

		for ($i=$th_st; $i<=$th_fn ; $i++) {
			$for_data = $this->mp->get_pendidikan_penduduk(array("th"=>$i))->result();
			if ($for_data) {
				$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>";

				$row_data = 0;
				foreach ($for_data as $key => $value) {
					$data["list_data"][$i][$row_data] = $value;
					$str_tbl .= "<td align=\"center\">".$value->prosentase." %</td>";

					$row_data++;
				}

				$str_tbl .= "</tr>";
			}

			$row_th++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Pertanian";	

        
        // print_r("<pre>");
        // print_r($data);
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
		// print_r("<pre>");
		// print_r($data);
		// print_r("<table>".$data["str_header"].$str_tbl."</table>");

		// $this->load->view("front_data/transportasi/jalan", $data);

		// $this->load->view("front_data/pendidikan/pendidikan_penduduk", $data);
	}

	
}

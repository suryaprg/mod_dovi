<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainkesehatan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_kesehatan", "mk");
		
		$this->load->library("response_message");
	}

	
	public function index(){
		$this->index_keuangan(date("Y"));
	
	}

	public function kesehatan_gizi_balita($th_fn){
		$data["list_data"] = array();

		$th_st = $th_fn-2;

		$str_title_data = "Jumlah Balita yang Kurang Gizi dan Gizi Buruk di Kota Malang";
		
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th width=\"10%\">No</th>
                                <th width=\"15%\">Tahun</th>
                                <th width=\"35%\">Jumlah Penduduk Kurang Gizi</th>
                                <th width=\"35%\">Jumlah Penduduk Gizi Buruk</th>";
                              

        $t_col = 4;
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

     	$str_tbl = "";
		$row_data = 1;

		$val_data = $this->mk->get_kesehatan_gizi_balita_all()->result();
		foreach ($val_data as $key => $value) {
			$str_tbl .= "<tr>
					<td align=\"center\">".$row_data."</td>
                    <td align=\"center\">".$value->th."</td>
                    <td align=\"center\">".number_format($value->jmh_kurang_gizi, 0, ".", ",")." jiwa</td>
                    <td align=\"center\">".number_format($value->jml_gizi_buruk, 0, ".", ",")." jiwa</td>
                 </tr>";

            $row_data++;
		}
			
		
		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Kesehatan";	

        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");


		$this->load->view("front_data/kesehatan/gizi_balita", $data);
	}

	public function main_jenis_penyakit($th_fn){
		$data["list_data"] = $this->mk->get_main_jenis_penyakit_all()->result();

		$th_st = $th_fn;

		$str_title_data = "Sepuluh Penyakit dengan Jumlah Kasus Terbanyak di Kota Malang Tahun ".($th_st);
		
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th width=\"10%\">No</th>
                                <th width=\"40%\">Jenis Penyakit</th>
                                <th width=\"40%\">Jumlah Penderita</th>";
                              

        $t_col = 3;
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------
		
		$str_val = "";
		$row_data = 1;
		foreach ($data["list_data"] as $key => $value) {
			$str_tbl .= "<tr>
					<td align=\"center\">".$row_data."</td>
                    <td align=\"left\">".$value->keterangan."</td>
                    <td align=\"center\">".number_format($value->jml, 0, ".", ",")." jiwa</td>
                 </tr>";
            $row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Kesehatan";	

        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");


		$this->load->view("front_data/kesehatan/gizi_balita", $data);

		
	}

}

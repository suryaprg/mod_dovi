<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainkependudukan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_kependudukan", "mk");
		$this->load->model("front_data/Main_miskin", "mm");
		
		$this->load->library("response_message");
	}

	public function index(){
		$this->index_pem_manusia(date("Y"));
	
	}

	public function index_pem_manusia($th_fn){
		$th_st = $th_fn-2;

		$str_title_data = "Indeks Pembangunan Manusia ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Index Pembangunan Manusia</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;
		
		for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
			$val_data = $this->mk->get_index_pem_manusia(array("th"=>$i))->row_array();

			$data["list_data"][$i] = $val_data;

			$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>
                            <td align=\"center\">".$val_data["ipm"]." %</td>
                        </tr>";
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Kependudukan";	

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");
	}

	public function index_kes_daya_pend($th_fn){
		$th_st = $th_fn-2;
		

		$str_title_data = "Perkembangan Indeks Kesehatan, Indeks Pendidikan, dan Indeks Daya Beli Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Daya Beli</th>
                            <th>Kesehatan</th>
                            <th>Pendidikan</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"4\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;
		for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
			$val_data = $this->mk->get_index_kes_daya_pend(array("th"=>$i))->row_array();

			$str_tbl .= "<tr>
				            <td align=\"center\">".$i."</a></td>
				            <td align=\"center\">".$val_data["daya_beli"]." %</td>
				            <td align=\"center\">".$val_data["kesehatan"]." %</td>
				            <td align=\"center\">".$val_data["pendidikan"]." %</td>
				        </tr>";


			$row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Kependudukan";	

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
	}

	public function kepandudukan_jk($th_fn){
		$th_st = $th_fn-2;

		$str_title_data = "Jumlah Penduduk menurut Jenis Kelamin di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Laki-Laki</th>
                            <th>Perempuan</th>
                            <th>Rasio</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"4\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		for ($i=$th_fn-2; $i<=$th_fn ; $i++) {
			$val_data = $this->mk->get_kepandudukan_jk(array("th"=>$i))->row_array();

			// print_r($val_data);
			$data["list_data"][$i] = $val_data;

			$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>
                            <td align=\"center\">".$val_data["t_cowo"]." jiwa</td>
                            <td align=\"center\">".$val_data["t_cewe"]." jiwa</td>
                            <td align=\"center\">".$val_data["rasio_jk"]." %</td>
                        </tr>";
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
				
		$data["kategori"] = "Kependudukan";	

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
    }

	public function kependudukan_kel_umur($th_fn){
		$jenis = $this->mk->get_kependudukan_kel_umur_jenis()->result();
		$th_st = $th_fn-2;

		$str_title_data = "Jumlah Penduduk menurut Kelompok Umur di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

        foreach ($jenis as $key => $value) {
			$str_header_mod .= "<th>".$value->kelompok."</th>";		
		}

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"4\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
			$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>";
                         
			foreach ($jenis as $key => $value) {
				$data_umur =  $this->mk->get_kependudukan_kel_umur(array("periode"=>$i, "kelompok"=>$value->kelompok))->row_array();
				$data["list_data"][$i][$key] = $data_umur;

				$str_tbl .= "<td align=\"center\">".number_format($data_umur["jml_penduduk"],0,".",",")." jiwa</td>";

			}

			$str_tbl .= "</tr>";
			$row_data++;
		}
		
		
		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Kependudukan";	

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
	
		
		// print_r("<pre>");
		// print_r($data);
		// print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");

		// $this->load->view("front_data/kependudukan/penduduk_umur", $data);
	}

	public function kependudukan_rasio_ketergantungan($th_fn){
		$th_st = $th_fn-2;
		
		$str_title_data = "Jumlah Penduduk menurut Jenis Kelamin di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Rasio ketergantungan</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"2\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
			$val_data = $this->mk->get_kependudukan_rasio_ketergantungan(array("th_rasio"=>$i))->row_array();
			$data["list_data"][$i] = $val_data["rasio_keter"];
			
			$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>
                            <td align=\"center\">".$val_data["rasio_keter"]." %</td>
                        </tr>";

            
			$row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Kependudukan";	

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
	}

	public function miskin_jml_pend($th_fn){
		$th_st = $th_fn-2;
		

		$str_title_data = "Jumlah Penduduk Miskin Menurut Jenis Kelamin di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Agregat</th>
        					<th>Prosentase</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"3\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;
		// $str_val = "";
		// $str_pro = "";
		for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 

			$agregat = 0;
			$prosentase = 0;

			$val_data = $this->mm->get_miskin_jml_pend(array("th"=>$i))->row_array();
			if($val_data){
				$agregat = $val_data["agregat"];
				$prosentase = $val_data["prosentase"];
			}

			$data["list_data"][$i] = $val_data;
			$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>
                            <td align=\"right\">".$agregat."</td>
                            <td align=\"right\">".$prosentase."</td>
                        </tr>";

			$row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Kependudukan";	

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
	}

	public function miskin_pengeluaran_perkapita($th_fn){
		$th_st = $th_fn-2;

		$str_title_data = "Persentase Penduduk menurut Golongan Pengeluaran Perkapita di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Agregat</th>
        					<th>Prosentase</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"3\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 

			$agregat = 0;
			$prosentase = 0;

			$val_data = $this->mm->get_miskin_pengeluaran_perkapita(array("th"=>$i))->row_array();
			if($val_data){
				$agregat = $val_data["pengeluaran_perkapita"];
				$prosentase = $val_data["garis_kemiskinan"];
			}
			
			$data["list_data"][$i] = $val_data;

			$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>
                            <td align=\"right\">".number_format($agregat, 0, ".", ",")."</td>
                            <td align=\"right\">".number_format($prosentase, 0, ".", ",")."</td>
                        </tr>";
            
			$row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Kependudukan";	

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");	
	}

}

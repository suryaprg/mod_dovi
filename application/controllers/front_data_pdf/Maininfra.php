<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maininfra extends CI_Controller {

	public function index(){
		$data['page'] = "Maininfra/index";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('infrastruktur/masterInfrastruktur',$data);
	
	}

	public function jembatan(){
		$data['page'] = "Maininfra/jembatan";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('infrastruktur/masterInfrastruktur',$data);
	}


	public function terminal(){
		$data['page'] = "Maininfra/terminal";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('infrastruktur/masterInfrastruktur',$data);
	}

	public function stasiun(){
		$data['page'] = "Maininfra/stasiun";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('infrastruktur/masterInfrastruktur',$data);
	}

	public function relKereta(){
		$data['page'] = "Maininfra/relKereta";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('infrastruktur/masterInfrastruktur',$data);
	}

	public function BTS(){
		$data['page'] = "Maininfra/BTS";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('infrastruktur/masterInfrastruktur',$data);
	}

		public function saranaP(){
		$data['page'] = "Maininfra/saranaP";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('infrastruktur/masterInfrastruktur',$data);
	}

	public function kendaraanMotor(){
		$data['page'] = "Maininfra/kendaraanMotor";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('infrastruktur/masterInfrastruktur',$data);
	}


}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpemerintahan extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model("front_data/Main_pemerintahan", "mp");
		
		$this->load->library("response_message");
	}

	public function index(){
		$this->pem_jml_rt_rw(date("Y"));
	}

	public function pem_jml_rt_rw($th_fn){
		$kecamatan = $this->mp->get_kecamatan();

		$th_st = $th_fn;

		$str_title_data = "Jumlah RW dan RT menurut Kecamatan di Kota Malang Tahun ".($th_st);
		
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <th width=\"5%\" rowspan=\"2\">No</th>
                                <th width=\"35%\" rowspan=\"2\">Kecamatan</th>";
        $str_header_bot = "<tr>";                        

        $t_col = 2;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th colspan=\"2\">".$i."</th>";

			$str_header_bot .= "<th width=\"10%\">RT</th>
								<th width=\"10%\">RW</th>";

            $t_col += 2;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";
        $row_jenis = 0;
        

        $t_rt = 0;
        $t_rw = 0; 
        foreach ($kecamatan as $r_data => $v_data) {
        	$data["list_data"][$row_jenis]["main"]["nama_kecamatan"] = $v_data->nama_kec;
            $data["list_data"][$row_jenis]["main"]["id_kecamatan"] = $v_data->id_kec;

            $str_tbl .= "<tr>
            				<td align=\"center\">".($row_jenis+1)."</td>
            				<td>".$v_data->nama_kec."</td>";

            // $data_graph[$row_jenis]

            $row_th = 0;
        	for ($i=$th_st; $i <= $th_fn ; $i++) {
        		$val_data = $this->mp->get_pem_jml_rt_rw(array("th"=>$i, "mk.id_kec"=>$v_data->id_kec))->row_array();
        		$data["list_data"][$row_jenis]["detail"][$i] = $val_data;


        		$str_tbl .="<td align=\"center\">".$val_data["jml_rt"]."</td>
        					<td align=\"center\">".$val_data["jml_rw"]."</td>";

        		
        		$t_rt += $val_data["jml_rt"];
        		$t_rw += $val_data["jml_rw"];

        		$row_th++;
        	}
        	$str_tbl .= "</tr>";

			$row_jenis++;
		}
		

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Pemerintahan";	

       
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");
		// print_r("<pre>");
		// print_r($data["data_graph"]);
		// print_r("<table>".$data["str_header"].$str_tbl."</table>");


		// $this->load->view('front_data/pemerintahan/rt_rw',$data);		
	}
	// public function kepegawaian(){
	// 	$data['page'] = "kepegawaian";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	
	// }

	// public function realisasi_pendapatan(){
	// 	$data['page'] = "realisasi_pendapatan";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }


	// public function realisasi_belanja(){
	// 	$data['page'] = "realisasi_belanja";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function eksekutif(){
	// 	$data['page'] = "eksekutif";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function legislatif(){
	// 	$data['page'] = "legislatif";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function dinas(){
	// 	$data['page'] = "dinas";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function badanKantor(){
	// 	$data['page'] = "badanKantor";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function bagian(){
	// 	$data['page'] = "bagian";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function kecamatanKelurahan(){
	// 	$data['page'] = "kecamatanKelurahan";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function rtRw($th_mulai){
	// 	$data['page'] = "rtRw";
		
	// 	$data_kecamatan = $this->mk->get_kec();
	// 	$single_data = array();
	// 	foreach ($data_kecamatan as $r_data_kecamatan => $v_data_kecamatan) {
	// 		$data["tr_rw"][$r_data_kecamatan]["keceamatan"] = $v_data_kecamatan;
	// 		$data["tr_rw"][$r_data_kecamatan]["single_data"] = $this->atw->get_rt_rw_where(array("id_kec"=>$v_data_kecamatan->id_kec, "th"=>$th_mulai))->result();
	// 		$data["tr_rw"][$r_data_kecamatan]["multi_data"] = null; 
	// 	}

	// 	// print_r("<pre>");
	// 	// print_r($data);
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

}

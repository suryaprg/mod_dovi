<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintehnologi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_tehnologi", "mt");
		
		$this->load->library("response_message");
	}

	public function index(){
		$this->tehnologi_jml_web_opd(date("Y"));
	}

	public function tehnologi_jml_bts($th_fn){
		$data["page"] = "bts";
		$data["kategori"] = "";

		$th_st = $th_fn-2;
		
		$str_title_data = "Jumlah Base Transceiver Station (BTS) di Kota Malang Tahun ".$th_st." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            	<th width=\"10%\">No.</th>
                                <th width=\"40%\">Periode (Tahun)</th>
                                <th width=\"40%\">Jumlah BTS</th>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <b><td colspan=\"3\" align=\"center\">".$str_title_data."</td></b>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
		$data["list_data"] = array();

		$str_tbl = "";
		$row_data = 1;
		for ($i=$th_st; $i<=$th_fn ; $i++) { 
			$data_val = $this->mt->get_tehnologi_jml_bts(array("th"=>$i))->row_array();
			$data["list_data"][$i] = $data_val;

			$str_tbl .= "<tr>
                            <td align=\"center\">".$row_data."</td>
                            <td align=\"center\">".$i."</td>
                            <td align=\"center\">".$data_val["jml"]." unit</td>
                        </tr>";
            $row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Tehnologi";	

        
        // print_r("<pre>");
        // print_r($data);
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");

	}

	public function tehnologi_jml_warnet($th_fn){
		$data["page"] = "warnet";
		$data["kategori"] = "";

		$th_st = $th_fn-2;
		
		$str_title_data = "Jumlah Base Warung Internet (Warnet) di Kota Malang Tahun ".$th_st." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            	<th width=\"10%\">No.</th>
                                <th width=\"40%\">Periode (Tahun)</th>
                                <th width=\"40%\">Jumlah Warung Internet</th>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"3\" align\"center\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
		$data["list_data"] = array();

		$str_tbl = "";
		$row_data = 1;
		for ($i=$th_st; $i<=$th_fn ; $i++) {
			$data_val = $this->mt->get_tehnologi_jml_warnet(array("th"=>$i))->row_array();
			$data["list_data"][$i] = $data_val;

			$str_tbl .= "<tr>
                            <td align=\"center\">".$row_data."</td>
                            <td align=\"center\">".$i."</td>
                            <td align=\"center\">".$data_val["jml"]." unit</td>
                        </tr>";
            $row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;	

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Tehnologi";	

        
        // print_r("<pre>");
        // print_r($data);
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");
	}

	public function tehnologi_jml_web_opd($th_fn){
		$data["kategori"] = "";
		
		$th_st = $th_fn-2;
		
		$str_title_data = "Jumlah Website SKPD Pemkot Malang di Kota Malang Tahun ".$th_st." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            	<th width=\"10%\">No.</th>
                                <th width=\"40%\">Periode (Tahun)</th>
                                <th width=\"40%\">Jumlah Website SKPD Pemkot Malang</th>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"3\" align\"center\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
		$data["list_data"] = array();

		$str_tbl = "";
		$row_data = 1;

		for ($i=$th_st; $i<=$th_fn ; $i++) { 
			$data_val = $this->mt->get_tehnologi_jml_web_opd(array("th"=>$i))->row_array();

			$str_tbl .= "<tr>
                            <td align=\"center\">".$row_data."</td>
                            <td align=\"center\">".$i."</td>
                            <td align=\"center\">".$data_val["jml"]." domain</td>
                        </tr>";

			$data["list_data"][$i] = $data_val;

		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;	

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Tehnologi";	
        
        // print_r("<pre>");
        // print_r($data);
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."/assets/pdfs/".$data["kategori"].".pdf");
	}
}

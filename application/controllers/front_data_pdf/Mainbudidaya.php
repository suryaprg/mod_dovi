<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainbudidaya extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_budidaya", "mb");
		
		$this->load->library("response_message");
	}

	public function index(){
		$this->pertanian_lahan(date("Y"));
	}

	public function pertanian_lahan($th_fn){
		$jenis = $this->mb->get_jenis_pertanian_lahan()->result();

		$th_st = $th_fn-2;
		

		$str_title_data = "Persentase Luas Lahan menurut Jenis Lahan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <th>No</th>
                                <th>Keterangan</th>";

        $t_col = 2;

        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";

            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;
		$series = "";
		foreach ($jenis as $r_data => $v_data) {
			$data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->jenis_lahan;

			$str_tbl.= "<tr>
							<td align=\"center\">".($row_data+1)."</a></td>
							<td>".$v_data->jenis_lahan."</a></td>";

			$row_th = 0;

			for ($i=$th_st; $i<=$th_fn ; $i++) { 
				$val_data =$this->mb->get_pertanian_lahan(array("th"=>$i, "jenis_lahan"=>$v_data->jenis_lahan))->row_array();
				// print_r($val_data);
				$data["list_data"][$r_data]["detail"][$i] = $val_data;

				if($val_data){
					$str_tbl .= "<td align=\"center\">".$val_data["luas_lahan"]." %</td>";
				}else{
					$str_tbl .= "<td align=\"center\">0</td>";
				}

				$row_th++;
			}

			$row_data++;

			$str_tbl.= "</tr>";
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;

		$data["kategori"] = "Pertanian";	
		
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
		

		$this->load->view("front_data/ekonomi/pendapatan_usaha", $data);
		
	}

	public function pertanian_lahan_penggunaan($th_fn){
			$th_st = $th_fn;

		$str_title_data = "Data Lahan Pertanian di Kota Malang Tahun ".($th_st);

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<th>No</th>";

       
        $str_header_mod .= "<th width=\"50%\">Kategori</th>
        					<th width=\"30%\">Jumlah/Prosentase</th>";
       	$colspan=3;
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$colspan."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";
        $str_tbl_1 = "";

        $str_tbl .= "<tr>
                            <td colspan=\"".$colspan."\" align=\"center\">Luas Lahan</td>
                        </tr>";
		$list_data = $this->mb->get_pertanian_lahan_penggunaan(array("th"=>$th_fn))->result();
		$row_th = 1; 
		foreach ($list_data as $r_list_data => $v_list_data) {
			$str_tbl .= "<tr>
							<td align=\"center\">".$row_th."</td>
							<td>".$v_list_data->jenis_peng."</td>";
			if($v_list_data){
				$str_tbl .= "<td align=\"center\">".number_format($v_list_data->luas_pend,0,".",",")." Hektar</td>";
			}else {
				$str_tbl .= "<td align=\"center\">0</td>";
			}
			
			$str_tbl .= "</tr>";

			$row_th++;
		}
		$str_tbl .= "<tr>
                            <td colspan=\"".$colspan."\" align=\"center\">Prosentase Penggunaan Lahan</td>
                        </tr>";
		
		$row_th = 1;
		$list_data_pn = $this->mb->get_pertanian_lahan(array("th"=>$th_fn))->result();
		foreach ($list_data_pn as $r_list_data_pn => $v_list_data_pn) {
			$str_tbl .= "<tr>
							<td align=\"center\">".$row_th."</td>
							<td>".$v_list_data_pn->jenis_lahan."</td>";
			if($v_list_data){
				$str_tbl .= "<td align=\"center\">".number_format($v_list_data_pn->luas_lahan,2,".",",")."%</td>";
			}else {
				$str_tbl .= "<td align=\"center\">0</td>";
			}
			
			$str_tbl .= "</tr>";
			$row_th++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;

		$data["list_data"] = $list_data;
		$data["list_data_pn"] = $list_data_pn;

		$data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["kategori"] = "Pertanian";

        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

		print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

		// print_r("<pre>");
		// print_r($data);

		// $this->load->view("front_data/pertanian/lahan", $data);

		// $this->load->view("front_data/pertanian/lahan", $data);
	}

	public function pertanian_kelapa_tebu($th_fn){

		$data["kategori"] = "";
		
		$th_st = $th_fn-2;

		$str_title_data = " Produksi Tanaman Kelapa dan Tebu di Kota Malang Tahun ".($th_st)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<th>Tahun Data</th>";

       
        $str_header_mod .= "<th width=\"40%\">Kelapa</th>
        					<th width=\"40%\">Tebu</th>";
       	$colspan=3;
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$colspan."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;


		$row_th = 0;

		for ($i=$th_st; $i<=$th_fn ; $i++) {
			$for_data = $this->mb->get_pertanian_kelapa_tebu(array("th"=>$i))->row_array();

			$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>";
			if($for_data){
				$str_tbl .= 	"<td align=\"center\">".number_format($for_data["jml_kelapa"],2,".",",")." buah</td>
							<td align=\"center\">".number_format($for_data["jml_tebu"],2,".",",")." buah</td>";
			}else {
				$str_tbl .= 	"<td align=\"center\">0</td>
							<td align=\"center\">0</td>";
			}
			
			$str_tbl .= "</tr>";

			$row_th++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Pertanian";	
		
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
		
	}

	public function pertanian_komoditas($th_fn){
		$jenis = $this->mb->get_pertanian_komoditas_jenis();
		$data = array();

		$th_st = $th_fn-2;
		
		$str_title_data = "Jumlah Komoditi di Kota Malang Tahun ".($th_st)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<th>No.</th>
        					<th>Keterangan</th>";

       	$t_col=2;
       	for ($i=$th_st; $i<=$th_fn ; $i++) {
        	$str_header_mod .= "<th>".$i."</th>";
        	$t_col++;
        }
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		$row_th = 0;
		foreach ($jenis as $r_data => $v_data) {
			$data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data;
			$str_tbl .= "<tr>
							<td align=\"center\">".($row_data+1)."</td>
				            <td>".$v_data->nama_jenis."</a></td>";

			for ($i=$th_st; $i<=$th_fn ; $i++) { 
				$val_data = $this->mb->get_pertanian_komoditas(array("pk.id_jenis"=>$v_data->id_jenis, "th"=>$i))->row_array();
				$data["list_data"][$r_data]["detail"][$i] = $val_data;

				$str_tbl .= "<td align=\"center\">".number_format($val_data["jml"], 0, ".", ",")."</td>";
			}

			$str_tbl .= "</tr>";

			$row_data++;
		}
		
		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Pertanian";	
		
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");	

		// print_r("<pre>");
		// print_r($data);

		// $this->load->view("front_data/pertanian/komoditi", $data);
	}

	public function pertanian_holti_jml($th_fn){
		$data["page"] = "hortikultura";
		$jenis = $this->mb->get_pertanian_holti_jenis();

		$th_st = $th_fn;
		
		$str_title_data = "Jumlah Komoditi Holtikultural di Kota Malang Tahun ".($th_st);

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<th width=\"5%\">No.</th>
        					<th>Keterangan</th>
        					<th width=\"25%\">Total Panen</th>";
        $t_col = 3;
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		$row_th = 1;

		foreach ($jenis as $r_data => $v_data) {
			$val_data = $this->mb->get_pertanian_holti_jml(array("pk.id_jenis"=>$v_data->id_jenis, "th"=>$th_fn))->row_array();
			$data["list_data"][$r_data]= $val_data;
			if ($val_data) {
				$str_tbl .= "<tr>
							<td align=\"center\">".$row_th."</td>
							<td>".$val_data["nama_jenis"]."</td>
							<td align=\"center\">".number_format($val_data["jml"],2,".",",")."</td>
						</tr>";
			
			}
			$row_th++;
		}
		
		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Pertanian";	
		
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");	
	}

	public function pertanian_ternak($th_fn){
		$jenis = $this->mb->get_pertanian_ternak_jenis();
		
		$th_st = $th_fn-2;
		
		$str_title_data = "Produksi Peternakan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <th>No</th>
                                <th>Keterangan</th>
                                <th>Jenis Ternak</th>";

        $t_col = 3;

        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";

            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;
		
		foreach ($jenis as $r_data => $v_data) {
			$data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->nama_jenis;
			$data["list_data"][$r_data]["main"]["id_jenis"] = $v_data->id_jenis;

			$str_tbl.= "<tr>
							<td align=\"center\">".($row_data+1)."</a></td>
							<td>".$v_data->nama_jenis."</a></td>";
			if($v_data->kategori == 0){
				$str_tbl.= "	<td>Hasil Ternak Non Unggas</td>";
			}elseif ($v_data->kategori == 1) {
				$str_tbl.= "	<td>Hasil Ternak Unggas</td>";
			}else {
				$str_tbl.= "	<td>Hasil Ternak Telur</td>";
			}


			$row_th = 0;

			for ($i=$th_st; $i<=$th_fn ; $i++) { 
				$val_data = $this->mb->get_pertanian_ternak_jml(array("pk.id_jenis"=>$v_data->id_jenis, "th"=>$i))->row_array();
				// print_r($val_data);
				$data["list_data"][$r_data]["detail"][$i] = $val_data;

				if(is_numeric($val_data["jml"])){
					$str_tbl .= "<td align=\"center\">".number_format($val_data["jml"],2,".",",")."</td>";
				}else{
					$str_tbl .= "<td align=\"center\">0</td>";
				}

				$row_th++;
			}

			$row_data++;

			$str_tbl.= "</tr>";
		}
		
		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Pertanian";	
		
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
		

		$this->load->view("front_data/ekonomi/pendapatan_usaha", $data);
		
	}

	public function pertanian_ikan($th_fn){
		$jenis = $this->mb->get_pertanian_ikan_jenis();

		$th_st = $th_fn-2;

		$str_title_data = "Produksi Perikanan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <th>No</th>
                                <th>Keterangan</th>
                                <th>Jenis Ikan</th>";

        $t_col = 3;

        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";

            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;
		
		foreach ($jenis as $r_data => $v_data) {
			$data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->nama_jenis;
			$data["list_data"][$r_data]["main"]["id_jenis"] = $v_data->id_jenis;

			$str_tbl.= "<tr>
							<td align=\"center\">".($row_data+1)."</a></td>
							<td>".$v_data->nama_jenis."</a></td>";

			if($v_data->kategori == 0){
				$str_tbl.= "	<td>Budidaya Ikan Dalam Kolam</td>";
			}else{
				$str_tbl.= "	<td>Budidaya Ikan Dalam Keramba</td>";
			}

			

			$row_th = 0;
			for ($i=$th_st; $i<=$th_fn ; $i++) { 
				$val_data = $this->mb->get_pertanian_ikan_jml(array("pk.id_jenis"=>$v_data->id_jenis, "th"=>$i))->row_array();
				$data["list_data"][$r_data]["detail"][$i] = $val_data;

				if(is_numeric($val_data["jml"])){
					$str_tbl .= "<td align=\"center\">".number_format($val_data["jml"], 2, ".", ",")." Kg</td>";
				}else{
					$str_tbl .= "<td align=\"center\">0</td>";
				}

				$row_th++;
			}

			$row_data++;

			$str_tbl.= "</tr>";
		}
		
		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Pertanian";	
		
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
		

		$this->load->view("front_data/ekonomi/pendapatan_usaha", $data);
		
	}

	
	
}
	
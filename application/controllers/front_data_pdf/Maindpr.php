<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Maindpr extends CI_Controller{

    public function __construct(){
        parent::__construct();  
        $this->load->model('front_data/Main_dpr', 'lp');
        
        $this->load->library("response_message");

    }

#===========================================================================================================================
#------------------------------------------------------------DPS-------------------------------------------------------
#===========================================================================================================================

    public function dpr($th_fn){
        $jenis = $this->lp->get_lp_jenis();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------

        $str_header_top = "<tr>
                            <th colspan=\"5\">Data Jumlah Anggota Parlemen Kota Malang Tahun ".$th_fn."</th>
                          </tr>
                          <tr>
                            <th>No.</th>
                            <th>Nama Partai</th>
                            <th>Laki-Laki</td>
                            <th>Perempuan</th>
                            <th>Jumlah Anggota</th>
                          </tr>";


        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";
        // $series = "";

        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_partai;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_partai;

            // $data_graph[$v_data->id_partai]["main"]["id_partai"] = $v_data->id_partai;
            // $data_graph[$v_data->id_partai]["main"]["nama_jenis"] = $v_data->nama_partai;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_partai."\">
                            <td align=\"center\"><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_partai."</a></td>";

            $val_data_l = $this->lp->get_lp(array("id_partai"=>$v_data->id_partai, "th"=>$th_fn, "jk"=>0));
            $jml_l = 0;
            if($val_data_l){
                $jml_l = $val_data_l["jml"];
            }
            $data["list_data"]["value"][0] = $jml_l;

            $val_data_p = $this->lp->get_lp(array("id_partai"=>$v_data->id_partai, "th"=>$th_fn, "jk"=>1));
            $jml_p = 0;
            if($val_data_p){
                $jml_p = $val_data_p["jml"];
            }
            $data["list_data"]["value"][1] = $jml_p;

            $str_tbl .= "<td align=\"center\">".$jml_l."</td>
                        <td align=\"center\">".$jml_p."</td>
                        <td align=\"center\">".($jml_l+$jml_p)."</td>";

            // "year"=>(string)$th_fn, 

            $main_data = array("nama_jenis"=> $v_data->nama_partai, "val_l"=>(float)$jml_l, "val_p"=>(float)$jml_p, "val_jml"=>(float)($jml_l+$jml_p));
            $data_graph[$row_jenis] = $main_data;

            $str_tbl .= "</tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // $data["title"] = $str_title_data;
        $data["kategori"] = "Parlemen";  
        
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
        // $data["series"] = $series;

        // print_r("<pre>");
        // print_r($data["data_graph"]);

        // $this->load->view("front_data/parlemen/main_dpr", $data);
        print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }

#===========================================================================================================================
#------------------------------------------------------------DPS-------------------------------------------------------
#===========================================================================================================================
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainekonomi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_ekonomi", "me");
		
		$this->load->library("response_message");
	}

	public function index(){
		// $this->index_keuangan(date("Y"));
		$data["page"] = "pendapatan_regional";

		$this->load->view("front_data/ekonomi/main_ekonomi", $data);
	
	}

	public function index_pendapatan_usaha($th_fn){
		$jenis = $this->me->get_pendapatan_usaha_jenis();

		$th_st = $th_fn-2;
		$data_graph = array();

		$str_title_data = "Data Pendapatan Lapangan Usaha Kota Malang Tahun ".($th_st)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <th>No</th>
                                <th>Keterangan</th>";

        $t_col = 2;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";
            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		$series = "";

		foreach ($jenis as $r_data => $v_data) {
			$data["pendapatan_usaha"][$r_data]["main"]["id_jenis"]= $v_data->id_jenis;
			$data["pendapatan_usaha"][$r_data]["main"]["nama_jenis"]= $v_data->nama_jenis;

			$str_tbl.= "<tr>
							<td align=\"center\">".($row_data+1)."</td>
							<td>".$v_data->nama_jenis."</a></td>";
			
			$row_th = 0;
			for ($i=$th_st; $i<=$th_fn ; $i++) { 
				$val_data = $this->me->get_pendapatan_usaha(array("th"=>$i, "plu.id_jenis"=>$v_data->id_jenis))->row_array();
				$data["list_data"][$row_data]["detail"][$i] = $val_data;

				if($val_data){
					$str_tbl .= "<td align=\"center\">".number_format($val_data["jml"], 2, ".", ",")." %</td>";
				}else{
					$str_tbl .= "<td align=\"center\">0</td>";
				}

				$row_th++;
			}
			$str_tbl.= "</tr>";

			$row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Ekonomi";	
		
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
		

		$this->load->view("front_data/ekonomi/pendapatan_usaha", $data);
	}


	public function index_pendapatan_regional($th_fn){
		$th_st = $th_fn-2;
		
		$str_title_data = "PDRB Kota Malang Atas Dasar Harga Berlaku di Kota Malang Tahun ".($th_st)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<th width=\"10%\">Tahun Data</th>";

       
        $str_header_mod .= "<th width=\"60%\">Jumlah Pendapatan Regional</th>";
       	$colspan=2;
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$colspan."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		$row_th = 0;

		for ($i=$th_st; $i<=$th_fn ; $i++) {
			$for_data = $this->me->get_pendapatan_regional(array("th_pendapatan"=>$i))->row_array();
			$data["list_data"][$i] = $for_data;


			$str_tbl .= "<tr>
							<td align=\"center\">".$i."</td>";

			$str_tbl .= "	<td align=\"center\">Rp. ".number_format($for_data["jml_pendapatan"], 2, ".", ",")."</td>";
			
			$str_tbl .= "</tr>";

			$row_th++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Ekonomi";	
		
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
		

		$this->load->view("front_data/ekonomi/pendapatan_usaha", $data);
		
	}

#==========================================================================================================
#--------------------------------------------------------Keuangan------------------------------------------
#==========================================================================================================
	
	public function index_keuangan($th_fn){
		$data["_get_realisasi"] = $this->_get_realisasi($th_fn);
		$data["_get_belanja"] = $this->_get_realisasi($th_fn);
		
	}

	public function get_realisasi($th_fn){
		$jenis = $this->me->get_keuangan_jenis_kate(array("kategori"=>"0"));

		$th_st = $th_fn-2;

		$str_title_data = "Data Realisasi Pendapatan Daerah ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <th>No</th>
                                <th>Keterangan</th>";

        $t_col = 2;

        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";

            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		foreach ($jenis as $r_data => $v_data) {
			$data["list_data"][$r_data]["main"]["id_jenis"] = $v_data->id_jenis;
			$data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->ket;

			$str_tbl.= "<tr>
							<td align=\"center\"><a href=\"#\">".($row_data+1)."</a></td>
							<td><a href=\"#\">".$v_data->ket."</a></td>";
			
			$row_th = 0;

			for ($i=$th_st; $i<=$th_fn ; $i++) { 
				$val_data = $this->me->get_keuangan(array("kjm.id_jenis"=>$v_data->id_jenis, "th"=>$i))->row_array();
				$data["list_data"][$row_data]["detail"][$i] = $val_data;

				if($val_data){
					$str_tbl .= "<td align=\"right\">Rp. ".number_format($val_data["jml"], 2, ".", ",")."</td>";
				}else{
					$str_tbl .= "<td align=\"right\">0</td>";
				}

				$row_th++;
			}

			$row_data++;

			$str_tbl.= "</tr>";
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Ekonomi";	
		
		//Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
	}

	public function get_belanja($th_fn){
		$jenis = $this->me->get_keuangan_jenis_kate(array("kategori"=>"1"));

		$th_st = $th_fn-2;

		$str_title_data = "Data Realisasi Belanja Daerah ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <th>No</th>
                                <th>Keterangan</th>";

        $t_col = 2;

        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";

            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;
		
		foreach ($jenis as $r_data => $v_data) {
			$data["list_data"][$r_data]["main"]["id_jenis"] = $v_data->id_jenis;
			$data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->ket;

			$str_tbl.= "<tr>
							<td align=\"center\">".($row_data+1)."</td>
							<td>".$v_data->ket."</td>";
			
			$row_th = 0;

			for ($i=$th_st; $i<=$th_fn ; $i++) { 
				$val_data = $this->me->get_keuangan(array("kjm.id_jenis"=>$v_data->id_jenis, "th"=>$i))->row_array();
				$data["list_data"][$row_data]["detail"][$i] = $val_data;

				if($val_data){
					$str_tbl .= "<td align=\"right\">Rp. ".$val_data["jml"]."</td>";
				}else{
					$str_tbl .= "<td align=\"right\">0</td>";
				}

				$row_th++;
			}

			$row_data++;

			$str_tbl.= "</tr>";
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Ekonomi";
		
		//Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
	}

#==========================================================================================================
#--------------------------------------------------------Keuangan------------------------------------------
#==========================================================================================================

	public function pengeluaran_penduduk($th_fn){
		$jenis = $this->me->get_pengeluaran_penduduk_jenis();

		$th_st = $th_fn-2;

		$str_title_data = "Data Pengeluaran Penduduk Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <th>No</th>
                                <th>Jenis Pendapatan</th>";

        $t_col = 2;

        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<th>".$i."</th>";
            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		foreach ($jenis as $r_data => $v_data) {
			$data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->nama_jenis;
			$data["list_data"][$r_data]["main"]["id_jenis"] = $v_data->id_jenis;

			$str_tbl.= "<tr>
							<td align=\"center\">".($row_data+1)."</a></td>
							<td>".$v_data->nama_jenis."</a></td>";

			$row_th = 0;
			for ($i=$th_st; $i<=$th_fn ; $i++) { 
				$val_data = $this->me->get_pengeluaran_penduduk(array("ppj.id_jenis"=>$v_data->id_jenis, "th"=>$i))->row_array();
				$data["list_data"][$r_data]["detail"][$i] = $val_data;

				if($val_data){
					$str_tbl .= "<td align=\"right\">".number_format($val_data["jml"], 2, ".", ",")." %</td>";

				}else{
					$str_tbl .= "<td align=\"right\">0</td>";
				}

				$row_th++;
			}

			$str_tbl.= "</tr>";
			$row_data++;
		}
		
		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Ekonomi";	
		
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
		

		$this->load->view("front_data/ekonomi/pendapatan_usaha", $data);
		
	}

	public function pengeluaran_rmt_tgg($th_fn){
		$data["page"] = "pengeluaran_rumah_tangga";

		$th_st = $th_fn-2;
		$data_graph = array();

		$str_title_data = "Data Pengeluaran Rumah Tangga Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <th>Tahun data</th>
                                <th>Jumlah Pengeluran Untuk Makan</th>
                                <th>Jumlah Pengeluran Untuk Yang Bukan Makan</th>";

        $t_col = 3;


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$t_col."\">".$str_title_data."</th>
                        </tr>";

        //------------------------------------------------header-------------------------------------------------

		$str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		for ($i=$th_st; $i<=$th_fn ; $i++) {
			$val_data = $this->me->pengeluaran_rmt_tgg(array("th"=>$i))->row_array(); 
			$data["list_data"][$i] = $val_data;

			// print_r($val_data);

			if($val_data){
				$str_tbl.= "<tr>
							<td align=\"center\">".$i."</td>
							<td align=\"center\">".$val_data["jml_mkn"]." %</td>
							<td align=\"center\">".$val_data["jml_non"]." %</td>";
				$str_tbl.= "</tr>";

			}else{
				$str_tbl.= "<tr>
							<td align=\"center\"><a href=\"#\">".$i."</a></td>
							<td align=\"right\">0</td>
							<td align=\"right\">0</td>";
				$str_tbl.= "</tr>";
			}

			$row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Ekonomi";	
		
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
		

		$this->load->view("front_data/ekonomi/pendapatan_usaha", $data);
		
	}

}

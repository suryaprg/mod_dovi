<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintransportasi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_transportasi", "mt");
		
		$this->load->library("response_message");
	}

	public function index(){
		// $this->tehnologi_jml_web_opd(date("Y"));
	}

	public function trans_jln($th_fn){
		$data["page"] = "jalan";
		$jenis = $this->mt->get_trans_jln_jenis()->result();

		$th_st = $th_fn-1;

		$str_title_data = "Panjang jalan menurut Kondisi Jalan di Kota Malang Tahun ".($th_fn-1)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<th rowspan=\"2\">No.</th>
                            <th rowspan=\"2\">Keterangan</th>";
        $str_header_bot = "<tr>";

        $colspan = 2;
        for ($i=$th_st; $i<=$th_fn ; $i++) {
        	$str_header_mod .= "<th colspan=\"3\">".$i."</th>";

        	$str_header_bot .= "<th>Jalan Negara</th>
        						<th>Jalan Provinsi</th>
        						<th>Jalan Kota</th>";
        	$colspan+=3;
        }
        
        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"".$colspan."\">".$str_title_data."</th>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";
		$data["list_data"] = array();
		$row_data = 0;

		foreach ($jenis as $r_data => $v_data) {
			$row_th = 0;

			$str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$row_data."\">
							<td>".($row_data+1)."</td>
							<td>".$v_data->kategori_jln."</td>";
			for ($i=$th_st; $i<=$th_fn ; $i++) { 
				$val_data = $this->mt->get_trans_jln(array("th"=>$i, "kategori_jln"=>$v_data->kategori_jln))->row_array();

				if($val_data){
					$str_tbl .= "<td align=\"center\">".$val_data["jln_negara"]." Km</td>
								<td align=\"center\">".$val_data["jln_prov"]." Km</td>
								<td align=\"center\">".$val_data["jln_kota"]." Km</td>";
				}else{
					$str_tbl .= "<td align=\"center\">0</td>
								<td align=\"center\">0</td>
								<td align=\"center\">0</td>";
				}

				$data["list_data"][$r_data][$i] = $val_data;
				$row_th++;
			}
			$str_tbl .= "</tr>";
			$row_data++;
		}
		
		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		
		$data["str_tbl"] = $str_tbl;
		$data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Transportasi";	
		
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");
        // $this->load->view('pdf', $data);

		// $this->load->view("front_data_pdf/transportasi/jalan", $data);
	}

	public function trans_jml_kendaraan($th_fn){

		$data["page"] = "kendaraan";
		$jenis = $this->mt->get_main_jenis_kendaraan();

		$th_st = $th_fn-2;
		
		$str_title_data = "Jumlah Kendaraan Bermotor menurut Jenis Kendaraan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Keterangan</th>";

        
        for ($i=$th_st; $i<=$th_fn ; $i++) {
        	$str_header_mod .= "<th>".$i."</th>";
        }
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\" align=\"center\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();

		$row_data = 0;

		foreach ($jenis as $r_data => $v_data) {
			$data["list_data"][$r_data]["main"]["id_jenis"] = $v_data->id_jenis_kendaraan;
			$data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->keterangan;

			$str_tbl .= "<tr>
				            <td><a class=\"jenis\" id=\"jenis_".$v_data->id_jenis_kendaraan."\">".$v_data->keterangan."</a></td>";

			for ($i=$th_st; $i<=$th_fn ; $i++) { 
				$val_data = $this->mt->get_trans_jml_kendaraan(array("pk.id_jenis_kendaraan"=>$v_data->id_jenis_kendaraan, "th"=>$i))->row_array();

				$data["list_data"][$r_data]["detail"][$i] = $val_data["jml"];
				
				$str_tbl .= "<td align=\"center\">".number_format($val_data["jml"], 0, ".", ",")." unit</td>";

			}


			$str_tbl .= "</tr>";

			$row_data++;
		}
		
		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["kategori"] = "Transportasi";	

        
        // print_r("<pre>");
        // print_r($data);
        //Load the library
        $this->load->library('html2pdf');
        
        //Set folder to save PDF to
        $this->html2pdf->folder('./assets/pdfs/');
        
        //Set the filename to save/download as
        $this->html2pdf->filename($data["kategori"].'.pdf');
        
        //Set the paper defaults
        $this->html2pdf->paper('a4', 'landscape');
        
        //Load html view
        $this->html2pdf->html($this->load->view('front_data_pdf/transportasi/jalan', $data, true));
        
        if($this->html2pdf->create('save')) {
            //PDF was successfully saved or downloaded
            echo 'PDF saved';
        }

        redirect(base_url()."assets/pdfs/".$data["kategori"].".pdf");

		// $this->load->view("front_data/transportasi/trans_kendaraan", $data);
	}

}

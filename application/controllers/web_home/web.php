<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	function __construct(){
		parent::__construct();	
		$this->load->helper(array('form','url', 'text_helper','date'));
		$this->load->database();	
		// $this->load->model('m_cpm');
		// $this->load->model('Users_model');
		$this->load->library(array('Pagination','user_agent','session','form_validation','upload'));
	
	}
	
	public function index(){
		$this->load->view('web_new/web');
	
	}


	public function about(){
		$this->load->view('web_new/about');
	
	}


	public function struktur(){
		$this->load->view('web_new/struktur');
	
	}

	
}

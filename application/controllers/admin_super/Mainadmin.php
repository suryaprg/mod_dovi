<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainadmin extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model("admin_super/main_admin", "ma");
		$this->load->model("admin_super/main_dinas", "md");

		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------Admin Lv----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	public function index(){
		$data["page"] = "admin";
		$data["list_admin"] = $this->ma->get_admin();
		$data["list_admin_lv"] = $this->ma->get_admin_lv();
		$data["list_dinas"] = $this->md->get_dinas();
		// print_r("<pre>");
		// print_r($data);
		$this->load->view('main_super', $data);
	}

	private function validation_ins_admin(){
		$config_val_input = array(
            array(
                'field'=>'admin_lv',
                'label'=>'Admin Level',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'id_dinas',
                'label'=>'Dinas',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'user',
                'label'=>'User Name',
                'rules'=>'required|alpha_numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'pass',
                'label'=>'Password',
                'rules'=>'required|alpha_numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'re_pass',
                'label'=>'Ulangi Password',
                'rules'=>'required|alpha_numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'nama',
                'label'=>'Nama',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'nip',
                'label'=>'NIP',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'jabatan',
                'label'=>'jabatan',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_admin(){
		if($this->validation_ins_admin()){
			$id_admin_lv = $this->input->post("admin_lv");
			$id_dinas = $this->input->post("id_dinas");
			$user = $this->input->post("user");
			$nama = $this->input->post("nama");
			$nip = $this->input->post("nip");
			$jabatan = $this->input->post("jabatan");

			$pass = $this->input->post("pass");
			$repass = $this->input->post("re_pass");
			
			if($pass == $repass){
				$insert = $this->db->query("select insert_admin('".$id_admin_lv."','".$user."','".md5($pass)."','".$nama."','".$nip."','".$jabatan."','".$id_dinas."');");

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			}

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."super/admin");
	}

	public function index_up_admin(){
		$id_admin = $this->input->post("id_admin");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_admin =$this->ma->get_admin_where(array("id_admin"=>$id_admin)); 
		if(!empty($data_admin)){
			$data["status"] = true;
			$data["val"] = $data_admin;
		}

		print_r(json_encode($data));
	}


	public function up_admin(){
		if($this->validation_ins_admin()){
			$id_admin_lv = $this->input->post("admin_lv");
			$id_dinas = $this->input->post("id_dinas");
			$user = $this->input->post("user");
			$nama = $this->input->post("nama");
			$nip = $this->input->post("nip");
			$jabatan = $this->input->post("jabatan");
			$id_admin = $this->input->post("id_admin");

			$pass = $this->input->post("pass");
			$repass = $this->input->post("re_pass");
			
			if($pass == $repass){
				$where = array(
							"id_admin"=>$id_admin
						);
				$data = array(
							"id_lv"=>$id_admin_lv,
							"id_dinas"=>$id_dinas,
							"email"=>$user,
							"nama"=>$nama,
							"password"=>$pass,
							"nip"=>$nip,
							"jabatan"=>$jabatan,
						);

				$update = $this->ma->update_admin($data, $where);
				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			}

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."super/admin");
	}

	public function delete_admin($id_admin){

		$delete = $this->ma->update_admin(array("is_delete"=> "1"),array("id_admin"=>$id_admin));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."super/admin");
	}
#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Admin------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	public function index_admin_lv(){
		$data["page"] = "admin_lv";
		$data["list_admin_lv"] = $this->ma->get_admin_lv();
		$this->load->view('main_super', $data);
	}

	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainkelkec extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model("admin_super/main_kecamatan", "mk");
		$this->load->model("admin_super/main_dinas", "md");

		$this->load->library("response_message");
			if(isset($_SESSION["admin_lv_1"])){
            if($_SESSION["admin_lv_1"]["is_log"] == 0){
                redirect(base_url()."admin/login");
            }
        }else{
        	redirect(base_url()."admin/login");
        }
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------Kecamatan----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	public function index(){
		$data["page"] = "main_kelkec";
		$data["list_kelurahan"] = $this->mk->get_kel();
		$data["list_kacamatan"] = $this->mk->get_kec();
		
		$this->load->view('admin_data/geo/main_admin_geo', $data);
	}

	private function validation_ins_kec(){
		$config_val_input = array(
            array(
                'field'=>'nama',
                'label'=>'Nama Kecamatan',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'latlng',
                'label'=>'Posisi Wilayah (Latitude dan Longitude)',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'luas',
                'label'=>'Luas Wilayah (km2)',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_kecamatan(){
		if($this->validation_ins_kec()){
			$nama = $this->input->post("nama");
			$latlng = $this->input->post("latlng");
			$luas = $this->input->post("luas");
			
			$insert = $this->db->query("select insert_kecamatan('".$nama."','".$latlng."','".$luas."');");

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."super/kelkec");
	}

	public function index_up_kecmatan(){
		$id_kec = $this->input->post("id_kec");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_kec =$this->mk->get_kec_where(array("id_kec"=>$id_kec)); 
		if(!empty($data_kec)){
			$data["status"] = true;
			$data["val"] = $data_kec;
		}

		print_r(json_encode($data));
	}


	public function up_kecmatan(){
		if($this->validation_ins_kec()){
			$id_kec = $this->input->post("id_kec");

			$nama = $this->input->post("nama");
			$latlng = $this->input->post("latlng");
			$luas = $this->input->post("luas");
			
			$where = array(
							"id_kec"=>$id_kec
						);
			
			$data = array(
							"nama_kec"=>$nama,
							"latlng"=>$latlng,
							"luas"=>$luas
						);
			$update = $this->mk->update_kec($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."super/kelkec");
	}

	public function delete_kecmatan($id_kec){

		$delete = $this->mk->update_kec(array("is_delete"=> "1"),array("id_kec"=>$id_kec));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."super/kelkec");
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Kelurahan--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins_kel(){
		$config_val_input = array(
            array(
                'field'=>'nama_kel',
                'label'=>'Nama Kelurahan',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'id_kec',
                'label'=>'Id Kecamatan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'latlng_kel',
                'label'=>'Posisi Wilayah (Latitude dan Longitude)',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'luas_kel',
                'label'=>'Luas Wilayah (km2)',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_kelurahan(){
		if($this->validation_ins_kel()){

			$nama_kel = $this->input->post("nama_kel");
			$id_kec = $this->input->post("id_kec");
			$latlng_kel = $this->input->post("latlng_kel");
			$luas_kel = $this->input->post("luas_kel");
			
			$insert = $this->db->query("select insert_kelurahan('".$nama_kel."','".$luas_kel."','".$latlng_kel."','".$id_kec."');");

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."super/kelkec");
	}

	public function index_up_kelurahan(){
		$id_kel = $this->input->post("id_kel");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_kec =$this->mk->get_kel_where(array("id_kel"=>$id_kel)); 
		if(!empty($data_kec)){
			$data["status"] = true;
			$data["val"] = $data_kec;
		}

		print_r(json_encode($data));
	}


	public function up_kelurahan(){
		print_r("<pre>");
			print_r($_POST);
		if($this->validation_ins_kel()){
			$id_kel = $this->input->post("up_id_kel");

			$nama_kel = $this->input->post("nama_kel");
			$id_kec = $this->input->post("id_kec");
			$latlng_kel = $this->input->post("latlng_kel");
			$luas_kel = $this->input->post("luas_kel");

			print_r("<pre>");
			print_r($_POST);
			
			$where = array(
							"id_kel"=>$id_kel
						);
			
			$data = array(
							"id_kec"=>$id_kec,
							"nama_kel"=>$nama_kel,
							"latlng_kel"=>$latlng_kel,
							// "luas_kel"=>$luas_kel
						);
			$update = $this->mk->update_kel($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."super/kelkec");
	}

	public function delete_kelurahan($id_kel){

		$delete = $this->mk->update_kel(array("is_del"=> "1"),array("id_kel"=>$id_kel));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."super/kelkec");
	}

	
}

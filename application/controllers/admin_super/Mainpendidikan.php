<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpendidikan extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model("admin_super/main_partai", "mp");

		$this->load->library("response_message");
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------Partai----------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	public function index(){
		$data["page"] = "main_partai";
		$data["list_partai"] = $this->mp->get_partai();
		$data["list_dpr"] = $this->mp->get_dpr();
		
		$this->load->view('main_super', $data);
	}

	private function validation_ins_partai(){
		$config_val_input = array(
            array(
                'field'=>'nama_partai',
                'label'=>'Nama Partai',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_partai(){
		print_r("<pre>");
		print_r($_POST);
		if($this->validation_ins_partai()){
			$nama = $this->input->post("nama_partai");
			
			$insert = $this->mp->insert_partai(array("id_partai"=>"", "nama_partai"=>$nama, "is_delete"=>"0"));
			if($insert){
				echo "yes";
			}else {
				echo "no";
			}

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."super/partai");
	}

	public function index_up_partai(){
		$id_partai = $this->input->post("id_partai");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_kec =$this->mp->get_partai_where(array("id_partai"=>$id_partai)); 
		if(!empty($data_kec)){
			$data["status"] = true;
			$data["val"] = $data_kec;
		}

		print_r(json_encode($data));
	}


	public function up_partai(){
		if($this->validation_ins_partai()){
			$id_partai = $this->input->post("id_partai");
			$nama = $this->input->post("nama_partai");

			$where = array(
							"id_partai"=>$id_partai
						);
			
			$data = array(
							"nama_partai"=>$nama
						);

			$update = $this->mp->update_partai($data, $where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."super/partai");
	}

	public function delete_partai($id_partai){
		$delete = $this->mp->update_partai(array("is_delete"=> "1"),array("id_partai"=>$id_partai));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."super/partai");
	}

#---------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------DPR--------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------------

	private function validation_ins_dpr(){
		$config_val_input = array(
            array(
                'field'=>'id_partai',
                'label'=>'Nama Partai',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'periode',
                'label'=>'Periode',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'jml',
                'label'=>'Posisi Wilayah (Latitude dan Longitude)',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_dpr(){
		if($this->validation_ins_dpr()){

			$id_partai = $this->input->post("id_partai");
			$periode = $this->input->post("periode");
			$jk = $this->input->post("jk");
			$jml = $this->input->post("jml");
			
			$data_send = array(
						"id_pem_dpr"=>"",
						"id_partai"=>$id_partai,
						"th"=>$periode,
						"jk"=>$jk,
						"jml"=>$jml,
					);

			$insert = $this->mp->insert_dpr($data_send);

				if($insert){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."super/partai");
	}

	public function index_up_dpr(){
		$id_dpr = $this->input->post("id_dpr");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_kec =$this->mp->get_dpr_where(array("id_pem_dpr"=>$id_dpr)); 
		if(!empty($data_kec)){
			$data["status"] = true;
			$data["val"] = $data_kec;
		}

		print_r(json_encode($data));
	}


	public function up_dpr(){
		if($this->validation_ins_dpr()){

			$id_partai = $this->input->post("id_partai");
			$periode = $this->input->post("periode");
			$jk = $this->input->post("jk");
			$jml = $this->input->post("jml");

			$id_dpr = $this->input->post("id_dpr");
			
			$data_send = array(
						"id_partai"=>$id_partai,
						"th"=>$periode,
						"jk"=>$jk,
						"jml"=>$jml,
					);

			$data_where = array(
						"id_pem_dpr"=>$id_dpr
					);

			$update = $this->mp->update_dpr($data_send, $data_where);

				if($update){
					echo "yes";
				}else {
					echo "no";
				}
			

		}else {
			print_r(validation_errors());
		}

		redirect(base_url()."super/partai");
	}

	public function delete_dpr($id_dpr){
		$delete = $this->mp->delete_dpr(array("id_pem_dpr"=> $id_dpr));
		if($delete){
			echo "del";
		}else {
			echo "fail";
		}
		redirect(base_url()."super/partai");
	}

	
}

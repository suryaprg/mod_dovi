<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintransportasi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_transportasi", "mt");
        $this->load->model('front_lp/Lp_terminal', 'lt');

        $this->load->model('front_lp/Lp_kendaraan_kec', 'lkk');
        $this->load->model('front_lp/Lp_kendaraan_plat', 'lkp');
        $this->load->model('front_lp/Lp_kendaraan_umum', 'lku');
		$this->load->library("response_message");
	}

	public function index(){
		// $this->tehnologi_jml_web_opd(date("Y"));
	}

	public function trans_jln($th_fn){
		$data["page"] = "jalan";
        $jenis = $this->mt->get_trans_jln_jenis()->result();

        $th_st = $th_fn-1;
        $data_graph = array();

        $str_title_data = "Panjang Jalan Menurut Kondisi Jalan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <td rowspan=\"2\">No.</td>
                            <td rowspan=\"2\">Keterangan</td>";
        $str_header_bot = "<tr>";

        $colspan = 2;
        for ($i=$th_st; $i<=$th_fn ; $i++) {
            $str_header_mod .= "<td colspan=\"3\">".$i."</td>";

            $str_header_bot .= "<td>Jalan Negara</td>
                                <td>Jalan Provinsi</td>
                                <td>Jalan Kota</td>";
            $colspan+=3;
        }
        
        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$colspan."\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;

        $series = "";

        foreach ($jenis as $r_data => $v_data) {
            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$row_data."\">
                            <td><a href=\"#\">".($row_data+1)."</a></td>
                            <td><a href=\"#\">".$v_data->kategori_jln."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i<=$th_fn ; $i++) { 
                // 
                $val_data = $this->mt->get_trans_jln(array("th"=>$i, "kategori_jln"=>$v_data->kategori_jln))->row_array();

                if($val_data){
                    $str_tbl .= "<td>".$val_data["jln_negara"]." Km</td>
                                <td>".$val_data["jln_prov"]." Km</td>
                                <td>".$val_data["jln_kota"]." Km</td>";
                }else{
                    $str_tbl .= "<td>0</td>
                                <td>0</td>
                                <td>0</td>";
                }

                $data_graph[$row_data][$row_th]["year"] = (int)$i;
                $data_graph[$row_data][$row_th]["val_negara"] = (double)$val_data["jln_negara"];
                $data_graph[$row_data][$row_th]["val_prov"] = (double)$val_data["jln_prov"];
                $data_graph[$row_data][$row_th]["val_kota"] = (double)$val_data["jln_kota"]; 
                // print_r($val_data);
                $data["list_data"][$r_data][$i] = $val_data;
                $row_th++;
            }
            $str_tbl .= "</tr>";
            $row_data++;
        }
        // $data["list_data"] = array();
        
        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["series"] = $series;

        $data["data_graph"] = json_encode($data_graph);     

		// print_r($data);
		$this->load->view("front_data/transportasi/main_transportasi", $data);
	}

	public function trans_jml_kendaraan($th_fn){
		$data["page"] = "kendaraan";
		$data["page"] = "kendaraan";
        $jenis = $this->mt->get_main_jenis_kendaraan();

        $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "PERSENTASE JUMLAH KENDARAAN MENURUT JENIS KENDARAAN DI KOTA MALANG TAHUN di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Keterangan</th>";

        
        for ($i=$th_st; $i<=$th_fn ; $i++) {
            $str_header_mod .= "<th>".$i."</th>";
        }
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;

        $series = "";
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$r_data]["main"]["id_jenis"] = $v_data->id_jenis_kendaraan;
            $data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->keterangan;

            $str_tbl .= "<tr>
                            <td><a href=\"#\" class=\"jenis\" id=\"jenis_".$v_data->id_jenis_kendaraan."\">".$v_data->keterangan."</a></td>";

            for ($i=$th_st; $i<=$th_fn ; $i++) { 
                $val_data = $this->mt->get_trans_jml_kendaraan(array("pk.id_jenis_kendaraan"=>$v_data->id_jenis_kendaraan, "th"=>$i))->row_array();

                $data["list_data"][$r_data]["detail"][$i] = $val_data["jml"];
                
                $str_tbl .= "<td>".number_format($val_data["jml"], 0, ".", ",")."</td>";

                $data_graph[$i]["year"] = (string)$i;
                $data_graph[$i]["val_".$row_data] = (int)$val_data["jml"];

            }

            $series .= "var series_".$row_data." = chart.series.push(new am4charts.LineSeries());
                        series_".$row_data.".dataFields.valueY = \"val_".$row_data."\";
                        series_".$row_data.".dataFields.categoryX = \"year\";
                        series_".$row_data.".name = \"".$v_data->keterangan."\";
                        series_".$row_data.".strokeWidth = 3;
                        series_".$row_data.".bullets.push(new am4charts.CircleBullet());
                        series_".$row_data.".tooltipText = \"Jumlah kendaraan pada {categoryX}: {valueY}\";
                        series_".$row_data.".legendSettings.valueText = \"{valueY}\";
                        series_".$row_data.".visible  = false;";

            $str_tbl .= "</tr>";

            $row_data++;
        }

        $row_graph = 1;
        $data_graph_new = array();
        foreach ($data_graph as $r_graph => $v_graph) {
            array_push($data_graph_new, $v_graph);
            $row_graph++;
        }
        
        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["series"] = $series;

        $data["data_graph"] = json_encode($data_graph_new); 

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table>".$data["str_header"].$str_tbl."</table>");

		// print_r("<pre>");
		// print_r($data);

		$this->load->view("front_data/transportasi/main_transportasi", $data);
	}


    public function terminal($th_fn){
        $data["page"] = "terminal";
        $jenis = $this->lt->get_lp_jenis();
        $row_jenis = 0;

        $data_graph = array();
        $str_tbl = "";
        $str_series = "";
        

        
        $str_header = "<thead>
                            <tr>
                                <th width=\"10%\">No</th>
                                <th width=\"30%\">Keterangan</th>
                                    ";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header .= "<th width=\"20%\">".$i."</th>";
            $t_col++;
        }
        $str_header .= "</tr></thead>";
                                        

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;

            // print_r("<pre>");
            // print_r($v_data);
            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"bottom: 30px;\" width=\"100%\">";

            $str_tbl .= $str_header;
            
            $sub_jenis = $this->lt->get_lp_sub_jenis(array("ltsj.id_jenis"=>$v_data->id_jenis));

            if(count($sub_jenis) != 0){
                foreach ($sub_jenis as $r_sub => $v_sub) {
                    $data["list_data"][$v_data->id_jenis][$v_sub->id_sub_jenis]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;
                    $data["list_data"][$v_data->id_jenis][$v_sub->id_sub_jenis]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                    
                    $data_graph[$v_data->id_jenis][$v_sub->id_sub_jenis]["caption"] = $v_sub->nama_sub_jenis;

                    $str_tbl .= "      
                                        <tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_sub_jenis."\">
                                            <td>-</td>
                                            <td><a href=\"#\">".$v_sub->nama_sub_jenis."</a></td>";
                    $row_th = 0;
                    for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                        $val_data = $this->lt->get_lp(array("lt.id_jenis"=>$v_data->id_jenis,
                                                            "lt.id_sub_jenis"=>$v_sub->id_sub_jenis,
                                                            "th"=>$i));
                        $data["list_data"][$v_data->id_jenis][$v_sub->id_sub_jenis]["value"][$i] = $val_data["jml"];

                        $data_graph[$v_data->id_jenis][$v_sub->id_sub_jenis]["val_data"][$row_th]["year"] = $i;
                        $data_graph[$v_data->id_jenis][$v_sub->id_sub_jenis]["val_data"][$row_th]["value"] = (float)$val_data["jml"];

                        $row_th++;
                        $str_tbl .= "           <td>".$val_data["jml"]."</td>";
                    
                    }
                }    
            }else{    
                    $data["list_data"][$v_data->id_jenis][0]["main"]["nama_sub_jenis"] = $v_data->nama_jenis;
                    $data["list_data"][$v_data->id_jenis][0]["main"]["id_sub_jenis"] = $v_data->id_jenis;
                    $data_graph[$v_data->id_jenis][0]["caption"] = $v_data->nama_jenis;

                    $str_tbl .= "      
                                        <tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_0\">
                                            <td>-</td>
                                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";
                    $row_th = 0;
                    for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                        $val_data = $this->lt->get_lp(array("lt.id_jenis"=>$v_data->id_jenis,
                                                            "lt.id_sub_jenis"=>0,
                                                            "th"=>$i));
                        $data["list_data"][$v_data->id_jenis][0]["value"][$i] = $val_data["jml"];

                        $data_graph[$v_data->id_jenis][0]["val_data"][$row_th]["year"] = $i;
                        $data_graph[$v_data->id_jenis][0]["val_data"][$row_th]["value"] = (float)$val_data["jml"];

                        $row_th++;
                        $str_tbl .= "           <td>".$val_data["jml"]."</td>";
                    
                    }
                
                
            }
            $row_jenis++;
            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $data["str_tbl"] = $str_tbl;
        $data["data_graph"] = json_encode($data_graph);
        $data["str_series"] = $str_series;

        $data["t_col"] = $t_col;

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table>".$str_tbl."</table>");

        $this->load->view("front_data/transportasi/main_transportasi",$data);
    }
#===========================================================================================================================
#------------------------------------------------------------Terminal-------------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Kecamatan--------------------------------------------------
#===========================================================================================================================
    public function kendaraan_kec($th_fn){
         $data["page"] = "kendaraan_kec";
        $jenis = $this->lkk->get_lp_jenis();
        $kecamatan = $this->lkk->get_kec();
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Jumlah Kendaraan Menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Kendaraan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------
        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis_kendaraan;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->keterangan;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis_kendaraan."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->keterangan."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis_kendaraan."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border='1' class=\"data-table\" width=\"100%\">";
            $str_tbl .= $str_header_mod;

            $t_data = array();

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis_kendaraan."_".$v_sub->id_kec."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kec."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lkk->get_lp(array("id_jenis_kendaraan"=>$v_data->id_jenis_kendaraan, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data["jml"]){
                        $str_tbl .= "<td>".$val_data["jml"]."</td>";
                        // $jml_panen = $val_data["jml_panen"];
                    }else{
                        $str_tbl .= "<td>-</td>";
                        // $jml_panen = 0;
                    }

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_data->id_jenis_kendaraan][$v_sub->id_kec][$row_th] = $main_data;
                    $row_th++;
                }
                
                $str_tbl .= "</tr>";
                $r_kec++;
            }

            $str_tbl .= "           
                                </table>
                            </td>
                        </tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kecamatan"] = $kecamatan;
        // $data["jml_th"] = $row_th;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");

         $this->load->view("front_data/transportasi/main_transportasi",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Kecamatan--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Plat--------------------------------------------------
#===========================================================================================================================
    public function kendaraan_plat($th_fn){
         $data["page"] = "kendaraan_plat";
        $jenis = $this->lkp->get_lp_jenis();
        
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\" rowspan=\"2\">No</td>
                                <td width=\"35%\" rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-1; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\" colspan='3'>".$i."</td>";

            $str_header_bot .= "<td>Hitam</td><td>Kuning</td><td>Merah</td>";
            $t_col += 3;
        }
        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Jumlah Kendaraan Berdasarkan Plat di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------       
        
        $th_st = $th_fn-1;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) { 
                $val_data = $this->lkp->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;
                    if($val_data){
                        $str_tbl .= "<td>".$val_data["jml_hitam"]."</td>";
                        $str_tbl .= "<td>".$val_data["jml_kuning"]."</td>";
                        $str_tbl .= "<td>".$val_data["jml_merah"]."</td>";
                    }else{
                        $str_tbl .= "<td>-</td>";
                        $str_tbl .= "<td>-</td>";
                        $str_tbl .= "<td>-</td>";
                    }
                $main_data = array("year"=>(string)$i, "jml_hitam"=>(float)$val_data["jml_hitam"], "jml_kuning"=>(float)$val_data["jml_kuning"], "jml_merah"=>(float)$val_data["jml_merah"]);
                $data_graph[$v_data->id_jenis][$row_th] = $main_data;

                $row_th++;
            }

            $t_data = array();

            $str_tbl .= "</tr>";            
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;
        
        $data["data_graph"] = json_encode($data_graph);

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;
        // $data["str_series"] = $str_series;
        // $data["jml_th"] = $row_th;

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$data["str_header"]. $data["str_tbl"]."</table>");

        $this->load->view("front_data/transportasi/main_transportasi",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Plat--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Kenrdaraan Umum--------------------------------------------------
#===========================================================================================================================
    public function kendaraan_umum($th_fn){
         $data["page"] = "kendaraan_umum";
        $jenis = $this->lku->get_lp_jenis();
        $kategori = $this->lku->get_lp_kategori();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Data Jumlah Kendaraan Umum di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Kendaraan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->id_jenis_kendaraan;
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->keterangan;

            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis_kendaraan;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->keterangan;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis_kendaraan."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->keterangan."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis_kendaraan."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border='1' class=\"data-table\" width=\"100%\">";
            $str_tbl .= $str_header_mod;

            $t_data = array();

            $r_kec = 0;
            foreach ($kategori as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["id_kategori"] = $v_sub->id_jenis;
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["nama_kategori"] = $v_sub->nama_jenis;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis_kendaraan."_".$v_sub->id_jenis."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_jenis."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lku->get_lp(array("id_jenis_kendaraan"=>$v_data->id_jenis_kendaraan, "id_jenis"=> $v_sub->id_jenis, "th"=>$i));
                    $data["list_data"][$row_jenis]["kategori"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data["jml"]){
                        $str_tbl .= "<td>".$val_data["jml"]."</td>";
                        // $jml_panen = $val_data["jml_panen"];
                    }else{
                        $str_tbl .= "<td>-</td>";
                        // $jml_panen = 0;
                    }

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_data->id_jenis_kendaraan][$v_sub->id_jenis][$row_th] = $main_data;
                    $row_th++;
                }
                
                $str_tbl .= "</tr>";
                $r_kec++;
            }

            $str_tbl .= "           
                                </table>
                            </td>
                        </tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;
        $data["str_tbl"] = $str_tbl;
        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kategori"] = $kategori;
        // $data["jml_th"] = $row_th;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");

        // $this->load->view("front_lp/keuangan/tenaga",$data);
         $this->load->view("front_data/transportasi/main_transportasi",$data);
    }
#

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainsosial extends CI_Controller {


        
	function __construct(){
		parent::__construct();	
		$this->load->helper(array('form','url', 'text_helper','date'));
		$this->load->database();	
		$this->load->model('front_lp/Lp_aparat', 'lp');
		     $this->load->model('front_lp/Lp_pidana', 'lpi');
        $this->load->model('front_lp/Lp_pidana_kec', 'lpk');
           $this->load->model('front_lp/Lp_pmks', 'lpm');
            $this->load->model('front_data/Main_dpr', 'lpd');
        $this->load->model("front_data/Main_pemerintahan", "mp");
		// $this->load->model('m_cpm');
		// $this->load->model('Users_model');
		$this->load->library(array('Pagination','user_agent','session','form_validation','upload'));
	
	} 

	// public function index(){
	// 	$data['page'] = "Mainsosial/index";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/sosial/masterSospolkem',$data);
	
	// }

	// public function organisasiAgama(){
	// 	$data['page'] = "Mainsosial/organisasiAgama";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/sosial/masterSospolkem',$data);
	// }


	public function parpol(){
		$data['page'] = "Mainsosial/parpol";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('front_data/sosial/masterSospolkem',$data);
	}

	public function parlementeria($th_fn){
		$data['page'] = "parlementeria";
		 $jenis = $this->lpd->get_lp_jenis();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------

        $str_header_top = "<tr>
                            <td colspan=\"5\">Data Jumlah Anggota Parlemen Kota Malang Tahun ".$th_fn."</td>
                          </tr>
                          <tr>
                            <td>No.</td>
                            <td>Keterangan</td>
                            <td>Laki-Laki</td>
                            <td>Perempuan</td>
                            <td>Jumlah Anggota</td>
                          </tr>";


        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";
        // $series = "";

        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_partai;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_partai;

            // $data_graph[$v_data->id_partai]["main"]["id_partai"] = $v_data->id_partai;
            // $data_graph[$v_data->id_partai]["main"]["nama_jenis"] = $v_data->nama_partai;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_partai."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_partai."</a></td>";

            $val_data_l = $this->lpd->get_lp(array("id_partai"=>$v_data->id_partai, "th"=>$th_fn, "jk"=>0));
            $jml_l = 0;
            if($val_data_l){
                $jml_l = $val_data_l["jml"];
            }
            $data["list_data"]["value"][0] = $jml_l;

            $val_data_p = $this->lpd->get_lp(array("id_partai"=>$v_data->id_partai, "th"=>$th_fn, "jk"=>1));
            $jml_p = 0;
            if($val_data_p){
                $jml_p = $val_data_p["jml"];
            }
            $data["list_data"]["value"][1] = $jml_p;

            $str_tbl .= "<td>".$jml_l."</td>
                        <td>".$jml_p."</td>
                        <td>".($jml_l+$jml_p)."</td>";

            // "year"=>(string)$th_fn, 

            $main_data = array("nama_jenis"=> $v_data->nama_partai, "val_l"=>(float)$jml_l, "val_p"=>(float)$jml_p, "val_jml"=>(float)($jml_l+$jml_p));
            $data_graph[$row_jenis] = $main_data;

            $str_tbl .= "</tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);
        // $data["series"] = $series;

        // print_r("<pre>");
        // print_r($data["data_graph"]);
		$this->load->view('front_data/sosial/masterSospolkem',$data);
	}

	public function aparat_keamanan(){
		$data['page'] = "aparat_keamanan";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('front_data/sosial/masterSospolkem',$data);
	}

	public function pidana_penyelesaian(){
		$data['page'] = "pidana_penyelesaian";
		// $data['admin'] = $this->m_pkl->tampil_data2()->result();
		$this->load->view('front_data/sosial/masterSospolkem',$data);
	}

	 public function aparat($th_fn){
	 	$data['page'] = "aparat";
        $aparat = $this->lp->get_lp_aparat();
        $data_graph = array();

        $row_kec = 0;
        foreach ($aparat as $r_data => $v_data) {
            $data["list_data"][$row_kec]["main"]["aparat"] = $v_data->nama_jenis;
            $data["list_data"][$row_kec]["main"]["id_jenis"] = $v_data->id_jenis;
            
            $row_th = 0;
            for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                $val_data = $this->lp->get_lp(array("lpt.id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_kec]["value"][$i] = $val_data["jml"];

                $main_data = array("year"=>$i, "val_data"=>(int)$val_data["jml"]);
                $data_graph[$v_data->id_jenis][$row_th] = $main_data;
                $row_th++;
            }
            $row_kec++;
        }

        $data["graph_data"] = json_encode($data_graph);
       

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;


        // print_r("<pre>");
        // print_r($data);
        // print_r($data_graph);


        $this->load->view("front_data/sosial/masterSospolkem",$data);

    }

     public function pidana($th_fn){
     	$data['page'] = "pidana";
        $jenis = $this->lpi->get_lp_jenis();

        $th_st = $th_fn-2;
        $str_tbl = "";

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td rowspan=\"2\">No</td>
                                <td rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"2\">".$i."</td>";
            $str_header_bot .= "<th>Lapor</th><th>Selesai</th>";
            $t_col += 2;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Data Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                        </tr>";


        //------------------------------------------------header-------------------------------------------------
        // $str_header .= "    </tr>";

        $data_graph = array();

        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";
            
            // $data_graph[$v_data->id_jenis]["caption"] = $v_data->nama_jenis;

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpi->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["lapor"]){
                    $str_tbl .= "<td>".$val_data["lapor"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                }

                if($val_data["selesai"]){
                    $str_tbl .= "<td>".$val_data["selesai"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                }

                $main_data = array("year"=>$i, "lapor"=>(int)$val_data["lapor"], "selesai"=>(int)$val_data["selesai"]);
                $data_graph[$v_data->id_jenis][$row_th] = $main_data;

                $row_th++;
                // $str_tbl .= "<td>".$val_data["selesai"]."</td>";
            }

            $str_tbl .= "</tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$str_header_top.$str_header_mod.$str_header_bot.$str_tbl."</table>");

         $this->load->view("front_data/sosial/masterSospolkem",$data);

    }

#===========================================================================================================================
#------------------------------------------------------------Pidana---------------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#------------------------------------------------------------Pidana_Kec---------------------------------------------------------
#===========================================================================================================================

    public function pidana_kec($th_fn){
    	$data['page'] = "pidana_kec";
        $kecamatan = $this->lpk->get_lp_jenis();

        $th_st = $th_fn-2;

        $str_tbl = "";
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td rowspan=\"2\">No</td>
                                <td rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"2\">".$i."</td>";
            $str_header_bot .= "<th>Lapor</th><th>Selesai</th>";
            $t_col += 2;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Jumlah Tindakan Pidana dan Penyelesaiannya Menurut Kepolisian Sektor Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                        </tr>";


        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 0;
        foreach ($kecamatan as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_kec"] = $v_data->id_kec;
            $data["list_data"][$row_jenis]["jenis"]["nama_kec"] = $v_data->nama_kec;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_kec."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_kec."</a></td>";

            $row_th = 0;

            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpk->get_lp(array("id_kec"=>$v_data->id_kec, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_lapor"]){
                    $str_tbl .= "<td>".$val_data["jml_lapor"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                }

                if($val_data["jml_lapor"]){
                    $str_tbl .= "<td>".$val_data["jml_selesai"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                }

                $main_data = array("year"=>$i, "lapor"=>(int)$val_data["jml_lapor"], "selesai"=>(int)$val_data["jml_selesai"]);
                $data_graph[$v_data->id_kec][$row_th] = $main_data;

                $row_th++;
            }

            $row_jenis++;

            $str_tbl .= "</tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

          $this->load->view("front_data/sosial/masterSospolkem",$data);

    }

    public function pmks($th_fn){
    	$data['page'] = "pmks";
        $jenis = $this->lpm->get_lp_jenis();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td rowspan=\"2\">No</td>
                                <td rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"3\">".$i."</td>";
            $str_header_bot .= "<th>Laki-laki</th>
                                <th>Perempuan</th>
                                <th>Jumlah</th>";
            $t_col += 3;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Data Penyandang Masalah Kesejahteraan Sosial (PMKS) Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                        </tr>";


        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";
        // $series = "";

        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

            // $series = "";
            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {

                $val_data = $this->lpm->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_l"]){
                    $jml_l = $val_data["jml_l"];
                    $str_tbl .= "<td>".$val_data["jml_l"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                    $jml_l = 0;
                }

                if($val_data["jml_p"]){
                    $jml_p = $val_data["jml_p"];
                    $str_tbl .= "<td>".$val_data["jml_p"]."</td>";
                }else{
                    $jml_p = 0;
                    $str_tbl .= "<td>-</td>";
                }

                $str_tbl .= "<td>".($jml_l+$jml_p)."</td>";

                $main_data = array("year"=>(string)$i, "val_l"=>(float)$val_data["jml_l"], "val_p"=>(float)$val_data["jml_p"], "val_jml"=>(float)($jml_l+$jml_p));
                $data_graph[$v_data->id_jenis][$row_th] = $main_data;

                // $series .= "createSeries(\"val_".($row_th+1)."\", \"".$i."\");";
                $row_th++;
            }

            $str_tbl .= "</tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);
        // $data["series"] = $series;

        // print_r("<pre>");
        // print_r($data["data_graph"]);

        $this->load->view("front_data/sosial/masterSospolkem", $data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }


    public function pem_jml_rt_rw($th_fn){
        $data['page'] = "rt_rw";
        $kecamatan = $this->mp->get_kecamatan();

        $th_st = $th_fn;
        $data_graph = array();

        $str_title_data = "Jumlah RW dan RT menurut Kecamatan di Kota Malang Tahun ".($th_st)." - ".$th_fn;
        
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\" rowspan=\"2\">No</td>
                                <td width=\"35%\" rowspan=\"2\">Kecamatan</td>";
        $str_header_bot = "<tr>";                        

        $t_col = 2;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"2\">".$i."</td>";

            $str_header_bot .= "<td width=\"10%\">RT</td>
                                <td width=\"10%\">RW</td>";

            $t_col += 2;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title_data."</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";
        $row_jenis = 0;
        $series = "";

        $t_rt = 0;
        $t_rw = 0; 
        foreach ($kecamatan as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["main"]["nama_kecamatan"] = $v_data->nama_kec;
            $data["list_data"][$row_jenis]["main"]["id_kecamatan"] = $v_data->id_kec;

            $str_tbl .= "<tr>
                            <td>".($row_jenis+1)."</td>
                            <td>".$v_data->nama_kec."</td>";

            // $data_graph[$row_jenis]

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->mp->get_pem_jml_rt_rw(array("th"=>$i, "mk.id_kec"=>$v_data->id_kec))->row_array();
                $data["list_data"][$row_jenis]["detail"][$i] = $val_data;


                $str_tbl .= "<td>".$val_data["jml_rt"]."</td>
                            <td>".$val_data["jml_rw"]."</td>";

                $data_graph[0]["year"] = "RT";
                $data_graph[0]["val_".$row_jenis] = (int)$val_data["jml_rt"];
                
                $data_graph[1]["year"] = "RW";
                $data_graph[1]["val_".$row_jenis] = (int)$val_data["jml_rw"];
                
                $t_rt += $val_data["jml_rt"];
                $t_rw += $val_data["jml_rw"];

                $row_th++;
            }
            $str_tbl .= "<tr>";

            $data_graph[0]["val_jml"] = (int)$t_rt;
            $data_graph[1]["val_jml"] = (int)$t_rw;

            // $data["list_data"][$i][$v_data->id_kec] = 

            $series .= "createSeries(\"val_".$row_jenis."\", \"".$v_data->nama_kec."\");";
            $row_jenis++;
        }
        $series .= "createSeries(\"val_jml\", \"Jumlah dari Seluruh Kelurahan\");";

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["series"] = $series;

        $data["data_graph"] = json_encode($data_graph);

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table>".$data["str_header"].$str_tbl."</table>");


        $this->load->view('front_data/sosial/masterSospolkem',$data);       
    }

}

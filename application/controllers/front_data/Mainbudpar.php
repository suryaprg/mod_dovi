<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainbudpar extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('front_lp/Disbudpar_dts', 'dds');
        $this->load->model('front_lp/Disbudpar_dtw', 'ddw');
        $this->load->model('front_lp/Disbudpar_hotel', 'dh');
        $this->load->model('front_lp/Disbudpar_obj_kunjungan', 'dok');
        $this->load->model('front_lp/Disbudpar_zona', 'dz');

        
        $this->load->library("response_message");

	}
    
    public function index_dts(){
        $data["page"] = "index_dts";
        $jenis = $this->dds->get_lp();
         
        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Daya Tarik Kawasan Strategis dan Destinasi Wisata Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"1%\">No</td>
                                <td width=\"23%\">Nama Objek Wisata</td>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"2\">".$str_title."</td>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($jenis as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td>".($row_data+1)."</td>
                            <td>".$v_data->nama."</td>
                        </tr>";

            $row_data++;
        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;

        $this->load->view("front_data/budpar/masterBudpar", $data);
       // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }

    public function index_dtw($th_fn){
         $data["page"] = "index_dtw";
        $jenis = $this->ddw->get_lp_jenis();
        $th_st = $th_fn-2;
         
        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Daya Tarik Wisata Kota Malang Tahun ". $th_st ." - ". $th_fn;

        $str_header_mod = "<tr>
                                <td rowspan=\"2\" width=\"1%\">No</td>
                                <td rowspan=\"2\" width=\"25%\">Nama Objek Wisata</td>";
        $str_header_bot = "<tr>";
        $t_col = 2;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\" colspan=\"2\">".$i."</td>";
            $str_header_bot .= "<td>wisman</td><td>wisnus</td>";
            $t_col += 2;
        }
        $str_header_bot .= "</tr>";

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title."</td>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";

        $data_graph = array();
        $row_jenis = 0;

        $series = "";
           foreach ($jenis as $r_jenis => $v_jenis) {
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;  

            $str_tbl .= " <tr class=\"jenis\" id=\"jenis_".$v_jenis->id_jenis."\">
                                <td>".($row_jenis+1)."</td>
                                <td>".$v_jenis->nama_jenis."</td>"; 
            

          $row_th= 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) { 
                $val_data = $this->ddw->get_lp(array("id_jenis"=>$v_jenis->id_jenis,"th"=>$i));

                $data["list_data"][$row_jenis]["detail"][$i][0] = $val_data["wisman"];
                $data["list_data"][$row_jenis]["detail"][$i][1] = $val_data["wisnus"];

                $data_graph[$v_jenis->id_jenis][$row_th]["year"] = (int)$i;
                $data_graph[$v_jenis->id_jenis][$row_th]["wisman"] = (int)$val_data["wisman"];
                $data_graph[$v_jenis->id_jenis][$row_th]["wisnus"] = (int)$val_data["wisnus"];
                

                $str_tbl .= "<td>".$val_data["wisman"]."</td><td>".$val_data["wisnus"]."</td>";
                $row_th++;
            }
            $row_jenis++;

            $str_tbl .= "</tr>";

            $series .= "createSeries(\"val_".$v_jenis->id_jenis."\", \"".$v_jenis->nama_jenis."\");";
        }

        $data["data_graph"] = json_encode($data_graph);
          $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;   
        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;

         $this->load->view("front_data/budpar/masterBudpar", $data);
    }

    public function index_hotel($th_fn){
         $data["page"] = "index_hotel";
         $jenis = $this->dh->get_lp_jenis();
        $th_st = $th_fn-2;
         
        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Kunjungan Hotel Kota Malang Tahun ". $th_st ." - ". $th_fn;

        $str_header_mod = "<tr>
                                <td rowspan=\"2\" width=\"1%\">No</td>
                                <td rowspan=\"2\" width=\"30%\">Nama Hotel</td>
                                <td rowspan=\"2\" width=\"10%\">Kategori Hotel</td>";
        $str_header_bot = "<tr>";
        $t_col = 3;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"15%\" colspan=\"2\">".$i."</td>";
            $str_header_bot .= "<td>wisman</td><td>wisnus</td>";
            $t_col += 2;
        }
        $str_header_bot .= "</tr>";

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title."</td>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";

        $data_graph = array();
        $row_jenis = 0;

        $series = "";
        foreach ($jenis as $r_jenis => $v_jenis) {
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;  

            $str_tbl .= " <tr class=\"jenis\" id=\"jenis_".$v_jenis->id_jenis."\">
                                <td>".($row_jenis+1)."</td>
                                <td>".$v_jenis->nama_jenis."</td>
                                <td>".$v_jenis->nama_kategori."</td>"; 
            


            $row_th= 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) { 
                $val_data = $this->dh->get_lp(array("id_jenis"=>$v_jenis->id_jenis,"th"=>$i));

                $data["list_data"][$row_jenis]["detail"][$i][0] = $val_data["wisman"];
                $data["list_data"][$row_jenis]["detail"][$i][1] = $val_data["wisnus"];

                $data_graph[$v_jenis->id_jenis][$row_th]["year"] = (int)$i;
                $data_graph[$v_jenis->id_jenis][$row_th]["wisman"] = (int)$val_data["wisman"];
                $data_graph[$v_jenis->id_jenis][$row_th]["wisnus"] = (int)$val_data["wisnus"];
                

                $str_tbl .= "<td>".$val_data["wisman"]."</td><td>".$val_data["wisnus"]."</td>";
                $row_th++;
            }
            $row_jenis++;

            $str_tbl .= "</tr>";

            $series .= "createSeries(\"val_".$v_jenis->id_jenis."\", \"".$v_jenis->nama_jenis."\");";
        }

        $data["data_graph"] = json_encode($data_graph);
         $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;   
        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;
        // print_r("<pre>");
        // // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
         $this->load->view("front_data/budpar/masterBudpar", $data);
    }

    public function index_obj_bud($th_fn){
        $data["page"] = "index_obj_bud";
        $jenis = $this->dok->get_lp_kategori();
        $th_st = $th_fn-2;
         
        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Objek Pemajuan Budaya di Kota Malang Tahun ". $th_fn;

        $str_header_mod = "<tr>
                                <td width=\"1%\">No</td>
                                <td width=\"30%\">Kebudayaan</td>
                                ";
        
        $t_col = 3;
        
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title."</td>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";

        $data_graph = array();
        $row_jenis = 0;

        $series = "";
        foreach ($jenis as $r_jenis => $v_jenis) {
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_kategori;
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_kategori;  

            $str_tbl .= " <tr>
                                <td colspan=\"".$t_col."\">".$v_jenis->id_kategori.". ".$v_jenis->nama_kategori."</td>";
            $str_tbl .= "</tr>";            

            
            $val_data = $this->dok->get_lp(array("id_kategori"=>$v_jenis->id_kategori));
            $no = 1;
            foreach ($val_data as $r_val => $v_val) {
                $str_tbl .= " <tr>
                                <td>".$v_jenis->id_kategori.".".$no."</td>
                                
                                <td>".$v_val->nama_obj."</td>";
                $str_tbl .= "</tr>";
                $no++;
            }
            
            $row_jenis++;
            
        }

        $data["data_graph"] = json_encode($data_graph);

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
        $this->load->view("front_data/budpar/masterBudpar", $data);
    }

    public function index_zona($th_fn){
      $data["page"] = "index_zona";
        $jenis = $this->dz->get_lp();
         
        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data SK Zona Kreatif di Kota Malang Tahun ". $th_fn;

        $str_header_mod = "<tr>
                                <th width=\"5%\">No</th>
                                <th width=\"40%\">Nama Kawasan</th>
                                <th width=\"45%\">Alamat Kawasan</th>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <th colspan=\"3\">".$str_title."</th>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($jenis as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td>".($row_data+1)."</td>
                            <td width=\"40%\">".$v_data->nama_zona."</td>
                            <td width=\"40%\">".$v_data->alamat."</td>
                        </tr>";

            $row_data++;
        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title;
        $data["kategori"] = "Disbudpar";
        $data["th_fn"] = $th_fn;
         $this->load->view("front_data/budpar/masterBudpar", $data);
    }


}
?>
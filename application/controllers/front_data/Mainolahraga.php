<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainolahraga extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('front_lp/Lp_olahraga', 'lp');
		
		$this->load->library("response_message");
	}

	public function index(){
		$this->kerja_angkatan(date("Y"));
	}

	 public function olahraga($th_fn){
        $olahraga = $this->lp->get_lp_olahraga();

        $row_kec = 0;

        $data_graph = array();
        $series = "";
        foreach ($olahraga as $r_data => $v_data) {
            $data["list_data"][$row_kec]["main"]["olahraga"] = $v_data->nama_jenis;
            $data["list_data"][$row_kec]["main"]["id_jenis"] = $v_data->id_jenis;
            
            $row_th = 0;
            for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 

                $val_data = $this->lp->get_lp(array("lpt.id_jenis"=>$v_data->id_jenis, "th"=>$i));
                
                $data["list_data"][$row_kec]["value"][$i] = $val_data["jml"];

                $main_data = array("year"=>$i, "val_data"=>(int)$val_data["jml"]);
                $data_graph[$v_data->id_jenis][$row_th] = $main_data;
                $row_th++;
            }

            $series .= "createSeries(\"".$v_data->id_jenis."\", \"".$v_data->nama_jenis."\");";

            $row_kec++;
        }
        $series .= "createSeries(\"jml\", \"Total Cabang Olahraga Kota Malang\");";

        $data["data_graph"] = json_encode($data_graph);
        $data["series_data"] = $series;

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;


        // print_r("<pre>");
        // print_r($data["graph_data"]);
        $this->load->view("front_lp/olahraga/lp_olahraga",$data);

    }

	
	

}

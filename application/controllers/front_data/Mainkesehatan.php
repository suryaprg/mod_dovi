<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainkesehatan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_kesehatan", "mk");
		 $this->load->model('front_lp/Lp_kesehatan_all', 'lp');
		$this->load->library("response_message");
	}

	// public function index(){
	// 	$data['page'] = "Mainkesehatan/index";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('kesehatan/masterKesehatan',$data);
	
	// }
	public function index(){
		$this->index_keuangan(date("Y"));
	
	}

	public function kesehatan_gizi_balita($date){
		$data_title = 
		$data["list_data"] = array();

		$val_th = "[";
		$val_buruk = "[";
		$val_kurang = "[";

		for ($i=$date-2; $i<=$date ; $i++) {
			$val_data = $this->mk->get_kesehatan_gizi_balita(array("th"=>$i))->row_array();
			$data["list_data"][$i] = $val_data;
			

			if($i == $date){
				if($val_data){
					$val_buruk .= $val_data["jmh_kurang_gizi"]."";
					$val_kurang .= $val_data["jml_gizi_buruk"]."";
				}else{
					$val_buruk .= "0 ";
					$val_kurang .= "0 ";
				}
				$val_th .= "'".$i."' ";
			}else{
				if($val_data){
					$val_buruk .= $val_data["jmh_kurang_gizi"].", ";
					$val_kurang .= $val_data["jml_gizi_buruk"].", ";
				}else{
					$val_buruk .= "0, ";
					$val_kurang .= "0, ";
				}
				$val_th .= "'".$i."', ";
			}
			

		}
		$val_th .= "]";
		$val_buruk .= "]";
		$val_kurang .= "]";

		$data["th_st"] = $date-2;
		$data["th_fn"] = $date;

		$data["val_th"] = $val_th;
		$data["val_buruk"] = $val_buruk;
		$data["val_kurang"] = $val_kurang;



		// print_r("<pre>");
		// print_r($data);

		$this->load->view("front_data/kesehatan/gizi_balita", $data);

		// $data["list_data"] = array();
		// for ($i=$date-2; $i<=$date ; $i++) { 
		// 	$data["list_data"][$i] = $this->mk->get_kesehatan_gizi_balita(array("th"=>$i))->row_array();
		// }

		// $data["th_st"] = $date-2;
		// $data["th_fn"] = $date;		

		// print_r("<pre>");
		// print_r($data);
	}

	public function main_jenis_penyakit($date){
		$data["list_data"] = $this->mk->get_main_jenis_penyakit(array("th"=>$date))->result();
		
		$str_penyakit ="";
		$str_val = "";
		foreach ($data["list_data"] as $key => $value) {
			if($key == count($data["list_data"])-1){
				if($value){
					$str_penyakit .= "'".$value->keterangan."'";
					$str_val .= $value->jml;
				}
				
			}else{
				if($value){
					if($value){
					$str_penyakit .= "'".$value->keterangan."',";
					$str_val .= $value->jml.",";
				}
				}
			}
		}

		$data["th_fn"] = $date;	
		$data["th_st"] = $date-2;
		$data["str_penyakit"] = $str_penyakit;
		$data["str_val"] = $str_val;	

		$this->load->view("front_data/kesehatan/penyakit", $data);

		// print_r("<pre>");
		// print_r($data);
	}

	  public function kesehatan_all($id_kategori,$th_fn){
        $kategori_filter = $this->lp->get_lp_kategori_filter(array("id_kategori"=> $id_kategori));
        $kategori_all = $this->lp->get_lp_kategori();        
        
        $th_st = $th_fn-2;
        $data_graph = array();
        $str_tbl = "";
        $data["list_data"] = array();

        //------------------------------------------------header-------------------------------------------------

        $str_title = "Data Kesehatan di Kota Malang Tahun ";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"25%\">Keterangan</td>";
        // $str_header_mod_sec ="";

        $t_col = 3;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td>".$i."</td>";
            
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Kategori</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 0;

        $str_dropdown_kategori = "";

        $roW_kate = 0;
        foreach ($kategori_filter as $r_kate => $v_kate) {
            $jenis = $this->lp->get_lp_jenis_filter(array("id_kategori"=>$v_kate->id_kategori));
            $data["list_data"][$v_kate->id_kategori]["kategori"]["main"]["id_jenis"] = $v_kate->id_kategori;
            $data["list_data"][$v_kate->id_kategori]["kategori"]["main"]["nama_kategori"] = $v_kate->nama_kategori;

            $str_tbl .= "<table class=\"data-table\" style=\"width: 100%;\">
                         <thead>
                          <tr>
                            <td colspan=\"2\">Data ".$v_kate->nama_kategori." di Kota Malang Tahun ".$th_st." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Kategori</td>
                          </tr>
                         </thead>";

            $row_jenis = 0;
            foreach ($jenis as $r_jenis => $v_jenis) {
                $sub_jenis = $this->lp->get_lp_sub(array("id_jenis"=>$v_jenis->id_jenis));
                $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_jenis->id_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
                $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_jenis->id_jenis]["main"]["nama_kategori"] = $v_jenis->nama_jenis;

                $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_jenis->id_jenis."\">
                                <td><a href=\"#\">".($row_jenis+1)."</a></td>
                                <td><a href=\"#\">".$v_jenis->nama_jenis."</a></td>
                            </tr>
                            <tr class=\"out_jenis\" id=\"out_jenis_".$v_jenis->id_jenis."\">
                                <td>&nbsp;</td>
                                <td>
                                     <table class=\"data-table\" style=\"width: 100%;\">".
                                     $str_header_mod;


                $row_sub = 0;
                foreach ($sub_jenis as $r_sub => $v_sub) {
                    $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_jenis->id_jenis]["sub_jenis"][$v_sub->id_sub_jenis]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                    $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_jenis->id_jenis]["sub_jenis"][$v_sub->id_sub_jenis]["main"]["nama_kategori"] = $v_sub->nama_sub_jenis;

                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_jenis->id_jenis."_".$v_sub->id_sub_jenis."\">
                                    <td><a href=\"#\">".($row_sub+1)."</a></td>
                                    <td><a href=\"#\">".$v_sub->nama_sub_jenis."</a></td>";
                    
                    $row_th = 0;
                    for($i=$th_st; $i <= $th_fn ; $i++){
                        $val_data = $this->lp->get_lp(array("id_jenis"=>$v_jenis->id_jenis, 
                                                            "id_sub_jenis"=>$v_sub->id_sub_jenis,
                                                            "th"=>$i));
                        $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_jenis->id_jenis]["sub_jenis"][$v_sub->id_sub_jenis]["detail"][$i] = $val_data;

                        if($val_data){
                            $str_tbl .= "<td>".$val_data["jml"]."</td>";
                        }else{
                            $str_tbl .= "<td>0</td>";
                        }


                        $data_graph[$v_kate->id_kategori][$v_jenis->id_jenis][$v_sub->id_sub_jenis][$row_th]["year"]=(string)$i;
                        $data_graph[$v_kate->id_kategori][$v_jenis->id_jenis][$v_sub->id_sub_jenis][$row_th]["val_data"]=(double)$val_data["jml"];

                        $row_th++;
                    }

                    $str_tbl .= "</tr>";

                    $row_sub++;
                }

                $str_tbl .= "           
                                    </table>
                                </td>
                            </tr>";
                $row_jenis++;
            }
            $str_tbl .= "</table>";
            $data["str_tbl"][$v_kate->id_kategori] = (string)$str_tbl;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
    $data["move_tbl"] = json_encode($data["str_tbl"]);
        // $data["str_series"] = $str_series;
        //$data["kategori"] = $kategori_all;
        $data["str_header"] = $str_header_top;
        $data["title"] = $str_title;
        $data["id_kategori"] = $id_kategori;


        foreach ($kategori_all as $r_kate_all => $v_kate_all) {
            $str_dropdown_kategori .= "<option id=\"val_".$v_kate_all->id_kategori."\" value=\"".$v_kate_all->id_kategori."\">".$v_kate_all->nama_kategori."</option>";    
        }
        
        $data["dropdown_kategori"] = "<select name=\"get_kate\" id=\"get_kate\" class=\"form-control select2\" style=\"width: 100%;\">".$str_dropdown_kategori."</select>";
        

        // print_r("<pre>");
       // print_r($data["data_graph"]);
     
        // print_r("<table border='1'>".$data["str_tbl"][$id_kategori]."</table>");

        
        $this->load->view("front_data/kesehatan/kesehatan_all", $data);

        // $this->load->view("front_lp/Pendidikan/lp_pendidikan_all",$data);
    }

	// public function penyakitTerbanyak(){
	// 	$data['page'] = "Mainkesehatan/penyakitTerbanyak";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('kesehatan/masterKesehatan',$data);
	// }


	// public function fasilitasKesehatan(){
	// 	$data['page'] = "Mainkesehatan/fasilitasKesehatan";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('kesehatan/masterKesehatan',$data);
	// }


}

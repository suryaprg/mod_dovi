<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maindata extends CI_Controller {


        
	public function index(){
	
		$this->load->view('front_data/dataselengkapnya/dataselengkapnya');
	}
	public function dataSatu(){
	
		$this->load->view('front_data/dataselengkapnya/datasatu');
	}
	public function dataDua(){
	
		$this->load->view('front_data/dataselengkapnya/datadua');
	}
	public function dataTiga(){
	
		$this->load->view('front_data/dataselengkapnya/datatiga');
	}
	public function dataEmpat(){
	
		$this->load->view('front_data/dataselengkapnya/dataempat');
	}
	public function dataLima(){
	
		$this->load->view('front_data/dataselengkapnya/datalima');
	}
	public function dataEnam(){
	
		$this->load->view('front_data/dataselengkapnya/dataenam');
	}
	public function dataTujuh(){
	
		$this->load->view('front_data/dataselengkapnya/datatujuh');
	}
	public function dataDelapan(){
	
		$this->load->view('front_data/dataselengkapnya/datadelapan');
	}
}
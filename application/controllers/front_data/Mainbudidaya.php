<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainbudidaya extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_budidaya", "mb");
				$this->load->model('front_lp/Lp_lahan', 'll');
        $this->load->model('front_lp/Lp_panen', 'lp');
		$this->load->library("response_message");
	}

	public function index(){
		$this->pertanian_lahan(date("Y"));
	}

	public function pertanian_lahan($date){
		// $data["list_data"] = array();
		// for ($i=$date-2; $i<=$date ; $i++) { 
		$data["list_data"] = $this->mb->get_pertanian_lahan(array("th"=>$date))->result();
		// }

		$data["th_st"] = $date-2;
		$data["th_fn"] = $date;		

		// print_r("<pre>");
		// print_r($data);

		$this->load->view("front_data/pertanian/lahan", $data);
	}

	public function pertanian_lahan_penggunaan($date){
		// $data["list_data"] = array();
		// for ($i=$date-2; $i<=$date ; $i++) { 
		$data["list_data"] = $this->mb->get_pertanian_lahan_penggunaan(array("th"=>$date))->result();
		// }
		$data["list_data_pn"] = $this->mb->get_pertanian_lahan(array("th"=>$date))->result();

		$data["th_st"] = $date-2;
		$data["th_fn"] = $date;		

		// print_r("<pre>");
		// print_r($data);

		$this->load->view("front_data/pertanian/lahan", $data);
	}

	public function pertanian_kelapa_tebu($th_fn){
		$data["kategori"] = "";
        
        $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Produksi Pertanian Kelapa dan Tebu di Kota Malang Tahun ".($th_st)." - ".$th_fn;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <td>Tahun Data</td>";

       
        $str_header_mod .= "<td width=\"40%\">Kelapa</td>
                            <td width=\"40%\">Tebu</td>";
        $colspan=3;
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$colspan."\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;

        $series = "createSeries(\"jml_kelapa\", \"Jumlah Tanaman Kelapa\");
                    createSeries(\"jml_tebu\", \"Jumlah Tanaman Tebu\");";
        $row_th = 0;

        for ($i=$th_st; $i<=$th_fn ; $i++) {
            $for_data = $this->mb->get_pertanian_kelapa_tebu(array("th"=>$i))->row_array();

            $str_tbl .= "<tr>
                            <td>".$i."</td>";
            
            $str_tbl .=     "<td>".$for_data["jml_kelapa"]."</td>
                            <td>".$for_data["jml_tebu"]."</td>";

            $str_tbl .= "</tr>";

            $data_graph[$row_th]["year"] = (string)$i;
            $data_graph[$row_th]["jml_kelapa"] = (double)$for_data["jml_kelapa"];
            $data_graph[$row_th]["jml_tebu"] = (double)$for_data["jml_tebu"];

            $row_th++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["series"] = $series;

        $data["data_graph"] = json_encode($data_graph);     

        // print_r("<pre>");
        // print_r($data);
        // print_r("<table>".$data["str_header"].$str_tbl."</table>");

        // $this->load->view("front_data/transportasi/jalan", $data);
		$this->load->view("front_data/pertanian/kelapa_tebu", $data);
	}

	public function pertanian_komoditas($date){
		$jenis = $this->mb->get_pertanian_komoditas_jenis();
		$data = array();

		foreach ($jenis as $r_data => $v_data) {
			$data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data;

			for ($i=$date-2; $i<=$date ; $i++) { 
				$data["list_data"][$r_data]["detail"][$i] = $this->mb->get_pertanian_komoditas(array("pk.id_jenis"=>$v_data->id_jenis, "th"=>$i))->row_array();
			}
		}
		
		$data["th_st"] = $date-2;
		$data["th_fn"] = $date;			

		// print_r("<pre>");
		// print_r($data);

		$this->load->view("front_data/pertanian/komoditi", $data);
	}

	public function pertanian_holti_jml($date){
		$data["page"] = "hortikultura";
		$jenis = $this->mb->get_pertanian_holti_jenis();
		// $data = array();

		foreach ($jenis as $r_data => $v_data) {
			// $data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data;

			// for ($i=$date-2; $i<=$date ; $i++) { 
			$data["list_data"][$r_data]= $this->mb->get_pertanian_holti_jml(array("pk.id_jenis"=>$v_data->id_jenis, "th"=>$date))->row_array();
			// }
		}
		
		$data["th_st"] = $date;
		$data["th_fn"] = $date;			

		// print_r("<pre>");
		// print_r($data);

		$this->load->view("front_data/pertanian/hortikultura", $data);
	}

	public function pertanian_ternak($th_fn){
		$jenis = $this->mb->get_pertanian_ternak_jenis();
        
        $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Produksi ternak, unggas, telur,dan susu di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

        //------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <td>No</td>
                                <td>Keterangan</td>
                                <td>Jenis Ternak</td>";

        $t_col = 3;

        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td>".$i."</td>";

            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;
        $series = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$r_data]["main"]["id_jenis"] = $v_data->id_jenis;

            $str_tbl.= "<tr>
                            <td><a href=\"#\">".($row_data+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";
            if($v_data->kategori == 0){
                $str_tbl.= "    <td>Hasil Ternak Non Unggas</td>";
            }elseif ($v_data->kategori == 1) {
                $str_tbl.= "    <td>Hasil Ternak Unggas</td>";
            }else {
                $str_tbl.= "    <td>Hasil Ternak Telur</td>";
            }

            $series .= "var series".$row_data." = chart.series.push(new am4charts.LineSeries());
                        series".$row_data.".dataFields.valueY = \"val_".$row_data."\";
                        series".$row_data.".dataFields.categoryX = \"year\";
                        series".$row_data.".name = \"".$v_data->nama_jenis."\";
                        series".$row_data.".strokeWidth = 3;
                        series".$row_data.".bullets.push(new am4charts.CircleBullet());
                        series".$row_data.".tooltipText = \"Jumlah {name} pada {categoryX}: {valueY}\";
                        series".$row_data.".legendSettings.valueText = \"{valueY}\";
                        series".$row_data.".visible  = false;";
            

            $row_th = 0;
// 

            for ($i=$th_st; $i<=$th_fn ; $i++) { 
                $val_data = $this->mb->get_pertanian_ternak_jml(array("pk.id_jenis"=>$v_data->id_jenis, "th"=>$i))->row_array();
                // print_r($val_data);
                $data["list_data"][$r_data]["detail"][$i] = $val_data;

                if($val_data){
                    $str_tbl .= "<td>".$val_data["jml"]."</td>";
                }else{
                    $str_tbl .= "<td>0</td>";
                }

                $data_graph[$row_th]["year"] = (int)$i;
                $data_graph[$row_th]["val_".$row_data] = (int)$val_data["jml"];

                $row_th++;
            }

            $row_data++;

            $str_tbl.= "</tr>";
        }
        
        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["series"] = $series;

        $data["data_graph"] = json_encode($data_graph);         

        // print_r("<pre>");
        // print_r($data["data_graph"]);

        // print_r("<table>".$data["str_header"].$str_tbl."</table>");
		$this->load->view("front_data/pertanian/peternakan", $data);
	}

	public function pertanian_ikan($th_fn){
		$jenis = $this->mb->get_pertanian_ikan_jenis();

        $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Produksi Ikan menurut Jenis Ikan (kg) di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

        //------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <td>No</td>
                                <td>Keterangan</td>
                                <td>Jenis Ikan</td>";

        $t_col = 3;

        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td>".$i."</td>";

            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;
        $series = "";
        
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$r_data]["main"]["id_jenis"] = $v_data->id_jenis;

            $str_tbl.= "<tr>
                            <td><a href=\"#\">".($row_data+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

            if($v_data->kategori == 0){
                $str_tbl.= "    <td>Budidaya Ikan Dalam Kolam</td>";
            }else{
                $str_tbl.= "    <td>Budidaya Ikan Dalam Keramba</td>";
            }

            $series .= "var series".$row_data." = chart.series.push(new am4charts.LineSeries());
                        series".$row_data.".dataFields.valueY = \"val_".$row_data."\";
                        series".$row_data.".dataFields.categoryX = \"year\";
                        series".$row_data.".name = \"".$v_data->nama_jenis."\";
                        series".$row_data.".strokeWidth = 3;
                        series".$row_data.".bullets.push(new am4charts.CircleBullet());
                        series".$row_data.".tooltipText = \"Jumlah {name} pada {categoryX}: {valueY}\";
                        series".$row_data.".legendSettings.valueText = \"{valueY}\";
                        series".$row_data.".visible  = false;";
            

            $row_th = 0;
            for ($i=$th_st; $i<=$th_fn ; $i++) { 
                $val_data = $this->mb->get_pertanian_ikan_jml(array("pk.id_jenis"=>$v_data->id_jenis, "th"=>$i))->row_array();
                $data["list_data"][$r_data]["detail"][$i] = $val_data;

                if($val_data){
                    $str_tbl .= "<td>".$val_data["jml"]."</td>";
                }else{
                    $str_tbl .= "<td>0</td>";
                }

                $data_graph[$row_th]["year"] = (int)$i;
                $data_graph[$row_th]["val_".$row_data] = (int)$val_data["jml"];

                $row_th++;
            }

            $row_data++;

            $str_tbl.= "</tr>";
        }
        
        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["series"] = $series;

        $data["data_graph"] = json_encode($data_graph);     

        // print_r("<pre>");
        // print_r($data["data_graph"]);


		$this->load->view("front_data/pertanian/perikanan", $data);
	}

	 public function lahan_kec($th_fn){
        $jenis = $this->ll->get_lp_jenis();
        $kecamatan = $this->ll->get_kec();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Data Luas Lahan (hektar) menurut Kecamatan dan Penggunaan Lahan ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $t_data = array();

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"bottom: 30px;\" width=\"100%\">".
                                 $str_header_mod;

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;
                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kec."</a></td>";
                
                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->ll->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data){
                        $str_tbl .= "<td>".$val_data["jml"]."</td>";
                    }else{
                        $str_tbl .= "<td>-</td>";
                    }
                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_data->id_jenis][$v_sub->id_kec][$row_th] = $main_data;
                    $row_th++;
                }

                $str_tbl .= "</tr>";
                
                $r_kec++;
            }
            $row_jenis++;
            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;
        $data["str_tbl"] = $str_tbl;
        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kecamatan"] = $kecamatan;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;
        // $data["jml_th"] = $row_th;

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_data/pertanian/lp_pertanian_lahan",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Lahan Kecamatan--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Panen Kecamatan--------------------------------------------------
#===========================================================================================================================
   public function panen_kec($th_fn){
        $jenis = $this->lp->get_lp_jenis();
        $kecamatan = $this->lp->get_kec();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Luas Penggunaan Lahan Sawah (hektar) menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"bottom: 30px;\" width=\"100%\">".
                                 $str_header_mod;            

            $t_data = array();

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kec."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $array_val;

                    if($val_data["jml_panen"]){
                        $str_tbl .= "<td>".$val_data["jml_panen"]."</td>";
                        $jml_panen = $val_data["jml_panen"];
                    }else{
                        $str_tbl .= "<td>-</td>";
                        $jml_panen = 0;
                    }

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml_panen"]);
                    $data_graph[$v_data->id_jenis][$v_sub->id_kec][$row_th] = $main_data;
                    $row_th++;

                    // if($val_data["luas_panen"]){
                    //     $str_tbl .= "<td>".$val_data["luas_panen"]."</td>";
                    //     $luas_panen = $val_data["luas_panen"];
                    // }else{
                    //     $str_tbl .= "<td>-</td>";
                    //     $luas_panen = 0;
                    // }

                    
                    // if($jml_panen == 0 or $luas_panen == 0){
                    //     $str_tbl .= "<td>-</td>";
                    // }else{
                    //     $prob_panen = $luas_panen/($jml_panen*10);
                    //     $str_tbl .= "<td>".$prob_panen."</td>";
                    // }
                }
                
                $r_kec++;
                $str_tbl .= "</tr>";
            }
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kecamatan"] = $kecamatan;
        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

         $this->load->view("front_data/pertanian/lp_pertanian_panen",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Luas Penen Kecamatan--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Hasil Panen Kecamatan--------------------------------------------------
#===========================================================================================================================
    public function hasil_panen_kec($th_fn){
        $jenis = $this->lp->get_lp_jenis();
        $kecamatan = $this->lp->get_kec();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Luas Penggunaan Lahan Sawah (hektar) menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" style=\"bottom: 30px;\" width=\"100%\">".
                                 $str_header_mod;            

            $t_data = array();

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kec."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $array_val;

                    // if($val_data["jml_panen"]){
                    //     $str_tbl .= "<td>".$val_data["jml_panen"]."</td>";
                    //     $jml_panen = $val_data["jml_panen"];
                    // }else{
                    //     $str_tbl .= "<td>-</td>";
                    //     $jml_panen = 0;
                    // }

                    
                    if($val_data["luas_panen"]){
                        $str_tbl .= "<td>".$val_data["luas_panen"]."</td>";
                        $luas_panen = $val_data["luas_panen"];
                    }else{
                        $str_tbl .= "<td>-</td>";
                        $luas_panen = 0;
                    }

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["luas_panen"]);
                    $data_graph[$v_data->id_jenis][$v_sub->id_kec][$row_th] = $main_data;
                    $row_th++;

                    
                    // if($jml_panen == 0 or $luas_panen == 0){
                    //     $str_tbl .= "<td>-</td>";
                    // }else{
                    //     $prob_panen = $luas_panen/($jml_panen*10);
                    //     $str_tbl .= "<td>".$prob_panen."</td>";
                    // }
                }
                
                $r_kec++;
                $str_tbl .= "</tr>";
            }
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kecamatan"] = $kecamatan;
        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

         $this->load->view("front_data/pertanian/lp_pertanian_panen_lahan",$data);
    }
#===========================================================================================================================
#-----------------------------------------------------Hasil Penen Kecamatan--------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------Produktivitas Panen Kecamatan--------------------------------------------------
#===========================================================================================================================
    public function prod_panen_kec($th_fn){
        $jenis = $this->lp->get_lp_jenis();
        $kecamatan = $this->lp->get_kec();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td>No</td>
                                <td>Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td>".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Luas Penggunaan Lahan Sawah (hektar) menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" width=\"100%\">".
                                 $str_header_mod;            

            $t_data = array();

            $r_kec = 0;
            foreach ($kecamatan as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kec."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lp->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kec"=> $v_sub->id_kec, "th"=>$i));
                    $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                    $data["list_data"][$row_jenis]["kecamatan"][$r_kec]["value"][$i] = $array_val;

                    if($val_data["jml_panen"]){
                        // $str_tbl .= "<td>".$val_data["jml_panen"]."</td>";
                        $jml_panen = $val_data["jml_panen"];
                    }else{
                        // $str_tbl .= "<td>-</td>";
                        $jml_panen = 0;
                    }

                    
                    if($val_data["luas_panen"]){
                        // $str_tbl .= "<td>".$val_data["luas_panen"]."</td>";
                        $luas_panen = $val_data["luas_panen"];
                    }else{
                        // $str_tbl .= "<td>-</td>";
                        $luas_panen = 0;
                    }                 
                    
                    $prob_panen = 0;
                    if($jml_panen == 0 or $luas_panen == 0){
                        $str_tbl .= "<td>-</td>";
                    }else{
                        $prob_panen = (($jml_panen*10)/$luas_panen)*100;
                        $str_tbl .= "<td>".number_format($prob_panen, 2, ".", ",")."</td>";
                    }

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$prob_panen);
                    $data_graph[$v_data->id_jenis][$v_sub->id_kec][$row_th] = $main_data;
                    $row_th++;

                }
                
                $r_kec++;
                $str_tbl .= "</tr>";
            }
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kecamatan"] = $kecamatan;
        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_data/pertanian/lp_pertanian_panen_prob",$data);
    }
	// public function sawahKebun(){
	// 	$data['page'] = "Mainbudidaya/sawahKebun";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('budidaya/masterBudidaya',$data);
	// }


	// public function perikanan(){
	// 	$data['page'] = "Mainbudidaya/perikanan";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('budidaya/masterBudidaya',$data);
	// }

	// public function peternakan(){
	// 	$data['page'] = "Mainbudidaya/peternakan";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('budidaya/masterBudidaya',$data);
	// }

	
}
	
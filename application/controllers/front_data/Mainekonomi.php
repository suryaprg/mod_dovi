<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainekonomi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_ekonomi", "me");
		$this->load->model('front_lp/Lp_keuangan', 'lk');
        $this->load->model('front_lp/Lp_koperasi', 'lkop');

        $this->load->model('front_lp/Lp_tenaga', 'lt');

        $this->load->model('front_lp/Lp_pend_ind', 'lpi');
        $this->load->model('front_lp/Lp_pajak', 'lp');
		$this->load->model('front_lp/Lp_pdb', 'lpb');
        $this->load->model('front_lp/Lp_dis_perdagangan', 'lper');

		$this->load->library("response_message");
	}

	public function index(){
		// $this->index_keuangan(date("Y"));
		$data["page"] = "pendapatan_regional";

		$this->load->view("front_data/ekonomi/main_ekonomi", $data);
	
	}

	public function index_pendapatan_usaha($th_fn){
        $data["page"] = "pendapatan_lap_usaha";
    $jenis = $this->me->get_pendapatan_usaha_jenis();

        $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Data Pendapatan Lapangan Usaha Kota Malang Tahun ".($th_fn-2)." - ".$th_fn." (Persen)";

        //------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <td>No</td>
                                <td>Keterangan</td>";

        $t_col = 2;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td>".$i."</td>";
            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;

        $series = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["pendapatan_usaha"][$r_data]["main"]["id_jenis"]= $v_data->id_jenis;
            $data["pendapatan_usaha"][$r_data]["main"]["nama_jenis"]= $v_data->nama_jenis;

            $str_tbl.= "<tr>
                            <td><a href=\"#\">".($row_data+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";
            

            $row_th = 0;
            for ($i=$th_st; $i<=$th_fn ; $i++) { 
                $val_data = $this->me->get_pendapatan_usaha(array("th"=>$i, "plu.id_jenis"=>$v_data->id_jenis))->row_array();
                $data["list_data"][$row_data]["detail"][$i] = $val_data;

                // print_r($val_data);
                if($val_data){
                    $str_tbl .= "<td>".$val_data["jml"]."%</td>";
                }else{
                    $str_tbl .= "<td>0</td>";
                }

                $data_graph[$row_th]["year"] = (int)$i;
                $data_graph[$row_th]["val_".$row_data] = (double)$val_data["jml"];
                $row_th++;
            }
            $str_tbl.= "</tr>";

            $series .= "var series_".$row_data." = chart.series.push(new am4charts.LineSeries());
                        series_".$row_data.".dataFields.valueY = \"val_".$row_data."\";
                        series_".$row_data.".dataFields.categoryX = \"year\";
                        series_".$row_data.".name = \"".$v_data->nama_jenis."\";
                        series_".$row_data.".strokeWidth = 3;
                        series_".$row_data.".bullets.push(new am4charts.CircleBullet());
                        series_".$row_data.".tooltipText = \"Jumlah kendaraan pada {categoryX}: {valueY}\";
                        series_".$row_data.".legendSettings.valueText = \"{valueY}\";
                        series_".$row_data.".visible  = false;";

            $row_data++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["series"] = $series;

        $data["data_graph"] = json_encode($data_graph);

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table>".$data["str_header"].$str_tbl."</table>");
        $this->load->view("front_data/ekonomi/main_ekonomi", $data);
	}


	// public function index_pendapatan_regional($date){
	// 	$data["page"] = "pendapatan_regional";

	// 	$data["pendapatan_regional"] = array();
	// 	for ($i=$date-2; $i<=$date ; $i++) { 
	// 		$data["pendapatan_regional"][$i] = $this->me->get_pendapatan_regional(array("th_pendapatan"=>$i))->row_array();
	// 	}

	// 	$jenis = $this->me->get_pendapatan_usaha_jenis();
	// 	// $data["list_jenis"] = $jenis;
	// 	$data["pendapatan_usaha"] = array();

	// 	foreach ($jenis as $r_data => $v_data) {
	// 		$data["pendapatan_usaha"][$r_data]["main"]["nama_jenis"]= $v_data->nama_jenis;
	// 		for ($i=$date-2; $i<=$date ; $i++) { 
	// 			$data["pendapatan_usaha"][$r_data]["detail"][$i] = $this->me->get_pendapatan_usaha(array("th"=>$i, "plu.id_jenis"=>$v_data->id_jenis))->row_array();
	// 		}
	// 	}

	// 	$data["th_st"] = $date-2;
	// 	$data["th_fn"] = $date;

	// 	$this->load->view("front_data/ekonomi/main_ekonomi", $data);
	// 	// print_r("<pre>");
	// 	// print_r($data);
	// }

    public function index_pendapatan_regional($th_fn){
        $data["page"] = "pendapatan_regional";

        $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "PDRB Kota Malang Atas Dasar Harga Berlaku di Kota Malang Tahun ".($th_st)." - ".$th_fn." (Rupiah)";

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <td>Tahun Data</td>";

       
        $str_header_mod .= "<td width=\"65%\">Jumlah Pendapatan Regional</td>";
        $colspan=2;
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$colspan."\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;

        $series = "";
        $row_th = 0;

        for ($i=$th_st; $i<=$th_fn ; $i++) {
            $for_data = $this->me->get_pendapatan_regional(array("th_pendapatan"=>$i))->row_array();
            $data["list_data"][$i] = $for_data;


            $str_tbl .= "<tr>
                            <td>".$i."</td>";

            $str_tbl .= "   <td>Rp ".$for_data["jml_pendapatan"]."</td>";
            
            $str_tbl .= "</tr>";

            $data_graph[$row_th]["country"] = (string)$i;
            $data_graph[$row_th]["val_data"] = (double)$for_data["jml_pendapatan"];

            
            $row_th++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["series"] = $series;

        $data["data_graph"] = json_encode($data_graph);

        // print_r($data["data_graph"]);    

        $this->load->view("front_data/ekonomi/main_ekonomi", $data);
        // print_r("<pre>");
        // print_r($data);
    }


	public function index_keuangan($th_fn){
        $data["page"] = "keuangan_daerah";
        $data["_get_realisasi"] = $this->_get_realisasi($th_fn);
        $data["_get_belanja"] = $this->_get_belanja($th_fn);
        
        $th_st = $th_fn-2;

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;
        
        // print_r("<pre>");
        // print_r($data);
        // print_r($data["_get_realisasi"]["data_graph"]);
        // print_r("<table>".$data["_get_realisasi"]["str_header"].$data["_get_realisasi"]["str_tbl"]."</table>");

        $this->load->view("front_data/ekonomi/main_ekonomi",$data);
    }

    public function _get_realisasi($th_fn){
        $jenis = $this->me->get_keuangan_jenis_kate(array("kategori"=>"0"));

        $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Realisasi Pendapatan Daerah ".($th_fn-2)." - ".$th_fn." (Rupiah)";

        //------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <td>No</td>
                                <td>Keterangan</td>";

        $t_col = 3;

        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td>".$i."</td>";

            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;
        $series = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$r_data]["main"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->ket;

            $str_tbl.= "<tr>
                            <td><a href=\"#\">".($row_data+1)."</a></td>
                            <td><a href=\"#\">".$v_data->ket."</a></td>";

            $series .= "var series".$row_data." = chart.series.push(new am4charts.LineSeries());
                        series".$row_data.".dataFields.valueY = \"val_".$row_data."\";
                        series".$row_data.".dataFields.categoryX = \"year\";
                        series".$row_data.".name = \"".$v_data->ket."\";
                        series".$row_data.".strokeWidth = 3;
                        series".$row_data.".bullets.push(new am4charts.CircleBullet());
                        series".$row_data.".tooltipText = \"Jumlah {name} pada {categoryX}: {valueY}\";
                        series".$row_data.".legendSettings.valueText = \"{valueY}\";
                        series".$row_data.".visible  = false;";
            
            $row_th = 0;

            for ($i=$th_st; $i<=$th_fn ; $i++) { 
                $val_data = $this->me->get_keuangan(array("kjm.id_jenis"=>$v_data->id_jenis, "th"=>$i))->row_array();
                $data["list_data"][$row_data]["detail"][$i] = $val_data;

                if($val_data){
                    $str_tbl .= "<td>Rp. ".number_format($val_data["jml"], 2, ".", ",")."</td>";
                }else{
                    $str_tbl .= "<td>0</td>";
                }

                $data_graph[$row_th]["year"] = (int)$i;
                $data_graph[$row_th]["val_".$row_data] = (double)$val_data["jml"];

                $row_th++;
            }

            $row_data++;

            $str_tbl.= "</tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["series"] = $series;

        $data["data_graph"] = json_encode($data_graph); 
        

        return $data;
        
    }

    public function _get_belanja($th_fn){
        $jenis = $this->me->get_keuangan_jenis_kate(array("kategori"=>"1"));

        $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Realisasi Belanja Daerah ".($th_fn-2)." - ".$th_fn." (Rupiah)";

        //------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <td>No</td>
                                <td>Keterangan</td>";

        $t_col = 3;

        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td>".$i."</td>";

            $t_col += 1;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;
        $series = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$r_data]["main"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->ket;

            $str_tbl.= "<tr>
                            <td><a href=\"#\">".($row_data+1)."</a></td>
                            <td><a href=\"#\">".$v_data->ket."</a></td>";

            $series .= "var series".$row_data." = chart.series.push(new am4charts.LineSeries());
                        series".$row_data.".dataFields.valueY = \"val_".$row_data."\";
                        series".$row_data.".dataFields.categoryX = \"year\";
                        series".$row_data.".name = \"".$v_data->ket."\";
                        series".$row_data.".strokeWidth = 3;
                        series".$row_data.".bullets.push(new am4charts.CircleBullet());
                        series".$row_data.".tooltipText = \"Jumlah {name} pada {categoryX}: {valueY}\";
                        series".$row_data.".legendSettings.valueText = \"{valueY}\";
                        series".$row_data.".visible  = false;";
            
            $row_th = 0;

            for ($i=$th_st; $i<=$th_fn ; $i++) { 
                $val_data = $this->me->get_keuangan(array("kjm.id_jenis"=>$v_data->id_jenis, "th"=>$i))->row_array();
                $data["list_data"][$row_data]["detail"][$i] = $val_data;

                if($val_data){
                    $str_tbl .= "<td>Rp. ".number_format($val_data["jml"],2,',','.')."%</td>";
                }else{
                    $str_tbl .= "<td>0</td>";
                }

                $data_graph[$row_th]["year"] = (int)$i;
                $data_graph[$row_th]["val_".$row_data] = (double)$val_data["jml"];

                $row_th++;
            }

            $row_data++;

            $str_tbl.= "</tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["series"] = $series;

        $data["data_graph"] = json_encode($data_graph); 
        

        return $data;
    }


	public function pengeluaran_penduduk($th_fn){
		$data["page"] = "pengeluaran_penduduk";
		$jenis = $this->me->get_pengeluaran_penduduk_jenis();

        $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Data Pengeluaran Penduduk Kota Malang Tahun ".($th_fn-2)." - ".$th_fn." (Persen)";

        //------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <td>No</td>
                                <td>Jenis Pendapatan</td>";

        $t_col = 2;

        // $r_th = 0;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td>".$i."</td>";
            $t_col += 1;

            

            // $r_th++;
        }


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;

        $series = "";
        // $data = array();

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$r_data]["main"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$r_data]["main"]["id_jenis"] = $v_data->id_jenis;

            $series .= "var series".$row_data." = chart.series.push(new am4charts.LineSeries());
                   series".$row_data.".dataFields.valueY = \"val_".$row_data."\";
                   series".$row_data.".dataFields.categoryX = \"year\";
                   series".$row_data.".name = \"".$v_data->nama_jenis."\";
                   series".$row_data.".strokeWidth = 3;
                   series".$row_data.".bullets.push(new am4charts.CircleBullet());
                   series".$row_data.".tooltipText = \"Place taken by {name} in {categoryX}: {valueY}\";
                   series".$row_data.".legendSettings.valueText = \"{valueY}\";
                   series".$row_data.".visible  = false;";

            $str_tbl.= "<tr>
                            <td><a href=\"#\">".($row_data+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i<=$th_fn ; $i++) { 
                $val_data = $this->me->get_pengeluaran_penduduk(array("ppj.id_jenis"=>$v_data->id_jenis, "th"=>$i))->row_array();
                $data["list_data"][$r_data]["detail"][$i] = $val_data;

                $data_graph[$row_th]["year"] = $i; 
                $data_graph[$row_th]["val_".$row_data] = $val_data["jml"]; 

                if($val_data){
                    $str_tbl .= "<td>".$val_data["jml"]."%</td>";

                }else{
                    $str_tbl .= "<td>0</td>";
                }

                $row_th++;
            }

            $str_tbl.= "<tr>";
            $row_data++;
        }

        // $data["jml"] = $jml;
        
        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["series"] = $series;

        $data["data_graph"] = json_encode($data_graph);         

        // print_r("<pre>");
        // print_r($data["data_graph"]);

        // print_r("<table>".$data["str_header"].$str_tbl."</table>");
		$this->load->view("front_data/ekonomi/main_ekonomi",$data);
	}

	public function pengeluaran_rmt_tgg($th_fn){
		$data["page"] = "pengeluaran_rumah_tangga";

		$th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Data Pengeluaran Rumah Tangga Kota Malang Tahun ".($th_fn-2)." - ".$th_fn." (Persen)";

        //------------------------------------------------header-------------------------------------------------
        
        $str_header_mod = "<tr>
                                <td>Tahun data</td>
                                <td>Jumlah Pengeluran Untuk Makan</td>
                                <td>Jumlah Pengeluran Untuk Yang Bukan Makan</td>";

        $t_col = 3;


        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title_data."</td>
                        </tr>";

        $series = "createSeries(\"jml_mkn\", \"Jumlah Pengeluran Untuk Makan\");
                   createSeries(\"jml_non\", \"Jumlah Pengeluran Untuk Yang Bukan Makan\");";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;

        for ($i=$th_st; $i<=$th_fn ; $i++) {
            $val_data = $this->me->pengeluaran_rmt_tgg(array("th"=>$i))->row_array(); 
            $data["list_data"][$i] = $val_data;

            // print_r($val_data);

            if($val_data){
                $data_graph[$row_data]["year"] = (string)$i;
                $data_graph[$row_data]["jml_mkn"] = (double)$val_data["jml_mkn"];
                $data_graph[$row_data]["jml_non"] = (double)$val_data["jml_non"];

                $str_tbl.= "<tr>
                            <td><a href=\"#\">".$i."</a></td>
                            <td>".$val_data["jml_mkn"]."%</td>
                            <td>".$val_data["jml_non"]."%</td>";
                $str_tbl.= "</tr>";

            }else{
                $data_graph[$row_data]["year"] = (string)$i;
                $data_graph[$row_data]["jml_mkn"] = (double)0;
                $data_graph[$row_data]["jml_non"] = (double)0;

                $str_tbl.= "<tr>
                            <td><a href=\"#\">".$i."</a></td>
                            <td>0</td>
                            <td>0</td>";
                $str_tbl.= "</tr>";
            }

            $row_data++;
        }

        // print_r("<pre>");
        // print_r($data);
        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;
        $data["series"] = $series;

        $data["data_graph"] = json_encode($data_graph);     

        // print_r($data["data_graph"]);
        // print_r("<table>".$data["str_header"].$str_tbl."</table>");
		$this->load->view("front_data/ekonomi/main_ekonomi",$data);
	}


	
	public function lp_keuangan_e($th_fn){
		$data["page"] = "lp_keuangan_e";

        $th_st = $th_fn - 2;

        $keuangan_jenis = $this->lk->get_lp_keu_jenis();
        $data["keuangan"] = array();
        foreach ($keuangan_jenis as $r_data => $v_data) {
            // print_r($v_data);
            $data["keuangan"][$v_data->id_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["keuangan"][$v_data->id_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;
            $data["keuangan"][$v_data->id_jenis]["jenis"]["kategori"] = $v_data->kategori;

            $data_sub_jenis = $this->lk->get_lp_keu_sub_jenis(array("lksj.id_jenis"=>$v_data->id_jenis));

            foreach ($data_sub_jenis as $r_data_sub => $v_data_sub) {
                $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["nama_sub_jenis"] = $v_data_sub->nama_sub_jenis;
                $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["id_sub_jenis"] = $v_data_sub->id_sub_jenis;

                $row_graph = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) { 
                    $data_keu = $this->lk->get_lp_keu(array("lk.id_jenis"=>$v_data->id_jenis,
                                                                    "lk.id_sub_jenis"=>$v_data_sub->id_sub_jenis,
                                                                    "lk.th"=>$i
                                                            )
                    
                                                    );
                    $jml = $data_keu["jml"];
                    $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["value"][$i] = $jml;

                    $tmp_array = array("year"=>$i, "val_jml"=>0);

                    if($jml){
                        $tmp_array["val_jml"] = (int)$jml;
                    }
                    $data["keuangan"][$v_data->id_jenis]["sub_jenis"][$v_data_sub->id_sub_jenis]["data_graph"][$row_graph] = $tmp_array;
                    
                    $row_graph++;
                }
            }
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $data["json_data"] = json_encode($data["keuangan"]);

        // print_r("<pre>");
        // print_r($data);

        
        $this->load->view("front_data/ekonomi/main_ekonomi",$data);
    }

        public function koperasi($th_fn){

        	$data["page"] = "koperasi";
          $jenis = $this->lkop->get_lp_jenis()->result();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Data Koperasi di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Koperasi</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_jenis = 0;
        $str_tbl = "";

        $data_graph = array();
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_data->id_jenis;
            
            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border='1' class=\"data-table\" width=\"100%\">";
            $str_tbl .= $str_header_mod;

            $sub_jenis = $this->lkop->get_lp_jenis_where(array("id_jenis"=>$v_data->id_jenis))->result();
            if(count($sub_jenis)){
                
                $row_sub = 0;
                foreach ($sub_jenis as $r_sub => $v_sub) {
                    $data["list_data"][$row_jenis]["main_sub"][$row_sub]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;
                    $data["list_data"][$row_jenis]["main_sub"][$row_sub]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;

                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_sub_jenis."\">
                                <td><a href=\"#\">".($row_sub+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_sub_jenis."</a></td>";
                    
                    $row_th = 0;
                    for ($i=$th_fn-2; $i <= $th_fn ; $i++) {
                        $val_data = $this->lkop->get_lp(array("id_sub_jenis"=>$v_sub->id_sub_jenis, "th"=>$i));
                        $data["list_data"][$row_jenis]["main_sub"][$row_sub]["value"][$i] = $val_data["jml"];

                        $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                        $data_graph[$v_data->id_jenis][$v_sub->id_sub_jenis][$row_th] = $main_data;

                        $str_tbl .= "<td align=\"right\">".number_format($val_data["jml"], 2, ".", ",")."</td>";
                        $row_th++;
                    }
                    // $data["list_data"][$row_jenis]["value"];

                    $str_tbl .= "</tr>";

                    $row_sub++;
                }
            }else{
                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_0\">
                                <td><a href=\"#\">1</a></td>
                                <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

                $row_th = 0;
                for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                    $val_data = $this->lkop->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                    $data["list_data"][$row_jenis]["value"][$i] = $val_data["jml"];

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_data->id_jenis][0][$row_th] = $main_data;

                    $str_tbl .= "<td align=\"right\">".number_format($val_data["jml"], 2, ".", ",")."</td>";

                    $row_th++;
                }

                $str_tbl .= "</tr>";
            }

            $str_tbl .= "           
                                </table>
                            </td>
                        </tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;
        
        $data["data_graph"] = json_encode($data_graph);

        // print_r("<pre>");
        // print_r($data);
        // print_r($data_graph);

        // print_r("<table border='1'>".$str_tbl."</table>");
        $this->load->view("front_data/ekonomi/main_ekonomi",$data);
    }

#===========================================================================================================================
#------------------------------------------------------------Koperasi-------------------------------------------------------
#===========================================================================================================================


#===========================================================================================================================
#------------------------------------------------------------Tenaga-------------------------------------------------------
#===========================================================================================================================

    public function tenaga($th_fn){
    	$data["page"] = "tenaga";
        $jenis = $this->lt->get_lp_jenis();
        $kategori = $this->lt->get_lp_kategori();

        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";
        
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

            $row_kategori = 0;
            foreach ($kategori as $r_kate => $v_kate) {
                $data["list_data"][$row_jenis]["kategori"][$row_kategori]["main"]["nama_kategori"] = $v_kate->nama_kategori;
                $data["list_data"][$row_jenis]["kategori"][$row_kategori]["main"]["id_kategori"] = $v_kate->id_kategori;

                $str_series = "";
                $row_th = 0;
                for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 

                    $str_series.= "createSeries(\"val_".$row_th."\", \"".$i."\");";

                    $val_data = $this->lt->get_lp_tenaga(array("id_jenis"=>$v_data->id_jenis, "id_kategori"=> $v_kate->id_kategori, "th"=>$i));
                    $data["list_data"][$row_jenis]["kategori"][$row_kategori]["value"][$i] = $val_data["jml"];

                    if($val_data){
                        // $str_tbl .= "<td>".$val_data["jml"]."</td>";
                        $str_tbl .= "<td>".number_format($val_data["jml"],2,".",",")."</td>";
                    }else{
                        $str_tbl .= "<td>-</td>";
                    }

                    $data_graph[$v_data->id_jenis][$row_kategori]["year"] = $v_kate->nama_kategori;
                    $data_graph[$v_data->id_jenis][$row_kategori]["val_".$row_th] = (float)$val_data["jml"];
                    

                    $row_th++;

                    

                }

                $row_kategori++;
            }

            $row_jenis++;
            $str_tbl .= "</tr>";
        }

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;
        $data["str_tbl"] = $str_tbl;
        $data["graph_data"] = json_encode($data_graph);
        $data["str_series"] = $str_series;
        $data["kategori"] = $kategori;
        $data["jml_th"] = $row_th;

        // print_r("<pre>");
        // print_r($data["graph_data"]);
        // print_r("<table>".$str_tbl."</table>");

        $this->load->view("front_data/ekonomi/main_ekonomi",$data);
    }

#===========================================================================================================================
#------------------------------------------------------------Tenaga-------------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#--------------------------------------------------------Pendapatan Industri------------------------------------------------
#===========================================================================================================================
    public function pendapatan_ind($th_fn){
     $jenis = $this->lpi->get_lp_jenis();
        $kategori = $this->lpi->get_lp_kategori();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Rekapan Pendataan Industri menurut Jenis Industri di Kota Malang di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td colspan=\"".($t_col-1)."\">Jenis Industri</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table class=\"data-table\" width=\"100%\">";
            $str_tbl .= $str_header_mod;     

            $t_data = array();

            $r_kec = 0;
            foreach ($kategori as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["id_kategori"] = $v_sub->id_kategori;
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["nama_kategori"] = $v_sub->nama_kategori;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kategori."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kategori."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lpi->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kategori"=> $v_sub->id_kategori, "th"=>$i));
                    $data["list_data"][$row_jenis]["kategori"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data["jml"]){
                        $str_tbl .= "<td>".$val_data["jml"]."</td>";
                        // $jml_panen = $val_data["jml_panen"];
                    }else{
                        $str_tbl .= "<td>-</td>";
                        // $jml_panen = 0;
                    }

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_data->id_jenis][$v_sub->id_kategori][$row_th] = $main_data;
                    $row_th++;
                }
                $str_tbl .= "</tr>";
                
                $r_kec++;
            }
            $str_tbl .= "           
                                </table>
                            </td>
                        </tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kategori"] = $kategori;

        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;
        
        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_data/ekonomi/pend_industri",$data);
    }

     public function pajak($th_fn){
     	$data["page"] = "pajak";
        $pajak = $this->lp->get_lp_pajak();

        $row_kec = 0;

        $data_graph = array();
        $series = "";
        foreach ($pajak as $r_data => $v_data) {
            $data["list_data"][$row_kec]["main"]["pajak"] = $v_data->nama_jenis;
            $data["list_data"][$row_kec]["main"]["id_jenis"] = $v_data->id_jenis;
            
            $row_th = 0;
            for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 

                $val_data = $this->lp->get_lp(array("lpt.id_jenis"=>$v_data->id_jenis, "th"=>$i));

                if(!isset($data_graph[$row_th]["year"])){
                    $data_graph[$row_th]["year"] = "$i";
                }

                if(!isset($data_graph[$row_th]["jml"])){
                    $data_graph[$row_th]["jml"] = (int)$val_data["jml"];
                }else {
                    $data_graph[$row_th]["jml"] += (int)$val_data["jml"];
                }

                $data_graph[$row_th][$v_data->id_jenis] =  (int)$val_data["jml"];

                
                $data["list_data"][$row_kec]["value"][$i] = $val_data["jml"];

                $row_th++;
            }

            $series .= "createSeries(\"".$v_data->id_jenis."\", \"".$v_data->nama_jenis."\");";

            $row_kec++;
        }
        $series .= "createSeries(\"jml\", \"Total Pajak Kota Malang\");";

        $data["graph_data"] = json_encode($data_graph);
        $data["series_data"] = $series;

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;


        // print_r("<pre>");
        // print_r($data);
        // print_r($data_graph);


        $this->load->view("front_data/ekonomi/main_ekonomi",$data);

    }
      public function pdb($th_fn){
        $data["page"] = "pdb";
        $jenis = $this->lpb->get_lp_jenis();
        $kategori = $this->lpb->get_lp_kategori();

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td>No</td>
                                <td>Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td>".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Produk Domestik Regional Bruto Atas Dasar Harga Berlaku Menurut
Lapangan Usaha di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Kategori</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" width=\"100%\">".
                                 $str_header_mod;            

            $t_data = array();

            $r_kec = 0;
            foreach ($kategori as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["id_kategori"] = $v_sub->id_kategori;
                $data["list_data"][$row_jenis]["kategori"][$r_kec]["main"]["nama_kategori"] = $v_sub->nama_kategori;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kategori."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_kategori."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lpb->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_kategori"=> $v_sub->id_kategori, "th"=>$i));
                    // $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                    $data["list_data"][$row_jenis]["kategori"][$r_kec]["value"][$i] = $val_data["jml"];

                    if($val_data["jml"]){
                        $str_tbl .= "<td>".$val_data["jml"]."</td>";
                        $jml = $val_data["jml"];
                    }else{
                        $str_tbl .= "<td>-</td>";
                        $jml = 0;
                    }
                    
                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_data->id_jenis][$v_sub->id_kategori][$row_th] = $main_data;
                    $row_th++;

                }
                
                $r_kec++;
                $str_tbl .= "</tr>";
            }
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["kategori"] = $kategori;
        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        $this->load->view("front_data/ekonomi/main_ekonomi",$data);
    }
#===========================================================================================================================
#--------------------------------------------------------Pendapatan Industri------------------------------------------------
#===========================================================================================================================


   public function perdagangan($th_fn){
    $data["page"] = "perdagangan";
        $jenis = $this->lper->get_lp_jenis();
        
        $str_title = "Data Dinas Perdagangan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;
        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"45%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"15%\">".$i."</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Kategori</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                        </tr>
                        <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                            <td>&nbsp;</td>
                            <td>
                                 <table border=\"1\" class=\"data-table\" width=\"100%\">".
                                 $str_header_mod;            

            $t_data = array();
            $kategori = $this->lper->get_lp_kategori(array("id_jenis"=>$v_data->id_jenis));

            if($kategori){
                $r_sub = 0;
                foreach ($kategori as $r_sub => $v_sub) {
                    $data["list_data"][$row_jenis]["sub_jenis"][$r_sub]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                    $data["list_data"][$row_jenis]["sub_jenis"][$r_sub]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;

                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_sub_jenis."\">
                                    <td><a href=\"#\">".($r_sub+1)."</a></td>
                                    <td><a href=\"#\">".$v_sub->nama_sub_jenis."</a></td>";

                    $row_th = 0;
                    for ($i=$th_st; $i <= $th_fn ; $i++) {
                        $val_data = $this->lper->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_sub_jenis"=> $v_sub->id_sub_jenis, "th"=>$i));
                        // $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                        $data["list_data"][$row_jenis]["sub_jenis"][$r_sub]["value"][$i] = $val_data["jml"];

                        if($val_data["jml"]){
                            $str_tbl .= "<td>".$val_data["jml"]."</td>";
                            $jml = $val_data["jml"];
                        }else{
                            $str_tbl .= "<td>-</td>";
                            $jml = 0;
                        }
                        
                        $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                        $data_graph[$v_data->id_jenis][$v_sub->id_sub_jenis][$row_th] = $main_data;
                        $row_th++;

                    }
                    
                    $r_sub++;
                    $str_tbl .= "</tr>";
                }
            }else{
                    $data["list_data"][$row_jenis]["sub_jenis"][$r_sub]["main"]["id_sub_jenis"] = $v_data->id_jenis;
                    $data["list_data"][$row_jenis]["sub_jenis"][$r_sub]["main"]["nama_sub_jenis"] = $v_data->nama_jenis;

                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_0\">
                                    <td><a href=\"#\">1</a></td>
                                    <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

                    $row_th = 0;
                    for ($i=$th_st; $i <= $th_fn ; $i++) {
                        $val_data = $this->lper->get_lp(array("id_jenis"=>$v_data->id_jenis, "id_sub_jenis"=> 0, "th"=>$i));
                        // $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                        $data["list_data"][$row_jenis]["sub_jenis"][0]["value"][$i] = $val_data["jml"];

                        if($val_data["jml"]){
                            $str_tbl .= "<td>".$val_data["jml"]."</td>";
                            $jml = $val_data["jml"];
                        }else{
                            $str_tbl .= "<td>-</td>";
                            $jml = 0;
                        }
                        
                        $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                        $data_graph[$v_data->id_jenis][0][$row_th] = $main_data;
                        $row_th++;

                    }
                    
                    
                    $str_tbl .= "</tr>";
            }
            
            $row_jenis++;

            $str_tbl .= "           </tr>
                                </table>
                            </td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        // $data["kategori"] = $kategori;
        $data["str_header"] = $str_header_top;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        // $this->load->view("front_lp/pdb/lp_pdb",$data);

        $this->load->view("front_data/ekonomi/main_ekonomi",$data);
    }
}



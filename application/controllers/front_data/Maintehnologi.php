<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintehnologi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_tehnologi", "mt");
		 $this->load->model('front_lp/Lp_tehnologi', 'lt');
          $this->load->model('front_lp/Lp_tehnologi', 'lp');
		$this->load->library("response_message");
	}

	public function index(){
		$this->tehnologi_jml_web_opd(date("Y"));
	}

	public function tehnologi_jml_bts($date){
		$data["page"] = "bts";
		$data["kategori"] = "";
		$data["list_data"] = array();
		for ($i=$date-2; $i<=$date ; $i++) { 
			$data["list_data"][$i] = $this->mt->get_tehnologi_jml_bts(array("th"=>$i))->row_array();
		}

		$data["th_st"] = $date-2;
		$data["th_fn"] = $date;		

		// print_r("<pre>");
		// print_r($data);

		$this->load->view("front_data/tehnologi/jml_bts", $data);
	}

	public function tehnologi_jml_warnet($date){
		$data["page"] = "warnet";
		$data["kategori"] = "";
		$data["list_data"] = array();
		for ($i=$date-2; $i<=$date ; $i++) { 
			$data["list_data"][$i] = $this->mt->get_tehnologi_jml_warnet(array("th"=>$i))->row_array();
		}

		$data["th_st"] = $date-2;
		$data["th_fn"] = $date;		

		// print_r("<pre>");
		// print_r($data);
		$this->load->view("front_data/tehnologi/jml_warnet", $data);
	}

	public function tehnologi_jml_web_opd($date){
		$data["kategori"] = "";
		$data["list_data"] = array();
		for ($i=$date-2; $i<=$date ; $i++) { 
			$data["list_data"][$i] = $this->mt->get_tehnologi_jml_web_opd(array("th"=>$i))->row_array();
		}

		$data["th_st"] = $date-2;
		$data["th_fn"] = $date;		

		// print_r("<pre>");
		// print_r($data);

		$this->load->view("front_data/tehnologi/web_opd", $data);
	}
	    public function tehnologi($th_fn){
        $jenis = $this->lt->get_lp_jenis();
        $sub_jenis = $this->lt->get_lp_sub();

        $th_st = $th_fn-2;
        $row_jenis = 0;
        $data_graph = array();
        $str_tbl = "";

        $str_header = "     <tr>
                                <th width=\"10%\">No</th>
                                <th width=\"30%\">Keterangan</th>
                                    ";
        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header .= "<th width=\"20%\">".$i."</th>";
            $t_col++;
        }

        $str_header .= "    </tr>";
        $data_graph = array();

        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td>-</td>
                            <td colspan=\"".$t_col."\"><b>".$v_data->nama_jenis."</b></td>";
            $str_tbl .= "</tr>";

            $t_data = array();

            $r_kec = 0;
            foreach ($sub_jenis as $r_sub => $v_sub) {
                $data["list_data"][$row_jenis]["sub_jenis"][$r_kec]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                $data["list_data"][$row_jenis]["sub_jenis"][$r_kec]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_sub->id_sub_jenis."\">
                                <td><a href=\"#\">".($r_kec+1)."</a></td>
                                <td><a href=\"#\">".$v_sub->nama_sub_jenis."</a></td>";

                $row_th = 0;
                for ($i=$th_st; $i <= $th_fn ; $i++) {
                    $val_data = $this->lt->get_lp(array("id_sub_jenis"=> $v_sub->id_sub_jenis, "th"=>$i));
                    $data["list_data"][$row_jenis]["sub_jenis"][$r_kec]["value"][$i] = $val_data["jml"];

                    $str_tbl .= "<td>".$val_data["jml"]."</td>";

                    $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                    $data_graph[$v_sub->id_sub_jenis][$row_th] = $main_data;
                    $row_th++;
                }
                $str_tbl .= "</tr>";
                $r_kec++;
            }
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;
        $data["str_tbl"] = $str_tbl;
        $data["str_header"] = $str_header;
        $data["data_graph"] = json_encode($data_graph);
        // $data["str_series"] = $str_series;
        $data["sub_jenis"] = $sub_jenis;
        $this->load->view("front_data/tehnologi/tehnologi",$data);
    }


     public function get_aplikasi(){  
        $val_data = $this->lp->get_aplikasi();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Aplikasi yang Digunakan di Pemerintah Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"25%\">Aplikasi</td>
                                <td width=\"40%\">Keterangan</td>
                                <td width=\"40%\">Fungsi</td>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title."</td>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td>".$v_data->id_aplikasi."</td>
                            <td>".$v_data->aplikasi."</td>
                            <td>".$v_data->fungsi."</td>
                            <td>".$v_data->keterangan."</td>
                        </tr>";

        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");
         $this->load->view("front_data/tehnologi/get_aplikasi",$data);
    }

    public function get_domain(){  
        $val_data = $this->lp->get_domain();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Domain yang Digunakan di Pemerintah Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"40%\">Sub Domain</td>
                                <td width=\"40%\">Nama SKPD</td>
                                <td width=\"25%\">th</td>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title."</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td>".$v_data->id_aplikasi."</td>
                            <td>".$v_data->subdomain."</td>
                            <td>".$v_data->nama_skpd."</td>
                            <td>".$v_data->th."</td>
                        </tr>";

        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");

         $this->load->view("front_data/tehnologi/get_domain",$data);
    }
}

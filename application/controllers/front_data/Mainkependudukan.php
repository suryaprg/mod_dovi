<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainkependudukan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_kependudukan", "mk");
		$this->load->model("front_data/Main_miskin", "mm");
		$this->load->model('front_lp/Lp_miskin', 'lm');
		$this->load->model('front_lp/Lp_penduduk_jk', 'lpj');
        $this->load->model('front_lp/Lp_penduduk_umur', 'lpu');
        
		$this->load->library("response_message");
	}

	public function index(){
		$this->index_pem_manusia(date("Y"));
	
	}

	public function index_pem_manusia($th_fn){
		$th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Indeks Pembangunan Manusia ".($th_fn-2)." - ".$th_fn."( Persen)";

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>index Pembangunan Manusia</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;
        
        for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
            $val_data = $this->mk->get_index_pem_manusia(array("th"=>$i))->row_array();

            $data["list_data"][$i] = $val_data;

            $str_tbl .= "<tr>
                            <td>".$i."</td>
                            <td>".$val_data["ipm"]."%</td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;

        $data["data_graph"] = json_encode($data_graph);

		$this->load->view("front_data/kependudukan/ipm", $data);
	}

	public function index_kes_daya_pend($th_fn){
		  $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Perkembangan Indeks Kesehatan, Indeks Pendidikan, dan Indeks

Daya Beli Kota Malang Tahun ".($th_fn-2)." - ".$th_fn." (Persen)";

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <td>Tahun Data</td>";

            
        $str_header_mod .= "<th>Daya Beli</th>
                            <th>Kesehatan</th>
                            <th>Pendidikan</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;
        for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
            $val_data = $this->mk->get_index_kes_daya_pend(array("th"=>$i))->row_array();

            $str_tbl .= "<tr>
                            <td><a href=\"#\" class=\"jenis\" id=\"jenis_".$i."\">".$i."</a></td>
                            <td>".$val_data["daya_beli"]." %</td>
                            <td>".$val_data["kesehatan"]." %</td>
                            <td>".$val_data["pendidikan"]." %</td>
                        </tr>";

            // print_r($val_data);
            $data_graph[$row_data]["year"] = (string)$i;
            $data_graph[$row_data]["daya_beli"] = (int)$val_data["daya_beli"];
            $data_graph[$row_data]["kesehatan"] = (int)$val_data["kesehatan"];
            $data_graph[$row_data]["pendidikan"] =(int)$val_data["pendidikan"];

            $row_data++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;

        $data["data_graph"] = json_encode($data_graph);

           $this->load->view("front_data/kependudukan/daya_beli_penduduk", $data);

	}

	public function kepandudukan_jk($th_fn){
		$th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Jumlah Penduduk menurut Jenis Kelamin di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn." (Jiwa)";

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Laki-Laki</th>
                            <th>Perempuan</th>
                            <th>Jumlah Penduduk</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;

        for ($i=$th_fn-2; $i<=$th_fn ; $i++) {
            $val_data = $this->mk->get_kepandudukan_jk(array("th"=>$i))->row_array();

            // print_r($val_data);
            $data["list_data"][$i] = $val_data;

            $str_tbl .= "<tr>
                            <td>".$i."</td>
                            <td>".$val_data["t_cowo"]." jiwa</td>
                            <td>".$val_data["t_cewe"]." jiwa</td>
                            <td>".$val_data["rasio_jk"]." %</td>
                        </tr>";
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;

        $data["data_graph"] = json_encode($data_graph);
                
        // print_r("<pre>");
        // print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");

		$this->load->view("front_data/kependudukan/penduduk_jk", $data);
	}

	public function kependudukan_kel_umur($th_fn){
		$jenis = $this->mk->get_kependudukan_kel_umur_jenis()->result();
        $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Jumlah Penduduk menurut Kelompok Umur di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn." (Jiwa)";

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

        $series = "";
        foreach ($jenis as $key => $value) {
            $str_header_mod .= "<th>".$value->kelompok."</th>";
            $series .= "createSeries(\"val_".$key."\", \"".$value->kelompok."\");";     
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;

        for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
            $str_tbl .= "<tr>
                            <td>".$i."</td>";

            $data_graph[$row_data]["year"] = (string)$i;                            
            foreach ($jenis as $key => $value) {
                $data_umur =  $this->mk->get_kependudukan_kel_umur(array("periode"=>$i, "kelompok"=>$value->kelompok))->row_array();
                $data["list_data"][$i][$key] = $data_umur;

                $str_tbl .= "<td>".$data_umur["jml_penduduk"]." jiwa</td>";

                $data_graph[$row_data]["val_".$key] = (int)$data_umur["jml_penduduk"];
                
                // $no = $i;
            }

            $str_tbl .= "</tr>";
            $row_data++;
        }
        
        
        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["series"] = $series;
        $data["title"] = $str_title_data;

        $data["data_graph"] = json_encode($data_graph);
        // print_r("<pre>");

		$this->load->view("front_data/kependudukan/penduduk_umur", $data);
	}

	public function kependudukan_rasio_ketergantungan($th_fn){
	   $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Jumlah Penduduk menurut Jenis Kelamin di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn." (Persen)";

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Rasio ketergantungan</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;

        for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
            $val_data = $this->mk->get_kependudukan_rasio_ketergantungan(array("th_rasio"=>$i))->row_array();
            $data["list_data"][$i] = $val_data["rasio_keter"];
            
            $str_tbl .= "<tr>
                            <td>".$i."</td>
                            <td>".$val_data["rasio_keter"]." %</td>
                        </tr>";

            $data_graph[$row_data]["year"] = (string)$i;
            $data_graph[$row_data]["val_data"] = (double)$val_data["rasio_keter"];

            $row_data++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;

        $data["data_graph"] = json_encode($data_graph); 
        // print_r("<pre>");
        // print_r($data["data_graph"]);

		$this->load->view("front_data/kependudukan/rasio_ketergantungan", $data);
	}

	public function miskin_jml_pend($th_fn){
	   $th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Jumlah Penduduk Miskin di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn." (Jiwa)";

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Agregat</th>
                            <th>Prosentase</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;
        // $str_val = "";
        // $str_pro = "";
        for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 

            $agregat = 0;
            $prosentase = 0;

            $val_data = $this->mm->get_miskin_jml_pend(array("th"=>$i))->row_array();
            if($val_data){
                $agregat = $val_data["agregat"];
                $prosentase = $val_data["prosentase"];
            }

            $data["list_data"][$i] = $val_data;
            $str_tbl .= "<tr>
                            <td>".$i."</td>
                            <td>".$agregat."</td>
                            <td>".$prosentase."</td>
                        </tr>";

            $data_graph[$row_data]["year"] = (string)$i;
            $data_graph[$row_data]["agregat"] = (double)$agregat;
            $data_graph[$row_data]["prosentase"] = (double)$prosentase;

            $row_data++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;

        $data["data_graph"] = json_encode($data_graph);         
        // print_r("<pre>");
        // print_r("<table border='1'>".$data["str_header"].$data["str_tbl"]."</table>");

		$this->load->view("front_data/kependudukan/penduduk_miskin", $data);
	}

	public function miskin_pengeluaran_perkapita($th_fn){

		$th_st = $th_fn-2;
        $data_graph = array();

        $str_title_data = "Persentase Penduduk menurut Golongan Pengeluaran Perkapita di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn." (Jiwa)";

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Agregat</th>
                            <th>Prosentase</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

        $data["list_data"] = array();
        $row_data = 0;

        for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 

            $agregat = 0;
            $prosentase = 0;

            $val_data = $this->mm->get_miskin_pengeluaran_perkapita(array("th"=>$i))->row_array();
            if($val_data){
                $agregat = $val_data["pengeluaran_perkapita"];
                $prosentase = $val_data["garis_kemiskinan"];
            }
            
            $data["list_data"][$i] = $val_data;

            $str_tbl .= "<tr>
                            <td>".$i."</td>
                            <td>".number_format($agregat, 0, ".", ",")."</td>
                            <td>".number_format($prosentase, 0, ".", ",")."</td>
                        </tr>";
            $data_graph[$row_data]["year"] = (string)$i;
            $data_graph[$row_data]["agregat"] = (double)$agregat;
            $data_graph[$row_data]["prosentase"] = (double)$prosentase;

            $row_data++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["title"] = $str_title_data;

        $data["data_graph"] = json_encode($data_graph);     
        // print_r("<pre>");
        // print_r($data);

		$this->load->view("front_data/kependudukan/penduduk_pengeluaran", $data);
		
	}



	 public function miskin($th_fn){
        $jenis = $this->lm->get_lp_jenis();

        $th_st = $th_fn-2;
        $str_tbl = "";

        $data_graph = array();
        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;


            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";
            
            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lm->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;
                $str_tbl .= "<td>".$val_data["jml"]."</td>";

                $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["jml"]);
                $data_graph[$v_data->id_jenis][$row_th] = $main_data;
                $row_th++;
            }

            $str_tbl .= "</tr>";
            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);
        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border=\"1\">".$str_tbl."</table>");
        // print_r("<table>".$str_tbl."</table>");


        // print_r("<pre>");
        // print_r($data);
        // print_r("<table border=\"1\">".$str_tbl."</table>");
        // print_r("<table>".$str_tbl."</table>");

        $this->load->view("front_data/kependudukan/miskin",$data);

    }

      public function penduduk_jk($th_fn){
        $kecamatan = $this->lpj->get_kec();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td rowspan=\"2\">No</td>
                                <td rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"2\">".$i."</td>";
            $str_header_bot .= "<th>Laki-laki</th>
                                <th>Perempuan</th>";
            $t_col += 3;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Data Rasio Penduduk Berdasarkan Jenis Kelamin di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";


        $row_jenis = 0;
        foreach ($kecamatan as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_kec;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_kec;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_kec."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_kec."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpj->get_lp(array("id_kec"=>$v_data->id_kec, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_l"]){
                    $jml_l = $val_data["jml_l"];
                    $str_tbl .= "<td>".$val_data["jml_l"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                    $jml_l = 0;
                }

                if($val_data["jml_p"]){
                    $jml_p = $val_data["jml_p"];
                    $str_tbl .= "<td>".$val_data["jml_p"]."</td>";
                }else{
                    $jml_p = 0;
                    $str_tbl .= "<td>-</td>";
                }

                $main_data = array("year"=>(string)$i, "val_l"=>(float)$val_data["jml_l"], "val_p"=>(float)$val_data["jml_p"], "val_jml"=>(float)($jml_l+$jml_p));
                $data_graph[$v_data->id_kec][$row_th] = $main_data;
                $row_th++;
                // $str_tbl .= "<td>".($jml_l+$jml_p)."</td>";
            }

            $str_tbl .= "</tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);
        

        // print_r("<pre>");
        // print_r($data);

        $this->load->view("front_data/kependudukan/lp_penduduk_jk",$data);
        // $this->load->view("front_lp/pmks/pmks", $data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }

#===========================================================================================================================
#------------------------------------------------------------Penduduk JK----------------------------------------------------
#===========================================================================================================================

#===========================================================================================================================
#-----------------------------------------------------------Penduduk Rasio--------------------------------------------------
#===========================================================================================================================

    public function penduduk_rasio($th_fn){
        $kecamatan = $this->lpj->get_kec();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td rowspan=\"2\">No</td>
                                <td rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"2\">".$i."</td>";
            $str_header_bot .= "<th>Jumlah Penduduk</th>
                                <th>Rasio</th>";
            $t_col += 3;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Rasio Penduduk Berdasarkan Gender Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";


        $row_jenis = 0;
        foreach ($kecamatan as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_kec;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_kec;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_kec."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_kec."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpj->get_lp(array("id_kec"=>$v_data->id_kec, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_l"]){
                    $jml_l = $val_data["jml_l"];
                    // $str_tbl .= "<td>".$val_data["jml_l"]."</td>";
                }else{
                    // $str_tbl .= "<td>-</td>";
                    $jml_l = 0;
                }

                if($val_data["jml_p"]){
                    $jml_p = $val_data["jml_p"];
                    // $str_tbl .= "<td>".$val_data["jml_p"]."</td>";
                }else{
                    $jml_p = 0;
                    // $str_tbl .= "<td>-</td>";
                }

                $str_tbl .= "<td>".($jml_l+$jml_p)."</td>";
                $str_tbl .= "<td>".$val_data["rasio"]."</td>";

                $main_data = array("year"=>(string)$i, "val_data"=>(float)$val_data["rasio"]);
                $data_graph[$v_data->id_kec][$row_th] = $main_data;
                $row_th++;
            }

            $str_tbl .= "</tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;
        $data["data_graph"] = json_encode($data_graph);

        // print_r("<pre>");
        // print_r($data);

        $this->load->view("front_data/kependudukan/lp_penduduk_rasio", $data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
    }

#===========================================================================================================================
#-----------------------------------------------------------Penduduk Rasio--------------------------------------------------
#===========================================================================================================================


#===========================================================================================================================
#-----------------------------------------------------------Penduduk Umur--------------------------------------------------
#===========================================================================================================================

    public function penduduk_umur($th_fn){
        $jenis = $this->lpu->get_lp_jenis();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td rowspan=\"2\">No</td>
                                <td rowspan=\"2\">Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td colspan=\"3\">".$i."</td>";
            $str_header_bot .= "<th>Laki-laki</th>
                                <th>Perempuan</th>
                                <th>Rasio</th>";
            $t_col += 3;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Rasio Penduduk Berdasarkan Umur Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";

        $data_graph = array();
        $row_jenis = 0;
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_jenis;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_jenis;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lpu->get_lp(array("id_jenis"=>$v_data->id_jenis, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml_l"]){
                    $jml_l = $val_data["jml_l"];
                    $str_tbl .= "<td>".$val_data["jml_l"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                    $jml_l = 0;
                }

                if($val_data["jml_p"]){
                    $jml_p = $val_data["jml_p"];
                    $str_tbl .= "<td>".$val_data["jml_p"]."</td>";
                }else{
                    $jml_p = 0;
                    $str_tbl .= "<td>-</td>";
                }

                $str_tbl .= "<td>".($jml_l+$jml_p)."</td>";
                // $str_tbl .= "<td>".$val_data["rasio"]."</td>";
                $main_data = array("year"=>(string)$i, "val_l"=>(float)$val_data["jml_l"], "val_p"=>(float)$val_data["jml_p"], "val_jml"=>(float)($jml_l+$jml_p));
                $data_graph[$v_data->id_jenis][$row_th] = $main_data;
                $row_th++;
            }

            $str_tbl .= "</tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod.$str_header_bot;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);

        // print_r("<pre>");
        // print_r($data);

        // $this->load->view("front_lp/pmks/pmks", $data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
        $this->load->view("front_data/kependudukan/lp_penduduk_umur",$data);
    }


	// public function jenis_kelamin(){
	// 	$data['page'] = "jenis_kelamin";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pendudukAngkatanKerja/masterKependudukan',$data);
	
	// }

	// public function umur(){
	// 	$data['page'] = "umur";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pendudukAngkatanKerja/masterKependudukan',$data);
	// }


	// public function gender(){
	// 	$data['page'] = "gender";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pendudukAngkatanKerja/masterKependudukan',$data);
	// }

	// public function ketergantungan(){
	// 	$data['page'] = "ketergantungan";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pendudukAngkatanKerja/masterKependudukan',$data);
	// }

	// public function gini(){
	// 	$data['page'] = "gini";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pendudukAngkatanKerja/masterKependudukan',$data);
	// }

	// public function indeksPemManusia(){
	// 	$data['page'] = "indeksPemManusia";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pendudukAngkatanKerja/masterKependudukan',$data);
	// }


}

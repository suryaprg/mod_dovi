<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpemerintahan extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model("front_data/Main_pemerintahan", "mp");
		
		$this->load->library("response_message");
	}

	public function index(){
		$this->pem_jml_rt_rw(date("Y"));
	}

	public function pem_jml_rt_rw($date){
		$kecamatan = $this->mp->get_kecamatan();
		$data["kecamatan"] = $kecamatan;
		$data["list_data"] = array();
		for ($i=$date-2; $i<=$date ; $i++) {
			foreach ($kecamatan as $r_data => $v_data) {
				$data["list_data"][$i][$v_data->id_kec] = $this->mp->get_pem_jml_rt_rw(array("th"=>$i, "mk.id_kec"=>$v_data->id_kec))->row_array();
			}
			
		}

		$data["th_st"] = $date-2;
		$data["th_fn"] = $date;		

		// print_r("<pre>");
		// print_r($data);

		$this->load->view('front_data/pemerintahan/rt_rw',$data);		
	}
	// public function kepegawaian(){
	// 	$data['page'] = "kepegawaian";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	
	// }

	// public function realisasi_pendapatan(){
	// 	$data['page'] = "realisasi_pendapatan";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }


	// public function realisasi_belanja(){
	// 	$data['page'] = "realisasi_belanja";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function eksekutif(){
	// 	$data['page'] = "eksekutif";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function legislatif(){
	// 	$data['page'] = "legislatif";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function dinas(){
	// 	$data['page'] = "dinas";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function badanKantor(){
	// 	$data['page'] = "badanKantor";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function bagian(){
	// 	$data['page'] = "bagian";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function kecamatanKelurahan(){
	// 	$data['page'] = "kecamatanKelurahan";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

	// public function rtRw($th_mulai){
	// 	$data['page'] = "rtRw";
		
	// 	$data_kecamatan = $this->mk->get_kec();
	// 	$single_data = array();
	// 	foreach ($data_kecamatan as $r_data_kecamatan => $v_data_kecamatan) {
	// 		$data["tr_rw"][$r_data_kecamatan]["keceamatan"] = $v_data_kecamatan;
	// 		$data["tr_rw"][$r_data_kecamatan]["single_data"] = $this->atw->get_rt_rw_where(array("id_kec"=>$v_data_kecamatan->id_kec, "th"=>$th_mulai))->result();
	// 		$data["tr_rw"][$r_data_kecamatan]["multi_data"] = null; 
	// 	}

	// 	// print_r("<pre>");
	// 	// print_r($data);
	// 	$this->load->view('front_data/pemerintahan/masterPemerintahan',$data);
	// }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainketenagakerjaan extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_ketenagakerjaan", "mk");
		$this->load->model('front_lp/Lp_disnaker_all', 'lp');

		$this->load->library("response_message");
	}

	public function index(){
		$this->kerja_angkatan(date("Y"));
	}

	public function kerja_angkatan($th_fn){
		$th_st = $th_fn-2;
		$data_graph = array();

		$str_title_data = "Jumlah Angkatan Kerja di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn." (Jiwa)";

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Jumlah Yang Bekerja</th>
        					<th>Jumlah Yang Tidak Bekerja</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		$data["list_data"] = array();
		for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
			$val_data = $this->mk->get_kerja_angkatan(array("th"=>$i))->row_array();
			$data["list_data"][$i] = $val_data;

			$str_tbl .= "<tr>
							<td>".$i."</td>";

			$data_graph[$row_data]["year"] = (string)$i;
			
			if($val_data){
				$str_tbl .= "<td>".number_format($val_data["jml_kerja"], 0, ".", ",")." Jiwa</td>
                            <td>".number_format($val_data["jml_no_kerja"], 0, ".", ",")." Jiwa</td>";

	            $data_graph[$row_data]["jml_kerja"] = (double)$val_data["jml_kerja"];
	            $data_graph[$row_data]["jml_no_kerja"] = (double)$val_data["jml_no_kerja"];
			}else{
				$str_tbl .= "<td>0</td>
                            <td>0</td>";
                $data_graph[$row_data]["jml_kerja"] = 0;
	            $data_graph[$row_data]["jml_no_kerja"] = 0;
			}

            $str_tbl .= "</tr>";

            $row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;

		$data["data_graph"] = json_encode($data_graph);	

		// print_r("<pre>");
		// print_r($data);

		$this->load->view("front_data/ketenagakerjaan/ketenagakerjaan_angkatan", $data);
	}

	public function kerja_pengangguran($th_fn){
		$th_st = $th_fn-2;
		$data_graph = array();

		$str_title_data = "Tingkat Pengangguran di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn." (Jiwa)";

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Jumlah Yang Bekerja</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;
		for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
			$val_data = $this->mk->get_kerja_pengangguran(array("th"=>$i))->row_array();
			$data["list_data"][$i] = $val_data;

			$str_tbl .= "<tr>
							<td>".$i."</td>";

			$data_graph[$row_data]["year"] = (string)$i;
			
			if($val_data){
				$str_tbl .= "<td>".number_format($val_data["jml"], 0, ".", ",")." Jiwa</td>";

	            $data_graph[$row_data]["jml"] = (double)$val_data["jml"];
	            
			}else{
				$str_tbl .= "<td>0</td>";

                $data_graph[$row_data]["jml"] = 0;
			}

            $str_tbl .= "</tr>";

            $row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;

		$data["data_graph"] = json_encode($data_graph);	

		// print_r("<pre>");
		// print_r($data);

		$this->load->view("front_data/ketenagakerjaan/ketenagakerjaan_pengangguran", $data);
	}

	public function kerja_ump($th_fn){
		$th_st = $th_fn-2;
		$data_graph = array();

		$str_title_data = "Upah Minimum di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn." (Rupiah)";

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                            <th>Tahun Data</th>";

            
        $str_header_mod .= "<th>Jumlah Yang Bekerja</th>";
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"4\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;
		for ($i=$th_fn-2; $i<=$th_fn ; $i++) { 
			$val_data = $this->mk->get_kerja_ump(array("th"=>$i))->row_array();
			$data["list_data"][$i] = $val_data;

			$str_tbl .= "<tr>
							<td>".$i."</td>";

			$data_graph[$row_data]["year"] = (string)$i;
			
			if($val_data){
				$str_tbl .= "<td>Rp. ".number_format($val_data["jml"], 0, ".", ",")."</td>";

	            $data_graph[$row_data]["jml"] = (double)$val_data["jml"];
	            
			}else{
				$str_tbl .= "<td>0</td>";

                $data_graph[$row_data]["jml"] = 0;
			}

            $str_tbl .= "</tr>";

            $row_data++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;

		$data["data_graph"] = json_encode($data_graph);	

		// print_r("<pre>");
		// print_r($data);

		$this->load->view("front_data/ketenagakerjaan/ketenagakerjaan_ump", $data);
	}

	    public function disnaker_all($id_kategori, $th_fn){
        $kategori_filter = $this->lp->get_lp_kategori_filter(array("id_kategori"=>$id_kategori));
        $kategori_all = $this->lp->get_lp_kategori();

        //------------------------------------------------header-------------------------------------------------

        $str_title = "Data Disnaker di Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"45%\">Keterangan</td>";
        $str_header_mod_sec ="";

        $t_col = 2;
        for ($i=$th_fn-1; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"25%\">".$i."</td>";
            // $str_header_mod_sec .= "<td width=\"10%\">Negeri</td>
            //                         <td width=\"10%\">Swasta</td>
            //                         <td width=\"10%\">Jumlah Sekolah</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Data</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-1;
        $data_graph = array();
        $str_tbl = "";

        $row_kategori = 0;

        $str_dropdown_kategori = "";

        foreach ($kategori_filter as $r_kate => $v_kate) {
            // $str_tbl = "";

            $data["list_data"][$v_kate->id_kategori]["kategori"]["main"]["id_kategori"] = $v_kate->id_kategori;
            $data["list_data"][$v_kate->id_kategori]["kategori"]["main"]["nama_kategori"] = $v_kate->nama_kategori;

            $str_tbl .= "<table border=\"1\" class=\"data-table\" style=\"width: 100%;\">
                         <thead>
                          <tr>
                            <td colspan=\"2\">Data ".$v_kate->nama_kategori." di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Kategori</td>
                          </tr>
                         </thead>";

            // $data_graph[$v_kate->id_kategori]["main"]

            $jenis = $this->lp->get_lp_jenis(array("id_kategori"=>$v_kate->id_kategori));

            $row_jenis = 0;
            foreach ($jenis as $r_data => $v_data) {
                $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["main"]["id_jenis"] = $v_data->id_jenis;
                $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["main"]["nama_jenis"] = $v_data->nama_jenis;

                $str_tbl .=    "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                                <td><a href=\"#\">".($row_jenis+1)."</a></td>
                                <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                            </tr>
                            <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                                <td>&nbsp;</td>
                                <td>
                                     <table border=\"1\" class=\"data-table\" style=\"width: 100%;\">".
                                     $str_header_mod;     


                $r_kec = 0;

                $array_jml = array();
                $sub = $this->lp->get_lp_sub_jenis(array("id_jenis"=>$v_data->id_jenis));

                if($sub){
                    foreach ($sub as $r_sub => $v_sub) {
                        $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["sub_jenis"][$r_kec]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                        $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["sub_jenis"][$r_kec]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis;
                        
                        $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_sub_jenis."\">
                                        <td><a href=\"#\">".($r_kec+1)."</a></td>
                                        <td><a href=\"#\">".$v_sub->nama_sub_jenis."</a></td>";


                        $row_th = 0;
                        for ($i=$th_st; $i <= $th_fn ; $i++) {
                            $val_data = $this->lp->get_lp(array(
                                                            "id_jenis"=>$v_data->id_jenis,
                                                            "id_sub_jenis"=>$v_sub->id_sub_jenis,
                                                            "th"=>$i));
                            $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["sub_jenis"][$r_kec]["value"][$i][0] = $val_data["jml"];
                            $str_tbl .= "<td>".($val_data["jml"])."</td>";

                            $data_graph[$v_kate->id_kategori][$v_data->id_jenis][$v_sub->id_sub_jenis][$row_th]["year"] = (int)$i;
                            $data_graph[$v_kate->id_kategori][$v_data->id_jenis][$v_sub->id_sub_jenis][$row_th]["val_data"] = (int)$val_data["jml"];

                            $row_th++;

                        }
                        
                        $r_kec++;
                        $str_tbl .= "</tr>";
                    }
                }else{
                    $r_kec = 0;
                    $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["sub_jenis"][$r_kec]["main"]["id_sub_jenis"]   = $v_data->id_jenis;
                    $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["sub_jenis"][$r_kec]["main"]["nama_sub_jenis"] = $v_data->nama_jenis;
                        
                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_0\">
                                        <td><a href=\"#\">".($r_kec+1)."</a></td>
                                        <td><a href=\"#\">".$v_data->nama_jenis."</a></td>";

                     $row_th = 0;
                        for ($i=$th_st; $i <= $th_fn ; $i++) {
                            $val_data = $this->lp->get_lp(array(
                                                            "id_jenis"=>$v_data->id_jenis,
                                                            "id_sub_jenis"=>0,
                                                            "th"=>$i));
                            $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["sub_jenis"][$r_kec]["value"][$i][0] = $val_data["jml"];
                            $str_tbl .= "<td>".($val_data["jml"])."</td>";

                            $data_graph[$v_kate->id_kategori][$v_data->id_jenis][0][$row_th]["year"] = (int)$i;
                            $data_graph[$v_kate->id_kategori][$v_data->id_jenis][0][$row_th]["val_data"] = (int)$val_data["jml"];

                            $row_th++;

                        }
                        
                        $r_kec++;
                        $str_tbl .= "</tr>";
                }
                
                $row_jenis++;

                $str_tbl .= "           
                                    </table>
                                </td>
                            </tr>";
                $row_jenis++;
            }

            $str_tbl .= "</table>";

            $data["str_tbl"][$v_kate->id_kategori] = (string)$str_tbl;

            $row_kategori++;   
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        $data["move_tbl"] = json_encode($data["str_tbl"]);
    //$data["str_series"] = $str_series;
        //$data["kategori"] = $kategori;
        $data["str_header"] = $str_header_top;
        $data["title"] = $str_title;
       $data["id_kategori"] = $id_kategori;


        foreach ($kategori_all as $r_kate_all => $v_kate_all) {
            $str_dropdown_kategori .= "<option id=\"val_".$v_kate_all->id_kategori."\" value=\"".$v_kate_all->id_kategori."\">".$v_kate_all->nama_kategori."</option>";    
        }
         $data["dropdown_kategori"] = "<select name=\"get_kate\" id=\"get_kate\" class=\"form-control select2\" style=\"width: 100%;\">".$str_dropdown_kategori."</select>";


       // print_r("<pre>");
        // print_r($data["data_graph"]);
       // print_r("<table border='1'>".$str_tbl."</table>");

        // $this->load->view("front_lp/Pendidikan/lp_pendidikan_all",$data);
        $this->load->view("front_data/ketenagakerjaan/disnaker", $data);
    }

}

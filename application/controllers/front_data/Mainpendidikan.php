/<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpendidikan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("front_data/Main_pendidikan", "mp");
			$this->load->model('front_lp/Lp_perpustakaan', 'lp');
			$this->load->model('front_lp/Lp_pendidikan_all', 'lpa');
		$this->load->library("response_message");
	}

	public function index(){
		$this->pendidikan_baca_tulis(date("Y"));
	}

	public function pendidikan_baca_tulis($th_fn){
		$data['page'] = "pendidikan_baca_tulis";

	$data["kategori"] = "";

		$th_st = $th_fn-2;
		$data_graph = array();

		$str_title_data = "Persentase Penduduk Usia 15 tahun ke Atas menurut Kemampuan

Baca Tulis di Kota Malang Tahun ".($th_st)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<td>Tahun Data</td>";

       
        $str_header_mod .= "<td width=\"40%\">Huruf Latin dan  atau Lainnya</td>
        					<td width=\"40%\">Buta Huruf</td>";
       	$colspan=3;
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$colspan."\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		$row_th = 0;
		for ($i=$th_st; $i<=$th_fn ; $i++) {
			$for_data = $this->mp->get_pendidikan_baca_tulis(array("th"=>$i))->result();

			$data_graph[$row_th]["year"] = (string)$i;

			$str_tbl .= "<tr>
							<td>".$i."</td>";
			$row_data = 0;
			$series = "";
			foreach ($for_data as $key => $value) {
				$data["list_data"][$i][$row_data] = $value;
				$str_tbl .= "<td>".$value->prosentase."</td>";

				$data_graph[$row_th]["val_".$row_data] = (string)$value->prosentase;

				$series .= "createSeries(\"val_".$row_data."\", \"".$value->nama_jenis."\");";
				$row_data++;
			}

			$str_tbl .= "</tr>";
			$row_th++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["series"] = $series;

		$data["data_graph"] = json_encode($data_graph);		

		// print_r("<pre>");
		// print_r($data);
		// print_r($data["data_graph"]);
		// print_r("<table>".$data["str_header"].$str_tbl."</table>");

		// print_r("<pre>");
		// print_r($data);
		$this->load->view('front_data/pendidikan/masterPendidikan',$data);
	}

	public function pendidikan_partisipasi_sklh($th_fn){
			$data['page'] = "pendidikan_partisipasi_sklh";
		$data["kategori"] = "";

		$th_st = $th_fn-2;
		$data_graph = array();

		$str_title_data = "Angka Partisipasi Sekolah (APS) Penduduk di Kota Malang Tahun ".($th_st)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<td>Tahun Data</td>";

       
        $str_header_mod .= "<td width=\"25%\">APS SD (7-12 tahun)</td>
        					<td width=\"25%\">APS SMP (13-15 tahun)</td>
        					<td width=\"25%\">APS SMA (16-18 tahun)</td>";
       	$colspan=4;
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$colspan."\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		$series = "";
		$row_th = 0;

		for ($i=$th_st; $i<=$th_fn ; $i++) {
			$for_data = $this->mp->get_pendidikan_partisipasi_sklh(array("th"=>$i))->result();

			$data_graph[$row_th]["year"] = (string)$i;
			$str_tbl .= "<tr>
							<td>".$i."</td>";
			$row_data = 0;
			$series = "";
			foreach ($for_data as $key => $value) {
				$data["list_data"][$i][$row_data] = $value;
				$str_tbl .= "<td>".$value->prosentase."</td>";

				$data_graph[$row_th]["val_".$row_data] = (string)$value->prosentase;

				$series .= "createSeries(\"val_".$row_data."\", \"".$value->nama_jenis."\");";
				
				$row_data++;
			}

			$str_tbl .= "</tr>";
			$row_th++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["series"] = $series;

		$data["data_graph"] = json_encode($data_graph);		

		// print_r("<pre>");
		// print_r($data["data_graph"]);
		// print_r("<table>".$data["str_header"].$str_tbl."</table>");

		$this->load->view('front_data/pendidikan/masterPendidikan',$data);
		// print_r("<pre>");
		// print_r($data);
	}

	public function pendidikan_penduduk($th_fn){
			$data['page'] = "pendidikan_penduduk";
			$th_st = $th_fn-2;
		$data_graph = array();

		$str_title_data = "Persentase Penduduk Usia 15 Tahun ke Atas menurut Pendidikan
Tertinggi yang Ditamatkan di Kota Malang Tahun ".($th_st)." - ".$th_fn;

		//------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
        					<td>Tahun Data</td>";

       
        $str_header_mod .= "<td width=\"17%\">Tidak Punya Ijazah SD</td>
        					<td width=\"17%\">SD / Sederajat</td>
        					<td width=\"17%\">SMP / Sederajat</td>
        					<td width=\"17%\">SMA / Sederajat</td>
        					<td width=\"17%\">PT / Universitas</td>";
       	$colspan=6;
        
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$colspan."\">".$str_title_data."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";

		$data["list_data"] = array();
		$row_data = 0;

		$series = "";
		$row_th = 0;

		for ($i=$th_st; $i<=$th_fn ; $i++) {
			$for_data = $this->mp->get_pendidikan_penduduk(array("th"=>$i))->result();

			$str_tbl .= "<tr>
							<td>".$i."</td>";

			$data_graph[$row_th]["year"] = (string)$i;
			$row_data = 0;
			$series = "";
			foreach ($for_data as $key => $value) {
				$data["list_data"][$i][$row_data] = $value;
				$str_tbl .= "<td>".$value->prosentase."</td>";

				$data_graph[$row_th]["val_".$row_data] = (string)$value->prosentase;

				$series .= "createSeries(\"val_".$row_data."\", \"".$value->nama_jenis."\");";

				$row_data++;
			}

			$str_tbl .= "</tr>";

			$row_th++;
		}

		$data["th_st"] = $th_st;
		$data["th_fn"] = $th_fn;		

		$data["str_header"] = $str_header_top.$str_header_mod;
		$data["str_tbl"] = $str_tbl;

		$data["title"] = $str_title_data;
		$data["series"] = $series;

		$data["data_graph"] = json_encode($data_graph);		

		// print_r("<pre>");
		// print_r($data);
		// print_r("<table>".$data["str_header"].$str_tbl."</table>");

		// $this->load->view("front_data/transportasi/jalan", $data);

$this->load->view('front_data/pendidikan/masterPendidikan',$data);
}
  public function perpustakaan($th_fn){
        $data['page'] = "perpustakaan";
        $jenis = $this->lp->get_lp_perpustakaan_jenis();
        $data["list_data_graph"] = array();

        $no = 0;

        $str_series = "";
        foreach ($jenis as $r_data => $v_data) {
            $data["list_data"][$r_data]["jenis"] = $v_data;
            // print_r($v_data);
            
            $no_row = 0;
            for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
                $data_main = $this->lp->get_lp_perpustakaan(array("lp.id_jenis"=>$v_data->id_jenis, "th"=>$i));

                // $tmp_array = array("year"=>$i, "value".$no_row=>0);
                if(!isset($data["list_data_graph"][$no_row]["year"])){
                    $data["list_data_graph"][$no_row]["year"] = "$i";
                }

                $data["list_data_graph"][$no_row]["value".$no] = (int)$data_main["jml"];

                $data["list_data"][$r_data]["value"][$i] = $data_main["jml"];

                // $data["list_data_graph"][$no_row] = "0";
                $no_row++;
            }
            // print_r($data_main);

            $str_series .= "
                        var series".($no+1)." = chart.series.push(new am4charts.LineSeries());
                        series".($no+1).".dataFields.valueY = \"value".$no."\";
                        series".($no+1).".dataFields.categoryX = \"year\";
                        series".($no+1).".name = \"".$v_data->nama_jenis."\";
                        series".($no+1).".strokeWidth = 3;
                        series".($no+1).".bullets.push(new am4charts.CircleBullet());
                        series".($no+1).".tooltipText = \"{name} pada {categoryX}: {valueY}\";
                        series".($no+1).".legendSettings.valueText = \"{valueY}\";
                        series".($no+1).".visible  = false;";

            $no++;
        }

        $data["str_series"] = $str_series;

        // print_r("<pre>");
        // print_r($data);
        // print_r($str_series);
        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;

        $this->load->view("front_data/pendidikan/masterPendidikan",$data);

    }


    public function pendidikan_all($id_kategori,$th_fn){
    	$data['page'] = "pendidikan_all";
           $jenis = $this->lpa->get_lp_jenis();
        $kategori = $this->lpa->get_lp_kategori_filter(array("id_kategori"=>$id_kategori));
        $kecamatan = $this->lpa->get_kec();

        $kategori_all = $this->lpa->get_lp_kategori();

        //------------------------------------------------header-------------------------------------------------

        $str_title = "Data Pendidikan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn;

        $str_header_mod = "<tr>
                                <td width=\"5%\" rowspan=\"2\">No</td>
                                <td width=\"35%\" rowspan=\"2\">Keterangan</td>";
        $str_header_mod_sec ="";

        $t_col = 2;
        for ($i=$th_fn-1; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td  colspan=\"3\">".$i."</td>";
            $str_header_mod_sec .= "<td width=\"10%\">Negeri</td>
                                    <td width=\"10%\">Swasta</td>
                                    <td width=\"10%\">Jumlah Sekolah</td>";
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"2\">".$str_title."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Kategori</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------


        $th_st = $th_fn-1;
        $data_graph = array();
        $str_tbl = "";

        $row_kategori = 0;

        $str_dropdown_kategori = "";

        foreach ($kategori as $r_kate => $v_kate) {
            $str_tbl = "";

            $data["list_data"][$v_kate->id_kategori]["kategori"]["main"]["id_kategori"] = $v_kate->id_kategori;
            $data["list_data"][$v_kate->id_kategori]["kategori"]["main"]["nama_kategori"] = $v_kate->nama_kategori;

            $str_tbl .= "<table class=\"data-table\" style=\"width: 100%;\">
                         <thead>
                          <tr>
                            <td colspan=\"2\">Data ".$v_kate->nama_kategori." di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Kategori</td>
                          </tr>
                         </thead>";

            // $data_graph[$v_kate->id_kategori]["main"]

            $row_jenis = 0;
            foreach ($jenis as $r_data => $v_data) {
                $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["main"]["id_jenis"] = $v_data->id_jenis;
                $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["main"]["nama_jenis"] = $v_data->nama_jenis;

                $str_tbl .=    "<tr class=\"jenis\" id=\"jenis_".$v_data->id_jenis."\">
                                <td><a href=\"#\">".($row_jenis+1)."</a></td>
                                <td><a href=\"#\">".$v_data->nama_jenis."</a></td>
                            </tr>
                            <tr class=\"out_jenis\" id=\"out_jenis_".$v_data->id_jenis."\">
                                <td>&nbsp;</td>
                                <td>
                                     <table class=\"data-table\" style=\"width: 100%;\">".
                                     $str_header_mod.$str_header_mod_sec;     


                $r_kec = 0;

                $array_jml = array();
                foreach ($kecamatan as $r_sub => $v_sub) {
                    $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["kecamatan"][$r_kec]["main"]["id_kecamatan"] = $v_sub->id_kec;
                    $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["kecamatan"][$r_kec]["main"]["nama_kecamatan"] = $v_sub->nama_kec;
                    
                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_".$v_sub->id_kec."\">
                                    <td><a href=\"#\">".($r_kec+1)."</a></td>
                                    <td><a href=\"#\">".$v_sub->nama_kec."</a></td>";


                    $row_th = 0;
                    for ($i=$th_st; $i <= $th_fn ; $i++) {
                        $val_data = $this->lpa->get_lp(array("id_kategori"=>$v_kate->id_kategori,
                                                        "id_jenis"=>$v_data->id_jenis,
                                                        "id_kec"=>$v_sub->id_kec,
                                                        "th"=>$i));
                        // $array_val = array("jml_panen"=> $val_data["jml_panen"], "luas_panen"=>$val_data["luas_panen"]);
                        $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["kecamatan"][$r_kec]["value"][$i][0] = $val_data["jml_negeri"];
                        $data["list_data"][$v_kate->id_kategori]["kategori"]["jenis"][$v_data->id_jenis]["kecamatan"][$r_kec]["value"][$i][1] = $val_data["jml_swasta"];

                        $jml_negri = 0;
                        if($val_data["jml_negeri"]){
                            $str_tbl .= "<td>".$val_data["jml_negeri"]."</td>";
                            $jml_negri = $val_data["jml_negeri"];
                        }else{
                            $str_tbl .= "<td>-</td>";
                            $jml_negri = 0;
                        }

                        $jml_swasta = 0;
                        if($val_data["jml_swasta"]){
                            $str_tbl .= "<td>".$val_data["jml_swasta"]."</td>";
                            $jml_swasta = $val_data["jml_swasta"];
                        }else{
                            $str_tbl .= "<td>-</td>";
                            $jml_swasta = 0;
                        }

                        $str_tbl .= "<td>".($jml_swasta+$jml_negri)."</td>";

                        $data_graph[$v_kate->id_kategori][$v_data->id_jenis][$v_sub->id_kec][$row_th]["year"] = (int)$i;
                        $data_graph[$v_kate->id_kategori][$v_data->id_jenis][$v_sub->id_kec][$row_th]["jml_negeri"] = (int)$val_data["jml_negeri"];
                        $data_graph[$v_kate->id_kategori][$v_data->id_jenis][$v_sub->id_kec][$row_th]["jml_swasta"] = (int)$val_data["jml_swasta"];
                        $data_graph[$v_kate->id_kategori][$v_data->id_jenis][$v_sub->id_kec][$row_th]["jml_all"] = (int)$jml_swasta+$jml_negri;

                        if(isset($array_jml[$i]["jml_swasta"])){
                            $array_jml[$i]["jml_swasta"] += $jml_swasta;
                        }else{
                            $array_jml[$i]["jml_swasta"] = $jml_swasta;
                        }

                        if(isset($array_jml[$i]["jml_negri"])){
                            $array_jml[$i]["jml_negri"] += $jml_negri;
                        }else{
                            $array_jml[$i]["jml_negri"] = $jml_negri;
                        }

                        if(isset($array_jml[$i]["jml_all"])){
                            $array_jml[$i]["jml_all"] += ($jml_swasta+$jml_negri);
                        }else{
                            $array_jml[$i]["jml_all"] = ($jml_swasta+$jml_negri);
                        }

                        $row_th++;

                    }
                    
                    $r_kec++;
                    $str_tbl .= "</tr>";
                }

                $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_data->id_jenis."_00\">
                                    <td><a href=\"#\">-</a></td>
                                    <td><a href=\"#\">Jumlah</a></td>";

                $row_jml = 0;
                foreach ($array_jml as $r_jml => $v_jml) {
                    $str_tbl .= "<td>".$v_jml["jml_swasta"]."</td>
                                <td>".$v_jml["jml_negri"]."</td>
                                <td>".$v_jml["jml_all"]."</td>";

                    $data_graph[$v_kate->id_kategori][$v_data->id_jenis]["00"][$row_jml]["year"] = (int)$r_jml;
                    $data_graph[$v_kate->id_kategori][$v_data->id_jenis]["00"][$row_jml]["jml_negeri"] = (int)$v_jml["jml_negri"];
                    $data_graph[$v_kate->id_kategori][$v_data->id_jenis]["00"][$row_jml]["jml_swasta"] = (int)$v_jml["jml_swasta"];
                    $data_graph[$v_kate->id_kategori][$v_data->id_jenis]["00"][$row_jml]["jml_all"] = (int)$v_jml["jml_all"];

                    $row_jml++;
                }

                $str_tbl .= "</tr>";

                $row_jenis++;

                $str_tbl .= "           
                                    </table>
                                </td>
                            </tr>";
                $row_jenis++;
            }

            $str_tbl .= "</table>";

            $data["str_tbl"][$v_kate->id_kategori] = (string)$str_tbl;

            $row_kategori++;   
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["data_graph"] = json_encode($data_graph);
        $data["move_tbl"] = json_encode($data["str_tbl"]);
        // $data["str_series"] = $str_series;
        $data["kategori"] = $kategori;
        $data["str_header"] = $str_header_top;
        $data["title"] = $str_title;
        $data["id_kategori"] = $id_kategori;


        foreach ($kategori_all as $r_kate_all => $v_kate_all) {
          // <div class="form-group">
          //       <label>Minimal</label>
          //       <select class="form-control select2" style="width: 100%;">
            
          //       </select>
          //     </div>
            $str_dropdown_kategori .= "<option id=\"val_".$v_kate_all->id_kategori."\" value=\"".$v_kate_all->id_kategori."\">".$v_kate_all->nama_kategori."</option>";    
        }
        
        $data["dropdown_kategori"] = "<select name=\"get_kate\" id=\"get_kate\" class=\"form-control select2\" style=\"width: 100%;\">".$str_dropdown_kategori."</select>";

        // print_r("<pre>");
        // print_r($data["data_graph"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>")
        

        $this->load->view("front_data/pendidikan/masterPendidikan",$data);
        // print_r("<pre>");
        // print_r($data["str_tbl"]);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

        // $this->load->view("front_lp/pdb/lp_pdb",$data);
    }
		// print_r("<pre>");
		// print_r($data);

      public function data_pokok(){
        $data['page'] = "data_pokok";
         $this->load->view("front_data/pendidikan/masterPendidikan",$data);
    }
   
	// public function partisipasiSekolah(){
	// 	$data['page'] = "Mainpendidikan/partisipasiSekolah";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('pendidikan/masterPendidikan',$data);
	
	// }
	// public function jumlahLulusan(){
	// 	$data['page'] = "Mainpendidikan/jumlahLulusan";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('pendidikan/masterPendidikan',$data);
	
	// }

	// public function guruMurid(){
	// 	$data['page'] = "Mainpendidikan/guruMurid";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('pendidikan/masterPendidikan',$data);
	
	// }

	// public function pendidikanFormal(){
	// 	$data['page'] = "Mainpendidikan/pendidikanFormal";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('pendidikan/masterPendidikan',$data);
	
	// }

	// public function perpustakaan(){
	// 	$data['page'] = "Mainpendidikan/perpustakaan";
	// 	// $data['admin'] = $this->m_pkl->tampil_data2()->result();
	// 	$this->load->view('pendidikan/masterPendidikan',$data);
	
	// }
}

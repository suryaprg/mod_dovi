<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maingeo extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("admin_super/Main_kecamatan", "mk");
		$this->load->model("admin_opd/Admin_iklim", "ai");
		$this->load->model('front_lp/Lp_tpa', 'lp');
		$this->load->model('front_lp/Lp_bencana', 'lpb');

        $this->load->model('front_lp/Lp_disperkim', 'lpk');
        $this->load->model('front_lp/Lp_disperkim_pohon', 'lpp');
        $this->load->model('front_lp/Lp_disperkim_taman', 'lpt');

        $this->load->model('front_lp/Lp_disperkim_all', 'lpa');
	}
	
	public function index(){
		$data['page'] = "geografi_wilayah";

		$data["kecamatan"] = array();
		$kecamatan = $this->mk->get_kec();
		foreach ($kecamatan as $r_kec => $v_kec) {
			$data["kecamatan"][$r_kec]["val_kec"] = $v_kec;
			$data["kecamatan"][$r_kec]["count_kel"] = count($this->mk->get_kel_all_where(array("mk.id_kec"=>$v_kec->id_kec)));
		}

		print_r("<pre>");
		print_r($data);
		// $this->load->view('geo/masterGeografi',$data);
	
	}

	public function geografi_wilayah(){
		$data['page'] = "geografi_wilayah";
		
		$data["kecamatan"] = array();
		$kecamatan = $this->mk->get_kec();
		
		foreach ($kecamatan as $r_kec => $v_kec) {
			$data["kecamatan"][$r_kec]["val_kec"] = $v_kec;
			$data["kecamatan"][$r_kec]["count_kel"] = count($this->mk->get_kel_all_where(array("mk.id_kec"=>$v_kec->id_kec)));
		}

		// print_r("<pre>");
		// print_r($data);

		$this->load->view('front_data/geo/masterGeografi',$data);
	
	}

	public function wilayahAdmin(){
		$data['page'] = "wilayah_admin";

		$data["kecamatan"] = array();
		$kecamatan = $this->mk->get_kec();
		foreach ($kecamatan as $r_kec => $v_kec) {
			$data["kecamatan"][$r_kec]["val_kec"] = $v_kec;
			$data["kecamatan"][$r_kec]["val_kel"] = $this->mk->get_kel_all_where(array("mk.id_kec"=>$v_kec->id_kec));
		}

		// print_r("<pre>");
		// print_r($data);

		$this->load->view('front_data/geo/masterGeografi',$data);
	}


	public function iklim($th_mulai){
		$data['page'] = "iklim";
		$data_station = $this->ai->get_st();

		$data["th"] = $th_mulai;
		$data["iklim"] = array();
		$data["st"] = $data_station;
		foreach ($data_station as $r_data_station => $v_data_station) {
			$data_cura = $this->ai->get_iklim_all_where(array("id_st"=>$v_data_station->id_st, "th"=>$th_mulai));
			$data_periode = array();
			// print_r($data_cura);
			foreach ($data_cura as $r_data_cura => $v_data_cura) {
				$data_periode[$v_data_cura->periode]["cura"] = $v_data_cura->jml_cura;
				$data_periode[$v_data_cura->periode]["hari"] = $v_data_cura->jml_hari;
			}
			$data["iklim"][$r_data_station]["nama_st"] = $v_data_station->ket_st;
			$data["iklim"][$r_data_station]["cura_hujan"] = $data_periode;
				
		}
        $data["th_st"] = $th_mulai-2;
        $data["th_mulai"] = $th_mulai;
		// print_r("<pre>");
		// print_r($data);
		$this->load->view('front_data/geo/masterGeografi',$data);
	}

	public function tpa($th_fn){
		$data['page'] = "tpa";
        $kec = $this->lp->get_lp_kec();

        $row_kec = 0;

        $data_graph = array();
        $series = "";
        foreach ($kec as $r_data => $v_data) {
            $data["list_data"][$row_kec]["main"]["kecamatan"] = $v_data->nama_kec;
            $data["list_data"][$row_kec]["main"]["id_kec"] = $v_data->id_kec;
            
            $row_th = 0;
            for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 

                $val_data = $this->lp->get_lp(array("lpt.id_kec"=>$v_data->id_kec, "th"=>$i));

                if(!isset($data_graph[$row_th]["year"])){
                    $data_graph[$row_th]["year"] = "$i";
                }

                if(!isset($data_graph[$row_th]["jml"])){
                    $data_graph[$row_th]["jml"] = (int)$val_data["jml"];
                }else {
                    $data_graph[$row_th]["jml"] += (int)$val_data["jml"];
                }

                $data_graph[$row_th][$v_data->id_kec] =  (int)$val_data["jml"];

                
                $data["list_data"][$row_kec]["value"][$i] = $val_data["jml"];

                $row_th++;
            }

            $series .= "createSeries(\"".$v_data->id_kec."\", \"".$v_data->nama_kec."\");";

            $row_kec++;
        }
        $series .= "createSeries(\"jml\", \"Total TPA Kota Malang\");";

        $data["graph_data"] = json_encode($data_graph);
        $data["series_data"] = $series;

        $data["th_st"] = $th_fn-2;
        $data["th_fn"] = $th_fn;


        // print_r("<pre>");
        // print_r($data);
        // print_r($data_graph);


        $this->load->view("front_data/geo/masterGeografi",$data);

    }

        public function bencana($th_fn){
        	$data['page'] = "bencana";
        $kecamatan = $this->lpb->get_lp_kecamatan();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------
        $str_header_mod = "<tr>
                                <td>No</td>
                                <td>Keterangan</td>";

        $str_header_bot = "<tr>";

        $t_col = 2;
        for ($i=$th_fn-2; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td>".$i."</td>";
            $str_header_bot .= "<th>Laki-laki</th>
                                <th>Perempuan</th>";
            $t_col += 1;
        }

        $str_header_bot .= "</tr>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">Jumlah Bencana Tanah Longsor menurut Kecamatan di Kota Malang Tahun ".($th_fn-2)." - ".$th_fn."</td>
                        </tr>";
        //------------------------------------------------header-------------------------------------------------
        $str_tbl = "";


        $row_jenis = 0;
        foreach ($kecamatan as $r_data => $v_data) {
            $data["list_data"][$row_jenis]["jenis"]["id_jenis"] = $v_data->id_kec;
            $data["list_data"][$row_jenis]["jenis"]["nama_jenis"] = $v_data->nama_kec;

            $str_tbl .= "<tr class=\"jenis\" id=\"jenis_".$v_data->id_kec."\">
                            <td><a href=\"#\">".($row_jenis+1)."</a></td>
                            <td><a href=\"#\">".$v_data->nama_kec."</a></td>";

            $row_th = 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) {
                $val_data = $this->lp->get_lp(array("lpt.id_kec"=>$v_data->id_kec, "th"=>$i));
                $data["list_data"][$row_jenis]["value"][$i] = $val_data;

                if($val_data["jml"]){
                    $jml = $val_data["jml"];
                    $str_tbl .= "<td>".$val_data["jml"]."</td>";
                }else{
                    $str_tbl .= "<td>-</td>";
                    $jml = 0;
                }


                $main_data = array("year"=>(string)$i, "val_data"=>(float) $jml);
                $data_graph[$v_data->id_kec][$row_th] = $main_data;
                $row_th++;
                // $str_tbl .= "<td>".($jml_l+$jml_p)."</td>";
            }

            $str_tbl .= "</tr>";

            $row_jenis++;
        }

        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["str_tbl"] = $str_tbl;

        $data["data_graph"] = json_encode($data_graph);
        

        // print_r("<pre>");
        // print_r($data);

              $this->load->view("front_data/geo/masterGeografi",$data);
        // $this->load->view("front_lp/pmks/pmks", $data);
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

    }



	  public function get_dsu(){
        $data['page'] = "psu";
        $val_data = $this->lpk->get_dsu();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data PSU yang di Serahkan Kepada Pemerintah Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"23%\">Lokasi</td>
                                <td width=\"23%\">Kawasan</td>
                                <td width=\"23%\">Pengembang</td>
                                <td width=\"23%\">Tanggal</td>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"5\">".$str_title."</td>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td>".($row_data++)."</td>
                            <td>".$v_data->lokasi."</td>
                            <td>".$v_data->nama_kawasan."</td>
                            <td>".$v_data->pengembang."</td>
                            <td>".$v_data->tgl."</td>
                        </tr>";

        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;

        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");

         $this->load->view("front_data/geo/masterGeografi",$data);
    }

    public function get_csr(){
        $data['page'] = "csr";
        $val_data = $this->lpk->get_csr();

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Pemberi CSR di Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"19%\">Objek Pembangunan</td>
                                <td width=\"19%\">Pemberi CSR</td>
                                <td width=\"19%\">RAB</td>
                                  <td width=\"19%\">Tanggal dan No. Bash</td>
                                <td width=\"19%\">Tanggal dan No. NPHD</td>";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"6\">".$str_title."</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";
        foreach ($val_data as $r_data => $v_data) {
            $str_tbl .= "<tr>
                            <td>".($row_data+1)."</td>
                        
                            <td>".$v_data->obj_pembangunan."</td>
                            <td>".$v_data->pemberi_csr."</td>
                            <td>".$v_data->rab."</td>
                                <td>tgl : ".$v_data->tgl_bash." <br><br> No.Bash: ".$v_data->no_bash."</td>
                            <td>tgl : ".$v_data->tgl_nphd." <br><br> No.NPHD: ".$v_data->no_nphd."</td>
                        </tr>";

        }

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;

        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
         $this->load->view("front_data/geo/masterGeografi",$data);
    }

  public function get_pohon($th_fn){
    $data['page'] = "pohon";
        $val_data = $this->lpp->get_lp($th_fn);

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Penanaman Pohon di Kota Malang";

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                              
                                <td width=\"19%\">Jenis Tanaman</td>
                                <td width=\"19%\">Jumlah Bibit</td>
                                <td width=\"19%\">Lokasi</td>
                                  <td width=\"19%\">Tanggal</td>
                                <td width=\"19%\">Keterangan</td>
                                ";
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"6\">".$str_title."</td>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";

        $data_graph = array();

       
            $row_data = 0;
            foreach ($val_data as $r_data => $v_data) {

                 $str_tbl .= "<tr>
                                <td>".($row_data+1)."</td>
                              
                                <td>".$v_data->nama_jenis."</td>
                                <td>".$v_data->jml."</td>
                                <td>".$v_data->lokasi."</td>
                                  <td>".$v_data->tgl."</td>
                                <td>".$v_data->keterangan."</td>
                            </tr>";
                $row_data++;
            }

            

        $data["graph_data"] = json_encode($data_graph);

        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;
            $data["th_fn"] = $th_fn;   
        // print_r("<pre>");
        // print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
        $this->load->view("front_data/geo/masterGeografi",$data);
    }

    public function get_taman($th_fn){
        // chart using Stacked Bar Chart
        $data['page'] = "taman";
        $jenis = $this->lpt->get_lp_jenis();

        $th_st = $th_fn-2;

        //------------------------------------------------header-------------------------------------------------
        $str_title = "Data Pemanfaatan Taman dan Hutan Kota dan Pedestrian di Kota Malang Tahun ".$th_st." - ".$th_fn;

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"35%\">Keterangan</td>";

        $t_col = 2;
        for ($i=$th_st; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"20%\">".$i."</td>";
            $t_col += 1;
        }
        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"".$t_col."\">".$str_title."</td>
                          </tr>
                          ";
        //------------------------------------------------header-------------------------------------------------

        $row_data = 0;

        $str_tbl = "";

        $data_graph = array();
        $row_jenis = 0;

        $series = "";
        foreach ($jenis as $r_jenis => $v_jenis) {
            $data["list_data"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
            $data["list_data"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;  

            $str_tbl .= " <tr>
                                <td>".($row_jenis+1)."</td>
                                <td>".$v_jenis->nama_jenis."</td>"; 
            

            $row_th= 0;
            for ($i=$th_st; $i <= $th_fn ; $i++) { 
                $val_data = $this->lpt->get_lp(array("lp.id_jenis"=>$v_jenis->id_jenis));
                $data["list_data"][$row_jenis]["detail"][$i] = $val_data["jml"];

                $data_graph[$row_th]["year"] = $i;
                $data_graph[$row_th]["val_".$v_jenis->id_jenis] =$val_data["jml"];

                $str_tbl .= "<td>".$val_data["jml"]."</td>";
                $row_th++;
            }
            $row_jenis++;

            $str_tbl .= "</tr>";

            $series .= "createSeries(\"val_".$v_jenis->id_jenis."\", \"".$v_jenis->nama_jenis."\");";
        }


        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["graph_data"] = json_encode($data_graph);
        $data["series_data"] = $series;
        $data["str_header"] = $str_header_top.$str_header_mod;
        $data["title"] = $str_title;
        $data["str_tbl"] = $str_tbl;

        // print_r("<pre>");
        // print_r($data["data_graph"]);
       //print_r("<table border='1'>".$data["str_header"].$str_tbl."</table>");
         $this->load->view("front_data/geo/masterGeografi",$data);
    }
    
    public function get_disperkim_all($id_kategori, $th_fn){
        $data["page"] = "disperkim";
       
        $kategori = $this->lpa->get_lp_kategori_filter(array("id_kategori"=>$id_kategori));
        $th_st = $th_fn-1;

        //------------------------------------------------header-------------------------------------------------

        $str_title = "Data Disperkim di Kota Malang Tahun ".$th_st." - ".$th_fn;

        $str_header_mod = "<tr>
                                <td width=\"5%\">No</td>
                                <td width=\"45%\">Keterangan</td>";
        

        $t_col = 2;
        for ($i=$th_fn-1; $i <= $th_fn ; $i++) { 
            $str_header_mod .= "<td width=\"25%\">".$i."</td>";
            
            $t_col += 1;
        }

        $str_header_mod .= "</tr>";

        $str_header_top = "<tr>
                            <td colspan=\"2\">".$str_title."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>";
        //------------------------------------------------header-------------------------------------------------

        $str_tbl = "";
        $row_kate = 0;
        $list_data = array();

        foreach ($kategori as $r_kate => $v_kate) {
            $data["list_data"][$row_kate]["main"]["id_kategori"] = $v_kate->id_kategori;
            $data["list_data"][$row_kate]["main"]["nama_kategori"] = $v_kate->nama_kategori;  

            $str_tbl .= "<table border='1' class=\"data-table\" style=\"width: 100%;\">
                         <thead>
                          <tr>
                            <td colspan=\"2\">Data ".$v_kate->nama_kategori." di Kota Malang Tahun ".$th_st." - ".$th_fn."</td>
                          </tr>
                          <tr>
                                <td width=\"5%\">No. </td>
                                <td>Jenis Lahan</td>
                          </tr>
                         </thead>";          

            $jenis = $this->lpa->get_lp_jenis(array("id_kategori"=>$v_kate->id_kategori));
            $row_jenis = 0;
            foreach ($jenis as $r_jenis => $v_jenis) {
                $data["list_data"][$row_kate]["jenis"][$row_jenis]["main"]["id_jenis"] = $v_jenis->id_jenis;
                $data["list_data"][$row_kate]["jenis"][$row_jenis]["main"]["nama_jenis"] = $v_jenis->nama_jenis;

                $str_tbl .=    "<tr class=\"jenis\" id=\"jenis_".$v_jenis->id_jenis."\">
                                    <td><a href=\"#\">".($row_jenis+1)."</a></td>
                                    <td><a href=\"#\">".$v_jenis->nama_jenis."</a></td>
                                </tr>
                                <tr class=\"out_jenis\" id=\"out_jenis_".$v_jenis->id_jenis."\">
                                    <td>&nbsp;</td>
                                    <td>
                                         <table class=\"data-table\" style=\"width: 100%;\">".
                                         $str_header_mod; 
                
                $sub_jenis = $this->lpa->get_lp_sub_jenis(array("id_jenis"=>$v_jenis->id_jenis));

                if($sub_jenis){
                    $row_sub = 0;
                    foreach ($sub_jenis as $r_sub => $v_sub) {
                        $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][$row_sub]["main"]["id_sub_jenis"] = $v_sub->id_sub_jenis;
                        $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][$row_sub]["main"]["nama_sub_jenis"] = $v_sub->nama_sub_jenis; 

                        $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_jenis->id_jenis."_".$v_sub->id_sub_jenis."\">
                                        <td><a href=\"#\">".($row_sub+1)."</a></td>
                                        <td><a href=\"#\">".$v_sub->nama_sub_jenis."</a></td>";

                        $row_th = 0;
                        for ($i=$th_st; $i <= $th_fn; $i++) { 
                            $val_data = $this->lpa->get_lp(array("id_jenis"=>$v_jenis->id_jenis, "id_sub_jenis"=>$v_sub->id_sub_jenis, "th"=>$i));
                            $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][$row_sub]["detail"][$row_th] = $val_data;

                            $data_graph[$v_jenis->id_jenis][$v_sub->id_sub_jenis][$row_th]["year"] = (int) $i;
                            $data_graph[$v_jenis->id_jenis][$v_sub->id_sub_jenis][$row_th]["val"] = (int) $val_data;
                            
                            // $str_tbl .= "<td>".$v_jenis->id_jenis."-".$v_sub->id_sub_jenis."-".$i."</td>";
                            $str_tbl .= "<td>".$val_data["jml"]."</td>";
                            $row_th++;
                        }

                        $str_tbl .= "</tr>";
                        $row_sub++;
                    }
                }else{

                    $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][0]["main"]["id_sub_jenis"] = $v_jenis->id_jenis;
                    $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][0]["main"]["nama_sub_jenis"] = $v_jenis->nama_jenis; 

                    $str_tbl .= "<tr class=\"sub\" id=\"sub_".$v_jenis->id_jenis."_0\">
                                        <td><a href=\"#\">1</a></td>
                                        <td><a href=\"#\">".$v_jenis->nama_jenis."</a></td>";

                    $row_th = 0;
                    for ($i=$th_st; $i <= $th_fn; $i++) { 
                        $val_data = $this->lpa->get_lp(array("id_jenis"=>$v_jenis->id_jenis, "id_sub_jenis"=>0, "th"=>$i));

                        $data_graph[$v_jenis->id_jenis]["0"][$row_th]["year"] = (int) $i;
                        $data_graph[$v_jenis->id_jenis]["0"][$row_th]["val"] = (int) $val_data;
                        // $data["list_data"][$row_kate]["jenis"][$row_jenis]["sub_jenis"][0]["detail"][$row_th] = $val_data;
                        $str_tbl .= "<td>".$val_data["jml"]."</td>";
                        $row_th++;
                    }

                    $str_tbl .= "</tr>";
                }
                
                $str_tbl .= "           
                                    </table>
                                </td>
                            </tr>";
                $row_jenis++;
            }
            $str_tbl .= "</table>";
            $row_kate++;
        }

        $kategori_all = $this->lpa->get_lp_kategori();
        $str_dropdown_kategori = "";
        foreach ($kategori_all as $r_kate_all => $v_kate_all) {
            $str_dropdown_kategori .= "<option id=\"val_".$v_kate_all->id_kategori."\" value=\"".$v_kate_all->id_kategori."\">".$v_kate_all->nama_kategori."</option>";    
        }
        // $data["dropdown_kategori"] = "<select name=\"get_kate\" id=\"get_kate\">".$str_dropdown_kategori."</select>";
          $data["dropdown_kategori"] = "<select name=\"get_kate\" id=\"get_kate\" class=\"form-control select2\" style=\"width: 100%;\">".$str_dropdown_kategori."</select>";
        $data["str_header"] = $str_header_top;
        $data["data_graph"] = json_encode($data_graph);
        $data["th_st"] = $th_st;
        $data["th_fn"] = $th_fn;        

        $data["id_kategori"] = $id_kategori;
        $data["str_tbl"] = $str_tbl;

        $data_dis = $this->lpa->get_lp(array("id_jenis"=> 1, "id_sub_jenis"=>1));

       // print_r("<table border='1'>".$str_tbl."</table>");
        // print_r($data["data_graph"]);
        $this->load->view("front_data/geo/masterGeografi",$data);
        
    }

        // $this->load->view("front_data/geo/masterGeografi",$data);


      public function kedungkandang(){
        $data['page'] = "kedungkandang";
         $this->load->view("front_data/geo/masterGeografi",$data);
    }

    public function sukun(){
        $data['page'] = "sukun";
         $this->load->view("front_data/geo/masterGeografi",$data);
    }

    public function blimbing(){
        $data['page'] = "blimbing";
         $this->load->view("front_data/geo/masterGeografi",$data);
    }

    public function klojen(){
        $data['page'] = "klojen";
         $this->load->view("front_data/geo/masterGeografi",$data);
    }

    public function lowokwaru(){
        $data['page'] = "lowokwaru";
         $this->load->view("front_data/geo/masterGeografi",$data);
    }

       public function peta_bencana(){
        $data['page'] = "peta_bencana";
         $this->load->view("front_data/geo/masterGeografi",$data);
    }
   
}

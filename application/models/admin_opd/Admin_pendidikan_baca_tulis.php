<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pendidikan_baca_tulis extends CI_Model {

#---------------------------------------------------------------Admin_pendidikan_baca_tulis---------------------------------------------------------------------

	public function get(){
		$this->db->join("pendidikan_baca_tulis_jenis pbtj", "pbt.id_jenis = pbtj.id_jenis");
		$data = $this->db->get("pendidikan_baca_tulis pbt")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pendidikan_baca_tulis",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pendidikan_baca_tulis", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pendidikan_baca_tulis", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pendidikan_baca_tulis", $where);
		return $delete;
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_ekonomi_kontribusi extends CI_Model {

#---------------------------------------------------------------Admin_ekonomi_kontribusi---------------------------------------------------------------------

	public function get(){
		$this->db->join("pendapatan_lap_usaha_jenis pluj", "plu.id_jenis = pluj.id_jenis");
		$data = $this->db->get("pendapatan_lap_usaha plu")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pendapatan_lap_usaha",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pendapatan_lap_usaha", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pendapatan_lap_usaha", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pendapatan_lap_usaha", $where);
		return $delete;
	}

}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_pendidikan extends CI_Model
{

//=============================================== LP PENDIDIKAN ================================================//
	public function get_pendidikan(){
		$this->db->join('lp_pendidikan_jenis lpj', 'lp.id_jenis = lpj.id_jenis');
		$this->db->join('lp_pendidikan_kategori lpk', 'lp.id_kategori = lpk.id_kategori');
		$this->db->join('main_kecamatan mk', 'lp.id_kec = mk.id_kec');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("lp_pendidikan lp")->result();//kurang faham
		return $data;
	}

	public function get_pendidikan_where($where){
		$data = $this->db->get_where("lp_pendidikan",$where)->row_array();
		return $data;
	}	

	public function insert_pendidikan($data){
		$insert = $this->db->insert("lp_pendidikan", $data);
		return $insert;
	}

	public function update_pendidikan($set, $where){
		$update = $this->db->update("lp_pendidikan", $set, $where);
		return $update;
	}		

	public function delete_pendidikan($where){
		$delete = $this->db->delete("lp_pendidikan", $where);
		return $delete;
	}
//=============================================== LP PENDIDIKAN ================================================//				
//=============================================== LP PENDIDIKAN JENIS ==========================================//
	public function get_pendidikan_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_pendidikan_jenis")->result();
		return $data;
	}	

	public function insert_pendidikan_jenis($data){
		$insert = $this->db->insert("lp_pendidikan_jenis", $data);
		return $insert;
	}	

	public function get_pendidikan_jenis_where($where){
		$data = $this->db->get_where("lp_pendidikan_jenis",$where)->row_array();
		return $data;
	}

	public function update_pendidikan_jenis($set, $where){
		$update = $this->db->update("lp_pendidikan_jenis", $set, $where);
		return $update;
	}		

	public function delete_pendidikan_jenis($where){
		$delete = $this->db->delete("lp_pendidikan_jenis", $where);
		return $delete;
	}		
//=============================================== LP PENDIDIKAN JENIS ==========================================//

//=============================================== LP PENDIDIKAN KATEGORI =======================================//
	public function get_pendidikan_kategori(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_pendidikan_kategori")->result();
		return $data;
	}	

	public function insert_pendidikan_kategori($data){
		$insert = $this->db->insert("lp_pendidikan_kategori", $data);
		return $insert;
	}	

	public function get_pendidikan_kategori_where($where){
		$data = $this->db->get_where("lp_pendidikan_kategori",$where)->row_array();
		return $data;
	}

	public function update_pendidikan_kategori($set, $where){
		$update = $this->db->update("lp_pendidikan_kategori", $set, $where);
		return $update;
	}		

	public function delete_pendidikan_kategori($where){
		$delete = $this->db->delete("lp_pendidikan_kategori", $where);
		return $delete;
	}		
//=============================================== LP PENDIDIKAN KATEGORI =======================================//

//=============================================== MAIN KECAMATAN ===============================================//
	public function get_kecamatan(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("main_kecamatan")->result();
		return $data;
	}	

	public function insert_kecamatan($data){
		$insert = $this->db->insert("main_kecamatan", $data);
		return $insert;
	}	

	public function get_kecamatan_where($where){
		$data = $this->db->get_where("main_kecamatan",$where)->row_array();
		return $data;
	}

	public function update_kecamatan($set, $where){
		$update = $this->db->update("main_kecamatan", $set, $where);
		return $update;
	}		

	public function delete_kecamatan($where){
		$delete = $this->db->delete("main_kecamatan", $where);
		return $delete;
	}		
//=============================================== MAIN KECAMATAN ===============================================//	
}
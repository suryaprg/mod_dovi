<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_iklim extends CI_Model {

#---------------------------------------------------------------stasiun---------------------------------------------------------------------

	public function get_st(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("iklim_station")->result();
		return $data;
	}

	public function get_st_where($where){
		$data = $this->db->get_where("iklim_station",$where)->row_array();
		return $data;
	}

	public function get_st_all_where($where){
		$data = $this->db->get_where("iklim_station",$where)->result();
		return $data;
	}

	public function insert_st($data){
		$insert = $this->db->insert("iklim_station", $data);
		return $insert;
	}

	public function update_st($set, $where){
		$update = $this->db->update("iklim_station", $set, $where);
		return $update;
	}

	public function delete_st($where){
		$delete = $this->db->delete("iklim_station", $where);
		return $delete;
	}

#---------------------------------------------------------------Iklim---------------------------------------------------------------------

	public function get_iklim(){
		$this->db->join("iklim_station is", "ih.id_st=is.id_st");
		$data = $this->db->get("iklim_hujan ih")->result();
		return $data;
	}

	public function get_iklim_where($where){
		$data = $this->db->get_where("iklim_hujan",$where)->row_array();
		return $data;
	}

	public function get_iklim_all_where($where){
		$this->db->order_by("periode", "asc");
		$data = $this->db->get_where("iklim_hujan",$where)->result();
		return $data;
	}

	public function insert_iklim($data){
		$insert = $this->db->insert("iklim_hujan", $data);
		return $insert;
	}

	public function update_iklim($set, $where){
		$update = $this->db->update("iklim_hujan", $set, $where);
		return $update;
	}

	public function delete_iklim($where){
		$delete = $this->db->delete("iklim_hujan", $where);
		return $delete;
	}

}

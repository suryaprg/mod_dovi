<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_disperkim_all_jenis extends CI_Model {

#---------------------------------------------------------------Admin_gizi---------------------------------------------------------------------


	public function get(){
		$this->db->join('disperkim_all_kategori dak', 'daj.id_kategori = dak.id_kategori');
		$data = $this->db->get("disperkim_all_jenis daj")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("disperkim_all_jenis",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("disperkim_all_jenis", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("disperkim_all_jenis", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("disperkim_all_jenis", $where);
		return $delete;
	}

}

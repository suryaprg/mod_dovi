<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_kependudukan_ipm_bidang extends CI_Model {

#---------------------------------------------------------------kependudukan_jk---------------------------------------------------------------------

	public function get_kependudukan(){
		$data = $this->db->get("index_kes_daya_pend")->result();
		return $data;
	}

	public function get_kependudukan_where($where){
		$data = $this->db->get_where("index_kes_daya_pend",$where);
		return $data;
	}

	public function insert_kependudukan($data){
		$insert = $this->db->insert("index_kes_daya_pend", $data);
		return $insert;
	}

	public function update_kependudukan($set, $where){
		$update = $this->db->update("index_kes_daya_pend", $set, $where);
		return $update;
	}

	public function delete_kependudukan($where){
		$delete = $this->db->delete("index_kes_daya_pend", $where);
		return $delete;
	}

}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_terminal extends CI_Model
{

//====================================================== LP Terminal =======================================================//
	public function get_terminal(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_terminal_jenis kj', 'kt.id_jenis = kj.id_jenis');
		$this->db->join('lp_terminal_sub_jenis ksj', 'kt.id_sub_jenis = ksj.id_sub_jenis');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("Lp_terminal kt")->result();
		return $data;
	}

	public function get_terminal_where($where){
		$data = $this->db->get_where("lp_terminal",$where)->row_array();
		return $data;
	}	

	public function insert_terminal($data){
		$insert = $this->db->insert("lp_terminal", $data);
		return $insert;
	}

	public function update_terminal($set, $where){
		$update = $this->db->update("lp_terminal", $set, $where);
		return $update;
	}		

	public function delete_terminal($where){
		$delete = $this->db->delete("lp_terminal", $where);
		return $delete;
	}
//====================================================== LP Terminal =======================================================//				
//====================================================== LP Terminal Jenis =====================================================//
	public function get_terminal_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_terminal_jenis ")->result();
		return $data;
	}	

	public function insert_terminal_jenis($data){
		$insert = $this->db->insert("lp_terminal_jenis", $data);
		return $insert;
	}	

	public function get_terminal_jenis_where($where){
		$data = $this->db->get_where("lp_terminal_jenis",$where)->row_array();
		return $data;
	}

	public function update_terminal_jenis($set, $where){
		$update = $this->db->update("lp_terminal_jenis", $set, $where);
		return $update;
	}		

	public function delete_terminal_jenis($where){
		$delete = $this->db->delete("lp_terminal_jenis", $where);
		return $delete;
	}		
//====================================================== LP Terminal Jenis =====================================================//	

//====================================================== LP Terminal Sub Jenis =====================================================//
	public function get_terminal_sub_jenis(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_terminal_jenis tj','kts.id_jenis = tj.id_jenis');
		$data = $this->db->get("lp_terminal_sub_jenis kts ")->result();
		return $data;
	}	

	public function insert_terminal_sub_jenis($data){
		$insert = $this->db->insert("lp_terminal_sub_jenis", $data);
		return $insert;
	}	

	public function get_terminal_sub_jenis_where($where){
		$data = $this->db->get_where("lp_terminal_sub_jenis",$where)->row_array();
		return $data;
	}

	public function update_terminal_sub_jenis($set, $where){
		$update = $this->db->update("lp_terminal_sub_jenis", $set, $where);
		return $update;
	}		

	public function delete_terminal_sub_jenis($where){
		$delete = $this->db->delete("lp_terminal_sub_jenis", $where);
		return $delete;
	}		
//====================================================== LP Terminal Sub Jenis =====================================================//		
}

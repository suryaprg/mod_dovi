<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_perdagangan_jenis extends CI_Model {

#---------------------------------------------------------------Admin_pendidikan_baca_tulis_jenis---------------------------------------------------------------------

	public function get(){
		
		$data = $this->db->get("dinas_perdagangan_jenis dpj")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("dinas_perdagangan_jenis",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("dinas_perdagangan_jenis", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("dinas_perdagangan_jenis", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("dinas_perdagangan_jenis", $where);
		return $delete;
	}

}

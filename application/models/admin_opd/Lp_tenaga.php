<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_tenaga extends CI_Model
{

//==================================================== LP TENAGA ====================================================//
	public function get_tenaga(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_tenaga_jenis ltj', 'lt.id_jenis = ltj.id_jenis');
		$this->db->join('lp_tenaga_kategori ltk', 'lt.id_kategori = ltk.id_kategori');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("lp_tenaga lt ")->result();
		return $data;
	}

	public function get_tenaga_where($where){
		$data = $this->db->get_where("lp_tenaga",$where)->row_array();
		return $data;
	}	

	public function insert_tenaga($data){
		$insert = $this->db->insert("lp_tenaga", $data);
		return $insert;
	}

	public function update_tenaga($set, $where){
		$update = $this->db->update("lp_tenaga", $set, $where);
		return $update;
	}		

	public function delete_tenaga($where){
		$delete = $this->db->delete("lp_tenaga", $where);
		return $delete;
	}
//==================================================== LP TENAGA =====================================================//				
//================================================== LP TENAGA JENIS ==================================================//
	public function get_tenaga_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_tenaga_jenis")->result();
		return $data;
	}	

	public function insert_tenaga_jenis($data){
		$insert = $this->db->insert("lp_tenaga_jenis", $data);
		return $insert;
	}	

	public function get_tenaga_jenis_where($where){
		$data = $this->db->get_where("lp_tenaga_jenis",$where)->row_array();
		return $data;
	}

	public function update_tenaga_jenis($set, $where){
		$update = $this->db->update("lp_tenaga_jenis", $set, $where);
		return $update;
	}		

	public function delete_tenaga_jenis($where){
		$delete = $this->db->delete("lp_tenaga_jenis", $where);
		return $delete;
	}		
//================================================== LP TENAGA JENIS ==================================================//
//================================================== LP TENAGA KATEGORI ==================================================//
	public function get_tenaga_kategori(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_tenaga_kategori")->result();
		return $data;
	}	

	public function insert_tenaga_kategori($data){
		$insert = $this->db->insert("lp_tenaga_kategori", $data);
		return $insert;
	}	

	public function get_tenaga_kategori_where($where){
		$data = $this->db->get_where("lp_tenaga_kategori",$where)->row_array();
		return $data;
	}

	public function update_tenaga_kategori($set, $where){
		$update = $this->db->update("lp_tenaga_kategori", $set, $where);
		return $update;
	}		

	public function delete_tenaga_kategori($where){
		$delete = $this->db->delete("lp_tenaga_kategori", $where);
		return $delete;
	}
//================================================== LP TENAGA KATEGORI ==================================================//			
}
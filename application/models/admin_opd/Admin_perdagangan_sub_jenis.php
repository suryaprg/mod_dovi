<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_perdagangan_sub_jenis extends CI_Model {

#---------------------------------------------------------------Admin_pendidikan_jml_sklh_jenis---------------------------------------------------------------------
	public function get(){
		$this->db->join("dinas_perdagangan_jenis dpj", "dpj.id_jenis = dpsj.id_jenis");
		$data = $this->db->get("dinas_perdagangan_sub_jenis dpsj")->result();
		return $data;
	}
	public function get_sub(){
		$this->db->join("dinas_perdagangan_jenis dpj", "dpj.id_jenis = dpsj.id_jenis");
		$data = $this->db->get("dinas_perdagangan_sub_jenis dpsj")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("dinas_perdagangan_sub_jenis",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("dinas_perdagangan_sub_jenis", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("dinas_perdagangan_sub_jenis", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("dinas_perdagangan_sub_jenis", $where);
		return $delete;
	}

}

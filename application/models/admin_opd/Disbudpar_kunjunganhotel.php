<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class disbudpar_kunjunganhotel extends CI_Model
{

//=================disbudpar_kunjunganhotel=======================================================//
	public function get_disbudpar_kunjungan_hotel_kate(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("disbudpar_kunjungan_hotel_kate")->result();
		return $data;
	}

	public function get_disbudpar_kunjungan_hotel_kate_where($where){
		$data = $this->db->get_where("disbudpar_kunjungan_hotel_kate",$where)->row_array();
		return $data;
	}	
	public function insert_disbudpar_kunjungan_hotel_kate($data){
		$insert = $this->db->insert("disbudpar_kunjungan_hotel_kate", $data);
		return $insert;
	}

	public function update_disbudpar_kunjungan_hotel_kate($set, $where){
		$update = $this->db->update("disbudpar_kunjungan_hotel_kate", $set, $where);
		return $update;
	}		

	public function delete_disbudpar_kunjungan_hotel_kate($where){
		$delete = $this->db->delete("disbudpar_kunjungan_hotel_kate", $where);
		return $delete;
	}


//===================================== disbudpar_kunjungan_hotel_jenis =====================================//		
	public function get_disbudpar_kunjungan_hotel_jenis(){
		// $this->db->where("is_delete !=", "1");

		$this->db->join('disbudpar_kunjungan_hotel_kate hk', 'hj.id_kategori = hj.id_kategori');
		$data = $this->db->get("disbudpar_kunjungan_hotel_jenis hj")->result();
		return $data;
	}	

	public function insert_disbudpar_kunjungan_hotel_jenis($data){
		$insert = $this->db->insert("disbudpar_kunjungan_hotel_jenis", $data);
		return $insert;
	}	

	public function get_disbudparkunjunganhoteljenis_where($where){
		$data = $this->db->get_where("disbudpar_kunjungan_hotel_jenis",$where)->row_array();
		return $data;
	}

	public function update_disbudpar_kunjungan_hotel_jenis($set, $where){
		$update = $this->db->update("disbudpar_kunjungan_hotel_jenis", $set, $where);
		return $update;
	}		

	public function delete_disbudpar_kunjungan_hotel_jenis($where){
		$delete = $this->db->delete("disbudpar_kunjungan_hotel_jenis", $where);
		return $delete;
	}		
//======================================disbudpar_kunjungan_hotel_main==========================================//	
public function get_disbudpar_kunjungan_hotel_main(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('disbudpar_kunjungan_hotel_jenis khj','khm.id_jenis = khj.id_jenis');
		$data = $this->db->get("disbudpar_kunjungan_hotel_main khm")->result();
		return $data;
	}	

	public function insert_disbudpar_kunjungan_hotel_main($data){
		$insert = $this->db->insert("disbudpar_kunjungan_hotel_main", $data);
		return $insert;
	}	

	public function get_disbudpar_kunjungan_hotel_main_where($where){
		$data = $this->db->get_where("disbudpar_kunjungan_hotel_main",$where)->row_array();
		return $data;
	}

	public function update_disbudpar_kunjungan_hotel_main($set, $where){
		$update = $this->db->update("disbudpar_kunjungan_hotel_main", $set, $where);
		return $update;
	}		

	public function delete_disbudpar_kunjungan_hotel_main($where){
		$delete = $this->db->delete("disbudpar_kunjungan_hotel_main", $where);
		return $delete;
	}

}
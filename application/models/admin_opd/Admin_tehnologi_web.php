<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_tehnologi_web extends CI_Model {

#---------------------------------------------------------------Admin_tehnologi_web---------------------------------------------------------------------

	public function get_tehnologi(){
		$data = $this->db->get("tehnologi_jml_web_opd")->result();
		return $data;
	}

	public function get_tehnologi_where($where){
		$data = $this->db->get_where("tehnologi_jml_web_opd",$where);
		return $data;
	}

	public function insert_tehnologi($data){
		$insert = $this->db->insert("tehnologi_jml_web_opd", $data);
		return $insert;
	}

	public function update_tehnologi($set, $where){
		$update = $this->db->update("tehnologi_jml_web_opd", $set, $where);
		return $update;
	}

	public function delete_tehnologi($where){
		$delete = $this->db->delete("tehnologi_jml_web_opd", $where);
		return $delete;
	}

}

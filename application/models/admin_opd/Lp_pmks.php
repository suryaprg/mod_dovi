<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_pmks extends CI_Model
{

//====================================================== LP PMKS =======================================================//
	public function get_pmks(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_pmks_jenis lj','ll.id_jenis = lj.id_jenis');
		$data = $this->db->get("lp_pmks ll")->result();
		return $data;
	}

	public function get_pmks_where($where){
		$data = $this->db->get_where("lp_pmks",$where)->row_array();
		return $data;
	}	

	public function insert_pmks($data){
		$insert = $this->db->insert("lp_pmks", $data);
		return $insert;
	}

	public function update_pmks($set, $where){
		$update = $this->db->update("lp_pmks", $set, $where);
		return $update;
	}		

	public function delete_pmks($where){
		$delete = $this->db->delete("lp_pmks", $where);
		return $delete;
	}
				
//====================================================== LP PMKS JENIS ================================================//
	public function get_pmks_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_pmks_jenis")->result();
		return $data;
	}	

	public function insert_pmks_jenis($data){
		$insert = $this->db->insert("lp_pmks_jenis", $data);
		return $insert;
	}	

	public function get_pmks_jenis_where($where){
		$data = $this->db->get_where("lp_pmks_jenis",$where)->row_array();
		return $data;
	}

	public function update_pmks_jenis($set, $where){
		$update = $this->db->update("lp_pmks_jenis", $set, $where);
		return $update;
	}		

	public function delete_pmks_jenis($where){
		$delete = $this->db->delete("lp_pmks_jenis", $where);
		return $delete;
	}	
}	
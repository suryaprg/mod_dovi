<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_trans_jenis_kendaraan extends CI_Model {

#---------------------------------------------------------------Admin_trans_jenis_kendaraan---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("main_jenis_kendaraan")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("main_jenis_kendaraan",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("main_jenis_kendaraan", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("main_jenis_kendaraan", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("main_jenis_kendaraan", $where);
		return $delete;
	}

}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_kesehatan extends CI_Model
{

//====================================================== LP Kesehatan =======================================================//
	public function get_kesehatan(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_kesehatan_jenis kj', 'kp.id_jenis = kj.id_jenis');
		$this->db->join('lp_kesehatan_sub_jenis ksj', 'kp.id_sub_jenis = ksj.id_sub_jenis');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("Lp_kesehatan kp")->result();
		return $data;
	}

	public function get_kesehatan_where($where){
		$data = $this->db->get_where("lp_kesehatan",$where)->row_array();
		return $data;
	}	

	public function insert_kesehatan($data){
		$insert = $this->db->insert("lp_kesehatan", $data);
		return $insert;
	}

	public function update_kesehatan($set, $where){
		$update = $this->db->update("lp_kesehatan", $set, $where);
		return $update;
	}		

	public function delete_kesehatan($where){
		$delete = $this->db->delete("lp_kesehatan", $where);
		return $delete;
	}
//====================================================== LP Kesehatan =======================================================//				
//====================================================== LP Kesehatan Jenis =================================================//
	public function get_kesehatan_jenis(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_kesehatan_kategori kk','ks.id_kategori = kk.id_kategori');
		$data = $this->db->get("lp_kesehatan_jenis ks")->result();
		return $data;
	}	

	public function insert_kesehatan_jenis($data){
		$insert = $this->db->insert("lp_kesehatan_jenis", $data);
		return $insert;
	}	

	public function get_kesehatan_jenis_where($where){
		$data = $this->db->get_where("lp_kesehatan_jenis",$where)->row_array();
		return $data;
	}

	public function update_kesehatan_jenis($set, $where){
		$update = $this->db->update("lp_kesehatan_jenis", $set, $where);
		return $update;
	}		

	public function delete_kesehatan_jenis($where){
		$delete = $this->db->delete("lp_kesehatan_jenis", $where);
		return $delete;
	}		
//====================================================== LP Kesehatan Jenis =====================================================//	

//====================================================== LP Kesehatan Kategori =====================================================//
	public function get_kesehatan_kategori(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_kesehatan_kategori")->result();
		return $data;
	}	

	public function insert_kesehatan_kategori($data){
		$insert = $this->db->insert("lp_kesehatan_kategori", $data);
		return $insert;
	}	

	public function get_kesehatan_kategori_where($where){
		$data = $this->db->get_where("lp_kesehatan_kategori",$where)->row_array();
		return $data;
	}

	public function update_kesehatan_kategori($set, $where){
		$update = $this->db->update("lp_kesehatan_kategori", $set, $where);
		return $update;
	}		

	public function delete_kesehatan_kategori($where){
		$delete = $this->db->delete("lp_kesehatan_kategori", $where);
		return $delete;
	}		
//====================================================== LP Kesehatan Kategori =====================================================//	
//====================================================== LP Kesehatan Sub3 Jenis ====================================================//
	public function get_kesehatan_sub3_jenis(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_kesehatan_sub_jenis ksj','ksj3.id_sub_jenis = ksj.id_sub_jenis');
		$data = $this->db->get("lp_kesehatan_sub3_jenis ksj3")->result();
		return $data;
	}	

	public function insert_kesehatan_sub3_jenis($data){
		$insert = $this->db->insert("lp_kesehatan_sub3_jenis", $data);
		return $insert;
	}	

	public function get_kesehatan_sub3_jenis_where($where){
		$data = $this->db->get_where("lp_kesehatan_sub3_jenis",$where)->row_array();
		return $data;
	}

	public function update_kesehatan_sub3_jenis($set, $where){
		$update = $this->db->update("lp_kesehatan_sub3_jenis", $set, $where);
		return $update;
	}		

	public function delete_kesehatan_sub3_jenis($where){
		$delete = $this->db->delete("lp_kesehatan_sub3_jenis", $where);
		return $delete;
	}		
//====================================================== LP Kesehatan Sub3 Jenis ====================================================//	
//====================================================== LP Kesehatan Sub Jenis =====================================================//
	public function get_kesehatan_sub_jenis(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_kesehatan_jenis ks','ksj.id_jenis = ks.id_jenis');
		$data = $this->db->get("lp_kesehatan_sub_jenis ksj")->result();
		return $data;
	}	

	public function insert_kesehatan_sub_jenis($data){
		$insert = $this->db->insert("lp_kesehatan_sub_jenis", $data);
		return $insert;
	}	

	public function get_kesehatan_sub_jenis_where($where){
		$data = $this->db->get_where("lp_kesehatan_sub_jenis",$where)->row_array();
		return $data;
	}

	public function update_kesehatan_sub_jenis($set, $where){
		$update = $this->db->update("lp_kesehatan_sub_jenis", $set, $where);
		return $update;
	}		

	public function delete_kesehatan_sub_jenis($where){
		$delete = $this->db->delete("lp_kesehatan_sub_jenis", $where);
		return $delete;
	}		
//====================================================== LP Kesehatan Sub Jenis =====================================================//	
}

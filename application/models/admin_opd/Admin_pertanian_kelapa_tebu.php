<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pertanian_kelapa_tebu extends CI_Model {

#---------------------------------------------------------------Admin_pertanian_kelapa_tebu---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("pertanian_kelapa_tebu")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pertanian_kelapa_tebu",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pertanian_kelapa_tebu", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pertanian_kelapa_tebu", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pertanian_kelapa_tebu", $where);
		return $delete;
	}

}

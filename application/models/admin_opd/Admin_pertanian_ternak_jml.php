<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pertanian_ternak_jml extends CI_Model {

#---------------------------------------------------------------Admin_pertanian_ternak_jml---------------------------------------------------------------------

	public function get(){
		$this->db->join("pertanian_ternak_jenis ptjn", "ptj.id_jenis = ptjn.id_jenis");
		$data = $this->db->get("pertanian_ternak_jml ptj")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pertanian_ternak_jml",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pertanian_ternak_jml", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pertanian_ternak_jml", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pertanian_ternak_jml", $where);
		return $delete;
	}

}

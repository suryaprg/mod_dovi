<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_pendindustri extends CI_Model
{

//====================================================== LP PENDUDUK =======================================================//
	public function get_pend_industri(){
		$this->db->join('lp_pend_industri_jenis lpj', 'lpi.id_jenis = lpj.id_jenis');
		$this->db->join('lp_pend_industri_kategori lpk', 'lpi.id_kategori = lpk.id_kategori');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("lp_pend_industri lpi")->result();
		return $data;
	}

	public function get_pend_industri_where($where){
		$data = $this->db->get_where("lp_pend_industri",$where)->row_array();
		return $data;
	}	

	public function insert_pend_industri($data){
		$insert = $this->db->insert("lp_pend_industri", $data);
		return $insert;
	}

	public function update_pend_industri($set, $where){
		$update = $this->db->update("lp_pend_industri", $set, $where);
		return $update;
	}		

	public function delete_pend_industri($where){
		$delete = $this->db->delete("lp_pend_industri", $where);
		return $delete;
	}
//====================================================== LP PENDUDUK =======================================================//			
	//====================================================== MAIN KECAMATAN =====================================================//
	public function get_pend_industri_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_pend_industri_jenis")->result();
		return $data;
	}	

	public function insert_pendindustri_jenis($data){
		$insert = $this->db->insert("lp_pend_industri_jenis", $data);
		return $insert;
	}	

	public function get_pendindustri_jenis_where($where){
		$data = $this->db->get_where("lp_pend_industri_jenis",$where)->row_array();
		return $data;
	}

	public function update_pendindustri_jenis($set, $where){
		$update = $this->db->update("lp_pend_industri_jenis", $set, $where);
		return $update;
	}		

	public function delete_pendindustri_jenis($where){
		$delete = $this->db->delete("lp_pend_industri_jenis", $where);
		return $delete;
	}		
//====================================================== MAIN KECAMATAN =====================================================//	
		//====================================================== MAIN KECAMATAN =====================================================//
	public function get_pend_industri_kategori(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_pend_industri_kategori")->result();
		return $data;
	}	

	public function insert_pendindustri_kategori($data){
		$insert = $this->db->insert("lp_pend_industri_kategori", $data);
		return $insert;
	}	

	public function get_pendindustri_kategori_where($where){
		$data = $this->db->get_where("lp_pend_industri_kategori",$where)->row_array();
		return $data;
	}

	public function update_pendindustri_kategori($set, $where){
		$update = $this->db->update("lp_pend_industri_kategori", $set, $where);
		return $update;
	}		

	public function delete_pendindustri_kategori($where){
		$delete = $this->db->delete("lp_pend_industri_kategori", $where);
		return $delete;
	}		
//====================================================== MAIN KECAMATAN =====================================================//	
}
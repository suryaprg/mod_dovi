<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pertanian_holti_jml extends CI_Model {

#---------------------------------------------------------------Admin_pertanian_holti_jml---------------------------------------------------------------------

	public function get(){
		$this->db->join("pertanian_holti_jenis phjn", "phjm.id_jenis = phjn.id_jenis");
		$data = $this->db->get("pertanian_holti_jml phjm")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pertanian_holti_jml",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pertanian_holti_jml", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pertanian_holti_jml", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pertanian_holti_jml", $where);
		return $delete;
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_miskin_jml_pend extends CI_Model {

#---------------------------------------------------------------Admin_miskin_jml_pend---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("miskin_jml_pend")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("miskin_jml_pend",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("miskin_jml_pend", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("miskin_jml_pend", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("miskin_jml_pend", $where);
		return $delete;
	}

}

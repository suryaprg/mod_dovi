<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_perpustakaan extends CI_Model
{
	
//====================================================== LP PERPUS =======================================================//
	public function get_perpus(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_perpustakaan_jenis lpj', 'lp.id_jenis = lpj.id_jenis');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("lp_perpustakaan lp")->result();
		return $data;
	}

	public function get_perpus_where($where){
		$data = $this->db->get_where("lp_perpustakaan",$where)->row_array();
		return $data;
	}	

	public function insert_perpus($data){
		$insert = $this->db->insert("lp_perpustakaan", $data);
		return $insert;
	}

	public function update_perpus($set, $where){
		$update = $this->db->update("lp_perpustakaan", $set, $where);
		return $update;
	}		

	public function delete_perpus($where){
		$delete = $this->db->delete("lp_perpustakaan", $where);
		return $delete;
	}
//====================================================== LP PERPUS =======================================================//

//====================================================== LP PERPUS JENIS =================================================//
	public function get_perpus_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_perpustakaan_jenis")->result();
		return $data;
	}

	public function get_perpus_jenis_where($where){
		$data = $this->db->get_where("lp_perpustakaan_jenis",$where)->row_array();
		return $data;
	}	

	public function insert_perpus_jenis($data){
		$insert = $this->db->insert("lp_perpustakaan_jenis", $data);
		return $insert;
	}

	public function update_perpus_jenis($set, $where){
		$update = $this->db->update("lp_perpustakaan_jenis", $set, $where);
		return $update;
	}		

	public function delete_perpus_jenis($where){
		$delete = $this->db->delete("lp_perpustakaan_jenis", $where);
		return $delete;
	}
//====================================================== LP PERPUS JENIS =================================================//	
}
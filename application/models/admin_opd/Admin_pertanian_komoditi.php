<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pertanian_komoditi extends CI_Model {

#---------------------------------------------------------------Admin_pertanian_komoditi---------------------------------------------------------------------

	public function get(){
		$this->db->join("pertanian_komoditas_jenis pkjn", "pkjm.id_jenis = pkjn.id_jenis");
		$data = $this->db->get("pertanian_komoditas_jml pkjm")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pertanian_komoditas_jml",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pertanian_komoditas_jml", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pertanian_komoditas_jml", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pertanian_komoditas_jml", $where);
		return $delete;
	}

}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_pdb extends CI_Model
{
	
//====================================================== PDB ================================================//
	public function get_pdb(){
		$this->db->join('lp_pdb_jenis lpj', 'lp.id_jenis = lpj.id_jenis');
		$this->db->join('lp_pdb_kategori lpk', 'lp.id_kategori = lpk.id_kategori');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("lp_pdb lp")->result();
		return $data;
	}

	public function get_pdb_where($where){
		$data = $this->db->get_where("lp_pdb", $where)->row_array();
		return $data;
	}	

	public function insert_pdb($data){
		$insert = $this->db->insert("lp_pdb", $data);
		return $insert;
	}

	public function update_pdb($set, $where){
		$update = $this->db->update("lp_pdb", $set, $where);
		return $update;
	}		

	public function delete_pdb($where){
		$delete = $this->db->delete("lp_pdb", $where);
		return $delete;
	}
//====================================================== PDB ================================================//

//====================================================== PDB JENIS ================================================//
	public function get_pdb_jenis(){
		$data = $this->db->get("lp_pdb_jenis")->result();
		return $data;
	}

	public function get_pdb_jenis_where($where){
		$data = $this->db->get_where("lp_pdb_jenis", $where)->row_array();
		return $data;
	}	

	public function insert_pdb_jenis($data){
		$insert = $this->db->insert("lp_pdb_jenis", $data);
		return $insert;
	}

	public function update_pdb_jenis($set, $where){
		$update = $this->db->update("lp_pdb_jenis", $set, $where);
		return $update;
	}		

	public function delete_pdb_jenis($where){
		$delete = $this->db->delete("lp_pdb_jenis", $where);
		return $delete;
	}
//====================================================== PDB JENIS ================================================//

//====================================================== PDB KATEGORi ================================================//
	public function get_pdb_kategori(){
		$data = $this->db->get("lp_pdb_kategori")->result();
		return $data;
	}

	public function get_pdb_kategori_where($where){
		$data = $this->db->get_where("lp_pdb_kategori", $where)->row_array();
		return $data;
	}	

	public function insert_pdb_kategori($data){
		$insert = $this->db->insert("lp_pdb_kategori", $data);
		return $insert;
	}

	public function update_pdb_kategori($set, $where){
		$update = $this->db->update("lp_pdb_kategori", $set, $where);
		return $update;
	}		

	public function delete_pdb_kategori($where){
		$delete = $this->db->delete("lp_pdb_kategori", $where);
		return $delete;
	}
//====================================================== PDB KATEGORI ================================================//	
}
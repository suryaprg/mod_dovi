<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_disperkim_all extends CI_Model
{

//====================================================== LP disperkim_all =======================================================//
	public function get_disperkim_all(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('disperkim_all_jenis daj', 'da.id_jenis = daj.id_jenis');
		$this->db->join('disperkim_all_sub_jenis dasj', 'da.id_sub_jenis = dasj.id_sub_jenis');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("disperkim_all da")->result();
		return $data;
	}

	public function get_disperkim_all_where($where){
		$data = $this->db->get_where("disperkim_all",$where)->row_array();
		return $data;
	}	

	public function insert_disperkim_all($data){
		$insert = $this->db->insert("disperkim_all", $data);
		return $insert;
	}

	public function update_disperkim_all($set, $where){
		$update = $this->db->update("disperkim_all", $set, $where);
		return $update;
	}		

	public function delete_disperkim_all($where){
		$delete = $this->db->delete("disperkim_all", $where);
		return $delete;
	}
//====================================================== LP disperkim_all =======================================================//	
//====================================================== LP disperkim_all kategori =====================================================//
	public function get_disperkim_all_kategori(){
		$data = $this->db->get("disperkim_all_kategori")->result();
		return $data;
	}	

	public function insert_disperkim_all_kategori($data){
		$insert = $this->db->insert("disperkim_all_kategori", $data);
		return $insert;
	}	

	public function get_disperkim_all_kategori_where($where){
		$data = $this->db->get_where("disperkim_all_kategori",$where)->row_array();
		return $data;
	}

	public function update_disperkim_all_kategori($set, $where){
		$update = $this->db->update("disperkim_all_kategori", $set, $where);
		return $update;
	}		

	public function delete_disperkim_all_kategori($where){
		$delete = $this->db->delete("disperkim_all_kategori", $where);
		return $delete;
	}		
//====================================================== LP disperkim_all kategori =====================================================//	



//====================================================== LP disperkim_all Jenis =====================================================//
	public function get_disperkim_all_jenis(){
		$this->db->join('disperkim_all_kategori dak', 'daj.id_kategori = dak.id_kategori');
		$data = $this->db->get("disperkim_all_jenis daj")->result();
		return $data;
	}	

	public function insert_disperkim_all_jenis($data){
		$insert = $this->db->insert("disperkim_all_jenis", $data);
		return $insert;
	}	

	public function get_disperkim_all_jenis_where($where){
		$data = $this->db->get_where("disperkim_all_jenis",$where);
		return $data;
	}

	public function update_disperkim_all_jenis($set, $where){
		$update = $this->db->update("disperkim_all_jenis", $set, $where);
		return $update;
	}		

	public function delete_disperkim_all_jenis($where){
		$delete = $this->db->delete("disperkim_all_jenis", $where);
		return $delete;
	}		
//====================================================== LP disperkim_all Jenis =====================================================//	

//====================================================== LP disperkim_all Sub Jenis =====================================================//
	public function get_disperkim_all_sub_jenis(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('disperkim_all_jenis daj','dasj.id_jenis = daj.id_jenis');
		$data = $this->db->get("disperkim_all_sub_jenis dasj ")->result();
		return $data;
	}	

	public function insert_disperkim_all_sub_jenis($data){
		$insert = $this->db->insert("disperkim_all_sub_jenis", $data);
		return $insert;
	}	

	public function get_disperkim_all_sub_jenis_where($where){
		$data = $this->db->get_where("disperkim_all_sub_jenis",$where);
		return $data;
	}

	public function update_disperkim_all_sub_jenis($set, $where){
		$update = $this->db->update("disperkim_all_sub_jenis", $set, $where);
		return $update;
	}		

	public function delete_disperkim_all_sub_jenis($where){
		$delete = $this->db->delete("disperkim_all_sub_jenis", $where);
		return $delete;
	}		
//====================================================== LP disperkim_all Sub Jenis =====================================================//		
}

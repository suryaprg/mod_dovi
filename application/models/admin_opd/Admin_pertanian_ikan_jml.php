<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pertanian_ikan_jml extends CI_Model {

#---------------------------------------------------------------Admin_pertanian_ikan_jml---------------------------------------------------------------------

	public function get(){
		$this->db->join("pertanian_ikan_jenis pijn", "pij.id_jenis = pijn.id_jenis");
		$data = $this->db->get("pertanian_ikan_jml pij")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pertanian_ikan_jml",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pertanian_ikan_jml", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pertanian_ikan_jml", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pertanian_ikan_jml", $where);
		return $delete;
	}

}

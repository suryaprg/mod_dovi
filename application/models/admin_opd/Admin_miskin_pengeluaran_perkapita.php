<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_miskin_pengeluaran_perkapita extends CI_Model {

#---------------------------------------------------------------Admin_miskin_pengeluaran_perkapita---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("miskin_pengeluaran_perkapita")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("miskin_pengeluaran_perkapita",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("miskin_pengeluaran_perkapita", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("miskin_pengeluaran_perkapita", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("miskin_pengeluaran_perkapita", $where);
		return $delete;
	}

}

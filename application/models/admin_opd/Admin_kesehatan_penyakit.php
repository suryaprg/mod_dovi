<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_kesehatan_penyakit extends CI_Model {

#---------------------------------------------------------------Admin_penyakit---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("main_jenis_penyakit")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("main_jenis_penyakit",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("main_jenis_penyakit", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("main_jenis_penyakit", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("main_jenis_penyakit", $where);
		return $delete;
	}

}

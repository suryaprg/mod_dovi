<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pengeluaran_penduduk extends CI_Model {

#---------------------------------------------------------------Admin_pengeluaran_penduduk---------------------------------------------------------------------

	public function get(){
		$this->db->join("pengeluaran_penduduk_jenis ppj", "pp.id_jenis = ppj.id_jenis");
		$data = $this->db->get("pengeluaran_penduduk pp")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pengeluaran_penduduk",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pengeluaran_penduduk", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pengeluaran_penduduk", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pengeluaran_penduduk", $where);
		return $delete;
	}

}

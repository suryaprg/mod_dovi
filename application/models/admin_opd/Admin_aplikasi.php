<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_aplikasi extends CI_Model {

#---------------------------------------------------------------Admin_gizi---------------------------------------------------------------------

	public function get_aplikasi(){
		$data = $this->db->get("kominfo_aplikasi")->result();
		return $data;
	}

	public function get_aplikasi_where($where){
		$data = $this->db->get_where("kominfo_aplikasi",$where);
		return $data;
	}

	public function insert_aplikasi($data){
		$insert = $this->db->insert("kominfo_aplikasi", $data);
		return $insert;
	}

	public function update_aplikasi($set, $where){
		$update = $this->db->update("kominfo_aplikasi", $set, $where);
		return $update;
	}

	public function delete_aplikasi($where){
		$delete = $this->db->delete("kominfo_aplikasi", $where);
		return $delete;
	}

}

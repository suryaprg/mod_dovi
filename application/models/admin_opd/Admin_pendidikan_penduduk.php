<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pendidikan_penduduk extends CI_Model {

#---------------------------------------------------------------Admin_pendidikan_penduduk---------------------------------------------------------------------

	public function get(){
		$this->db->join("pendidikan_penduduk_jenis ppj", "pp.id_jenis = ppj.id_jenis");
		$data = $this->db->get("pendidikan_penduduk pp")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pendidikan_penduduk",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pendidikan_penduduk", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pendidikan_penduduk", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pendidikan_penduduk", $where);
		return $delete;
	}

}

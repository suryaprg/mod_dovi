<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_kajian_penelitian extends CI_Model
{
	public function get_kajian(){
		// $this->db->where("is_delete !=", "1");
		$this->db->order_by('nama_kajian', 'asc');
		$data = $this->db->get("lp_kajian_penelitian")->result();
		return $data;
	}	

	public function get_kajian_where($where){
		$data = $this->db->get_where("lp_kajian_penelitian",$where)->row_array();
		return $data;
	}	

	public function insert_kajian($data){
		$insert = $this->db->insert("lp_kajian_penelitian", $data);
		return $insert;
	}

	public function update_kajian($set, $where){
		$update = $this->db->update("lp_kajian_penelitian", $set, $where);
		return $update;
	}		

	public function delete_kajian($where){
		$delete = $this->db->delete("lp_kajian_penelitian", $where);
		return $delete;
	}
}
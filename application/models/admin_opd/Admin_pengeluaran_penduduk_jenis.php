<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pengeluaran_penduduk_jenis extends CI_Model {

#---------------------------------------------------------------Admin_pengeluaran_penduduk_jenis---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("pengeluaran_penduduk_jenis")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pengeluaran_penduduk_jenis",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pengeluaran_penduduk_jenis", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pengeluaran_penduduk_jenis", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pengeluaran_penduduk_jenis", $where);
		return $delete;
	}

}

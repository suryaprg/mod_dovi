<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_csr extends CI_Model {

#---------------------------------------------------------------Admin_gizi---------------------------------------------------------------------

	public function get_csr(){
		$data = $this->db->get("disperkim_csr")->result();
		return $data;
	}

	public function get_csr_where($where){
		$data = $this->db->get_where("disperkim_csr",$where);
		return $data;
	}

	public function insert_csr($data){
		$insert = $this->db->insert("disperkim_csr", $data);
		return $insert;
	}

	public function update_csr($set, $where){
		$update = $this->db->update("disperkim_csr", $set, $where);
		return $update;
	}

	public function delete_csr($where){
		$delete = $this->db->delete("disperkim_csr", $where);
		return $delete;
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pohon extends CI_Model {

#---------------------------------------------------------------Admin_gizi---------------------------------------------------------------------

	public function get_pohon(){
		$this->db->join("disperkim_penanaman_pohon_jenis appj", "app.id_jenis = appj.id_jenis");
		$data = $this->db->get("disperkim_penanaman_pohon app")->result();
		return $data;
	}
	public function get_pohon_jenis(){
		$data = $this->db->get("disperkim_penanaman_pohon_jenis")->result();
		return $data;
	}

	public function get_pohon_where($where){
		$data = $this->db->get_where("disperkim_penanaman_pohon",$where)->row_array();
		return $data;
	}

	public function insert_pohon($data){
		$insert = $this->db->insert("disperkim_penanaman_pohon", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("disperkim_penanaman_pohon", $set, $where);
		return $update;
	}

	public function delete_pohon($where){
		$delete = $this->db->delete("disperkim_penanaman_pohon", $where);
		return $delete;
	}


	public function get_pohon_jenis_where($where){
		$data = $this->db->get_where("disperkim_penanaman_pohon_jenis",$where)->row_array();;
		return $data;
	}

	public function insert_pohon_jenis($data){
		$insert = $this->db->insert("disperkim_penanaman_pohon_jenis", $data);
		return $insert;
	}

	public function update_pohon_jenis($set, $where){
		$update = $this->db->update("disperkim_penanaman_pohon_jenis", $set, $where);
		return $update;
	}

	public function delete_pohon_jenis($where){
		$delete = $this->db->delete("disperkim_penanaman_pohon_jenis", $where);
		return $delete;
	}

}

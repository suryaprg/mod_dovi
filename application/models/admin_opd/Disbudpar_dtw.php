<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Disbudpar_dtw extends CI_Model
{
public function get_dtw_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("disbudpar_dtw_jenis")->result();
		return $data;
	}	

	public function insert_dtw_jenis($data){
		$insert = $this->db->insert("disbudpar_dtw_jenis", $data);
		return $insert;
	}	

	public function get_dtw_jenis_where($where){
		$data = $this->db->get_where("disbudpar_dtw_jenis",$where)->row_array();
		return $data;
	}

	public function update_dtw_jenis($set, $where){
		$update = $this->db->update("disbudpar_dtw_jenis", $set, $where);
		return $update;
	}		

	public function delete_dtw_jenis($where){
		$delete = $this->db->delete("disbudpar_dtw_jenis", $where);
		return $delete;
	}		
//====================================================== LP PERTANIAN sSUB JENIS =======================================//	
	public function get_dtw_main(){
		// $this->db->where("is_delete !=", "1");
		 $this->db->join('disbudpar_dtw_jenis lpp', 'lpa.id_jenis = lpp.id_jenis');
		$data = $this->db->get("disbudpar_dtw_main lpa")->result();
		return $data;
	}	

	public function insert_dtw_main($data){
		$insert = $this->db->insert("disbudpar_dtw_main", $data);
		return $insert;
	}	

	public function get_dtw_main_where($where){
		$data = $this->db->get_where("disbudpar_dtw_main",$where)->row_array();
		return $data;
	}

	public function update_dtw_main($set, $where){
		$update = $this->db->update("disbudpar_dtw_main", $set, $where);
		return $update;
	}		

	public function delete_dtw_main($where){
		$delete = $this->db->delete("disbudpar_dtw_main", $where);
		return $delete;
	}		
}
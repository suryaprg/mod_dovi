<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_kerja_angkatan extends CI_Model {

#---------------------------------------------------------------kerja_angkatan---------------------------------------------------------------------

	public function get_kerja(){
		$data = $this->db->get("kerja_angkatan")->result();
		return $data;
	}

	public function get_kerja_where($where){
		$data = $this->db->get_where("kerja_angkatan",$where);
		return $data;
	}

	public function insert_kerja($data){
		$insert = $this->db->insert("kerja_angkatan", $data);
		return $insert;
	}

	public function update_kerja($set, $where){
		$update = $this->db->update("kerja_angkatan", $set, $where);
		return $update;
	}

	public function delete_kerja($where){
		$delete = $this->db->delete("kerja_angkatan", $where);
		return $delete;
	}

}

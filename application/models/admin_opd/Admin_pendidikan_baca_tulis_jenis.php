<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pendidikan_baca_tulis_jenis extends CI_Model {

#---------------------------------------------------------------Admin_pendidikan_baca_tulis_jenis---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("pendidikan_baca_tulis_jenis")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pendidikan_baca_tulis_jenis",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pendidikan_baca_tulis_jenis", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pendidikan_baca_tulis_jenis", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pendidikan_baca_tulis_jenis", $where);
		return $delete;
	}

}

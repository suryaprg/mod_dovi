<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_taman_jenis extends CI_Model {

#---------------------------------------------------------------Admin_gizi---------------------------------------------------------------------

	public function get_taman_jenis(){
		$data = $this->db->get("disperkim_taman_jenis")->result();
		return $data;
	}

	public function get_taman_jenis_where($where){
		$data = $this->db->get_where("disperkim_taman_jenis",$where);
		return $data;
	}

	public function insert_taman_jenis($data){
		$insert = $this->db->insert("disperkim_taman_jenis", $data);
		return $insert;
	}

	public function update_taman_jenis($set, $where){
		$update = $this->db->update("disperkim_taman_jenis", $set, $where);
		return $update;
	}

	public function delete_taman_jenis($where){
		$delete = $this->db->delete("disperkim_taman_jenis", $where);
		return $delete;
	}

}

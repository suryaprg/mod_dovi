<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_perdagangan extends CI_Model {

#---------------------------------------------------------------Admin_pendidikan_baca_tulis---------------------------------------------------------------------

	public function get(){
		$this->db->join("dinas_perdagangan_jenis dpj", "dp.id_jenis = dpj.id_jenis");
		
		$data = $this->db->get("dinas_perdagangan dp")->result();
		return $data;
	}
	

	public function get_where($where){
		$data = $this->db->get_where("dinas_perdagangan",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("dinas_perdagangan", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("dinas_perdagangan", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("dinas_perdagangan", $where);
		return $delete;
	}

}

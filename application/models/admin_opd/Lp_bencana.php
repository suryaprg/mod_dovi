<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_bencana extends CI_Model
{
//===================================================== LP BENCANA =======================================================//	
	public function get_bencana(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_bencana_jenis bj', 'bc.id_jenis = bj.id_jenis');
		$this->db->join('main_kecamatan mk', 'bc.id_kec = mk.id_kec');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("lp_bencana bc")->result();
		return $data;
	}	

	public function get_bencana_where($where){
		$data = $this->db->get_where("lp_bencana",$where)->row_array();
		return $data;
	}	

	public function insert_bencana($data){
		$insert = $this->db->insert("lp_bencana", $data);
		return $insert;
	}

	public function update_bencana($set, $where){
		$update = $this->db->update("lp_bencana", $set, $where);
		return $update;
	}		

	public function delete_bencana($where){
		$delete = $this->db->delete("lp_bencana", $where);
		return $delete;
	}				
//===================================================== LP BENCANA =======================================================//

//===================================================== LP BENCANA JENIS ===================================================//
	public function get_bencana_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_bencana_jenis")->result();
		return $data;
	}

	public function get_bencana_jenis_where($where){
		$data = $this->db->get_where("lp_bencana_jenis",$where)->row_array();
		return $data;
	}	

	public function insert_bencana_jenis($data){
		$insert = $this->db->insert("lp_bencana_jenis", $data);
		return $insert;
	}

	public function update_bencana_jenis($set, $where){
		$update = $this->db->update("lp_bencana_jenis", $set, $where);
		return $update;
	}		

	public function delete_bencana_jenis($where){
		$delete = $this->db->delete("lp_bencana_jenis", $where);
		return $delete;
	}		
//===================================================== LP BENCANA JENIS ===================================================//	

	public function get_main_kec(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("main_kecamatan")->result();
		return $data;
	}	
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_kendaraan extends CI_Model
{
	public function get_kendaraan_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("main_jenis_kendaraan")->result();
		return $data;
	}

	public function get_kecamatan(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("main_kecamatan")->result();
		return $data;
	}

//================================================== LP KENDARAAN KEC =============================================//	
	public function get_kendaraan(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('main_jenis_kendaraan mjk', 'kk.id_jenis_kendaraan = mjk.id_jenis_kendaraan');
		$this->db->join('main_kecamatan mk', 'kk.id_kec = mk.id_kec');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("lp_kendaran_kec kk")->result();
		return $data;
	}

	public function get_kendaraan_where($where){
		$data = $this->db->get_where("lp_kendaran_kec",$where)->row_array();
		return $data;
	}	

	public function insert_kendaraan($data){
		$insert = $this->db->insert("lp_kendaran_kec", $data);
		return $insert;
	}

	public function update_kendaraan($set, $where){
		$update = $this->db->update("lp_kendaran_kec", $set, $where);
		return $update;
	}		

	public function delete_kendaraan($where){
		$delete = $this->db->delete("lp_kendaran_kec", $where);
		return $delete;
	}
//================================================== LP KENDARAAN KEC =============================================//

//================================================== LP KENDARAAN PLAT =============================================//	
	public function get_kendaraan_plat(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_kendaran_plat_jenis lkpj', 'lkp.id_jenis = lkpj.id_jenis');
		$data = $this->db->get("lp_kendaran_plat lkp")->result();
		return $data;
	}

	public function get_kendaraan_plat_where($where){
		$data = $this->db->get_where("lp_kendaran_plat",$where)->row_array();
		return $data;
	}	

	public function insert_kendaraan_plat($data){
		$insert = $this->db->insert("lp_kendaran_plat", $data);
		return $insert;
	}

	public function update_kendaraan_plat($set, $where){
		$update = $this->db->update("lp_kendaran_plat", $set, $where);
		return $update;
	}		

	public function delete_kendaraan_plat($where){
		$delete = $this->db->delete("lp_kendaran_plat", $where);
		return $delete;
	}
//================================================== LP KENDARAAN PLAT =============================================//

//================================================== LP KENDARAAN PLAT JENIS =============================================//
	public function get_kendaraan_plat_jenis(){
		// $this->db->where("is_delete !=", "1");
		// $this->db->join('lp_kendaran_plat_jenis lkpj', 'lkp.id_jenis = lkpj.id_jenis');
		$data = $this->db->get("lp_kendaran_plat_jenis")->result();
		return $data;
	}

	public function get_kendaraan_plat_jenis_where($where){
		$data = $this->db->get_where("lp_kendaran_plat_jenis",$where)->row_array();
		return $data;
	}	

	public function insert_kendaraan_plat_jenis($data){
		$insert = $this->db->insert("lp_kendaran_plat_jenis", $data);
		return $insert;
	}

	public function update_kendaraan_plat_jenis($set, $where){
		$update = $this->db->update("lp_kendaran_plat_jenis", $set, $where);
		return $update;
	}		

	public function delete_kendaraan_plat_jenis($where){
		$delete = $this->db->delete("lp_kendaran_plat_jenis", $where);
		return $delete;
	}
//================================================== LP KENDARAAN PLAT JENIS =============================================//	

//================================================== LP KENDARAAN UMUM =============================================//	
	public function get_kendaraan_umum(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_kendaran_umum_jenis lkuj', 'lku.id_jenis = lkuj.id_jenis');
		$this->db->join('main_jenis_kendaraan mjk2', 'lku.id_jenis_kendaraan = mjk2.id_jenis_kendaraan');
		$data = $this->db->get("lp_kendaran_umum lku")->result();
		return $data;
	}

	public function get_kendaraan_umum_where($where){
		$data = $this->db->get_where("lp_kendaran_umum",$where)->row_array();
		return $data;
	}	

	public function insert_kendaraan_umum($data){
		$insert = $this->db->insert("lp_kendaran_umum", $data);
		return $insert;
	}

	public function update_kendaraan_umum($set, $where){
		$update = $this->db->update("lp_kendaran_umum", $set, $where);
		return $update;
	}		

	public function delete_kendaraan_umum($where){
		$delete = $this->db->delete("lp_kendaran_umum", $where);
		return $delete;
	}	
//================================================== LP KENDARAAN UMUM =============================================//

//================================================== LP KENDARAAN UMUM JENIS =============================================//
	public function get_kendaraan_umum_jenis(){
		// $this->db->where("is_delete !=", "1");
		// $this->db->join('lp_kendaran_plat_jenis lkpj', 'lkp.id_jenis = lkpj.id_jenis');
		$data = $this->db->get("lp_kendaran_umum_jenis")->result();
		return $data;
	}

	public function get_kendaraan_umum_jenis_where($where){
		$data = $this->db->get_where("lp_kendaran_umum_jenis",$where)->row_array();
		return $data;
	}	

	public function insert_kendaraan_umum_jenis($data){
		$insert = $this->db->insert("lp_kendaran_umum_jenis", $data);
		return $insert;
	}

	public function update_kendaraan_umum_jenis($set, $where){
		$update = $this->db->update("lp_kendaran_umum_jenis", $set, $where);
		return $update;
	}		

	public function delete_kendaraan_umum_jenis($where){
		$delete = $this->db->delete("lp_kendaran_umum_jenis", $where);
		return $delete;
	}	
//================================================== LP KENDARAAN UMUM JENIS =============================================//	

}
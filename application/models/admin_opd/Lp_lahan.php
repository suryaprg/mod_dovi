<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_lahan extends CI_Model
{

//====================================================== LP lahan =======================================================//
	public function get_lahan(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_lahan_jenis lj','ll.id_jenis = lj.id_jenis');
		$this->db->join('main_kecamatan mk','ll.id_kec = mk.id_kec');
		$data = $this->db->get("lp_lahan ll")->result();
		return $data;
	}

	public function get_lahan_where($where){
		$data = $this->db->get_where("lp_lahan",$where)->row_array();
		return $data;
	}	
	public function insert_lahan($data){
		$insert = $this->db->insert("lp_lahan", $data);
		return $insert;
	}

	public function update_lahan($set, $where){
		$update = $this->db->update("lp_lahan", $set, $where);
		return $update;
	}		

	public function delete_lahan($where){
		$delete = $this->db->delete("lp_lahan", $where);
		return $delete;
	}
//====================================================== LP lahan =======================================================//				
//================================================= LP LAHAN JENIS =====================================================//
	public function get_lahan_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_lahan_jenis")->result();
		return $data;
	}	

	public function insert_lahan_jenis($data){
		$insert = $this->db->insert("lp_lahan_jenis", $data);
		return $insert;
	}	

	public function get_lahan_jenis_where($where){
		$data = $this->db->get_where("lp_lahan_jenis",$where)->row_array();
		return $data;
	}

	public function update_lahan_jenis($set, $where){
		$update = $this->db->update("lp_lahan_jenis", $set, $where);
		return $update;
	}		

	public function delete_lahan_jenis($where){
		$delete = $this->db->delete("lp_lahan_jenis", $where);
		return $delete;
	}		
//====================================================== LP lahan JENIS =====================================================//	
public function get_main_kecamatan(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("main_kecamatan")->result();
		return $data;
	}	
}
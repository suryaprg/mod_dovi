<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_pajak extends CI_Model
{

//====================================================== LP pajak =======================================================//
	public function get_pajak(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_pajak_jenis pj','lp.id_jenis = pj.id_jenis');
		$data = $this->db->get("lp_pajak lp")->result();
		return $data;
	}

	public function get_pajak_where($where){
		$data = $this->db->get_where("lp_pajak",$where)->row_array();
		return $data;
	}	

	public function insert_pajak($data){
		$insert = $this->db->insert("lp_pajak", $data);
		return $insert;
	}

	public function update_pajak($set, $where){
		$update = $this->db->update("lp_pajak", $set, $where);
		return $update;
	}		

	public function delete_pajak($where){
		$delete = $this->db->delete("lp_pajak", $where);
		return $delete;
	}
//====================================================== LP pajak =======================================================//				
//====================================================== LP pajak JENIS =====================================================//
	public function get_pajak_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_pajak_jenis")->result();
		return $data;
	}	

	public function insert_pajak_jenis($data){
		$insert = $this->db->insert("lp_pajak_jenis", $data);
		return $insert;
	}	

	public function get_pajak_jenis_where($where){
		$data = $this->db->get_where("lp_pajak_jenis",$where)->row_array();
		return $data;
	}

	public function update_pajak_jenis($set, $where){
		$update = $this->db->update("lp_pajak_jenis", $set, $where);
		return $update;
	}		

	public function delete_pajak_jenis($where){
		$delete = $this->db->delete("lp_pajak_jenis", $where);
		return $delete;
	}		
//====================================================== LP pajak JENIS =====================================================//	
}
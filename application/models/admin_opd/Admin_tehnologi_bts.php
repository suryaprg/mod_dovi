<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_tehnologi_bts extends CI_Model {

#---------------------------------------------------------------Admin_tehnologi_bts---------------------------------------------------------------------

	public function get_tehnologi(){
		$data = $this->db->get("tehnologi_jml_bts")->result();
		return $data;
	}

	public function get_tehnologi_where($where){
		$data = $this->db->get_where("tehnologi_jml_bts",$where);
		return $data;
	}

	public function insert_tehnologi($data){
		$insert = $this->db->insert("tehnologi_jml_bts", $data);
		return $insert;
	}

	public function update_tehnologi($set, $where){
		$update = $this->db->update("tehnologi_jml_bts", $set, $where);
		return $update;
	}

	public function delete_tehnologi($where){
		$delete = $this->db->delete("tehnologi_jml_bts", $where);
		return $delete;
	}

}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_keu extends CI_Model
{

//====================================================== LP Keu =======================================================//
	public function get_keu(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_keu_jenis kj', 'ku.id_jenis = kj.id_jenis');
		$this->db->join('lp_keu_sub_jenis ksj', 'ku.id_sub_jenis = ksj.id_sub_jenis');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("Lp_keu ku")->result();
		return $data;
	}

	public function get_keu_where($where){
		$data = $this->db->get_where("lp_keu",$where)->row_array();
		return $data;
	}	

	public function insert_keu($data){
		$insert = $this->db->insert("lp_keu", $data);
		return $insert;
	}

	public function update_keu($set, $where){
		$update = $this->db->update("lp_keu", $set, $where);
		return $update;
	}		

	public function delete_keu($where){
		$delete = $this->db->delete("lp_keu", $where);
		return $delete;
	}
//====================================================== LP Keu =======================================================//				
//====================================================== LP Keu Jenis =====================================================//
	public function get_keu_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_keu_jenis ")->result();
		return $data;
	}	

	public function insert_keu_jenis($data){
		$insert = $this->db->insert("lp_keu_jenis", $data);
		return $insert;
	}	

	public function get_keu_jenis_where($where){
		$data = $this->db->get_where("lp_keu_jenis",$where)->row_array();
		return $data;
	}

	public function update_keu_jenis($set, $where){
		$update = $this->db->update("lp_keu_jenis", $set, $where);
		return $update;
	}		

	public function delete_keu_jenis($where){
		$delete = $this->db->delete("lp_keu_jenis", $where);
		return $delete;
	}		
//====================================================== LP Keu Jenis =====================================================//	

//====================================================== LP Keu Sub Jenis =====================================================//
	public function get_keu_sub_jenis(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_keu_jenis kj','ksj.id_jenis = kj.id_jenis');
		$data = $this->db->get("lp_keu_sub_jenis ksj")->result();
		return $data;
	}	

	public function insert_keu_sub_jenis($data){
		$insert = $this->db->insert("lp_keu_sub_jenis", $data);
		return $insert;
	}	

	public function get_keu_sub_jenis_where($where){
		$data = $this->db->get_where("lp_keu_sub_jenis",$where)->row_array();
		return $data;
	}

	public function update_keu_sub_jenis($set, $where){
		$update = $this->db->update("lp_keu_sub_jenis", $set, $where);
		return $update;
	}		

	public function delete_keu_sub_jenis($where){
		$delete = $this->db->delete("lp_keu_sub_jenis", $where);
		return $delete;
	}		
//====================================================== LP Keu Sub Jenis =====================================================//		
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pengeluaran_rmt_tgg extends CI_Model {

#---------------------------------------------------------------Admin_pengeluaran_rmt_tgg---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("pengeluaran_rmt_tgg")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pengeluaran_rmt_tgg",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pengeluaran_rmt_tgg", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pengeluaran_rmt_tgg", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pengeluaran_rmt_tgg", $where);
		return $delete;
	}

}

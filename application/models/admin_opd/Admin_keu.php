<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_keu extends CI_Model {

#---------------------------------------------------------------Data Keuangan---------------------------------------------------------------------

	public function get_keu(){
		$this->db->join("keu_jenis kjn", "kjm.id_jenis = kjn.id_jenis");
		$data = $this->db->get("keu_jml kjm")->result();
		return $data;
	}

	public function get_keu_where($where){
		$data = $this->db->get_where("keu_jml",$where);
		return $data;
	}

	public function insert_keu($data){
		$insert = $this->db->insert("keu_jml", $data);
		return $insert;
	}

	public function update_keu($set, $where){
		$update = $this->db->update("keu_jml", $set, $where);
		return $update;
	}

	public function delete_keu($where){
		$delete = $this->db->delete("keu_jml", $where);
		return $delete;
	}

#---------------------------------------------------------------Jenis Keuangan---------------------------------------------------------------------

	public function get_jenis_keu(){
		$data = $this->db->get("keu_jenis")->result();
		return $data;
	}

	public function get_jenis_keu_where($where){
		$data = $this->db->get_where("keu_jenis",$where);
		return $data;
	}

	public function insert_jenis_keu($data){
		$insert = $this->db->insert("keu_jenis", $data);
		return $insert;
	}

	public function update_jenis_keu($set, $where){
		$update = $this->db->update("keu_jenis", $set, $where);
		return $update;
	}

	public function delete_jenis_keu($where){
		$delete = $this->db->delete("keu_jenis", $where);
		return $delete;
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_dsu extends CI_Model {

#---------------------------------------------------------------Admin_gizi---------------------------------------------------------------------

	public function get_dsu(){
		$data = $this->db->get("disperkim_dsu")->result();
		return $data;
	}

	public function get_dsu_where($where){
		$data = $this->db->get_where("disperkim_dsu",$where);
		return $data;
	}

	public function insert_dsu($data){
		$insert = $this->db->insert("disperkim_dsu", $data);
		return $insert;
	}

	public function update_dsu($set, $where){
		$update = $this->db->update("disperkim_dsu", $set, $where);
		return $update;
	}

	public function delete_dsu($where){
		$delete = $this->db->delete("disperkim_dsu", $where);
		return $delete;
	}

}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Obj extends CI_Model
{

//====================================================== OBJ =======================================================//
	public function get_obj(){
		
		$this->db->join('disbudpar_obj_pemajuan_bud_kate ok', 'om.id_kategori = ok.id_kategori');
		$data = $this->db->get("disbudpar_obj_pemajuan_bud_main om")->result();//kurang faham
		return $data;
	}

	public function get_obj_where($where){
		$data = $this->db->get_where("disbudpar_obj_pemajuan_bud_main",$where)->row_array();
		return $data;
	}	

	public function insert_obj($data){
		$insert = $this->db->insert("disbudpar_obj_pemajuan_bud_main", $data);
		return $insert;
	}

	public function update_obj($set, $where){
		$update = $this->db->update("disbudpar_obj_pemajuan_bud_main", $set, $where);
		return $update;
	}		

	public function delete_obj($where){
		$delete = $this->db->delete("disbudpar_obj_pemajuan_bud_main", $where);
		return $delete;
	}
//====================================================== OBJ =======================================================//				
//====================================================== OBJ JENIS =================================================//
	public function get_obj_kategori(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("disbudpar_obj_pemajuan_bud_kate")->result();
		return $data;
	}	

	public function insert_obj_kategori($data){
		$insert = $this->db->insert("disbudpar_obj_pemajuan_bud_kate", $data);
		return $insert;
	}	

	public function get_obj_kategori_where($where){
		$data = $this->db->get_where("disbudpar_obj_pemajuan_bud_kate",$where)->row_array();
		return $data;
	}

	public function update_obj_kategori($set, $where){
		$update = $this->db->update("disbudpar_obj_pemajuan_bud_kate", $set, $where);
		return $update;
	}		

	public function delete_obj_kategori($where){
		$delete = $this->db->delete("disbudpar_obj_pemajuan_bud_kate", $where);
		return $delete;
	}		
//====================================================== OBJ JENIS =================================================//		
}
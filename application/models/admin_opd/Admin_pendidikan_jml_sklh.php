<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pendidikan_jml_sklh extends CI_Model {

#---------------------------------------------------------------Admin_pendidikan_jml_sklh---------------------------------------------------------------------

	public function get(){
		$this->db->join("pendidikan_jml_sklh_jenis ppsj", "pps.id_jenis = ppsj.id_jenis");
		$data = $this->db->get("pendidikan_jml_sklh pps")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pendidikan_jml_sklh",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pendidikan_jml_sklh", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pendidikan_jml_sklh", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pendidikan_jml_sklh", $where);
		return $delete;
	}

}

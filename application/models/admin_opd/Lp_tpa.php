<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_tpa extends CI_Model
{

//==================================================== LP TPA ====================================================//
	public function get_tpa(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('main_kecamatan mk', 'lt.id_kec = mk.id_kec');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("lp_tpa lt ")->result();
		return $data;
	}

	public function get_tpa_where($where){
		$data = $this->db->get_where("lp_tpa",$where)->row_array();
		return $data;
	}	

	public function insert_tpa($data){
		$insert = $this->db->insert("lp_tpa", $data);
		return $insert;
	}

	public function update_tpa($set, $where){
		$update = $this->db->update("lp_tpa", $set, $where);
		return $update;
	}		

	public function delete_tpa($where){
		$delete = $this->db->delete("lp_tpa", $where);
		return $delete;
	}
//==================================================== LP TPA =====================================================//	

//==================================================== LP Kecamatan ====================================================//
	public function get_kecamatan(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("main_kecamatan")->result();
		return $data;
	}

//==================================================== LP Kecamatan =====================================================//			
}
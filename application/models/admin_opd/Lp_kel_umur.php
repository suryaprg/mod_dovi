<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_kel_umur extends CI_Model
{
//================================================== LP KEL UMUR =============================================//	
	public function get_umur(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_kel_umur_jenis kuj', 'ku.id_jenis = kuj.id_jenis');
		$data = $this->db->get("lp_kel_umur ku")->result();
		return $data;
	}		

	public function get_umur_where($where){
		$data = $this->db->get_where("lp_kel_umur",$where)->row_array();
		return $data;
	}	

	public function insert_umur($data){
		$insert = $this->db->insert("lp_kel_umur", $data);
		return $insert;
	}

	public function update_umur($set, $where){
		$update = $this->db->update("lp_kel_umur", $set, $where);
		return $update;
	}		

	public function delete_umur($where){
		$delete = $this->db->delete("lp_kel_umur", $where);
		return $delete;
	}
//================================================== LP KEL UMUR =============================================//

//================================================== LP KEL UMUR JENIS =============================================//
	public function get_umur_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_kel_umur_jenis")->result();
		return $data;
	}

	public function get_umur_jenis_where($where){
		$data = $this->db->get_where("lp_kel_umur_jenis",$where)->row_array();
		return $data;
	}	

	public function insert_umur_jenis($data){
		$insert = $this->db->insert("lp_kel_umur_jenis", $data);
		return $insert;
	}

	public function update_umur_jenis($set, $where){
		$update = $this->db->update("lp_kel_umur_jenis", $set, $where);
		return $update;
	}		

	public function delete_umur_jenis($where){
		$delete = $this->db->delete("lp_kel_umur_jenis", $where);
		return $delete;
	}	
//================================================== LP KEL UMUR JENIS =============================================//	
}
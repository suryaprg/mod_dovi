<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_trans_jalan extends CI_Model {

#---------------------------------------------------------------Admin_trans_jalan---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("trans_jln")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("trans_jln",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("trans_jln", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("trans_jln", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("trans_jln", $where);
		return $delete;
	}

}

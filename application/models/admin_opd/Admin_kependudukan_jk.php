<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_kependudukan_jk extends CI_Model {

#---------------------------------------------------------------kependudukan_jk---------------------------------------------------------------------

	public function get_kependudukan(){
		$data = $this->db->get("kepandudukan_jk")->result();
		return $data;
	}

	public function get_kependudukan_where($where){
		$data = $this->db->get_where("kepandudukan_jk",$where);
		return $data;
	}

	public function insert_kependudukan($data){
		$insert = $this->db->insert("kepandudukan_jk", $data);
		return $insert;
	}

	public function update_kependudukan($set, $where){
		$update = $this->db->update("kepandudukan_jk", $set, $where);
		return $update;
	}

	public function delete_kependudukan($where){
		$delete = $this->db->delete("kepandudukan_jk", $where);
		return $delete;
	}

}

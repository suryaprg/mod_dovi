<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_trans_jml_kendaraan extends CI_Model {

#---------------------------------------------------------------Admin_trans_jml_kendaraan---------------------------------------------------------------------

	public function get(){
		$this->db->join("main_jenis_kendaraan mjk", "tjk.id_jenis_kendaraan = mjk.id_jenis_kendaraan");
		$data = $this->db->get("trans_jml_kendaraan tjk")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("trans_jml_kendaraan",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("trans_jml_kendaraan", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("trans_jml_kendaraan", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("trans_jml_kendaraan", $where);
		return $delete;
	}

}

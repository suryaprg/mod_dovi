<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_kerja_ump extends CI_Model {

#---------------------------------------------------------------Admin_kerja_pengangguran---------------------------------------------------------------------

	public function get_kerja(){
		$data = $this->db->get("kerja_ump")->result();
		return $data;
	}

	public function get_kerja_where($where){
		$data = $this->db->get_where("kerja_ump",$where);
		return $data;
	}

	public function insert_kerja($data){
		$insert = $this->db->insert("kerja_ump", $data);
		return $insert;
	}

	public function update_kerja($set, $where){
		$update = $this->db->update("kerja_ump", $set, $where);
		return $update;
	}

	public function delete_kerja($where){
		$delete = $this->db->delete("kerja_ump", $where);
		return $delete;
	}

}

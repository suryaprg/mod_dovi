<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_pidana extends CI_Model
{

//====================================================== LP PIDANA =======================================================//
	public function get_pidana(){
		//$this->db->where("is_delete !=", "1");
		//$this->db->join('lp_pidana_sub_jenis lpj', 'lp.id_sub_jenis = lpj.id_sub_jenis');
		//$data = $this->db->get("lp_pertanian lp")->result();
		//return $data;

		$this->db->join('lp_pidana_jenis lpp', 'lpa.id_jenis = lpp.id_jenis');
		$data = $this->db->get("lp_pidana lpa")->result();
		return $data;
		
	}

	public function get_pidana_where($where){
		$data = $this->db->get_where("lp_pidana",$where)->row_array();
		return $data;
	}	

	public function insert_pidana($data){
		$insert = $this->db->insert("lp_pidana", $data);
		return $insert;
	}

	public function update_pidana($set, $where){
		$update = $this->db->update("lp_pidana", $set, $where);
		return $update;
	}		

	public function delete_pidana($where){
		$delete = $this->db->delete("lp_pidana", $where);
		return $delete;
	}				
//=============================================== LP PIDANA JENIS =====================================================//
	public function get_pidana_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_pidana_jenis")->result();
		return $data;
	}	

	public function insert_pidana_jenis($data){
		$insert = $this->db->insert("lp_pidana_jenis", $data);
		return $insert;
	}	

	public function get_pidana_jenis_where($where){
		$data = $this->db->get_where("lp_pidana_jenis",$where)->row_array();
		return $data;
	}

	public function update_pidana_jenis($set, $where){
		$update = $this->db->update("lp_pidana_jenis", $set, $where);
		return $update;
	}		

	public function delete_pidana_jenis($where){
		$delete = $this->db->delete("lp_pidana_jenis", $where);
		return $delete;
	}		
//====================================================== LP PIDANA KEC =====================================================//
public function get_pidana_kec(){

	$this->db->join('main_kecamatan mk', 'lss.id_kec = mk.id_kec');
		$data = $this->db->get("lp_pidana_kec lss")->result();
		return $data;
		
	
		
	}

	public function get_pidana_kec_where($where){
		$data = $this->db->get_where("lp_pidana_kec",$where)->row_array();
		return $data;
	}	

	public function insert_pidana_kec($data){
		$insert = $this->db->insert("lp_pidana_kec", $data);
		return $insert;
	}

	public function update_pidana_kec($set, $where){
		$update = $this->db->update("lp_pidana_kec", $set, $where);
		return $update;
	}		

	public function delete_pidana_kec($where){
		$delete = $this->db->delete("lp_pidana_kec", $where);
		return $delete;	
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pertanian_lahan extends CI_Model {

#---------------------------------------------------------------Admin_pertanian_lahan---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("pertanian_lahan")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pertanian_lahan",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pertanian_lahan", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pertanian_lahan", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pertanian_lahan", $where);
		return $delete;
	}

}

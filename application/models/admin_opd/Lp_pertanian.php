<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_pertanian extends CI_Model
{

//====================================================== LP APARAT =======================================================//
	public function get_pertanian(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_pertanian_sub_jenis lpj', 'lp.id_sub_jenis = lpj.id_sub_jenis');
		$data = $this->db->get("lp_pertanian lp")->result();
		return $data;


		//$this->db->join('lp_pertanian_jenis lpp', 'lpa.id_jenis = lpp.id_jenis');
		//$data = $this->db->get("lp_pertanian_sub_jenis lpa")->result();
		//return $data;
		
	}

	public function get_pertanian_where($where){
		$data = $this->db->get_where("lp_pertanian",$where)->row_array();
		return $data;
	}	

	public function insert_pertanian($data){
		$insert = $this->db->insert("lp_pertanian", $data);
		return $insert;
	}

	public function update_pertanian($set, $where){
		$update = $this->db->update("lp_pertanian", $set, $where);
		return $update;
	}		

	public function delete_pertanian($where){
		$delete = $this->db->delete("lp_pertanian", $where);
		return $delete;
	}
//====================================================== LP PERTANIAN =======================================================//				
//====================================================== LP PERTANIAN JENIS===========================================//
	public function get_pertanian_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_pertanian_jenis")->result();
		return $data;
	}	

	public function insert_pertanian_jenis($data){
		$insert = $this->db->insert("lp_pertanian_jenis", $data);
		return $insert;
	}	

	public function get_pertanian_jenis_where($where){
		$data = $this->db->get_where("lp_pertanian_jenis",$where)->row_array();
		return $data;
	}

	public function update_pertanian_jenis($set, $where){
		$update = $this->db->update("lp_pertanian_jenis", $set, $where);
		return $update;
	}		

	public function delete_pertanian_jenis($where){
		$delete = $this->db->delete("lp_pertanian_jenis", $where);
		return $delete;
	}		
//====================================================== LP PERTANIAN sSUB JENIS =======================================//	
	public function get_pertanian_sub_jenis(){
		// $this->db->where("is_delete !=", "1");

		$this->db->join('lp_pertanian_jenis lpp', 'lpa.id_jenis = lpp.id_jenis');
		$data = $this->db->get("lp_pertanian_sub_jenis lpa")->result();
		//$data = $this->db->get("lp_pertanian_sub_jenis")->result();
		return $data;
	}	

	public function insert_pertanian_sub_jenis($data){
		$insert = $this->db->insert("lp_pertanian_sub_jenis", $data);
		return $insert;
	}	

	public function get_pertanian_sub_jenis_where($where){
		$data = $this->db->get_where("lp_pertanian_sub_jenis",$where)->row_array();
		return $data;
	}

	public function update_pertanian_sub_jenis($set, $where){
		$update = $this->db->update("lp_pertanian_sub_jenis", $set, $where);
		return $update;
	}		

	public function delete_pertanian_sub_jenis($where){
		$delete = $this->db->delete("lp_pertanian_sub_jenis", $where);
		return $delete;
	}		
}
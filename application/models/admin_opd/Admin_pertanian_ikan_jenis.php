<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pertanian_ikan_jenis extends CI_Model {

#---------------------------------------------------------------Admin_pertanian_ikan_jenis---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("pertanian_ikan_jenis")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pertanian_ikan_jenis",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pertanian_ikan_jenis", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pertanian_ikan_jenis", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pertanian_ikan_jenis", $where);
		return $delete;
	}

}

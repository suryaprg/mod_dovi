<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pendidikan_penduduk_jenis extends CI_Model {

#---------------------------------------------------------------Admin_pendidikan_penduduk_jenis---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("pendidikan_penduduk_jenis")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pendidikan_penduduk_jenis",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pendidikan_penduduk_jenis", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pendidikan_penduduk_jenis", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pendidikan_penduduk_jenis", $where);
		return $delete;
	}

}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Disbudpar extends CI_Model
{
	
//====================================================== ZONA KREATIF ================================================//
	public function get_zona(){
		$data = $this->db->get("disbudpar_zona_kreatif")->result();
		return $data;
	}

	public function get_zona_where($where){
		$data = $this->db->get_where("disbudpar_zona_kreatif", $where)->row_array();
		return $data;
	}	

	public function insert_zona($data){
		$insert = $this->db->insert("disbudpar_zona_kreatif", $data);
		return $insert;
	}

	public function update_zona($set, $where){
		$update = $this->db->update("disbudpar_zona_kreatif", $set, $where);
		return $update;
	}		

	public function delete_zona($where){
		$delete = $this->db->delete("disbudpar_zona_kreatif", $where);
		return $delete;
	}
//====================================================== ZONA KREATIF ================================================//
}
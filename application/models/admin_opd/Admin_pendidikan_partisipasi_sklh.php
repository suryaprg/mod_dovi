<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pendidikan_partisipasi_sklh extends CI_Model {

#---------------------------------------------------------------Admin_pendidikan_partisipasi_sklh---------------------------------------------------------------------

	public function get(){
		$this->db->join("pendidikan_partisipasi_sklh_jenis ppsj", "pps.id_jenis = ppsj.id_jenis");
		$data = $this->db->get("pendidikan_partisipasi_sklh pps")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pendidikan_partisipasi_sklh",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pendidikan_partisipasi_sklh", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pendidikan_partisipasi_sklh", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pendidikan_partisipasi_sklh", $where);
		return $delete;
	}

}

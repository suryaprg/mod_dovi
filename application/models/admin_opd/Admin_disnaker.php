<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_disnaker extends CI_Model
{

//====================================================== LP disnaker =======================================================//
	public function get_disnaker(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('disnaker_jenis kj', 'kt.id_jenis = kj.id_jenis');
		$this->db->join('disnaker_sub_jenis ksj', 'kt.id_sub_jenis = ksj.id_sub_jenis');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("disnaker_main kt")->result();

		return $data;
	}

	public function get_disnaker_where($where){
		$data = $this->db->get_where("disnaker_main",$where)->row_array();
		return $data;
	}	

	public function insert_disnaker($data){
		$insert = $this->db->insert("disnaker_main", $data);
		return $insert;
	}

	public function update_disnaker($set, $where){
		$update = $this->db->update("disnaker_main", $set, $where);
		return $update;
	}		

	public function delete_disnaker($where){
		$delete = $this->db->delete("disnaker_main", $where);
		return $delete;
	}
//====================================================== LP disnaker =======================================================//		
//====================================================== LP disnaker Ktegori =======================================================//
	public function get_kategori(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('disnaker_jenis kj', 'kt.id_jenis = kj.id_jenis');
		$this->db->join('disnaker_sub_jenis ksj', 'kt.id_sub_jenis = ksj.id_sub_jenis');
	//	$this->db->order_by('jml', 'desc');
		$data = $this->db->get("disnaker_kategori kt")->result();
		return $data;
	}

	public function get_disnaker_kategori_where($where){
		$data = $this->db->get_where("disnaker_kategori",$where)->row_array();
		return $data;
	}	

	public function insert_disnaker_kategori($data){
		$insert = $this->db->insert("disnaker_kategori", $data);
		return $insert;
	}

	public function update_disnaker_kategori($set, $where){
		$update = $this->db->update("disnaker_kategori", $set, $where);
		return $update;
	}		

	public function delete_disnaker_kategori($where){
		$delete = $this->db->delete("disnaker_kategori", $where);
		return $delete;
	}
//====================================================== LP disnaker =======================================================//		
//====================================================== LP disnaker Jenis =====================================================//
	public function get_disnaker_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("disnaker_jenis ")->result();
		return $data;
	}	

	public function insert_disnaker_jenis($data){
		$insert = $this->db->insert("disnaker_jenis", $data);
		return $insert;
	}	

	public function get_disnaker_jenis_where($where){
		$data = $this->db->get_where("disnaker_jenis",$where)->row_array();
		return $data;
	}

	public function update_disnaker_jenis($set, $where){
		$update = $this->db->update("disnaker_jenis", $set, $where);
		return $update;
	}		

	public function delete_disnaker_jenis($where){
		$delete = $this->db->delete("disnaker_jenis", $where);
		return $delete;
	}		
//====================================================== LP disnaker Jenis =====================================================//	

//====================================================== LP disnaker Sub Jenis =====================================================//
	public function get_disnaker_sub_jenis(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('disnaker_jenis tj','kts.id_jenis = tj.id_jenis');
		$data = $this->db->get("disnaker_sub_jenis kts ")->result();
		return $data;
	}	

	public function insert_disnaker_sub_jenis($data){
		$insert = $this->db->insert("disnaker_sub_jenis", $data);
		return $insert;
	}	

	public function get_disnaker_sub_jenis_where($where){
		$data = $this->db->get_where("disnaker_sub_jenis",$where)->row_array();
		return $data;
	}

	public function update_disnaker_sub_jenis($set, $where){
		$update = $this->db->update("disnaker_sub_jenis", $set, $where);
		return $update;
	}		

	public function delete_disnaker_sub_jenis($where){
		$delete = $this->db->delete("disnaker_sub_jenis", $where);
		return $delete;
	}		
//====================================================== LP disnaker Sub Jenis =====================================================//		
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_kependudukan_umur extends CI_Model {

#---------------------------------------------------------------kependudukan_umur---------------------------------------------------------------------

	public function get_kependudukan(){
		$data = $this->db->get("kependudukan_kel_umur")->result();
		return $data;
	}

	public function get_kependudukan_where($where){
		$data = $this->db->get_where("kependudukan_kel_umur",$where);
		return $data;
	}

	public function insert_kependudukan($data){
		$insert = $this->db->insert("kependudukan_kel_umur", $data);
		return $insert;
	}

	public function update_kependudukan($set, $where){
		$update = $this->db->update("kependudukan_kel_umur", $set, $where);
		return $update;
	}

	public function delete_kependudukan($where){
		$delete = $this->db->delete("kependudukan_kel_umur", $where);
		return $delete;
	}

}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_panen extends CI_Model
{

//====================================================== LP panen =======================================================//
	public function get_panen(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_panen_jenis lj','ll.id_jenis = lj.id_jenis');
		$this->db->join('main_kecamatan mk','ll.id_kec = mk.id_kec');
		$data = $this->db->get("lp_panen ll")->result();
		return $data;
	}

	public function get_panen_where($where){
		$data = $this->db->get_where("lp_panen",$where)->row_array();
		return $data;
	}	
	public function insert_panen($data){
		$insert = $this->db->insert("lp_panen", $data);
		return $insert;
	}

	public function update_panen($set, $where){
		$update = $this->db->update("lp_panen", $set, $where);
		return $update;
	}		

	public function delete_panen($where){
		$delete = $this->db->delete("lp_panen", $where);
		return $delete;
	}
//====================================================== LP panen =======================================================//				
//====================================================== LP panen JENIS =====================================================//
	public function get_panen_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_panen_jenis")->result();
		return $data;
	}	

	public function insert_panen_jenis($data){
		$insert = $this->db->insert("lp_panen_jenis", $data);
		return $insert;
	}	

	public function get_panen_jenis_where($where){
		$data = $this->db->get_where("lp_panen_jenis",$where)->row_array();
		return $data;
	}

	public function update_panen_jenis($set, $where){
		$update = $this->db->update("lp_panen_jenis", $set, $where);
		return $update;
	}		

	public function delete_panen_jenis($where){
		$delete = $this->db->delete("lp_panen_jenis", $where);
		return $delete;
	}		
//====================================================== LP panen JENIS =====================================================//	
public function get_main_kecamatan(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("main_kecamatan")->result();
		return $data;
	}	
}
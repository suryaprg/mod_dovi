<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_aparat extends CI_Model
{

//====================================================== LP APARAT =======================================================//
	public function get_aparat(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_aparat_jenis aj', 'ap.id_jenis = aj.id_jenis');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("lp_aparat ap")->result();
		// $data
		return $data;
	}

	// public function get_aparat2(){
	// 	// $this->db->where("is_delete !=", "1");
	// 	$this->db->join('lp_aparat_jenis aj', 'ap.id_jenis = aj.id_jenis');
	// 	$this->db->order_by('th', 'desc');
	// 	$data = $this->db->get("lp_aparat ap")->result();
	// 	// $data
	// 	return $data;
	// }	

	public function get_aparat_where($where){
		$data = $this->db->get_where("lp_aparat",$where)->row_array();
		return $data;
	}	

	public function insert_aparat($data){
		$insert = $this->db->insert("lp_aparat", $data);
		return $insert;
	}

	public function update_aparat($set, $where){
		$update = $this->db->update("lp_aparat", $set, $where);
		return $update;
	}		

	public function delete_aparat($where){
		$delete = $this->db->delete("lp_aparat", $where);
		return $delete;
	}
//====================================================== LP APARAT =======================================================//				
//====================================================== LP APARAT JENIS =====================================================//
	public function get_aparat_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_aparat_jenis")->result();
		return $data;
	}	

	public function insert_aparat_jenis($data){
		$insert = $this->db->insert("lp_aparat_jenis", $data);
		return $insert;
	}	

	public function get_aparat_jenis_where($where){
		$data = $this->db->get_where("lp_aparat_jenis",$where)->row_array();
		return $data;
	}

	public function update_aparat_jenis($set, $where){
		$update = $this->db->update("lp_aparat_jenis", $set, $where);
		return $update;
	}		

	public function delete_aparat_jenis($where){
		$delete = $this->db->delete("lp_aparat_jenis", $where);
		return $delete;
	}		
//====================================================== LP APARAT JENIS =====================================================//	
}
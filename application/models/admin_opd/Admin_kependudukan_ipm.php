<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_kependudukan_ipm extends CI_Model {

#---------------------------------------------------------------kependudukan_jk---------------------------------------------------------------------

	public function get_kependudukan(){
		$data = $this->db->get("index_pem_manusia")->result();
		return $data;
	}

	public function get_kependudukan_where($where){
		$data = $this->db->get_where("index_pem_manusia",$where);
		return $data;
	}

	public function insert_kependudukan($data){
		$insert = $this->db->insert("index_pem_manusia", $data);
		return $insert;
	}

	public function update_kependudukan($set, $where){
		$update = $this->db->update("index_pem_manusia", $set, $where);
		return $update;
	}

	public function delete_kependudukan($where){
		$delete = $this->db->delete("index_pem_manusia", $where);
		return $delete;
	}

}

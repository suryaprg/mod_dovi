<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_gizi extends CI_Model {

#---------------------------------------------------------------Admin_gizi---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("kesehatan_gizi_balita")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("kesehatan_gizi_balita",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("kesehatan_gizi_balita", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("kesehatan_gizi_balita", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("kesehatan_gizi_balita", $where);
		return $delete;
	}

}

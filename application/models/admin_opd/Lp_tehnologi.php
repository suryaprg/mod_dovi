<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_tehnologi extends CI_Model
{

//====================================================== LP APARAT =======================================================//
	public function get_tehnologi(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_tehnologi_sub_jenis lpj', 'lp.id_sub_jenis = lpj.id_sub_jenis');
		$data = $this->db->get("lp_tehnologi lp")->result();
		return $data;


		//$this->db->join('lp_tehnologi_jenis lpp', 'lpa.id_jenis = lpp.id_jenis');
		//$data = $this->db->get("lp_tehnologi_sub_jenis lpa")->result();
		//return $data;
		
	}

	public function get_tehnologi_where($where){
		$data = $this->db->get_where("lp_tehnologi",$where)->row_array();
		return $data;
	}	

	public function insert_tehnologi($data){
		$insert = $this->db->insert("lp_tehnologi", $data);
		return $insert;
	}

	public function update_tehnologi($set, $where){
		$update = $this->db->update("lp_tehnologi", $set, $where);
		return $update;
	}		

	public function delete_tehnologi($where){
		$delete = $this->db->delete("lp_tehnologi", $where);
		return $delete;
	}
//====================================================== LP PERTANIAN =======================================================//				
//====================================================== LP PERTANIAN JENIS===========================================//
	public function get_tehnologi_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_tehnologi_jenis")->result();
		return $data;
	}	

	public function insert_tehnologi_jenis($data){
		$insert = $this->db->insert("lp_tehnologi_jenis", $data);
		return $insert;
	}	

	public function get_tehnologi_jenis_where($where){
		$data = $this->db->get_where("lp_tehnologi_jenis",$where)->row_array();
		return $data;
	}

	public function update_tehnologi_jenis($set, $where){
		$update = $this->db->update("lp_tehnologi_jenis", $set, $where);
		return $update;
	}		

	public function delete_tehnologi_jenis($where){
		$delete = $this->db->delete("lp_tehnologi_jenis", $where);
		return $delete;
	}		
//====================================================== LP PERTANIAN sSUB JENIS =======================================//	
	public function get_tehnologi_sub_jenis(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_tehnologi_jenis lpp', 'lpa.id_jenis = lpp.id_jenis');
		$data = $this->db->get("lp_tehnologi_sub_jenis lpa")->result();
		//$data = $this->db->get("lp_tehnologi_sub_jenis")->result();
		return $data;
	}	

	public function insert_tehnologi_sub_jenis($data){
		$insert = $this->db->insert("lp_tehnologi_sub_jenis", $data);
		return $insert;
	}	

	public function get_tehnologi_sub_jenis_where($where){
		$data = $this->db->get_where("lp_tehnologi_sub_jenis",$where)->row_array();
		return $data;
	}

	public function update_tehnologi_sub_jenis($set, $where){
		$update = $this->db->update("lp_tehnologi_sub_jenis", $set, $where);
		return $update;
	}		

	public function delete_tehnologi_sub_jenis($where){
		$delete = $this->db->delete("lp_tehnologi_sub_jenis", $where);
		return $delete;
	}		
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lppdb extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("admin_opd/Lp_pdb", "pd");
		$this->load->library("response_message");
	}

	public function index(){
		$data["page"] 				= "pdb";
		$data["list_pdb"] 			= $this->pd->get_pdb();
		$data["list_pdb_jenis"] 	= $this->pd->get_pdb_jenis();
		$data["list_pdb_kategori"] 	= $this->pd->get_pdb_kategori();
		$this->load->view('admin_data/geo/main_admin_geo', $data);
	}

//=============================================== PDB =======================================================//
	private function validation_ins_lp_pdb(){
		$config_val_input = array(
            array(
                'field'=>'id_jenis',
                'label'=>'Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'id_kategori',
                'label'=>'Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	
                 )
                       
            ),

            array(
                'field'=>'th',
                'label'=>'Tahun',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),

            array(
                'field'=>'jml',
                'label'=>'Jumlah',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")

                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pdb(){
		if($this->validation_ins_lp_pdb()){
			$id_jenis 		= $this->input->post("id_jenis");
			$id_kategori 	= $this->input->post("id_kategori");
			$th 			= $this->input->post("th");
			$jml 			= $this->input->post("jml");
			
			$data_insert = array(
							"id_pdb"=>"",
							"id_jenis"=>$id_jenis,
							"id_kategori"=>$id_kategori,
							"th"=>$th,
							"jml"=>$jml
						);
			$insert = $this->pd->insert_pdb($data_insert);
				if($insert){
					redirect('admin/pdb');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}	

	public function index_up_lp_pdb(){
		$id_pdb = $this->input->post("id_pdb");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->pd->get_pdb_where(array("id_pdb"=>$id_pdb)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pdb(){
		if($this->validation_ins_lp_pdb()){
			$id_pdb = $this->input->post("id_pdb");

			$id_jenis = $this->input->post("id_jenis");
			$id_kategori = $this->input->post("id_kategori");
			$th = $this->input->post("th");
			$jml = $this->input->post("jml");

			$data = array(
						
						"id_jenis"=>$id_jenis,
						"id_kategori"=>$id_kategori,
						"th"=>$th,
						"jml"=>$jml
					);

			$data_where = array(
							"id_pdb"=>$id_pdb,
						);

			$insert = $this->pd->update_pdb($data, $data_where);
				if($insert){
					redirect('admin/pdb');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_pdb($id_pdb){
		$delete = $this->pd->delete_pdb(array("id_pdb"=>$id_pdb));
		if($delete){
			echo "del";
			redirect('admin/pdb');
		}else {
			echo "fail";
		}
	}	
//=============================================== PDB =======================================================//

//=============================================== PDB JENIS =======================================================//
	private function validation_ins_lp_pdb_jenis(){
		$config_val_input = array(

            array(
                'field'=>'nama_jenis',
                'label'=>'Nama Jenis',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")

                 )       
            ),

            array(
                'field'=>'satuan',
                'label'=>'Satuan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")

                 )       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pdb_jenis(){
		if($this->validation_ins_lp_pdb_jenis()){
			$nama_jenis = $this->input->post("nama_jenis");
			$satuan 	= $this->input->post("satuan");
			
			$data_insert = array(
							"id_jenis"=>"",
							"nama_jenis"=>$nama_jenis,
							"satuan"=>$satuan
						);
			$insert = $this->pd->insert_pdb_jenis($data_insert);
				if($insert){
					redirect('admin/pdb');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pdb_jenis(){
		$id_jenis = $this->input->post("id_jenis");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->pd->get_pdb_jenis_where(array("id_jenis"=>$id_jenis)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pdb_jenis(){
		if($this->validation_ins_lp_pdb_jenis()){
			$id_jenis = $this->input->post("id_jenis");

			$nama_jenis = $this->input->post("nama_jenis");
			$satuan = $this->input->post("satuan");

			$data = array(
						
						"nama_jenis"=>$nama_jenis,
						"satuan"=>$satuan
					);

			$data_where = array(
							"id_jenis"=>$id_jenis,
						);

			$insert = $this->pd->update_pdb_jenis($data, $data_where);
				if($insert){
					redirect('admin/pdb');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_pdb_jenis($id_jenis){
		$delete = $this->pd->delete_pdb_jenis(array("id_jenis"=>$id_jenis));
		if($delete){
			echo "del";
			redirect('admin/pdb');
		}else {
			echo "fail";
		}
	}	
//=============================================== PDB JENIS =======================================================//

//=============================================== PDB KATEGORI =======================================================//
	private function validation_ins_lp_pdb_kategori(){
		$config_val_input = array(
          
            array(
                'field'=>'nama_kategori',
                'label'=>'Nama Kategori',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                	// 'number'=>"%s ".$this->response_message->get_error_msg("NUMBER")

                 )
                       
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_lp_pdb_kategori(){
		if($this->validation_ins_lp_pdb_kategori()){
		
			$nama_kategori = $this->input->post("nama_kategori");
			
			$data_insert = array(
							"id_kategori"=>"",
							"nama_kategori"=>$nama_kategori
						);
			$insert = $this->pd->insert_pdb_kategori($data_insert);
				if($insert){
					redirect('admin/pdb');
					echo "yes";
				}else {
					echo "no";
				}
			
		}else {
			print_r(validation_errors());
		}
	}

	public function index_up_lp_pdb_kategori(){
		$id_kategori = $this->input->post("id_kategori");
		$data["status"] = false;
		$data["val"] = null;
		
		$data_main =$this->pd->get_pdb_kategori_where(array("id_kategori"=>$id_kategori)); 
		if(!empty($data_main)){
			$data["status"] = true;
			$data["val"] = $data_main;
		}

		print_r(json_encode($data));
	}

	public function update_lp_pdb_kategori(){
		if($this->validation_ins_lp_pdb_kategori()){
			$id_kategori = $this->input->post("id_kategori");

			$nama_kategori = $this->input->post("nama_kategori");

			$data = array(
						
						"nama_kategori"=>$nama_kategori
					);

			$data_where = array(
							"id_kategori"=>$id_kategori,
						);

			$insert = $this->pd->update_pdb_kategori($data, $data_where);
				if($insert){
					redirect('admin/pdb');
					echo "yes";
				}else {
					echo "no";
				}
						

		}else {
			print_r(validation_errors());
		}

	}

	public function delete_pdb_kategori($id_kategori){
		$delete = $this->pd->delete_pdb_kategori(array("id_kategori"=>$id_kategori));
		if($delete){
			echo "del";
			redirect('admin/pdb');
		}else {
			echo "fail";
		}
	}	
//=============================================== PDB KATEGORI =======================================================//			
}
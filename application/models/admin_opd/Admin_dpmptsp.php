<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_dpmptsp extends CI_Model
{

//====================================================== LP dpmptsp =======================================================//
	public function get_dpmptsp(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('dpmptsp_jenis kj', 'kt.id_jenis = kj.id_jenis');
		$this->db->order_by('th', 'asc');
		$data = $this->db->get("dpmptsp_main kt")->result();

		return $data;
	}

	public function get_dpmptsp_where($where){
		$data = $this->db->get_where("dpmptsp_main",$where)->row_array();
		return $data;
	}	

	public function insert_dpmptsp($data){
		$insert = $this->db->insert("dpmptsp_main", $data);
		return $insert;
	}

	public function update_dpmptsp($set, $where){
		$update = $this->db->update("dpmptsp_main", $set, $where);
		return $update;
	}		

	public function delete_dpmptsp($where){
		$delete = $this->db->delete("dpmptsp_main", $where);
		return $delete;
	}
//====================================================== LP dpmptsp =======================================================//		
		
//====================================================== LP dpmptsp Jenis =====================================================//
	public function get_dpmptsp_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("dpmptsp_jenis ")->result();
		return $data;
	}	

	public function insert_dpmptsp_jenis($data){
		$insert = $this->db->insert("dpmptsp_jenis", $data);
		return $insert;
	}	

	public function get_dpmptsp_jenis_where($where){
		$data = $this->db->get_where("dpmptsp_jenis",$where)->row_array();
		return $data;
	}

	public function update_dpmptsp_jenis($set, $where){
		$update = $this->db->update("dpmptsp_jenis", $set, $where);
		return $update;
	}		

	public function delete_dpmptsp_jenis($where){
		$delete = $this->db->delete("dpmptsp_jenis", $where);
		return $delete;
	}		
//====================================================== LP dpmptsp Jenis =====================================================//	

}

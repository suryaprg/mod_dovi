<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pendidikan_partisipasi_sklh_jenis extends CI_Model {

#---------------------------------------------------------------Admin_pendidikan_partisipasi_sklh_jenis---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("pendidikan_partisipasi_sklh_jenis")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pendidikan_partisipasi_sklh_jenis",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pendidikan_partisipasi_sklh_jenis", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pendidikan_partisipasi_sklh_jenis", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pendidikan_partisipasi_sklh_jenis", $where);
		return $delete;
	}

}

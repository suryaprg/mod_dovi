<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_koperasi extends CI_Model
{

//=================================================== LP KOPERASI =================================================//
	public function get_koperasi(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_kooperasi_jenis kj', 'ko.id_jenis = kj.id_jenis');
		$this->db->join('lp_kooperasi_sub_jenis ksj', 'ko.id_sub_jenis = ksj.id_sub_jenis');
		$this->db->order_by('th', 'desc');
		$data = $this->db->get("Lp_koperasi ko")->result();
		return $data;
	}

	public function get_koperasi_where($where){
		$data = $this->db->get_where("lp_koperasi",$where)->row_array();
		return $data;
	}	

	public function insert_koperasi($data){
		$insert = $this->db->insert("lp_koperasi", $data);
		return $insert;
	}

	public function update_koperasi($set, $where){
		$update = $this->db->update("lp_koperasi", $set, $where);
		return $update;
	}		

	public function delete_koperasi($where){
		$delete = $this->db->delete("lp_koperasi", $where);
		return $delete;
	}
//=================================================== LP KOPERASI =================================================//				
//=================================================== LP KOPERASI JENIS ===========================================//
	public function get_koperasi_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_kooperasi_jenis ")->result();
		return $data;
	}	

	public function insert_koperasi_jenis($data){
		$insert = $this->db->insert("lp_kooperasi_jenis", $data);
		return $insert;
	}	

	public function get_koperasi_jenis_where($where){
		$data = $this->db->get_where("lp_kooperasi_jenis",$where)->row_array();
		return $data;
	}

	public function update_koperasi_jenis($set, $where){
		$update = $this->db->update("lp_kooperasi_jenis", $set, $where);
		return $update;
	}		

	public function delete_koperasi_jenis($where){
		$delete = $this->db->delete("lp_kooperasi_jenis", $where);
		return $delete;
	}		
//=================================================== LP KOPERASI JENSI ===========================================//	

//=================================================== LP KOPERASI SUB JENIS =======================================//
	public function get_koperasi_sub_jenis(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_kooperasi_jenis kj','ksj.id_jenis = kj.id_jenis');
		$data = $this->db->get("lp_kooperasi_sub_jenis ksj")->result();
		return $data;
	}	

	public function insert_koperasi_sub_jenis($data){
		$insert = $this->db->insert("lp_kooperasi_sub_jenis", $data);
		return $insert;
	}	

	public function get_koperasi_sub_jenis_where($where){
		$data = $this->db->get_where("lp_kooperasi_sub_jenis",$where)->row_array();
		return $data;
	}

	public function update_koperasi_sub_jenis($set, $where){
		$update = $this->db->update("lp_kooperasi_sub_jenis", $set, $where);
		return $update;
	}		

	public function delete_koperasi_sub_jenis($where){
		$delete = $this->db->delete("lp_kooperasi_sub_jenis", $where);
		return $delete;
	}		
//=================================================== LP KOPERASI SUB JENIS =======================================//		
}

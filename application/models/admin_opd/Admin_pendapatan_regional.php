<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pendapatan_regional extends CI_Model {

#---------------------------------------------------------------Admin_pendapatan_regional---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("pendapatan_regional")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pendapatan_regional",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pendapatan_regional", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pendapatan_regional", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pendapatan_regional", $where);
		return $delete;
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_domain extends CI_Model {

#---------------------------------------------------------------stasiun---------------------------------------------------------------------

	public function get_domain(){
		$data = $this->db->get("kominfo_domain")->result();
		return $data;
	}

	public function get_domain_where($where){
		$data = $this->db->get_where("kominfo_domain",$where);
		return $data;
	}

	public function insert_domain($data){
		$insert = $this->db->insert("kominfo_domain", $data);
		return $insert;
	}

	public function update_domain($set, $where){
		$update = $this->db->update("kominfo_domain", $set, $where);
		return $update;
	}

	public function delete_domain($where){
		$delete = $this->db->delete("kominfo_domain", $where);
		return $delete;
	}

}

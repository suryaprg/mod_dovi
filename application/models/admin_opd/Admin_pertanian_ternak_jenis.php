<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pertanian_ternak_jenis extends CI_Model {

#---------------------------------------------------------------Admin_pertanian_ternak_jenis---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("pertanian_ternak_jenis")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pertanian_ternak_jenis",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pertanian_ternak_jenis", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pertanian_ternak_jenis", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pertanian_ternak_jenis", $where);
		return $delete;
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_tehnologi_warnet extends CI_Model {

#---------------------------------------------------------------Admin_tehnologi_warnet---------------------------------------------------------------------

	public function get_tehnologi(){
		$data = $this->db->get("tehnologi_jml_warnet")->result();
		return $data;
	}

	public function get_tehnologi_where($where){
		$data = $this->db->get_where("tehnologi_jml_warnet",$where);
		return $data;
	}

	public function insert_tehnologi($data){
		$insert = $this->db->insert("tehnologi_jml_warnet", $data);
		return $insert;
	}

	public function update_tehnologi($set, $where){
		$update = $this->db->update("tehnologi_jml_warnet", $set, $where);
		return $update;
	}

	public function delete_tehnologi($where){
		$delete = $this->db->delete("tehnologi_jml_warnet", $where);
		return $delete;
	}

}

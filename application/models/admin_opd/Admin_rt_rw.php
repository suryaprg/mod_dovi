<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_rt_rw extends CI_Model {

#---------------------------------------------------------------stasiun---------------------------------------------------------------------

	public function get_rt_rw(){
		$this->db->join("main_kecamatan mk", "rtw.id_kec=mk.id_kec");
		$data = $this->db->get("pem_jml_rt_rw rtw")->result();
		return $data;
	}

	public function get_rt_rw_where($where){
		$data = $this->db->get_where("pem_jml_rt_rw",$where);
		return $data;
	}

	public function insert_rt_rw($data){
		$insert = $this->db->insert("pem_jml_rt_rw", $data);
		return $insert;
	}

	public function update_rt_rw($set, $where){
		$update = $this->db->update("pem_jml_rt_rw", $set, $where);
		return $update;
	}

	public function delete_rt_rw($where){
		$delete = $this->db->delete("pem_jml_rt_rw", $where);
		return $delete;
	}

}

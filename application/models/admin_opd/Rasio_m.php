<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Rasio_m extends CI_Model
{
	
//====================================================== RASIO ================================================//
	public function get_rasio(){
		$this->db->join('main_kecamatan mk', 'lp.id_kec = mk.id_kec');
		$data = $this->db->get("lp_penduduk_jk lp")->result();
		return $data;
	}

	public function get_rasio_where($where){
		$data = $this->db->get_where("lp_penduduk_jk", $where)->row_array();
		return $data;
	}	

	public function insert_rasio($data){
		$insert = $this->db->insert("lp_penduduk_jk", $data);
		return $insert;
	}

	public function update_rasio($set, $where){
		$update = $this->db->update("lp_penduduk_jk", $set, $where);
		return $update;
	}		

	public function delete_rasio($where){
		$delete = $this->db->delete("lp_penduduk_jk", $where);
		return $delete;
	}

	public function get_kecamatan(){
		$data = $this->db->get("main_kecamatan")->result();
		return $data;
	}
//====================================================== RASIO ================================================//
}
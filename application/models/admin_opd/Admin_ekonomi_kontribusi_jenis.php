<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_ekonomi_kontribusi_jenis extends CI_Model {

#---------------------------------------------------------------Admin_ekonomi_kontribusi_jenis---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("pendapatan_lap_usaha_jenis")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pendapatan_lap_usaha_jenis",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pendapatan_lap_usaha_jenis", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pendapatan_lap_usaha_jenis", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pendapatan_lap_usaha_jenis", $where);
		return $delete;
	}

}

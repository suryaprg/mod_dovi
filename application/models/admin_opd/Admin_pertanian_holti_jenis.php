<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pertanian_holti_jenis extends CI_Model {

#---------------------------------------------------------------Admin_pertanian_holti_jenis---------------------------------------------------------------------

	public function get(){
		$data = $this->db->get("pertanian_holti_jenis")->result();
		return $data;
	}

	public function get_where($where){
		$data = $this->db->get_where("pertanian_holti_jenis",$where);
		return $data;
	}

	public function insert($data){
		$insert = $this->db->insert("pertanian_holti_jenis", $data);
		return $insert;
	}

	public function update($set, $where){
		$update = $this->db->update("pertanian_holti_jenis", $set, $where);
		return $update;
	}

	public function delete($where){
		$delete = $this->db->delete("pertanian_holti_jenis", $where);
		return $delete;
	}

}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_dpmptsp extends CI_Model{
	
    public function get_lp_jenis(){
        $data = $this->db->get("dpmptsp_jenis")->result();
        return $data;
    }

    public function get_lp($where){
    	$this->db->join("dpmptsp_jenis lp", "lpt.id_jenis = lp.id_jenis");
        $data = $this->db->get_where("dpmptsp_main lpt", $where)->row_array();
        return $data;
    }
    
}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_aparat extends CI_Model{
	
    public function get_lp_aparat(){
        $data = $this->db->get("lp_aparat_jenis")->result();
        return $data;
    }

    public function get_lp($where){
    	$this->db->join("lp_aparat_jenis lp", "lpt.id_jenis = lp.id_jenis
            ");
        $data = $this->db->get_where("lp_aparat lpt", $where)->row_array();
        return $data;
    }
    
}
?>
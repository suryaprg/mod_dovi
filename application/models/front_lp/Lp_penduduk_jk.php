<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_penduduk_jk extends CI_Model{
    
    public function get_kec(){
        $data = $this->db->get("main_kecamatan")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_penduduk_jk", $where)->row_array();
        return $data;
    }
    
}
?>
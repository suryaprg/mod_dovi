<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_kesehatan_all extends CI_Model{

    public function get_lp_jenis(){
        $data = $this->db->get("lp_kesehatan_jenis")->result();
        return $data;
    }

    public function get_lp_jenis_filter($where){
        $data = $this->db->get_where("lp_kesehatan_jenis", $where)->result();
        return $data;
    }

    public function get_lp_kategori(){
        $data = $this->db->get("lp_kesehatan_kategori")->result();
        return $data;
    }

     public function get_lp_kategori_filter($where){
        $data = $this->db->get_where("lp_kesehatan_kategori", $where)->result();
        return $data;
    }

    public function get_lp_sub($where){
        $data = $this->db->get_where("lp_kesehatan_sub_jenis", $where)->result();
        return $data;
    }

    public function get_lp_sub3($where){
        $data = $this->db->get_where("lp_kesehatan_sub3_jenis", $where)->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_kesehatan", $where)->row_array();
        return $data;
    }
    
}
?>
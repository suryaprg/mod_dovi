<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_disperkim_taman extends CI_Model{
    
    public function get_lp_jenis(){
        $data = $this->db->get("disperkim_taman_jenis")->result();
        return $data;
    }

    public function get_lp($where){
        // $this->db->order_by("tgl","asc");
        $this->db->join("disperkim_taman_jenis lpj", "lp.id_jenis = lpj.id_jenis");
        $data = $this->db->get_where("disperkim_taman lp", $where)->row_array();

        return $data;
    }
    
}
?>
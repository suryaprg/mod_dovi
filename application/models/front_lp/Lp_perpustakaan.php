<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_perpustakaan extends CI_Model{
	
    public function get_lp_perpustakaan_jenis(){
        $data = $this->db->get("lp_perpustakaan_jenis")->result();
        return $data;
    }

    public function get_lp_perpustakaan_jenis_where($where){
        $data = $this->db->get_where("lp_perpustakaan_jenis", $where)->result();
        return $data;
    }

    public function get_lp_perpustakaan($where){
    	$this->db->join("lp_perpustakaan_jenis lpj", "lp.id_jenis = lpj.id_jenis");
        $data = $this->db->get_where("lp_perpustakaan lp", $where)->row_array();
        return $data;
    }
    
}
?>
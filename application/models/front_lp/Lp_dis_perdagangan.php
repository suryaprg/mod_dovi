<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_dis_perdagangan extends CI_Model{
    
    public function get_lp_jenis(){
        $data = $this->db->get("dinas_perdagangan_jenis")->result();
        return $data;
    }

    public function get_lp_kategori($where){
        $data = $this->db->get_where("dinas_perdagangan_sub_jenis", $where)->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("dinas_perdagangan", $where)->row_array();
        return $data;
    }
    
}
?>
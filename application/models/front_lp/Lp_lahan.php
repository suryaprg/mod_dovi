<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_lahan extends CI_Model{
    
    public function get_lp_jenis(){
        $data = $this->db->get("lp_lahan_jenis")->result();
        return $data;
    }

    public function get_kec(){
        $data = $this->db->get("main_kecamatan")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_lahan", $where)->row_array();
        return $data;
    }
    
}
?>
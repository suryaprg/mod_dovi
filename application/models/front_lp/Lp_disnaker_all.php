<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_disnaker_all extends CI_Model{
    
    public function get_lp_jenis($where){
        $data = $this->db->get_where("disnaker_jenis", $where)->result();
        return $data;
    }

    public function get_lp_sub_jenis($where){
        $data = $this->db->get_where("disnaker_sub_jenis", $where)->result();
        return $data;
    }

    public function get_lp_kategori(){
        $data = $this->db->get("disnaker_kategori")->result();
        return $data;
    }

    public function get_lp_kategori_filter($where){
        $data = $this->db->get_where("disnaker_kategori",$where)->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("disnaker_main", $where)->row_array();
        return $data;
    }
    
}
?>
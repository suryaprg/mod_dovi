<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_tehnologi extends CI_Model{
    
    public function get_lp_jenis(){
        $data = $this->db->get("lp_tehnologi_jenis")->result();
        return $data;
    }

    public function get_lp_sub(){
        $data = $this->db->get("lp_tehnologi_sub_jenis")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_tehnologi", $where)->row_array();
        return $data;
    }

        public function get_aplikasi(){
        $data = $this->db->get("kominfo_aplikasi")->result();
        return $data;
    }

    public function get_domain(){
        $data = $this->db->get("kominfo_domain")->result();
        return $data;   
    }

    
}
?>
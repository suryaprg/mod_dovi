<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_pajak extends CI_Model{
	
    public function get_lp_pajak(){
        $data = $this->db->get("lp_pajak_jenis")->result();
        return $data;
    }

    public function get_lp($where){
    	$this->db->join("lp_pajak_jenis lp", "lpt.id_jenis = lp.id_jenis
            ");
        $data = $this->db->get_where("lp_pajak lpt", $where)->row_array();
        return $data;
    }
    
}
?>
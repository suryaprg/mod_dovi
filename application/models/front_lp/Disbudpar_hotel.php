<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Disbudpar_hotel extends CI_Model{
    
    public function get_lp_jenis(){
        $this->db->join("disbudpar_kunjungan_hotel_kate lk", "lj.id_kategori = lk.id_kategori");
        $data = $this->db->get("disbudpar_kunjungan_hotel_jenis lj")->result();
        return $data;
    }

    public function get_lp_kategori(){
        $data = $this->db->get("disbudpar_kunjungan_hotel_kate")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("disbudpar_kunjungan_hotel_main", $where)->row_array();
        return $data;
    }
    
}
?>
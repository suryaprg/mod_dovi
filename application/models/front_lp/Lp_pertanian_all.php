<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_pertanian_all extends CI_Model{

    public function get_lp_jenis(){
        $data = $this->db->get("lp_pertanian_jenis")->result();
        return $data;
    }

    public function get_lp_jenis_filter($where){
        $data = $this->db->get_where("lp_pertanian_jenis", $where)->result();
        return $data;
    }

    public function get_lp_sub($where){
        $data = $this->db->get_where("lp_pertanian_sub_jenis", $where)->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_pertanian", $where)->row_array();
        return $data;
    }
    
}
?>
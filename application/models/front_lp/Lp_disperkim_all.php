<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_disperkim_all extends CI_Model{
    
    public function get_lp_jenis($where){
        $data = $this->db->get_where("disperkim_all_jenis", $where)->result();
        return $data;
    }

    public function get_lp_sub_jenis($where){
        $data = $this->db->get_where("disperkim_all_sub_jenis", $where)->result();
        return $data;
    }

    public function get_lp_kategori_filter($where){
        $data = $this->db->get_where("disnaker_kategori",$where)->result();
        return $data;
    }    

    public function get_lp_kategori(){
        $data = $this->db->get("disperkim_all_kategori")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("disperkim_all", $where)->row_array();
        return $data;
    }
    
}
?>
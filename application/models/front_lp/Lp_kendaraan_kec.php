<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_kendaraan_kec extends CI_Model{
    
    public function get_lp_jenis(){
        $data = $this->db->get("main_jenis_kendaraan")->result();
        return $data;
    }

    public function get_kec(){
        $data = $this->db->get("main_kecamatan")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_kendaran_kec", $where)->row_array();
        return $data;
    }
    
}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_pendidikan_all extends CI_Model{

    public function get_kec(){
        $data = $this->db->get("main_kecamatan")->result();
        return $data;
    }
    
    public function get_lp_jenis(){
        $data = $this->db->get("lp_pendidikan_jenis")->result();
        return $data;
    }

    public function get_lp_kategori(){
        $data = $this->db->get("lp_pendidikan_kategori")->result();
        return $data;
    }

    public function get_lp_kategori_filter($where){
        $data = $this->db->get_where("lp_pendidikan_kategori", $where)->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_pendidikan", $where)->row_array();
        return $data;
    }
    
}
?>
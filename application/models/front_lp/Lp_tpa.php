<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_tpa extends CI_Model{
	
    public function get_lp_kec(){
        $data = $this->db->get("main_kecamatan")->result();
        return $data;
    }

    public function get_lp($where){
    	$this->db->join("main_kecamatan mk", "lpt.id_kec = mk.id_kec");
        $data = $this->db->get_where("lp_tpa lpt", $where)->row_array();
        return $data;
    }
    
}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_bencana extends CI_Model{
	
    public function get_lp_kecamatan(){
        $data = $this->db->get("main_kecamatan")->result();
        return $data;
    }

    public function get_lp($where){
    	$this->db->join("main_kecamatan lp", "lpt.id_kec = lp.id_kec
            ");
        $data = $this->db->get_where("lp_bencana lpt", $where)->row_array();
        return $data;
    }
    
}
?>
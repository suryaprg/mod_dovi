<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_disperkim_pohon extends CI_Model{
    
    public function get_lp_jenis(){
        $data = $this->db->get("disperkim_penanaman_pohon_jenis")->result();
        return $data;
    }

    public function get_lp($th){
        $this->db->order_by("tgl","asc");
        $this->db->join("disperkim_penanaman_pohon_jenis lpj", "lp.id_jenis = lpj.id_jenis");
        $this->db->where("year(tgl)", $th);
        $data = $this->db->get("disperkim_penanaman_pohon lp")->result();

        return $data;
    }
    
}
?>
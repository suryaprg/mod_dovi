<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_penduduk_umur extends CI_Model{
	
    public function get_lp_jenis(){
        $data = $this->db->get("lp_kel_umur_jenis")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_kel_umur", $where)->row_array();
        return $data;
    }
    
}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_terminal extends CI_Model{
	
    public function get_lp_jenis(){
        $data = $this->db->get("lp_terminal_jenis")->result();
        return $data;
    }

    public function get_lp_jenis_where($where){
        $data = $this->db->get_where("lp_terminal_sub_jenis", $where)->result();
        return $data;
    }

    public function get_lp_sub_jenis($where){
    	$this->db->join("lp_terminal_jenis ltj", "ltsj.id_jenis = ltj.id_jenis");
        $data = $this->db->get_where("lp_terminal_sub_jenis ltsj", $where)->result();
        return $data;
    }

    public function get_lp($where){
    	// $this->db->join("lp_terminal_jenis ltj", "lt.id_jenis = ltj.id_jenis");
     //    $this->db->join("lp_terminal_sub_jenis ltsj", "lt.id_sub_jenis = ltsj.id_sub_jenis");
        $data = $this->db->get_where("lp_terminal lt", $where)->row_array();
        return $data;
    }
    
}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_disperkim extends CI_Model{
	
    public function get_dsu(){
        $data = $this->db->get("disperkim_dsu")->result();
        return $data;
    }

    public function get_csr(){
        $data = $this->db->get("disperkim_csr")->result();
        return $data;   
    }

}
?>
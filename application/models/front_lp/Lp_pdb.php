<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_pdb extends CI_Model{
    
    public function get_lp_jenis(){
        $data = $this->db->get("lp_pdb_jenis")->result();
        return $data;
    }

    public function get_lp_kategori(){
        $data = $this->db->get("lp_pdb_kategori")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_pdb", $where)->row_array();
        return $data;
    }
    
}
?>
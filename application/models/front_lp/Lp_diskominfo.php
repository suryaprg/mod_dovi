<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_diskominfo extends CI_Model{
	
    public function get_aplikasi(){
        $data = $this->db->get("kominfo_aplikasi")->result();
        return $data;
    }

    public function get_domain(){
        $data = $this->db->get("kominfo_domain")->result();
        return $data;   
    }


}
?>
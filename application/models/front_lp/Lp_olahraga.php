<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_olahraga extends CI_Model{
	
    public function get_lp_olahraga(){
        $data = $this->db->get("lp_or_jenis")->result();
        return $data;
    }

    public function get_lp($where){
    	$this->db->join("lp_or_jenis lp", "lpt.id_jenis = lp.id_jenis
            ");
        $data = $this->db->get_where("lp_or lpt", $where)->row_array();
        return $data;
    }
    
}
?>
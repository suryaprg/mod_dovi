<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_or extends CI_Model
{

//====================================================== LP or =======================================================//
	public function get_or(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_or_jenis loj','lo.id_jenis = loj.id_jenis');
		$data = $this->db->get("lp_or lo")->result();
		return $data;
	}

	public function get_or_where($where){
		$data = $this->db->get_where("lp_or",$where)->row_array();
		return $data;
	}	

	public function insert_or($data){
		$insert = $this->db->insert("lp_or", $data);
		return $insert;
	}

	public function update_or($set, $where){
		$update = $this->db->update("lp_or", $set, $where);
		return $update;
	}		

	public function delete_or($where){
		$delete = $this->db->delete("lp_or", $where);
		return $delete;
	}
//====================================================== LP or =======================================================//				
//====================================================== LP or JENIS =====================================================//
	public function get_or_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_or_jenis")->result();
		return $data;
	}	

	public function insert_or_jenis($data){
		$insert = $this->db->insert("lp_or_jenis", $data);
		return $insert;
	}	

	public function get_or_jenis_where($where){
		$data = $this->db->get_where("lp_or_jenis",$where)->row_array();
		return $data;
	}

	public function update_or_jenis($set, $where){
		$update = $this->db->update("lp_or_jenis", $set, $where);
		return $update;
	}		

	public function delete_or_jenis($where){
		$delete = $this->db->delete("lp_or_jenis", $where);
		return $delete;
	}		
//====================================================== LP or JENIS =====================================================//	
}
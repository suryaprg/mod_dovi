<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_miskin extends CI_Model
{

   public function get_lp_jenis(){
        $data = $this->db->get("lp_miskin_jenis")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_miskin", $where)->row_array();
        return $data;
    }
    
//====================================================== LP miskin =======================================================//
	public function get_miskin(){
		// $this->db->where("is_delete !=", "1");
		$this->db->join('lp_miskin_jenis li','lm.id_jenis = li.id_jenis');
		$data = $this->db->get("lp_miskin lm")->result();
		return $data;
	}

	public function get_miskin_where($where){
		$data = $this->db->get_where("lp_miskin",$where)->row_array();
		return $data;
	}	

	public function insert_miskin($data){
		$insert = $this->db->insert("lp_miskin", $data);
		return $insert;
	}

	public function update_miskin($set, $where){
		$update = $this->db->update("lp_miskin", $set, $where);
		return $update;
	}		

	public function delete_miskin($where){
		$delete = $this->db->delete("lp_miskin", $where);
		return $delete;
	}
				
//================================================== LP MISKIN JENIS =====================================================//
	public function get_miskin_jenis(){
		// $this->db->where("is_delete !=", "1");
		$data = $this->db->get("lp_miskin_jenis")->result();
		return $data;
	}	

	public function insert_miskin_jenis($data){
		$insert = $this->db->insert("lp_miskin_jenis", $data);
		return $insert;
	}	

	public function get_miskin_jenis_where($where){
		$data = $this->db->get_where("lp_miskin_jenis",$where)->row_array();
		return $data;
	}

	public function update_miskin_jenis($set, $where){
		$update = $this->db->update("lp_miskin_jenis", $set, $where);
		return $update;
	}		

	public function delete_miskin_jenis($where){
		$delete = $this->db->delete("lp_miskin_jenis", $where);
		return $delete;
	}		
//====================================================== LP miskin JENIS =====================================================//	
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_tenaga extends CI_Model{
	
    public function get_lp_jenis(){
        $data = $this->db->get("lp_tenaga_jenis")->result();
        return $data;
    }

    public function get_lp_kategori(){
        $data = $this->db->get("lp_tenaga_kategori")->result();
        return $data;
    }

    public function get_lp_tenaga($where){
        $data = $this->db->get_where("lp_tenaga lp", $where)->row_array();
        return $data;
    }
    
}
?>
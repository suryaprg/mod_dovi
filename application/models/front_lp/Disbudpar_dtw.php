<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Disbudpar_dtw extends CI_Model{
    
    public function get_lp_jenis(){
        $data = $this->db->get("disbudpar_dtw_jenis")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("disbudpar_dtw_main", $where)->row_array();
        return $data;
    }
    
}
?>
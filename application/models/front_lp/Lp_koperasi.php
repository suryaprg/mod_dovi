<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_koperasi extends CI_Model{
    
    public function get_lp_jenis(){
        $data = $this->db->get("lp_kooperasi_jenis");
        return $data;
    }

    public function get_lp_jenis_where($where){
        $data = $this->db->get_where("lp_kooperasi_sub_jenis", $where);
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_koperasi d", $where)->row_array();
        return $data;
    }
    
}
?>
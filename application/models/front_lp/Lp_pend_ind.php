<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_pend_ind extends CI_Model{
    
    public function get_lp_jenis(){
        $data = $this->db->get("lp_pend_industri_jenis")->result();
        return $data;
    }
    public function get_lp_kategori(){
        $data = $this->db->get("lp_pend_industri_kategori")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_pend_industri", $where)->row_array();
        return $data;
    }
    
}
?>
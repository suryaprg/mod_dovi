<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_keuangan extends CI_Model{
	
    public function get_lp_keu_jenis(){
        $data = $this->db->get("lp_keu_jenis")->result();
        return $data;
    }

    public function get_lp_keu_jenis_where($where){
        $data = $this->db->get_where("lp_keu_jenis", $where)->result();
        return $data;
    }

    public function get_lp_keu_sub_jenis($where){
    	$this->db->join("lp_keu_jenis lkj", "lksj.id_jenis = lkj.id_jenis");
        $data = $this->db->get_where("lp_keu_sub_jenis lksj", $where)->result();
        return $data;
    }

    public function get_lp_keu($where){
    	$this->db->join("lp_keu_jenis lkj", "lk.id_jenis = lkj.id_jenis");
        $this->db->join("lp_keu_sub_jenis lksj", "lk.id_sub_jenis = lksj.id_sub_jenis");
        $data = $this->db->get_where("lp_keu lk", $where)->row_array();
        return $data;
    }
    
}
?>
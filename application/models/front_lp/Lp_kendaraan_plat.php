<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_kendaraan_plat extends CI_Model{
    
    public function get_lp_jenis(){
        $data = $this->db->get("lp_kendaran_plat_jenis")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("lp_kendaran_plat", $where)->row_array();
        return $data;
    }
    
}
?>
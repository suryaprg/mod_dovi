<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_budidaya extends CI_Model {

#---------------------------------------pertanian_lahan----------------------------------------------

	public function get_jenis_pertanian_lahan(){
		$this->db->select('jenis_lahan');
		$this->db->distinct('jenis_lahan');
		$data = $this->db->get("pertanian_lahan");
		return $data;
	}

	public function get_pertanian_lahan($where){
		$data = $this->db->get_where("pertanian_lahan", $where);
		return $data;
	}

#----------------------------------pertanian_lahan_penggunaan----------------------------------------------

	public function get_pertanian_lahan_penggunaan($where){
		$data = $this->db->get_where("pertanian_lahan_penggunaan", $where);
		return $data;
	}

#----------------------------------kepala_tebu----------------------------------------------

	public function get_pertanian_kelapa_tebu($where){
		$data = $this->db->get_where("pertanian_kelapa_tebu", $where);
		return $data;
	}


#---------------------------------------Komoditi----------------------------------------------
	public function get_pertanian_komoditas_jenis(){
		$data = $this->db->get("pertanian_komoditas_jenis")->result();

		return $data;
	}

	public function get_pertanian_komoditas($where){
		// $this->db->select("nama_jenis, prosentase, th");
		$this->db->join("pertanian_komoditas_jenis pkj", "pk.id_jenis = pkj.id_jenis");
		$data = $this->db->get_where("pertanian_komoditas_jml pk", $where);

		return $data;
	}

#---------------------------------------Hortikultura----------------------------------------------
	public function get_pertanian_holti_jenis(){
		$data = $this->db->get("pertanian_holti_jenis")->result();

		return $data;
	}

	public function get_pertanian_holti_jml($where){
		// $this->db->select("nama_jenis, prosentase, th");
		$this->db->join("pertanian_holti_jenis pkj", "pk.id_jenis = pkj.id_jenis");
		$data = $this->db->get_where("pertanian_holti_jml pk", $where);

		return $data;
	}

#--------------------------------------pertanian_ternak_jenis----------------------------------------------
	public function get_pertanian_ternak_jenis(){
		$data = $this->db->get("pertanian_ternak_jenis")->result();

		return $data;
	}

	public function get_pertanian_ternak_jml($where){
		// $this->db->select("nama_jenis, prosentase, th");
		$this->db->join("pertanian_ternak_jenis pkj", "pk.id_jenis = pkj.id_jenis");
		$data = $this->db->get_where("pertanian_ternak_jml pk", $where);

		return $data;
	}

#--------------------------------------pertanian_ikan_jenis----------------------------------------------
	public function get_pertanian_ikan_jenis(){
		$data = $this->db->get("pertanian_ikan_jenis")->result();

		return $data;
	}

	public function get_pertanian_ikan_jml($where){
		// $this->db->select("nama_jenis, prosentase, th");
		$this->db->join("pertanian_ikan_jenis pkj", "pk.id_jenis = pkj.id_jenis");
		$data = $this->db->get_where("pertanian_ikan_jml pk", $where);

		return $data;
	}



}
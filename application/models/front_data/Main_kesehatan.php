<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_kesehatan extends CI_Model {

#---------------------------------------------kesehatan_gizi_balita----------------------------------------------
public function get_kesehatan_gizi_balita($where){
		$this->db->select("jmh_kurang_gizi, jml_gizi_buruk, th");
		$data = $this->db->get_where("kesehatan_gizi_balita", $where);

		return $data;
	}

	public function get_kesehatan_gizi_balita_all(){
		$this->db->select("jmh_kurang_gizi, jml_gizi_buruk, th");
		$data = $this->db->get("kesehatan_gizi_balita");

		return $data;
	}

#-------------------------------------------main_jenis_penyakit----------------------------------------------

	public function get_main_jenis_penyakit($where){
		$this->db->select("keterangan, jml, th");
		$data = $this->db->get_where("main_jenis_penyakit", $where);

		return $data;
	}

	public function get_main_jenis_penyakit_all(){
		$this->db->select("keterangan, jml, th");
		$data = $this->db->get_where("main_jenis_penyakit");

		return $data;
	}
}
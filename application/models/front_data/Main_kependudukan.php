<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_kependudukan extends CI_Model {

#---------------------------------------------index_pem_manusia----------------------------------------------

	public function get_index_pem_manusia($where){
		$this->db->select("ipm, th");
		$data = $this->db->get_where("index_pem_manusia ipm", $where);

		return $data;
	}

#-------------------------------------------index_kes_daya_pend----------------------------------------------

	public function get_index_kes_daya_pend($where){
		$this->db->select("in_daya as daya_beli, in_kes as kesehatan, in_pend as pendidikan, th");
		$data = $this->db->get_where("index_kes_daya_pend ipm", $where);

		return $data;
	}

#-------------------------------------------kepandudukan_jk----------------------------------------------

	public function get_kepandudukan_jk($where){
		$this->db->select("th, t_cowo, t_cewe, rasio_jk");
		$data = $this->db->get_where("kepandudukan_jk", $where);
		return $data;
	}

#-------------------------------------------kependudukan_kel_umur----------------------------------------------

	public function get_kependudukan_kel_umur_jenis(){
		// $this->db->select("periode, kelompok, jml_penduduk");
		$data = $this->db->query("SELECT Distinct kelompok from kependudukan_kel_umur");
		return $data;
	}

	public function get_kependudukan_kel_umur($where){
		$data = $this->db->get_where("kependudukan_kel_umur", $where);
		return $data;
	}

#-------------------------------------kependudukan_rasio_ketergantungan----------------------------------------------

	public function get_kependudukan_rasio_ketergantungan($where){
		$this->db->select("th_rasio, rasio_keter");
		$data = $this->db->get_where("kependudukan_rasio_ketergantungan", $where);
		return $data;
	}



}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_dpr extends CI_Model{
	
    public function get_lp_jenis(){
        $data = $this->db->get("main_partai")->result();
        return $data;
    }

    public function get_lp($where){
        $data = $this->db->get_where("pem_dpr", $where)->row_array();
        return $data;
    }
    
}
?>
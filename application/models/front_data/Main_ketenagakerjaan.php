<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_ketenagakerjaan extends CI_Model {

#---------------------------------------------kerja_angkatan----------------------------------------------

	public function get_kerja_angkatan($where){
		$this->db->select("th, jml_kerja, jml_no_kerja");
		$data = $this->db->get_where("kerja_angkatan", $where);

		return $data;
	}

#-------------------------------------------kerja_pengangguran----------------------------------------------

	public function get_kerja_pengangguran($where){
		$this->db->select("th, jml");
		$data = $this->db->get_where("kerja_pengangguran", $where);

		return $data;
	}

#-------------------------------------------kerja_ump----------------------------------------------

	public function get_kerja_ump($where){
		$this->db->select("th, jml");
		$data = $this->db->get_where("kerja_ump", $where);
		return $data;
	}

}
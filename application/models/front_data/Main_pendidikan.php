<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_pendidikan extends CI_Model {

#---------------------------------------pendidikan_baca_tulis----------------------------------------------

	public function get_pendidikan_baca_tulis($where){
		$this->db->select("nama_jenis, prosentase, th");
		$this->db->join("pendidikan_baca_tulis_jenis pbtj", "pbt.id_jenis = pbtj.id_jenis");
		$data = $this->db->get_where("pendidikan_baca_tulis pbt", $where);

		return $data;
	}


#---------------------------------------pendidikan_partisipasi_sklh----------------------------------------------

	public function get_pendidikan_partisipasi_sklh($where){
		$this->db->select("nama_jenis, prosentase, th");
		$this->db->join("pendidikan_partisipasi_sklh_jenis ppsj", "pps.id_jenis = ppsj.id_jenis");
		$data = $this->db->get_where("pendidikan_partisipasi_sklh pps", $where);

		return $data;
	}

#---------------------------------------pendidikan_penduduk----------------------------------------------

	public function get_pendidikan_penduduk($where){
		$this->db->select("nama_jenis, prosentase, th");
		$this->db->join("pendidikan_penduduk_jenis ppj", "pp.id_jenis = ppj.id_jenis");
		$data = $this->db->get_where("pendidikan_penduduk pp", $where);

		return $data;
	}


}
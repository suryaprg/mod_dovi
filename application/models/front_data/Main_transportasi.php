<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_transportasi extends CI_Model {

#---------------------------------------trans_jln----------------------------------------------

	public function get_trans_jln_jenis(){
		$data = $this->db->query("SELECT DISTINCT kategori_jln FROM trans_jln");

		return $data;
	}

	public function get_trans_jln($where){
		$data = $this->db->get_where("trans_jln", $where);

		return $data;
	}


#---------------------------------------trans_jml_kendaraan----------------------------------------------

	public function get_main_jenis_kendaraan(){
		$data = $this->db->get("main_jenis_kendaraan")->result();

		return $data;
	}

	public function get_trans_jml_kendaraan($where){
		// $this->db->select("nama_jenis, prosentase, th");
		$this->db->join("main_jenis_kendaraan pkj", "pk.id_jenis_kendaraan = pkj.id_jenis_kendaraan");
		$data = $this->db->get_where("trans_jml_kendaraan pk", $where);

		return $data;
	}



}
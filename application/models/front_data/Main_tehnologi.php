<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_tehnologi extends CI_Model {

#---------------------------------------tehnologi_jml_bts----------------------------------------------

	public function get_tehnologi_jml_bts($where){
		$data = $this->db->get_where("tehnologi_jml_bts pbt", $where);

		return $data;
	}


#---------------------------------------tehnologi_jml_warnet----------------------------------------------

	public function get_tehnologi_jml_warnet($where){
		$data = $this->db->get_where("tehnologi_jml_warnet pps", $where);

		return $data;
	}

#---------------------------------------tehnologi_jml_web_opd----------------------------------------------

	public function get_tehnologi_jml_web_opd($where){
		$data = $this->db->get_where("tehnologi_jml_web_opd pps", $where);

		return $data;
	}


}
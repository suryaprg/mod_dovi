<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_miskin extends CI_Model {

#---------------------------------------miskin_jml_pend----------------------------------------------

	public function get_miskin_jml_pend($where){
		$data = $this->db->get_where("miskin_jml_pend pbt", $where);

		return $data;
	}

#---------------------------------------miskin_pengeluaran_perkapita----------------------------------------------

	public function get_miskin_pengeluaran_perkapita($where){
		$data = $this->db->get_where("miskin_pengeluaran_perkapita pbt" , $where);

		return $data;
	}

}
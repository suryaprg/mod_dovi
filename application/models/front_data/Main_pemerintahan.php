<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_pemerintahan extends CI_Model {

#---------------------------------------------pem_jml_rt_rw----------------------------------------------
	
	public function get_kecamatan(){
		
		$data = $this->db->get("main_kecamatan")->result();
		return $data;
	}


	public function get_pem_jml_rt_rw($where){
		$this->db->select("nama_kec, jml_rt, jml_rw, th");
		$this->db->join("main_kecamatan mk", "pjr.id_kec = mk.id_kec");
		$data = $this->db->get_where("pem_jml_rt_rw pjr", $where);

		return $data;
	}

}
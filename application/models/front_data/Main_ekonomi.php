<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_ekonomi extends CI_Model {

#---------------------------------------------get_pendapatan_usaha----------------------------------------------

	public function get_pendapatan_usaha_jenis(){
		$data = $this->db->get("pendapatan_lap_usaha_jenis plu")->result();

		return $data;
	}

	public function get_pendapatan_usaha($where){
		$this->db->select(" jml, th");
		$this->db->join("pendapatan_lap_usaha_jenis pluj", "plu.id_jenis = pluj.id_jenis");
		$data = $this->db->get_where("pendapatan_lap_usaha plu", $where);

		return $data;
	}

#-------------------------------------------get_pendapatan_regional----------------------------------------------

	public function get_pendapatan_regional($where){
		$this->db->select("jml_pendapatan, th_pendapatan");
		$data = $this->db->get_where("pendapatan_regional", $where);

		return $data;
	}

#-------------------------------------------keuangan----------------------------------------------

	public function get_keuangan_jenis(){
		// $this->db->select("ket, kategori");
		$data = $this->db->get("keu_jenis")->result();
		return $data;
	}

	public function get_keuangan_jenis_kate($where){
		// $this->db->select("ket, kategori");
		$data = $this->db->get_where("keu_jenis", $where)->result();
		return $data;
	}

	public function get_keuangan($where){
		$this->db->select("jml, th");
		$this->db->join("keu_jenis kjn", "kjm.id_jenis = kjn.id_jenis");
		$data = $this->db->get_where("keu_jml kjm", $where);

		return $data;
	}

#-------------------------------------------pengeluaran_penduduk----------------------------------------------

	public function get_pengeluaran_penduduk_jenis(){
		// $this->db->select("ket, kategori");
		$data = $this->db->get("pengeluaran_penduduk_jenis")->result();
		return $data;
	}

	public function get_pengeluaran_penduduk($where){
		$this->db->select("jml, th");
		$this->db->join("pengeluaran_penduduk_jenis ppjn", "ppj.id_jenis = ppjn.id_jenis");
		$data = $this->db->get_where("pengeluaran_penduduk ppj", $where);

		return $data;
	}

#-------------------------------------------pengeluaran_rmt_tgg----------------------------------------------

	public function pengeluaran_rmt_tgg($where){
		$this->db->select("th, jml_mkn, jml_non");
		$data = $this->db->get_where("pengeluaran_rmt_tgg", $where);

		return $data;
	}
}
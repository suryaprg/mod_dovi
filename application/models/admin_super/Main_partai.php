<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_partai extends CI_Model {

#---------------------------------------------------------------Partai---------------------------------------------------------------------

	public function get_partai(){
		$this->db->where("is_delete !=", "1");
		$data = $this->db->get("main_partai")->result();
		return $data;
	}

	public function get_partai_where($where){
		$data = $this->db->get_where("main_partai",$where)->row_array();
		return $data;
	}

	public function insert_partai($data){
		$insert = $this->db->insert("main_partai", $data);
		return $insert;
	}

	public function update_partai($set, $where){
		$update = $this->db->update("main_partai", $set, $where);
		return $update;
	}

	public function delete_partai($where){
		$delete = $this->db->delete("main_partai", $where);
		return $delete;
	}

#---------------------------------------------------------------DPR---------------------------------------------------------------------

	public function get_dpr(){
		// $this->db->where("pd.is");
		$this->db->join("main_partai mp", "pd.id_partai = mp.id_partai");
		$data = $this->db->get("pem_dpr pd")->result();
		return $data;
	}

	public function get_dpr_where($where){
		$this->db->join("main_partai mp", "pd.id_partai = mp.id_partai");
		$data = $this->db->get_where("pem_dpr pd",$where)->row_array();
		return $data;
	}

	public function insert_dpr($data){
		$insert = $this->db->insert("pem_dpr", $data);
		return $insert;
	}

	public function update_dpr($set, $where){
		$update = $this->db->update("pem_dpr", $set, $where);
		return $update;
	}

	public function delete_dpr($where){
		$delete = $this->db->delete("pem_dpr", $where);
		return $delete;
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_kecamatan extends CI_Model {

#---------------------------------------------------------------Kecamatan---------------------------------------------------------------------

	public function get_kec(){
		$this->db->where("is_delete !=", "1");
		$data = $this->db->get("main_kecamatan")->result();
		return $data;
	}

	public function get_kec_where($where){
		$data = $this->db->get_where("main_kecamatan",$where)->row_array();
		return $data;
	}

	public function insert_kec($data){
		$insert = $this->db->query("select insert_kecamatan('','','','','','');");
		return $insert;
	}

	public function update_kec($set, $where){
		$update = $this->db->update("main_kecamatan", $set, $where);
		return $update;
	}

	public function delete_kec($where){
		$delete = $this->db->delete("main_kecamatan", $where);
		return $delete;
	}

#---------------------------------------------------------------Kelurahan---------------------------------------------------------------------

	public function get_kel(){
		
		$this->db->join("main_kecamatan mk", "ml.id_kec = mk.id_kec");
		$this->db->where("ml.is_del !=", "1");
		$data = $this->db->get("main_kelurahan ml")->result();
		return $data;
	}

	public function get_kel_where($where){
		$this->db->join("main_kecamatan mk", "ml.id_kec = mk.id_kec");
		$data = $this->db->get_where("main_kelurahan ml",$where)->row_array();
		return $data;
	}

	public function get_kel_all_where($where){
		$this->db->join("main_kecamatan mk", "ml.id_kec = mk.id_kec");
		$data = $this->db->get_where("main_kelurahan ml",$where)->result();
		return $data;
	}

	public function insert_kel($data){
		$insert = $this->db->query("select insert_kelurahan('','','','','','');");
		return $insert;
	}

	public function update_kel($set, $where){
		$update = $this->db->update("main_kelurahan", $set, $where);
		return $update;
	}

	public function delete_kel($where){
		$delete = $this->db->delete("main_kelurahan", $where);
		return $delete;
	}

}

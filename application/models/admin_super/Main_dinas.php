<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_dinas extends CI_Model {

#---------------------------------------------------------------Pendidikan---------------------------------------------------------------------

	public function get_dinas(){
		$data = $this->db->get("main_dinas")->result();
		return $data;
	}

	public function get_dinas_where($where){
		$data = $this->db->get_where("main_dinas",$where)->row_array();
		return $data;
	}

	public function insert_dinas($data){
		$insert = $this->db->insert("main_dinas", $data);
		return $insert;
	}

	public function update_dinas($set, $where){
		$update = $this->db->update("main_dinas", $set, $where);
		return $update;
	}

	public function delete_dinas($where){
		$delete = $this->db->delete("main_dinas", $where);
		return $delete;
	}

}

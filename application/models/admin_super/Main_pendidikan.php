<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_pendidikan extends CI_Model {

#---------------------------------------------------------------Pendidikan---------------------------------------------------------------------

	public function get_pendidikan(){
		$data = $this->db->get("main_pendidikan")->result();
		return $data;
	}

	public function get_pendidikan_where($where){
		$data = $this->db->get_where("main_pendidikan",$where)->row_array();
		return $data;
	}

	public function insert_pendidikan($data){
		$insert = $this->db->insert("main_pendidikan", $data);
		return $insert;
	}

	public function update_pendidikan($set, $where){
		$update = $this->db->update("main_pendidikan", $set, $where);
		return $update;
	}

	public function delete_pendidikan($where){
		$delete = $this->db->delete("main_pendidikan", $where);
		return $delete;
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_admin extends CI_Model {

#---------------------------------------------------------------Admin Lv----------------------------------------------------------------------

	public function get_admin_lv(){

		$data = $this->db->get("admin_lv")->result();
		return $data;
	}

	public function get_admin_lv_where($where){
		$data = $this->db->get_where("admin_lv",$where)->row_array();
		return $data;
	}

	public function insert_admin_lv($data){
		$insert = $this->db->insert("admin_lv",$data);
		return $insert;
	}

	public function update_admin_lv($set, $where){
		$update = $this->db->update("admin_lv", $set, $where);
		return $update;
	}

	public function delete_admin_lv($where){
		$delete = $this->db->delete("admin_lv", $where);
		return $delete;
	}

#---------------------------------------------------------------Admin---------------------------------------------------------------------

	public function get_admin(){
		// $this->db->select("id_admin, ad.id_lv, email, nama, nip, jabatan, id_dinas, is_delete, ket");
		$this->db->join("admin_lv al", "ad.id_lv=al.id_lv");
		$this->db->join("main_dinas md", "ad.id_dinas=md.id_dinas");
		$this->db->where("ad.is_delete !=", "1");
		$data = $this->db->get("admin ad")->result();
		return $data;
	}

	public function get_admin_all(){
		$this->db->join("admin_lv al", "ad.id_lv=al.id_lv");
		$this->db->join("main_dinas md", "ad.id_dinas=md.id_dinas");
		$data = $this->db->get("admin ad")->result();
		return $data;
	}
	

	public function get_admin_where($where){
		$this->db->select("id_admin, ad.id_lv, email, nama, nip, jabatan, status_active, is_delete, ket, ad.id_dinas, nama_dinas");
		$this->db->join("admin_lv al", "ad.id_lv=al.id_lv");
		$this->db->join("main_dinas md", "ad.id_dinas=md.id_dinas");
		$data = $this->db->get_where("admin ad",$where)->row_array();
		return $data;
	}

	public function insert_admin($data){
		$insert = $this->db->query("select insert_admin('','','','','','');");
		return $insert;
	}

	public function update_admin($set, $where){
		$update = $this->db->update("admin", $set, $where);
		return $update;
	}

	public function delete_admin($where){
		$delete = $this->db->delete("admin", $where);
		return $delete;
	}


}

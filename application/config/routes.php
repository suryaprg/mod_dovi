<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_methodok
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['beranda'] = "web_home/web/index";
$route['beranda/tentang'] = "web_home/web/about";
$route['beranda/struktur'] = "web_home/web/struktur";

$route['beranda/profil'] = "front/mainfront/profil";
$route['beranda/unitkerja'] = "front/mainfront/unit_kerja";
$route['beranda/kepegawaian'] = "front/mainfront/kepegawaian";

// $route['super/home'] = "admin_super/mainadmin/index";
#------------------------------------------------------------------Admin---------------------------------------------------------------------
$route['admin/login'] = "Adminlogin";
#------------------------------------------------------------------Admin---------------------------------------------------------------------

$route['super/admin'] = "admin_super/mainadmin/index";
$route['super/akses'] = "admin_super/mainadmin/index_admin_lv";

#------------------------------------------------------------------Kalurahan Kecamatan---------------------------------------------------------------------
$route['super/kelkec'] = "admin_super/mainkelkec/index";
// $route['super/kecamatan'] = "front/mainfront/tentang";

#------------------------------------------------------------------Partai---------------------------------------------------------------------
$route['super/partai'] = "admin_super/mainpartai/index";
$route['super/jenjang'] = "front/mainfront/profil";



#------------------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------Input Admin---------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------------------

#------------------------------------------------------------------ Geografi --------------------------------------------------------------------

	// $route['admin/sawah_kebun']			= "admin_opd/geo/Maingeo/sawahKebun";
	// $route['admin/sungai'] 				= "admin_opd/geo/Maingeo/sungai";
	// $route['admin/hutan'] 				= "admin_opd/geo/Maingeo/hutan";
	// $route['admin/tamanRTH'] 			= "admin_opd/geo/Maingeo/tamanRTH";

	$route['admin/iklim'] 				= "admin_opd/geo/Adminiklim/index";

#------------------------------------------------------------------ Pemerintahan --------------------------------------------------------------------

	$route['admin/rt_rw'] 				= "admin_opd/pemerintahan/Adminrtrw/index";

#------------------------------------------------------------------ Keuangan Daerah --------------------------------------------------------------------

	$route['admin/keuangan_daerah'] 	= "admin_opd/ekonomi/Adminkeudaerah/index";

#------------------------------------------------------------------ Kependudukan --------------------------------------------------------------------

	$route['admin/kependudukan_jk'] 			= "admin_opd/kependudukan/Adminkependudukanjk/index";
	$route['admin/kependudukan_umur'] 			= "admin_opd/kependudukan/Adminkependudukanumur/index";
	$route['admin/kependudukan_ketergantungan'] = "admin_opd/kependudukan/Adminkependudukanketer/index";
	$route['admin/kependudukan_ipm'] 			= "admin_opd/kependudukan/Adminkependudukanipm/index";
	$route['admin/kependudukan_ipm_bid'] 		= "admin_opd/kependudukan/Adminkependudukanipmbidang/index";

#------------------------------------------------------------------ Kerja --------------------------------------------------------------------

	$route['admin/kerja_angkatan'] 		= "admin_opd/kerja/Adminkerjaangkatan/index";
	$route['admin/kerja_pengangguran'] 	= "admin_opd/kerja/Adminkerjapengangguran/index";
	$route['admin/kerja_ump'] 			= "admin_opd/kerja/Adminkerjaump/index";

#------------------------------------------------------------------ Tehnologi --------------------------------------------------------------------

	$route['admin/tehno_bts'] 			= "admin_opd/tehnologi/Admintehnologibts/index";
	$route['admin/tehno_warnet'] 		= "admin_opd/tehnologi/Admintehnologiwarnet/index";
	$route['admin/tehno_web'] 			= "admin_opd/tehnologi/Admintehnologiweb/index";

#------------------------------------------------------------------ Kesehatan --------------------------------------------------------------------

	$route['admin/kesehatan_gizi'] 		= "admin_opd/kesehatan/Adminkesehatangizi/index";
	$route['admin/kesehatan_penyakit'] 	= "admin_opd/kesehatan/Adminkesehatanpenyakit/index";

#------------------------------------------------------------------ Transportasi --------------------------------------------------------------------

	$route['admin/trans_kendaraan'] 	= "admin_opd/transportasi/Admintranskendaraan/index";
	$route['admin/trans_jalan'] 		= "admin_opd/transportasi/Admintransjalan/index";

#------------------------------------------------------------------ Pertanian --------------------------------------------------------------------

	$route['admin/pertanian_lahan'] 			= "admin_opd/pertanian/Adminpertanianlahan/index";
	$route['admin/pertanian_lahan_penggunaan'] 	= "admin_opd/pertanian/Adminpertanianlahanpenggunaan/index";

	$route['admin/kelapa_tebu'] 				= "admin_opd/pertanian/Adminpertaniankelapatebu/index";
	$route['admin/pertanian_komoditi'] 			= "admin_opd/pertanian/Adminpertaniankomoditi/index";
	$route['admin/pertanian_hortikultura'] 		= "admin_opd/pertanian/Adminpertanianholtijml/index";

	$route['admin/peternakan'] 					= "admin_opd/pertanian/Adminpertanianternakjml/index";
	$route['admin/perikanan'] 					= "admin_opd/pertanian/Adminpertanianikanjml/index";

#------------------------------------------------------------------ Pendapatan Regional --------------------------------------------------------------------

	$route['admin/pendapatan_regional'] 		= "admin_opd/ekonomi/Adminpendapatanpdo/index";
	$route['admin/kegiatan_usaha'] 				= "admin_opd/ekonomi/Adminekonomikontribusi/index";

#------------------------------------------------------------------ pengeluaran Penduduk --------------------------------------------------------------------

	$route['admin/pengeluaran_penduduk'] 		= "admin_opd/ekonomi/Adminpengeluaranpenduduk/index";
	$route['admin/pengeluaran_rumah_tangga'] 	= "admin_opd/ekonomi/Adminpengeluaranrmttgg/index";

#------------------------------------------------------------------  Miskin --------------------------------------------------------------------

	$route['admin/penduduk_miskin'] 		= "admin_opd/kependudukan/Adminmiskinjmlpend/index";
	$route['admin/garis_miskin'] 			= "admin_opd/kependudukan/Adminmiskinpengeluaranperkapita/index";

#------------------------------------------------------------------  Pendidikan --------------------------------------------------------------------

	$route['admin/pendidikan_baca_tulis'] 			= "admin_opd/pendidikan/Adminpendidikanbacatulis/index";
	$route['admin/pendidikan_partisipasi_sklh'] 	= "admin_opd/pendidikan/Adminpendidikanpartisipasisklh/index";
	$route['admin/pendidikan_penduduk'] 			= "admin_opd/pendidikan/Adminpendidikanpenduduk/index";
	$route['admin/jumlah_sekolah'] 					= "admin_opd/pendidikan/Adminpendidikanjmlsklh/index";
	$route['admin/rasio_guru_murid'] 				= "admin_opd/pendidikan/Adminpendidikanrasiogurumurid/index";
	

#------------------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------Input Admin---------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------------------





#------------------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------Data User---------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------------------

#------------------------------------------------------------------ Geografi --------------------------------------------------------------------

$route['data/geografi_wilayah'] 	= "front_data/Maingeo/geografi_wilayah";
$route['data/wilayah_administrasi'] = "front_data/Maingeo/wilayahAdmin";

$route['data/iklim/(:num)'] 		= "front_data/Maingeo/iklim/$1";

$route['data/kedungkandang'] 		= "front_data/Maingeo/kedungkandang";
$route['data/sukun'] 				= "front_data/Maingeo/sukun";
$route['data/lowokwaru'] 			= "front_data/Maingeo/lowokwaru";
$route['data/blimbing'] 			= "front_data/Maingeo/blimbing";
$route['data/klojen'] 				= "front_data/Maingeo/klojen";

$route['data/peta_bencana'] 				= "front_data/Maingeo/peta_bencana";
// $route['data/sawah_kebun']			= "front_data/Maingeo/sawahKebun";
// $route['data/sungai'] 				= "front_data/Maingeo/sungai";
// $route['data/hutan'] 				= "front_data/Maingeo/hutan";

// $route['data/tamanRTH'] 			= "front_data/Maingeo/tamanRTH";


#------------------------------------------------------------------ Kependudukan --------------------------------------------------------------------

$route['data/index_pembangunan_manusia/(:num)'] = "front_data/Mainkependudukan/index_pem_manusia/$1";
$route['data/pengembangan_index/(:num)'] 		= "front_data/Mainkependudukan/index_kes_daya_pend/$1";
$route['data/kepandudukan_jk/(:num)'] 			= "front_data/Mainkependudukan/kepandudukan_jk/$1";
$route['data/kependudukan_kel_umur/(:num)'] 	= "front_data/Mainkependudukan/kependudukan_kel_umur/$1";
$route['data/rasio_ketergantungan/(:num)'] 		= "front_data/Mainkependudukan/kependudukan_rasio_ketergantungan/$1";

$route['data/jumlah_penduduk_miskin/(:num)'] 	= "front_data/Mainkependudukan/miskin_jml_pend/$1";
$route['data/pengeluaran_perkapita/(:num)'] 	= "front_data/Mainkependudukan/miskin_pengeluaran_perkapita/$1";

// $route['data/umur'] 				= "front_data/Mainkependudukan/umur";
// $route['data/gender'] 				= "front_data/Mainkependudukan/gender";
// $route['data/ketergantungan']		= "front_data/Mainkependudukan/ketergantungan";
// $route['data/gini'] 				= "front_data/Mainkependudukan/gini";
// $route['data/indeksPemManusia'] 	= "front_data/Mainkependudukan/indeksPemManusia";\

#------------------------------------------------------------------ Ketenagakerjaan --------------------------------------------------------------------

$route['data/angkatan_kerja/(:num)'] 		= "front_data/Mainketenagakerjaan/kerja_angkatan/$1";
$route['data/pengangguran/(:num)'] 			= "front_data/Mainketenagakerjaan/kerja_pengangguran/$1";
$route['data/upah_minimum/(:num)'] 			= "front_data/Mainketenagakerjaan/kerja_ump/$1";

// $route['data/ketenagakerjaan'] 		= "front_data/Mainkependudukan/ketenagakerjaan";
// $route['data/angkatanKerja'] 		= "front_data/Mainkependudukan/angkatanKerja";
// $route['data/pengangguran'] 		= "front_data/Mainkependudukan/pengangguran";
// $route['data/upahMinimum'] 			= "front_data/Mainkependudukan/upahMinimum";


#------------------------------------------------------------------ Kesehatan --------------------------------------------------------------------

$route['data/kesehatan_gizi_balita/(:num)'] 	= "front_data/Mainkesehatan/kesehatan_gizi_balita/$1";
$route['data/jenis_penyakit/(:num)'] 			= "front_data/Mainkesehatan/main_jenis_penyakit/$1";

#------------------------------------------------------------------ Ekonomi -------------------------------------------------------------------- fn

$route['data/pendapatan_lap_usaha/(:num)'] 	= "front_data/Mainekonomi/index_pendapatan_usaha/$1";
$route['data/pendapatan_regional/(:num)'] 	= "front_data/Mainekonomi/index_pendapatan_regional/$1";
$route['data/keuangan_daerah/(:num)'] 	= "front_data/Mainekonomi/index_keuangan/$1";
$route['data/pengeluaran_penduduk/(:num)'] 	= "front_data/Mainekonomi/pengeluaran_penduduk/$1";
$route['data/pengeluaran_rumah_tangga/(:num)'] 	= "front_data/Mainekonomi/pengeluaran_rmt_tgg/$1";



#------------------------------------------------------------------ Pemerintahan -------------------------------------------------------------------- lw



#------------------------------------------------------------------ Pendidikan --------------------------------------------------------------------

$route['data/pendidikan_baca_tulis/(:num)'] 			= "front_data/Mainpendidikan/pendidikan_baca_tulis/$1";
$route['data/pendidikan_partisipasi_sklh/(:num)'] 		= "front_data/Mainpendidikan/pendidikan_partisipasi_sklh/$1";
$route['data/pendidikan_penduduk/(:num)'] 				= "front_data/Mainpendidikan/pendidikan_penduduk/$1";

#------------------------------------------------------------------ Pertanian -------------------------------------------------------------------- fn

$route['data/pertanian_kelapa_tebu/(:num)'] 		= "front_data/Mainbudidaya/pertanian_kelapa_tebu/$1";

$route['data/hortikultura/(:num)'] 					= "front_data/Mainbudidaya/pertanian_holti_jml/$1";
$route['data/komoditi/(:num)'] 						= "front_data/Mainbudidaya/pertanian_komoditas/$1";
// $route['data/sayuran_buah/(:num)'] 				= "front_data/Mainbudidaya/pendidikan_penduduk/$1";

$route['data/perikanan/(:num)'] 					= "front_data/Mainbudidaya/pertanian_ikan/$1";
$route['data/peternakan/(:num)'] 					= "front_data/Mainbudidaya/pertanian_ternak/$1";

$route['data/pertanian_lahan/(:num)'] 				= "front_data/Mainbudidaya/pertanian_lahan/$1";
$route['data/pertanian_lahan_penggunaan/(:num)'] 	= "front_data/Mainbudidaya/pertanian_lahan_penggunaan/$1";

#------------------------------------------------------------------ Tehnologi -------------------------------------------------------------------- fn

$route['data/bts/(:num)'] 			 = "front_data/Maintehnologi/tehnologi_jml_bts/$1";
$route['data/warnet/(:num)'] 		 = "front_data/Maintehnologi/tehnologi_jml_warnet/$1";
$route['data/web_pemerintah/(:num)'] = "front_data/Maintehnologi/tehnologi_jml_web_opd/$1";

#------------------------------------------------------------------ Transportasi -------------------------------------------------------------------- fn

$route['data/infrastruktur_jalan/(:num)'] 	= "front_data/Maintransportasi/trans_jln/$1";
$route['data/kendaraan/(:num)'] 			= "front_data/Maintransportasi/trans_jml_kendaraan/$1";

#------------------------------------------------------------------ Sosial -------------------------------------------------------------------- re

// $route['data/organisasi_pemuda'] 			= "front_data/Mainsosial/index";
// $route['data/organisasi_agama'] 			= "front_data/Mainsosial/organisasiAgama";
// $route['data/parpol'] 						= "front_data/Mainsosial/parpol";
$route['data/rt_rw/(:num)'] 					= "front_data/Mainsosial/pem_jml_rt_rw/$1";
$route['data/parlementeria/(:num)'] 				= "front_data/Mainsosial/parlementeria/$1";
// $route['data/aparat_keamanan'] 				= "front_data/Mainsosial/aparat_keamanan";
// $route['data/pidana_penyelesaian'] 			= "front_data/Mainsosial/pidana_penyelesaian";




#------------------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------Data User---------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------------------





#------------------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------Lampiran---------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------------------

#------------------------------------------------------------------ Keuangan --------------------------------------------------------------------

// $route['data/lp_keuangan/(:num)'] 			= "front_lp/Lpkeuangan/index/$1";
// $route['data/keuangan_get/(:num)/(:num)'] 			= "front_lp/Lpkeuangan/keuangan_get/$1/$2";

$route['data/lp_keuangan_e/(:num)'] 		= "front_data/Mainekonomi/lp_keuangan_e/$1";
$route['data/tenaga/(:num)'] 				= "front_data/Mainekonomi/tenaga/$1";
$route['data/lp_koperasi/(:num)'] 			= "front_data/Mainekonomi/koperasi/$1";
$route['data/lp_pendapatan_indutri/(:num)'] = "front_data/Mainekonomi/pendapatan_ind/$1";
$route['data/lp_pajak/(:num)'] 				= "front_data/Mainekonomi/pajak/$1";
$route['data/lp_pdb/(:num)'] 				= "front_data/Mainekonomi/pdb/$1";

#------------------------------------------------------------------ Pendidikan --------------------------------------------------------------------
$route['data/data_pokok'] 		= "front_data/Mainpendidikan/data_pokok";

$route['data/lp_perpustakaan/(:num)'] 		= "front_data/Mainpendidikan/perpustakaan/$1";
$route['data/lp_pendidikan_all/(:num)/(:num)'] 	= "front_data/Mainpendidikan/pendidikan_all/$1/$2"; //fn_new


#------------------------------------------------------------------ lingkungan --------------------------------------------------------------------

$route['data/lp_tpa/(:num)'] 				= "front_data/Maingeo/tpa/$1";

#----------------------------------------------------------------- Transportasi ------------------------------------------------------------------

$route['data/lp_terminal/(:num)'] 			= "front_data/Maintransportasi/terminal/$1";
$route['data/lp_kendaraan/(:num)'] 			= "front_data/Maintransportasi/kendaraan_kec/$1";
$route['data/lp_kendaraan_plat/(:num)'] 	= "front_data/Maintransportasi/kendaraan_plat/$1";
$route['data/lp_kendaraan_umum/(:num)'] 	= "front_data/Maintransportasi/kendaraan_umum/$1";

#----------------------------------------------------------------- Pertanian ------------------------------------------------------------------
$route['data/lp_lahan_kec/(:num)'] 			= "front_data/Mainbudidaya/lahan_kec/$1";
$route['data/lp_panen/(:num)'] 				= "front_data/Mainbudidaya/panen_kec/$1";
$route['data/lp_hasil_panen/(:num)'] 		= "front_data/Mainbudidaya/hasil_panen_kec/$1"; //--fn
$route['data/lp_prod_panen/(:num)'] 		= "front_data/Mainbudidaya/prod_panen_kec/$1"; //--fn

#----------------------------------------------------------------- Penduduk ------------------------------------------------------------------
$route['data/lp_penduduk_jk/(:num)'] 		= "front_data/Mainkependudukan/penduduk_jk/$1";
$route['data/lp_penduduk_rasio/(:num)'] 	= "front_data/Mainkependudukan/penduduk_rasio/$1";
$route['data/lp_penduduk_umur/(:num)'] 		= "front_data/Mainkependudukan/penduduk_umur/$1"; //--fn
#----------------------------------------------------------------- Tehnologi ------------------------------------------------------------------
$route['data/lp_tehnologi/(:num)'] 			= "front_data/Maintehnologi/tehnologi/$1";

#----------------------------------------------------------------- pmks ------------------------------------------------------------------
$route['data/lp_pmks/(:num)'] 				= "front_data/Mainsosial/pmks/$1";

#----------------------------------------------------------------- hukum ------------------------------------------------------------------
$route['data/lp_aparat/(:num)'] 			= "front_data/Mainsosial/aparat/$1";
$route['data/lp_pidana/(:num)'] 			= "front_data/Mainsosial/pidana/$1";
$route['data/lp_pidana_kec/(:num)'] 		= "front_data/Mainsosial/pidana_kec/$1";

#----------------------------------------------------------------- Miskin ------------------------------------------------------------------
$route['data/lp_miskin/(:num)'] 			= "front_data/Mainkependudukan/miskin/$1";

#----------------------------------------------------------------- Bencana ------------------------------------------------------------------
$route['data/lp_bencana/(:num)'] 			= "front_data/Maingeo/bencana/$1";

#----------------------------------------------------------------- olahraga ------------------------------------------------------------------
$route['data/lp_olahraga/(:num)'] 			= "front_data/Mainolahraga/olahraga/$1";

#----------------------------------------------------------------- Diskominfo ------------------------------------------------------------------
$route['data/domain'] 				= "front_data/Maintehnologi/get_domain/"; 
$route['data/aplikasi'] 			= "front_data/Maintehnologi/get_aplikasi/";

#----------------------------------------------------------------- Perdagangan ------------------------------------------------------------------
$route['data/perdagangan/(:num)'] 	= "front_data/Mainekonomi/perdagangan/$1"; 

#----------------------------------------------------------------- Kesehatan ------------------------------------------------------------------
$route['data/lp_kesehatan_all/(:num)/(:num)'] 				= "front_data/Mainkesehatan/kesehatan_all/$1/$2"; //--fn

#----------------------------------------------------------------- Disperkim ------------------------------------------------------------------ otw
$route['data/csr'] 					= "front_data/Maingeo/get_csr/";
$route['data/psu'] 					= "front_data/Maingeo/get_dsu/";
$route['data/disperkim_all/(:num)/(:num)'] 		= "front_data/Maingeo/get_disperkim_all/$1/$2";
$route['data/tanaman/(:num)'] 					= "front_data/Maingeo/get_pohon/$1";
$route['data/taman/(:num)'] 					= "front_data/Maingeo/get_taman/$1";

#----------------------------------------------------------------- Disnaker ------------------------------------------------------------------ otw
$route['data/disnaker/(:num)/(:num)'] 					= "front_data/Mainketenagakerjaan/disnaker_all/$1/$2";

#----------------------------------------------------------------- Disbudpar ------------------------------------------------------------------
$route['data/kawasan_strategis'] 				= "front_data/Mainbudpar/index_dts/$1";
$route['data/pengunjung_wisata/(:num)'] 		= "front_data/Mainbudpar/index_dtw/$1";
$route['data/pengunjung_hotel/(:num)'] 			= "front_data/Mainbudpar/index_hotel/$1";
$route['data/objek_pemajuan_budaya/(:num)'] 	= "front_data/Mainbudpar/index_obj_bud/$1";
$route['data/zona_kratif/(:num)'] 				= "front_data/Mainbudpar/index_zona/$1";

#---------------------------------------------------------------- DPMPTSP ----------------
		$route['data/daftar_perijinan_investasi/(:num)'] 			= "front_lp/Lpdpmptsp/lp_dpmptsp_investasi/$1";
		$route['data/daftar_perijinan_terbit/(:num)'] 			= "front_lp/Lpdpmptsp/lp_dpmptsp_terbit/$1";
		$route['data/daftar_perijinan_tk/(:num)'] 			= "front_lp/Lpdpmptsp/lp_dpmptsp_tk/$1";

	#----------------------------------------------------------------- BPKAD ----------------
		$route['data/kepegawaian/([a-z]+)/(:num)'] 			= "front_lp/bpkad/get_json_file/$1/$2";





#------------------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------Lampiran---------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------------------
$route['data/dataselengkapnya'] 					= "front_data/Maindata";
$route['data/dataselengkapnya/datasatu'] 			= "front_data/Maindata/dataSatu";
$route['data/dataselengkapnya/datadua'] 			= "front_data/Maindata/dataDua";
$route['data/dataselengkapnya/datatiga'] 			= "front_data/Maindata/dataTiga";
$route['data/dataselengkapnya/dataempat'] 			= "front_data/Maindata/dataEmpat";
$route['data/dataselengkapnya/datalima'] 			= "front_data/Maindata/dataLima";
$route['data/dataselengkapnya/dataenam'] 			= "front_data/Maindata/dataEnam";
$route['data/dataselengkapnya/datatujuh'] 			= "front_data/Maindata/dataTujuh";
$route['data/dataselengkapnya/datadelapan'] 		= "front_data/Maindata/dataDelapan";



#-------------------------------------------------------------------ADMIN--------------------------------------------------------------
	$route['admin/aparat']				= "front_lp/Lpaparat/index";            #   
	$route['admin/bencana']				= "front_lp/Lpbencana/index";			#	
	$route['admin/kajian_penelitian']	= "front_lp/Lpkajianpenelitian/index";	#	
	$route['admin/umur']				= "front_lp/Lpkel_umur/index";			#	
	$route['admin/kendaraan']			= "front_lp/Lpkendaraan/index";			#	
	$route['admin/kendaraan/plat']		= "front_lp/Lpkendaraan_plat/index";	#	
	$route['admin/kendaraan/umum']		= "front_lp/Lpkendaraan_umum/index";	#	
	$route['admin/kesehatan']			= "front_lp/Lpkesehatan/index";			#	
	$route['admin/kesehatan/jenis']		= "front_lp/Lpkesehatan_jenis/index";	#	
	$route['admin/kesehatan/sub']		= "front_lp/Lpkesehatan_sub/index";		#	
	$route['admin/koperasi']			= "front_lp/Lpkoperasi/index";			#	
	$route['admin/keuangan']			= "front_lp/Lpkeu/index";				#	
	$route['admin/pendidikan']			= "front_lp/Lppendidikan/index";		#	
	$route['admin/tenaga']				= "front_lp/Lptenaga/index";			#
	$route['admin/terminal']			= "front_lp/Lpterminal/index";			#		
	$route['admin/tpa']					= "front_lp/Lptpa/index";				#
	$route['admin/perpustakaan']		= "front_lp/Lpperpustakaan/index";		#	
	$route['admin/industri']			= "front_lp/Lpindustri/index";			#
	$route['admin/pertanian']			= "front_lp/Lppertanian/index";				#
	$route['admin/pidana']				= "front_lp/Lppidana/index"; 
	$route['admin/tehnologi']			= "front_lp/Lptehnologi/index"; 		#

	$route['admin/lahan']			= "front_lp/Lplahan/index";
	$route['admin/miskin']			= "front_lp/Lpmiskin/index";		
	$route['admin/or']				= "front_lp/Lpor/index";
	$route['admin/pajak']			= "front_lp/Lppajak/index";
	$route['admin/panen']			= "front_lp/Lppanen/index";
	$route['admin/pmks']			= "front_lp/Lppmks/index";
	$route['admin/dts']				= "admin_opd/geo/Disbudpardts/index";

	
	$route['admin/domain'] 			= "admin_opd/diskominfo/Admindomain/index";
	$route['admin/aplikasi'] 		= "admin_opd/diskominfo/Adminaplikasi/index";

	$route['admin/perdagangan'] 	= "admin_opd/perdagangan/Adminperdagangan/index";
	$route['admin/csr'] 			= "admin_opd/disperkim/Admincsr/index";
	$route['admin/dsu'] 			= "admin_opd/disperkim/Admindsu/index";
	$route['admin/taman'] 			= "admin_opd/disperkim/Admintaman/index";
	$route['admin/pohon'] 			= "admin_opd/disperkim/Adminpohon/index";
	$route['admin/disperkim/all'] 	= "admin_opd/disperkim/Admindisperkim/index";
	$route['admin/disnaker'] 		= "admin_opd/disnaker/Admindisnaker/index";

	$route['admin/dts']			= "admin_opd/budpar/Disbudpardts/index";
	$route['admin/dtw']			= "admin_opd/budpar/Disbudpardtw/index";
	$route['admin/dth']			= "admin_opd/budpar/Disbudparkunjunganhoteljenis/index";
	$route['admin/obj']			= "admin_opd/budpar/Obj_main/index";
	$route['admin/zona_kreatif']	= "admin_opd/budpar/Disbudpar_zona_kreatif/index";

	$route['admin/pdb']						= "front_lp/Lppdb/index";
	$route['admin/disnaker'] 		= "admin_opd/disnaker/Admindisnaker/index";
	$route['admin/dpmptsp'] 		= "admin_opd/dpmptsp/Admindpmptsp/index";

		$route['admin/rasio']						= "front_lp/Rasio/index";


#---------------------------------------------------------------------PDF----------------------------------------------------------------------

		
	#------------------------------------------------------------------ Geografi ----------------

		$route['pdf/wilayah_administrasi'] = "front_data_pdf/Maingeo/wilayahAdmin"; //fn
		$route['pdf/iklim/(:num)'] 		= "front_data_pdf/Maingeo/iklim/$1"; //fn

	#------------------------------------------------------------------ Kependudukan ------------

		$route['pdf/index_pembangunan_manusia/(:num)'] = "front_data_pdf/Mainkependudukan/index_pem_manusia/$1"; //fn
		$route['pdf/pengembangan_index/(:num)'] 		= "front_data_pdf/Mainkependudukan/index_kes_daya_pend/$1"; //fn
		$route['pdf/kepandudukan_jk/(:num)'] 			= "front_data_pdf/Mainkependudukan/kepandudukan_jk/$1"; //fn
		$route['pdf/kependudukan_kel_umur/(:num)'] 	= "front_data_pdf/Mainkependudukan/kependudukan_kel_umur/$1"; //fn
		$route['pdf/rasio_ketergantungan/(:num)'] 		= "front_data_pdf/Mainkependudukan/kependudukan_rasio_ketergantungan/$1"; //fn
		$route['pdf/jumlah_penduduk_miskin/(:num)'] 	= "front_data_pdf/Mainkependudukan/miskin_jml_pend/$1"; //fn
		$route['pdf/pengeluaran_perkapita/(:num)'] 	= "front_data_pdf/Mainkependudukan/miskin_pengeluaran_perkapita/$1"; //fn

	#------------------------------------------------------------------ Ketenagakerjaan ---------

		$route['pdf/angkatan_kerja/(:num)'] 		= "front_data_pdf/Mainketenagakerjaan/kerja_angkatan/$1"; //fn
		$route['pdf/pengangguran/(:num)'] 			= "front_data_pdf/Mainketenagakerjaan/kerja_pengangguran/$1"; //fn
		$route['pdf/upah_minimum/(:num)'] 			= "front_data_pdf/Mainketenagakerjaan/kerja_ump/$1"; //fn

	#------------------------------------------------------------------ Kesehatan ---------------

		$route['pdf/kesehatan_gizi_balita/(:num)'] 	= "front_data_pdf/Mainkesehatan/kesehatan_gizi_balita/$1"; //fn
		$route['pdf/jenis_penyakit/(:num)'] 			= "front_data_pdf/Mainkesehatan/main_jenis_penyakit/$1"; //fn

	#------------------------------------------------------------------ Ekonomi ----------------- fn

		$route['pdf/pendapatan_lap_usaha/(:num)'] 	= "front_data_pdf/Mainekonomi/index_pendapatan_usaha/$1"; //fn
		$route['pdf/pendapatan_regional/(:num)'] 	= "front_data_pdf/Mainekonomi/index_pendapatan_regional/$1"; //fn
		$route['pdf/realisasi_pendapatan_daerah/(:num)'] 	= "front_data_pdf/Mainekonomi/get_realisasi/$1"; //fn
		$route['pdf/benaja_daerah/(:num)'] 		= "front_data_pdf/Mainekonomi/get_belanja/$1";  //fn
		$route['pdf/pengeluaran_penduduk/(:num)'] 	= "front_data_pdf/Mainekonomi/pengeluaran_penduduk/$1"; //fn
		$route['pdf/pengeluaran_rumah_tangga/(:num)'] 	= "front_data_pdf/Mainekonomi/pengeluaran_rmt_tgg/$1"; //fn

	#------------------------------------------------------------------ Pemerintahan ------------ lw

		$route['pdf/rt_rw/(:num)'] 					= "front_data_pdf/Mainpemerintahan/pem_jml_rt_rw/$1"; //fn

	#------------------------------------------------------------------ Pendidikan --------------

		$route['pdf/pendidikan_baca_tulis/(:num)'] 			= "front_data_pdf/Mainpendidikan/pendidikan_baca_tulis/$1"; //fn
		$route['pdf/pendidikan_partisipasi_sklh/(:num)'] 		= "front_data_pdf/Mainpendidikan/pendidikan_partisipasi_sklh/$1"; //fn 
		$route['pdf/pendidikan_penduduk/(:num)'] 				= "front_data_pdf/Mainpendidikan/pendidikan_penduduk/$1"; //fn

	#------------------------------------------------------------------ Pertanian --------------- fn

		$route['pdf/pertanian_kelapa_tebu/(:num)'] 		= "front_data_pdf/Mainbudidaya/pertanian_kelapa_tebu/$1"; //fn

		$route['pdf/hortikultura/(:num)'] 				= "front_data_pdf/Mainbudidaya/pertanian_holti_jml/$1"; //fn
		$route['pdf/komoditi/(:num)'] 					= "front_data_pdf/Mainbudidaya/pertanian_komoditas/$1"; //fn

		$route['pdf/perikanan/(:num)'] 					= "front_data_pdf/Mainbudidaya/pertanian_ikan/$1"; //fn
		$route['pdf/peternakan/(:num)'] 				= "front_data_pdf/Mainbudidaya/pertanian_ternak/$1"; //fn

		$route['pdf/pertanian_lahan/(:num)'] 			= "front_data_pdf/Mainbudidaya/pertanian_lahan/$1"; //fn
		$route['pdf/pertanian_lahan_penggunaan/(:num)'] = "front_data_pdf/Mainbudidaya/pertanian_lahan_penggunaan/$1";

	#------------------------------------------------------------------ Tehnologi ---------------- fn

		$route['pdf/bts/(:num)'] 			 = "front_data_pdf/Maintehnologi/tehnologi_jml_bts/$1"; //fn
		$route['pdf/warnet/(:num)'] 		 = "front_data_pdf/Maintehnologi/tehnologi_jml_warnet/$1"; //fn
		$route['pdf/web_pemerintah/(:num)'] = "front_data_pdf/Maintehnologi/tehnologi_jml_web_opd/$1"; //fn

	#------------------------------------------------------------------ Transportasi ------------- fn

		$route['pdf/infrastruktur_jalan/(:num)'] 	= "front_data_pdf/Maintransportasi/trans_jln/$1"; //fn
		$route['pdf/kendaraan/(:num)'] 			= "front_data_pdf/Maintransportasi/trans_jml_kendaraan/$1"; //fn

	#------------------------------------------------------------------ Sosial ------------------- re

		$route['pdf/dpr/(:num)'] 					= "front_data_pdf/Maindpr/dpr/$1"; //fn

	#------------------------------------------------------------------ Parlemen ----------------- re

#------------------------------------------------------------------------------------------------
#----------------------------------------------------------------Data_Main_Pdf-------------------
#------------------------------------------------------------------------------------------------


#------------------------------------------------------------------------------------------------
#------------------------------------------------------------------Lampiran_Pdf------------------
#------------------------------------------------------------------------------------------------

	#------------------------------------------------------------------ Keuangan ----------------

		$route['pdf/lp_keuangan_e/(:num)'] 			= "front_lp_pdf/Lpkeuangan/index/$1"; //fn
		$route['pdf/tenaga/(:num)'] 				= "front_lp_pdf/Lpkeuangan/tenaga/$1"; //fn
		$route['pdf/lp_koperasi/(:num)'] 			= "front_lp_pdf/Lpkeuangan/koperasi/$1"; //fn
		$route['pdf/lp_pendapatan_indutri/(:num)'] 	= "front_lp_pdf/Lpkeuangan/pendapatan_ind/$1"; //fn 
		$route['pdf/lp_pajak/(:num)'] 				= "front_lp_pdf/Lppajak/pajak/$1"; //fn 

	#------------------------------------------------------------------ Pendidikan --------------

		$route['pdf/lp_perpustakaan/(:num)'] 		= "front_lp_pdf/Lppendidikan/perpustakaan/$1"; //fn
		$route['pdf/lp_pendidikan_all/(:num)'] 		= "front_lp_pdf/Lppendidikanall/pendidikan_all/$1";  //fn

	#------------------------------------------------------------------ lingkungan --------------

		$route['pdf/lp_tpa/(:num)'] 				= "front_lp_pdf/Lplingkungan/tpa/$1"; //fn

	#----------------------------------------------------------------- Transportasi -------------

		$route['pdf/lp_terminal/(:num)'] 			= "front_lp_pdf/Lptransportasi/terminal/$1"; //fn
		$route['pdf/lp_kendaraan/(:num)'] 			= "front_lp_pdf/Lptransportasi/kendaraan_kec/$1"; //fn
		$route['pdf/lp_kendaraan_plat/(:num)'] 		= "front_lp_pdf/Lptransportasi/kendaraan_plat/$1"; //fn
		$route['pdf/lp_kendaraan_umum/(:num)'] 		= "front_lp_pdf/Lptransportasi/kendaraan_umum/$1"; //fn

	#----------------------------------------------------------------- Pertanian ----------------
		$route['pdf/lp_lahan_kec/(:num)'] 				= "front_lp_pdf/Lppertanian/lahan_kec/$1"; //fn
		$route['pdf/lp_panen/(:num)'] 					= "front_lp_pdf/Lppertanian/panen_kec/$1"; //fn
		$route['pdf/lp_hasil_panen/(:num)'] 			= "front_lp_pdf/Lppertanian/hasil_panen_kec/$1"; //fn 
		$route['pdf/lp_prod_panen/(:num)'] 				= "front_lp_pdf/Lppertanian/prod_panen_kec/$1"; //fn
		$route['pdf/lp_pertanian_hasil/(:num)']  		= "front_lp_pdf/Lppertanianall/pertanian_all/$1"; //fn

	#----------------------------------------------------------------- Penduduk -----------------
		$route['pdf/lp_penduduk_jk/(:num)'] 		= "front_lp_pdf/Lppenduduk/penduduk_jk/$1";  //fn
		$route['pdf/lp_penduduk_rasio/(:num)'] 		= "front_lp_pdf/Lppenduduk/penduduk_rasio/$1";  //fn
		$route['pdf/lp_penduduk_umur/(:num)'] 		= "front_lp_pdf/Lppenduduk/penduduk_umur/$1";  //fn

	#----------------------------------------------------------------- Tehnologi ----------------

		$route['pdf/lp_tehnologi/(:num)'] 			= "front_lp_pdf/Lptehnologi/tehnologi/$1"; //fn

	#----------------------------------------------------------------- pmks ---------------------

		$route['pdf/lp_pmks/(:num)'] 				= "front_lp_pdf/Lppmks/pmks/$1"; //fn

	#----------------------------------------------------------------- hukum --------------------
		$route['pdf/lp_aparat/(:num)'] 				= "front_lp_pdf/Lpaparat/aparat/$1"; //fn
		$route['pdf/lp_pidana/(:num)'] 				= "front_lp_pdf/Lphukum/pidana/$1"; //fn
		$route['pdf/lp_pidana_kec/(:num)'] 			= "front_lp_pdf/Lphukum/pidana_kec/$1";	//fn

	#----------------------------------------------------------------- Miskin -------------------

		$route['pdf/lp_miskin/(:num)'] 				= "front_lp_pdf/Lpmiskin/miskin/$1";  //fn

	#----------------------------------------------------------------- Bencana ------------------
		
		$route['pdf/lp_bencana/(:num)'] 			= "front_lp_pdf/Lpbencana/bencana/$1"; //fn

	#----------------------------------------------------------------- olahraga -----------------
		
		$route['pdf/lp_olahraga/(:num)'] 			= "front_lp_pdf/Lpolahraga/olahraga/$1"; //fn

	#----------------------------------------------------------------- PDB ----------------------
		
		$route['pdf/lp_pdb/(:num)'] 				= "front_lp_pdf/Lppdb/pdb/$1"; //fn

	#----------------------------------------------------------------- Kesehatan ----------------
		
		$route['pdf/lp_kesehatan_all/(:num)']= "front_lp_pdf/Lpkesehatanall/kesehatan_all/$1"; //fn

	#----------------------------------------------------------------- Diskominfo ---------------
		$route['pdf/domain'] 						= "front_lp_pdf/Lpdiskominfo/get_domain/"; //fn
		$route['pdf/aplikasi'] 						= "front_lp_pdf/Lpdiskominfo/get_aplikasi/"; //fn

	#----------------------------------------------------------------- Perdagangan --------------
		
		$route['pdf/perdagangan/(:num)'] 			= "front_lp_pdf/Lpdisperdagangan/perdagangan/$1"; //fn 

	#----------------------------------------------------------------- Disperkim ----------------
		$route['pdf/csr'] 							= "front_lp_pdf/Lpdisperkim/get_csr/"; //fn
		$route['pdf/psu'] 							= "front_lp_pdf/Lpdisperkim/get_dsu/"; //fn
		$route['pdf/disperkim_all/(:num)/(:num)'] 	= "front_lp_pdf/Lpdisperkim/get_disperkim_all/$1/$2"; //fn
		$route['pdf/tanaman/(:num)'] 				= "front_lp_pdf/Lpdisperkim/get_pohon/$1"; //fn
		$route['pdf/taman/(:num)'] 					= "front_lp_pdf/Lpdisperkim/get_taman/$1"; //fn

	#----------------------------------------------------------------- Disnaker -----------------
		
		$route['pdf/disnaker/(:num)'] 		= "front_lp_pdf/Lpdisnakerall/disnaker_all/$1"; //fn

	#----------------------------------------------------------------- Disbudpar ----------------
		$route['pdf/kawasan_strategis'] 			= "front_lp_pdf/Disbudpar/index_dts/$1"; //fn
		$route['pdf/pengunjung_wisata/(:num)'] 		= "front_lp_pdf/Disbudpar/index_dtw/$1"; //fn
		$route['pdf/pengunjung_hotel/(:num)'] 		= "front_lp_pdf/Disbudpar/index_hotel/$1"; //fn
		$route['pdf/objek_pemajuan_budaya/(:num)'] 	= "front_lp_pdf/Disbudpar/index_obj_bud/$1"; //gaiso
		$route['pdf/zona_kreatif/(:num)'] 			= "front_lp_pdf/Disbudpar/index_zona/$1"; //fn

	#----------------------------------------------------------------- DPMPTSP ----------------
		$route['pdf/daftar_perijinan_investasi/(:num)'] 	= "front_lp_pdf/Lpdpmptsp/lp_dpmptsp_investasi/$1"; //fn
		$route['pdf/daftar_perijinan_terbit/(:num)'] 		= "front_lp_pdf/Lpdpmptsp/lp_dpmptsp_terbit/$1"; //fn
		$route['pdf/daftar_perijinan_tk/(:num)'] 			= "front_lp_pdf/Lpdpmptsp/lp_dpmptsp_tk/$1"; //fn

	#----------------------------------------------------------------- BPKAD ----------------
		$route['pdf/kepegawaian/([a-z]+)/(:num)'] 			= "front_lp_pdf/bpkad/get_json_file/$1/$2"; //fn